/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef EXT_HEADERS_H
#define EXT_HEADERS_H

#include "dmg-information-elements.h"
#include "fields-headers.h"

#include "ns3/dmg-capabilities.h"
#include "ns3/header.h"
#include "ns3/mac48-address.h"
#include "ns3/mgt-headers.h"

namespace ns3 {

/**********************************************************
 *  Channel Measurement Info field format (Figure 8-502h)
 **********************************************************/

/**
 * \ingroup wigig
 * Implement the header for Channel Measurement Info field format (Figure
 * 8-502h)
 */
class ExtChannelMeasurementInfo
    : public SimpleRefCount<ExtChannelMeasurementInfo> {
public:
  ExtChannelMeasurementInfo();
  ~ExtChannelMeasurementInfo();

  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the AID of the STA toward which the reporting STA measures link.
   * \param aid
   */
  void SetPeerStaAid(uint16_t aid);
  /**
   * The SNR subfield indicates the SNR measured in the link toward the STA
   * corresponding to Peer STA AID. This field is encoded as 8-bit twos
   * complement value of 4x(SNR-19), where SNR is measured in dB. This covers
   * from –13 dB to 50.75 dB in 0.25 dB steps. \param
   */
  void SetSnr(uint8_t snr);
  /**
   * The Internal Angle subfield indicates the angle between directions toward
   * the other STAs involved in the relay operation. This covers from 0 degree
   * to 180 degree in 2 degree steps. This subfield uses the degree of the
   * direction from the sector that the feedbacks indicates has highest quality
   * during TXSS if SLS phase of BF is performed and RXSS is not included.
   * \param angle
   */
  void SetInternalAngle(uint8_t angle);
  /**
   * The Recommend subfield indicates whether the responding STA recommends the
   * relay operation based on the channel measurement with the Peer STA. This
   * subfield is set to 1 when the relay operation is recommended and otherwise
   * is set to 0. \param value
   */
  void SetRecommendSubField(bool value);

  uint16_t GetPeerStaAid() const;
  uint8_t GetSnr() const;
  uint8_t GetInternalAngle() const;
  bool GetRecommendSubField() const;

private:
  uint16_t m_aid; //!< Peer STA AID.
  uint8_t m_snr;
  uint8_t m_angle;
  bool m_recommend;
};

enum BssType { Reserved = 0, IBSS = 1, PBSS = 2, InfrastructureBSS = 3 };

/******************************************
 *  DMG Parameters Field (8.4.1.46)
 *******************************************/

/**
 * \ingroup wigig
 * Implement the header for DMG Parameters Field.
 */
class ExtDmgParameters : public ObjectBase {
public:
  ExtDmgParameters();
  ~ExtDmgParameters() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the Basic Service Set (BSS) Type.
   *
   * \param duration The duration of the beacon frame
   */
  void SetBssType(BssType type);
  /**
   * The CBAP Only subfield indicates the type of link access provided by the
   * STA sending the DMG Beacon frame in the data transfer interval (DTI)
   * (see 9.33.2) of the beacon interval. The CBAP Only subfield is set to 1
   * when the entirety of the DTI portion of the beacon interval is allocated as
   * a CBAP. The CBAP Only subfield is set to 0 when the allocation of the DTI
   * portion of the beacon interval is provided through the Extended Schedule
   * element. \param value
   */
  void SetCbapOnly(bool value);
  /**
   * The CBAP Source subfield is valid only if the CBAP Only subfield is 1. The
   * CBAP Source subfield is set to 1 to indicate that the PCP/AP has higher
   * priority to initiate transmissions during the CBAP than non-PCP/non-AP
   * STAs. The CBAP Source subfield is set to 0 otherwise. \param value
   */
  void SetCbapSource(bool value);
  void SetDmgPrivacy(bool value);
  /**
   * The ECPAC Policy Enforced subfield is set to 1 to indicate that medium
   * access policies specific to the centralized PCP/AP cluster are required as
   * defined in 9.34.3.4. The ECPAC Policy Enforced subfield is set to 0 to
   * indicate that medium access policies specific to the centralized PCP/AP
   * cluster are not required. \param value
   */
  void SetEcpacPolicyEnforced(bool value);

  /**
   * Return the Basic Service Set (BSS) Type.
   *
   * \return BssType
   */
  BssType GetBssType() const;
  bool GetCbapOnly() const;
  bool GetCbapSource() const;
  bool Get_DMG_Privacy() const;
  bool GetEcpacPolicyEnforced() const;

private:
  BssType m_bssType;          //!< BSS Type.
  bool m_cbapOnly;            //!< CBAP Only.
  bool m_cbapSource;          //!< CBAP Source.
  bool m_dmgPrivacy;          //!< DMG Privacy.
  bool m_ecpacPolicyEnforced; //!< ECPAC Policy Enforced.
};

/******************************************
 *   Beacon Interval Control Field (8-34b)
 *******************************************/

/**
 * \ingroup wigig
 * Implement the header for DMG Beacon Interval Control Field.
 */
class ExtDmgBeaconIntervalCtrlField : public ObjectBase {
public:
  ExtDmgBeaconIntervalCtrlField();
  ~ExtDmgBeaconIntervalCtrlField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * The CC Present field is set to 1 to indicate that the Clustering Control
   * field is present in the DMG Beacon. Otherwise, the Clustering Control field
   * is not present. \param value
   */
  void SetCcPresent(bool value);
  /**
   * The Discovery Mode field is set to 1 if the STA is generating the DMG
   * Beacon following the procedure described in 10.1.3.2b. Otherwise, this
   * field is set to 0. \param value
   */
  void SetDiscoveryMode(bool value);
  /**
   * The Next Beacon field indicates the number of beacon intervals following
   * the current beacon interval during which the DMG Beacon is not be present.
   * \param value
   */
  void SetNextBeacon(uint8_t value);
  /**
   * The ATI Present field is set to 1 to indicate that the announcement
   * transmission interval (ATI) is present in the current beacon interval.
   * Otherwise, the ATI is not present. \param value
   */
  void SetAtiPresent(bool value);
  /**
   * The A-BFT Length field specifies the size of the A-BFT following the BTI,
   * and is defined in units of a sector sweep slot (9.35.5). The value of this
   * field is in the range of 1 to 8, with the value being equal to the bit
   * representation plus 1. \param length The length of A-BFT period
   */
  void SetAbftLength(uint8_t length);
  /**
   * The FSS field specifies the number of SSW frames allowed per sector sweep
   * slot (9.35.5). The value of this field is in the range of 1 to 16, with the
   * value being equal to the bit representation plus 1. \param number The
   * number of SSW frames per sector sweep slot.
   */
  void SetFss(uint8_t number);
  /**
   * The IsResponderTxss field is set to 1 to indicate the A-BFT following the
   * BTI is used for responder transmit sector sweep (TXSS). This field is set
   * to 0 to indicate responder receive sector sweep (RXSS). When this field is
   * set to 0, the FSS field specifies the length of a complete receive sector
   * sweep by the STA sending the DMG Beacon frame. \param value
   */
  void SetIsResponderTxss(bool value);
  /**
   * The Next A-BFT field indicates the number of beacon intervals during which
   * the A-BFT is not be present. A value of 0 indicates that the A-BFT
   * immediately follows this BTI. \param value
   */
  void SetNextAbft(uint8_t value);
  /**
   * The Fragmented TXSS field is set to 1 to indicate the TXSS is a fragmented
   * sector sweep, and is set to 0 to indicate the TXSS is a complete sector
   * sweep. \param value
   */
  void SetFragmentedTxss(bool value);
  /**
   * The TXSS Span field indicates the number of beacon intervals it takes for
   * the STA sending the DMG Beacon frame to complete the TXSS phase. This field
   * is always greater than or equal to one. \param value
   */
  void SetTxssSpan(uint8_t value);
  /**
   * The N BIs A-BFT field indicates the interval, in number of beacon
   * intervals, at which the STA sending the DMG Beacon frame allocates an
   * A-BFT. A value of 1 indicates that every beacon interval contains an A-BFT.
   * \param value
   */
  void SetNBi(uint8_t value);
  /**
   * The A-BFT Count field indicates the number of A-BFTs since the STA sending
   * the DMG Beacon frame last switched RX DMG antennas for an A-BFT. A value of
   * 0 indicates that the DMG antenna used in the forthcoming A-BFT differs from
   * the DMG antenna used in the last A-BFT. This field is reserved if the value
   * of the Number of RX DMG Antennas field within the STA’s DMG Capabilities
   * element is 1. \param value
   */
  void SetAbftCount(uint8_t value);
  /**
   * The N A-BFT in Ant field indicates how many A-BFTs the STA sending the DMG
   * Beacon frame receives from each DMG antenna in the DMG antenna receive
   * rotation. This field is reserved if the value of the Number of RX DMG
   * Antennas field within the STA’s DMG Capabilities element is 1. \param value
   */
  void SetNAbftAnt(uint8_t value);
  /**
   * The PCP Association Ready field is set to 1 to indicate that the PCP is
   * ready to receive Association Request frames from non-PCP STAs and is set to
   * 0 otherwise. This field is reserved when transmitted by a non-PCP STA.
   * \param value
   */
  void SetPcpAssociationReady(bool value);

  bool IsCcPresent() const;
  bool IsDiscoveryMode() const;
  uint8_t GetNextBeacon() const;
  bool IsAtiPresent() const;
  uint8_t GetAbftLength() const;
  uint8_t GetFss() const;
  bool IsResponderTxss() const;
  uint8_t GetNextAbft() const;
  bool GetFragmentedTxss() const;
  uint8_t GetTxssSpan() const;
  uint8_t GetNBi() const;
  uint8_t GetAbftCount() const;
  uint8_t GetNAbftAnt() const;
  bool GetPcpAssociationReady() const;

private:
  bool m_ccPresent;
  bool m_discoveryMode;
  uint8_t m_nextBeacon;
  bool m_atiPresent;
  uint8_t m_abftLength;
  uint8_t m_fss;
  bool m_isResponderTxss;
  uint8_t m_nextAbft;
  bool m_fragmentedTxss;
  uint8_t m_txssSpan;
  uint8_t m_nBi;
  uint8_t m_abftCount;
  uint8_t m_nAbftAnt;
  bool m_pcpAssociationReady;
};

using WigigProbeRequestElems = std::tuple<Ssid, std::optional<SupportedRates>,
                                          std::optional<DmgCapabilities>>;

using WigigProbeResponseElems =
    std::tuple<Ssid, std::optional<SupportedRates>,
               std::optional<DmgCapabilities>,
               std::optional<DmgOperationElement>, std::optional<NextDmgAti>,
               std::optional<RelayCapabilitiesElement>,
               std::optional<ExtendedScheduleElement>>;

using WigigAssocResponseElems =
    std::tuple<std::optional<SupportedRates>,
               std::optional<ExtendedSupportedRatesIE>,
               std::optional<RequestElement>, std::vector<DmgCapabilities>>;

/// List of Information Elements included in Association Request frames
using WigigAssocRequestElems = std::tuple<
    std::optional<Ssid>, std::optional<SupportedRates>,
    std::optional<DmgTspecElement>, std::optional<RelayCapabilitiesElement>,
    std::optional<BeamRefinementElement>, std::optional<RequestElement>,
    std::optional<DmgCapabilities>, std::optional<StaAvailabilityElement>>;

/******************************************
 *       DMG Beacon (8.3.4.1)
 *******************************************/
/**
 * \ingroup wigig
 * Implement the header for DMG Beacon.
 */
class ExtDmgBeacon
    : public WifiMgtHeader<ExtDmgBeacon, WigigProbeResponseElems> {
  friend class WifiMgtHeader<ExtDmgBeacon, WigigProbeResponseElems>;

public:
  ExtDmgBeacon();
  ~ExtDmgBeacon() override;

  /**
   * Set the Basic Service Set Identifier (BSSID).
   *
   * \param ssid BSSID
   */
  void SetBssid(Mac48Address bssid);
  /**
   * Set the Timestamp in the DMG Beacon frame body.
   *
   * \param timestamp The timestamp.
   */
  void SetTimestamp(uint64_t timestamp);
  /**
   * Set Sector Sweep Information Field in the DMG Beacon frame body.
   *
   * \param ssw The sector sweep information field.
   */
  void SetSswField(DmgSswField &ssw);
  /**
   * Set the DMG Beacon Interval.
   *
   * \param interval The DMG Beacon Interval.
   */
  void SetBeaconIntervalUs(uint64_t interval);
  /**
   * Set Beacon Interval Control Field in the DMG Beacon frame body.
   *
   * \param ssw The Beacon Interval Control field.
   */
  void SetBeaconIntervalControlField(ExtDmgBeaconIntervalCtrlField &ctrl);
  /**
   * Set DMG Parameters Field in the DMG Beacon frame body.
   *
   * \param parameters The DMG Parameters field.
   */
  void SetBeaconIntervalControlField(ExtDmgParameters &parameters);
  /**
   * Set DMG Parameters Field in the DMG Beacon frame body.
   *
   * \param The DMG Parameters field.
   */
  void SetDmgParameters(ExtDmgParameters &parameters);
  /**
   * Set DMG Cluster Control Field
   *
   * \param The DMG Cluster Control Field.
   */
  void SetClusterControlField(ExtDmgClusteringControlField &cluster);

  /**
   * Return the Service Set Identifier (SSID).
   *
   * \return SSID
   */
  Mac48Address GetBssid() const;
  /**
   * Get the Timestamp in the DMG Beacon frame body.
   *
   * \return The timestamp.
   */
  uint64_t GetTimestamp() const;
  /**
   * Get Sector Sweep Information Field in the DMG Beacon frame body.
   *
   * \return The sector sweep information field.
   */
  DmgSswField GetSswField() const;
  /**
   * Get the DMG Beacon Interval.
   *
   * \return The DMG Beacon Interval.
   */
  uint64_t GetBeaconIntervalUs() const;
  /**
   * Get Beacon Interval Control Field in the DMG Beacon frame body.
   *
   * \return The Beacon Interval Control field.
   */
  ExtDmgBeaconIntervalCtrlField GetBeaconIntervalControlField() const;
  /**
   * Get DMG Parameters Field in the DMG Beacon frame body.
   *
   * \return The DMG Parameters field.
   */
  ExtDmgParameters GetDmgParameters() const;
  /**
   * Get DMG Cluster Control Field
   *
   * \return The DMG Cluster Control Field.
   */
  ExtDmgClusteringControlField GetClusterControlField() const;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  // void Print (std::ostream &os) const override;
  // uint32_t GetSerializedSize (void) const override;
  // void Serialize (Buffer::Iterator start) const override;
  // uint32_t Deserialize (Buffer::Iterator start) override;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize*/
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;

private:
  Mac48Address m_bssid;      //!< Basic Service Set ID (SSID).
  uint64_t m_timestamp;      //!< Timestamp.
  DmgSswField m_ssw;         //!< Sector Sweep Field.
  uint64_t m_beaconInterval; //!< Beacon Interval.
  ExtDmgBeaconIntervalCtrlField
      m_beaconIntervalCtrl;               //!< Beacon Interval Control.
  ExtDmgParameters m_dmgParameters;       //!< DMG Parameters.
  ExtDmgClusteringControlField m_cluster; //!< Cluster Control Field.
};

} // namespace ns3

#endif /* EXT_HEADERS_H */
