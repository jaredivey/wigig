/*
 * Copyright (c) 2017
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Sébastien Deronne <sebastien.deronne@gmail.com>
 */

#include "simple-wigig-frame-capture-model.h"

#include "wigig-phy.h"

#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("SimpleWigigFrameCaptureModel");

NS_OBJECT_ENSURE_REGISTERED(SimpleWigigFrameCaptureModel);

TypeId SimpleWigigFrameCaptureModel::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::SimpleWigigFrameCaptureModel")
          .SetParent<WigigFrameCaptureModel>()
          .SetGroupName("Wigig")
          .AddConstructor<SimpleWigigFrameCaptureModel>()
          .AddAttribute(
              "Margin",
              "Reception is switched if the newly arrived frame has a power "
              "higher than "
              "this value above the frame currently being received (expressed "
              "in dB).",
              DoubleValue(5),
              MakeDoubleAccessor(&SimpleWigigFrameCaptureModel::GetMargin,
                                 &SimpleWigigFrameCaptureModel::SetMargin),
              MakeDoubleChecker<double>());
  return tid;
}

SimpleWigigFrameCaptureModel::SimpleWigigFrameCaptureModel() {
  NS_LOG_FUNCTION(this);
}

SimpleWigigFrameCaptureModel::~SimpleWigigFrameCaptureModel() {
  NS_LOG_FUNCTION(this);
}

void SimpleWigigFrameCaptureModel::SetMargin(double margin) {
  NS_LOG_FUNCTION(this << margin);
  m_margin = margin;
}

double SimpleWigigFrameCaptureModel::GetMargin() const { return m_margin; }

bool SimpleWigigFrameCaptureModel::CaptureNewFrame(
    Ptr<WigigSignalEvent> currentEvent, Ptr<WigigSignalEvent> newEvent) const {
  NS_LOG_FUNCTION(this);
  return (WToDbm(currentEvent->GetRxPowerW()) + GetMargin()) <
             WToDbm(newEvent->GetRxPowerW()) &&
         (IsInCaptureWindow(currentEvent->GetStartTime()));
}

} // namespace ns3
