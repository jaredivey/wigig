/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "codebook.h"

#include "wigig-net-device.h"

#include "ns3/log.h"
#include "ns3/uinteger.h"

#include <algorithm>
#include <fstream>
#include <numeric>
#include <string>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("Codebook");

NS_OBJECT_ENSURE_REGISTERED(Codebook);

TypeId Codebook::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::Codebook")
          .SetGroupName("Wigig")
          .SetParent<Object>()
          .AddAttribute("ActiveRfChainId",
                        "The ID of the current active RF chain.",
                        UintegerValue(1),
                        MakeUintegerAccessor(&Codebook::m_activeRfChainId),
                        MakeUintegerChecker<uint8_t>());
  return tid;
}

Codebook::Codebook()
    : m_fileName(""), m_rfChainList(), m_activeRfChainList(),
      m_activeRfChainId(1), m_activeRfChain(nullptr), m_antennaArrayList(),
      m_currentBFPhase(BHI_PHASE), m_sectorSweepType(TransmitSectorSweep),
      m_currentSectorIndex(0), m_remainingSectors(0), m_beamformingAntenna(),
      m_beamformingSectorList(), m_currentSectorList(nullptr), m_peerStation(),
      m_txBeamformingSectors(), m_rxBeamformingSectors(), m_txCustomSectors(),
      m_rxCustomSectors(), m_totalTxSectors(0), m_totalRxSectors(0),
      m_totalSectors(0), m_totalAntennas(0), m_bhiAntennaList(),
      m_beaconRandomization(false), m_btiSectorOffset(0), m_bhiAntennaI(),
      m_currentAwvI() {
  NS_LOG_FUNCTION(this);
}

Codebook::~Codebook() { NS_LOG_FUNCTION(this); }

void Codebook::DoInitialize() {
  NS_LOG_FUNCTION(this);

  /* Activate the first phased antenna array */
  SetActiveRfChainId(m_activeRfChainId);

  /* Initialize each RF chain associated with this codebook and set the RF chain
   * ID */
  for (const auto &rfChain : m_rfChainList) {
    rfChain.second->Initialize();
  }

  /* Get the first antenna array in the list and set the active antenna to it.
   */
  m_bhiAntennaI = m_bhiAntennaList.find(m_activeRfChain->GetActiveAntennaId());
  NS_ASSERT_MSG(m_bhiAntennaI != m_bhiAntennaList.end(),
                "Cannot find the specified AntennaId");
  m_beamformingSectorList = m_bhiAntennaI->second;

  /* At initialization stage, a DMG STA should be in quasi-omni receiving mode
   */
  SetReceivingInQuasiOmniMode();
}

std::string Codebook::GetCodebookFileName() const { return m_fileName; }

uint8_t Codebook::GetTotalNumberOfTransmitSectors() const {
  return m_totalTxSectors;
}

uint8_t Codebook::GetTotalNumberOfReceiveSectors() const {
  return m_totalRxSectors;
}

uint8_t Codebook::GetTotalNumberOfSectors() const { return m_totalSectors; }

uint8_t Codebook::GetTotalNumberOfAntennas() const { return m_totalAntennas; }

void Codebook::AppendToSectorList(Antenna2SectorList &globalList,
                                  AntennaId antennaID, SectorId sectorID) {
  const auto iter = globalList.find(antennaID);
  if (iter != globalList.cend()) {
    SectorIdList *sectorList = &(iter->second);
    sectorList->push_back(sectorID);
  } else {
    SectorIdList sectorList;
    sectorList.push_back(sectorID);
    globalList[antennaID] = sectorList;
  }
}

uint8_t Codebook::GetNumberOfSectorsInBHI() {
  return m_bhiAntennaI->second.size();
}

void Codebook::AppendBeamformingSector(SectorSweepType type,
                                       AntennaId antennaID, SectorId sectorID) {
  NS_LOG_FUNCTION(this << static_cast<SectorSweepType>(type) << +antennaID
                       << +sectorID);
  if (type == TransmitSectorSweep) {
    AppendToSectorList(m_txBeamformingSectors, antennaID, sectorID);
  } else {
    AppendToSectorList(m_rxBeamformingSectors, antennaID, sectorID);
  }
}

uint8_t Codebook::GetNumberOfSectorsForBeamforming(SectorSweepType type) {
  if (type == TransmitSectorSweep) {
    return CountNumberOfSectors(&m_txBeamformingSectors);
  } else {
    return CountNumberOfSectors(&m_rxBeamformingSectors);
  }
}

void Codebook::SetBeamformingSectorList(SectorSweepType type,
                                        Mac48Address address,
                                        Antenna2SectorList &sectorList) {
  if (type == TransmitSectorSweep) {
    m_txCustomSectors[address] = sectorList;
  } else {
    m_rxCustomSectors[address] = sectorList;
  }
}

uint8_t Codebook::GetNumberOfSectorsForBeamforming(Mac48Address address,
                                                   SectorSweepType type) {
  if (type == TransmitSectorSweep) {
    return GetNumberOfSectors(address, m_txCustomSectors);
  } else {
    return GetNumberOfSectors(address, m_rxCustomSectors);
  }
}

uint8_t Codebook::GetNumberOfSectors(Mac48Address address,
                                     BeamformingSectorList &list) {
  const auto iter = list.find(address);
  if (iter != list.cend()) {
    return CountNumberOfSectors(&iter->second);
  } else {
    return 0;
  }
}

void Codebook::SetDevice(Ptr<WigigNetDevice> device) { m_device = device; }

Ptr<WigigNetDevice> Codebook::GetDevice() const { return m_device; }

void Codebook::SetActiveTxAwvId(AwvId awvId) {
  NS_LOG_FUNCTION(this << +awvId);
  m_activeRfChain->SetActiveTxAwvId(awvId);
}

void Codebook::SetActiveRxAwvId(AwvId awvId) {
  NS_LOG_FUNCTION(this << +awvId);
  m_activeRfChain->SetActiveRxAwvId(awvId);
}

void Codebook::SetActiveTxSectorId(SectorId sectorID) {
  NS_LOG_FUNCTION(this << +sectorID);
  m_activeRfChain->SetActiveTxSectorId(sectorID);
}

void Codebook::SetActiveRxSectorId(SectorId sectorID) {
  NS_LOG_FUNCTION(this << +sectorID);
  m_activeRfChain->SetActiveRxSectorId(sectorID);
}

void Codebook::SetActiveTxSectorId(AntennaId antennaID, SectorId sectorID) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID);
  m_activeRfChain->SetActiveTxSectorId(antennaID, sectorID);
}

void Codebook::SetActiveRxSectorId(AntennaId antennaID, SectorId sectorID) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID);
  m_activeRfChain->SetActiveRxSectorId(antennaID, sectorID);
}

AntennaId Codebook::GetActiveAntennaId() const {
  return m_activeRfChain->GetActiveAntennaId();
}

SectorId Codebook::GetActiveTxSectorId() const {
  return m_activeRfChain->GetActiveTxSectorId();
}

SectorId Codebook::GetActiveRxSectorId() const {
  return m_activeRfChain->GetActiveRxSectorId();
}

void Codebook::RandomizeBeacon(bool beaconRandomization) {
  NS_LOG_FUNCTION(this << beaconRandomization);
  m_beaconRandomization = beaconRandomization;
}

void Codebook::StartBtiAccessPeriod() {
  NS_LOG_FUNCTION(this);
  SectorId sectorID;
  m_currentBFPhase = BHI_PHASE;
  if (m_beaconRandomization) {
    if (m_btiSectorOffset == m_beamformingSectorList.size()) {
      m_btiSectorOffset = 0;
    }
    m_currentSectorIndex = m_btiSectorOffset;
    sectorID = m_beamformingSectorList.at(m_currentSectorIndex);
    m_btiSectorOffset++;
  } else {
    sectorID = m_beamformingSectorList.front();
    m_currentSectorIndex = 0;
  }
  /* Set the directional sector for transmission in BTI */
  SetActiveTxSectorId(m_bhiAntennaI->first, sectorID);
  /* Set the remaining number of sectors in the current BTI */
  m_remainingSectors = m_beamformingSectorList.size() - 1;
}

bool Codebook::GetNextSectorInBti() {
  NS_LOG_FUNCTION(this);
  if (m_remainingSectors == 0) {
    /* The PCP/AP shall not change DMG antennas within a BTI. */
    m_bhiAntennaI++;
    if (m_bhiAntennaI == m_bhiAntennaList.end()) {
      m_bhiAntennaI = m_bhiAntennaList.begin();
    }
    m_beamformingSectorList = m_bhiAntennaI->second;
    return false;
  } else {
    m_currentSectorIndex++;
    if (m_beaconRandomization &&
        (m_currentSectorIndex == m_beamformingSectorList.size())) {
      m_currentSectorIndex = 0;
    }
    SetActiveTxSectorId(m_beamformingSectorList.at(m_currentSectorIndex));
    m_remainingSectors--;
    return true;
  }
}

uint8_t Codebook::GetNumberOfBIs() const { return m_bhiAntennaList.size(); }

uint16_t Codebook::GetRemainingSectorCount() const {
  return m_remainingSectors;
}

uint8_t Codebook::CountNumberOfSectors(Antenna2SectorList *sectorList) {
  uint8_t totalSector = 0;
  for (Antenna2SectorListI iter = sectorList->begin();
       iter != sectorList->end(); iter++) {
    totalSector += iter->second.size();
  }
  return totalSector;
}

void Codebook::InitiateAbft(Mac48Address address) {
  NS_LOG_DEBUG(this << address);
  m_currentBFPhase = BHI_PHASE;
  m_bhiAntennaI = m_bhiAntennaList.begin();
  m_beamformingSectorList = m_bhiAntennaI->second;
  SetActiveTxSectorId(m_bhiAntennaI->first, m_beamformingSectorList.front());
  m_peerStation = address;
  m_currentSectorIndex = 0;
  m_sectorSweepType = TransmitSectorSweep;
  /* Set the remaining number of sectors in the current BTI */
  m_remainingSectors = m_beamformingSectorList.size() - 1;
}

bool Codebook::GetNextSectorInAbft() {
  NS_LOG_FUNCTION(this);
  if (m_remainingSectors == 0) {
    /* The PCP/AP shall not change DMG antennas within a A-BFT. */
    m_bhiAntennaI++;
    if (m_bhiAntennaI == m_bhiAntennaList.end()) {
      m_bhiAntennaI = m_bhiAntennaList.begin();
    }
    m_beamformingSectorList = m_bhiAntennaI->second;
    return false;
  } else {
    m_currentSectorIndex++;
    SetActiveTxSectorId(m_beamformingSectorList.at(m_currentSectorIndex));
    m_remainingSectors--;
    return true;
  }
}

void Codebook::StartSectorSweeping(Mac48Address address, SectorSweepType type,
                                   uint8_t peerAntennas) {
  NS_LOG_DEBUG(this << address << type << +peerAntennas);
  if (type == TransmitSectorSweep) {
    /* Transmit Sector Sweep */
    const auto iter = m_txCustomSectors.find(address);
    if (iter != m_txCustomSectors.cend()) {
      m_currentSectorList = (Antenna2SectorList *)&iter->second;
    } else {
      m_currentSectorList = &m_txBeamformingSectors;
    }
    m_beamformingAntenna = m_currentSectorList->begin();
    m_beamformingSectorList = m_beamformingAntenna->second;
    SetActiveTxSectorId(m_beamformingAntenna->first,
                        m_beamformingSectorList.front());
  } else {
    /* Receive Sector Sweep */
    const auto iter = m_rxCustomSectors.find(address);
    if (iter != m_rxCustomSectors.cend()) {
      m_currentSectorList = (Antenna2SectorList *)&iter->second;
    } else {
      m_currentSectorList = &m_rxBeamformingSectors;
    }
    m_beamformingAntenna = m_currentSectorList->begin();
    m_beamformingSectorList = m_beamformingAntenna->second;
    SetActiveRxSectorId(m_beamformingAntenna->first,
                        m_beamformingSectorList.front());
  }
  m_currentBFPhase = SLS_PHASE;
  m_sectorSweepType = type;
  m_peerStation = address;
  m_currentSectorIndex = 0;
  m_remainingSectors =
      CountNumberOfSectors(m_currentSectorList) * peerAntennas - 1;
}

bool Codebook::GetNextSector(bool &changeAntenna) {
  NS_LOG_DEBUG("Remaining Sectors=" << m_remainingSectors);
  if (m_remainingSectors == 0) {
    changeAntenna = false;
    return false;
  } else {
    m_currentSectorIndex++;
    /* Check if we have changed the current used Antenna */
    if (m_currentSectorIndex == m_beamformingSectorList.size()) {
      changeAntenna = true;
      m_beamformingAntenna++;
      /* This is a special case when we have multiple antennas */
      if (m_beamformingAntenna == m_currentSectorList->end()) {
        m_beamformingAntenna = m_currentSectorList->begin();
      }
      m_beamformingSectorList = m_beamformingAntenna->second;
      m_currentSectorIndex = 0;
      NS_LOG_DEBUG(
          "Switch to the next AntennaId=" << +m_beamformingAntenna->first);
    } else {
      changeAntenna = false;
    }

    if (m_sectorSweepType == TransmitSectorSweep) {
      SetActiveTxSectorId(m_beamformingAntenna->first,
                          m_beamformingSectorList[m_currentSectorIndex]);
    } else {
      SetActiveRxSectorId(m_beamformingAntenna->first,
                          m_beamformingSectorList[m_currentSectorIndex]);
    }

    m_remainingSectors--;
    return true;
  }
}

void Codebook::SetReceivingInQuasiOmniMode() {
  NS_LOG_FUNCTION(this);
  m_activeRfChain->SetReceivingInQuasiOmniMode();
}

void Codebook::SetReceivingInQuasiOmniMode(AntennaId antennaID) {
  NS_LOG_FUNCTION(this << +antennaID);
  m_activeRfChain->SetReceivingInQuasiOmniMode(antennaID);
}

void Codebook::StartReceivingInQuasiOmniMode() {
  NS_LOG_FUNCTION(this);
  m_activeRfChain->StartReceivingInQuasiOmniMode();
}

bool Codebook::SwitchToNextQuasiPattern() {
  NS_LOG_FUNCTION(this);
  return m_activeRfChain->SwitchToNextQuasiPattern();
}

void Codebook::SetReceivingInDirectionalMode() {
  NS_LOG_FUNCTION(this);
  m_activeRfChain->SetReceivingInDirectionalMode();
}

void Codebook::CopyCodebook(const Ptr<Codebook> codebook) {
  NS_LOG_FUNCTION(this << codebook);
  /* Sectors Data */
  m_txBeamformingSectors = codebook->m_txBeamformingSectors;
  m_rxBeamformingSectors = codebook->m_rxBeamformingSectors;
  m_totalTxSectors = codebook->m_totalTxSectors;
  m_totalRxSectors = codebook->m_totalRxSectors;
  m_totalAntennas = codebook->m_totalAntennas;
  m_txCustomSectors = codebook->m_txCustomSectors;
  m_rxCustomSectors = codebook->m_rxCustomSectors;
  m_activeRfChainId = codebook->m_activeRfChainId;
  /* BTI Variables */
  m_bhiAntennaList = codebook->m_bhiAntennaList;
}

void Codebook::ChangeAntennaOrientation(AntennaId antennaID, double psi,
                                        double theta, double phi) {
  NS_LOG_FUNCTION(this << psi << theta << phi);
  const auto iter = m_antennaArrayList.find(antennaID);
  if (iter != m_antennaArrayList.cend()) {
    Ptr<PhasedAntennaArrayConfig> antennaConfig =
        StaticCast<PhasedAntennaArrayConfig>(iter->second);
    antennaConfig->orientation.psi = DegreesToRadians(psi);
    antennaConfig->orientation.theta = DegreesToRadians(theta);
    antennaConfig->orientation.phi = DegreesToRadians(phi);
  } else {
    NS_ABORT_MSG("Cannot find the specified antenna ID=" << +antennaID);
  }
}

uint8_t Codebook::GetNumberOfAwvs(AntennaId antennaID,
                                  SectorId sectorID) const {
  const auto it = m_antennaArrayList.find(antennaID);
  if (it != m_antennaArrayList.cend()) {
    Ptr<PhasedAntennaArrayConfig> config =
        StaticCast<PhasedAntennaArrayConfig>(it->second);
    const auto sectorIt = config->sectorList.find(sectorID);
    if (sectorIt != config->sectorList.cend()) {
      Ptr<SectorConfig> sectorConfig =
          StaticCast<SectorConfig>(sectorIt->second);
      return sectorConfig->awvList.size();
    } else {
      NS_FATAL_ERROR("Sector [" << +sectorID << "] does not exist");
    }
  } else {
    NS_FATAL_ERROR("Antenna [" << +antennaID << "] does not exist");
  }
}

void Codebook::InitiateBrp(AntennaId antennaID, SectorId sectorID,
                           BeamRefinementType type) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID << type);
  m_activeRfChain->InitiateBrp(antennaID, sectorID, type);
}

void Codebook::GetNextAwv() {
  NS_LOG_FUNCTION(this);
  for (const auto &rfChain : m_activeRfChainList) {
    m_rfChainList[rfChain.first]->GetNextAwv();
  }
}

void Codebook::UseLastTxSector() {
  NS_LOG_FUNCTION(this);
  for (const auto &rfChain : m_activeRfChainList) {
    m_rfChainList[rfChain.first]->UseLastTxSector();
  }
}

void Codebook::UseCustomAwv(BeamRefinementType type) {
  NS_LOG_FUNCTION(this << type);
  for (const auto &rfChain : m_activeRfChainList) {
    m_rfChainList[rfChain.first]->UseCustomAwv(type);
  }
}

bool Codebook::IsCustomAwvUsed() const {
  return m_activeRfChain->IsCustomAwvUsed();
}

uint8_t Codebook::GetActiveTxPatternId() const {
  return m_activeRfChain->GetActiveTxPatternId();
}

uint8_t Codebook::GetActiveRxPatternId() const {
  return m_activeRfChain->GetActiveRxPatternId();
}

bool Codebook::IsQuasiOmniMode() const {
  return m_activeRfChain->IsQuasiOmniMode();
}

Orientation Codebook::GetOrientation(AntennaId antennaId) {
  return m_antennaArrayList[antennaId]->orientation;
}

void Codebook::ActivateRfChain(RfChainId chainID) {
  NS_LOG_FUNCTION(this << +chainID);
  for (auto &rfChain : m_activeRfChainList) {
    m_rfChainList[rfChain.first]->Reset();
  }
  m_activeRfChainList.clear();
  auto it = m_activeRfChainList.find(chainID);
  if (it == m_activeRfChainList.end()) {
    m_activeRfChainList[chainID] = m_rfChainList[chainID]->GetActiveAntennaId();
  } else {
    it->second = m_rfChainList[chainID]->GetActiveAntennaId();
    ;
  }
}

void Codebook::SetActiveRfChainId(RfChainId chainID) {
  NS_LOG_FUNCTION(this << +chainID);
  const auto iter = m_rfChainList.find(chainID);
  if (iter != m_rfChainList.end()) {
    m_activeRfChainId = chainID;
    m_activeRfChain = iter->second;
    ActivateRfChain(chainID);
  } else {
    NS_ABORT_MSG("Cannot find the specified RF Chain ID=" << +chainID);
  }
}

Ptr<PatternConfig> Codebook::GetTxPatternConfig() const {
  return m_activeRfChain->GetTxPatternConfig();
}

Ptr<PatternConfig> Codebook::GetRxPatternConfig() const {
  return m_activeRfChain->GetRxPatternConfig();
}

Ptr<PhasedAntennaArrayConfig> Codebook::GetAntennaArrayConfig() const {
  return m_activeRfChain->GetAntennaArrayConfig();
}

void Codebook::StartSectorRefinement() {
  NS_LOG_FUNCTION(this);
  m_activeRfChain->StartSectorRefinement();
}

void Codebook::StartBeaconTraining() {
  SetReceivingInDirectionalMode();
  m_currentSectorList = &m_rxBeamformingSectors;
  m_beamformingAntenna = m_currentSectorList->begin();
  m_beamformingSectorList = m_beamformingAntenna->second;
  SetActiveRxSectorId(m_beamformingAntenna->first,
                      m_beamformingSectorList.front());
  m_sectorSweepType = ReceiveSectorSweep;
  m_currentSectorIndex = 0;
  m_remainingSectors = CountNumberOfSectors(m_currentSectorList) - 1;
  StartSectorRefinement();
}

uint8_t Codebook::GetRemainingAwvCount() const {
  return m_activeRfChain->GetRemainingAwvCount();
}

} // namespace ns3
