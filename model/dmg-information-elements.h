/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef DMG_INFORMATION_ELEMENTS_H
#define DMG_INFORMATION_ELEMENTS_H

#include "fields-headers.h"

#include "ns3/attribute-helper.h"
#include "ns3/buffer.h"
#include "ns3/mac48-address.h"
#include "ns3/wifi-information-element.h"

#include <stdint.h>

namespace ns3 {

/**
 * This type is used to represent an Information Element ID Extension.
 */
typedef uint8_t WifiInformationElementIdExtension;

typedef std::pair<WifiInformationElementId, WifiInformationElementIdExtension>
    WifiInfoElementId;
typedef std::vector<WifiInfoElementId> WifiInformationElementIdList;

/**
 * Deserialize IE ID, length and Extension ID. The iterator passed in  must be
 * pointing at the Element ID of an information element.
 *
 * \param i an iterator which points to where the IE should be read.
 *
 * \return an iterator
 */
Buffer::Iterator DeserializeExtensionElementId(Buffer::Iterator i,
                                               uint8_t &elementID,
                                               uint8_t &length,
                                               uint8_t &extElementId);

/**
 * Deserialize IE ID and length. The iterator passed in  must be
 * pointing at the Element ID of an information element.
 *
 * \param i an iterator which points to where the IE should be read.
 *
 * \return an iterator
 */
Buffer::Iterator DeserializeElementId(Buffer::Iterator i, uint8_t &elementID,
                                      uint8_t &length);

/**
 * \ingroup wigig
 *
 * The IEEE 802.11 Request Element 8.4.2.13
 */
class RequestElement : public WifiInformationElement {
public:
  RequestElement();

  WifiInformationElementId ElementId() const override;
  /**
   * Add Element ID to the list of request elements.
   * \param id The ID of the requested element.
   */
  void AddRequestElementId(WifiInfoElementId id);
  /**
   * Add list of request elements.
   * \param list A list of the IDs of the requested elements.
   */
  void AddListOfRequestedElementId(WifiInformationElementIdList &list);
  /**
   * \return The list of elements that are requested to be included in the
   * frame. The Requested Element IDs are listed in order of increasing element
   * ID
   */
  WifiInformationElementIdList GetWifiInformationElementIdList() const;
  /**
   * \return The number of requested IEs.
   */
  size_t GetNumberOfRequestedIEs() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  WifiInformationElementIdList m_list;
};

std::ostream &operator<<(std::ostream &os, const RequestElement &element);
std::istream &operator>>(std::istream &is, RequestElement &element);

ATTRIBUTE_HELPER_HEADER(RequestElement);

enum TrafficType {
  TRAFFIC_TYPE_PERIODIC_TRAFFIC_PATTERN = 0,
  TRAFFIC_TYPE_APERIODIC_TRAFFIC_PATTERN = 1
};

enum DataDirection {
  DATA_DIRECTION_UPLINK = 0,
  DATA_DIRECTION_DOWNLINK = 1,
  DATA_DIRECTION_DIRECT_LINK = 2,
  DATA_DIRECTION_BIDIRECTIONAL_LINK = 3
};

enum AccessPolicy {
  ACCESS_POLICY_RESERVED = 0,
  ACCESS_POLICY_CONTENTION_BASED_CHANNEL_ACCESS = 1,
  ACCESS_POLICY_CONTROLLED_CHANNEL_ACCESS = 2,
  ACCESS_POLICY_MIXED_MODE = 3
};

enum TsInfoAckPolicy {
  ACK_POLICY_NORMAL_ACK = 0,
  ACK_POLICY_NO_ACK = 1,
  ACK_POLICY_RESERVED = 2,
  ACK_POLICY_BLOCK_ACK = 3
};

enum AllocationDirection {
  ALLOCATION_DIRECTION_SOURCE = 0,
  ALLOCATION_DIRECTION_DESTINATION = 1
};

/**
 * \ingroup wigig
 *
 * The IEEE 802.11 Traffic Specifications Element (TSPEC) 8.4.2.32
 */
class TspecElement : public WifiInformationElement {
public:
  TspecElement();

  WifiInformationElementId ElementId() const override;
  void Print(std::ostream &os) const override;

  /**
   * The Traffic Type subfield is a single bit and is set to 1 for a periodic
   * traffic pattern (e.g., isochronous TS of MSDUs or A-MSDUs, with constant or
   * variable sizes, that are originated at fixed rate) or set to 0 for an
   * aperiodic, or unspecified, traffic pattern (e.g., asynchronous TS of
   * low-duty cycles). \param type The type of the traffic.
   */
  void SetTrafficType(TrafficType type);
  /**
   * The TSID subfield is 4 bits in length and contains a value that is a TSID.
   * The MSB (bit 4 in TS Info field) of the TSID subfield is always set to 1
   * when the TSPEC element is included within an ADDTS Response frame. \param
   * id
   */
  void SetTsId(uint8_t id);
  /**
   * Set the direction of data carried by the TS.
   * \param direction The direction of data carried by the TS.
   */
  void SetDataDirection(DataDirection direction);
  /**
   * Set the access method to be used for the TS.
   * \param policy The access policy to be used for the TS.
   */
  void SetAccessPolicy(AccessPolicy policy);
  /**
   * Set the Aggregation subfield is valid only when the access method is HCCA
   * or SPCA or when the access method is EDCA and the Schedule subfield is
   * equal to 1. It is set to 1 by a non-AP STA to indicate that an aggregate
   * schedule is required. It is set to 1 by the AP if an aggregate schedule is
   * being provided to the STA. It is set to 0 otherwise. In all other cases,
   * the Aggregation subfield is reserved. \param enable
   */
  void SetAggregation(bool enable);
  /**
   * Set the APSD subfield to indicate that automatic PS delivery is to be used
   * for the traffic associated with the TSPEC and set to 0 otherwise. \param
   * enable
   */
  void SetApsd(bool enable);
  /**
   * The UP indicates the actual value of the UP to be used for the transport of
   * MSDUs or A-MSDUs belonging to this TS when relative prioritization is
   * required. When the TCLAS element is present in the request, the UP subfield
   * in TS Info field of the TSPEC element is reserved. \param priority
   */
  void SetUserPriority(uint8_t priority);
  /**
   * Set TS Info Ack Policy to indicate whether MAC acknowledgments are required
   * for MPDUs or A-MSDUs belonging to this TSID and the form of those
   * acknowledgments. \param policy
   */
  void SetTsInfoAckPolicy(TsInfoAckPolicy policy);
  /**
   * The Schedule subfield is 1 bit in length and specifies the requested type
   * of schedule. The setting of the subfield when the access policy is EDCA is
   * shown in Table 9-142. When the Access Policy subfield is equal to any value
   * other than EDCA, the Schedule subfield is reserved. When the Schedule and
   * APSD subfields are equal to 1, the AP sets the aggregation bit to 1,
   * indicating that an aggregate schedule is being provided to the STA. \param
   * schedule
   */
  void SetSchedule(bool schedule);

  void SetDmgAttributesField(uint16_t infieldfo);
  uint16_t GetDmgAttributesField() const;

  /**
   * The Nominal MSDU Size field is 2 octets long and contains an unsigned
   * integer that specifies the nominal size, in octets, of MSDUs or (where
   * A-MSDU aggregation is employed) A-MSDUs belonging to the TS under this
   * TSPEC. If the Fixed subfield is equal to 1, then the size of the MSDU or
   * A-MSDU is fixed and is indicated by the Size subfield. If the Fixed
   * subfield is equal to 0, then the size of the MSDU or A-MSDU might not be
   * fixed and the Size subfield indicates the nominal MSDU size. If both the
   * Fixed and Size subfields are equal to 0, then the nominal MSDU or A-MSDU
   * size is unspecified. \param size
   */
  void SetNominalMsduSize(uint16_t size, bool fixed);
  /**
   * Set the maximum size, in octets, of MSDUs or A-MSDUs belonging to the TS
   * under this TSPEC. \param size
   */
  void SetMaximumMsduSize(uint16_t size);
  /**
   * Set minimum interval, in microseconds, between the start of two successive
   * SPs. \param interval
   */
  void SetMinimumServiceInterval(uint32_t interval);
  /**
   * SetMaximumServiceInterval
   * \param interval
   */
  void SetMaximumServiceInterval(uint32_t interval);
  void SetInactivityInterval(uint32_t interval);
  void SetSuspensionInterval(uint32_t interval);
  void SetServiceStartTime(uint32_t time);
  void SetMinimumDataRate(uint32_t rate);
  void SetMeanDataRate(uint32_t rate);
  void SetPeakDataRate(uint32_t rate);
  void SetBurstSize(uint32_t size);
  void SetDelayBound(uint32_t bound);
  void SetMinimumPhyRate(uint32_t rate);
  void SetSurplusBandwidthAllowance(uint16_t allowance);
  void SetMediumTime(uint16_t time);

  void SetAllocationId(uint8_t id);
  void SetAllocationDirection(AllocationDirection dircetion);
  void SetAmsduSubframe(uint8_t amsdu);
  void SetReliabilityIndex(uint8_t index);

  TrafficType GetTrafficType() const;
  uint8_t GetTsId() const;
  DataDirection GetDataDirection() const;
  AccessPolicy GetAccessPolicy() const;
  bool GetAggregation() const;
  bool GetApsd() const;
  uint8_t GetUserPriority() const;
  TsInfoAckPolicy GetTsInfoAckPolicy() const;
  bool GetSchedule() const;

  uint16_t GetNominalMsduSize() const;
  bool IsMsduSizeFixed() const;
  uint16_t GetMaximumMsduSize() const;
  uint32_t GetMinimumServiceInterval() const;
  uint32_t GetMaximumServiceInterval() const;
  uint32_t GetInactivityInterval() const;
  uint32_t GetSuspensionInterval() const;
  uint32_t GetServiceStartTime() const;
  uint32_t GetMinimumDataRate() const;
  uint32_t GetMeanDataRate() const;
  uint32_t GetPeakDataRate() const;
  uint32_t GetBurstSize() const;
  uint32_t GetDelayBound() const;
  uint32_t GetMinimumPhyRate() const;
  uint16_t GetSurplusBandwidthAllowance() const;
  uint16_t GetMediumTime() const;

  uint8_t GetAllocationId() const;
  AllocationDirection GetAllocationDirection() const;
  uint8_t GetAmsduSubframe() const;
  uint8_t GetReliabilityIndex() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  uint8_t m_trafficType;
  uint8_t m_tsid;
  uint8_t m_dataDirection;
  uint8_t m_accessPolicy;
  uint8_t m_aggregation;
  uint8_t m_apsd;
  uint8_t m_userPriority;
  uint8_t m_tsInfoAckPolicy;
  uint8_t m_schedule;

  uint16_t m_nominalMsduSize;
  uint16_t m_maximumMsduSize;
  uint32_t m_minimumServiceInterval;
  uint32_t m_maximumServiceInterval;
  uint32_t m_inactivityInterval;
  uint32_t m_suspensionInterval;
  uint32_t m_serviceStartTime;
  uint32_t m_minimumDateRate;
  uint32_t m_meanDateRate;
  uint32_t m_peakDateRate;
  uint32_t m_burstSize;
  uint32_t m_delayBound;
  uint32_t m_minimumPhyRate;
  uint16_t m_surplusBandwidthAllowance;
  uint16_t m_mediumTime;

  uint8_t m_allocationId;
  uint8_t m_allocationDirection;
  uint8_t m_amsduSubframe;
  uint8_t m_reliabilityIndex;
};

std::ostream &operator<<(std::ostream &os, const TspecElement &element);
std::istream &operator>>(std::istream &is, TspecElement &element);

ATTRIBUTE_HELPER_HEADER(TspecElement);

/**
 * \ingroup wigig
 *
 * The IEEE 802.11 Traffic Stream (TS) Delay 8.4.2.34
 */
class TsDelayElement : public WifiInformationElement {
public:
  TsDelayElement();

  WifiInformationElementId ElementId() const override;

  /**
   * Set the amount of delay time, in TUs, a STA should wait before it
   * reinitiates setup of a TS. The Delay field is set to 0 when an AP does not
   * expect to serve any TSPECs for  an indeterminate time and does not know
   * this time a priori. \param type
   */
  void SetDelay(uint32_t delay);
  uint32_t GetDelay() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  uint32_t m_delay;
};

std::ostream &operator<<(std::ostream &os, const TsDelayElement &element);
std::istream &operator>>(std::istream &is, TsDelayElement &element);

ATTRIBUTE_HELPER_HEADER(TsDelayElement);

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad DMG Operation Element 8.4.2.131
 */
class DmgOperationElement : public WifiInformationElement {
public:
  DmgOperationElement();

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  WifiInformationElementId ElementId() const override;
  void Print(std::ostream &os) const override;

  /* DMG Operation Information */
  /**
   * The TDDTI (time division data transfer interval) field is set to 1 if the
   * STA, while operating as a PCP/AP, is capable of providing channel access as
   * defined in 9.33.6 and 10.4 and is set to 0 otherwise. \param tddti
   */
  void SetTddti(bool tddti);
  /**
   * The Pseudo-static Allocations field is set to 1 if the STA, while operating
   * as a PCP/AP, is capable of providing pseudo-static allocations as defined
   * in 9.33.6.4 and is set to 0 otherwise. The Pseudo-static Allocations field
   * is set to 1 only if the TDDTI field in the DMG PCP/AP Capability
   * Information field is set to 1. The Pseudo-static Allocations field is
   * reserved if the TDDTI field in the DMG PCP/AP Capability Information field
   * is set to 0. \param pseudoStatic
   */
  void SetPseudoStaticAllocations(bool pseudoStatic);
  /**
   * The PCP Handover field is set to 1 if the PCP provides PCP Handover as
   * defined in 10.28.2 and is set to 0 ifthe PCP does not provide PCP Handover.
   * \param handover
   */
  void SetPcpHandover(bool handover);

  bool GetTddti() const;
  bool GetPseudoStaticAllocations() const;
  bool GetPcpHandover() const;

  void SetDmgOperationInformation(uint16_t info);
  uint16_t GetDmgOperationInformation() const;

  /* DMG BSS Parameter Configuration */
  void SetPSRequestSuspensionInterval(uint8_t interval);
  void SetMinBhiDuration(uint16_t duration);
  void SetBroadcastSTAInfoDuration(uint8_t duration);
  void SetAssocRespConfirmTime(uint8_t time);
  void SetMinPPDuration(uint8_t duration);
  void SetSPIdleTimeout(uint8_t timeout);
  void SetMaxLostBeacons(uint8_t max);

  uint8_t GetPsRequestSuspensionInterval() const;
  uint16_t GetMinBhiDuration() const;
  uint8_t GetBroadcastSTAInfoDuration() const;
  uint8_t GetAssocRespConfirmTime() const;
  uint8_t GetMinPPDuration() const;
  uint8_t GetSpIdleTimeout() const;
  uint8_t GetMaxLostBeacons() const;

  void SetDmgBssParameterConfiguration(uint64_t config);
  uint64_t GetDmgBssParameterConfiguration() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  /* DMG Operation Information */
  bool m_tddti;
  bool m_pseudo;
  bool m_handover;
  /* DMG BSS Parameter Configuration */
  uint8_t m_psRequestSuspensionInterval;
  uint16_t m_minBhiDuration;
  uint8_t m_broadcastSTAInfoDuration;
  uint8_t m_assocRespConfirmTime;
  uint8_t m_minPPDuration;
  uint8_t m_spIdleTimeout;
  uint8_t m_maxLostBeacons;
};

std::ostream &operator<<(std::ostream &os, const DmgOperationElement &element);
std::istream &operator>>(std::istream &is, DmgOperationElement &element);

ATTRIBUTE_HELPER_HEADER(DmgOperationElement);

enum NumTaps {
  TAPS_1 = 0,
  TAPS_5 = 1,
  TAPS_15 = 2,
  TAPS_63 = 3,
};

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad DMG Beam Refinement Element 8.4.2.132
 */
class BeamRefinementElement : public WifiInformationElement {
public:
  BeamRefinementElement();

  WifiInformationElementId ElementId() const override;
  void Print(std::ostream &os) const override;

  /**
   * \param initiator A value of 1 in the Initiator field indicates that the
   * sender is the beam refinement initiator. Otherwise, this field is set to 0.
   */
  void SetAsBeamRefinementInitiator(bool initiator);
  /**
   * \param response A value of 1 in the TX-train-response field indicates that
   * this packet is the response to a TX training request. Otherwise, this field
   * is set to 0.
   */
  void SetTxTrainResponse(bool response);
  /**
   * \param response A value of 1 in the RX-train-response field indicates that
   * the packet serves as  an acknowledgment for a RX training request.
   * Otherwise, this field is set to 0.
   */
  void SetRxTrainResponse(bool response);
  /**
   * \param value A value of 1 in the TX-TRN-OK field confirms a previous
   * training request received by a STA. Otherwise, this field is set to 0.
   */
  void SetTxTrnOk(bool value);
  /**
   * \param feedback A value of 1 in the TXSS-FBCK-REQ field indicates a request
   * for transmit sector sweep feedback.
   */
  void SetTxssFbckReq(bool feedback);
  /**
   * \param index The BS-FBCK field indicates the index of the TRN-T field that
   * was received with the best quality in the last received BRP-TX PPDU, where
   * the first TRN-T field in the PPDU is defined as having an index equal to 1.
   */
  void SetBsFbck(uint8_t index);
  /**
   * \param The BS-FBCK Antenna ID field specifies the antenna ID corresponding
   * to the sector indicated by the BF-FBCK field.
   */
  void SetBsFbckAntennaId(uint8_t id);

  /* FBCK-REQ field */

  /**
   * \param requested If set to 1, the SNR subfield is requested as part of the
   * channel measurement feedback. Otherwise, set to 0.
   */
  void SetSnrRequested(bool requested);
  /**
   * \param requested If set to 1, the Channel Measurement subfield is requested
   * as part of the channel measurement feedback. Otherwise, set to 0.
   */
  void SetChannelMeasurementRequested(bool requested);
  /**
   * \param number The number of taps in each channel measurement.
   */
  void SetNumberOfTapsRequested(NumTaps number);
  /**
   * \param present If set to 1, the Sector ID Order subfield is requested as
   * part of the channel measurement feedback. Otherwise, set to 0.
   */
  void SetSectorIdOrderRequested(bool present);

  /* FBCK-TYPE field */

  /**
   * \param present Set to 1 to indicate that the SNR subfield is present as
   * part of the channel measurement feedback. Set to 0 otherwise.
   */
  void SetSnrPresent(bool present);
  /**
   * \param present Set to 1 to indicate that the Channel Measurement subfield
   * is present as part of the channel measurement feedback. Set to 0 otherwise.
   */
  void SetChannelMeasurementPresent(bool present);
  /**
   * \param present Set to 1 to indicate that the Tap Delay subfield is present
   * as part of the channel measurement feedback. Set to 0 otherwise.
   */
  void SetTapDelayPresent(bool present);
  /**
   * \param number Number of taps in each channel measurement.
   */
  void SetNumberOfTapsPresent(NumTaps number);
  /**
   * \param number Number of measurements in the SNR subfield and the Channel
   * Measurement subfield.
   */
  void SetNumberOfMeasurements(uint8_t number);
  /**
   * \param present Set to 1 to indicate that the Sector ID Order subfield is
   * present as part of the channel measurement feedback. Set to 0 otherwise.
   */
  void SetSectorIdOrderPresent(bool present);
  /**
   * \param linkType SetLinkType Set to 0 for the initiator link and to 1 for
   * the responder link.
   */
  void SetLinkType(bool linkType);
  /**
   * \param type Set to 0 for the transmitter antenna and to 1 for the receiver
   * antenna.
   */
  void SetAntennaType(bool type);
  /**
   * \param number Indicates the number of beams in the Sector ID Order subfield
   * for the MIDC subphase.
   */
  void SetNumberOfBeams(uint8_t number);

  /**
   * \param mid A value of 1 in the MID Extension field indicates the intention
   * to continue transmitting BRP-RX packets during the MID subphases.
   * Otherwise, this field is set to 0.
   */
  void SetMidExtension(bool mid);
  /**
   * \param mid A value of 1 in the Capability Request field indicates that the
   * transmitter of the frame requests the intended receiver to respond with a
   * BRP frame that includes the BRP Request field. Otherwise, this field is set
   * to 0.
   */
  void SetCapabilityRequest(bool mid);
  /**
   * \param index Sets the BS-FBCK field.
   */
  void SetExtendedBsFbck(uint16_t index);
  /**
   * \param id Sets the BS-FBCK Antenna ID field.
   */
  void SetExtendedBsFbckAntennaId(uint8_t id);
  /**
   * \param number Sets the Number of Measurements field.
   */
  void SetExtendedNumberOfMeasurements(uint16_t number);

  bool IsBeamRefinementInitiator() const;
  bool IsTxTrainResponse() const;
  bool IsRxTrainResponse() const;
  bool IsTxTrnOk() const;
  bool IsTxssFbckReq() const;
  uint8_t GetBsFbck() const;
  uint8_t GetBsFbckAntennaId() const;

  /* FBCK-REQ field */
  bool IsSnrRequested() const;
  bool IsChannelMeasurementRequested() const;
  NumTaps GetNumberOfTapsRequested() const;
  bool IsSectorIdOrderRequested() const;

  /* FBCK-TYPE field */
  bool IsSnrPresent() const;
  bool IsChannelMeasurementPresent() const;
  bool IsTapDelayPresent() const;
  NumTaps GetNumberOfTapsPresent() const;
  uint8_t GetNumberOfMeasurements() const;
  bool IsSectorIdOrderPresent() const;
  bool GetLinkType() const;
  bool GetAntennaType() const;
  uint8_t GetNumberOfBeams() const;

  bool IsMidExtension() const;
  bool IsCapabilityRequest() const;

  uint16_t GetExtendedBsFbck() const;
  uint8_t GetExtendedBsFbckAntennaId() const;
  uint16_t GetExtendedNumberOfMeasurements() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  bool m_initiator;
  bool m_txTrainResponse;
  bool m_rxTrainResponse;
  bool m_txTrnOk;
  bool m_txssFbckReq;
  uint16_t m_bsFbck;
  uint8_t m_bsFbckAntennaId;
  /* FBCK-REQ field format */
  bool m_snrRequested;
  bool m_channelMeasurementRequested;
  NumTaps m_numberOfTapsRequested;
  bool m_sectorIdOrderRequested;
  /* FBCK-TYPE field */
  bool m_snrPresent;
  bool m_channelMeasurementPresent;
  bool m_tapDelayPresent;
  NumTaps m_numberOfTapsPresent;
  uint16_t m_numberOfMeasurements;
  bool m_sectorIdOrderPresent;
  bool m_linkType;
  bool m_antennaType;
  uint8_t m_numberOfBeams;

  bool m_midExtension;
  bool m_capabilityRequest;
};

std::ostream &operator<<(std::ostream &os,
                         const BeamRefinementElement &element);
std::istream &operator>>(std::istream &is, BeamRefinementElement &element);

ATTRIBUTE_HELPER_HEADER(BeamRefinementElement);

/******************************************
 *    Allocation Field Format (8-401aa)
 *******************************************/

/**
 * \ingroup wigig
 * Implement the Allocation Field Format for the Extended Schedule Element.
 */
class AllocationField {
public:
  AllocationField();

  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * The Allocation ID field, when set to a nonzero value, identifies an airtime
   * allocation from Source AID to Destination AID. Except for CBAP allocations
   * with broadcast Source AID and broadcast Destination AID, the tuple (Source
   * AID, Destination AID, Allocation ID) uniquely identifies the allocation.
   * For CBAP allocations with broadcast Source AID and broadcast Destination
   * AID, the Allocation ID is zero. \param id The ID of the current allocation.
   */
  void SetAllocationId(AllocationId id);
  /**
   * The AllocationType field defines the channel access mechanism during the
   * allocation. \param type either SP allocation or CBAP allocation.
   */
  void SetAllocationType(AllocationType type);
  /**
   * The Pseudo-static field is set to 1 to indicate that this allocation is
   * pseudo-static and set to 0 otherwise. \param value
   */
  void SetAsPseudoStatic(bool value);
  /**
   * For an SP allocation, the Truncatable field is set to 1 to indicate that
   * the source DMG STA and destination DMG STA can request SP truncation and
   * set to 0 otherwise. For a CBAP allocation, the Truncatable field is
   * reserved. \param value
   */
  void SetAsTruncatable(bool value);
  /**
   * For an SP allocation, the Extendable field is set to 1 to indicate that the
   * source DMG STA and destination DMG STA can request SP extension and set to
   * 0 otherwise. For a CBAP allocation, the Extendable field is reserved.
   * \param value
   */
  void SetAsExtendable(bool value);
  /**
   * The PCP Active field is set to 1 if the PCP is available to receive
   * transmissions during the CBAP or SP allocation and set to 0 otherwise. The
   * PCP Active field is set to 1 if at least one of the Truncatable or
   * Extendable fields are set to 1, or when transmitted by an AP. \param value
   */
  void SetPcpActive(bool value);
  /**
   * The LP SC Used subfield is set to 1 to indicate that the low-power SC PHY
   * is used in this SP. Otherwise, it is set to 0. \param value True if LP SC
   * is used in this SP otherwise is false.
   */
  void SetLpScUsed(bool value);

  AllocationId GetAllocationId() const;
  AllocationType GetAllocationType() const;
  bool IsPseudoStatic() const;
  bool IsTruncatable() const;
  bool IsExtendable() const;
  bool IsPcpActive() const;
  bool IsLpScUsed() const;

  void SetAllocationControl(uint16_t ctrl);
  uint16_t GetAllocationControl() const;

  void SetBfControl(BfControlField &field);
  /**
   * The Source AID field is set to the AID of the STA that initiates channel
   * access during the SP or CBAP allocation or, in the case of a CBAP
   * allocation, can also be set to the broadcast AID if all STAs are allowed to
   * initiate transmissions during the CBAP allocation. \param aid
   */
  void SetSourceAid(uint8_t aid);
  /**
   * The Destination AID field indicates the AID of a STA that is expected to
   * communicate with the source DMG STA during the allocation or broadcast AID
   * if all STAs are expected to communicate with the source DMG STA during the
   * allocation. The broadcast AID asserted in the Source AID and the
   * Destination AID fields for an SP allocation indicates that during the SP a
   * non-PCP/non-AP STA does not transmit unless it receives a Poll or Grant
   * frame from the PCP/AP. \param aid
   */
  void SetDestinationAid(uint8_t aid);
  /**
   * The Allocation Start field contains the lower 4 octets of the TSF at the
   * time the SP or CBAP starts. The Allocation Start field can be specified at
   * a future beacon interval when the pseudo-static field is set to 1. \param
   * start
   */
  void SetAllocationStart(uint32_t start);
  /**
   * The Allocation Block Duration field indicates the duration, in
   * microseconds, of a contiguous time block for which the SP or CBAP
   * allocation is made and cannot cross beacon interval boundaries. Possible
   * values range from 1 to 32 767 for an SP allocation and 1 to 65535 for a
   * CBAP allocation. \param start
   */
  void SetAllocationBlockDuration(uint16_t duration);
  /**
   * The Number of Blocks field contains the number of time blocks making up the
   * allocation. \param number
   */
  void SetNumberOfBlocks(uint8_t number);
  /**
   * The Allocation Block Period field contains the time, in microseconds,
   * between the start of two consecutive time blocks belonging to the same
   * allocation. The Allocation Block Period field is reserved when the Number
   * of Blocks field is set to 1. \param period
   */
  void SetAllocationBlockPeriod(uint16_t period);

  BfControlField GetBfControl() const;
  uint8_t GetSourceAid() const;
  uint8_t GetDestinationAid() const;
  uint32_t GetAllocationStart() const;
  uint16_t GetAllocationBlockDuration() const;
  uint8_t GetNumberOfBlocks() const;
  uint16_t GetAllocationBlockPeriod() const;

  /* Private Information */
  void SetAllocationAnnounced();
  bool IsAllocationAnnounced() const;

private:
  /* Allocation Control Subfield*/
  AllocationId m_allocationId; //!< Allocation ID.
  uint8_t m_allocationType;    //!< Allocation Type.
  bool m_pseudoStatic;         //!< Pseudostatic Allocation.
  bool m_truncatable;          //!< Truncatable service period.
  bool m_extendable;           //!< Extendable service period.
  bool m_pcpActive;            //!< PCP Active.
  bool m_lpScUsed;             //!< Low Power SC Operation.

  BfControlField m_bfControl;         //!< BF Control.
  uint8_t m_SourceAid;                //!< Source STA AID.
  uint8_t m_destinationAid;           //!< Destination STA AID.
  uint32_t m_allocationStart;         //!< Allocation Start.
  uint16_t m_allocationBlockDuration; //!< Allocation Block Duration.
  uint8_t m_numberOfBlocks;           //!< Number of Blocks.
  uint16_t m_allocationBlockPeriod;   //!< Allocation Block Period.

  bool m_allocationAnnounced; //!< Flag to indicate if allocation has been
                              //!< announced in BTI or ATI.
};

typedef std::vector<AllocationField> AllocationFieldList;

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad Extended Schedule Element 8.4.2.134
 *
 * Because the length parameter supports only 255 octets of payload in an
 * element, the PCP/AP can split the Allocation fields into more than one
 * Extended Schedule element entry in the same DMG Beacon or Announce frame.
 * Despite this splitting, the set of Extended Schedule element entries conveyed
 * within a DMG Beacon and Announce frame is considered to be a single schedule
 * for the beacon interval, and in this standard referred to simply as Extended
 * Schedule element unless otherwise noted. The Allocation fields are ordered by
 * increasing allocation start time with allocations beginning at the same time
 * arbitrarily ordered.
 */
class ExtendedScheduleElement : public WifiInformationElement {
public:
  ExtendedScheduleElement();

  WifiInformationElementId ElementId() const override;

  void AddAllocationField(AllocationField &field);
  void SetAllocationFieldList(const AllocationFieldList &list);

  AllocationFieldList GetAllocationFieldList() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;
  AllocationFieldList m_list;
};

std::ostream &operator<<(std::ostream &os,
                         const ExtendedScheduleElement &element);
std::istream &operator>>(std::istream &is, ExtendedScheduleElement &element);

ATTRIBUTE_HELPER_HEADER(ExtendedScheduleElement);

/**
 * \ingroup wigig
 * Implement the header for Relay Capable STA Info Field.
 */
class StaInfoField {
public:
  StaInfoField();

  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the AID of the STA for which availability is indicated.
   * \param aid
   */
  void SetAid(uint8_t aid);
  /**
   * Set CBAP field to 1 to indicate that the STA is available to receive
   * transmissions during CBAPs or not. \param value
   */
  void SetCbap(bool value);
  /**
   * Set Polling Phase to 1 to indicate that the STA is available during PPs and
   * is set to 0 otherwise. \param value
   */
  void SetPollingPhase(bool value);

  uint8_t GetAid() const;
  bool GetCbap() const;
  bool GetPollingPhase() const;

private:
  uint8_t m_aid;
  bool m_cbap;
  bool m_pp;
};

typedef std::vector<StaInfoField> StaInfoFieldList;

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad Extended Schedule Element 8.4.2.135
 *
 * The STA Availability element is used by a non-PCP/non-AP STA to inform a
 * PCP/AP about the STA availability during the subsequent CBAPs (9.33.5) and to
 * indicate participation in the Dynamic allocation of service periods (9.33.7).
 * The PCP/AP uses the STA Availability element to inform the non-PCP/non-AP
 * STAs of other STAs availability during subsequent CBAPs and participation of
 * other STAs in the Dynamic allocation of service periods.
 */
class StaAvailabilityElement : public WifiInformationElement {
public:
  StaAvailabilityElement();

  WifiInformationElementId ElementId() const override;

  /**
   * Add STA Info Field.
   * \param field The STA Information Field.
   */
  void AddStaInfo(StaInfoField &field);
  /**
   * Set the STA Info list to be sent by the PCP/AP.
   * \param list The STA Info list.
   */
  void SetStaInfoList(const StaInfoFieldList &list);
  /**
   * \return The list of STA Info sent by the PCP/AP.
   */
  StaInfoFieldList GetStaInfoList() const;
  /**
   * This function returns the first STA Info Field in the list (Used by the
   * PCP/AP to obtain information about the STA). \return
   */
  StaInfoField GetStaInfoField() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  StaInfoFieldList m_list;
};

std::ostream &operator<<(std::ostream &os,
                         const StaAvailabilityElement &element);
std::istream &operator>>(std::istream &is, StaAvailabilityElement &element);

ATTRIBUTE_HELPER_HEADER(StaAvailabilityElement);

/*********************************************
 *  DMG Allocation Info Field Format (8-401af)
 **********************************************/

enum AllocationFormat { ISOCHRONOUS = 0, ASYNCHRONOUS = 1 };

/**
 * \ingroup wigig
 * Implement the header for DMG Allocation Info Field.
 */
class DmgAllocationInfo {
public:
  DmgAllocationInfo();

  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * The Allocation ID field identifies an allocation if set to a nonzero value,
   * and is used as follows: — The STA that transmits an ADDTS Request
   * containing the DMG TSPEC element sets the Allocation ID to a nonzero value
   * to create a new allocation or modify an existing allocation. — The STA that
   * transmits an ADDTS Response containing the DMG TSPEC element sets
   * theAllocation ID to a nonzero value to identify a created or modified
   * allocation or sets it to zero if creating or modifying the allocation
   * failed. \param id The unique ID of the allocation.
   */
  void SetAllocationId(AllocationId id);
  /**
   * The AllocationType field defines the channel access mechanism used during
   * the allocation. \param type
   */
  void SetAllocationType(AllocationType type);
  /**
   * Set the allocation format.
   * \param format Either Isochronous allocation or Asynchronous allocation.
   */
  void SetAllocationFormat(AllocationFormat format);
  /**
   * The Pseudo-static field is set to 1 for a pseudo-static allocation and set
   * to 0 otherwise. \param value
   */
  void SetAsPseudoStatic(bool value);
  void SetAsTruncatable(bool value);
  void SetAsExtendable(bool value);
  void SetLpScUsed(bool value);
  /**
   * The UP field indicates the lowest priority UP to be used for possible
   * transport of MSDUs belonging to TCs with the same source and destination of
   * the allocation. \param value
   */
  void SetUp(uint8_t value);
  void SetDestinationAid(uint8_t aid);

  AllocationId GetAllocationId() const;
  AllocationType GetAllocationType() const;
  AllocationFormat GetAllocationFormat() const;
  bool IsPseudoStatic() const;
  bool IsTruncatable() const;
  bool IsExtendable() const;
  bool IsLpScUsed() const;
  uint8_t GetUp() const;
  uint8_t GetDestinationAid() const;

private:
  AllocationId m_allocationId; //!< Allocation ID.
  uint8_t m_allocationType;    //!< Allocation Type.
  uint8_t m_allocationFormat;  //!< Allocation Format.
  bool m_pseudoStatic;         //!< Pseudostatic Allocation.
  bool m_truncatable;          //!< Truncatable service period.
  bool m_extendable;           //!< Extendable service period.
  bool m_lpScUsed;             //!< Low Power SC Operation.
  uint8_t m_up;                //!< User Priority.
  uint8_t m_destAid;           //!< Destination Association Identifier.
};

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad DMG TSPEC element 8.4.2.136
 *
 * The DMG TSPEC element is present in the ADDTS Request frame sent by a
 * non-PCP/non-AP DMG STA and contains the set of parameters needed to create or
 * modify an airtime allocation. The DMG TSPEC element is also present in the
 * ADDTS Response frame sent by a DMG PCP/AP and reflects the parameters,
 * possibly modified, by which the allocation was created.
 */
class DmgTspecElement : public WifiInformationElement {
public:
  DmgTspecElement();

  WifiInformationElementId ElementId() const override;

  void SetDmgAllocationInfo(DmgAllocationInfo &info);
  void SetBfControl(BfControlField &ctrl);
  /**
   * Set the allocation period as a fraction or multiple of the beacon interval
   * (BI). \param period The allocation period check table (Table 8-183k) in the
   * 802.11ad standard. \param multiple Flag to indicate if the allocation
   * period is multiple or fraction of BI.
   */
  void SetAllocationPeriod(uint16_t period, bool multiple);
  void SetMinimumAllocation(uint16_t min);
  void SetMaximumAllocation(uint16_t max);
  void SetMinimumDuration(uint16_t duration);

  DmgAllocationInfo GetDmgAllocationInfo() const;
  BfControlField GetBfControl() const;
  uint16_t GetAllocationPeriod() const;
  bool IsAllocationPeriodMultipleBi() const;
  uint16_t GetMinimumAllocation() const;
  uint16_t GetMaximumAllocation() const;
  uint16_t GetMinimumDuration() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  DmgAllocationInfo m_dmgAllocationInfo;
  BfControlField m_bfControlField;
  uint16_t m_allocationPeriod;
  uint16_t m_minAllocation;
  uint16_t m_maxAllocation;
  uint16_t m_minDuration;
};

std::ostream &operator<<(std::ostream &os, const DmgTspecElement &element);
std::istream &operator>>(std::istream &is, DmgTspecElement &element);

ATTRIBUTE_HELPER_HEADER(DmgTspecElement);

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad Next DMG ATI Element 8.4.2.137
 */
class NextDmgAti : public WifiInformationElement {
public:
  NextDmgAti();

  WifiInformationElementId ElementId() const override;
  void Print(std::ostream &os) const override;

  void SetStartTime(uint32_t time);
  void SetAtiDuration(uint16_t duration);

  uint32_t GetStartTime() const;
  uint16_t GetAtiDuration() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  uint32_t m_startTime;
  uint16_t m_atiDuration;
};

std::ostream &operator<<(std::ostream &os, const NextDmgAti &element);
std::istream &operator>>(std::istream &is, NextDmgAti &element);

ATTRIBUTE_HELPER_HEADER(NextDmgAti);

enum RelayDuplexMode {
  RELAY_FD_AF = 1, /* Full Duplex, Amplify and Forward */
  RELAY_HD_DF = 2, /* Half Duplex, Decode and Forward */
  RELAY_BOTH = 3
};

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad Relay Capabilities Information Element 8.4.2.150
 *
 * A STA that intends to participate in relay operation (10.35) advertises its
 * capabilities through the Relay Capabilities element.
 *
 */
class RelayCapabilitiesInfo {
public:
  RelayCapabilitiesInfo();

  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * The Relay Supportability field indicates whether the STA is capable of
   * relaying by transmitting and receiving frames between a pair of other STAs.
   * A STA capable of relaying is named “relay STA.” This field is set to 1 if
   * the STA supports relaying; otherwise, it is set to 0. \param value
   */
  void SetRelaySupportability(bool value);
  /**
   * The Relay Usability field indicates whether the STA is capable of using
   * frame-relaying through a relay STA. It is set to 1 if the STA supports
   * transmission and reception of frames through a relay STA; otherwise, it set
   * to 0. \param value
   */
  void SetRelayUsability(bool value);
  /**
   * The Relay Permission field indicates whether the PCP/AP allows relay
   * operation (10.35) to be used within the PCP/AP’s BSS. It is set to 0 if
   * relay operation is not allowed in the BSS; otherwise, it is set to 1. This
   * field is reserved when transmitted by a non-PCP/non-AP STA. \param value
   */
  void SetRelayPermission(bool value);
  /**
   * The A/C Power field indicates whether the STA is capable of obtaining A/C
   * power. It is set to 1 if the STA is capable of being supplied by A/C power;
   * otherwise, it is set to 0. \param value
   */
  void SetAcPower(bool value);
  /**
   * The Relay Preference field indicates whether the STA prefers to become RDS
   * rather than REDS. It is set to 1 if a STA prefers to be RDS; otherwise, it
   * is set to 0. \param value
   */
  void SetRelayPreference(bool value);
  /**
   * The Duplex field indicates whether the STA is capable of
   * full-duplex/amplify-and-forward (FD-AF) or half- duplex/decode-and-forward
   * (HD-DF). It is set to 1 if the STA is not capable of HD-DF but is capable
   * of only FD-AF. It is set to 2 if the STA is capable of HD-DF but is not
   * capable of FD-AF. It is set to 3 if the STA is capable of both HD-DF and
   * FD-AF. The value 0 is reserved. \param value
   */
  void SetDuplex(RelayDuplexMode duplex);
  /**
   * The Cooperation field indicates whether a STA is capable of supporting Link
   * cooperating. It is set to 1 if the\ STA supports both Link cooperating type
   * and Link switching type. It is set to 0 if a STA supports only Link
   * switching or if the Duplex field is set to 1. \param value
   */
  void SetCooperation(bool value);

  bool GetRelaySupportability() const;
  bool GetRelayUsability() const;
  bool GetRelayPermission() const;
  bool GetAcPower() const;
  bool GetRelayPreference() const;
  RelayDuplexMode GetDuplex() const;
  bool GetCooperation() const;

private:
  bool m_supportability;
  bool m_usability;
  bool m_permission;
  bool m_acPower;
  bool m_preference;
  uint8_t m_duplex;
  bool m_cooperation;
};

/**
 * \ingroup wigig
 *
 * The IEEE 802.11ad Relay Capabilities Information Element 8.4.2.150
 *
 * A STA that intends to participate in relay operation (10.35) advertises its
 * capabilities through the Relay Capabilities element.
 *
 */
class RelayCapabilitiesElement : public WifiInformationElement {
public:
  RelayCapabilitiesElement();

  WifiInformationElementId ElementId() const override;

  void SetRelayCapabilitiesInfo(RelayCapabilitiesInfo &info);
  RelayCapabilitiesInfo GetRelayCapabilitiesInfo() const;

private:
  uint16_t GetInformationFieldSize() const override;
  void SerializeInformationField(Buffer::Iterator start) const override;
  uint16_t DeserializeInformationField(Buffer::Iterator start,
                                       uint16_t length) override;

  RelayCapabilitiesInfo m_info;
};

std::ostream &operator<<(std::ostream &os,
                         const RelayCapabilitiesElement &element);
std::istream &operator>>(std::istream &is, RelayCapabilitiesElement &element);

ATTRIBUTE_HELPER_HEADER(RelayCapabilitiesElement);

} // namespace ns3

#endif /* DMG_INFORMATION_ELEMENTS_H */
