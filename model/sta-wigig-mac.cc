/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "sta-wigig-mac.h"

#include "ext-headers.h"
#include "mac-low.h"
#include "wigig-channel-access-manager.h"
#include "wigig-mac-header.h"
#include "wigig-mac-queue.h"
#include "wigig-mgt-headers.h"
#include "wigig-msdu-aggregator.h"
#include "wigig-phy.h"

#include "ns3/boolean.h"
#include "ns3/dmg-capabilities.h"
#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/mgt-headers.h"
#include "ns3/pointer.h"
#include "ns3/simulator.h"
#include "ns3/snr-tag.h"
#include "ns3/status-code.h"
#include "ns3/string.h"
#include "ns3/trace-source-accessor.h"

#include <cmath>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("StaWigigMac");

NS_OBJECT_ENSURE_REGISTERED(StaWigigMac);

TypeId StaWigigMac::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::StaWigigMac")
          .SetParent<WigigMac>()
          .AddConstructor<StaWigigMac>()
          /* Association State Machine Attributes  */
          .AddAttribute("ActiveScanning",
                        "Flag to indicate whether we scan the network to find "
                        "the best DMG "
                        "PCP/AP to associate"
                        "with or we use a static SSID.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&StaWigigMac::m_activeScanning),
                        MakeBooleanChecker())
          .AddAttribute("ProbeRequestTimeout",
                        "The duration to actively probe the channel.",
                        TimeValue(Seconds(0.05)),
                        MakeTimeAccessor(&StaWigigMac::m_probeRequestTimeout),
                        MakeTimeChecker())
          .AddAttribute("WaitBeaconTimeout",
                        "The duration to dwell on a channel while passively "
                        "scanning for beacon",
                        TimeValue(MilliSeconds(120)),
                        MakeTimeAccessor(&StaWigigMac::m_waitBeaconTimeout),
                        MakeTimeChecker())
          .AddAttribute(
              "AssocRequestTimeout",
              "The interval between two consecutive assoc request attempts.",
              TimeValue(Seconds(0.5)),
              MakeTimeAccessor(&StaWigigMac::m_assocRequestTimeout),
              MakeTimeChecker())
          .AddAttribute(
              "MaxMissedBeacons",
              "Number of beacons which much be consecutively missed before "
              "we attempt to restart association.",
              UintegerValue(10),
              MakeUintegerAccessor(&StaWigigMac::m_maxMissedBeacons),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute("ActiveProbing",
                        "If true, we send probe requests. If false, we don't."
                        "NOTE: if more than one STA in your simulation is "
                        "using active probing, "
                        "you should enable it at a different simulation time "
                        "for each STA, "
                        "otherwise all the STAs will start sending probes at "
                        "the same time resulting in "
                        "collisions."
                        "See bug 1060 for more info.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&StaWigigMac::SetActiveProbing,
                                            &StaWigigMac::GetActiveProbing),
                        MakeBooleanChecker())
          /* A-BFT Attributes */
          .AddAttribute("ImmediateAbft",
                        "If true, we start a responder TXSS in the following "
                        "A-BFT when we "
                        "receive a DMG Beacon frame"
                        " in the BTI with a fragmented initiator TXSS, "
                        "otherwise we scan for  "
                        "the number of beacon"
                        " intervals indicated in a received TXSS Span field in "
                        "order to cover a "
                        "complete initiator"
                        " TXSS and find a suitable TX sector from the PCP/AP.",
                        BooleanValue(true),
                        MakeBooleanAccessor(&StaWigigMac::m_immediateAbft),
                        MakeBooleanChecker())
          .AddAttribute("RSSRetryLimit", "Responder Sector Sweep Retry Limit.",
                        UintegerValue(dot11RSSRetryLimit),
                        MakeUintegerAccessor(&StaWigigMac::m_rssAttemptsLimit),
                        MakeUintegerChecker<uint8_t>(1, 32))
          .AddAttribute("RSSBackoff",
                        "Maximum Responder Sector Sweep Backoff value.",
                        UintegerValue(dot11RSSBackoff),
                        MakeUintegerAccessor(&StaWigigMac::m_rssBackoffLimit),
                        MakeUintegerChecker<uint8_t>(1, 32))
          /* DMG Relay Capabilities */
          .AddAttribute("RDSDuplexMode", "0 = HD-DF, 1 = FD-AF.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&StaWigigMac::m_rdsDuplexMode),
                        MakeBooleanChecker())
          .AddAttribute(
              "RDSDataSensingTime",
              "In MicroSeconds. By default, it is set to SIFS plus SBIFS.",
              UintegerValue(4),
              MakeUintegerAccessor(&StaWigigMac::m_relayDataSensingTime),
              MakeUintegerChecker<uint8_t>(1,
                                           std::numeric_limits<uint8_t>::max()))
          /* DMG Capabilities */
          .AddAttribute("SupportSPSH",
                        "Whether the DMG STA supports Spartial Sharing and "
                        "Interference Mitigation (SPSH)",
                        BooleanValue(false),
                        MakeBooleanAccessor(&StaWigigMac::m_supportSpsh),
                        MakeBooleanChecker())
          /* Dynamic Allocation of Service Period */
          .AddAttribute(
              "StaAvailabilityElement",
              "Whether STA availability element is announced in Association "
              "Request",
              BooleanValue(false),
              MakeBooleanAccessor(&StaWigigMac::m_staAvailabilityElement),
              MakeBooleanChecker())
          .AddAttribute(
              "PollingPhase",
              "The PollingPhase is set to 1 to indicate that the STA is "
              "available during PPs otherwise it is set to 0",
              BooleanValue(false),
              MakeBooleanAccessor(&StaWigigMac::m_pollingPhase),
              MakeBooleanChecker())
          /* Add Scanning Capability to StaWigigMac */
          .AddTraceSource(
              "BeaconArrival",
              "Time that beacons arrive from my AP while associated",
              MakeTraceSourceAccessor(&StaWigigMac::m_beaconArrival),
              "ns3::Time::TracedValueCallback")
          /* Association Information */
          .AddTraceSource("Assoc", "Associated with an access point.",
                          MakeTraceSourceAccessor(&StaWigigMac::m_assocLogger),
                          "ns3::WigigMac::AssociationTracedCallback")
          .AddTraceSource(
              "DeAssoc", "Associated with the access point is lost.",
              MakeTraceSourceAccessor(&StaWigigMac::m_deAssocLogger),
              "ns3::Mac48Address::TracedCallback")
          /* DMG BSS peer and service discovery */
          .AddTraceSource(
              "InformationResponseReceived",
              "Received information response regarding specific station.",
              MakeTraceSourceAccessor(&StaWigigMac::m_informationReceived),
              "ns3::Mac48Address::TracedCallback")
          /* Relay Procedure Related Traces */
          .AddTraceSource(
              "ChannelReportReceived",
              "The DMG STA has received a channel report.",
              MakeTraceSourceAccessor(&StaWigigMac::m_channelReportReceived),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "RdsDiscoveryCompleted",
              "The RDS Discovery procedure is completed",
              MakeTraceSourceAccessor(
                  &StaWigigMac::m_rdsDiscoveryCompletedCallback),
              "ns3::Mac48Address::RdsDiscoveryCompletedTracedCallback")
          .AddTraceSource(
              "TransmissionLinkChanged",
              "The current transmission link has been changed.",
              MakeTraceSourceAccessor(&StaWigigMac::m_transmissionLinkChanged),
              "ns3::StaWigigMac::TransmissionLinkChangedTracedCallback");
  return tid;
}

StaWigigMac::StaWigigMac()
    : m_state(StaWifiMac::UNASSOCIATED), m_aid(0), m_waitBeaconEvent(),
      m_probeRequestEvent(), m_assocRequestEvent(), m_beaconWatchdog(),
      m_beaconWatchdogEnd(Seconds(0.0)), m_moreData(false), m_abftEvent(),
      m_TXSSSpan(0), m_remainingBis(0), m_completedBi(false),
      m_nextDti(Seconds(0.0)), m_dtiStartEvent(), m_receivedDmgBeacon(false),
      m_remainingSlotsPerAbft(0), m_currentSlotIndex(0), m_selectedSlotIndex(0),
      m_failedRssAttemptsCounter(0), m_rssBackoffRemaining(0),
      m_relayMode(false), m_linkChangeInterval(), m_firstPeriod(),
      m_secondPeriod(), m_relayDataExchanged(false), m_periodProtected(false),
      m_relayLinkMap(), m_relayLinkInfo(),
      m_abftState(WAIT_BEAMFORMING_TRAINING) {
  NS_LOG_FUNCTION(this);

  /* Set missed ACK/BlockACK callback */
  for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->SetMissedAckCallback(
        MakeCallback(&StaWigigMac::MissedAck, this));
  }

  /* Let the lower layers know that we are acting as a non-AP DMG STA in an
   * infrastructure BSS. */
  SetTypeOfStation(STA);
}

StaWigigMac::~StaWigigMac() { NS_LOG_FUNCTION(this); }

void StaWigigMac::DoInitialize() {
  NS_LOG_FUNCTION(this);

  /** Initialize A_BFT Variables **/
  m_abftSlot = CreateObject<UniformRandomVariable>();
  m_rssBackoffVariable = CreateObject<UniformRandomVariable>();
  m_rssBackoffVariable->SetAttribute("Min", DoubleValue(0));
  m_rssBackoffVariable->SetAttribute("Max", DoubleValue(m_rssBackoffLimit));
  m_abftState = WAIT_BEAMFORMING_TRAINING;

  /* Initialize upper DMG Wifi MAC */
  WigigMac::DoInitialize();

  /* At the beginning of the BTI period, a DMG STA should stay in Quasi-Omni
   * receiving mode */
  m_codebook->StartReceivingInQuasiOmniMode();
  m_phy->SetIsAp(false);

  /* Check if we need to scan the network */
  if (m_activeScanning) {
    StartScanning();
  }
}

void StaWigigMac::ResumePendingTxss() {
  NS_LOG_FUNCTION(this);
  if (m_receivedDmgBeacon) {
    m_dmgSlsTxop->ResumeTXSS();
  } else {
    NS_LOG_INFO(
        "Cannot resume TXSS because we did not receive DMG Beacon in this BI");
  }
}

void StaWigigMac::PerformTxssTxop(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);
  if (m_receivedDmgBeacon) {
    m_dmgSlsTxop->InitiateTxopSectorSweep(peerAddress);
  } else {
    m_dmgSlsTxop->AppendSlsRequest(peerAddress);
  }
}

void StaWigigMac::SetActiveProbing(bool enable) {
  NS_LOG_FUNCTION(this << enable);
  m_activeProbing = enable;
  if (m_state == StaWifiMac::SCANNING) {
    NS_LOG_DEBUG("DMG STA is still scanning, reset scanning process");
    StartScanning();
  }
}

bool StaWigigMac::GetActiveProbing() const { return m_activeProbing; }

void StaWigigMac::SendProbeRequest() {
  NS_LOG_FUNCTION(this);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_PROBE_REQUEST);
  hdr.SetAddr1(Mac48Address::GetBroadcast());
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(Mac48Address::GetBroadcast());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  Ptr<Packet> packet = Create<Packet>();

  WigigMgtProbeRequestHeader probe;

  // WifiMac::AddInformationElement (packet, GetSsid ());
  probe.Get<Ssid>() = GetSsid();

  /* DMG Capabilities Information Element */
  // WifiMac::AddInformationElement (packet, GetDmgCapabilities ());
  probe.Get<DmgCapabilities>() = GetDmgCapabilities();

  // The standard is not clear on the correct queue for management
  // frames if we are a QoS AP. The approach taken here is to always
  // use the non-QoS for these regardless of whether we have a QoS
  // association or not.

  packet->AddHeader(probe);
  m_txop->Queue(packet, hdr);
}

void StaWigigMac::SendAssociationRequest(bool isReassoc) {
  NS_LOG_FUNCTION(this << GetBssid() << isReassoc);
  WigigMacHeader hdr;
  hdr.SetType(isReassoc ? WIFI_MAC_MGT_REASSOCIATION_REQUEST
                        : WIFI_MAC_MGT_ASSOCIATION_REQUEST);
  hdr.SetAddr1(GetBssid());
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  Ptr<Packet> packet = Create<Packet>();
  if (!isReassoc) {
    WigigMgtAssocRequestHeader assoc;
    assoc.Get<Ssid>() = GetSsid();
    // WifiMac::AddInformationElement (packet, GetSsid ());

    /* DMG Capabilities Information Element */
    // WifiMac::AddInformationElement (packet, GetDmgCapabilities ());
    assoc.Get<DmgCapabilities>() = GetDmgCapabilities();

    /* Add Relay Capability Element */
    if (m_redsActivated || m_rdsActivated) {
      // WifiMac::AddInformationElement (packet, GetRelayCapabilitiesElement
      // ());
      assoc.Get<RelayCapabilitiesElement>() = GetRelayCapabilitiesElement();
    }

    if (m_staAvailabilityElement) {
      // WifiMac::AddInformationElement (packet, GetStaAvailabilityElement ());
      assoc.Get<StaAvailabilityElement>() = GetStaAvailabilityElement();
    }

    packet->AddHeader(assoc);
  } else {
    WigigMgtReassocRequestHeader reassoc;
    reassoc.SetCurrentApAddress(GetBssid());
    reassoc.SetListenInterval(0);

    // WifiMac::AddInformationElement (packet, GetSsid ());
    reassoc.Get<Ssid>() = GetSsid();
    /* DMG Capabilities Information Element */
    // WifiMac::AddInformationElement (packet, GetDmgCapabilities ());
    reassoc.Get<DmgCapabilities>() = GetDmgCapabilities();
    /* Add Relay Capability Element */
    if (m_redsActivated || m_rdsActivated) {
      // WifiMac::AddInformationElement (packet, GetRelayCapabilitiesElement
      // ());
      reassoc.Get<RelayCapabilitiesElement>() = GetRelayCapabilitiesElement();
    }

    if (m_staAvailabilityElement) {
      // WifiMac::AddInformationElement (packet, GetStaAvailabilityElement ());
      reassoc.Get<StaAvailabilityElement>() = GetStaAvailabilityElement();
    }

    packet->AddHeader(reassoc);
  }

  // The standard is not clear on the correct queue for management
  // frames if we are a QoS DMG PCP/AP. The approach taken here is
  // to alwaysuse the non-QoS for these regardless of whether we
  // have a QoS association or not.
  m_txop->Queue(packet, hdr);

  /* For now, we assume station talks to the DMG PCP/AP only */
  SteerAntennaToward(GetBssid());

  if (m_assocRequestEvent.IsRunning()) {
    m_assocRequestEvent.Cancel();
  }
  m_assocRequestEvent = Simulator::Schedule(
      m_assocRequestTimeout, &StaWigigMac::AssocRequestTimeout, this);
}

void StaWigigMac::TryToEnsureAssociated() {
  NS_LOG_FUNCTION(this);
  switch (m_state) {
  case StaWifiMac::ASSOCIATED:
    return;
    break;
  case StaWifiMac::SCANNING:
    /* we have initiated active or passive scanning, continue to wait
       and gather beacons or probe responses until the scanning timeout
     */
    break;
  case StaWifiMac::UNASSOCIATED:
    /* we were associated but we missed a bunch of beacons
     * so we should assume we are not associated anymore.
     * We try to initiate a scan now.
     */
    m_linkDown();
    // We should cancel all the events related to the current DMG PCP/AP
    m_dtiStartEvent.Cancel();
    if (m_activeScanning) {
      StartScanning();
    }
    break;
  case StaWifiMac::WAIT_ASSOC_RESP:
    /* we have sent an association request so we do not need to
       re-send an association request right now. We just need to
       wait until either assoc-request-timeout or until
       we get an association response.
     */
    break;
  case StaWifiMac::REFUSED:
    /* we have sent an association request and received a negative
       association response. We wait until someone restarts an
       association with a given SSID.
     */
    break;
  }
}

uint16_t StaWigigMac::GetAssociationId() {
  NS_LOG_FUNCTION(this);
  if (m_state == StaWifiMac::ASSOCIATED) {
    return m_aid;
  } else {
    return 0;
  }
}

void StaWigigMac::CreateAllocation(DmgTspecElement elem) {
  NS_LOG_FUNCTION(this);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(GetBssid());
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  DmgAddTsRequestFrame frame;

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.qos = WifiActionHeader::ADDTS_REQUEST;
  actionHdr.SetAction(WifiActionHeader::QOS, action);
  frame.Get<DmgTspecElement>() = elem;

  Ptr<Packet> packet = Create<Packet>();
  // packet->AddHeader (elem);
  packet->AddHeader(frame);
  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

void StaWigigMac::DeleteAllocation(uint16_t reason,
                                   DmgAllocationInfo &allocationInfo) {
  NS_LOG_FUNCTION(this);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(GetBssid());
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  DelTsFrame frame;
  frame.SetReasonCode(reason);
  frame.SetDmgAllocationInfo(allocationInfo);

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.qos = WifiActionHeader::DELTS;
  actionHdr.SetAction(WifiActionHeader::QOS, action);

  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(frame);
  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

bool StaWigigMac::IsBeamformedTrained() const {
  return m_abftState == BEAMFORMING_TRAINING_COMPLETED;
}

bool StaWigigMac::IsAssociated() const {
  return m_state == StaWifiMac::ASSOCIATED;
}

void StaWigigMac::ForwardDataFrame(WigigMacHeader hdr, Ptr<Packet> packet,
                                   Mac48Address destAddress) {
  NS_LOG_FUNCTION(this << packet << destAddress);
  hdr.SetAddr1(destAddress);
  hdr.SetAddr2(GetAddress());
  if (!hdr.IsQosAmsdu()) {
    m_edca[AC_BE]->Queue(packet, hdr);
  }
}

void StaWigigMac::Enqueue(Ptr<Packet> packet, Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << to);
  if (!IsAssociated() && IsBeamformedTrained()) {
    NotifyTxDrop(packet);
    TryToEnsureAssociated();
    return;
  }
  WigigMacHeader hdr;
  // If we are not a QoS AP then we definitely want to use AC_BE to
  // transmit the packet. A TID of zero will map to AC_BE (through \c
  // QosUtilsMapTidToAc()), so we use that as our default here.
  uint8_t tid = 0;
  /* The HT Control field is not present in frames transmitted by a DMG STA. */
  hdr.SetAsDmgPpdu();
  /* The QoS Data and QoS Null subtypes are the only Data subtypes transmitted
   * by a DMG STA. */
  hdr.SetType(WIFI_MAC_QOSDATA);
  // Fill in the QoS control field in the MAC header
  tid = QosUtilsGetTidForPacket(packet);
  // Any value greater than 7 is invalid and likely indicates that
  // the packet had no QoS tag, so we revert to zero, which'll
  // mean that AC_BE is used.
  if (tid > 7) {
    tid = 0;
  }
  hdr.SetQosTid(tid);
  hdr.SetQosNoEosp();
  hdr.SetQosAckPolicy(WigigMacHeader::NORMAL_ACK);
  hdr.SetQosNoAmsdu();
  hdr.SetQosRdGrant(m_supportRdp);

  bool found = false;
  AccessPeriodInformation accessPeriodInfo;
  for (DataForwardingTableIterator it = m_dataForwardingTable.begin();
       it != m_dataForwardingTable.end(); it++) {
    accessPeriodInfo = it->second;
    if (it->first == to) {
      found = true;
      break;
    }
  }

  if (found && accessPeriodInfo.nextHopAddress != GetBssid()) {
    hdr.SetAddr1(accessPeriodInfo.nextHopAddress);
    hdr.SetAddr2(GetAddress());
    hdr.SetAddr3(GetBssid());
    hdr.SetDsNotTo();
  } else {
    /* The DMG PCP/AP is our receiver */
    hdr.SetAddr1(GetBssid());
    hdr.SetAddr2(GetAddress());
    hdr.SetAddr3(to);
    hdr.SetDsTo();
  }
  hdr.SetDsNotFrom();

  // Sanity check that the TID is valid
  NS_ASSERT(tid < 8);

  m_edca[QosUtilsMapTidToAc(tid)]->Queue(packet, hdr);
}

void StaWigigMac::StartScanning() {
  NS_LOG_FUNCTION(this);
  m_candidateAps.clear();
  if (m_probeRequestEvent.IsRunning()) {
    m_probeRequestEvent.Cancel();
  }
  if (m_waitBeaconEvent.IsRunning()) {
    m_waitBeaconEvent.Cancel();
  }
  if (GetActiveProbing()) {
    SetState(StaWifiMac::SCANNING);
    SendProbeRequest();
    m_probeRequestEvent = Simulator::Schedule(
        m_probeRequestTimeout, &StaWigigMac::ScanningTimeout, this);
  } else {
    SetState(StaWifiMac::SCANNING);
    m_waitBeaconEvent = Simulator::Schedule(
        m_waitBeaconTimeout, &StaWigigMac::ScanningTimeout, this);
  }
}

void StaWigigMac::ScanningTimeout() {
  NS_LOG_FUNCTION(this);
  if (!m_candidateAps.empty()) {
    DmgApInfo bestAp = m_candidateAps.front();
    m_candidateAps.erase(m_candidateAps.begin());
    NS_LOG_DEBUG("Attempting to associate with DMG BSS-ID " << bestAp.m_bssid);
    // lambda to get beacon interval from Beacon or Probe Response
    auto getBeaconInterval = [](auto &&frame) {
      using T = std::decay_t<decltype(frame)>;
      if constexpr (std::is_same_v<T, MgtBeaconHeader> ||
                    std::is_same_v<T, MgtProbeResponseHeader>) {
        return MicroSeconds(frame.GetBeaconIntervalUs());
      } else {
        NS_ABORT_MSG("Unexpected frame type");
        return Seconds(0);
      }
    };
    Time beaconInterval = std::visit(getBeaconInterval, bestAp.m_frame);

    Time delay = beaconInterval * m_maxMissedBeacons;
    RestartBeaconWatchdog(delay);
    SetState(StaWifiMac::WAIT_ASSOC_RESP);
    SendAssociationRequest(false);
  } else {
    NS_LOG_DEBUG("Exhausted list of candidate DMG PCP/AP; restart scanning");
    StartScanning();
  }
}

void StaWigigMac::AssocRequestTimeout() {
  NS_LOG_FUNCTION(this);
  SetState(StaWifiMac::WAIT_ASSOC_RESP);
  SendAssociationRequest(false);
}

void StaWigigMac::MissedBeacons() {
  NS_LOG_FUNCTION(this);
  if (m_beaconWatchdogEnd > Simulator::Now()) {
    if (m_beaconWatchdog.IsRunning()) {
      m_beaconWatchdog.Cancel();
    }
    m_beaconWatchdog =
        Simulator::Schedule(m_beaconWatchdogEnd - Simulator::Now(),
                            &StaWigigMac::MissedBeacons, this);
    return;
  }
  NS_LOG_DEBUG("DMG Beacon missed");
  SetState(StaWifiMac::UNASSOCIATED);
  TryToEnsureAssociated();
}

void StaWigigMac::RestartBeaconWatchdog(Time delay) {
  NS_LOG_FUNCTION(this << delay);
  m_beaconWatchdogEnd = std::max(Simulator::Now() + delay, m_beaconWatchdogEnd);
  if (Simulator::GetDelayLeft(m_beaconWatchdog) < delay &&
      m_beaconWatchdog.IsExpired()) {
    NS_LOG_DEBUG("Really restart watchdog.");
    m_beaconWatchdog =
        Simulator::Schedule(delay, &StaWigigMac::MissedBeacons, this);
  }
}

StaAvailabilityElement StaWigigMac::GetStaAvailabilityElement() const {
  StaAvailabilityElement availabilityElement;
  StaInfoField field;
  field.SetAid(m_aid);
  field.SetCbap(true);
  field.SetPollingPhase(m_pollingPhase);
  availabilityElement.AddStaInfo(field);
  return availabilityElement;
}

void StaWigigMac::StartBeaconInterval() {
  NS_LOG_FUNCTION(this << "DMG STA Starting BI at " << Simulator::Now());

  /* Save BI start time */
  m_biStartTime = Simulator::Now();

  /* Schedule the next period */
  if (m_nextBeacon == 0) {
    StartBeaconTransmissionInterval();
  } else {
    /* We will not receive DMG Beacon during this BI */
    m_nextBeacon--;
    if (m_atiPresent) {
      StartAnnouncementTransmissionInterval();
      NS_LOG_DEBUG("ATI Channel Access Period for Station:"
                   << GetAddress() << " is starting Now");
    } else {
      StartDataTransmissionInterval();
      NS_LOG_DEBUG("DTI Channel Access Period for Station:"
                   << GetAddress() << " is starting Now");
    }
  }
}

void StaWigigMac::EndBeaconInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Ending BI at " << Simulator::Now());

  /* Cancel any ISS Retry event in the BI to avoid performing SLS in case no DMG
   * beacon is received in the next BI */
  if (m_restartISSEvent.IsRunning()) {
    m_restartISSEvent.Cancel();
  }

  /* Reset State at MacLow */
  m_low->SlsPhaseEnded();

  /* Initialize DMG Beacon Reception */
  m_receivedDmgBeacon = false;

  StartBeaconInterval();
}

void StaWigigMac::StartBeaconTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Starting BTI");
  m_accessPeriod = CHANNEL_ACCESS_BTI;
  /* Re-initialize variables since we expect to receive DMG Beacon */
  m_sectorFeedbackScheduled = false;
  /* Check if there is an ongoing reception */
  Time endRx = m_phy->GetDelayUntilEndRx();
  if (endRx == NanoSeconds(0)) {
    /* If there is not switch to the next Quasi-omni pattern */
    m_codebook->SwitchToNextQuasiPattern();
  } else {
    /* If there is wait until the current reception is finished before switching
     * the antenna configuration */
    Simulator::Schedule(endRx, &Codebook::SwitchToNextQuasiPattern, m_codebook);
  }

  /* Note: Handle special case in which we have associated to DMG PCP/AP but we
     did not receive a DMG Beacon due to interference, so we try to use old
     scheduling information. */
  /**
   * 9.33.6.3 Contention-based access period (CBAP) allocation:
   * When the entire DTI is allocated to CBAP through the CBAP Only field in the
   * DMG Parameters field, then that CBAP is pseudo-static and exists for
   * dot11MaxLostBeacons beacon intervals following the most recently
   * transmitted DMG Beacon that contained the indication, except if the CBAP is
   * canceled by the transmission by the PCP/AP of a DMG Beacon with the CBAP
   * Only field of the DMG Parameters field equal to 0 or an Announce frame with
   * an Extended Schedule element.
   */
  if (IsAssociated() && m_isCbapOnly) {
    m_dtiStartEvent = Simulator::Schedule(
        m_nextDti, &StaWigigMac::StartDataTransmissionInterval, this);
  }
}

void StaWigigMac::StartAssociationBeamformTraining() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Starting A-BFT at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_ABFT;

  /* Schedule the end of the A-BFT period */
  Simulator::Schedule(m_abftDuration,
                      &StaWigigMac::EndAssociationBeamformTraining, this);

  /* Schedule access period after A-BFT period */
  if (m_atiPresent) {
    Simulator::Schedule(m_abftDuration + m_mbifs,
                        &StaWigigMac::StartAnnouncementTransmissionInterval,
                        this);
    NS_LOG_DEBUG("ATI for Station:"
                 << GetAddress() << " is scheduled at "
                 << Simulator::Now() + m_abftDuration + m_mbifs);
  } else {
    m_dtiStartEvent =
        Simulator::Schedule(m_abftDuration + m_mbifs,
                            &StaWigigMac::StartDataTransmissionInterval, this);
    NS_LOG_DEBUG("DTI for Station:"
                 << GetAddress() << " is scheduled at "
                 << Simulator::Now() + m_abftDuration + m_mbifs);
  }

  /* Initialize Variables */
  m_currentSlotIndex = 0;
  m_remainingSlotsPerAbft = m_ssSlotsPerAbft;

  /* Check if we should contend directly after receiving DMG Beacon */
  if (m_immediateAbft) {
    /* If we have already successfully beamformed in previous A-BFT then no need
     * to contend again */
    if (!IsBeamformedTrained()) {
      /* Reinitialize variables */
      m_isBeamformingInitiator = false;
      m_isInitiatorTxss = true; /* DMG-AP always performs TXSS in BTI */
      /* Sector selected -> Perform Responder TXSS */
      m_slsResponderStateMachine = SLS_RESPONDER_SSW_FBCK;
      /* Do the actual association beamforming training */
      DoAssociationBeamformingTraining(m_currentSlotIndex);
    }
  } else {
    if (m_completedBi) {
      m_completedBi = false;
      /* If we have already successfully beamformed in previous A-BFT then no
       * need to contend again */
      if (!IsBeamformedTrained()) {
        /* Reinitialize variables */
        m_isBeamformingInitiator = false;
        m_isInitiatorTxss = true; /* DMG-AP always performs TXSS in BTI */
        /* Do the actual association beamforming training */
        DoAssociationBeamformingTraining(m_currentSlotIndex);
      }
    }
  }

  /* Start A-BFT access period */
  Simulator::ScheduleNow(&StaWigigMac::StartSectorSweepSlot, this);
}

void StaWigigMac::EndAssociationBeamformTraining() {
  NS_LOG_FUNCTION(this << m_abftState);
  /* End SSW slot (A-BFT) and no backoff*/
  m_slsResponderStateMachine = SLS_RESPONDER_IDLE;
  if (m_abftState == START_RSS_BACKOFF_STATE) {
    /* Extract random value for RSS backoff */
    m_rssBackoffRemaining = m_rssBackoffVariable->GetInteger();
    m_abftState = IN_RSS_BACKOFF_STATE;
    NS_LOG_DEBUG("Extracted RSS Backoff Value=" << m_rssBackoffRemaining);
  } else if (m_abftState == IN_RSS_BACKOFF_STATE) {
    /* The responder shall decrement the backoff count by one at the end of each
     * A-BFT period in the following beacon intervals. */
    m_rssBackoffRemaining--;
    NS_LOG_DEBUG("RSS Remaining Backoff=" << m_rssBackoffRemaining);
    if (m_rssBackoffRemaining == 0) {
      NS_LOG_DEBUG("Completed RSS Backoff");
      m_abftState = WAIT_BEAMFORMING_TRAINING;
    }
  }
}

void StaWigigMac::DoAssociationBeamformingTraining(uint8_t currentSlotIndex) {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("Selected Slot Index=" << +m_selectedSlotIndex
                                      << ", A-BFT State=" << m_abftState);
  /* We are in RSS Backoff State and the backoff count equals zero */
  if ((m_abftState == IN_RSS_BACKOFF_STATE) && (m_rssBackoffRemaining == 0)) {
    /* The responder may re-initiate RSS only during an A-BFT when the backoff
     * count becomes zero. */
    m_abftState = WAIT_BEAMFORMING_TRAINING;
  }
  if ((m_abftState == WAIT_BEAMFORMING_TRAINING) ||
      (m_abftState == FAILED_BEAMFORMING_TRAINING)) {
    uint8_t slotIndex; /* The index of the selected slot in the A-BFT period. */
    /* Choose a random SSW Slot to transmit SSW Frames in it */
    m_abftSlot->SetAttribute("Min", DoubleValue(0));
    m_abftSlot->SetAttribute("Max", DoubleValue(m_remainingSlotsPerAbft - 1));
    slotIndex = m_abftSlot->GetInteger();
    NS_LOG_DEBUG("Local Slot Index="
                 << +slotIndex << ", Remaining Slots in the current A-BFT="
                 << +m_remainingSlotsPerAbft);
    m_selectedSlotIndex = slotIndex + currentSlotIndex;
    NS_LOG_DEBUG("Selected Sector Slot Index=" << +m_selectedSlotIndex);
  }
}

void StaWigigMac::StartSectorSweepSlot() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Starting A-BFT SSW Slot ["
              << +m_currentSlotIndex << "] at " << Simulator::Now());
  /* If we have already successfully beamformed in previous A-BFT then no need
   * to contend again */
  if (!IsBeamformedTrained() && (m_selectedSlotIndex == m_currentSlotIndex) &&
      ((m_abftState == WAIT_BEAMFORMING_TRAINING) ||
       (m_abftState == FAILED_BEAMFORMING_TRAINING))) {
    Simulator::ScheduleNow(&StaWigigMac::StartAbftResponderSectorSweep, this,
                           GetBssid());
  }
  m_currentSlotIndex++;
  m_remainingSlotsPerAbft--;
  if (m_remainingSlotsPerAbft > 0) {
    /* Schedule the beginning of the next A-BFT Slot */
    Simulator::Schedule(GetSectorSweepSlotTime(m_ssFramesPerSlot),
                        &StaWigigMac::StartSectorSweepSlot, this);
  }
}

void StaWigigMac::StartAbftResponderSectorSweep(Mac48Address address) {
  NS_LOG_FUNCTION(this << address << m_isResponderTxss);
  if (m_channelAccessManager->CanAccess()) {
    m_sectorSweepStarted = Simulator::Now();
    m_sectorSweepDuration = CalculateSectorSweepDuration(m_ssFramesPerSlot);
    /* Obtain antenna configuration for the highest received SNR to feed it back
     * in SSW-FBCK Field */
    m_feedbackAntennaConfig =
        GetBestAntennaConfiguration(address, true, m_maxSnr);
    /* Schedule SSW FBCK Timeout to detect a collision i.e. missing SSW-FBCK */
    Time timeout = GetSectorSweepSlotTime(m_ssFramesPerSlot) - GetMbifs();
    NS_LOG_DEBUG("Scheduled SSW-FBCK Timeout Event at "
                 << Simulator::Now() + timeout);
    m_sswFbckTimeout =
        Simulator::Schedule(timeout, &StaWigigMac::MissedSswFeedback, this);
    /* Now start doing the specified sweeping in the Responder Phase */
    if (m_isResponderTxss) {
      m_codebook->InitiateAbft(address);
      SendRespodnerTransmitSectorSweepFrame(address);
    } else {
      /* The initiator is switching receive sectors at the same time (Not
       * supported yet) */
    }
  } else {
    /**
     * HANY: Temporary Solution in case of multiple APs and at the beginning of
     * A-BFT Slot another STA is transmitting we consider it as unsuccessful
     * attempt and we try in another slot. The IEEE 802.11-2016 standard does
     * not specify anything about this case. Check Section (10.38.5 Beamforming
     * in A-BFT)
     */
    NS_LOG_DEBUG("Medium is busy, contend in another A-BFT slot.");
    FailedRssAttempt();
  }
}

void StaWigigMac::FailedRssAttempt() {
  NS_LOG_FUNCTION(this);
  /* Each STA maintains a counter, FailedRSSAttempts, of the consecutive number
   * of times the STA initiates RSS during A-BFTs but does not successfully
   * receive an SSW-Feedback frame as a response. If FailedRSSAttempts exceeds
   * dot11RSSRetryLimit, the STA shall select a backoff count as a random
   * integer drawn from a uniform distribution [0, dot11RSSBackoff), i.e., 0
   * inclusive through dot11RSSBackoff exclusive. */
  NS_LOG_DEBUG("Current FailedRssAttemptsCounter=" << m_failedRssAttemptsCounter
                                                   << ", A-BFT State="
                                                   << m_abftState);
  m_failedRssAttemptsCounter++;
  if ((m_failedRssAttemptsCounter <= m_rssAttemptsLimit) &&
      (m_remainingSlotsPerAbft > 0)) {
    m_abftState = FAILED_BEAMFORMING_TRAINING;
    DoAssociationBeamformingTraining(m_currentSlotIndex - 1);
  } else if (m_failedRssAttemptsCounter > m_rssAttemptsLimit) {
    m_abftState = START_RSS_BACKOFF_STATE;
    NS_LOG_DEBUG("Exceeded the number of RSS Attempts. Start RSS Backoff.");
  }
}

void StaWigigMac::MissedSswFeedback() {
  NS_LOG_FUNCTION(this);
  FailedRssAttempt();
}

void StaWigigMac::StartAnnouncementTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO(this << "DMG STA Starting ATI at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_ATI;
  /* We started ATI Period we should stay in quasi-omni mode waiting for packets
   */
  /* Check if there is an ongoing reception */
  Time endRx = m_phy->GetDelayUntilEndRx();
  if (endRx == NanoSeconds(0)) {
    /* If there is not switch to quasi-omni mode */
    m_codebook->SetReceivingInQuasiOmniMode();
  } else {
    /* If there is wait until the current reception is finished before switching
     * the antenna configuration */
    void (Codebook::*sn)() = &Codebook::SetReceivingInQuasiOmniMode;
    Simulator::Schedule(endRx, sn, m_codebook);
  }
  m_dtiStartEvent = Simulator::Schedule(
      m_atiDuration, &StaWigigMac::StartDataTransmissionInterval, this);
  m_dmgAtiTxop->InitiateAtiAccessPeriod(m_atiDuration);
}

void StaWigigMac::StartDataTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Starting DTI at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_DTI;

  if (m_receivedDmgBeacon) {
    /* Get the relative starting time of the DTI channel access period w.r.t the
     * BI */
    m_nextDti = Simulator::Now() - m_biStartTime;
    NS_LOG_DEBUG("DTI starts " << m_nextDti << " after the BI");
  }

  /* Schedule the beginning of the next BI interval */
  m_dtiStartTime = Simulator::Now();
  m_dtiDuration = m_beaconInterval - (Simulator::Now() - m_biStartTime);
  Simulator::Schedule(m_dtiDuration, &StaWigigMac::EndBeaconInterval, this);
  NS_LOG_DEBUG("Next Beacon Interval will start at "
               << Simulator::Now() + m_dtiDuration);

  /* Send Association Request if we are not associated */
  if (!IsAssociated() && IsBeamformedTrained()) {
    /* Check if there is an ongoing reception */
    Time endRx = m_phy->GetDelayUntilEndRx();
    if (endRx == NanoSeconds(0)) {
      /* If there is not send the association request */
      /* We allow normal DCA for access */
      SetState(StaWifiMac::WAIT_ASSOC_RESP);
      Simulator::ScheduleNow(&StaWigigMac::SendAssociationRequest, this, false);
    } else {
      /* If there is wait until the current reception is finished before sending
       * the request
       */
      Simulator::Schedule(endRx, &StaWigigMac::SetState, this,
                          StaWifiMac::WAIT_ASSOC_RESP);
      Simulator::Schedule(endRx, &StaWigigMac::SendAssociationRequest, this,
                          false);
    }
  }

  if (IsBeamformedTrained()) {
    /**
     * A STA shall not transmit within a CBAP unless at least one of the
     * following conditions is met: — The value of the CBAP Only field is equal
     * to 1 and the value of the CBAP Source field is equal to 0 within the DMG
     * Parameters field of the DMG Beacon that allocates the CBAP — The STA is a
     * PCP/AP and the value of the CBAP Only field is equal to 1 and the value
     * of the CBAP Source field is equal to 1 within the DMG Parameters field of
     * the DMG Beacon that allocates the CBAP — The value of the Source AID
     * field of the CBAP is equal to the broadcast AID — The STA’s AID is equal
     * to the value of the Source AID field of the CBAP — The STA’s AID is equal
     * to the value of the Destination AID field of the CBAP
     */

    if (m_isCbapOnly && !m_isCbapSource) {
      NS_LOG_INFO("CBAP allocation only in DTI");
      /* Check if there is an ongoing reception */
      Time endRx = m_phy->GetDelayUntilEndRx();
      if (endRx == NanoSeconds(0)) {
        /* If there is not start the contention period */
        StartContentionPeriod(BROADCAST_CBAP, m_dtiDuration);
      } else {
        /* If there is wait until the current reception is finished before
         * starting the contention period */
        Simulator::Schedule(endRx, &StaWigigMac::StartContentionPeriod, this,
                            BROADCAST_CBAP, m_dtiDuration - endRx);
      }
    } else {
      Time endRx = m_phy->GetDelayUntilEndRx();
      AllocationField field;
      for (AllocationFieldList::iterator iter = m_allocationList.begin();
           iter != m_allocationList.end(); iter++) {
        field = (*iter);
        if (field.GetAllocationType() == SERVICE_PERIOD_ALLOCATION) {
          Time spStart = MicroSeconds(field.GetAllocationStart());
          Time spLength = MicroSeconds(field.GetAllocationBlockDuration());

          NS_ASSERT_MSG(spStart + spLength <= m_dtiDuration,
                        "Allocation should not exceed DTI period.");
          /* Check if there is an ongoing reception when the allocation period
           * will start
           * - based on information from the PHY abut current receptions. if the
           * PHY will still be receiving when the allocation starts, delay the
           * start until the reception is finished and shorten the duration so
           * that the end time remains the same */
          /**
           * Note NINA: Temporary solution for when we are in the middle of
           * receiving a packet from a station from another BSS when a service
           * period is supposed to start. The standard is not clear about
           * whether we end the reception or finish it. For now, the reception
           * is completed and the service period will start after the end of the
           * reception (it will still finish at the same time and have a reduced
           * duration).
           */
          if (spStart < endRx) {
            if (spStart + spLength < endRx) {
              spLength = NanoSeconds(0);
            } else {
              spLength = spLength - (endRx - spStart);
            }
            spStart = endRx;
          }

          if (field.GetSourceAid() == m_aid) {
            uint8_t destAid = field.GetDestinationAid();
            Mac48Address destAddress = m_aidMap[destAid];
            if (field.GetBfControl().IsBeamformTraining()) {
              Simulator::Schedule(
                  spStart, &StaWigigMac::StartBeamformingTraining, this,
                  destAid, destAddress, true,
                  field.GetBfControl().IsInitiatorTxss(),
                  field.GetBfControl().IsResponderTxss(), spLength);
            } else {
              DataForwardingTableIterator forwardingIterator =
                  m_dataForwardingTable.find(destAddress);
              if (forwardingIterator == m_dataForwardingTable.end()) {
                NS_LOG_ERROR("Did not perform Beamforming Training with "
                             << destAddress);
                continue;
              } else {
                forwardingIterator->second.isCbapPeriod = false;
              }
              ScheduleAllocationBlocks(field, SOURCE_STA);
            }
          } else if ((field.GetSourceAid() == AID_BROADCAST) &&
                     (field.GetDestinationAid() == AID_BROADCAST)) {
            /* The PCP/AP may create SPs in its beacon interval with the source
             * and destination AID subfields within an Allocation field set to
             * 255 to prevent transmissions during specific periods in the
             * beacon interval. This period can used for Dynamic Allocation of
             * service peridos (Polling)
             */
            NS_LOG_INFO("No transmission is allowed from "
                        << spStart << " till " << spStart + spLength);
          } else if ((field.GetDestinationAid() == m_aid) ||
                     (field.GetDestinationAid() == AID_BROADCAST)) {
            /* The STA identified by the Destination AID field in the Extended
             * Schedule element should be in the receive state for the duration
             * of the SP in order to receive transmissions from the source DMG
             * STA. */
            uint8_t sourceAid = field.GetSourceAid();
            Mac48Address sourceAddress = m_aidMap[sourceAid];
            if (field.GetBfControl().IsBeamformTraining()) {
              Simulator::Schedule(
                  spStart, &StaWigigMac::StartBeamformingTraining, this,
                  sourceAid, sourceAddress, false,
                  field.GetBfControl().IsInitiatorTxss(),
                  field.GetBfControl().IsResponderTxss(), spLength);
            } else {
              ScheduleAllocationBlocks(field, DESTINATION_STA);
            }
          } else if ((field.GetSourceAid() != m_aid) &&
                     (field.GetDestinationAid() != m_aid)) {
            ScheduleAllocationBlocks(field, RELAY_STA);
          }
        } else if ((field.GetAllocationType() == CBAP_ALLOCATION) &&
                   ((field.GetSourceAid() == AID_BROADCAST) ||
                    (field.GetSourceAid() == m_aid) ||
                    (field.GetDestinationAid() == m_aid)))

        {
          /* Check if there is an ongoing reception and if there is, delay the
           * start of the contention period until the reception is finish,
           * making sure the end time is the same. */
          Time spStart = MicroSeconds(field.GetAllocationStart());
          Time spLength = MicroSeconds(field.GetAllocationBlockDuration());
          if (spStart < endRx) {
            if (spStart + spLength < endRx) {
              spLength = NanoSeconds(0);
            } else {
              spLength = spLength - (endRx - spStart);
            }
            spStart = endRx;
          }

          Simulator::Schedule(spStart, &StaWigigMac::StartContentionPeriod,
                              this, field.GetAllocationId(), spLength);
        }
      }
    }
  }
  /* Raise a callback that we have started DTI */
  m_dtiStarted(GetAddress(), m_dtiDuration);
}

void StaWigigMac::ScheduleAllocationBlocks(AllocationField &field,
                                           StaRole role) {
  NS_LOG_FUNCTION(this);
  Time spStart = MicroSeconds(field.GetAllocationStart());
  Time spLength = MicroSeconds(field.GetAllocationBlockDuration());
  Time spPeriod = MicroSeconds(field.GetAllocationBlockPeriod());
  uint8_t blocks = field.GetNumberOfBlocks();
  Time endRx = m_phy->GetDelayUntilEndRx();
  if (spPeriod.IsStrictlyPositive()) {
    /* We allocate multiple blocks of this allocation as in (9.33.6 Channel
     * access in scheduled DTI) */
    /* A_start + (i – 1) × A_period */
    for (uint8_t i = 0; i < blocks; i++) {
      NS_LOG_INFO("Schedule SP Block [" << i << "] at " << spStart << " till "
                                        << spStart + spLength);
      /* Check if the service period starts while there is an ongoing reception
       */
      /**
       * Note NINA: Temporary solution for when we are in the middle of
       * receiving a packet from a station from another BSS when a service
       * period is supposed to start. The standard is not clear about whether we
       * end the reception or finish it. For now, the reception is completed and
       * the service period will start after the end of the reception (it will
       * still finish at the same time and have a reduced duration).
       */
      Time spLengthNew = spLength;
      Time spStartNew = spStart;
      if (spStart < endRx) {
        /* if does schedule the start after the reception is complete */

        if (spStart + spLength < endRx) {
          spLengthNew = NanoSeconds(0);
        } else {
          spLengthNew = spLength - (endRx - spStart);
        }
        spStartNew = endRx;
      }
      Simulator::Schedule(spStartNew, &StaWigigMac::InitiateAllocationPeriod,
                          this, field.GetAllocationId(), field.GetSourceAid(),
                          field.GetDestinationAid(), spLengthNew, role);
      spStart += spLength + spPeriod + GUARD_TIME;
    }
  } else {
    /* Special case when Allocation Block Period=0, i.e. consecutive blocks *
     * We try to avoid scheduling multiple blocks, so we schedule one big block
     */
    spLength = spLength * blocks;
    if (spStart < endRx) {
      if (spStart + spLength < endRx) {
        spLength = NanoSeconds(0);
      } else {
        spLength = spLength - (endRx - spStart);
      }
      spStart = endRx;
    }
    Simulator::Schedule(spStart, &StaWigigMac::InitiateAllocationPeriod, this,
                        field.GetAllocationId(), field.GetSourceAid(),
                        field.GetDestinationAid(), spLength, role);
  }
}

void StaWigigMac::InitiateAllocationPeriod(AllocationId id, uint8_t srcAid,
                                           uint8_t dstAid, Time spLength,
                                           StaRole role) {
  NS_LOG_FUNCTION(this << +id << +srcAid << +dstAid << spLength << role);

  /* Relay Pair */
  auto redsPair = std::make_pair(srcAid, dstAid);
  auto it = m_relayLinkMap.find(redsPair);
  bool protectedAllocation = (it != m_relayLinkMap.end());

  if (role == SOURCE_STA) {
    Mac48Address dstAddress = m_aidMap[dstAid];
    if (protectedAllocation) {
      auto info = it->second;
      NS_LOG_INFO("Initiating relay periods for the source REDS");
      /* Schedule events related to the beginning and end of relay period */
      Simulator::ScheduleNow(&StaWigigMac::InitiateRelayPeriods, this, info);
      Simulator::Schedule(spLength, &StaWigigMac::EndRelayPeriods, this,
                          redsPair);

      /* Schedule events related to the intervals within the relay period */
      if ((info.transmissionLink == RELAY_LINK) && (info.rdsDuplexMode == 0)) {
        Simulator::ScheduleNow(&StaWigigMac::StartHalfDuplexRelay, this, id,
                               spLength, true);
      } else if ((info.transmissionLink == DIRECT_LINK) &&
                 (info.rdsDuplexMode == 0)) {
        /* Schedule the beginning of this service period */
        Simulator::ScheduleNow(&StaWigigMac::StartServicePeriod, this, id,
                               spLength, dstAid, dstAddress, true);
      } else if (info.rdsDuplexMode == 1) {
        Simulator::ScheduleNow(&StaWigigMac::StartFullDuplexRelay, this, id,
                               spLength, dstAid, dstAddress, true);
      }
      Simulator::Schedule(spLength, &StaWigigMac::EndServicePeriod, this);
    } else {
      /* No relay link has been established so schedule normal service period */
      Simulator::ScheduleNow(&StaWigigMac::StartServicePeriod, this, id,
                             spLength, dstAid, dstAddress, true);
      Simulator::Schedule(spLength, &StaWigigMac::EndServicePeriod, this);
    }
  } else if (role == DESTINATION_STA) {
    Mac48Address srcAddress = m_aidMap[srcAid];
    if (protectedAllocation) {
      auto info = it->second;
      NS_LOG_INFO("Initiating relay periods for the destination REDS");
      /* Schedule events related to the beginning and end of relay period */
      Simulator::ScheduleNow(&StaWigigMac::InitiateRelayPeriods, this, info);
      Simulator::Schedule(spLength, &StaWigigMac::EndRelayPeriods, this,
                          redsPair);

      /* Schedule events related to the intervals within the relay period */
      if ((info.transmissionLink == RELAY_LINK) && (info.rdsDuplexMode == 0)) {
        Simulator::ScheduleNow(&StaWigigMac::StartHalfDuplexRelay, this, id,
                               spLength, false);
      } else if ((info.transmissionLink == DIRECT_LINK) &&
                 (info.rdsDuplexMode == 0)) {
        Simulator::ScheduleNow(&StaWigigMac::StartServicePeriod, this, id,
                               spLength, srcAid, srcAddress, false);
      } else if (info.rdsDuplexMode == 1) {
        /* Schedule Data Sensing Timeout to detect missing frame transmission */
        Simulator::ScheduleNow(&StaWigigMac::StartFullDuplexRelay, this, id,
                               spLength, srcAid, srcAddress, false);
        Simulator::Schedule(MicroSeconds(m_relayDataSensingTime),
                            &StaWigigMac::RelayDataSensingTimeout, this);
      }
    } else {
      Simulator::ScheduleNow(&StaWigigMac::StartServicePeriod, this, id,
                             spLength, srcAid, srcAddress, false);
      Simulator::Schedule(spLength, &StaWigigMac::EndServicePeriod, this);
    }
  } else if ((role == RELAY_STA) && (protectedAllocation)) {
    /* We protect this SP allocation by this RDS */
    auto info = it->second;
    NS_LOG_INFO("Initiating relay periods for the RDS");

    /* We are the RDS */
    Simulator::ScheduleNow(&StaWigigMac::SwitchToRelayOperationalMode, this);
    Simulator::Schedule(spLength, &StaWigigMac::RelayOperationTimeout, this);

    if (info.rdsDuplexMode != 1) // HD-DF
    {
      NS_LOG_INFO("Protecting the SP by an RDS in HD-DF Mode: Source AID="
                  << info.srcRedsAid
                  << " and Destination AID=" << info.dstRedsAid);

      /* Schedule events related to the beginning and end of relay period */
      Simulator::ScheduleNow(&StaWigigMac::InitiateRelayPeriods, this, info);
      Simulator::Schedule(spLength, &StaWigigMac::EndRelayPeriods, this,
                          redsPair);

      /* Schedule an event to direct the antennas toward the source REDS */
      Simulator::ScheduleNow(&StaWigigMac::SteerAntennaToward, this,
                             info.srcRedsAddress, false);

      /* Schedule half duplex relay periods */
      Simulator::ScheduleNow(&StaWigigMac::StartHalfDuplexRelay, this, id,
                             spLength, false);
    }
  }
}

void StaWigigMac::InitiateRelayPeriods(RelayLinkInfo &info) {
  NS_LOG_FUNCTION(this);
  m_periodProtected = true;
  m_relayLinkInfo = info;
  /* Schedule peridos associated to the transmission link */
  if ((m_relayLinkInfo.transmissionLink == RELAY_LINK) &&
      (m_relayLinkInfo.rdsDuplexMode == 0)) {
    m_firstPeriod =
        Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relayFirstPeriod),
                            &StaWigigMac::RelayFirstPeriodTimeout, this);
  } else if ((m_relayLinkInfo.transmissionLink == DIRECT_LINK) ||
             (m_relayLinkInfo.rdsDuplexMode == 1)) {
    m_linkChangeInterval = Simulator::Schedule(
        MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval),
        &StaWigigMac::RelayLinkChangeIntervalTimeout, this);
  }
}

void StaWigigMac::EndRelayPeriods(REDS_PAIR &pair) {
  NS_LOG_FUNCTION(this);
  if (m_periodProtected) {
    m_periodProtected = false;
    /* Check if we need to remove relay link at the end of the SP allocation
     * protected by a relay */
    if (m_relayLinkInfo.tearDownRelayLink) {
      RemoveRelayEntry(pair.first, pair.second);
    } else {
      /* Store information related to the relay operation mode */
      m_relayLinkMap[pair] = m_relayLinkInfo;
    }
  }
}

void StaWigigMac::RelayLinkChangeIntervalTimeout() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG STA Starting Link Change Interval at " << Simulator::Now());
  if (m_relayLinkInfo.rdsDuplexMode == 1) // FD-AF
  {
    if (CheckTimeAvailabilityForPeriod(
            GetRemainingAllocationTime(),
            MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval))) {
      /* Schedule the next Link Change Interval */
      m_linkChangeInterval = Simulator::Schedule(
          MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval),
          &StaWigigMac::RelayLinkChangeIntervalTimeout, this);

      /* Schedule Data Sensing Timeout Event at the destination REDS */
      if (m_relayLinkInfo.dstRedsAid == m_aid) {
        m_relayDataExchanged = false;

        /* Schedule Data Sensing Timeout to detect missing frame transmission */
        Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relayDataSensingTime),
                            &StaWigigMac::RelayDataSensingTimeout, this);
      } else if ((m_relayLinkInfo.switchTransmissionLink) &&
                 (m_relayLinkInfo.srcRedsAid == m_aid)) {
        /* If the source REDS decides to change the link at the start of the
         * following Link Change Interval period and the Normal mode is used,
         * the source REDS shall start its frame transmission after Data Sensing
         * Time from the start of the following Link Change Interval period. */
        m_relayLinkInfo.switchTransmissionLink = false;
        if (m_relayLinkInfo.transmissionLink == DIRECT_LINK) {
          m_relayLinkInfo.transmissionLink = RELAY_LINK;
        } else {
          m_relayLinkInfo.transmissionLink = RELAY_LINK;
        }
        m_transmissionLinkChanged(GetAddress(),
                                  m_relayLinkInfo.transmissionLink);
        SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
        Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relayDataSensingTime),
                            &StaWigigMac::ResumeServicePeriodTransmission,
                            this);
      }
    }
  } else // HD-DF
  {
    if (m_relayLinkInfo.switchTransmissionLink) {
      /* We are using the direct link and we decided to switch to the relay link
       */
      m_relayLinkInfo.switchTransmissionLink = false;
      m_relayLinkInfo.transmissionLink = RELAY_LINK;
      SuspendServicePeriodTransmission();

      if (CheckTimeAvailabilityForPeriod(
              GetRemainingAllocationTime(),
              MicroSeconds(m_relayLinkInfo.relayFirstPeriod))) {
        if (m_relayLinkInfo.srcRedsAid == m_aid) {
          NS_LOG_DEBUG(
              "We are the source REDS and we want to switch to the relay link");
          /* If the source REDS decides to change to the relay link at the start
           * of the following Link Change Interval period, the source REDS shall
           * start its frame transmission at the start of the following Link
           * Change Interval period. */
          m_relayLinkInfo.relayForwardingActivated = true;
          m_dataForwardingTable[m_relayLinkInfo.dstRedsAddress].nextHopAddress =
              m_relayLinkInfo.selectedRelayAddress;
        }
        /* Special case for First Period after link switching */
        StartRelayFirstPeriodAfterSwitching();
        m_firstPeriod =
            Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relayFirstPeriod),
                                &StaWigigMac::RelayFirstPeriodTimeout, this);
      }
    } else {
      /* Check how much time left in the current service period protected by the
       * relay */
      if (CheckTimeAvailabilityForPeriod(
              GetRemainingAllocationTime(),
              MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval))) {
        /* Schedule the next Link Change Interval */
        m_linkChangeInterval = Simulator::Schedule(
            MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval),
            &StaWigigMac::RelayLinkChangeIntervalTimeout, this);
      }
    }
  }
}

bool StaWigigMac::CheckTimeAvailabilityForPeriod(Time servicePeriodDuration,
                                                 Time partialDuration) {
  return ((servicePeriodDuration - partialDuration) >= Seconds(0));
}

void StaWigigMac::StartFullDuplexRelay(AllocationId allocationId, Time length,
                                       uint8_t peerAid,
                                       Mac48Address peerAddress,
                                       bool isSource) {
  NS_LOG_FUNCTION(this << length << +peerAid << peerAddress << isSource);
  m_currentAllocationId = allocationId;
  m_currentAllocation = SERVICE_PERIOD_ALLOCATION;
  m_currentAllocationLength = length;
  m_allocationStarted = Simulator::Now();
  m_peerStationAid = peerAid;
  m_peerStationAddress = peerAddress;
  m_moreData = true;
  m_servicePeriodStartedCallback(GetAddress(), peerAddress);
  /* Check current transmission link */
  if (m_relayLinkInfo.transmissionLink == DIRECT_LINK) {
    SteerAntennaToward(peerAddress);
  } else if (m_relayLinkInfo.transmissionLink == RELAY_LINK) {
    SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
  }
  m_edca[AC_BE]->StartAllocationPeriod(SERVICE_PERIOD_ALLOCATION, allocationId,
                                       peerAddress, length);
  if (isSource) {
    m_edca[AC_BE]->InitiateServicePeriodTransmission();
  }
}

void StaWigigMac::StartHalfDuplexRelay(AllocationId allocationId,
                                       Time servicePeriodLength,
                                       bool firstPertiodInitiator) {
  NS_LOG_FUNCTION(this << servicePeriodLength << firstPertiodInitiator);
  m_currentAllocationId = allocationId;
  m_currentAllocation = SERVICE_PERIOD_ALLOCATION;
  m_currentAllocationLength = servicePeriodLength;
  m_allocationStarted = Simulator::Now();
  if ((!m_relayLinkInfo.relayForwardingActivated) &&
      (m_relayLinkInfo.srcRedsAid == m_aid)) {
    m_relayLinkInfo.relayForwardingActivated = true;
    m_dataForwardingTable[m_relayLinkInfo.dstRedsAddress].nextHopAddress =
        m_relayLinkInfo.selectedRelayAddress;
  }
  if (m_relayLinkInfo.transmissionLink == RELAY_LINK) {
    StartRelayFirstPeriod();
  }
}

void StaWigigMac::StartRelayFirstPeriodAfterSwitching() {
  NS_LOG_FUNCTION(this);
  if (m_relayLinkInfo.srcRedsAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
    m_edca[AC_BE]->StartAllocationPeriod(
        SERVICE_PERIOD_ALLOCATION, m_currentAllocationId,
        m_relayLinkInfo.selectedRelayAddress,
        MicroSeconds(m_relayLinkInfo.relayFirstPeriod));
    m_edca[AC_BE]->AllowChannelAccess();
  } else if (m_relayLinkInfo.selectedRelayAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.srcRedsAddress);
  } else if (m_relayLinkInfo.dstRedsAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.srcRedsAddress);
  }
}

void StaWigigMac::StartRelayFirstPeriod() {
  NS_LOG_FUNCTION(this);
  if (m_relayLinkInfo.srcRedsAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
    /* Restore previously suspended transmission in LowMac */
    m_low->RestoreAllocationParameters(m_currentAllocationId);
    m_edca[AC_BE]->StartAllocationPeriod(
        SERVICE_PERIOD_ALLOCATION, m_currentAllocationId,
        m_relayLinkInfo.selectedRelayAddress,
        MicroSeconds(m_relayLinkInfo.relayFirstPeriod));
    m_edca[AC_BE]->InitiateServicePeriodTransmission();
  } else if (m_relayLinkInfo.selectedRelayAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.srcRedsAddress);
  } else if (m_relayLinkInfo.dstRedsAid == m_aid) {
    /* The destination REDS shall switch to the direct link at each First Period
     * and listen to the medium toward the source REDS. If the destination REDS
     * receives a valid frame from the source REDS, the destination REDS shall
     * remain on the direct link and consider the Link Change Interval to begin
     * at the start of the First Period. Otherwise, the destination  REDS shall
     * change the link at the start of the next Second Period and attempt to
     * receive frames from the source REDS through the RDS. If the active link
     * is the relay link and the More Data field in the last frame  received
     * from the RDS is equal to 0, then the destination REDS shall not switch to
     * the direct link even if it does not receive any frame during the Second
     * Period. */
    SteerAntennaToward(m_relayLinkInfo.srcRedsAddress);
  }
}

void StaWigigMac::StartRelaySecondPeriod() {
  NS_LOG_FUNCTION(this);
  if (m_relayLinkInfo.srcRedsAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.dstRedsAddress);
  } else if (m_relayLinkInfo.selectedRelayAid == m_aid) {
    SteerAntennaToward(m_relayLinkInfo.dstRedsAddress);
    m_edca[AC_BE]->StartAllocationPeriod(
        SERVICE_PERIOD_ALLOCATION, m_currentAllocationId,
        m_relayLinkInfo.dstRedsAddress,
        MicroSeconds(m_relayLinkInfo.relaySecondPeriod));
    m_edca[AC_BE]->InitiateServicePeriodTransmission();
  } else if (m_relayLinkInfo.dstRedsAid == m_aid) {
    /* The destination REDS shall change the link at the start of the next
     * Second Period and attempt to receive frames from the source REDS through
     * the RDS. */
    SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
  }
}

void StaWigigMac::SuspendRelayPeriod() {
  NS_LOG_FUNCTION(this);
  m_edca[AC_BE]->DisableChannelAccess();
  m_edca[AC_BE]->EndAllocationPeriod();
}

void StaWigigMac::RelayFirstPeriodTimeout() {
  NS_LOG_FUNCTION(this);
  if (CheckTimeAvailabilityForPeriod(
          GetRemainingAllocationTime(),
          MicroSeconds(m_relayLinkInfo.relaySecondPeriod))) {
    /* Data has been exchanged during the first period, so schedule Second
     * Period Timer */
    m_secondPeriod =
        Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relaySecondPeriod),
                            &StaWigigMac::RelaySecondPeriodTimeout, this);
    if (m_relayLinkInfo.srcRedsAid == m_aid) {
      /* We are the source REDS and the first period has expired so suspend its
       * transmission
       */
      SuspendRelayPeriod();
      StartRelaySecondPeriod();
    } else if (m_relayLinkInfo.selectedRelayAid == m_aid) {
      /* We are the RDS and the first period has expired so initiate
       * transmission in the second period */
      StartRelaySecondPeriod();
    } else if (m_relayLinkInfo.dstRedsAid == m_aid) {
      /* We are the destination REDS and the first period has expired so prepare
       * for recepition in the second period  */
      StartRelaySecondPeriod();
    }
  }
}

void StaWigigMac::RelaySecondPeriodTimeout() {
  NS_LOG_FUNCTION(this);
  if (!m_relayLinkInfo.switchTransmissionLink) {
    if (CheckTimeAvailabilityForPeriod(
            GetRemainingAllocationTime(),
            MicroSeconds(m_relayLinkInfo.relayFirstPeriod))) {
      /* Data has been exchanged during the first period, so schedule Second
       * Period Timer */
      m_firstPeriod =
          Simulator::Schedule(MicroSeconds(m_relayLinkInfo.relayFirstPeriod),
                              &StaWigigMac::RelayFirstPeriodTimeout, this);
      if (m_relayLinkInfo.srcRedsAid == m_aid) {
        /* We are the source REDS and the second period has expired so start
         * transmission in the first period */
        StartRelayFirstPeriod();
      } else if (m_relayLinkInfo.selectedRelayAid == m_aid) {
        /* We are the RDS and the second period has expired so prepare for
         * recepition in the first period */
        SuspendRelayPeriod();
        StartRelayFirstPeriod();
      } else if (m_relayLinkInfo.dstRedsAid == m_aid) {
        /* We are the destination REDS and the second period has expired so
         * suspend operations */
        StartRelayFirstPeriod();
      }
    }
  } else {
    /* If a link change to the direct link occurs, the source REDS shall start
     * to transmit a frame using the direct link at the end of the Second Period
     * when the Link Change Interval begins. */
    m_relayLinkInfo.switchTransmissionLink = false;
    m_relayLinkInfo.transmissionLink = DIRECT_LINK;
    SuspendServicePeriodTransmission();

    /* Check how much time left in the current service period protected by the
     * relay */
    if (CheckTimeAvailabilityForPeriod(
            GetRemainingAllocationTime(),
            MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval))) {
      /* Schedule the next Link Change Interval */
      m_linkChangeInterval = Simulator::Schedule(
          MicroSeconds(m_relayLinkInfo.relayLinkChangeInterval),
          &StaWigigMac::RelayLinkChangeIntervalTimeout, this);
    }
  }
}

void StaWigigMac::MissedAck(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this << hdr);
  if (m_periodProtected && (hdr.GetAddr1() == m_relayLinkInfo.dstRedsAddress) &&
      (m_relayLinkInfo.rdsDuplexMode)) {
    /* If a source REDS transmits a frame to the destination REDS via the direct
     * link but does not receive an expected ACK frame or BA frame from the
     * destination REDS during a Link Change Interval period, the source REDS
     * should change the link used for frame transmission at the start of the
     * following Link Change Interval period and use the RDS to forward frames
     * to the destination REDS. */
    m_relayLinkInfo.switchTransmissionLink = true;
    SuspendServicePeriodTransmission();
  }
}

void StaWigigMac::RelayDataSensingTimeout() {
  NS_LOG_FUNCTION(this << m_relayDataExchanged
                       << m_channelAccessManager->IsReceiving() << m_moreData);
  if (m_relayLinkInfo.rdsDuplexMode == 1) // FD-AF
  {
    if ((!m_relayDataExchanged) && (!m_channelAccessManager->IsReceiving()) &&
        m_moreData) {
      m_relayLinkInfo.switchTransmissionLink = true;
      if (m_relayLinkInfo.transmissionLink == DIRECT_LINK) {
        /* In the Normal mode, if the destination REDS does not receive a valid
         * frame from the source REDS within Data Sensing Time after the start
         * of a Link Change Interval, the destination REDS shall immediately
         * change the link to attempt to receive frames from the source REDS
         * through the RDS. If the More Data field in the last frame received
         * from the source REDS is equal to 0, then the destination REDS shall
         * not switch to the link in the next Link Change Interval period even
         * if it does not receive a frame during the Data Sensing Time. An
         * example of frame transfer under Normal mode with FD-AF RDS is
         * illustrated in Figure 9-77. */
        NS_LOG_DEBUG(
            "Destinations REDS did not receive frames during data sensing "
            "interval so switch to the relay link");
        m_relayLinkInfo.transmissionLink = RELAY_LINK;
        SteerAntennaToward(m_relayLinkInfo.selectedRelayAddress);
      } else {
        NS_LOG_DEBUG(
            "Destinations REDS did not receive frames during data sensing "
            "interval so switch to the direct link");
        m_relayLinkInfo.transmissionLink = DIRECT_LINK;
        SteerAntennaToward(m_relayLinkInfo.srcRedsAddress);
      }
      m_transmissionLinkChanged(GetAddress(), m_relayLinkInfo.transmissionLink);
    }
  }
}

void StaWigigMac::SwitchToRelayOperationalMode() {
  NS_LOG_FUNCTION(this);
  m_relayMode = true;
}

void StaWigigMac::RelayOperationTimeout() {
  NS_LOG_FUNCTION(this);
  m_relayMode = false;
}

void StaWigigMac::BeamLinkMaintenanceTimeout() {
  NS_LOG_FUNCTION(this);
  if (!m_spSource) {
    /* Following the expiration of the beamlink maintenance time (specified by
     * the current value of the dot11BeamLinkMaintenanceTime variable), the
     * destination DMG STA of the SP shall configure its receive antenna to a
     * quasi-omni antenna pattern for the remainder of the SP and during any SP
     * following the expiration of the beamlink maintenance time. */
    m_codebook->SetReceivingInQuasiOmniMode();
  }
  WigigMac::BeamLinkMaintenanceTimeout();
}

void StaWigigMac::BrpSetupCompleted(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
}

void StaWigigMac::NotifyBrpPhaseCompleted() { NS_LOG_FUNCTION(this); }

void StaWigigMac::RequestInformation(Mac48Address stationAddress,
                                     WifiInformationElementIdList &list) {
  NS_LOG_FUNCTION(this << stationAddress);
  /* Obtain Information about the node like DMG Capabilities and AID */

  RequestElement requestElement;
  requestElement.AddListOfRequestedElementId(list);

  Ptr<Packet> packet = Create<Packet>();
  // packet->AddHeader (requestElement);

  ExtInformationRequest requestHdr;
  requestHdr.Get<RequestElement>() = requestElement;
  requestHdr.SetSubjectAddress(stationAddress);
  packet->AddHeader(requestHdr);

  SendInformationRequest(GetBssid(), packet);
}

void StaWigigMac::SendChannelMeasurementReport(
    Mac48Address to, uint8_t token,
    ChannelMeasurementInfoList &measurementList) {
  NS_LOG_FUNCTION(this);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  ExtMultiRelayChannelMeasurementReport responseHdr;
  responseHdr.SetDialogToken(token);
  responseHdr.SetChannelMeasurementList(measurementList);

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.dmgAction =
      WifiActionHeader::DMG_MULTI_RELAY_CHANNEL_MEASUREMENT_REPORT;
  actionHdr.SetAction(WifiActionHeader::DMG, action);

  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(responseHdr);
  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

void StaWigigMac::RemoveRelayEntry(uint16_t sourceAid,
                                   uint16_t destinationAid) {
  NS_LOG_FUNCTION(this << sourceAid << destinationAid);
  REDS_PAIR redsPair = std::make_pair(sourceAid, destinationAid);
  auto it = m_relayLinkMap.find(redsPair);
  if (it != m_relayLinkMap.end()) {
    RelayLinkInfo info = it->second;
    if (info.srcRedsAid == m_aid) {
      /* Change next hop address for packets */
      m_dataForwardingTable[info.dstRedsAddress].nextHopAddress =
          info.dstRedsAddress;
    }
    m_relayLinkMap.erase(it);
  }
}

void StaWigigMac::TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) {
  if (hdr.IsAction()) {
    WifiActionHeader actionHdr;
    Ptr<Packet> packet1 = packet->Copy();
    packet1->RemoveHeader(actionHdr);
    if (actionHdr.GetAction().dmgAction ==
        WifiActionHeader::DMG_INFORMATION_RESPONSE) {
      NS_LOG_LOGIC("Transmitted Information Response to "
                   << hdr.GetAddr2() << " after Group Beamforming");
    }
  }
  WigigMac::TxOk(packet, hdr);
}

void StaWigigMac::FrameTxOk(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  if (hdr.IsSsw()) {
    if (m_accessPeriod == CHANNEL_ACCESS_ABFT) {
      if (m_codebook->GetNextSectorInAbft()) {
        /* We still have more sectors to sweep */
        if (m_isResponderTxss) {
          Simulator::Schedule(
              m_sbifs, &StaWigigMac::SendRespodnerTransmitSectorSweepFrame,
              this, hdr.GetAddr1());
        } else {
          Simulator::Schedule(
              m_sbifs, &StaWigigMac::SendReceiveSectorSweepFrame, this,
              hdr.GetAddr1(), m_totalSectors, BeamformingResponder);
        }
      }
    } else if (m_accessPeriod == CHANNEL_ACCESS_DTI) {
      /* We are performing SLS during DTI access period */
      bool changeAntenna;
      if (m_codebook->GetNextSector(changeAntenna)) {
        /* Check if we change antenna so we use different spacing value */
        Time spacing;
        if (changeAntenna) {
          spacing = m_lbifs;
        } else {
          spacing = m_sbifs;
        }

        if (m_isBeamformingInitiator) {
          if (m_isInitiatorTxss) {
            /* We are I-TXSS */
            Simulator::Schedule(
                spacing, &StaWigigMac::SendInitiatorTransmitSectorSweepFrame,
                this, hdr.GetAddr1());
          } else {
            /* We are I-RXSS */
            Simulator::Schedule(
                spacing, &StaWigigMac::SendReceiveSectorSweepFrame, this,
                hdr.GetAddr1(), m_totalSectors, BeamformingInitiator);
          }
        } else {
          if (m_isResponderTxss) {
            /* We are R-TXSS */
            Simulator::Schedule(
                spacing, &StaWigigMac::SendRespodnerTransmitSectorSweepFrame,
                this, hdr.GetAddr1());
          } else {
            /* We are R-RXSS */
            Simulator::Schedule(
                spacing, &StaWigigMac::SendReceiveSectorSweepFrame, this,
                hdr.GetAddr1(), m_totalSectors, BeamformingResponder);
          }
        }
      } else {
        /* We have finished sector sweeping */
        if (m_isBeamformingInitiator) {
          if (m_isResponderTxss) {
            /* We are the initiator and the responder is performing TXSS */
            m_codebook->SetReceivingInQuasiOmniMode();
          } else {
            /* I-RXSS so initiator switches between different receiving sectors
             */
            m_codebook->StartSectorSweeping(hdr.GetAddr1(), ReceiveSectorSweep,
                                            1);
          }
          /* Start Timeout value: The initiator may restart the ISS up to
           * dot11BFRetryLimit times if it does not receive an SSW frame from
           * the responder in dot11BFTXSSTime time following the end of the ISS.
           */
          m_restartISSEvent = Simulator::Schedule(
              MilliSeconds(dot11BFTXSSTime),
              &StaWigigMac::RestartInitiatorSectorSweep, this, hdr.GetAddr1());
        } else {
          /* Shall we use previous Tx or Rx sector if we are doing Rx or Tx
           * sector sweep
           */
          m_codebook->SetReceivingInQuasiOmniMode();
          /* Start Timeout value: The responder goes to Idle state if it does
           * not receive SSW-FBCK from the initiator in dot11MaxBFTime time
           * following the end of the RSS. */
          m_sswFbckTimeout = Simulator::Schedule(
              dot11MaxBFTime * m_beaconInterval,
              &StaWigigMac::ResetSlsResponderVariables, this);
        }
      }
    }
  } else if (hdr.IsSswAck()) {
    /* We are SLS Responder, raise callback for SLS Phase completion. */
    Mac48Address address = hdr.GetAddr1();
    BEST_ANTENNA_CONFIGURATION info = m_bestAntennaConfig[address];
    ANTENNA_CONFIGURATION antennaConfig;
    if (m_isResponderTxss) {
      antennaConfig = std::get<0>(info);
    } else if (!m_isInitiatorTxss) {
      antennaConfig = std::get<1>(info);
    }

    /* Note: According to the standard, the Responder transits to the TXSS Phase
       Complete when we receive non SSW frame and non-SSW-FBCK frame */
    m_dmgSlsTxop->SlsBftCompleted();
    m_performingBft = false;
    m_slsResponderStateMachine = SLS_RESPONDER_TXSS_PHASE_COMPELTED;
    m_slsCompleted(SlsCompletionAttrbitutes(
        hdr.GetAddr1(), CHANNEL_ACCESS_DTI, BeamformingResponder,
        m_isInitiatorTxss, m_isResponderTxss, m_bftIdMap[hdr.GetAddr1()],
        antennaConfig.first, antennaConfig.second, m_maxSnr));
    /* Resume data transmission after SLS operation */
    if (m_currentAllocation == CBAP_ALLOCATION) {
      m_txop->ResumeTxopTransmission();
      for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
        i->second->ResumeTxopTransmission();
      }
    }
  } else if (hdr.IsSswFbck()) {
    Time sswAckTimeout = SSW_ACK_TIMEOUT;
    /* We are SLS Initiator, so schedule event for not receiving SSW-ACK, so we
     * restart SSW Feedback process again */
    NS_LOG_INFO("Schedule SSW-ACK Timeout at "
                << Simulator::Now() + sswAckTimeout);
    m_slsInitiatorStateMachine = SLS_INITIATOR_SSW_ACK;
    m_sswAckTimeoutEvent = Simulator::Schedule(
        sswAckTimeout, &StaWigigMac::ResendSswFbckFrame, this);
  } else {
    WigigMac::FrameTxOk(hdr);
  }
}

void StaWigigMac::Receive(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);

  const WigigMacHeader *hdr = &mpdu->GetHeader();
  Ptr<Packet> packet = mpdu->GetPacket()->Copy();
  Mac48Address from = hdr->GetAddr2();

  if (hdr->GetAddr3() == GetAddress()) {
    NS_LOG_LOGIC("packet sent by us.");
    return;
  } else if (hdr->GetAddr1() != GetAddress() && !hdr->GetAddr1().IsGroup() &&
             !hdr->IsDmgBeacon()) {
    NS_LOG_LOGIC("packet is not for us");
    NotifyRxDrop(packet);
    return;
  } else if (m_relayMode && (m_rdsDuplexMode == 0) && hdr->IsData()) {
    NS_LOG_LOGIC("Work as relay, forward packet to "
                 << m_relayLinkInfo.dstRedsAddress);
    /* We are the RDS in HD-DF so forward the packet to the destination REDS */
    ForwardDataFrame(*hdr, packet, m_relayLinkInfo.dstRedsAddress);
    return;
  } else if (hdr->IsData()) {
    if (!IsAssociated() && hdr->GetAddr2() != GetBssid()) {
      NS_LOG_LOGIC("Received data frame while not associated: ignore");
      NotifyRxDrop(packet);
      return;
    }

    if (hdr->IsQosData()) {
      /* Relay Related Variables */
      m_relayDataExchanged = true;
      m_moreData = hdr->IsMoreData();
      if (hdr->IsQosAmsdu()) {
        NS_ASSERT(hdr->GetAddr3() == GetBssid());
        DeaggregateAmsduAndForward(mpdu);
        packet = nullptr;
      } else {
        ForwardUp(packet, hdr->GetAddr3(), hdr->GetAddr1());
      }
    } else {
      ForwardUp(packet, hdr->GetAddr3(), hdr->GetAddr1());
    }
    return;
  } else if (hdr->IsProbeReq() || hdr->IsAssocReq()) {
    // This is a frame aimed at DMG PCP/AP, so we can safely ignore it.
    NotifyRxDrop(packet);
    return;
  } else if (hdr->IsAction() || hdr->IsActionNoAck()) {
    WifiActionHeader actionHdr;
    packet->RemoveHeader(actionHdr);
    switch (actionHdr.GetCategory()) {
    case WifiActionHeader::QOS:
      switch (actionHdr.GetAction().qos) {
      case WifiActionHeader::ADDTS_RESPONSE: {
        DmgAddTsResponseFrame frame;
        packet->RemoveHeader(frame);
        /* Contain modified airtime allocation */
        if (frame.GetStatusCode().IsSuccess()) {
          NS_LOG_LOGIC("DMG Allocation Request accepted by the PCP/AP");
        } else {
          NS_LOG_LOGIC("DMG Allocation Request rejected by the PCP/AP");
        }
        return;
      }
      default:
        packet->AddHeader(actionHdr);
        WigigMac::Receive(mpdu);
        return;
      }

    case WifiActionHeader::DMG:
      switch (actionHdr.GetAction().dmgAction) {
      case WifiActionHeader::DMG_RELAY_SEARCH_RESPONSE: {
        ExtRelaySearchResponseHeader responseHdr;
        packet->RemoveHeader(responseHdr);
        /* The response contains list of RDSs in the current DMG BSS */
        m_rdsList = responseHdr.GetRelayCapableList();
        m_rdsDiscoveryCompletedCallback(m_rdsList);
        return;
      }
      case WifiActionHeader::DMG_MULTI_RELAY_CHANNEL_MEASUREMENT_REQUEST: {
        NS_LOG_LOGIC("Received Multi-Relay Channel Measurement Request from "
                     << hdr->GetAddr2());
        ExtMultiRelayChannelMeasurementRequest requestHdr;
        packet->RemoveHeader(requestHdr);
        /* Prepare the Channel Report */
        ChannelMeasurementInfoList list;
        Ptr<ExtChannelMeasurementInfo> elem;
        double measuredsnr;
        uint8_t snr;
        if (m_rdsActivated) {
          /** We are the RDS and we received the request from the source REDS
           * **/
          /* Obtain Channel Measurement between the source REDS and RDS */
          GetBestAntennaConfiguration(hdr->GetAddr2(), true, measuredsnr);
          snr = -(unsigned int)(4 * (measuredsnr - 19));
          elem = Create<ExtChannelMeasurementInfo>();
          elem->SetPeerStaAid(m_macMap[hdr->GetAddr2()]);
          elem->SetSnr(snr);
          list.push_back(elem);
        } else {
          /**
           * We are the destination REDS and we've received the request from the
           * source REDS. Report back the measurement information between
           * destination REDS and all the available RDS.
           */
          for (RelayCapableStaList::iterator iter = m_rdsList.begin();
               iter != m_rdsList.end(); iter++) {
            elem = Create<ExtChannelMeasurementInfo>();
            GetBestAntennaConfiguration(hdr->GetAddr2(), true, measuredsnr);
            snr = -(unsigned int)(4 * (measuredsnr - 19));
            elem->SetPeerStaAid(iter->first);
            elem->SetSnr(snr);
            list.push_back(elem);
          }
        }
        SendChannelMeasurementReport(hdr->GetAddr2(),
                                     requestHdr.GetDialogToken(), list);
        return;
      }
      case WifiActionHeader::DMG_MULTI_RELAY_CHANNEL_MEASUREMENT_REPORT: {
        if (m_relayLinkInfo.srcRedsAid == m_aid) {
          ExtMultiRelayChannelMeasurementReport responseHdr;
          packet->RemoveHeader(responseHdr);
          if (!m_relayLinkInfo.waitingDestinationRedsReports) {
            /* Perform BF with the destination REDS, currently this is done by
             * invoking a callback function to the main program. The main
             * program schedules manually a service period between the source
             * REDS and destination REDS */

            /* Send Multi-Relay Channel Measurement Request to the Destination
             * REDS */
            m_relayLinkInfo.waitingDestinationRedsReports = true;
          } else {
            /**
             * The source REDS is aware of the following channel measurements
             * with:
             * 1. Zero or more RDS.
             * 2. Between Destination REDS and zero or more RDS.
             * The Source REDS shall select on of the previous RDS.
             */

            /* Report the measurements to the user to decide relay selection */
            m_relayLinkInfo.rdsCapabilitiesInfo =
                m_rdsList[m_relayLinkInfo.selectedRelayAid];
          }
          m_channelReportReceived(hdr->GetAddr2());
        }
        return;
      }
      case WifiActionHeader::DMG_INFORMATION_RESPONSE: {
        ExtInformationResponse responseHdr;
        packet->RemoveHeader(responseHdr);
        // WifiInformationElementContainer informationElements;
        // packet->RemoveHeader (informationElements);

        /* Record the Information Obtained */
        Mac48Address stationAddress = responseHdr.GetSubjectAddress();

        /* If this field is set to the broadcast address, then the STA is
         * providing information regarding all associated STAs.*/
        if (stationAddress.IsBroadcast()) {
          NS_LOG_LOGIC(
              "Received DMG Information Response frame regarding all DMG STAs "
              "in the DMG BSS.");
        } else {
          NS_LOG_LOGIC(
              "Received DMG Information Response frame regarding DMG STA: "
              << stationAddress);
        }

        /* Store all the DMG Capabilities */
        DmgCapabilities dmgCapabilities;
        std::size_t count = responseHdr.Get<DmgCapabilities>().size();
        for (std::size_t i = 0; i < count; ++i) {
          DmgCapabilities dmgCapabilities =
              responseHdr.Get<DmgCapabilities>()[i];
          InformationMapIterator infoIter =
              m_informationMap.find(dmgCapabilities.GetStaAddress());
          if (infoIter != m_informationMap.end()) {
            m_informationMap[dmgCapabilities.GetStaAddress()] = dmgCapabilities;
          } else {
            m_informationMap[dmgCapabilities.GetStaAddress()] = dmgCapabilities;
            MapAidToMacAddress(dmgCapabilities.GetAid(),
                               dmgCapabilities.GetStaAddress());
          }
        }

        /* Store Information related to the requested IEs */
        InformationMapIterator infoIter = m_informationMap.find(stationAddress);
        if (infoIter != m_informationMap.end()) {
          m_informationMap[stationAddress] = dmgCapabilities;
        } else {
          m_informationMap[stationAddress] = dmgCapabilities;
          MapAidToMacAddress(dmgCapabilities.GetAid(),
                             dmgCapabilities.GetStaAddress());
        }

        m_informationReceived(stationAddress);
        return;
      }
      default:
        NS_FATAL_ERROR("Unsupported Action frame received");
        return;
      }
    default:
      packet->AddHeader(actionHdr);
      WigigMac::Receive(mpdu);
      return;
    }
  } else if (hdr->IsGrantFrame()) {
    NS_LOG_LOGIC("Received Grant frame from=" << hdr->GetAddr2());

    CtrlDmgGrant grant;
    packet->RemoveHeader(grant);

    /* Initiate Service Period */
    DynamicAllocationInfoField field = grant.GetDynamicAllocationInfo();
    BfControlField bf = grant.GetBfControl();
    Mac48Address peerAddress;
    uint8_t peerAid;
    bool isSource = false;
    Time startTime =
        hdr->GetDuration() - MicroSeconds(field.GetAllocationDuration());
    if (field.GetSourceAid() == m_aid) {
      /* We are the initiator in the allocated SP */
      isSource = true;
      peerAid = field.GetDestinationAid();
    } else {
      /* We are the responder in the allocated SP */
      peerAid = field.GetSourceAid();
    }
    peerAddress = m_aidMap[peerAid];

    /** The allocation begins upon successful reception of the Grant frame plus
     * the value from the Duration field of the Grant frame minus the value of
     * the Allocation Duration field of the Grant frame. */

    /* Check whether the service period for BFT or Data Communication */
    if (bf.IsBeamformTraining()) {
      Simulator::Schedule(startTime, &StaWigigMac::StartBeamformingTraining,
                          this, peerAid, peerAddress, isSource,
                          bf.IsInitiatorTxss(), bf.IsResponderTxss(),
                          MicroSeconds(field.GetAllocationDuration()));
    } else {
      Simulator::Schedule(startTime, &StaWigigMac::StartServicePeriod, this, 0,
                          MicroSeconds(field.GetAllocationDuration()), peerAid,
                          peerAddress, isSource);
    }

    return;
  } else if (hdr->IsDmgBeacon()) {
    NS_LOG_LOGIC("Received DMG Beacon frame with BSSID=" << hdr->GetAddr1());

    ExtDmgBeacon beacon;
    packet->RemoveHeader(beacon);
    // WifiInformationElementContainer informationElements;
    // packet->RemoveHeader (informationElements);
    bool goodBeacon = false;
    // const auto apInformationElements = StaWifiMac::GetApInformationElements
    // (informationElements);
    if (GetSsid().IsBroadcast() || beacon.Get<Ssid>()->IsEqual(GetSsid())) {
      NS_LOG_LOGIC("DMG Beacon is for our SSID");
      goodBeacon = true;
    }
    if (goodBeacon && m_state == StaWifiMac::ASSOCIATED) {
      m_beaconArrival = Simulator::Now();
      Time delay =
          MicroSeconds(beacon.GetBeaconIntervalUs() * m_maxMissedBeacons);
      RestartBeaconWatchdog(delay);
    }

    if (goodBeacon && !m_activeScanning &&
        m_accessPeriod == CHANNEL_ACCESS_BTI) {
      /* Check if we have already received a DMG Beacon during the BTI period.
       */
      if (!m_receivedDmgBeacon) {
        m_receivedDmgBeacon = true;
        m_stationSnrMap.erase(hdr->GetAddr1());

        /* Beacon Interval Field */
        ExtDmgBeaconIntervalCtrlField beaconInterval =
            beacon.GetBeaconIntervalControlField();
        m_nextBeacon = beaconInterval.GetNextBeacon();
        m_atiPresent = beaconInterval.IsAtiPresent();
        m_nextAbft = beaconInterval.GetNextAbft();
        m_ssSlotsPerAbft = beaconInterval.GetAbftLength();
        m_ssFramesPerSlot = beaconInterval.GetFss();
        if (m_nextAbft == 0) {
          m_isResponderTxss = beaconInterval.IsResponderTxss();
        }
        /* DMG Parameters */
        ExtDmgParameters parameters = beacon.GetDmgParameters();
        m_isCbapOnly = parameters.GetCbapOnly();
        m_isCbapSource = parameters.GetCbapSource();

        if (m_state == StaWifiMac::UNASSOCIATED) {
          m_TXSSSpan = beaconInterval.GetTxssSpan();
          m_remainingBis = m_TXSSSpan;
          m_completedBi = false;

          /* Next DMG ATI Element */
          if (m_atiPresent) {
            std::optional<NextDmgAti> atiElement = beacon.Get<NextDmgAti>();
            NS_ASSERT(atiElement);
            m_atiDuration = MicroSeconds(atiElement->GetAtiDuration());
          } else {
            m_atiDuration = MicroSeconds(0);
          }
        }

        /* Update the remaining number of BIs to cover the whole TXSS */
        m_remainingBis--;
        if (m_remainingBis == 0) {
          m_completedBi = true;
          m_remainingBis = m_TXSSSpan;
        }

        /* Record DMG Capabilities */
        std::optional<DmgCapabilities> capabilities =
            beacon.Get<DmgCapabilities>();

        if (capabilities) {
          m_informationMap[hdr->GetAddr1()] = *capabilities;
          m_stationManager->AddStationDmgCapabilities(hdr->GetAddr2(),
                                                      *capabilities);
        }

        /* DMG Operation Element */
        std::optional<DmgOperationElement> operationElement =
            beacon.Get<DmgOperationElement>();
        // informationElements.DeserializeIfPresent (operationElement);
        NS_ASSERT(operationElement);
        /* Organizing medium access periods (Synchronization with TSF) */
        m_abftDuration =
            m_ssSlotsPerAbft * GetSectorSweepSlotTime(m_ssFramesPerSlot);
        m_biStartTime = MicroSeconds(beacon.GetTimestamp());
        m_beaconInterval = MicroSeconds(beacon.GetBeaconIntervalUs());
        NS_LOG_DEBUG(
            "BI started="
            << m_biStartTime.As(Time::US)
            << ", A-BFT duration=" << m_abftDuration.As(Time::US)
            << ", ATI duration=" << m_atiDuration.As(Time::US)
            << ", BeaconInterval=" << m_beaconInterval.As(Time::US)
            << ", BHI duration="
            << MicroSeconds(operationElement->GetMinBhiDuration()).As(Time::US)
            << ", TSF=" << MicroSeconds(beacon.GetTimestamp()).As(Time::US)
            << ", HDR-Duration=" << hdr->GetDuration().As(Time::US)
            << ", frame duration=" << m_phy->GetLastRxDuration());

        /* Check if we have schedulled DTI before */
        if (m_dtiStartEvent.IsRunning()) {
          m_dtiStartEvent.Cancel();
          NS_LOG_DEBUG("Cancel the pre-schedulled DTI event since we received "
                       "one DMG Beacon");
        }

        if (!beaconInterval.IsDiscoveryMode()) {
          Time trnDuration = NanoSeconds(0);

          /* This function is triggered on NanoSeconds basis and the duration
           * field is in MicroSeconds basis */
          Time dmgBeaconDurationUs = MicroSeconds(ceil(
              static_cast<double>(m_phy->GetLastRxDuration().GetNanoSeconds()) /
              1000));
          Time startTime =
              hdr->GetDuration() +
              (dmgBeaconDurationUs - m_phy->GetLastRxDuration() + trnDuration) +
              GetMbifs();
          if (m_nextAbft == 0) {
            /* Schedule A-BFT following the end of the BTI Period */
            NS_LOG_DEBUG("A-BFT Period for Station="
                         << GetAddress() << " is scheduled at "
                         << Simulator::Now() + startTime);
            SetBssid(hdr->GetAddr1());
            m_abftEvent = Simulator::Schedule(
                startTime, &StaWigigMac::StartAssociationBeamformTraining,
                this);
          } else {
            /* Schedule ATI period following the end of BTI Period */
            if (m_atiPresent) {
              Simulator::Schedule(
                  startTime,
                  &StaWigigMac::StartAnnouncementTransmissionInterval, this);
              NS_LOG_DEBUG("ATI for Station:" << GetAddress()
                                              << " is scheduled at "
                                              << Simulator::Now() + startTime);
            } else {
              m_dtiStartEvent = Simulator::Schedule(
                  startTime, &StaWigigMac::StartDataTransmissionInterval, this);
              NS_LOG_DEBUG("DTI for Station:" << GetAddress()
                                              << " is scheduled at "
                                              << Simulator::Now() + startTime);
            }
          }
        }

        /* A STA shall not transmit in the A-BFT of a beacon interval if it does
         * not receive at least one DMG Beacon frame during the BTI of that
         * beacon interval.*/

        /** Check the existence of other Information Element Fields **/

        /* Extended Scheudle Element */
        std::optional<ExtendedScheduleElement> scheduleElement =
            beacon.Get<ExtendedScheduleElement>();
        if (scheduleElement) {
          m_allocationList = scheduleElement->GetAllocationFieldList();
        }
      }

      /* Sector Sweep Field */
      DmgSswField ssw = beacon.GetSswField();
      NS_LOG_DEBUG("DMG Beacon CDOWN=" << +ssw.GetCountDown());
      /* Map the antenna configuration, Addr1=BSSID */
      MapTxSnr(hdr->GetAddr1(), ssw.GetDmgAntennaId(), ssw.GetSectorId(),
               m_stationManager->GetRxSnr());
      m_slsResponderStateMachine = SLS_RESPONDER_SECTOR_SELECTOR;
    }

    if (goodBeacon && m_state == StaWifiMac::SCANNING && m_activeScanning) {
      NS_LOG_DEBUG("DMG Beacon received while scanning from "
                   << hdr->GetAddr2());
      SnrTag snrTag;
      bool removed = packet->RemovePacketTag(snrTag);
      NS_ASSERT(removed);
      DmgApInfo apInfo;
      apInfo.m_apAddr = hdr->GetAddr2();
      apInfo.m_bssid = hdr->GetAddr3();
      apInfo.m_snr = snrTag.Get();
      apInfo.m_beacon = beacon;
      UpdateCandidateApList(apInfo);
    }

    return;
  } else if (hdr->IsSswFbck()) {
    NS_LOG_LOGIC("Responder: Received SSW-FBCK frame from=" << hdr->GetAddr2());

    if (m_performingBft && (m_peerStationAddress != hdr->GetAddr2())) {
      NS_LOG_LOGIC("Responder: Received SSW-FBCK frame from different "
                   "initiator, so ignore it");
      return;
    }

    CtrlDmgSswFbck fbck;
    packet->RemoveHeader(fbck);

    /* Check Beamformed link maintenance */
    RecordBeamformedLinkMaintenanceValue(fbck.GetBfLinkMaintenanceField());

    if (m_isResponderTxss) {
      /* The SSW-FBCK contains the best TX antenna by this station */
      DmgSswFbckField sswFeedback = fbck.GetSswFeedbackField();
      sswFeedback.IsPartOfIss(false);

      /* Record best antenna configuration */
      ANTENNA_CONFIGURATION antennaConfig =
          std::make_pair(sswFeedback.GetDmgAntenna(), sswFeedback.GetSector());
      UpdateBestTxAntennaConfiguration(hdr->GetAddr2(), antennaConfig,
                                       sswFeedback.GetSnrReport());

      /* We add the station to the list of the stations we can directly
       * communicate with */
      AddForwardingEntry(hdr->GetAddr2());

      /* Cancel SSW-FBCK timeout */
      m_sswFbckTimeout.Cancel();

      if (m_accessPeriod == CHANNEL_ACCESS_ABFT) {
        NS_LOG_LOGIC("Best Tx Antenna Config by this DMG STA to DMG STA="
                     << hdr->GetAddr2()
                     << ": AntennaId=" << +antennaConfig.first
                     << ", SectorId=" << +antennaConfig.second);

        /* Raise an event that we selected the best sector to the DMG AP */
        m_slsResponderStateMachine = SLS_RESPONDER_TXSS_PHASE_COMPELTED;
        m_slsCompleted(SlsCompletionAttrbitutes(
            hdr->GetAddr2(), CHANNEL_ACCESS_BHI, BeamformingResponder,
            m_isInitiatorTxss, m_isResponderTxss, m_bftIdMap[hdr->GetAddr2()],
            antennaConfig.first, antennaConfig.second, m_maxSnr));
        /* We received SSW-FBCK so we cancel the timeout event and update
         * counters */
        /* The STA shall set FailedRSSAttempts to 0 upon successfully receiving
         * an SSW-Feedback frame during the A-BFT. */
        m_failedRssAttemptsCounter = 0;
        m_abftState = BEAMFORMING_TRAINING_COMPLETED;
      } else if (m_accessPeriod == CHANNEL_ACCESS_DTI) {
        /* This might be a retry SSW-FBCK frame, so we need to inform the MacLow
         * that we are serving SLS.
         * CHECK: why MacLow receive even though we set the NAV duration
         * correctly */
        m_low->SlsPhaseStarted();

        NS_LOG_LOGIC("Scheduled SSW-ACK Frame to "
                     << hdr->GetAddr2() << " at "
                     << Simulator::Now() + m_mbifs);
        m_slsResponderStateMachine = SLS_RESPONDER_TXSS_PHASE_PRECOMPLETE;
        Simulator::Schedule(GetMbifs(), &StaWigigMac::SendSswAckFrame, this,
                            hdr->GetAddr2(), hdr->GetDuration());
      }
    }

    return;
  } else if (hdr->IsProbeResp()) {
    if (m_state == StaWifiMac::SCANNING) {
      MgtProbeResponseHeader probeResp;
      packet->RemoveHeader(probeResp);
      // WifiInformationElementContainer informationElements;
      // packet->RemoveHeader (informationElements);

      std::optional<Ssid> ssid = probeResp.Get<Ssid>();

      NS_ASSERT(ssid);
      if (!ssid->IsEqual(GetSsid())) {
        NS_LOG_DEBUG("Probe response is not for our SSID");
        return;
      }
      SnrTag tag;
      bool removed = packet->RemovePacketTag(tag);
      NS_ASSERT(removed);
      NS_LOG_DEBUG("SnrTag value: " << tag.Get());
      DmgApInfo apInfo;
      apInfo.m_apAddr = hdr->GetAddr2();
      apInfo.m_bssid = hdr->GetAddr3();
      apInfo.m_snr = tag.Get();
      apInfo.m_frame = probeResp;
      UpdateCandidateApList(apInfo);
    }
    return;
  } else if (hdr->IsAssocResp() || hdr->IsReassocResp()) {
    if (m_state == StaWifiMac::WAIT_ASSOC_RESP) {
      WigigMgtAssocResponseHeader assocResp;
      packet->RemoveHeader(assocResp);

      if (m_assocRequestEvent.IsRunning()) {
        m_assocRequestEvent.Cancel();
      }
      if (assocResp.GetStatusCode().IsSuccess()) {
        /** Record DMG AP Capabilities **/
        NS_ASSERT(assocResp.Get<DmgCapabilities>().size() == 1);
        std::optional<DmgCapabilities> capabilities =
            assocResp.Get<DmgCapabilities>()[0];

        /* Record DMG SC MCSs (1-4) as mandatory modes for data communication */
        AddMcsSupport(from, 1, 4);
        if (capabilities) {
          /* Record SC MCSs range */
          AddMcsSupport(from, 5, capabilities->GetMaximumScTxMcs());
          /* Record OFDM MCSs range */
          if ((GetPhy()->GetSupportOfdmPhy()) &&
              (capabilities->GetMaximumOfdmTxMcs() != 0)) {
            AddMcsSupport(from, 13, capabilities->GetMaximumOfdmTxMcs());
          }
        }

        /* Record DMG Capabilities */
        m_stationManager->AddStationDmgCapabilities(hdr->GetAddr2(),
                                                    *capabilities);

        /* Change association state */
        m_aid = assocResp.GetAssociationId();
        MapAidToMacAddress(AID_AP, hdr->GetAddr3());
        SetState(StaWifiMac::ASSOCIATED);
        NS_LOG_DEBUG("Association completed with " << hdr->GetAddr3());

        if (!m_linkUp.IsNull()) {
          m_linkUp();
        }

        if (m_waitBeaconEvent.IsRunning()) {
          m_waitBeaconEvent.Cancel();
        }
      } else {
        NS_LOG_DEBUG("Association refused");
        if (m_candidateAps.empty()) {
          SetState(StaWifiMac::REFUSED);
        } else {
          ScanningTimeout();
        }
      }
    }
    return;
  }

  WigigMac::Receive(mpdu);
}

DmgCapabilities StaWigigMac::GetDmgCapabilities() const {
  DmgCapabilities capabilities;
  capabilities.SetStaAddress(GetAddress()); /* STA MAC Address*/
  capabilities.SetAid(m_aid);

  /* DMG STA Capability Information Field */
  capabilities.SetSpsh(m_supportSpsh);
  capabilities.SetReverseDirection(m_supportRdp);
  capabilities.SetNumberOfRxDmgAntennas(m_codebook->GetTotalNumberOfAntennas());
  capabilities.SetNumberOfSectors(
      m_codebook->GetTotalNumberOfTransmitSectors());
  capabilities.SetRxssLength(m_codebook->GetTotalNumberOfReceiveSectors());
  capabilities.SetAmpduParameters(
      5, 0); /* Hardcoded Now (Maximum A-MPDU + No restriction) */
  capabilities.SetSupportedMcs(
      GetPhy()->GetMaxScRxMcs(), GetPhy()->GetMaxOfdmRxMcs(),
      GetPhy()->GetMaxScTxMcs(), GetPhy()->GetMaxOfdmTxMcs(),
      GetPhy()->GetSupportLpScPhy(), false);
  capabilities.SetAppduSupported(
      false); /* Currently A-PPDU Aggregation is not supported */
  capabilities.SetAntennaPatternReciprocity(m_antennaPatternReciprocity);

  return capabilities;
}

void StaWigigMac::UpdateCandidateApList(DmgApInfo &newApInfo) {
  NS_LOG_FUNCTION(this << newApInfo.m_bssid << newApInfo.m_apAddr
                       << newApInfo.m_snr);
  // Remove duplicate DMG PCP/AP Info entry.
  for (auto i = m_candidateAps.begin(); i != m_candidateAps.end(); ++i) {
    if ((newApInfo.m_bssid == (*i).m_bssid)) {
      if (newApInfo.m_snr > (*i).m_snr) {
        NS_LOG_DEBUG("Received DMG Beacon with higher SNR than the current "
                     "stored DMG Beacon");
        m_candidateAps.erase(i);
        break;
      } else {
        NS_LOG_DEBUG(
            "Received DMG Beacon with lower SNR than the current stored DMG "
            "Beacon, so we keep the old one");
        return;
      }
    }
  }
  // Insert before the entry with lower SNR.
  for (auto i = m_candidateAps.begin(); i != m_candidateAps.end(); ++i) {
    if (newApInfo.m_snr > (*i).m_snr) {
      m_candidateAps.insert(i, newApInfo);
      return;
    }
  }
  // If the new DMG PCP/AP Info is the lowest, insert at the back of the list.
  m_candidateAps.push_back(newApInfo);
}

void StaWigigMac::SetState(StaWifiMac::MacState value) {
  StaWifiMac::MacState previousState = m_state;
  m_state = value;
  if (value == StaWifiMac::ASSOCIATED &&
      previousState != StaWifiMac::ASSOCIATED) {
    NS_LOG_DEBUG("Associating to " << GetBssid());
    m_assocLogger(GetBssid(), GetAssociationId());
  } else if (value != StaWifiMac::ASSOCIATED &&
             previousState == StaWifiMac::ASSOCIATED) {
    NS_LOG_DEBUG("Disassociating from " << GetBssid());
    m_deAssocLogger(GetBssid());
  }
}

} // namespace ns3
