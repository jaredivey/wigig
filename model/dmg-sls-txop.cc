/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "dmg-sls-txop.h"

#include "mac-low.h"
#include "wigig-mac-queue.h"

#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

#include <algorithm>

#undef NS_LOG_APPEND_CONTEXT
#define NS_LOG_APPEND_CONTEXT                                                  \
  if (m_low) {                                                                 \
    std::clog << "[mac=" << m_low->GetAddress() << "] ";                       \
  }

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("DmgSlsTxop");

NS_OBJECT_ENSURE_REGISTERED(DmgSlsTxop);

TypeId DmgSlsTxop::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgSlsTxop")
                          .SetParent<WigigTxop>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgSlsTxop>();
  return tid;
}

DmgSlsTxop::DmgSlsTxop()
    : m_slsRequestsDeque(), m_servingSls(false), m_slsRole(SLS_IDLE),
      m_isFeedback(false) {
  NS_LOG_FUNCTION(this);
}

DmgSlsTxop::~DmgSlsTxop() { NS_LOG_FUNCTION(this); }

void DmgSlsTxop::SetAccessGrantedCallback(AccessGranted callback) {
  m_accessGrantedCallback = callback;
}

void DmgSlsTxop::ResumeTXSS() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("Is SLS Requests Queue Empty = " << m_slsRequestsDeque.empty());
  ResetCw();
  GenerateBackoff();
  if (!m_servingSls || (m_servingSls && m_slsRole == SLS_RESPONDER)) {
    InitializeVariables();
    if (!m_slsRequestsDeque.empty() && !IsAccessRequested() &&
        m_channelAccessManager->IsAccessAllowed()) {
      m_slsRole = SLS_INITIATOR;
      m_channelAccessManager->RequestAccess(this);
    }
  } else if (m_servingSls && m_slsRole == SLS_INITIATOR) {
    /* We are already performing SLS as initiator, but it failed in the previous
     * BI and there was no enough time in the DTI to continue so we need to
     * continue trying in this new BI.
     */
    RestartAccessIfNeeded();
  }
}

void DmgSlsTxop::AppendSlsRequest(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);

  /* Check if we are serving the same peer address (We've received SLS Request
     due to timeout but we are going to do feedback, so we avoid doing new SLS
     request. */
  if (m_servingSls && m_peerStation == peerAddress &&
      m_slsRole == SLS_INITIATOR) {
    NS_LOG_DEBUG("We are performing SLS with "
                 << peerAddress << ", so avoid adding new SLS Request");
    return;
  }

  // Check if the Deque has already a previous Beamforming Request to avoid too
  // many beamforming training accesses.
  bool found = false;
  /* Check if we already have a request for the same peer Address */
  for (auto it = m_slsRequestsDeque.begin(); it != m_slsRequestsDeque.end();
       ++it) {
    if ((*it) == peerAddress) {
      found = true;
      NS_LOG_DEBUG("Another SLS Request exists for " << peerAddress);
      break;
    }
  }

  if (!found) {
    m_slsRequestsDeque.push_front(peerAddress);
  }
}

void DmgSlsTxop::InitiateTxopSectorSweep(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);

  /* Check if we are serving the same peer address (We've received SLS Request
     due to timeout but we are going to do feedback, so we avoid doing new SLS
     request. */
  if (m_servingSls && (m_peerStation == peerAddress) &&
      (m_slsRole == SLS_INITIATOR)) {
    NS_LOG_DEBUG("We are performing SLS with "
                 << peerAddress << ", so avoid adding new SLS Request");
    return;
  }

  // Check if the Deque has already a previous Beamforming Request to avoid too
  // many beamforming training accesses.
  bool found = false;
  /* Check if we already have a request for the same peer Address */
  for (auto it = m_slsRequestsDeque.begin(); it != m_slsRequestsDeque.end();
       ++it) {
    if ((*it) == peerAddress) {
      found = true;
      NS_LOG_DEBUG("Another SLS Request exists for " << peerAddress);
      break;
    }
  }

  if (!found) {
    m_slsRequestsDeque.push_front(peerAddress);
    NS_LOG_DEBUG("AccessRequested=" << IsAccessRequested() << ", AccessAllowed="
                                    << m_channelAccessManager->IsAccessAllowed()
                                    << ", ServingSLS=" << m_servingSls);
    if (!IsAccessRequested() && m_channelAccessManager->IsAccessAllowed() &&
        !m_servingSls) {
      m_slsRole = SLS_INITIATOR;
      m_channelAccessManager->RequestAccess(this);
    }
  }
}

void DmgSlsTxop::StartResponderSectorSweep(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);
  NS_ASSERT_MSG(!IsAccessRequested(),
                "We should not have requested Responder Sector Sweep before.");
  if (m_channelAccessManager->IsAccessAllowed()) {
    m_slsRole = SLS_RESPONDER;
    m_peerStation = peerAddress;
    m_channelAccessManager->RequestAccess(this, true);
  }
}

void DmgSlsTxop::StartInitiatorFeedback(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);
  /* Check if we are serving the same peer address (We've received SLS Request
     due to timeout but we are going to do feedback, so we avoid doing new SLS
     request. */
  NS_ASSERT_MSG(m_servingSls && m_peerStation == peerAddress,
                "Feedback should be done with the current SLS request");
  NS_ASSERT_MSG(!IsAccessRequested(),
                "We should not have requested Initiator Sector Sweep before.");
  if (m_channelAccessManager->IsAccessAllowed()) {
    m_isFeedback = true;
    m_channelAccessManager->RequestAccess(this, true);
  }
}

void DmgSlsTxop::NotifyAccessGranted() {
  NS_LOG_FUNCTION(this << m_slsRole);
  NS_ASSERT(m_accessRequested);
  m_accessRequested = false;
  /* We are in different access period, we are not allowed to do BFT */
  if (!m_channelAccessManager->IsAccessAllowed()) {
    /* We were granted access to the channel during BHI, so abort access and
       leave it to the ResumeTXSS function to continue beamforming training */
    NS_LOG_DEBUG("Granted access during BHI, so abort.");
    return;
  }
  if (!m_servingSls) {
    /* Serving new SLS Request */
    if (m_slsRole == SLS_INITIATOR) {
      m_peerStation = m_slsRequestsDeque.front();
    }
    m_servingSls = true;
    GetLow()->SlsPhaseStarted();
    NS_LOG_DEBUG("Access granted for a new SLS request with " << m_peerStation);
  } else {
    NS_LOG_DEBUG("Access granted for an existing SLS request with "
                 << m_peerStation);
  }
  m_accessGrantedCallback(m_peerStation, m_slsRole, m_isFeedback);
}

void DmgSlsTxop::InitializeVariables() {
  NS_LOG_DEBUG(this);
  m_servingSls = false;
  m_isFeedback = false;
  m_slsRole = SLS_IDLE;
  m_currentPacket = nullptr;
}

void DmgSlsTxop::SectorSweepPhaseFailed() {
  NS_LOG_FUNCTION(this);
  UpdateFailedCw();
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void DmgSlsTxop::RxSswAckFailed() {
  NS_LOG_FUNCTION(this);
  /* Initiator failed to receive SSW-ACK from the responder */
  RestartAccessIfNeeded();
}

void DmgSlsTxop::SlsBftCompleted() {
  NS_LOG_FUNCTION(this);
  if (m_slsRole == SLS_INITIATOR) {
    /* Remove the request from the queue */
    m_slsRequestsDeque.pop_front();
  }
  InitializeVariables();
  /* Reset WigigTxop state */
  ResetCw();
  m_cwTrace = GetCw();
  GenerateBackoff();
  GetLow()->SlsPhaseEnded();
  RestartAccessIfNeeded();
}

void DmgSlsTxop::SlsBftFailed(bool retryAccess) {
  NS_LOG_FUNCTION(this << retryAccess);
  InitializeVariables();
  /* Remove the current request from the queue as we exceed the number of
   * trials. */
  m_slsRequestsDeque.pop_front();
  /* Reset SLS state at the MacLow */
  GetLow()->SlsPhaseEnded();
  if (retryAccess) {
    /* Reset SLS WigigTxop state */
    ResetCw();
    m_cwTrace = GetCw();
    GenerateBackoff();
    RestartAccessIfNeeded();
  }
}

void DmgSlsTxop::TransmitFrame(Ptr<const Packet> packet,
                               const WigigMacHeader &hdr, Time duration) {
  NS_LOG_FUNCTION(this << packet << &hdr << duration);
  m_currentHdr = hdr;
  m_currentPacket = packet;
  m_currentParams.EnableOverrideDurationId(duration);
  m_currentParams.DisableRts();
  m_currentParams.DisableAck();
  m_currentParams.DisableNextData();
  GetLow()->TransmitSingleFrame(Create<WigigMacQueueItem>(packet, m_currentHdr),
                                m_currentParams, this);
}

void DmgSlsTxop::RestartAccessIfNeeded() {
  NS_LOG_FUNCTION(this);
  if (!IsAccessRequested() && m_channelAccessManager->IsAccessAllowed() &&
      (m_servingSls || !m_slsRequestsDeque.empty())) {
    m_channelAccessManager->RequestAccess(this);
  }
}

void DmgSlsTxop::NotifyInternalCollision() {
  NS_LOG_FUNCTION(this);
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void DmgSlsTxop::Cancel() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("Transmission cancelled");
}

void DmgSlsTxop::EndTxNoAck() {
  NS_LOG_FUNCTION(this);
  if (!m_txOkNoAckCallback.IsNull()) {
    m_txOkNoAckCallback(m_currentHdr);
  }
}

} // namespace ns3
