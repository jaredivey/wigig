/*
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mirko Banchi <mk.banchi@gmail.com>
 *         Stefano Avallone <stavallo@unina.it>
 */

#include "wigig-msdu-aggregator.h"

#include "mac-low.h"
#include "wigig-mac-queue.h"
#include "wigig-mac.h"
#include "wigig-mpdu-aggregator.h"
#include "wigig-net-device.h"
#include "wigig-phy.h"
#include "wigig-qos-txop.h"
#include "wigig-remote-station-manager.h"

#include "ns3/amsdu-subframe-header.h"
#include "ns3/ht-capabilities.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/wifi-mac-trailer.h"

#include <algorithm>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigMsduAggregator");

NS_OBJECT_ENSURE_REGISTERED(WigigMsduAggregator);

TypeId WigigMsduAggregator::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMsduAggregator")
                          .SetParent<Object>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMsduAggregator>();
  return tid;
}

WigigMsduAggregator::WigigMsduAggregator() {}

WigigMsduAggregator::~WigigMsduAggregator() {}

void WigigMsduAggregator::SetEdcaQueues(EdcaQueues map) { m_edca = map; }

uint16_t WigigMsduAggregator::GetSizeIfAggregated(uint16_t msduSize,
                                                  uint16_t amsduSize) {
  NS_LOG_FUNCTION(msduSize << amsduSize);

  // the size of the A-MSDU subframe header is 14 bytes: DA (6), SA (6) and
  // Length (2)
  return amsduSize + CalculatePadding(amsduSize) + 14 + msduSize;
}

Ptr<WigigMacQueueItem> WigigMsduAggregator::GetNextAmsdu(
    Mac48Address recipient, uint8_t tid, const WigigTxVector &txVector,
    uint32_t ampduSize, Time ppduDurationLimit) const {
  NS_LOG_FUNCTION(recipient << +tid << txVector << ampduSize
                            << ppduDurationLimit);

  /* "The Address 1 field of an MPDU carrying an A-MSDU shall be set to an
   * individual address or to the GCR concealment address" (Section 10.12
   * of 802.11-2016)
   */
  NS_ABORT_MSG_IF(recipient.IsBroadcast(), "Recipient address is broadcast");

  Ptr<WigigQosTxop> qosWigigTxop = m_edca.find(QosUtilsMapTidToAc(tid))->second;
  Ptr<WigigMacQueue> queue = qosWigigTxop->GetWigigMacQueue();
  WigigMacQueue::ConstIterator peekedIt =
      queue->PeekByTidAndAddress(tid, recipient);

  if (peekedIt == queue->GetContainer().end()) {
    NS_LOG_DEBUG("No packet with the given TID and address in the queue");
    return nullptr;
  }

  /* "A STA shall not transmit an A-MSDU within a QoS Data frame under a block
   * ack agreement unless the recipient indicates support for A-MSDU by setting
   * the A-MSDU Supported field to 1 in its BlockAck Parameter Set field of the
   * ADDBA Response frame" (Section 10.12 of 802.11-2016)
   */
  // No check required for now, as we always set the A-MSDU Supported field to 1

  WifiModulationClass modulation = txVector.GetMode().GetModulationClass();

  // Get the maximum size of the A-MSDU we can send to the recipient
  uint16_t maxAmsduSize = GetMaxAmsduSize(recipient, tid, modulation);

  if (maxAmsduSize == 0) {
    return nullptr;
  }

  Ptr<WigigMacQueueItem> amsdu = Create<WigigMacQueueItem>(
      Create<const Packet>(), (*peekedIt)->GetHeader());
  uint8_t nMsdu = 0;
  // We need to keep track of the first MSDU. When it is processed, it is not
  // known if aggregation will succeed or not.
  WigigMacQueue::ConstIterator first = peekedIt;

  // TODO Add support for the Max Number Of MSDUs In A-MSDU field in the
  // Extended Capabilities element sent by the recipient

  while (peekedIt != queue->GetContainer().end()) {
    // check if aggregating the peeked MSDU violates the A-MSDU size limit
    uint16_t newAmsduSize = GetSizeIfAggregated(
        (*peekedIt)->GetPacket()->GetSize(), amsdu->GetPacket()->GetSize());

    if (newAmsduSize > maxAmsduSize) {
      NS_LOG_DEBUG(
          "No other MSDU can be aggregated: maximum A-MSDU size reached");
      break;
    }

    // check if the A-MSDU obtained by aggregating the peeked MSDU violates
    // the A-MPDU size limit or the PPDU duration limit
    if (!qosWigigTxop->GetLow()->IsWithinSizeAndTimeLimits(
            amsdu->GetHeader().GetSize() + newAmsduSize + WIFI_MAC_FCS_LENGTH,
            recipient, tid, txVector, ampduSize, ppduDurationLimit)) {
      NS_LOG_DEBUG("No other MSDU can be aggregated");
      break;
    }

    // The MSDU can be aggregated to the A-MSDU.
    // If it is the first MSDU, move to the next one
    if (nMsdu == 0) {
      amsdu = Copy(*peekedIt);
      peekedIt++;
    }
    // otherwise, remove it from the queue
    else {
      amsdu->Aggregate(*peekedIt);
      peekedIt = queue->Remove(peekedIt);
    }

    nMsdu++;

    peekedIt = queue->PeekByTidAndAddress(tid, recipient, peekedIt);
  }

  if (nMsdu < 2) {
    NS_LOG_DEBUG("Aggregation failed (could not aggregate at least two MSDUs)");
    return nullptr;
  }

  // Aggregation succeeded, we have to remove the first MSDU
  queue->Remove(first);

  return amsdu;
}

uint8_t WigigMsduAggregator::CalculatePadding(uint16_t amsduSize) {
  return (4 - (amsduSize % 4)) % 4;
}

uint16_t
WigigMsduAggregator::GetMaxAmsduSize(Mac48Address recipient, uint8_t tid,
                                     WifiModulationClass modulation) const {
  NS_LOG_FUNCTION(this << recipient << +tid << modulation);

  AcIndex ac = QosUtilsMapTidToAc(tid);
  Ptr<WigigQosTxop> qosWigigTxop = m_edca.find(ac)->second;
  Ptr<WigigNetDevice> device = DynamicCast<WigigNetDevice>(
      qosWigigTxop->GetLow()->GetPhy()->GetDevice());
  NS_ASSERT(device);
  Ptr<WigigRemoteStationManager> stationManager =
      device->GetRemoteStationManager();
  NS_ASSERT(stationManager);

  // Find the A-MSDU max size configured on this device
  UintegerValue size;

  switch (ac) {
  case AC_BE:
    device->GetMac()->GetAttribute("BE_MaxAmsduSize", size);
    break;
  case AC_BK:
    device->GetMac()->GetAttribute("BK_MaxAmsduSize", size);
    break;
  case AC_VI:
    device->GetMac()->GetAttribute("VI_MaxAmsduSize", size);
    break;
  case AC_VO:
    device->GetMac()->GetAttribute("VO_MaxAmsduSize", size);
    break;
  default:
    NS_ABORT_MSG("Unknown AC " << ac);
    return 0;
  }

  uint16_t maxAmsduSize = size.Get();

  if (maxAmsduSize == 0) {
    NS_LOG_DEBUG("A-MSDU Aggregation is disabled on this station for AC "
                 << ac);
    return 0;
  }

  // Retrieve the Capabilities elements advertised by the recipient
  auto dmgCapabilities = stationManager->GetStationDmgCapabilities(recipient);
  if (dmgCapabilities.has_value()) {
    /* In IEEE 802.11ad/ay, DMG STAs do not negotiate the maximum size of an
     * A-MSDU not the maximum length of an MPDU */
    return maxAmsduSize;
  }

  // the maximum A-MSDU size is indirectly constrained by the maximum PSDU size
  // supported by the recipient (see Table 9-19 of 802.11-2016)
  maxAmsduSize = std::min(maxAmsduSize, static_cast<uint16_t>(3839));

  return maxAmsduSize;
}

WigigMsduAggregator::DeaggregatedMsdus
WigigMsduAggregator::Deaggregate(Ptr<Packet> aggregatedPacket) {
  NS_LOG_FUNCTION_NOARGS();
  DeaggregatedMsdus set;

  AmsduSubframeHeader hdr;
  Ptr<Packet> extractedMsdu = Create<Packet>();
  uint32_t maxSize = aggregatedPacket->GetSize();
  uint16_t extractedLength;
  uint8_t padding;
  uint32_t deserialized = 0;

  while (deserialized < maxSize) {
    deserialized += aggregatedPacket->RemoveHeader(hdr);
    extractedLength = hdr.GetLength();
    extractedMsdu = aggregatedPacket->CreateFragment(
        0, static_cast<uint32_t>(extractedLength));
    aggregatedPacket->RemoveAtStart(extractedLength);
    deserialized += extractedLength;

    padding = (4 - ((extractedLength + 14) % 4)) % 4;

    if (padding > 0 && deserialized < maxSize) {
      aggregatedPacket->RemoveAtStart(padding);
      deserialized += padding;
    }

    std::pair<Ptr<const Packet>, AmsduSubframeHeader> packetHdr(extractedMsdu,
                                                                hdr);
    set.push_back(packetHdr);
  }
  NS_LOG_INFO("Deaggreated A-MSDU: extracted " << set.size() << " MSDUs");
  return set;
}

} // namespace ns3
