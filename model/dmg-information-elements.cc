/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "dmg-information-elements.h"

#include "ns3/address-utils.h"
#include "ns3/assert.h"

#include <cmath>

namespace ns3 {

Buffer::Iterator DeserializeExtensionElementId(Buffer::Iterator i,
                                               uint8_t &elementID,
                                               uint8_t &length,
                                               uint8_t &extElementId) {
  Buffer::Iterator start = i;
  elementID = i.ReadU8();
  if (elementID != IE_EXTENSION) {
    return start;
  }
  length = i.ReadU8();
  extElementId = i.ReadU8();
  return i;
}

Buffer::Iterator DeserializeElementId(Buffer::Iterator i, uint8_t &elementID,
                                      uint8_t &length) {
  elementID = i.ReadU8();
  length = i.ReadU8();
  return i;
}

/***************************************************
 *             Request Element 8.4.2.51
 ****************************************************/

RequestElement::RequestElement() {}

WifiInformationElementId RequestElement::ElementId() const {
  return IE_REQUEST;
}

uint16_t RequestElement::GetInformationFieldSize() const {
  return m_list.size();
}

void RequestElement::SerializeInformationField(Buffer::Iterator start) const {
  for (WifiInformationElementIdList::const_iterator i = m_list.begin();
       i != m_list.end(); i++) {
    start.WriteU8(i->first);
  }
}

uint16_t RequestElement::DeserializeInformationField(Buffer::Iterator start,
                                                     uint16_t length) {
  WifiInformationElementId id;
  while (!start.IsEnd()) {
    id = start.ReadU8();
    m_list.emplace_back(std::make_pair(id, 0));
  }
  return length;
}

void RequestElement::AddRequestElementId(WifiInfoElementId id) {
  m_list.push_back(id);
}

void RequestElement::AddListOfRequestedElementId(
    WifiInformationElementIdList &list) {
  m_list = list;
}

WifiInformationElementIdList
RequestElement::GetWifiInformationElementIdList() const {
  return m_list;
}

size_t RequestElement::GetNumberOfRequestedIEs() const { return m_list.size(); }

ATTRIBUTE_HELPER_CPP(RequestElement);

std::ostream &operator<<(std::ostream &os, const RequestElement &element) {
  WifiInformationElementIdList list = element.GetWifiInformationElementIdList();
  for (WifiInformationElementIdList::const_iterator i = list.begin();
       i != list.end() - 1; i++) {
    os << i->first << "|";
  }
  os << list[list.size() - 1].first;
  return os;
}

std::istream &operator>>(std::istream &is, RequestElement &element) {
  WifiInformationElementId c1;
  is >> c1;

  element.AddRequestElementId(std::make_pair(c1, 0));

  return is;
}

/***************************************************
 *             TSPEC Element (8.4.2.32)
 ****************************************************/

TspecElement::TspecElement()
    : m_trafficType(TRAFFIC_TYPE_PERIODIC_TRAFFIC_PATTERN), m_tsid(0),
      m_dataDirection(DATA_DIRECTION_UPLINK),
      m_accessPolicy(ACCESS_POLICY_RESERVED), m_aggregation(0), m_apsd(0),
      m_userPriority(0), m_tsInfoAckPolicy(ACK_POLICY_RESERVED), m_schedule(0),
      m_nominalMsduSize(0), m_maximumMsduSize(0), m_minimumServiceInterval(0),
      m_maximumServiceInterval(0), m_inactivityInterval(0),
      m_suspensionInterval(0), m_serviceStartTime(0), m_minimumDateRate(0),
      m_meanDateRate(0), m_peakDateRate(0), m_burstSize(0), m_delayBound(0),
      m_minimumPhyRate(0), m_surplusBandwidthAllowance(0), m_mediumTime(0),
      m_allocationId(0), m_allocationDirection(ALLOCATION_DIRECTION_SOURCE),
      m_amsduSubframe(0), m_reliabilityIndex(0) {}

WifiInformationElementId TspecElement::ElementId() const { return IE_TSPEC; }

uint16_t TspecElement::GetInformationFieldSize() const { return 57; }

void TspecElement::SerializeInformationField(Buffer::Iterator start) const {
  uint16_t tsInfoField1 = 0;
  uint8_t tsInfoField2;
  tsInfoField1 |= m_trafficType & 0x1;
  tsInfoField1 |= (m_tsid & 0xF) << 1;
  tsInfoField1 |= (m_dataDirection & 0x3) << 5;
  tsInfoField1 |= (m_accessPolicy & 0x3) << 7;
  tsInfoField1 |= (m_aggregation & 0x1) << 9;
  tsInfoField1 |= (m_apsd & 0x1) << 10;
  tsInfoField1 |= (m_userPriority & 0x7) << 11;
  tsInfoField1 |= (m_tsInfoAckPolicy & 0x3) << 14;
  tsInfoField2 = m_schedule & 0x1;

  start.WriteHtolsbU16(tsInfoField1);
  start.WriteU8(tsInfoField2);
  start.WriteHtolsbU16(m_nominalMsduSize);
  start.WriteHtolsbU16(m_maximumMsduSize);
  start.WriteHtolsbU32(m_minimumServiceInterval);
  start.WriteHtolsbU32(m_maximumServiceInterval);
  start.WriteHtolsbU32(m_inactivityInterval);
  start.WriteHtolsbU32(m_suspensionInterval);
  start.WriteHtolsbU32(m_serviceStartTime);
  start.WriteHtolsbU32(m_minimumDateRate);
  start.WriteHtolsbU32(m_meanDateRate);
  start.WriteHtolsbU32(m_peakDateRate);
  start.WriteHtolsbU32(m_burstSize);
  start.WriteHtolsbU32(m_delayBound);
  start.WriteHtolsbU32(m_minimumPhyRate);
  start.WriteHtolsbU16(m_surplusBandwidthAllowance);
  start.WriteHtolsbU16(m_mediumTime);
  start.WriteHtolsbU16(GetDmgAttributesField());
}

uint16_t TspecElement::DeserializeInformationField(Buffer::Iterator start,
                                                   uint16_t length) {
  Buffer::Iterator i = start;
  uint16_t tsInfoField1 = 0;
  uint8_t tsInfoField2;
  tsInfoField1 = i.ReadLsbtohU16();
  m_trafficType = tsInfoField1 & 0x1;
  m_tsid = (tsInfoField1 >> 1) & 0xF;
  m_dataDirection = (tsInfoField1 >> 5) & 0x3;
  m_accessPolicy = (tsInfoField1 >> 7) & 0x3;
  m_aggregation = (tsInfoField1 >> 9) & 0x1;
  m_apsd = (tsInfoField1 >> 10) & 0x1;
  m_userPriority = (tsInfoField1 >> 11) & 0x7;
  m_tsInfoAckPolicy = (tsInfoField1 >> 14) & 0x3;
  tsInfoField2 = i.ReadU8();
  m_schedule = tsInfoField2 & 0x1;

  m_nominalMsduSize = i.ReadLsbtohU16();
  m_maximumMsduSize = i.ReadLsbtohU16();
  m_minimumServiceInterval = i.ReadLsbtohU32();
  m_maximumServiceInterval = i.ReadLsbtohU32();
  m_inactivityInterval = i.ReadLsbtohU32();
  m_suspensionInterval = i.ReadLsbtohU32();
  m_serviceStartTime = i.ReadLsbtohU32();
  m_minimumDateRate = i.ReadLsbtohU32();
  m_meanDateRate = i.ReadLsbtohU32();
  m_peakDateRate = i.ReadLsbtohU32();
  m_burstSize = i.ReadLsbtohU32();
  m_delayBound = i.ReadLsbtohU32();
  m_minimumPhyRate = i.ReadLsbtohU32();
  m_surplusBandwidthAllowance = i.ReadLsbtohU16();
  m_mediumTime = i.ReadLsbtohU16();
  SetDmgAttributesField(i.ReadLsbtohU16());

  return length;
}

void TspecElement::SetDmgAttributesField(uint16_t infieldfo) {
  m_allocationId = infieldfo & 0xF;
  m_allocationDirection = (infieldfo >> 6) & 0x1;
  m_amsduSubframe = (infieldfo >> 7) & 0x1;
  m_reliabilityIndex = (infieldfo >> 8) & 0x3;
}

uint16_t TspecElement::GetDmgAttributesField() const {
  uint16_t dmgAttributes = 0;
  dmgAttributes |= m_allocationId & 0xF;
  dmgAttributes |= (m_allocationDirection & 0x1) << 6;
  dmgAttributes |= (m_amsduSubframe & 0x1) << 7;
  dmgAttributes |= (m_reliabilityIndex & 0x3) << 8;
  return dmgAttributes;
}

void TspecElement::SetTrafficType(TrafficType type) {
  m_trafficType = static_cast<uint8_t>(type);
}

void TspecElement::SetTsId(uint8_t id) { m_tsid = id; }

void TspecElement::SetDataDirection(DataDirection direction) {
  m_dataDirection = static_cast<uint8_t>(direction);
}

void TspecElement::SetAccessPolicy(AccessPolicy policy) {
  m_accessPolicy = static_cast<uint8_t>(policy);
}

void TspecElement::SetAggregation(bool enable) { m_aggregation = enable; }

void TspecElement::SetApsd(bool enable) { m_apsd = enable; }

void TspecElement::SetUserPriority(uint8_t priority) {
  m_userPriority = priority;
}

void TspecElement::SetTsInfoAckPolicy(TsInfoAckPolicy policy) {
  m_tsInfoAckPolicy = static_cast<uint8_t>(policy);
}

void TspecElement::SetSchedule(bool schedule) { m_schedule = schedule; }

void TspecElement::SetNominalMsduSize(uint16_t size, bool fixed) {
  m_nominalMsduSize = static_cast<uint8_t>(fixed) << 14;
  m_nominalMsduSize |= size & 0x7FFF;
}

void TspecElement::SetMaximumMsduSize(uint16_t size) {
  m_maximumMsduSize = size;
}

void TspecElement::SetMinimumServiceInterval(uint32_t interval) {
  m_minimumServiceInterval = interval;
}

void TspecElement::SetMaximumServiceInterval(uint32_t interval) {
  m_maximumServiceInterval = interval;
}

void TspecElement::SetInactivityInterval(uint32_t interval) {
  m_inactivityInterval = interval;
}

void TspecElement::SetSuspensionInterval(uint32_t interval) {
  m_suspensionInterval = interval;
}

void TspecElement::SetServiceStartTime(uint32_t time) {
  m_serviceStartTime = time;
}

void TspecElement::SetMinimumDataRate(uint32_t rate) {
  m_minimumDateRate = rate;
}

void TspecElement::SetMeanDataRate(uint32_t rate) { m_meanDateRate = rate; }

void TspecElement::SetPeakDataRate(uint32_t rate) { m_peakDateRate = rate; }

void TspecElement::SetBurstSize(uint32_t size) { m_burstSize = size; }

void TspecElement::SetDelayBound(uint32_t bound) { m_delayBound = bound; }

void TspecElement::SetMinimumPhyRate(uint32_t rate) { m_minimumPhyRate = rate; }

void TspecElement::SetSurplusBandwidthAllowance(uint16_t allowance) {
  m_surplusBandwidthAllowance = allowance;
}

void TspecElement::SetMediumTime(uint16_t time) { m_mediumTime = time; }

void TspecElement::SetAllocationId(uint8_t id) { m_allocationId = id; }

void TspecElement::SetAllocationDirection(AllocationDirection dircetion) {
  m_allocationDirection = static_cast<AllocationDirection>(dircetion);
}

void TspecElement::SetAmsduSubframe(uint8_t amsdu) { m_amsduSubframe = amsdu; }

void TspecElement::SetReliabilityIndex(uint8_t index) {
  m_reliabilityIndex = index;
}

TrafficType TspecElement::GetTrafficType() const {
  return static_cast<TrafficType>(m_trafficType);
}

uint8_t TspecElement::GetTsId() const { return m_tsid; }

DataDirection TspecElement::GetDataDirection() const {
  return static_cast<DataDirection>(m_dataDirection);
}

AccessPolicy TspecElement::GetAccessPolicy() const {
  return static_cast<AccessPolicy>(m_accessPolicy);
}

bool TspecElement::GetAggregation() const { return m_aggregation; }

bool TspecElement::GetApsd() const { return m_apsd; }

uint8_t TspecElement::GetUserPriority() const { return m_userPriority; }

TsInfoAckPolicy TspecElement::GetTsInfoAckPolicy() const {
  return static_cast<TsInfoAckPolicy>(m_tsInfoAckPolicy);
}

bool TspecElement::GetSchedule() const { return m_schedule; }

uint16_t TspecElement::GetNominalMsduSize() const {
  return m_nominalMsduSize & 0x3FFF;
}

bool TspecElement::IsMsduSizeFixed() const {
  bool fixed = static_cast<bool>((m_nominalMsduSize >> 14) & 0x1);
  return fixed;
}

uint16_t TspecElement::GetMaximumMsduSize() const { return m_maximumMsduSize; }

uint32_t TspecElement::GetMinimumServiceInterval() const {
  return m_minimumServiceInterval;
}

uint32_t TspecElement::GetMaximumServiceInterval() const {
  return m_maximumServiceInterval;
}

uint32_t TspecElement::GetInactivityInterval() const {
  return m_inactivityInterval;
}

uint32_t TspecElement::GetSuspensionInterval() const {
  return m_suspensionInterval;
}

uint32_t TspecElement::GetServiceStartTime() const {
  return m_serviceStartTime;
}

uint32_t TspecElement::GetMinimumDataRate() const { return m_minimumDateRate; }

uint32_t TspecElement::GetMeanDataRate() const { return m_meanDateRate; }

uint32_t TspecElement::GetPeakDataRate() const { return m_peakDateRate; }

uint32_t TspecElement::GetBurstSize() const { return m_burstSize; }

uint32_t TspecElement::GetDelayBound() const { return m_delayBound; }

uint32_t TspecElement::GetMinimumPhyRate() const { return m_minimumPhyRate; }

uint16_t TspecElement::GetSurplusBandwidthAllowance() const {
  return m_surplusBandwidthAllowance;
}

uint16_t TspecElement::GetMediumTime() const { return m_mediumTime; }

uint8_t TspecElement::GetAllocationId() const { return m_allocationId; }

AllocationDirection TspecElement::GetAllocationDirection() const {
  return static_cast<AllocationDirection>(m_allocationDirection);
}

uint8_t TspecElement::GetAmsduSubframe() const { return m_amsduSubframe; }

uint8_t TspecElement::GetReliabilityIndex() const { return m_reliabilityIndex; }

void TspecElement::Print(std::ostream &os) const { os << *this; }

ATTRIBUTE_HELPER_CPP(TspecElement);

std::ostream &operator<<(std::ostream &os, const TspecElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, TspecElement &element) {
  uint32_t c1;
  is >> c1;
  return is;
}

/***************************************************
 *         Traffic Stream (TS) Delay 8.4.2.34
 ****************************************************/

TsDelayElement::TsDelayElement() : m_delay(0) {}

WifiInformationElementId TsDelayElement::ElementId() const {
  return IE_TS_DELAY;
}

uint16_t TsDelayElement::GetInformationFieldSize() const { return 4; }

void TsDelayElement::SerializeInformationField(Buffer::Iterator start) const {
  start.WriteHtolsbU32(m_delay);
}

uint16_t TsDelayElement::DeserializeInformationField(Buffer::Iterator start,
                                                     uint16_t length) {
  Buffer::Iterator i = start;
  m_delay = i.ReadLsbtohU32();
  return length;
}

void TsDelayElement::SetDelay(uint32_t value) { m_delay = value; }

uint32_t TsDelayElement::GetDelay() const { return m_delay; }

ATTRIBUTE_HELPER_CPP(TsDelayElement);

std::ostream &operator<<(std::ostream &os, const TsDelayElement &element) {
  os << element.GetDelay();
  return os;
}

std::istream &operator>>(std::istream &is, TsDelayElement &element) {
  uint32_t c1;
  is >> c1;
  element.SetDelay(c1);
  return is;
}

/***************************************************
 *           DMG Information 8.4.2.131
 ****************************************************/

DmgOperationElement::DmgOperationElement()
    : m_tddti(0), m_pseudo(0), m_handover(0), m_psRequestSuspensionInterval(0),
      m_minBhiDuration(0), m_broadcastSTAInfoDuration(0),
      m_assocRespConfirmTime(0), m_minPPDuration(0), m_spIdleTimeout(0),
      m_maxLostBeacons(0) {}

WifiInformationElementId DmgOperationElement::ElementId() const {
  return IE_DMG_OPERATION;
}

uint16_t DmgOperationElement::GetInformationFieldSize() const { return 10; }

void DmgOperationElement::SerializeInformationField(
    Buffer::Iterator start) const {
  start.WriteHtolsbU16(GetDmgOperationInformation());
  start.WriteHtolsbU64(GetDmgBssParameterConfiguration());
}

uint16_t
DmgOperationElement::DeserializeInformationField(Buffer::Iterator start,
                                                 uint16_t length) {
  Buffer::Iterator i = start;
  uint32_t operation = i.ReadLsbtohU16();
  uint64_t config = i.ReadLsbtohU64();

  SetDmgOperationInformation(operation);
  SetDmgBssParameterConfiguration(config);

  return length;
}

void DmgOperationElement::SetTddti(bool tddti) { m_tddti = tddti; }

void DmgOperationElement::SetPseudoStaticAllocations(bool pseudoStatic) {
  m_pseudo = pseudoStatic;
}

void DmgOperationElement::SetPcpHandover(bool handover) {
  m_handover = handover;
}

bool DmgOperationElement::GetTddti() const { return m_tddti; }

bool DmgOperationElement::GetPseudoStaticAllocations() const {
  return m_pseudo;
}

bool DmgOperationElement::GetPcpHandover() const { return m_handover; }

void DmgOperationElement::SetDmgOperationInformation(uint16_t info) {
  m_tddti = info & 0x1;
  m_pseudo = (info >> 1) & 0x1;
  m_handover = (info >> 2) & 0x1;
}

uint16_t DmgOperationElement::GetDmgOperationInformation() const {
  uint16_t val = 0;

  val |= (m_tddti & 0x1);
  val |= (m_pseudo & 0x1) << 1;
  val |= (m_handover & 0x1) << 2;

  return val;
}

void DmgOperationElement::SetPSRequestSuspensionInterval(uint8_t interval) {
  m_psRequestSuspensionInterval = interval;
}

void DmgOperationElement::SetMinBhiDuration(uint16_t duration) {
  m_minBhiDuration = duration;
}

void DmgOperationElement::SetBroadcastSTAInfoDuration(uint8_t duration) {
  m_broadcastSTAInfoDuration = duration;
}

void DmgOperationElement::SetAssocRespConfirmTime(uint8_t time) {
  m_assocRespConfirmTime = time;
}

void DmgOperationElement::SetMinPPDuration(uint8_t duration) {
  m_minPPDuration = duration;
}

void DmgOperationElement::SetSPIdleTimeout(uint8_t timeout) {
  m_spIdleTimeout = timeout;
}

void DmgOperationElement::SetMaxLostBeacons(uint8_t max) {
  m_maxLostBeacons = max;
}

uint8_t DmgOperationElement::GetPsRequestSuspensionInterval() const {
  return m_psRequestSuspensionInterval;
}

uint16_t DmgOperationElement::GetMinBhiDuration() const {
  return m_minBhiDuration;
}

uint8_t DmgOperationElement::GetBroadcastSTAInfoDuration() const {
  return m_broadcastSTAInfoDuration;
}

uint8_t DmgOperationElement::GetAssocRespConfirmTime() const {
  return m_assocRespConfirmTime;
}

uint8_t DmgOperationElement::GetMinPPDuration() const {
  return m_minPPDuration;
}

uint8_t DmgOperationElement::GetSpIdleTimeout() const {
  return m_spIdleTimeout;
}

uint8_t DmgOperationElement::GetMaxLostBeacons() const {
  return m_maxLostBeacons;
}

void DmgOperationElement::SetDmgBssParameterConfiguration(uint64_t config) {
  m_psRequestSuspensionInterval = config & 0xFF;
  m_minBhiDuration = (config >> 8) & 0xFFFF;
  m_broadcastSTAInfoDuration = (config >> 24) & 0xFF;
  m_assocRespConfirmTime = (config >> 32) & 0xFF;
  m_minPPDuration = (config >> 40) & 0xFF;
  m_spIdleTimeout = (config >> 48) & 0xFF;
  m_maxLostBeacons = (config >> 56) & 0xFF;
}

uint64_t DmgOperationElement::GetDmgBssParameterConfiguration() const {
  uint64_t val = 0;

  val |= static_cast<uint64_t>(m_psRequestSuspensionInterval);
  val |= static_cast<uint64_t>(m_minBhiDuration) << 8;
  val |= static_cast<uint64_t>(m_broadcastSTAInfoDuration) << 24;
  val |= static_cast<uint64_t>(m_assocRespConfirmTime) << 32;
  val |= static_cast<uint64_t>(m_minPPDuration) << 40;
  val |= static_cast<uint64_t>(m_spIdleTimeout) << 48;
  val |= static_cast<uint64_t>(m_maxLostBeacons) << 56;

  return val;
}

void DmgOperationElement::Print(std::ostream &os) const { os << *this; }

ATTRIBUTE_HELPER_CPP(DmgOperationElement);

std::ostream &operator<<(std::ostream &os, const DmgOperationElement &element) {
  os << element.GetDmgOperationInformation() << "|"
     << element.GetDmgBssParameterConfiguration();

  return os;
}

std::istream &operator>>(std::istream &is, DmgOperationElement &element) {
  uint16_t c1;
  uint64_t c2;
  is >> c1 >> c2;
  element.SetDmgOperationInformation(c1);
  element.SetDmgBssParameterConfiguration(c2);

  return is;
}

/***************************************************
 *       DMG Beam Refinement Element 8.4.2.132
 ****************************************************/

BeamRefinementElement::BeamRefinementElement()
    : m_initiator(false), m_txTrainResponse(false), m_rxTrainResponse(false),
      m_txTrnOk(false), m_txssFbckReq(false), m_bsFbck(0), m_bsFbckAntennaId(0),
      m_snrRequested(false), m_channelMeasurementRequested(false),
      m_numberOfTapsRequested(TAPS_1), m_sectorIdOrderRequested(false),
      m_snrPresent(false), m_channelMeasurementPresent(false),
      m_tapDelayPresent(false), m_numberOfTapsPresent(TAPS_1),
      m_numberOfMeasurements(0), m_sectorIdOrderPresent(false),
      m_linkType(false), m_antennaType(false), m_numberOfBeams(0),
      m_midExtension(false), m_capabilityRequest(false) {}

WifiInformationElementId BeamRefinementElement::ElementId() const {
  return IE_DMG_BEAM_REFINEMENT;
}

uint16_t BeamRefinementElement::GetInformationFieldSize() const { return 8; }

void BeamRefinementElement::SerializeInformationField(
    Buffer::Iterator start) const {
  uint32_t value1 = 0;
  uint8_t value2 = 0;

  value1 |= (m_initiator & 0x1);
  value1 |= (m_txTrainResponse & 0x1) << 1;
  value1 |= (m_rxTrainResponse & 0x1) << 2;
  value1 |= (m_txTrnOk & 0x1) << 3;
  value1 |= (m_txssFbckReq & 0x1) << 4;
  value1 |= (m_bsFbck & 0x3F) << 5;
  value1 |= (m_bsFbckAntennaId & 0x3) << 11;
  /* FBCK-REQ */
  value1 |= (m_snrRequested & 0x1) << 13;
  value1 |= (m_channelMeasurementRequested & 0x1) << 14;
  value1 |= (static_cast<uint8_t>(m_numberOfTapsRequested) & 0x3) << 15;
  value1 |= (m_sectorIdOrderRequested & 0x1) << 17;
  /* FBCK-TYPE */
  value1 |= (m_snrPresent & 0x1) << 18;
  value1 |= (m_channelMeasurementPresent & 0x1) << 19;
  value1 |= (m_tapDelayPresent & 0x1) << 20;
  value1 |= (static_cast<uint8_t>(m_numberOfTapsPresent) & 0x3) << 21;
  value1 |= (m_numberOfMeasurements & 0x7F) << 23;
  value1 |= (m_sectorIdOrderPresent & 0x1) << 30;
  value1 |= (m_linkType & 0x1) << 31;
  value2 |= (m_antennaType & 0x1);
  value2 |= (m_numberOfBeams & 0x7) << 1;

  value2 |= (m_midExtension & 0x1) << 4;
  value2 |= (m_capabilityRequest & 0x1) << 5;

  start.WriteHtolsbU32(value1);
  start.WriteU8(value2);
  start.WriteHtolsbU16(0);
  start.WriteU8(0);
}

uint16_t
BeamRefinementElement::DeserializeInformationField(Buffer::Iterator start,
                                                   uint16_t length) {
  Buffer::Iterator i = start;
  uint32_t value1 = i.ReadLsbtohU32();
  uint8_t value2 = i.ReadU8();
  i.ReadLsbtohU16();
  i.ReadU8();

  m_initiator = value1 & 0x1;
  m_txTrainResponse = (value1 >> 1) & 0x1;
  m_rxTrainResponse = (value1 >> 2) & 0x1;
  m_txTrnOk = (value1 >> 3) & 0x1;
  m_txssFbckReq = (value1 >> 4) & 0x1;
  m_bsFbck = (value1 >> 5) & 0x3F;
  m_bsFbckAntennaId = (value1 >> 11) & 0x3;
  /* FBCK-REQ */
  m_snrRequested = (value1 >> 13) & 0x1;
  m_channelMeasurementRequested = (value1 >> 14) & 0x1;
  m_numberOfTapsRequested = static_cast<NumTaps>((value1 >> 15) & 0x3);
  m_sectorIdOrderRequested = (value1 >> 17) & 0x1;
  /* FBCK-TYPE */
  m_snrPresent = (value1 >> 18) & 0x1;
  m_channelMeasurementPresent = (value1 >> 19) & 0x1;
  m_tapDelayPresent = (value1 >> 20) & 0x1;
  m_numberOfTapsPresent = static_cast<NumTaps>((value1 >> 21) & 0x3);
  m_numberOfMeasurements = (value1 >> 23) & 0x7F;
  m_sectorIdOrderPresent = (value1 >> 30) & 0x1;
  m_linkType = (value1 >> 31) & 0x1;
  m_antennaType = value2 & 0x1;
  m_numberOfBeams = (value2 >> 1) & 0x7;

  m_midExtension = (value2 >> 4) & 0x1;
  m_capabilityRequest = (value2 >> 5) & 0x1;

  return length;
}

void BeamRefinementElement::SetAsBeamRefinementInitiator(bool initiator) {
  m_initiator = initiator;
}

void BeamRefinementElement::SetTxTrainResponse(bool response) {
  m_txTrainResponse = response;
}

void BeamRefinementElement::SetRxTrainResponse(bool response) {
  m_rxTrainResponse = response;
}

void BeamRefinementElement::SetTxTrnOk(bool value) { m_txTrnOk = value; }

void BeamRefinementElement::SetTxssFbckReq(bool feedback) {
  m_txssFbckReq = feedback;
}

void BeamRefinementElement::SetBsFbck(uint8_t index) { m_bsFbck = index; }

void BeamRefinementElement::SetBsFbckAntennaId(uint8_t id) {
  m_bsFbckAntennaId = id;
}

/* FBCK-REQ field */
void BeamRefinementElement::SetSnrRequested(bool requested) {
  m_snrRequested = requested;
}

void BeamRefinementElement::SetChannelMeasurementRequested(bool requested) {
  m_channelMeasurementRequested = requested;
}

void BeamRefinementElement::SetNumberOfTapsRequested(NumTaps number) {
  m_numberOfTapsRequested = number;
}

void BeamRefinementElement::SetSectorIdOrderRequested(bool present) {
  m_sectorIdOrderRequested = present;
}

bool BeamRefinementElement::IsSnrRequested() const { return m_snrRequested; }

bool BeamRefinementElement::IsChannelMeasurementRequested() const {
  return m_channelMeasurementRequested;
}

NumTaps BeamRefinementElement::GetNumberOfTapsRequested() const {
  return m_numberOfTapsRequested;
}

bool BeamRefinementElement::IsSectorIdOrderRequested() const {
  return m_sectorIdOrderRequested;
}

/* FBCK-TYPE field */
void BeamRefinementElement::SetSnrPresent(bool present) {
  m_snrPresent = present;
}

void BeamRefinementElement::SetChannelMeasurementPresent(bool present) {
  m_channelMeasurementPresent = present;
}

void BeamRefinementElement::SetTapDelayPresent(bool present) {
  m_tapDelayPresent = present;
}

void BeamRefinementElement::SetNumberOfTapsPresent(NumTaps number) {
  m_numberOfTapsPresent = number;
}

void BeamRefinementElement::SetNumberOfMeasurements(uint8_t number) {
  m_numberOfMeasurements = number;
}

void BeamRefinementElement::SetSectorIdOrderPresent(bool present) {
  m_sectorIdOrderPresent = present;
}

void BeamRefinementElement::SetLinkType(bool linkType) {
  m_linkType = linkType;
}

void BeamRefinementElement::SetAntennaType(bool type) { m_antennaType = type; }

void BeamRefinementElement::SetNumberOfBeams(uint8_t number) {
  m_numberOfBeams = number;
}

void BeamRefinementElement::SetExtendedBsFbck(uint16_t index) {
  m_bsFbck = index & 0x3F;
}

void BeamRefinementElement::SetExtendedBsFbckAntennaId(uint8_t id) {
  m_bsFbckAntennaId = id & 0x3;
}

void BeamRefinementElement::SetExtendedNumberOfMeasurements(uint16_t number) {
  m_numberOfMeasurements = number & 0x7F;
}

bool BeamRefinementElement::IsSnrPresent() const { return m_snrPresent; }

bool BeamRefinementElement::IsChannelMeasurementPresent() const {
  return m_channelMeasurementPresent;
}

bool BeamRefinementElement::IsTapDelayPresent() const {
  return m_tapDelayPresent;
}

NumTaps BeamRefinementElement::GetNumberOfTapsPresent() const {
  return m_numberOfTapsPresent;
}

uint8_t BeamRefinementElement::GetNumberOfMeasurements() const {
  return m_numberOfMeasurements;
}

bool BeamRefinementElement::IsSectorIdOrderPresent() const {
  return m_sectorIdOrderPresent;
}

bool BeamRefinementElement::GetLinkType() const { return m_linkType; }

bool BeamRefinementElement::GetAntennaType() const { return m_antennaType; }

uint8_t BeamRefinementElement::GetNumberOfBeams() const {
  return m_numberOfBeams;
}

void BeamRefinementElement::SetMidExtension(bool mid) { m_midExtension = mid; }

void BeamRefinementElement::SetCapabilityRequest(bool request) {
  m_capabilityRequest = request;
}

bool BeamRefinementElement::IsBeamRefinementInitiator() const {
  return m_initiator;
}

bool BeamRefinementElement::IsTxTrainResponse() const {
  return m_txTrainResponse;
}

bool BeamRefinementElement::IsRxTrainResponse() const {
  return m_rxTrainResponse;
}

bool BeamRefinementElement::IsTxTrnOk() const { return m_txTrnOk; }

bool BeamRefinementElement::IsTxssFbckReq() const { return m_txssFbckReq; }

uint8_t BeamRefinementElement::GetBsFbck() const { return m_bsFbck; }

uint8_t BeamRefinementElement::GetBsFbckAntennaId() const {
  return m_bsFbckAntennaId;
}

bool BeamRefinementElement::IsMidExtension() const { return m_midExtension; }

bool BeamRefinementElement::IsCapabilityRequest() const {
  return m_capabilityRequest;
}

uint16_t BeamRefinementElement::GetExtendedBsFbck() const {
  uint16_t bsFbck = 0;
  bsFbck |= (m_bsFbck & 0x3F);
  return bsFbck;
}

uint8_t BeamRefinementElement::GetExtendedBsFbckAntennaId() const {
  uint8_t bsFbckAntennaId = 0;
  bsFbckAntennaId |= (m_bsFbckAntennaId & 0x3);
  return bsFbckAntennaId;
}

uint16_t BeamRefinementElement::GetExtendedNumberOfMeasurements() const {
  uint16_t numberOfMeasurements = 0;
  numberOfMeasurements |= (m_numberOfMeasurements & 0x7F);
  return numberOfMeasurements;
}

void BeamRefinementElement::Print(std::ostream &os) const { os << *this; }

ATTRIBUTE_HELPER_CPP(BeamRefinementElement);

std::ostream &operator<<(std::ostream &os,
                         const BeamRefinementElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, BeamRefinementElement &element) {
  return is;
}

/******************************************
 *    Allocation Field Format (8-401aa)
 *******************************************/

AllocationField::AllocationField()
    : m_allocationId(0), m_allocationType(0), m_pseudoStatic(false),
      m_truncatable(false), m_extendable(false), m_pcpActive(false),
      m_lpScUsed(false), m_SourceAid(0), m_destinationAid(0),
      m_allocationStart(0), m_allocationBlockDuration(0), m_numberOfBlocks(0),
      m_allocationBlockPeriod(0), m_allocationAnnounced(false) {}

void AllocationField::Print(std::ostream &os) const {}

uint32_t AllocationField::GetSerializedSize() const { return 15; }

Buffer::Iterator AllocationField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;

  i.WriteHtolsbU16(GetAllocationControl());
  i = m_bfControl.Serialize(i);
  i.WriteU8(m_SourceAid);
  i.WriteU8(m_destinationAid);
  i.WriteHtolsbU32(m_allocationStart);
  i.WriteHtolsbU16(m_allocationBlockDuration);
  i.WriteU8(m_numberOfBlocks);
  i.WriteHtolsbU16(m_allocationBlockPeriod);

  return i;
}

Buffer::Iterator AllocationField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  SetAllocationControl(i.ReadLsbtohU16());
  i = m_bfControl.Deserialize(i);
  m_SourceAid = i.ReadU8();
  m_destinationAid = i.ReadU8();
  m_allocationStart = i.ReadLsbtohU32();
  m_allocationBlockDuration = i.ReadLsbtohU16();
  m_numberOfBlocks = i.ReadU8();
  m_allocationBlockPeriod = i.ReadLsbtohU16();

  return i;
}

void AllocationField::SetAllocationId(AllocationId id) { m_allocationId = id; }

void AllocationField::SetAllocationType(AllocationType type) {
  m_allocationType = type;
}

void AllocationField::SetAsPseudoStatic(bool value) { m_pseudoStatic = value; }

void AllocationField::SetAsTruncatable(bool value) { m_truncatable = value; }

void AllocationField::SetAsExtendable(bool value) { m_extendable = value; }

void AllocationField::SetPcpActive(bool value) { m_pcpActive = value; }

void AllocationField::SetLpScUsed(bool value) { m_lpScUsed = value; }

AllocationId AllocationField::GetAllocationId() const { return m_allocationId; }

AllocationType AllocationField::GetAllocationType() const {
  return static_cast<AllocationType>(m_allocationType);
}

bool AllocationField::IsPseudoStatic() const { return m_pseudoStatic; }

bool AllocationField::IsTruncatable() const { return m_truncatable; }

bool AllocationField::IsExtendable() const { return m_extendable; }

bool AllocationField::IsPcpActive() const { return m_pcpActive; }

bool AllocationField::IsLpScUsed() const { return m_lpScUsed; }

void AllocationField::SetAllocationControl(uint16_t ctrl) {
  m_allocationId = ctrl & 0xF;
  m_allocationType = (ctrl >> 4) & 0x7;
  m_pseudoStatic = (ctrl >> 7) & 0x1;
  m_truncatable = (ctrl >> 8) & 0x1;
  m_extendable = (ctrl >> 9) & 0x1;
  m_pcpActive = (ctrl >> 10) & 0x1;
  m_lpScUsed = (ctrl >> 11) & 0x1;
}

uint16_t AllocationField::GetAllocationControl() const {
  uint16_t val = 0;
  val |= m_allocationId & 0xF;
  val |= (m_allocationType & 0x7) << 4;
  val |= (m_pseudoStatic & 0x1) << 7;
  val |= (m_truncatable & 0x1) << 8;
  val |= (m_extendable & 0x1) << 9;
  val |= (m_pcpActive & 0x1) << 10;
  val |= (m_lpScUsed & 0x1) << 11;
  return val;
}

void AllocationField::SetBfControl(BfControlField &field) {
  m_bfControl = field;
}

void AllocationField::SetSourceAid(uint8_t aid) { m_SourceAid = aid; }

void AllocationField::SetDestinationAid(uint8_t aid) { m_destinationAid = aid; }

void AllocationField::SetAllocationStart(uint32_t start) {
  m_allocationStart = start;
}

void AllocationField::SetAllocationBlockDuration(uint16_t duration) {
  if (m_allocationType == SERVICE_PERIOD_ALLOCATION) {
    NS_ASSERT(1 <= duration && duration <= 32767);
  } else {
    NS_ASSERT(1 <= duration && duration <= 65535);
  }
  m_allocationBlockDuration = duration;
}

void AllocationField::SetNumberOfBlocks(uint8_t number) {
  m_numberOfBlocks = number;
}

void AllocationField::SetAllocationBlockPeriod(uint16_t period) {
  m_allocationBlockPeriod = period;
}

BfControlField AllocationField::GetBfControl() const { return m_bfControl; }

uint8_t AllocationField::GetSourceAid() const { return m_SourceAid; }

uint8_t AllocationField::GetDestinationAid() const { return m_destinationAid; }

uint32_t AllocationField::GetAllocationStart() const {
  return m_allocationStart;
}

uint16_t AllocationField::GetAllocationBlockDuration() const {
  return m_allocationBlockDuration;
}

uint8_t AllocationField::GetNumberOfBlocks() const { return m_numberOfBlocks; }

uint16_t AllocationField::GetAllocationBlockPeriod() const {
  return m_allocationBlockPeriod;
}

void AllocationField::SetAllocationAnnounced() { m_allocationAnnounced = true; }

bool AllocationField::IsAllocationAnnounced() const {
  return m_allocationAnnounced;
}

/***************************************************
 *        Extended Schedule Element 8.4.2.134
 ****************************************************/

ExtendedScheduleElement::ExtendedScheduleElement() {}

WifiInformationElementId ExtendedScheduleElement::ElementId() const {
  return IE_EXTENDED_SCHEDULE;
}

uint16_t ExtendedScheduleElement::GetInformationFieldSize() const {
  return m_list.size() * 15;
}

void ExtendedScheduleElement::SerializeInformationField(
    Buffer::Iterator start) const {
  for (AllocationFieldList::const_iterator i = m_list.begin();
       i != m_list.end(); i++) {
    start = i->Serialize(start);
  }
}

uint16_t
ExtendedScheduleElement::DeserializeInformationField(Buffer::Iterator start,
                                                     uint16_t length) {
  AllocationField field;
  Buffer::Iterator i = start;
  while (i.GetDistanceFrom(start) != length) {
    i = field.Deserialize(i);
    m_list.push_back(field);
  }
  return length;
}

void ExtendedScheduleElement::AddAllocationField(AllocationField &field) {
  m_list.push_back(field);
}

void ExtendedScheduleElement::SetAllocationFieldList(
    const AllocationFieldList &list) {
  m_list = list;
}

AllocationFieldList ExtendedScheduleElement::GetAllocationFieldList() const {
  return m_list;
}

ATTRIBUTE_HELPER_CPP(ExtendedScheduleElement);

std::ostream &operator<<(std::ostream &os,
                         const ExtendedScheduleElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, ExtendedScheduleElement &element) {
  return is;
}

/***************************************************
 *              Next DMG ATI 8.4.2.137
 ****************************************************/

StaInfoField::StaInfoField() : m_aid(0), m_cbap(false), m_pp(false) {}

void StaInfoField::Print(std::ostream &os) const {}

uint32_t StaInfoField::GetSerializedSize() const { return 2; }

Buffer::Iterator StaInfoField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint8_t value = 0;
  value |= m_cbap;
  value |= (m_pp << 1);

  i.WriteU8(m_aid);
  i.WriteU8(value);

  return i;
}

Buffer::Iterator StaInfoField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  m_aid = i.ReadU8();
  uint8_t value = i.ReadU8();
  m_cbap = value & 0x1;
  m_pp = (value >> 1) & 0x1;

  return i;
}

void StaInfoField::SetAid(uint8_t aid) { m_aid = aid; }

void StaInfoField::SetCbap(bool value) { m_cbap = value; }

void StaInfoField::SetPollingPhase(bool value) { m_pp = value; }

uint8_t StaInfoField::GetAid() const { return m_aid; }

bool StaInfoField::GetCbap() const { return m_cbap; }

bool StaInfoField::GetPollingPhase() const { return m_pp; }

/***************************************************
 *        STA Availability element 8.4.2.135
 ****************************************************/

StaAvailabilityElement::StaAvailabilityElement() {}

WifiInformationElementId StaAvailabilityElement::ElementId() const {
  return IE_STA_AVAILABILITY;
}

uint16_t StaAvailabilityElement::GetInformationFieldSize() const {
  return m_list.size() * 2;
}

void StaAvailabilityElement::SerializeInformationField(
    Buffer::Iterator start) const {
  for (StaInfoFieldList::const_iterator i = m_list.begin(); i != m_list.end();
       i++) {
    start = i->Serialize(start);
  }
}

uint16_t
StaAvailabilityElement::DeserializeInformationField(Buffer::Iterator start,
                                                    uint16_t length) {
  StaInfoField field;
  Buffer::Iterator i = start;
  while (i.GetDistanceFrom(start) != length) {
    i = field.Deserialize(i);
    m_list.push_back(field);
  }
  return length;
}

void StaAvailabilityElement::AddStaInfo(StaInfoField &field) {
  m_list.push_back(field);
}

void StaAvailabilityElement::SetStaInfoList(const StaInfoFieldList &list) {
  m_list = list;
}

StaInfoFieldList StaAvailabilityElement::GetStaInfoList() const {
  return m_list;
}

StaInfoField StaAvailabilityElement::GetStaInfoField() const {
  NS_ASSERT(!m_list.empty());
  return m_list[0];
}

ATTRIBUTE_HELPER_CPP(StaAvailabilityElement);

std::ostream &operator<<(std::ostream &os,
                         const StaAvailabilityElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, StaAvailabilityElement &element) {
  return is;
}

/***************************************************
 *             DMG Allocation Info Field
 ****************************************************/

DmgAllocationInfo::DmgAllocationInfo()
    : m_allocationId(0), m_allocationType(SERVICE_PERIOD_ALLOCATION),
      m_allocationFormat(ISOCHRONOUS), m_pseudoStatic(false),
      m_truncatable(false), m_extendable(false), m_lpScUsed(false), m_up(0),
      m_destAid(0) {}

void DmgAllocationInfo::Print(std::ostream &os) const {}

uint32_t DmgAllocationInfo::GetSerializedSize() const { return 3; }

Buffer::Iterator DmgAllocationInfo::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint16_t val1 = 0;
  uint8_t val2 = 0;

  val1 |= m_allocationId & 0xF;
  val1 |= (m_allocationType & 0x7) << 4;
  val1 |= (m_allocationFormat & 0x1) << 7;
  val1 |= (m_pseudoStatic & 0x1) << 8;
  val1 |= (m_truncatable & 0x1) << 9;
  val1 |= (m_extendable & 0x1) << 10;
  val1 |= (m_lpScUsed & 0x1) << 11;
  val1 |= (m_up & 0x7) << 12;
  val1 |= (m_destAid & 0x1) << 15;
  val2 |= (m_destAid >> 1);

  i.WriteHtolsbU16(val1);
  i.WriteU8(val2);

  return i;
}

Buffer::Iterator DmgAllocationInfo::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint16_t val1 = i.ReadLsbtohU16();
  uint8_t val2 = i.ReadU8();

  m_allocationId = val1 & 0xF;
  m_allocationType = (val1 >> 4) & 0x7;
  m_allocationFormat = (val1 >> 7) & 0x1;
  m_pseudoStatic = (val1 >> 8) & 0x1;
  m_truncatable = (val1 >> 9) & 0x1;
  m_extendable = (val1 >> 10) & 0x1;
  m_lpScUsed = (val1 >> 11) & 0x1;
  m_up = (val1 >> 12) & 0x7;
  m_destAid = (val1 >> 15) & 0x1;
  m_destAid |= (val2 << 1);

  return i;
}

void DmgAllocationInfo::SetAllocationId(AllocationId id) {
  m_allocationId = id;
}

void DmgAllocationInfo::SetAllocationType(AllocationType type) {
  m_allocationType = static_cast<AllocationType>(type);
}

void DmgAllocationInfo::SetAllocationFormat(AllocationFormat format) {
  m_allocationFormat = static_cast<AllocationFormat>(format);
}

void DmgAllocationInfo::SetAsPseudoStatic(bool value) {
  m_pseudoStatic = value;
}

void DmgAllocationInfo::SetAsTruncatable(bool value) { m_truncatable = value; }

void DmgAllocationInfo::SetAsExtendable(bool value) { m_extendable = value; }

void DmgAllocationInfo::SetLpScUsed(bool value) { m_lpScUsed = value; }

void DmgAllocationInfo::SetUp(uint8_t value) { m_up = value; }

void DmgAllocationInfo::SetDestinationAid(uint8_t aid) { m_destAid = aid; }

AllocationId DmgAllocationInfo::GetAllocationId() const {
  return m_allocationId;
}

AllocationType DmgAllocationInfo::GetAllocationType() const {
  return static_cast<AllocationType>(m_allocationType);
}

AllocationFormat DmgAllocationInfo::GetAllocationFormat() const {
  return static_cast<AllocationFormat>(m_allocationFormat);
}

bool DmgAllocationInfo::IsPseudoStatic() const { return m_pseudoStatic; }

bool DmgAllocationInfo::IsTruncatable() const { return m_truncatable; }

bool DmgAllocationInfo::IsExtendable() const { return m_extendable; }

bool DmgAllocationInfo::IsLpScUsed() const { return m_lpScUsed; }

uint8_t DmgAllocationInfo::GetUp() const { return m_up; }

uint8_t DmgAllocationInfo::GetDestinationAid() const { return m_destAid; }

/***************************************************
 *             DMG TSPEC element 8.4.2.136
 ****************************************************/

DmgTspecElement::DmgTspecElement()
    : m_allocationPeriod(0), m_minAllocation(0), m_maxAllocation(0),
      m_minDuration(0) {}

WifiInformationElementId DmgTspecElement::ElementId() const {
  return IE_DMG_TSPEC;
}

uint16_t DmgTspecElement::GetInformationFieldSize() const { return 14; }

void DmgTspecElement::SerializeInformationField(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i = m_dmgAllocationInfo.Serialize(i);
  i = m_bfControlField.Serialize(i);
  i.WriteHtolsbU16(m_allocationPeriod);
  i.WriteHtolsbU16(m_minAllocation);
  i.WriteHtolsbU16(m_maxAllocation);
  i.WriteHtolsbU16(m_minDuration);
  i.WriteU8(0);
}

uint16_t DmgTspecElement::DeserializeInformationField(Buffer::Iterator start,
                                                      uint16_t length) {
  Buffer::Iterator i = start;

  i = m_dmgAllocationInfo.Deserialize(i);
  i = m_bfControlField.Deserialize(i);
  m_allocationPeriod = i.ReadLsbtohU16();
  m_minAllocation = i.ReadLsbtohU16();
  m_maxAllocation = i.ReadLsbtohU16();
  m_minDuration = i.ReadLsbtohU16();
  i.ReadU8();

  return length;
}

void DmgTspecElement::SetDmgAllocationInfo(DmgAllocationInfo &info) {
  m_dmgAllocationInfo = info;
}

void DmgTspecElement::SetBfControl(BfControlField &ctrl) {
  m_bfControlField = ctrl;
}

void DmgTspecElement::SetAllocationPeriod(uint16_t period, bool multiple) {
  NS_ASSERT(period <= 32767);
  m_allocationPeriod = static_cast<uint16_t>(multiple) << 15;
  m_allocationPeriod |= period;
}

void DmgTspecElement::SetMinimumAllocation(uint16_t min) {
  m_minAllocation = min;
}

void DmgTspecElement::SetMaximumAllocation(uint16_t max) {
  m_maxAllocation = max;
}

void DmgTspecElement::SetMinimumDuration(uint16_t duration) {
  m_minDuration = duration;
}

DmgAllocationInfo DmgTspecElement::GetDmgAllocationInfo() const {
  return m_dmgAllocationInfo;
}

BfControlField DmgTspecElement::GetBfControl() const {
  return m_bfControlField;
}

uint16_t DmgTspecElement::GetAllocationPeriod() const {
  return (m_allocationPeriod & 0x7FFF);
}

bool DmgTspecElement::IsAllocationPeriodMultipleBi() const {
  return ((m_allocationPeriod >> 15) & 0x1);
}

uint16_t DmgTspecElement::GetMinimumAllocation() const {
  return m_minAllocation;
}

uint16_t DmgTspecElement::GetMaximumAllocation() const {
  return m_maxAllocation;
}

uint16_t DmgTspecElement::GetMinimumDuration() const { return m_minDuration; }

std::ostream &operator<<(std::ostream &os, const DmgTspecElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, DmgTspecElement &element) {
  return is;
}

/***************************************************
 *              Next DMG ATI 8.4.2.137
 ****************************************************/

NextDmgAti::NextDmgAti() : m_startTime(0), m_atiDuration(0) {}

WifiInformationElementId NextDmgAti::ElementId() const {
  return IE_NEXT_DMG_ATI;
}

uint16_t NextDmgAti::GetInformationFieldSize() const { return 6; }

void NextDmgAti::SerializeInformationField(Buffer::Iterator start) const {
  start.WriteHtolsbU32(m_startTime);
  start.WriteHtolsbU16(m_atiDuration);
}

uint16_t NextDmgAti::DeserializeInformationField(Buffer::Iterator start,
                                                 uint16_t length) {
  Buffer::Iterator i = start;
  uint32_t time = i.ReadLsbtohU32();
  uint16_t duration = i.ReadLsbtohU16();

  SetStartTime(time);
  SetAtiDuration(duration);

  return length;
}

void NextDmgAti::SetStartTime(uint32_t time) { m_startTime = time; }

void NextDmgAti::SetAtiDuration(uint16_t duration) { m_atiDuration = duration; }

uint32_t NextDmgAti::GetStartTime() const { return m_startTime; }

uint16_t NextDmgAti::GetAtiDuration() const { return m_atiDuration; }

void NextDmgAti::Print(std::ostream &os) const { os << *this; }

ATTRIBUTE_HELPER_CPP(NextDmgAti);

std::ostream &operator<<(std::ostream &os, const NextDmgAti &element) {
  os << element.GetStartTime() << "|" << element.GetAtiDuration();
  return os;
}

std::istream &operator>>(std::istream &is, NextDmgAti &element) {
  uint32_t c1;
  uint16_t c2;
  is >> c1 >> c2;
  element.SetStartTime(c1);
  element.SetAtiDuration(c2);

  return is;
}

/***************************************************
 *   Relay Capabilities Info field (Figure 8-401ba)
 ****************************************************/

RelayCapabilitiesInfo::RelayCapabilitiesInfo()
    : m_supportability(0), m_usability(0), m_permission(0), m_acPower(0),
      m_preference(0), m_duplex(0), m_cooperation(0) {}

void RelayCapabilitiesInfo::Print(std::ostream &os) const {}

uint32_t RelayCapabilitiesInfo::GetSerializedSize() const { return 2; }

Buffer::Iterator
RelayCapabilitiesInfo::Serialize(Buffer::Iterator start) const {
  uint16_t val = 0;
  val |= m_supportability & 0x1;
  val |= (m_usability & 0x1) << 1;
  val |= (m_permission & 0x1) << 2;
  val |= (m_acPower & 0x1) << 3;
  val |= (m_preference & 0x1) << 4;
  val |= (m_duplex & 0x3) << 5;
  val |= (m_cooperation & 0x1) << 7;
  start.WriteHtolsbU16(val);
  return start;
}

Buffer::Iterator RelayCapabilitiesInfo::Deserialize(Buffer::Iterator start) {
  uint16_t info = start.ReadLsbtohU16();
  m_supportability = info & 0x1;
  m_usability = (info >> 1) & 0x1;
  m_permission = (info >> 2) & 0x1;
  m_acPower = (info >> 3) & 0x1;
  m_preference = (info >> 4) & 0x1;
  m_duplex = (info >> 5) & 0x3;
  m_cooperation = (info >> 7) & 0x1;
  return start;
}

void RelayCapabilitiesInfo::SetRelaySupportability(bool value) {
  m_supportability = value;
}

void RelayCapabilitiesInfo::SetRelayUsability(bool value) {
  m_usability = value;
}

void RelayCapabilitiesInfo::SetRelayPermission(bool value) {
  m_permission = value;
}

void RelayCapabilitiesInfo::SetAcPower(bool value) { m_acPower = value; }

void RelayCapabilitiesInfo::SetRelayPreference(bool value) {
  m_preference = value;
}

void RelayCapabilitiesInfo::SetDuplex(RelayDuplexMode duplex) {
  m_duplex = duplex;
}

void RelayCapabilitiesInfo::SetCooperation(bool value) {
  m_cooperation = value;
}

bool RelayCapabilitiesInfo::GetRelaySupportability() const {
  return m_supportability;
}

bool RelayCapabilitiesInfo::GetRelayUsability() const { return m_usability; }

bool RelayCapabilitiesInfo::GetRelayPermission() const { return m_permission; }

bool RelayCapabilitiesInfo::GetAcPower() const { return m_acPower; }

bool RelayCapabilitiesInfo::GetRelayPreference() const { return m_preference; }

RelayDuplexMode RelayCapabilitiesInfo::GetDuplex() const {
  return static_cast<RelayDuplexMode>(m_duplex);
}

bool RelayCapabilitiesInfo::GetCooperation() const { return m_cooperation; }

/***************************************************
 *       Relay Capabilities Element 8.4.2.150
 ****************************************************/

RelayCapabilitiesElement::RelayCapabilitiesElement() : m_info() {}

WifiInformationElementId RelayCapabilitiesElement::ElementId() const {
  return IE_RELAY_CAPABILITIES;
}

uint16_t RelayCapabilitiesElement::GetInformationFieldSize() const {
  return m_info.GetSerializedSize();
}

void RelayCapabilitiesElement::SerializeInformationField(
    Buffer::Iterator start) const {
  m_info.Serialize(start);
}

uint16_t
RelayCapabilitiesElement::DeserializeInformationField(Buffer::Iterator start,
                                                      uint16_t length) {
  Buffer::Iterator i = start;
  i = m_info.Deserialize(i);
  return i.GetDistanceFrom(start);
}

void RelayCapabilitiesElement::SetRelayCapabilitiesInfo(
    RelayCapabilitiesInfo &info) {
  m_info = info;
}

RelayCapabilitiesInfo
RelayCapabilitiesElement::GetRelayCapabilitiesInfo() const {
  return m_info;
}

ATTRIBUTE_HELPER_CPP(RelayCapabilitiesElement);

std::ostream &operator<<(std::ostream &os,
                         const RelayCapabilitiesElement &element) {
  return os;
}

std::istream &operator>>(std::istream &is, RelayCapabilitiesElement &element) {
  return is;
}

} // namespace ns3
