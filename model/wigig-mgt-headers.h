/*
 * Copyright (c) 2006 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef MGT_HEADERS_COPY_H
#define MGT_HEADERS_COPY_H

#include "ext-headers.h"
#include "fields-headers.h"

#include "ns3/capability-information.h"
#include "ns3/dmg-capabilities.h"
#include "ns3/header.h"
#include "ns3/mgt-headers.h"
#include "ns3/ssid.h"
#include "ns3/status-code.h"
#include "ns3/wifi-mgt-header.h"

namespace ns3 {

/**
 * \ingroup wifi
 * Implement the header for management frames that can be included in a Per-STA
 * Profile subelement of a Multi-Link Element. \tparam Derived \explicit the
 * type of derived management frame \tparam Tuple \explicit A tuple of the types
 * of Information Elements included in the mgt frame
 */
template <typename Derived, typename Tuple> class WigigMgtHeaderInPerStaProfile;

/**
 * \ingroup wifi
 *
 * Add methods needed to serialize/deserialize a management header into a
 * Per-STA Profile subelement of a Multi-Link Element.
 *
 * \tparam Derived \explicit the type of derived management frame
 * \tparam Elems \explicit sorted list of Information Elements that can be
 * included in mgt frame
 */
template <typename Derived, typename... Elems>
class WigigMgtHeaderInPerStaProfile<Derived, std::tuple<Elems...>>
    : public WifiMgtHeader<Derived, std::tuple<Elems...>> {
public:
  /**
   * \param frame the frame containing the Multi-Link Element
   * \return the number of bytes that are needed to serialize this header into a
   * Per-STA Profile subelement of the Multi-Link Element
   */
  uint32_t GetSerializedSizeInPerStaProfile(const Derived &frame) const;

  /**
   * Serialize this header into a Per-STA Profile subelement of a Multi-Link
   * Element
   *
   * \param start an iterator which points to where the header should be written
   * \param frame the frame containing the Multi-Link Element
   */
  void SerializeInPerStaProfile(Buffer::Iterator start,
                                const Derived &frame) const;

  /**
   * Deserialize this header from a Per-STA Profile subelement of a Multi-Link
   * Element.
   *
   * \param start an iterator which points to where the header should be read
   * from \param length the expected number of bytes to read \param frame the
   * frame containing the Multi-Link Element \return the number of bytes read
   */
  uint32_t DeserializeFromPerStaProfile(Buffer::Iterator start, uint16_t length,
                                        const Derived &frame);

  /**
   * Copy Information Elements inherited from the management frame containing
   * the Multi-Link Element into this header (which is stored in a Per-STA
   * Profile subelement). This method shall be invoked when the deserialization
   * has been completed (i.e., the Non-Inheritance element, if present, has been
   * deserialized).
   *
   * \param frame the frame containing the Multi-Link Element
   */
  void CopyIesFromContainingFrame(const Derived &frame);

protected:
  using WifiMgtHeader<Derived, std::tuple<Elems...>>::InitForDeserialization;

  /**
   * \param frame the frame containing the Multi-Link Element
   * \return the number of bytes that are needed to serialize this header into a
   * Per-STA Profile subelement of the Multi-Link Element
   */
  uint32_t GetSerializedSizeInPerStaProfileImpl(const Derived &frame) const;

  /**
   * Serialize this header into a Per-STA Profile subelement of a Multi-Link
   * Element
   *
   * \param start an iterator which points to where the header should be written
   * \param frame the frame containing the Multi-Link Element
   */
  void SerializeInPerStaProfileImpl(Buffer::Iterator start,
                                    const Derived &frame) const;

  /**
   * Deserialize this header from a Per-STA Profile subelement of a Multi-Link
   * Element.
   *
   * \param start an iterator which points to where the header should be read
   * from \param length the expected number of bytes to read \param frame the
   * frame containing the Multi-Link Element \return the number of bytes read
   */
  uint32_t DeserializeFromPerStaProfileImpl(Buffer::Iterator start,
                                            uint16_t length,
                                            const Derived &frame);

  /**
   * Pass a pointer to this frame to the Multi-Link Element (if any) included in
   * this frame.
   */
  void SetMleContainingFrame() const;

  /**
   * \param optElem the MultiLinkElement object to initialize for deserializing
   * the information element into
   */
  void InitForDeserialization(std::optional<MultiLinkElement> &optElem);

private:
  using WifiMgtHeader<Derived, std::tuple<Elems...>>::DoDeserialize;
  using WifiMgtHeader<Derived, std::tuple<Elems...>>::m_elements;

  std::optional<NonInheritance>
      m_nonInheritance; /**< the Non-Inheritance IE possibly appended
                             to the Per-STA Profile subelement */
};

/**
 * \ingroup wigig
 * Implement the header for action frames of type DMG ADDTS request.
 */
class DmgAddTsRequestFrame
    : public WifiMgtHeader<DmgAddTsRequestFrame, WigigAssocRequestElems> {
  friend class WifiMgtHeader<DmgAddTsRequestFrame, WigigAssocRequestElems>;

public:
  DmgAddTsRequestFrame();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  // virtual void Print (std::ostream &os) const;
  // virtual uint32_t GetSerializedSize (void) const;
  // virtual void Serialize (Buffer::Iterator start) const;
  // virtual uint32_t Deserialize (Buffer::Iterator start);

  /**
   * The Dialog Token field.
   * \param token
   */
  void SetDialogToken(uint8_t token);

  uint8_t GetDialogToken() const;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize*/
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;

private:
  uint8_t m_dialogToken;
};

/**
 * \ingroup wigig
 * Implement the header for action frames of type DMG ADDTS request.
 */
class DmgAddTsResponseFrame : public Header {
public:
  DmgAddTsResponseFrame();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * The Dialog Token field.
   * \param token
   */
  void SetDialogToken(uint8_t token);
  void SetStatusCode(StatusCode status);
  void SetTsDelay(TsDelayElement &element);
  void SetDmgTspecElement(DmgTspecElement &element);

  uint8_t GetDialogToken() const;
  StatusCode GetStatusCode() const;
  TsDelayElement GetTsDelay() const;
  DmgTspecElement GetDmgTspec() const;

private:
  uint8_t m_dialogToken;
  StatusCode m_status;
  TsDelayElement m_tsDelayElement;
  DmgTspecElement m_dmgTspecElement;
};

/**
 * \ingroup wigig
 * Implement the header for action frames of type DELTS (8.5.3.4).
 */
class DelTsFrame : public Header {
public:
  DelTsFrame();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetReasonCode(uint16_t reason);
  void SetDmgAllocationInfo(DmgAllocationInfo info);

  uint16_t GetReasonCode() const;
  DmgAllocationInfo GetDmgAllocationInfo() const;

private:
  uint8_t m_tsInfo[3];
  uint16_t m_reasonCode;
  DmgAllocationInfo m_dmgAllocationInfo;
};

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Information Request frame
 * (8.5.20.4).
 */
class ExtInformationRequest
    : public WifiMgtHeader<ExtInformationRequest, WigigAssocRequestElems> {
  friend class WifiMgtHeader<ExtInformationRequest, WigigAssocRequestElems>;

public:
  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;

  /**
   * Set the MAC address of the STA whose information is being requested. If
   * this frame is sent to the PCP and the value of the Subject Address field is
   * the broadcast address, then the STA is requesting information regarding all
   * associated STAs. \param address The Mac address
   */
  void SetSubjectAddress(Mac48Address address);

  Mac48Address GetSubjectAddress() const;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize*/
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  // void PrintImpl(std::ostream& os) const;

private:
  Mac48Address m_subjectAddress;
};

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Information Request frame
 * (8.5.20.4).
 */
class ExtInformationResponse
    : public WifiMgtHeader<ExtInformationResponse, WigigAssocResponseElems> {
  friend class WifiMgtHeader<ExtInformationResponse, WigigAssocResponseElems>;

public:
  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;

  void SetSubjectAddress(Mac48Address address);

  Mac48Address GetSubjectAddress() const;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize*/
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  // void PrintImpl(std::ostream& os) const;

private:
  Mac48Address m_subjectAddress;
};

/**
 * \ingroup wifi
 * Implement the header for extension frames of type Relay Search Request frame.
 */
class ExtRelaySearchRequestHeader : public Header {
public:
  ExtRelaySearchRequestHeader();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetDialogToken(uint8_t token);
  void SetDestinationRedsAid(uint16_t aid);

  uint8_t GetDialogToken() const;
  uint16_t GetDestinationRedsAid() const;

private:
  uint8_t m_dialogToken;
  uint16_t m_aid;
};

typedef std::map<uint16_t, RelayCapabilitiesInfo> RelayCapableStaList;

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Relay Search Response
 * frame.
 */
class ExtRelaySearchResponseHeader : public Header {
public:
  ExtRelaySearchResponseHeader();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetDialogToken(uint8_t token);
  void SetStatusCode(uint16_t status);
  void AddRelayCapableStaInfo(uint8_t aid, RelayCapabilitiesInfo &element);
  void SetRelayCapableList(RelayCapableStaList &list);

  uint8_t GetDialogToken() const;
  uint16_t GetStatusCode() const;
  RelayCapableStaList GetRelayCapableList() const;

private:
  uint8_t m_dialogToken;
  uint16_t m_statusCode;
  RelayCapableStaList m_list;
};

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Multi-relay Channel
 * Measurement Request frame. The Multi-Relay Channel Measurement Request frame
 * is transmitted by a STA initiating relay operation to the recipient STA in
 * order to obtain Channel Measurements between the recipient STA and the other
 * STA participating in the relay operation.
 *
 */
class ExtMultiRelayChannelMeasurementRequest : public Header {
public:
  ExtMultiRelayChannelMeasurementRequest();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * The Dialog Token field is set to a nonzero value chosen by the STA sending
   * the Multi-Relay Channel Measurement Request frame to identify the
   * request/report transaction. \param token
   */
  void SetDialogToken(uint8_t token);

  uint8_t GetDialogToken() const;

private:
  uint8_t m_dialogToken;
};

typedef std::vector<Ptr<ExtChannelMeasurementInfo>> ChannelMeasurementInfoList;

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Relay Search Response
 * frame.
 */
class ExtMultiRelayChannelMeasurementReport : public Header {
public:
  ExtMultiRelayChannelMeasurementReport();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetDialogToken(uint8_t token);
  void AddChannelMeasurementInfo(Ptr<ExtChannelMeasurementInfo> element);
  void SetChannelMeasurementList(ChannelMeasurementInfoList &list);

  uint8_t GetDialogToken() const;
  ChannelMeasurementInfoList GetChannelMeasurementInfoList() const;

private:
  uint8_t m_dialogToken;
  ChannelMeasurementInfoList m_list;
};

/**
 * Implement the header for extension frames of type Unprotected DMG Announce
 * frame (8.5.22.2). The Announce frame is an Action or an Action No Ack frame
 * of category Unprotected DMG. Announce frames can be transmitted during the
 * ATI of a beacon interval and can perform functions including of a DMG Beacon
 * frame, but since this frame does not have to be transmitted as a sector sweep
 * to a STA, it can provide much more spectrally efficient access than using a
 * DMG Beacon frame.
 */
class ExtAnnounceFrame : public Header {
public:
  ExtAnnounceFrame();

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  // Inherited
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set the Timestamp for this frame.
   * \param timestamp The Timestamp for this frame
   */
  void SetTimestamp(uint8_t timestamp);
  /**
   * Set the Beacon Interval.
   * \param interval the Beacon Interval.
   */
  void SetBeaconInterval(uint16_t interval);

  uint8_t GetTimestamp() const;
  uint16_t GetBeaconInterval() const;

private:
  uint8_t m_timestamp;
  uint16_t m_beaconInterval;
};

/**
 * \ingroup wigig
 * Implement the header for extension frames of type Unprotected DMG BRP Frame
 * (8.5.22.3). The BRP frame is an Action No Ack frame.
 */
class ExtBrpFrame : public WifiMgtHeader<ExtBrpFrame, WigigAssocRequestElems> {
  friend class WifiMgtHeader<ExtBrpFrame, WigigAssocRequestElems>;

public:
  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();
  ExtBrpFrame();
  // Inherited
  TypeId GetInstanceTypeId() const override;

  virtual void PrintImpl(std::ostream &os) const;
  virtual uint32_t GetSerializedSizeImpl() const;
  virtual void SerializeImpl(Buffer::Iterator start) const;
  virtual uint32_t DeserializeImpl(Buffer::Iterator start);

  void SetDialogToken(uint8_t token);
  void SetBrpRequestField(BrpRequestField &field);

  uint8_t GetDialogToken() const;
  BrpRequestField GetBrpRequestField() const;

private:
  uint8_t m_dialogToken;
  BrpRequestField m_brpRequestField;
};

/**
 * \ingroup wigig
 * Implement the header for management frames of type probe response.
 */
class WigigMgtProbeResponseHeader
    : public WifiMgtHeader<WigigMgtProbeResponseHeader,
                           WigigProbeResponseElems> {
  friend class WifiMgtHeader<WigigMgtProbeResponseHeader,
                             WigigProbeResponseElems>;

public:
  ~WigigMgtProbeResponseHeader() override = default;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /** \copydoc Header::GetInstanceTypeId */
  TypeId GetInstanceTypeId() const override;

  /**
   * Return the beacon interval in microseconds unit.
   *
   * \return beacon interval in microseconds unit
   */
  uint64_t GetBeaconIntervalUs() const;
  /**
   * Set the beacon interval in microseconds unit.
   *
   * \param us beacon interval in microseconds unit
   */
  void SetBeaconIntervalUs(uint64_t us);
  /**
   * \return a reference to the Capability information
   */
  CapabilityInformation &Capabilities();
  /**
   * \return a const reference to the Capability information
   */
  const CapabilityInformation &Capabilities() const;
  /**
   * Return the time stamp.
   *
   * \return time stamp
   */
  uint64_t GetTimestamp() const;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize*/
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);

private:
  uint64_t m_timestamp;               //!< Timestamp
  uint64_t m_beaconInterval;          //!< Beacon interval
  CapabilityInformation m_capability; //!< Capability information
};

/**
 * \ingroup wigig
 * Implement the header for management frames of type association request.
 */
class WigigMgtAssocRequestHeader
    : public WifiMgtHeader<WigigMgtAssocRequestHeader, WigigAssocRequestElems> {
  friend class WifiMgtHeader<WigigMgtAssocRequestHeader,
                             WigigAssocRequestElems>;

public:
  ~WigigMgtAssocRequestHeader() override = default;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /** \copydoc Header::GetInstanceTypeId */
  TypeId GetInstanceTypeId() const override;

  /**
   * Set the listen interval.
   *
   * \param interval the listen interval
   */
  void SetListenInterval(uint16_t interval);
  /**
   * Return the listen interval.
   *
   * \return the listen interval
   */
  uint16_t GetListenInterval() const;
  /**
   * \return a reference to the Capability information
   */
  CapabilityInformation &Capabilities();
  /**
   * \return a const reference to the Capability information
   */
  const CapabilityInformation &Capabilities() const;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize */
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;

private:
  CapabilityInformation m_capability; //!< Capability information
  uint16_t m_listenInterval{0};       //!< listen interval
};

/**
 * \ingroup wigig
 * Implement the header for management frames of type association and
 * reassociation response.
 */
class WigigMgtAssocResponseHeader
    : public WifiMgtHeader<WigigMgtAssocResponseHeader,
                           WigigAssocResponseElems> {
  friend class WifiMgtHeader<WigigMgtAssocResponseHeader,
                             WigigAssocResponseElems>;

public:
  ~WigigMgtAssocResponseHeader() override = default;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /** \copydoc Header::GetInstanceTypeId */
  TypeId GetInstanceTypeId() const override;

  /**
   * Return the status code.
   *
   * \return the status code
   */
  StatusCode GetStatusCode();
  /**
   * Set the status code.
   *
   * \param code the status code
   */
  void SetStatusCode(StatusCode code);
  /**
   * \return a reference to the Capability information
   */
  CapabilityInformation &Capabilities();
  /**
   * \return a const reference to the Capability information
   */
  const CapabilityInformation &Capabilities() const;
  /**
   * Return the association ID.
   *
   * \return the association ID
   */
  uint16_t GetAssociationId() const;
  /**
   * Set the association ID.
   *
   * \param aid the association ID
   */
  void SetAssociationId(uint16_t aid);

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize */
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;

private:
  CapabilityInformation m_capability; //!< Capability information
  StatusCode m_code;                  //!< Status code
  uint16_t m_aid{0};                  //!< AID
};

/**
 * \ingroup wigig
 * Implement the header for management frames of type probe request.
 */
class WigigMgtProbeRequestHeader
    : public WifiMgtHeader<WigigMgtProbeRequestHeader, WigigProbeRequestElems> {
  friend class WifiMgtHeader<WigigMgtProbeRequestHeader,
                             WigigProbeRequestElems>;

public:
  ~WigigMgtProbeRequestHeader() override = default;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /** \copydoc Header::GetInstanceTypeId */
  TypeId GetInstanceTypeId() const override;

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize */
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;
};

/**
 * \ingroup wifi
 * Implement the header for management frames of type reassociation request.
 */
class WigigMgtReassocRequestHeader
    : public WifiMgtHeader<WigigMgtReassocRequestHeader,
                           WigigAssocRequestElems> {
  friend class WifiMgtHeader<WigigMgtReassocRequestHeader,
                             WigigAssocRequestElems>;

public:
  ~WigigMgtReassocRequestHeader() override = default;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /** \copydoc Header::GetInstanceTypeId */
  TypeId GetInstanceTypeId() const override;

  /**
   * Set the listen interval.
   *
   * \param interval the listen interval
   */
  void SetListenInterval(uint16_t interval);
  /**
   * Return the listen interval.
   *
   * \return the listen interval
   */
  uint16_t GetListenInterval() const;
  /**
   * \return a reference to the Capability information
   */
  CapabilityInformation &Capabilities();
  /**
   * \return a const reference to the Capability information
   */
  const CapabilityInformation &Capabilities() const;
  /**
   * Set the address of the current access point.
   *
   * \param currentApAddr address of the current access point
   */
  void SetCurrentApAddress(Mac48Address currentApAddr);

protected:
  /** \copydoc Header::GetSerializedSize */
  uint32_t GetSerializedSizeImpl() const;
  /** \copydoc Header::Serialize */
  void SerializeImpl(Buffer::Iterator start) const;
  /** \copydoc Header::Deserialize */
  uint32_t DeserializeImpl(Buffer::Iterator start);
  /** \copydoc Header::Print */
  void PrintImpl(std::ostream &os) const;

private:
  Mac48Address m_currentApAddr;       //!< Address of the current access point
  CapabilityInformation m_capability; //!< Capability information
  uint16_t m_listenInterval{0};       //!< listen interval
};

} // namespace ns3

#endif /* MGT_HEADERS_COPY_H */
