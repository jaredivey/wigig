/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef CODEBOOK_PARAMETRIC_H
#define CODEBOOK_PARAMETRIC_H

#include "codebook.h"

#include <array>
#include <complex>
#include <iostream>

namespace ns3 {

typedef std::complex<float> Complex; //!< Typedef for a complex number.
typedef std::vector<Complex>
    WeightsVector; //!< Typedef for an antenna weights vector.
typedef WeightsVector::iterator
    WeightsVectorI; //!< Typedef for an iterator for AWV.
typedef WeightsVector::const_iterator
    WeightsVectorCI; //!< Typedef for a constant iterator for AWV.
typedef std::array<std::array<Complex, ELEVATION_CARDINALITY>,
                   AZIMUTH_CARDINALITY>
    ArrayPattern; //!< Typedef for an phased antenna array pattern.
typedef std::array<std::array<Directivity, ELEVATION_CARDINALITY>,
                   AZIMUTH_CARDINALITY>
    DirectivityMatrix; //!< Typedef for phased antenna directivity matrix.
typedef std::array<std::array<std::vector<Complex>, ELEVATION_CARDINALITY>,
                   AZIMUTH_CARDINALITY>
    SteeringVector; //!< Typedef for phased antenna steering vector.
typedef std::pair<uint16_t, uint16_t>
    PatternAngles; //!< Tyepdef for angles (Azimuth and Elevation) in degrees.
typedef std::map<PatternAngles, Complex>
    ArrayPatternMap; //!< Tyepdef for mapping between angles and array pattern
                     //!< value.

struct ParametricPatternConfig;

/**
 * Parametric phased antenna array configuration.
 */
struct ParametricAntennaConfig : public PhasedAntennaArrayConfig {
  /**
   * Calculate phased antenna array complex pattern.
   * \param weightsVector The complex weights of the antenna elements.
   * \return the complex matrix of antenna array pattern.
   */
  ArrayPattern CalculateArrayPattern(WeightsVector weights);
  /**
   * Get a pointer to the quasi-omni pattern associated with this array.
   * \return A pointer to the quasi-omni pattern.
   */
  Ptr<ParametricPatternConfig> GetQuasiOmniConfig() const;
  /**
   * Set the number of bits for quanitizing phase values.
   * \param num The number of bits for quanitizing phase values
   */
  void SetPhaseQuantizationBits(uint8_t num);
  /**
   * Copy the content of the source antenna array.
   * \param srcAntennaConfig Pointer to the source antenna array config.
   */
  void CopyAntennaArray(Ptr<ParametricAntennaConfig> srcAntennaConfig);

  uint16_t numElements; //!< The number of the antenna elements in the phased
                        //!< antenna array.
  SteeringVector steeringVector; //!< Steering matrix of MxNxL where L is the
                                 //!< number of antenna elements
                                 // and M and N represent the azimuth and
                                 // elevation angles of the incoming plane wave.

  DirectivityMatrix
      singleElementDirectivity; //!< The directivity of a single antenna element
                                //!< in linear scale.
  uint8_t amplitudeQuantizationBits; //!< Number of bits for quanitizing gain
                                     //!< (amplitude) value.

private:
  uint8_t
      m_phaseQuantizationBits; //!< Number of bits for quanitizing phase values.
  double m_phaseQuantizationStepSize; //<! phase quantization step size.
};

/**
 * Interface for generating radiation pattern using parametric method.
 */
struct ParametricPatternConfig : virtual public PatternConfig {
  /**
   * Set the weights that define the array pattern of the phased antenna array.
   * \param weights Vector of complex weights values.
   */
  void SetWeights(const WeightsVector &weights);
  /**
   * Get the weights that define the array pattern of the phased antenna array.
   * \return Vector of complex weights values.
   */
  WeightsVector GetWeights() const;
  /**
   * Get the normalization value based on the current antenna weights vector.
   * \return The normalization value based on the current antenna weights
   * vector.
   */
  float GetNormalizationFactor() const;
  void SetNormalizationFactor(float factor);
  /**
   * Get the array pattern associated with this sector/awv.
   * \return The array pattern of the antenna array.
   */
  ArrayPattern GetArrayPattern() const;
  void SetArrayPattern(const ArrayPattern &arrayPattern);
  /**
   * Get the array pattern value associated with this sector/awv for particular
   * angles. \param azimuthAngle The azimuth angle in degrees. \param
   * elevationAngle The azimuth angle in degrees. \return The array pattern of
   * the antenna array for particular angles.
   */
  Complex GetArrayPattern(uint16_t azimuthAngle, uint16_t elevationAngle);
  /**
   * Calculate the complex array pattern value for particular angles.
   * \param azimuthAngle The azimuth angle in degrees.
   * \param elevationAngle The azimuth angle in degrees.
   */
  void CalculateArrayPattern(Ptr<ParametricAntennaConfig> antennaConfig,
                             uint16_t azimuthAngle, uint16_t elevationAngle);

protected:
  ArrayPattern m_arrayPattern; //<! The complex phased antenna array pattern
                               //after applying the
                               // weights vector.
  ArrayPatternMap m_arrayPatternMap; //<! The complex values of the phased
                                     //antenna array pattern.

private:
  WeightsVector m_weights;     //!< Weights that define the directivity of the
                               //!< phased antenna array.
  float m_normalizationFactor; //!< Normalization factor based on the current
                               //!< weights.
};

/**
 * Parametric AWV Configuration.
 */
struct ParametricAwvConfig : virtual public AwvConfig,
                             virtual public ParametricPatternConfig {};

/**
 * Parametric Sector configuration.
 */
struct ParametricSectorConfig : virtual public SectorConfig,
                                virtual public ParametricPatternConfig {};

/**
 * \brief Codebook for Sectors generated using antenna Array Factor (AF). The
 * user describes the steering vector of the antenna. In addition, the user
 * defines list of antenna weight to describe shapes.
 */
class CodebookParametric : public Codebook {
public:
  static TypeId GetTypeId();

  CodebookParametric();
  ~CodebookParametric() override;
  /**
   * Load code book from a text file.
   */
  void LoadCodebook(std::string filename) override;

  /**
   * Get transmit antenna gain dBi.
   * \param angle The angle towards the intended receiver.
   * \return Transmit antenna gain in dBi based on the steering angle.
   */
  double GetTxGainDbi(double angle) override;
  /**
   * Get transmit antenna gain in dBi.
   * \param angle The angle towards the intended receiver.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  double GetRxGainDbi(double angle) override;
  /**
   * Get transmit antenna gain in dBi.
   * \param azimuth The azimuth angle towards the intended receiver.
   * \param elevation The elevation angle towards the intended receiver.
   * \return Transmit antenna gain in dBi based on the steering angle.
   */
  double GetTxGainDbi(double azimuth, double elevation) override;
  /**
   * Get receive antenna gain in dBi.
   * \param azimuth The azimuth angle towards the intended receiver.
   * \param elevation The elevation angle towards the intended receiver.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  double GetRxGainDbi(double azimuth, double elevation) override;
  /**
   * Get the total number of sectors for a specific phased antenna array.
   * \param antennaID The ID of the phased antenna array.
   * \return The number of sectors for the specified phased antenna array.
   */
  uint8_t GetNumberSectorsPerAntenna(AntennaId antennaID) const override;
  /**
   * Append a new sector to for a specific antenna array or update an existing
   * sector in the codebook. If the sector is new then the number of sectors
   * should not exceed 64 sectors per antenna array and 128 across all the
   * antennas. \param antennaID The of the phased antenna array. \param sectorID
   * The ID of the sector to ID. \param sectorUsage The usage of the sector.
   * \param sectorType The type of the sector.
   * \param weightsVector The antenna weights vector of the sector.
   */
  void AppendSector(AntennaId antennaID, SectorId sectorID,
                    SectorUsage sectorUsage, SectorType sectorType,
                    WeightsVector &weightsVector);
  /**
   * Copy the content of an existing codebook to the current codebook.
   * \param codebook A pointer to the codebook that we want to copy its
   * contents.
   */
  void CopyCodebook(const Ptr<Codebook> codebook) override;

  /**
   * Get the complex value of the specified antenna array pattern.
   * \param config Pointer to the pattern configuration.
   * \param azimuthAngle The azimuth angle in degrees.
   * \param elevationAngle The elevation angle in degrees.
   * \return The complex array pattern of the given pattern.
   */
  Complex GetAntennaArrayPattern(Ptr<PatternConfig> config,
                                 uint16_t azimuthAngle,
                                 uint16_t elevationAngle) const;
  /**
   * Calculate array pattern for a particular pair of angles for all the sectors
   * and their AWVs in the specified antenna array ID. \param antennaID The ID
   * of the antenna array. \param azimuthAngle The azimuth angle in degrees
   * after rounding it. \param elevationAngle The elevation angle in degrees
   * after rounding it.
   */
  void CalculateArrayPatterns(AntennaId antennaID, uint16_t azimuthAngle,
                              uint16_t elevationAngle);
  /**
   * \return True if the array patterns are precalculated, otherwise false.
   */
  bool ArrayPatternsPrecalculated() const;

private:
  /**
   * Print antenna weights vector or beamforming vector.
   * \param weightsVector The list of antenna weights to be printed.
   */
  void PrintWeights(WeightsVector weightsVector);
  /**
   * Set Codebook File Name.
   * \param fileName The name of the codebook file to load.
   */
  void SetCodebookFileName(std::string fileName);
  /**
   * Read Antenna Weights Vector for a predefined antenna pattern.
   * \param file The file from where to read the values.
   * \param elements The number of antenna elements in the antenna array.
   * \return A weight vector that includes the excitation (Phase and amplitude)
   * for each antenna element.
   */
  WeightsVector ReadAntennaWeightsVector(std::ifstream &file,
                                         uint16_t elements);

  bool m_precalculatedPatterns; //!< Flag to indicate whether we have
                                //!< precalculated the array pattern.
  bool m_cloned; //!< Flag to indicate if we have cloned this codebook.
};

} // namespace ns3

#endif /* CODEBOOK_PARAMETRIC_H */
