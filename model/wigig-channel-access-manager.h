/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef WIGIG_CHANNEL_ACCESS_MANAGER_H
#define WIGIG_CHANNEL_ACCESS_MANAGER_H

#include "ns3/event-id.h"
#include "ns3/nstime.h"
#include "ns3/object.h"

#include <algorithm>
#include <vector>

namespace ns3 {

class PhyListener;
class MacLow;
class WigigPhy;
class WigigTxop;

/**
 * \brief Manage a set of ns3::WigigTxop
 * \ingroup wigig
 *
 * Handle a set of independent ns3::WigigTxop, each of which represents
 * a single DCF within a MAC stack. Each ns3::WigigTxop has a priority
 * implicitly associated with it (the priority is determined when the
 * ns3::WigigTxop is added to the WigigChannelAccessManager: the first WigigTxop
 * to be added gets the highest priority, the second, the second highest
 * priority, and so on.) which is used to handle "internal" collisions.
 * i.e., when two local WigigTxop are expected to get access to the
 * medium at the same time, the highest priority local WigigTxop wins
 * access to the medium and the other WigigTxop suffers a "internal"
 * collision.
 */
class WigigChannelAccessManager : public Object {
public:
  WigigChannelAccessManager();
  ~WigigChannelAccessManager() override;

  /**
   * Set up listener for PHY events.
   *
   * \param phy the WigigPhy to listen to
   */
  void SetupPhyListener(Ptr<WigigPhy> phy);
  /**
   * Remove current registered listener for PHY events.
   *
   * \param phy the WigigPhy to listen to
   */
  void RemovePhyListener(Ptr<WigigPhy> phy);
  /**
   * Set up listener for MacLow events.
   *
   * \param low the MacLow to listen to
   */
  void SetupLow(Ptr<MacLow> low);

  /**
   * \param slotTime the duration of a slot.
   *
   * It is a bad idea to call this method after RequestAccess or
   * one of the Notify methods has been invoked.
   */
  void SetSlot(Time slotTime);
  /**
   * \param sifs the duration of a SIFS.
   *
   * It is a bad idea to call this method after RequestAccess or
   * one of the Notify methods has been invoked.
   */
  void SetSifs(Time sifs);
  /**
   * \param eifsNoDifs the duration of a EIFS minus the duration of DIFS.
   *
   * It is a bad idea to call this method after RequestAccess or
   * one of the Notify methods has been invoked.
   */
  void SetEifsNoDifs(Time eifsNoDifs);

  /**
   * \return value set previously using SetEifsNoDifs.
   */
  Time GetEifsNoDifs() const;

  /**
   * \param txop a new WigigTxop.
   *
   * The WigigChannelAccessManager does not take ownership of this pointer so,
   * the callee must make sure that the WigigTxop pointer will stay valid as
   * long as the WigigChannelAccessManager is valid. Note that the order in
   * which WigigTxop objects are added to a WigigChannelAccessManager matters:
   * the first WigigTxop added has the highest priority, the second WigigTxop
   * added, has the second highest priority, etc.
   */
  void Add(Ptr<WigigTxop> txop);

  /**
   * Determine if a new backoff needs to be generated when a packet is queued
   * for transmission.
   *
   * \param txop the WigigTxop requesting to generate a backoff
   * \return true if backoff needs to be generated, false otherwise
   */
  bool NeedBackoffUponAccess(Ptr<WigigTxop> txop);

  /**
   * \param txop a WigigTxop
   * \param isCfPeriod flag whether it is called during the CF period
   *
   * Notify the WigigChannelAccessManager that a specific WigigTxop needs access
   * to the medium. The WigigChannelAccessManager is then responsible for
   * starting an access timer and, invoking WigigTxop::DoNotifyAccessGranted
   * when the access is granted if it ever gets granted.
   */
  void RequestAccess(Ptr<WigigTxop> txop, bool isCfPeriod = false);

  /**
   * Check if we are receiving any packet.
   * \return true if we receiving packet.
   */
  bool IsReceiving() const;
  /**
   * \param duration expected duration of reception
   *
   * Notify the WigigTxop that a packet reception started
   * for the expected duration.
   */
  void NotifyRxStartNow(Time duration);
  /**
   * Notify the WigigTxop that a packet reception was just
   * completed successfully.
   */
  void NotifyRxEndOkNow();
  /**
   * Notify the WigigTxop that a packet reception was just
   * completed unsuccessfully.
   */
  void NotifyRxEndErrorNow();
  /**
   * \param duration expected duration of transmission
   *
   * Notify the WigigTxop that a packet transmission was
   * just started and is expected to last for the specified
   * duration.
   */
  void NotifyTxStartNow(Time duration);
  /**
   * \param duration expected duration of CCA busy period
   *
   * Notify the WigigTxop that a CCA busy period has just started.
   */
  void NotifyCcaBusyStartNow(Time duration);
  /**
   * \param duration expected duration of channel switching period
   *
   * Notify the WigigTxop that a channel switching period has just started.
   * During switching state, new packets can be enqueued in
   * WigigTxop/WigigQosTxop but they won't access to the medium until the end of
   * the channel switching.
   */
  void NotifySwitchingStartNow(Time duration);
  /**
   * Notify the WigigTxop that the device has been put in sleep mode.
   */
  void NotifySleepNow();
  /**
   * Notify the WigigTxop that the device has been put in off mode.
   */
  void NotifyOffNow();
  /**
   * Notify the WigigTxop that the device has been resumed from sleep mode.
   */
  void NotifyWakeupNow();
  /**
   * Notify the WigigTxop that the device has been resumed from off mode.
   */
  void NotifyOnNow();
  /**
   * \param duration the value of the received NAV.
   *
   * Called at end of RX
   */
  void NotifyNavResetNow(Time duration);
  /**
   * \param duration the value of the received NAV.
   *
   * Called at end of RX
   */
  void NotifyNavStartNow(Time duration);
  /**
   * Notify that ack timer has started for the given duration.
   *
   * \param duration the duration of the timer
   */
  void NotifyAckTimeoutStartNow(Time duration);
  /**
   * Notify that ack timer has reset.
   */
  void NotifyAckTimeoutResetNow();
  /**
   * Notify that CTS timer has started for the given duration.
   *
   * \param duration the duration of the timer
   */
  void NotifyCtsTimeoutStartNow(Time duration);
  /**
   * Notify that CTS timer has reset.
   */
  void NotifyCtsTimeoutResetNow();

  /**
   * Check if the device is busy sending or receiving,
   * or NAV or CCA busy.
   *
   * \return true if the device is busy,
   *         false otherwise
   */
  bool IsBusy() const;

  /* This is used only for EDCA contention */
  void AllowChannelAccess();
  void DisableChannelAccess();
  bool IsAccessAllowed() const;
  bool CanAccess() const;

private:
  void DoDispose() override;

  /**
   * Update backoff slots for all WigigTxops.
   */
  void UpdateBackoff();
  /**
   * Return the most recent time.
   *
   * \param list the initializer list including the times to compare
   *
   * \return the most recent time
   */
  Time MostRecent(std::initializer_list<Time> list) const;
  /**
   * Access will never be granted to the medium _before_
   * the time returned by this method.
   *
   * \param ignoreNav flag whether NAV should be ignored
   *
   * \returns the absolute time at which access could start to be granted
   */
  Time GetAccessGrantStart(bool ignoreNav = false) const;
  /**
   * Return the time when the backoff procedure
   * started for the given WigigTxop.
   *
   * \param txop the WigigTxop
   *
   * \return the time when the backoff procedure started
   */
  Time GetBackoffStartFor(Ptr<WigigTxop> txop);
  /**
   * Return the time when the backoff procedure
   * ended (or will ended) for the given WigigTxop.
   *
   * \param txop the WigigTxop
   *
   * \return the time when the backoff procedure ended (or will ended)
   */
  Time GetBackoffEndFor(Ptr<WigigTxop> txop);

  void DoRestartAccessTimeoutIfNeeded();

  /**
   * Called when access timeout should occur
   * (e.g. backoff procedure expired).
   */
  void AccessTimeout();
  /**
   * Grant access to WigigTxop using DCF/EDCF contention rules
   */
  void DoGrantDcfAccess();
  /**
   * Grant access to WigigTxop using PCF preemption
   *
   * \param txop the WigigTxop
   */
  void DoGrantPcfAccess(Ptr<WigigTxop> txop);

  /**
   * typedef for a vector of WigigTxops
   */
  typedef std::vector<Ptr<WigigTxop>> WigigTxops;

  WigigTxops m_txops;           //!< the vector of managed WigigTxops
  Time m_lastAckTimeoutEnd;     //!< the last Ack timeout end time
  Time m_lastCtsTimeoutEnd;     //!< the last CTS timeout end time
  Time m_lastNavStart;          //!< the last NAV start time
  Time m_lastNavDuration;       //!< the last NAV duration time
  Time m_lastRxStart;           //!< the last receive start time
  Time m_lastRxDuration;        //!< the last receive duration time
  bool m_lastRxReceivedOk;      //!< the last receive OK
  Time m_lastRxEnd;             //!< the last receive end time
  Time m_lastTxStart;           //!< the last transmit start time
  Time m_lastTxDuration;        //!< the last transmit duration time
  Time m_lastBusyStart;         //!< the last busy start time
  Time m_lastBusyDuration;      //!< the last busy duration time
  Time m_lastSwitchingStart;    //!< the last switching start time
  Time m_lastSwitchingDuration; //!< the last switching duration time
  bool m_sleeping;              //!< flag whether it is in sleeping state
  bool m_off;                   //!< flag whether it is in off state
  Time m_eifsNoDifs;            //!< EIFS no DIFS time
  EventId m_accessTimeout;      //!< the access timeout ID
  Time m_slot;                  //!< the slot time
  Time m_sifs;                  //!< the SIFS time
  PhyListener *m_phyListener;   //!< the PHY listener
  Ptr<WigigPhy> m_phy;          //!< pointer to the PHY
  bool m_accessAllowed;         //!< Access allowed by WigigTxop instances
};

} // namespace ns3

#endif /* WIGIG_CHANNEL_ACCESS_MANAGER_H */
