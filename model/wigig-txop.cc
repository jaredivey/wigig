/*
 * Copyright (c) 2005 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "wigig-txop.h"

#include "mac-low.h"
#include "wigig-channel-access-manager.h"
#include "wigig-mac-queue.h"
#include "wigig-remote-station-manager.h"

#include "ns3/log.h"
#include "ns3/mac-tx-middle.h"
#include "ns3/pointer.h"
#include "ns3/random-variable-stream.h"
#include "ns3/simulator.h"
#include "ns3/socket.h"
#include "ns3/wifi-mac-trailer.h"

#undef NS_LOG_APPEND_CONTEXT
#define NS_LOG_APPEND_CONTEXT                                                  \
  if (m_low) {                                                                 \
    std::clog << "[mac=" << m_low->GetAddress() << "] ";                       \
  }

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigTxop");

NS_OBJECT_ENSURE_REGISTERED(WigigTxop);

TypeId WigigTxop::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigTxop")
          .SetParent<ns3::Object>()
          .SetGroupName("Wigig")
          .AddConstructor<WigigTxop>()
          .AddAttribute(
              "MinCw", "The minimum value of the contention window.",
              UintegerValue(15),
              MakeUintegerAccessor(&WigigTxop::SetMinCw, &WigigTxop::GetMinCw),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "MaxCw", "The maximum value of the contention window.",
              UintegerValue(1023),
              MakeUintegerAccessor(&WigigTxop::SetMaxCw, &WigigTxop::GetMaxCw),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "Aifsn", "The AIFSN: the default value conforms to non-QOS.",
              UintegerValue(2),
              MakeUintegerAccessor(&WigigTxop::SetAifsn, &WigigTxop::GetAifsn),
              MakeUintegerChecker<uint8_t>())
          .AddAttribute(
              "TxopLimit",
              "The TXOP limit: the default value conforms to non-QoS.",
              TimeValue(MilliSeconds(0)),
              MakeTimeAccessor(&WigigTxop::SetTxopLimit,
                               &WigigTxop::GetTxopLimit),
              MakeTimeChecker())
          .AddAttribute("Queue", "The WigigMacQueue object", PointerValue(),
                        MakePointerAccessor(&WigigTxop::GetWigigMacQueue),
                        MakePointerChecker<WigigMacQueue>())
          .AddTraceSource("BackoffTrace", "Trace source for backoff values",
                          MakeTraceSourceAccessor(&WigigTxop::m_backoffTrace),
                          "ns3::TracedCallback::Uint32Callback")
          .AddTraceSource("CwTrace",
                          "Trace source for contention window values",
                          MakeTraceSourceAccessor(&WigigTxop::m_cwTrace),
                          "ns3::TracedValueCallback::Uint32");
  return tid;
}

WigigTxop::WigigTxop()
    : m_channelAccessManager(nullptr), m_txMiddle(nullptr), m_low(nullptr),
      m_stationManager(nullptr), m_cwMin(0), m_cwMax(0), m_cw(0), m_backoff(0),
      m_accessRequested(false), m_backoffSlots(0), m_backoffStart(Seconds(0.0)),
      m_txopLimit(Seconds(0.0)), m_currentPacket(nullptr), m_fragmentNumber(0),
      m_allocationId(0), m_allocationType(CBAP_ALLOCATION),
      m_allocationStart(Seconds(0)), m_allocationDuration(Seconds(0)),
      m_firstTransmission(false), m_accessAllowed(false) {
  NS_LOG_FUNCTION(this);
  m_queue = CreateObject<WigigMacQueue>();
  m_rng = CreateObject<UniformRandomVariable>();
}

WigigTxop::~WigigTxop() { NS_LOG_FUNCTION(this); }

void WigigTxop::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_queue = nullptr;
  m_low = nullptr;
  m_stationManager = nullptr;
  m_rng = nullptr;
  m_txMiddle = nullptr;
  m_channelAccessManager = nullptr;
}

void WigigTxop::SetChannelAccessManager(
    const Ptr<WigigChannelAccessManager> manager) {
  NS_LOG_FUNCTION(this << manager);
  m_channelAccessManager = manager;
  m_channelAccessManager->Add(this);
}

void WigigTxop::SetTxMiddle(const Ptr<MacTxMiddle> txMiddle) {
  NS_LOG_FUNCTION(this);
  m_txMiddle = txMiddle;
}

void WigigTxop::SetMacLow(const Ptr<MacLow> low) {
  NS_LOG_FUNCTION(this << low);
  m_low = low;
}

void WigigTxop::SetWifiRemoteStationManager(
    const Ptr<WigigRemoteStationManager> remoteManager) {
  NS_LOG_FUNCTION(this << remoteManager);
  m_stationManager = remoteManager;
}

void WigigTxop::SetTxOkCallback(TxPacketOk callback) {
  NS_LOG_FUNCTION(this << &callback);
  m_txOkCallback = callback;
}

void WigigTxop::SetTxOkNoAckCallback(TxOk callback) {
  NS_LOG_FUNCTION(this << &callback);
  m_txOkNoAckCallback = callback;
}

void WigigTxop::SetTxFailedCallback(TxFailed callback) {
  NS_LOG_FUNCTION(this << &callback);
  m_txFailedCallback = callback;
}

void WigigTxop::SetTxDroppedCallback(TxDropped callback) {
  NS_LOG_FUNCTION(this << &callback);
  m_txDroppedCallback = callback;
  m_queue->TraceConnectWithoutContext(
      "Drop", MakeCallback(&WigigTxop::TxDroppedPacket, this));
}

void WigigTxop::TxDroppedPacket(Ptr<const WigigMacQueueItem> item) {
  if (!m_txDroppedCallback.IsNull()) {
    m_txDroppedCallback(item->GetPacket());
  }
}

Ptr<WigigMacQueue> WigigTxop::GetWigigMacQueue() const {
  NS_LOG_FUNCTION(this);
  return m_queue;
}

void WigigTxop::SetMinCw(uint32_t minCw) {
  NS_LOG_FUNCTION(this << minCw);
  bool changed = (m_cwMin != minCw);
  m_cwMin = minCw;
  if (changed) {
    ResetCw();
    m_cwTrace = GetCw();
  }
}

void WigigTxop::SetMaxCw(uint32_t maxCw) {
  NS_LOG_FUNCTION(this << maxCw);
  bool changed = (m_cwMax != maxCw);
  m_cwMax = maxCw;
  if (changed) {
    ResetCw();
    m_cwTrace = GetCw();
  }
}

uint32_t WigigTxop::GetCw() const { return m_cw; }

void WigigTxop::ResetCw() {
  NS_LOG_FUNCTION(this);
  m_cw = m_cwMin;
}

void WigigTxop::UpdateFailedCw() {
  NS_LOG_FUNCTION(this);
  // see 802.11-2012, section 9.19.2.5
  m_cw = std::min(2 * (m_cw + 1) - 1, m_cwMax);
}

uint32_t WigigTxop::GetBackoffSlots() const { return m_backoffSlots; }

Time WigigTxop::GetBackoffStart() const { return m_backoffStart; }

void WigigTxop::UpdateBackoffSlotsNow(uint32_t nSlots,
                                      Time backoffUpdateBound) {
  NS_LOG_FUNCTION(this << nSlots << backoffUpdateBound);
  m_backoffSlots -= nSlots;
  m_backoffStart = backoffUpdateBound;
  NS_LOG_DEBUG("update slots=" << nSlots
                               << " slots, backoff=" << m_backoffSlots);
}

void WigigTxop::StartBackoffNow(uint32_t nSlots) {
  NS_LOG_FUNCTION(this << nSlots);
  if (m_backoffSlots != 0) {
    NS_LOG_DEBUG("reset backoff from " << m_backoffSlots << " to " << nSlots
                                       << " slots");
  } else {
    NS_LOG_DEBUG("start backoff=" << nSlots << " slots");
  }
  m_backoffSlots = nSlots;
  m_backoffStart = Simulator::Now();
}

void WigigTxop::SetAifsn(uint8_t aifsn) {
  NS_LOG_FUNCTION(this << +aifsn);
  m_aifsn = aifsn;
}

void WigigTxop::SetTxopLimit(Time txopLimit) {
  NS_LOG_FUNCTION(this << txopLimit);
  NS_ASSERT_MSG(
      (txopLimit.GetMicroSeconds() % 32 == 0),
      "The TXOP limit must be expressed in multiple of 32 microseconds!");
  m_txopLimit = txopLimit;
}

uint32_t WigigTxop::GetMinCw() const { return m_cwMin; }

uint32_t WigigTxop::GetMaxCw() const { return m_cwMax; }

uint8_t WigigTxop::GetAifsn() const { return m_aifsn; }

Time WigigTxop::GetTxopLimit() const { return m_txopLimit; }

bool WigigTxop::HasFramesToTransmit() {
  bool ret = (m_currentPacket || !m_queue->IsEmpty());
  NS_LOG_FUNCTION(this << ret);
  return ret;
}

void WigigTxop::ResetState() {
  NS_LOG_FUNCTION(this);
  ResetCw();
  m_cwTrace = GetCw();
  GenerateBackoff();
}

void WigigTxop::SetAllocationType(AllocationType allocationType) {
  NS_LOG_FUNCTION(this << +allocationType);
  m_allocationType = allocationType;
}

Time WigigTxop::GetAllocationRemaining() const {
  Time remainingAllocation = m_allocationDuration;
  remainingAllocation -= (Simulator::Now() - m_allocationStart);
  if (remainingAllocation.IsStrictlyNegative()) {
    remainingAllocation = Seconds(0);
  }
  NS_LOG_FUNCTION(this << remainingAllocation);
  return remainingAllocation;
}

Time WigigTxop::GetRemainingTimeForTransmission() {
  Time remainingTime = Seconds(0);
  if (m_stationManager->IsWiGigSupported()) {
    if (GetTxopLimit().IsStrictlyPositive() &&
        GetAllocationRemaining() >= GetWigigTxopRemaining()) {
      /* The remaining allocation duration should always be larger than the
       * WigigTxop remaining*/
      remainingTime = GetWigigTxopRemaining();
    } else if (GetTxopLimit().IsZero()) {
      /* Our limitation here is the remaining duration in the current allocation
       * period. */
      remainingTime = GetAllocationRemaining();
    }
  } else if (GetTxopLimit().IsStrictlyPositive()) {
    /* We only have WigigTxop limitation */
    remainingTime = GetWigigTxopRemaining();
  }
  return remainingTime;
}

void WigigTxop::Queue(Ptr<Packet> packet, const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this << packet << &hdr);
  // remove the priority tag attached, if any
  SocketPriorityTag priorityTag;
  packet->RemovePacketTag(priorityTag);
  if (m_channelAccessManager->NeedBackoffUponAccess(this)) {
    GenerateBackoff();
  }
  m_queue->Enqueue(Create<WigigMacQueueItem>(packet, hdr));
  StartAccessIfNeeded();
}

int64_t WigigTxop::AssignStreams(int64_t stream) {
  NS_LOG_FUNCTION(this << stream);
  m_rng->SetStream(stream);
  return 1;
}

void WigigTxop::RestartAccessIfNeeded() {
  NS_LOG_FUNCTION(this);
  if ((m_currentPacket || !m_queue->IsEmpty()) && !IsAccessRequested() &&
      m_channelAccessManager->IsAccessAllowed()) {
    m_channelAccessManager->RequestAccess(this);
  }
}

void WigigTxop::StartAccessIfNeeded() {
  NS_LOG_FUNCTION(this);
  if (!m_currentPacket && !m_queue->IsEmpty() && !IsAccessRequested() &&
      m_channelAccessManager->IsAccessAllowed()) {
    m_channelAccessManager->RequestAccess(this);
  }
}

void WigigTxop::StartAllocationPeriod(AllocationType allocationType,
                                      AllocationId allocationId,
                                      Mac48Address peerStation,
                                      Time allocationDuration) {
  NS_LOG_FUNCTION(this << allocationType << +allocationId << peerStation
                       << allocationDuration);
  m_allocationType = allocationType;
  m_allocationId = allocationId;
  m_peerStation = peerStation;
  m_allocationDuration = allocationDuration;
  m_allocationStart = Simulator::Now();
  ResetState();

  /* Check if we have stored packet for this allocation period */
  StoredPacketsCI it = m_storedPackets.find(m_allocationId);
  if (it != m_storedPackets.end()) {
    PacketInformation info = it->second;
    m_currentPacket = info.first;
    m_currentHdr = info.second;
  }

  StartAccessIfNeeded();
}

void WigigTxop::ResumeTxopTransmission() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(m_allocationType == CBAP_ALLOCATION,
                "Allocation type must be CBAP.");
  ResetState();
  RestartAccessIfNeeded();
}

void WigigTxop::EndAllocationPeriod() {
  NS_LOG_FUNCTION(this);
  m_accessAllowed = false;
  /* Store parameters related to this Allocation period which include
   * MSDU/A-MSDU */
  if (m_currentPacket) {
    NS_LOG_DEBUG("Store packet with seq=0x"
                 << std::hex << m_currentHdr.GetSequenceControl()
                 << " for AllocationId=" << std::dec << +m_allocationId);
    m_storedPackets[m_allocationId] =
        std::make_pair(m_currentPacket, m_currentHdr);
    m_currentPacket = nullptr;
  } else {
    StoredPacketsI it = m_storedPackets.find(m_allocationId);
    if (it != m_storedPackets.end()) {
      m_storedPackets.erase(it);
    }
  }
}

Ptr<MacLow> WigigTxop::GetLow() const { return m_low; }

void WigigTxop::DoInitialize() {
  NS_LOG_FUNCTION(this);
  ResetCw();
  m_cwTrace = GetCw();
  GenerateBackoff();
}

bool WigigTxop::NeedRtsRetransmission(Ptr<const Packet> packet,
                                      const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  return m_stationManager->NeedRetransmission(hdr.GetAddr1(), &hdr, packet);
}

bool WigigTxop::NeedDataRetransmission(Ptr<const Packet> packet,
                                       const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  return m_stationManager->NeedRetransmission(hdr.GetAddr1(), &hdr, packet);
}

bool WigigTxop::NeedFragmentation() const {
  NS_LOG_FUNCTION(this);
  return m_stationManager->NeedFragmentation(m_currentHdr.GetAddr1(),
                                             &m_currentHdr, m_currentPacket);
}

void WigigTxop::NextFragment() {
  NS_LOG_FUNCTION(this);
  m_fragmentNumber++;
}

uint32_t WigigTxop::GetFragmentSize() const {
  NS_LOG_FUNCTION(this);
  return m_stationManager->GetFragmentSize(m_currentHdr.GetAddr1(),
                                           &m_currentHdr, m_currentPacket,
                                           m_fragmentNumber);
}

bool WigigTxop::IsLastFragment() const {
  NS_LOG_FUNCTION(this);
  return m_stationManager->IsLastFragment(m_currentHdr.GetAddr1(),
                                          &m_currentHdr, m_currentPacket,
                                          m_fragmentNumber);
}

uint32_t WigigTxop::GetNextFragmentSize() const {
  NS_LOG_FUNCTION(this);
  return m_stationManager->GetFragmentSize(m_currentHdr.GetAddr1(),
                                           &m_currentHdr, m_currentPacket,
                                           m_fragmentNumber + 1);
}

uint32_t WigigTxop::GetFragmentOffset() const {
  NS_LOG_FUNCTION(this);
  return m_stationManager->GetFragmentOffset(m_currentHdr.GetAddr1(),
                                             &m_currentHdr, m_currentPacket,
                                             m_fragmentNumber);
}

Time WigigTxop::GetPpduDurationLimit(
    Ptr<const WigigMacQueueItem> item,
    const MacLowTransmissionParameters &params) {
  Time ppduDurationLimit = Time::Min();
  if (m_stationManager->IsWiGigSupported()) {
    ppduDurationLimit =
        GetAllocationRemaining() - m_low->CalculateOverheadTxTime(item, params);
  }
  return ppduDurationLimit;
}

Ptr<Packet> WigigTxop::GetFragmentPacket(WigigMacHeader *hdr) {
  NS_LOG_FUNCTION(this << hdr);
  *hdr = m_currentHdr;
  hdr->SetFragmentNumber(m_fragmentNumber);
  uint32_t startOffset = GetFragmentOffset();
  Ptr<Packet> fragment;
  if (IsLastFragment()) {
    hdr->SetNoMoreFragments();
  } else {
    hdr->SetMoreFragments();
  }
  fragment = m_currentPacket->CreateFragment(startOffset, GetFragmentSize());
  return fragment;
}

bool WigigTxop::IsAccessRequested() const { return m_accessRequested; }

void WigigTxop::NotifyAccessRequested() {
  NS_LOG_FUNCTION(this);
  m_accessRequested = true;
}

void WigigTxop::NotifyAccessGranted() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT(m_accessRequested);
  m_accessRequested = false;
  if (m_stationManager->HasDmgSupported()) {
    if (GetAllocationRemaining().IsZero()) {
      NS_LOG_DEBUG("No more time in the current CBAP Allocation");
      return;
    }
    if (m_low->IsPerformingSls()) {
      NS_LOG_DEBUG("Performing SLS, abort frames transmission.");
      return;
    }
  }
  if (!m_currentPacket) {
    if (m_queue->IsEmpty()) {
      NS_LOG_DEBUG("queue empty");
      return;
    }
    Ptr<WigigMacQueueItem> item = m_queue->Dequeue();
    NS_ASSERT(item);
    m_currentPacket = item->GetPacket();
    m_currentHdr = item->GetHeader();
    NS_ASSERT(m_currentPacket);
    uint16_t sequence = m_txMiddle->GetNextSequenceNumberFor(&m_currentHdr);
    m_currentHdr.SetSequenceNumber(sequence);
    m_stationManager->UpdateFragmentationThreshold();
    m_currentHdr.SetFragmentNumber(0);
    m_currentHdr.SetNoMoreFragments();
    m_currentHdr.SetNoRetry();
    m_fragmentNumber = 0;
    NS_LOG_DEBUG("dequeued size=" << m_currentPacket->GetSize() << ", to="
                                  << m_currentHdr.GetAddr1() << ", seq="
                                  << m_currentHdr.GetSequenceControl());
  }
  if (m_currentHdr.GetAddr1().IsGroup() || m_currentHdr.IsActionNoAck()) {
    m_currentParams.DisableRts();
    m_currentParams.DisableAck();
    m_currentParams.DisableNextData();
    NS_LOG_DEBUG("tx broadcast");
    GetLow()->StartTransmission(
        Create<WigigMacQueueItem>(m_currentPacket, m_currentHdr),
        m_currentParams, this);
  } else {
    m_currentParams.EnableAck();
    if (NeedFragmentation()) {
      m_currentParams.DisableRts();
      WigigMacHeader hdr;
      Ptr<Packet> fragment = GetFragmentPacket(&hdr);
      if (IsLastFragment()) {
        NS_LOG_DEBUG("fragmenting last fragment size=" << fragment->GetSize());
        m_currentParams.DisableNextData();
      } else {
        NS_LOG_DEBUG("fragmenting size=" << fragment->GetSize());
        m_currentParams.EnableNextData(GetNextFragmentSize());
      }
      GetLow()->StartTransmission(Create<WigigMacQueueItem>(fragment, hdr),
                                  m_currentParams, this);
    } else {
      uint32_t size = m_currentHdr.GetSize() + m_currentPacket->GetSize() +
                      WIFI_MAC_FCS_LENGTH;
      if (m_stationManager->NeedRts(m_currentHdr, size)) {
        m_currentParams.EnableRts();
      } else {
        m_currentParams.DisableRts();
      }
      m_currentParams.DisableNextData();
      GetLow()->StartTransmission(
          Create<WigigMacQueueItem>(m_currentPacket, m_currentHdr),
          m_currentParams, this);
    }
  }
}

void WigigTxop::GenerateBackoff() {
  NS_LOG_FUNCTION(this);
  m_backoff = m_rng->GetInteger(0, GetCw());
  m_backoffTrace(m_backoff);
  StartBackoffNow(m_backoff);
}

void WigigTxop::NotifyInternalCollision() {
  NS_LOG_FUNCTION(this);
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void WigigTxop::NotifyChannelSwitching() {
  NS_LOG_FUNCTION(this);
  m_queue->Flush();
  m_currentPacket = nullptr;
}

void WigigTxop::NotifySleep() {
  NS_LOG_FUNCTION(this);
  if (m_currentPacket) {
    m_queue->PushFront(
        Create<WigigMacQueueItem>(m_currentPacket, m_currentHdr));
    m_currentPacket = nullptr;
  }
}

void WigigTxop::NotifyOff() {
  NS_LOG_FUNCTION(this);
  m_queue->Flush();
  m_currentPacket = nullptr;
}

void WigigTxop::NotifyWakeUp() {
  NS_LOG_FUNCTION(this);
  RestartAccessIfNeeded();
}

void WigigTxop::NotifyOn() {
  NS_LOG_FUNCTION(this);
  StartAccessIfNeeded();
}

void WigigTxop::MissedCts() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("missed cts");
  if (!NeedRtsRetransmission(m_currentPacket, m_currentHdr)) {
    NS_LOG_DEBUG("Cts Fail");
    m_stationManager->ReportFinalRtsFailed(m_currentHdr.GetAddr1(),
                                           &m_currentHdr);
    if (!m_txFailedCallback.IsNull()) {
      m_txFailedCallback(m_currentHdr);
    }
    // to reset the WigigTxop.
    m_currentPacket = nullptr;
    ResetCw();
    m_cwTrace = GetCw();
  } else {
    UpdateFailedCw();
    m_cwTrace = GetCw();
  }
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void WigigTxop::GotAck() {
  NS_LOG_FUNCTION(this);
  if (!NeedFragmentation() || IsLastFragment()) {
    NS_LOG_DEBUG("got ack. tx done.");
    if (!m_txOkCallback.IsNull()) {
      m_txOkCallback(m_currentPacket, m_currentHdr);
    }

    /* we are not fragmenting or we are done fragmenting
     * so we can get rid of that packet now.
     */
    m_currentPacket = nullptr;
    ResetCw();
    m_cwTrace = GetCw();
    GenerateBackoff();
    RestartAccessIfNeeded();
  } else {
    NS_LOG_DEBUG("got ack. tx not done, size=" << m_currentPacket->GetSize());
  }
}

void WigigTxop::MissedAck() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("missed ack");
  if (!NeedDataRetransmission(m_currentPacket, m_currentHdr)) {
    NS_LOG_DEBUG("Ack Fail");
    m_stationManager->ReportFinalDataFailed(
        m_currentHdr.GetAddr1(), &m_currentHdr, m_currentPacket->GetSize());
    if (!m_txFailedCallback.IsNull()) {
      m_txFailedCallback(m_currentHdr);
    }
    // to reset the WigigTxop.
    m_currentPacket = nullptr;
    ResetCw();
    m_cwTrace = GetCw();
  } else {
    NS_LOG_DEBUG("Retransmit");
    m_stationManager->ReportDataFailed(m_currentHdr.GetAddr1(), &m_currentHdr,
                                       m_currentPacket->GetSize());
    m_currentHdr.SetRetry();
    UpdateFailedCw();
    m_cwTrace = GetCw();
  }
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void WigigTxop::StartNextFragment() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("start next packet fragment");
  /* this callback is used only for fragments. */
  NextFragment();
  WigigMacHeader hdr;
  Ptr<Packet> fragment = GetFragmentPacket(&hdr);
  m_currentParams.EnableAck();
  m_currentParams.DisableRts();
  m_currentParams.DisableOverrideDurationId();
  if (IsLastFragment()) {
    m_currentParams.DisableNextData();
  } else {
    m_currentParams.EnableNextData(GetNextFragmentSize());
  }
  GetLow()->StartTransmission(Create<WigigMacQueueItem>(fragment, hdr),
                              m_currentParams, this);
}

void WigigTxop::Cancel() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("transmission cancelled");
}

void WigigTxop::EndTxNoAck() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("a transmission that did not require an ACK just finished");
  m_currentPacket = nullptr;
  ResetCw();
  m_cwTrace = GetCw();
  GenerateBackoff();
  if (!m_txOkNoAckCallback.IsNull()) {
    m_txOkNoAckCallback(m_currentHdr);
  }
  StartAccessIfNeeded();
}

bool WigigTxop::IsWigigQosTxop() const { return false; }

void WigigTxop::StartNextPacket() {
  NS_LOG_WARN("StartNext should not be called for non QoS!");
}

void WigigTxop::GotBlockAck(const CtrlBAckResponseHeader *blockAck,
                            Mac48Address recipient, double rxSnr,
                            double dataSnr, const WigigTxVector &dataTxVector) {
  NS_LOG_WARN("GotBlockAck should not be called for non QoS!");
}

void WigigTxop::MissedBlockAck(uint8_t nMpdus) {
  NS_LOG_WARN("MissedBlockAck should not be called for non QoS!");
}

Time WigigTxop::GetWigigTxopRemaining() const {
  NS_LOG_WARN("GetWigigTxopRemaining should not be called for non QoS!");
  return Seconds(0);
}

void WigigTxop::TerminateWigigTxop() {
  NS_LOG_WARN("TerminateWigigTxop should not be called for non QoS!");
}

} // namespace ns3
