/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef DMG_BEACON_TXOP_H
#define DMG_BEACON_TXOP_H

#include "ext-headers.h"
#include "wigig-txop.h"

#include "ns3/callback.h"
#include "ns3/nstime.h"
#include "ns3/object.h"
#include "ns3/packet.h"

#include <stdint.h>

namespace ns3 {

class DmgBeaconTxop : public WigigTxop {
public:
  static TypeId GetTypeId();

  DmgBeaconTxop();
  ~DmgBeaconTxop() override;

  /**
   * Transmit single DMG Beacon..
   * \param packet The DMG Beacon packet.
   * \param hdr header of packet to send.
   * \param btiRemainingTime The remaining time in BTI access period.
   */
  void TransmitDmgBeacon(Ptr<Packet> packet, const WigigMacHeader &hdr,
                         Time btiRemainingTime);
  /**
   * Perform Clear Channel Assessment Procedure.
   */
  void PerformCca();
  /**
   * typedef for a callback to invoke when a
   * packet transmission was completed successfully.
   */
  typedef Callback<void> AccessGranted;
  /**
   * CCA procedure is completed and access is granted.
   * \param callback the callback to invoke when access is granted
   */
  void SetAccessGrantedCallback(AccessGranted callback);

  DmgBeaconTxop &operator=(const DmgBeaconTxop &) = delete;
  DmgBeaconTxop(const DmgBeaconTxop &o) = delete;

private:
  void NotifyAccessGranted() override;
  void NotifyInternalCollision() override;
  void Cancel() override;
  void EndTxNoAck() override;
  void RestartAccessIfNeeded() override;

  AccessGranted m_accessGrantedCallback;
};

} // namespace ns3

#endif /* DMG_BEACON_TXOP_H */
