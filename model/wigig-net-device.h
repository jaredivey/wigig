/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef WIGIG_NET_DEVICE_H
#define WIGIG_NET_DEVICE_H

#include "ns3/traced-callback.h"
#include "ns3/wifi-net-device.h"

namespace ns3 {

class WigigRemoteStationManager;
class WigigPhy;
class WigigMac;

/**
 * \defgroup wifi Wifi Models
 *
 * This section documents the API of the ns-3 Wifi module. For a generic
 * functional description, please refer to the ns-3 manual.
 */

/**
 * \brief Hold together all Wifi-related objects.
 * \ingroup wigig
 *
 * This class holds together ns3::Channel, ns3::WigigPhy,
 * ns3::WigigMac, and, ns3::WigigRemoteStationManager.
 */
class WigigNetDevice : public NetDevice {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  WigigNetDevice();
  ~WigigNetDevice() override;

  /**
   * \param mac the MAC layer to use.
   */
  void SetMac(const Ptr<WigigMac> mac);
  /**
   * \param phy the PHY layer to use.
   */
  void SetPhy(const Ptr<WigigPhy> phy);
  /**
   * \param manager the manager to use.
   */
  void SetRemoteStationManager(const Ptr<WigigRemoteStationManager> manager);
  /**
   * \returns the MAC we are currently using.
   */
  Ptr<WigigMac> GetMac() const;
  /**
   * \returns the PHY we are currently using.
   */
  Ptr<WigigPhy> GetPhy() const;
  /**
   * \returns the remote station manager we are currently using.
   */
  Ptr<WigigRemoteStationManager> GetRemoteStationManager() const;

  void SetIfIndex(const uint32_t index) override;
  uint32_t GetIfIndex() const override;
  Ptr<Channel> GetChannel() const override;
  void SetAddress(Address address) override;
  Address GetAddress() const override;
  bool SetMtu(const uint16_t mtu) override;
  uint16_t GetMtu() const override;
  bool IsLinkUp() const override;
  void AddLinkChangeCallback(Callback<void> callback) override;
  bool IsBroadcast() const override;
  Address GetBroadcast() const override;
  bool IsMulticast() const override;
  Address GetMulticast(Ipv4Address multicastGroup) const override;
  bool IsPointToPoint() const override;
  bool IsBridge() const override;
  bool Send(Ptr<Packet> packet, const Address &dest,
            uint16_t protocolNumber) override;
  Ptr<Node> GetNode() const override;
  void SetNode(const Ptr<Node> node) override;
  bool NeedsArp() const override;
  void SetReceiveCallback(NetDevice::ReceiveCallback cb) override;
  Address GetMulticast(Ipv6Address addr) const override;
  bool SendFrom(Ptr<Packet> packet, const Address &source, const Address &dest,
                uint16_t protocolNumber) override;
  void SetPromiscReceiveCallback(PromiscReceiveCallback cb) override;
  bool SupportsSendFrom() const override;

  /**
   * \brief Copy constructor
   * \param o object to copy
   *
   * Defined and unimplemented to avoid misuse
   */
  WigigNetDevice(const WigigNetDevice &o) = delete;

  /**
   * \brief Assignment operator
   * \param o object to copy
   * \returns the copied object
   *
   * Defined and unimplemented to avoid misuse
   */
  WigigNetDevice &operator=(const WigigNetDevice &o) = delete;

protected:
  void DoDispose() override;
  void DoInitialize() override;
  /**
   * Receive a packet from the lower layer and pass the
   * packet up the stack.
   *
   * \param packet the packet to forward up
   * \param from the source address
   * \param to the destination address
   */
  void ForwardUp(Ptr<const Packet> packet, Mac48Address from, Mac48Address to);

private:
  /**
   * Set that the link is up. A link is always up in ad-hoc mode.
   * For a STA, a link is up when the STA is associated with an AP.
   */
  void LinkUp();
  /**
   * Set that the link is down (i.e. STA is not associated).
   */
  void LinkDown();
  /**
   * Complete the configuration of this Wi-Fi device by
   * connecting all lower components (e.g. MAC, WigigRemoteStation) together.
   */
  void CompleteConfig();

  Ptr<Node> m_node;                                //!< the node
  Ptr<WigigPhy> m_phy;                             //!< the phy
  Ptr<WigigMac> m_mac;                             //!< the MAC
  Ptr<WigigRemoteStationManager> m_stationManager; //!< the station manager
  NetDevice::ReceiveCallback m_forwardUp;          //!< forward up callback
  NetDevice::PromiscReceiveCallback
      m_promiscRx; //!< promiscuous receive callback

  TracedCallback<Ptr<const Packet>, Mac48Address>
      m_rxLogger; //!< receive trace callback
  TracedCallback<Ptr<const Packet>, Mac48Address>
      m_txLogger; //!< transmit trace callback

  uint32_t m_ifIndex;             //!< IF index
  bool m_linkUp;                  //!< link up
  TracedCallback<> m_linkChanges; //!< link change callback
  mutable uint16_t m_mtu;         //!< MTU
  bool m_configComplete;          //!< configuration complete
};

} // namespace ns3

#endif /* WIGIG_NET_DEVICE_H */
