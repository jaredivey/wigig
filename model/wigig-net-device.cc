/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "wigig-net-device.h"

#include "wigig-mac.h"
#include "wigig-phy.h"

#include "ns3/channel.h"
#include "ns3/llc-snap-header.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/pointer.h"
#include "ns3/uinteger.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigNetDevice");

NS_OBJECT_ENSURE_REGISTERED(WigigNetDevice);

TypeId WigigNetDevice::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigNetDevice")
          .SetParent<NetDevice>()
          .AddConstructor<WigigNetDevice>()
          .SetGroupName("Wigig")
          .AddAttribute("Mtu", "The MAC-level Maximum Transmission Unit",
                        UintegerValue(MAX_MSDU_SIZE - LLC_SNAP_HEADER_LENGTH),
                        MakeUintegerAccessor(&WigigNetDevice::SetMtu,
                                             &WigigNetDevice::GetMtu),
                        MakeUintegerChecker<uint16_t>(
                            1, MAX_MSDU_SIZE - LLC_SNAP_HEADER_LENGTH))
          .AddAttribute("Channel", "The channel attached to this device",
                        PointerValue(),
                        MakePointerAccessor(&WigigNetDevice::GetChannel),
                        MakePointerChecker<Channel>())
          .AddAttribute("Phy", "The PHY layer attached to this device.",
                        PointerValue(),
                        MakePointerAccessor(&WigigNetDevice::GetPhy,
                                            &WigigNetDevice::SetPhy),
                        MakePointerChecker<WigigPhy>())
          .AddAttribute("Mac", "The MAC layer attached to this device.",
                        PointerValue(),
                        MakePointerAccessor(&WigigNetDevice::GetMac,
                                            &WigigNetDevice::SetMac),
                        MakePointerChecker<WigigMac>())
          .AddAttribute(
              "RemoteStationManager",
              "The station manager attached to this device.", PointerValue(),
              MakePointerAccessor(&WigigNetDevice::SetRemoteStationManager,
                                  &WigigNetDevice::GetRemoteStationManager),
              MakePointerChecker<WigigRemoteStationManager>());
  return tid;
}

WigigNetDevice::WigigNetDevice()
    : m_node(nullptr), m_ifIndex(0), m_linkUp(false), m_configComplete(false) {
  NS_LOG_FUNCTION_NOARGS();
}

WigigNetDevice::~WigigNetDevice() { NS_LOG_FUNCTION_NOARGS(); }

void WigigNetDevice::DoDispose() {
  NS_LOG_FUNCTION_NOARGS();
  m_node = nullptr;
  if (m_mac) {
    m_mac->Dispose();
    m_mac = nullptr;
  }
  if (m_phy) {
    m_phy->Dispose();
    m_phy = nullptr;
  }
  if (m_stationManager) {
    m_stationManager->Dispose();
    m_stationManager = nullptr;
  }
  NetDevice::DoDispose();
}

void WigigNetDevice::DoInitialize() {
  NS_LOG_FUNCTION_NOARGS();
  if (m_phy) {
    m_phy->Initialize();
  }
  if (m_mac) {
    m_mac->Initialize();
  }
  if (m_stationManager) {
    m_stationManager->Initialize();
  }
  NetDevice::DoInitialize();
}

void WigigNetDevice::CompleteConfig() {
  if (!m_mac || !m_phy || !m_stationManager || !m_node || m_configComplete) {
    return;
  }
  m_mac->SetWifiRemoteStationManager(m_stationManager);
  m_mac->SetPhy(m_phy);
  m_mac->SetForwardUpCallback(MakeCallback(&WigigNetDevice::ForwardUp, this));
  m_mac->SetLinkUpCallback(MakeCallback(&WigigNetDevice::LinkUp, this));
  m_mac->SetLinkDownCallback(MakeCallback(&WigigNetDevice::LinkDown, this));
  m_stationManager->SetupPhy(m_phy);
  m_stationManager->SetupMac(m_mac);
  m_configComplete = true;
}

void WigigNetDevice::SetMac(const Ptr<WigigMac> mac) {
  m_mac = mac;
  CompleteConfig();
}

void WigigNetDevice::SetPhy(const Ptr<WigigPhy> phy) {
  m_phy = phy;
  CompleteConfig();
}

void WigigNetDevice::SetRemoteStationManager(
    const Ptr<WigigRemoteStationManager> manager) {
  m_stationManager = manager;
  CompleteConfig();
}

Ptr<WigigMac> WigigNetDevice::GetMac() const { return m_mac; }

Ptr<WigigPhy> WigigNetDevice::GetPhy() const { return m_phy; }

Ptr<WigigRemoteStationManager> WigigNetDevice::GetRemoteStationManager() const {
  return m_stationManager;
}

void WigigNetDevice::SetIfIndex(const uint32_t index) { m_ifIndex = index; }

uint32_t WigigNetDevice::GetIfIndex() const { return m_ifIndex; }

Ptr<Channel> WigigNetDevice::GetChannel() const { return m_phy->GetChannel(); }

void WigigNetDevice::SetAddress(Address address) {
  m_mac->SetAddress(Mac48Address::ConvertFrom(address));
}

Address WigigNetDevice::GetAddress() const { return m_mac->GetAddress(); }

bool WigigNetDevice::SetMtu(const uint16_t mtu) {
  if (mtu > MAX_MSDU_SIZE - LLC_SNAP_HEADER_LENGTH) {
    return false;
  }
  m_mtu = mtu;
  return true;
}

uint16_t WigigNetDevice::GetMtu() const { return m_mtu; }

bool WigigNetDevice::IsLinkUp() const { return m_phy && m_linkUp; }

void WigigNetDevice::AddLinkChangeCallback(Callback<void> callback) {
  m_linkChanges.ConnectWithoutContext(callback);
}

bool WigigNetDevice::IsBroadcast() const { return true; }

Address WigigNetDevice::GetBroadcast() const {
  return Mac48Address::GetBroadcast();
}

bool WigigNetDevice::IsMulticast() const { return true; }

Address WigigNetDevice::GetMulticast(Ipv4Address multicastGroup) const {
  return Mac48Address::GetMulticast(multicastGroup);
}

Address WigigNetDevice::GetMulticast(Ipv6Address addr) const {
  return Mac48Address::GetMulticast(addr);
}

bool WigigNetDevice::IsPointToPoint() const { return false; }

bool WigigNetDevice::IsBridge() const { return false; }

bool WigigNetDevice::Send(Ptr<Packet> packet, const Address &dest,
                          uint16_t protocolNumber) {
  NS_LOG_FUNCTION(this << packet << dest << protocolNumber);
  NS_ASSERT(Mac48Address::IsMatchingType(dest));

  Mac48Address realTo = Mac48Address::ConvertFrom(dest);

  LlcSnapHeader llc;
  llc.SetType(protocolNumber);
  packet->AddHeader(llc);

  m_mac->NotifyTx(packet);
  m_mac->Enqueue(packet, realTo);
  return true;
}

Ptr<Node> WigigNetDevice::GetNode() const { return m_node; }

void WigigNetDevice::SetNode(const Ptr<Node> node) {
  m_node = node;
  CompleteConfig();
}

bool WigigNetDevice::NeedsArp() const { return true; }

void WigigNetDevice::SetReceiveCallback(NetDevice::ReceiveCallback cb) {
  m_forwardUp = cb;
}

void WigigNetDevice::ForwardUp(Ptr<const Packet> packet, Mac48Address from,
                               Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << from << to);
  LlcSnapHeader llc;
  NetDevice::PacketType type;
  if (to.IsBroadcast()) {
    type = NetDevice::PACKET_BROADCAST;
  } else if (to.IsGroup()) {
    type = NetDevice::PACKET_MULTICAST;
  } else if (to == m_mac->GetAddress()) {
    type = NetDevice::PACKET_HOST;
  } else {
    type = NetDevice::PACKET_OTHERHOST;
  }

  Ptr<Packet> copy = packet->Copy();
  if (type != NetDevice::PACKET_OTHERHOST) {
    m_mac->NotifyRx(packet);
    copy->RemoveHeader(llc);
    m_forwardUp(this, copy, llc.GetType(), from);
  } else {
    copy->RemoveHeader(llc);
  }

  if (!m_promiscRx.IsNull()) {
    m_mac->NotifyPromiscRx(copy);
    m_promiscRx(this, copy, llc.GetType(), from, to, type);
  }
}

void WigigNetDevice::LinkUp() {
  m_linkUp = true;
  m_linkChanges();
}

void WigigNetDevice::LinkDown() {
  m_linkUp = false;
  m_linkChanges();
}

bool WigigNetDevice::SendFrom(Ptr<Packet> packet, const Address &source,
                              const Address &dest, uint16_t protocolNumber) {
  NS_LOG_FUNCTION(this << packet << source << dest << protocolNumber);
  NS_ASSERT(Mac48Address::IsMatchingType(dest));
  NS_ASSERT(Mac48Address::IsMatchingType(source));

  Mac48Address realTo = Mac48Address::ConvertFrom(dest);
  Mac48Address realFrom = Mac48Address::ConvertFrom(source);

  LlcSnapHeader llc;
  llc.SetType(protocolNumber);
  packet->AddHeader(llc);

  m_mac->NotifyTx(packet);
  m_mac->Enqueue(packet, realTo, realFrom);

  return true;
}

void WigigNetDevice::SetPromiscReceiveCallback(PromiscReceiveCallback cb) {
  m_promiscRx = cb;
  m_mac->SetPromisc();
}

bool WigigNetDevice::SupportsSendFrom() const {
  return m_mac->SupportsSendFrom();
}

} // namespace ns3
