/*
 * Copyright (c) 2004,2005 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "constant-rate-wigig-manager.h"

#include "wigig-phy-common.h"
#include "wigig-tx-vector.h"

#include "ns3/log.h"
#include "ns3/string.h"
#include "ns3/wifi-utils.h"

#define Min(a, b) ((a < b) ? a : b)

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("ConstantRateWigigManager");

NS_OBJECT_ENSURE_REGISTERED(ConstantRateWigigManager);

TypeId ConstantRateWigigManager::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::ConstantRateWigigManager")
          .SetParent<WigigRemoteStationManager>()
          .SetGroupName("Wigig")
          .AddConstructor<ConstantRateWigigManager>()
          .AddAttribute(
              "DataMode",
              "The transmission mode to use for every data packet transmission",
              StringValue("OfdmRate6Mbps"),
              MakeWifiModeAccessor(&ConstantRateWigigManager::m_dataMode),
              MakeWifiModeChecker())
          .AddAttribute(
              "ControlMode",
              "The transmission mode to use for every RTS packet transmission.",
              StringValue("OfdmRate6Mbps"),
              MakeWifiModeAccessor(&ConstantRateWigigManager::m_ctlMode),
              MakeWifiModeChecker());
  return tid;
}

void ConstantRateWigigManager::SetDataMode(WifiMode mode) { m_dataMode = mode; }

void ConstantRateWigigManager::SetControlMode(WifiMode mode) {
  m_ctlMode = mode;
}

ConstantRateWigigManager::ConstantRateWigigManager() { NS_LOG_FUNCTION(this); }

ConstantRateWigigManager::~ConstantRateWigigManager() { NS_LOG_FUNCTION(this); }

WigigRemoteStation *ConstantRateWigigManager::DoCreateStation() const {
  NS_LOG_FUNCTION(this);
  WigigRemoteStation *station = new WigigRemoteStation();
  return station;
}

void ConstantRateWigigManager::DoReportRxOk(WigigRemoteStation *station,
                                            double rxSnr, WifiMode txMode) {
  NS_LOG_FUNCTION(this << station << rxSnr << txMode);
}

void ConstantRateWigigManager::DoReportRtsFailed(WigigRemoteStation *station) {
  NS_LOG_FUNCTION(this << station);
}

void ConstantRateWigigManager::DoReportDataFailed(WigigRemoteStation *station) {
  NS_LOG_FUNCTION(this << station);
}

void ConstantRateWigigManager::DoReportRtsOk(WigigRemoteStation *st,
                                             double ctsSnr, WifiMode ctsMode,
                                             double rtsSnr) {
  NS_LOG_FUNCTION(this << st << ctsSnr << ctsMode << rtsSnr);
}

void ConstantRateWigigManager::DoReportDataOk(WigigRemoteStation *st,
                                              double ackSnr, WifiMode ackMode,
                                              double dataSnr,
                                              uint16_t dataChannelWidth,
                                              uint8_t dataNss) {
  NS_LOG_FUNCTION(this << st << ackSnr << ackMode << dataSnr << dataChannelWidth
                       << +dataNss);
}

void ConstantRateWigigManager::DoReportFinalRtsFailed(
    WigigRemoteStation *station) {
  NS_LOG_FUNCTION(this << station);
}

void ConstantRateWigigManager::DoReportFinalDataFailed(
    WigigRemoteStation *station) {
  NS_LOG_FUNCTION(this << station);
}

WigigTxVector
ConstantRateWigigManager::DoGetDataTxVector(WigigRemoteStation *st) {
  NS_LOG_FUNCTION(this << st);
  return WigigTxVector(
      m_dataMode, GetDefaultTxPowerLevel(),
      GetPreambleForTransmission(m_dataMode.GetModulationClass(), false),
      ConvertGuardIntervalToNanoSeconds(m_dataMode, false, NanoSeconds(0)), 1,
      1, 0, GetChannelWidthForTransmission(m_dataMode, GetChannelWidth(st)),
      GetAggregation(st), false);
}

WigigTxVector
ConstantRateWigigManager::DoGetRtsTxVector(WigigRemoteStation *st) {
  NS_LOG_FUNCTION(this << st);
  return WigigTxVector(
      m_ctlMode, GetDefaultTxPowerLevel(),
      GetPreambleForTransmission(m_ctlMode.GetModulationClass(), false),
      ConvertGuardIntervalToNanoSeconds(m_ctlMode, false, NanoSeconds(0)), 1, 1,
      0, GetChannelWidthForTransmission(m_ctlMode, GetChannelWidth(st)),
      GetAggregation(st), false);
}

} // namespace ns3
