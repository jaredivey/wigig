/*
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mirko Banchi <mk.banchi@gmail.com>
 */

#include "wigig-block-ack-agreement.h"

#include "ns3/log.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigBlockAckAgreement");

WigigBlockAckAgreement::WigigBlockAckAgreement(Mac48Address peer, uint8_t tid)
    : m_peer(peer), m_amsduSupported(0), m_blockAckPolicy(1), m_tid(tid),
      m_bufferSize(0), m_timeout(0), m_startingSeq(0), m_winEnd(0),
      m_inactivityEvent() {
  NS_LOG_FUNCTION(this << peer << +tid);
}

WigigBlockAckAgreement::~WigigBlockAckAgreement() {
  NS_LOG_FUNCTION(this);
  m_inactivityEvent.Cancel();
}

void WigigBlockAckAgreement::SetBufferSize(uint16_t bufferSize) {
  NS_LOG_FUNCTION(this << bufferSize);
  NS_ASSERT(bufferSize <= 1024);
  NS_ASSERT(bufferSize % 16 == 0);
  m_bufferSize = bufferSize;
}

void WigigBlockAckAgreement::SetTimeout(uint16_t timeout) {
  NS_LOG_FUNCTION(this << timeout);
  m_timeout = timeout;
}

void WigigBlockAckAgreement::SetStartingSequence(uint16_t seq) {
  NS_LOG_FUNCTION(this << seq);
  NS_ASSERT(seq < 4096);
  m_startingSeq = seq;
}

void WigigBlockAckAgreement::SetStartingSequenceControl(uint16_t seq) {
  NS_LOG_FUNCTION(this << seq);
  NS_ASSERT(((seq >> 4) & 0x0fff) < 4096);
  m_startingSeq = (seq >> 4) & 0x0fff;
}

void WigigBlockAckAgreement::SetImmediateBlockAck() {
  NS_LOG_FUNCTION(this);
  m_blockAckPolicy = 1;
}

void WigigBlockAckAgreement::SetDelayedBlockAck() {
  NS_LOG_FUNCTION(this);
  m_blockAckPolicy = 0;
}

void WigigBlockAckAgreement::SetAmsduSupport(bool supported) {
  NS_LOG_FUNCTION(this << supported);
  m_amsduSupported = supported;
}

uint8_t WigigBlockAckAgreement::GetTid() const { return m_tid; }

Mac48Address WigigBlockAckAgreement::GetPeer() const {
  NS_LOG_FUNCTION(this);
  return m_peer;
}

uint16_t WigigBlockAckAgreement::GetBufferSize() const { return m_bufferSize; }

uint16_t WigigBlockAckAgreement::GetTimeout() const { return m_timeout; }

uint16_t WigigBlockAckAgreement::GetStartingSequence() const {
  return m_startingSeq;
}

uint16_t WigigBlockAckAgreement::GetStartingSequenceControl() const {
  uint16_t seqControl = (m_startingSeq << 4) & 0xfff0;
  return seqControl;
}

bool WigigBlockAckAgreement::IsImmediateBlockAck() const {
  return (m_blockAckPolicy == 1);
}

bool WigigBlockAckAgreement::IsAmsduSupported() const {
  return m_amsduSupported == 1;
}

uint16_t WigigBlockAckAgreement::GetWinEnd() const {
  return (GetStartingSequence() + GetBufferSize() - 1) % SEQNO_SPACE_SIZE;
}

} // namespace ns3
