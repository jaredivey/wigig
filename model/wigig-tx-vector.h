/*
 * Copyright (c) 2010 CTTC
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Nicola Baldo <nbaldo@cttc.es>
 *          Ghada Badawy <gbadawy@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_TX_VECTOR_H
#define WIGIG_TX_VECTOR_H

#include "wigig-data-types.h"

#include "ns3/mac48-address.h"
#include "ns3/wifi-tx-vector.h"

namespace ns3 {

class WigigTxVector : public WifiTxVector {
public:
  WigigTxVector();
  /**
   * Create a TXVECTOR with the given parameters.
   *
   * \param mode WifiMode
   * \param powerLevel transmission power level
   * \param preamble preamble type
   * \param channelWidth the channel width in MHz
   * \param aggregation enable or disable MPDU aggregation
   */
  WigigTxVector(WifiMode mode, uint8_t powerLevel, WifiPreamble preamble,
                uint16_t channelWidth, bool aggregation);
  /**
   * Create a TXVECTOR with the given parameters.
   *
   * \param mode WifiMode
   * \param powerLevel transmission power level
   * \param preamble preamble type
   * \param guardInterval the guard interval duration in nanoseconds
   * \param nTx the number of TX antennas
   * \param nss the number of spatial STBC streams (NSS)
   * \param ness the number of extension spatial streams (NESS)
   * \param channelWidth the channel width in MHz
   * \param aggregation enable or disable MPDU aggregation
   * \param stbc enable or disable STBC
   * \param bssColor the BSS color
   */
  WigigTxVector(WifiMode mode, uint8_t powerLevel, WifiPreamble preamble,
                uint16_t guardInterval, uint8_t nTx, uint8_t nss, uint8_t ness,
                uint16_t channelWidth, bool aggregation, bool stbc,
                uint8_t bssColor = 0);

  /**
   * Set BRP Packet Type.
   * \param type The type of BRP packet.
   */
  void SetPacketType(PacketType type);
  /**
   * Get BRP Packet Type.
   * \return The type of BRP packet.
   */
  PacketType GetPacketType() const;
  /**
   * Set the length of te training field.
   * \param length The length of the training field.
   */
  void SetTrainingFieldLength(uint8_t length);
  /**
   * Get the length of te training field.
   * \return The length of te training field.
   */
  uint8_t GetTrainingFieldLength() const;
  /**
   * Request Beam Tracking.
   */
  void RequestBeamTracking();
  /**
   * \return True if Beam Tracking requested, otherwise false.
   */
  bool IsBeamTrackingRequested() const;
  /**
   * In the TXVECTOR, LAST_RSSI indicates the received power level of
   * the last packet with a valid PHY header that was received a SIFS period
   * before transmission of the current packet; otherwise, it is 0.
   *
   * In the RXVECTOR, LAST_RSSI indicates the value of the LAST_RSSI
   * field from the PCLP header of the received packet. Valid values are
   * integers in the range of 0 to 15:
   * — Values of 2 to 14 represent power levels (–71+value×2) dBm.
   * — A value of 15 represents power greater than or equal to –42 dBm.
   * — A value of 1 represents power less than or equal to –68 dBm.
   * — A value of 0 indicates that the previous packet was not received a
   * SIFS period before the current transmission.
   *
   * \param length The length of the training field.
   */
  void SetLastRssi(uint8_t level);
  /**
   * Get the level of Last RSSI.
   * \return The level of Last RSSI.
   */
  uint8_t GetLastRssi() const;

  uint8_t remainingTrnUnits;
  uint8_t remainingTrnSubfields;

  /**
   * Set the MAC address of the sender of the packet.
   */
  void SetSender(Mac48Address sender);
  /**
   * Returns the MAC address of the sender of the packet.
   */
  Mac48Address GetSender() const;
  /**
   * Whether the transmitted packet is a DMG beacon or not.
   */
  void SetDmgBeacon(bool beacon);
  /**
   * Returns whether the transmitted packet is a DMG beacon or not.
   */
  bool IsDmgBeacon() const;

private:
  PacketType m_packetType;     //!< BRP-RX, BRP-TX or BRP-RX/TX packet.
  uint8_t m_traingFieldLength; //!< The length of the training field (Number of
                               //!< TRN-Units).
  bool m_beamTrackingRequest;  //!< Flag to indicate the need for beam tracking.
  uint8_t m_lastRssi;          //!< Last Received Signal Strength Indicator.

  /* Helper values needed for PHY processing of TRN fields */
  Mac48Address m_sender; //!< MAC address of the sender of the packet.
  bool m_isDMGBeacon; //!< Flag that specifies whether the transmitted packet is
                      //!< a DMG beacon or not.
};

} // namespace ns3

#endif /* WIGIG_TX_VECTOR_H */
