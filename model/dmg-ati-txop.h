/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef DMG_ATI_TXOP_H
#define DMG_ATI_TXOP_H

#include "mac-low.h"
#include "wigig-mac-header.h"
#include "wigig-remote-station-manager.h"
#include "wigig-txop.h"

namespace ns3 {

class DmgAtiTxop : public WigigTxop {
public:
  static TypeId GetTypeId();

  DmgAtiTxop();
  ~DmgAtiTxop() override;
  /**
   * \param packet packet to send
   * \param hdr header of packet to send.
   *
   * Store the packet in the internal queue until it
   * can be sent safely.
   */
  void Queue(Ptr<Packet> packet, const WigigMacHeader &hdr) override;
  /**
   * Initiate ATI access Period. This function is called by DMG STA.
   * \param atiDuration The duration of the ATI access period in microseconds.
   */
  void InitiateAtiAccessPeriod(Time atiDuration);
  /**
   * Initiate Transmission in this ATI access period. This function is called by
   * DMG PCP/AP. \param atiDuration The duration of the ATI access period in
   * microseconds.
   */
  void InitiateTransmission(Time atiDuration);

  DmgAtiTxop &operator=(const DmgAtiTxop &) = delete;
  DmgAtiTxop(const DmgAtiTxop &o) = delete;

private:
  void NotifyAccessGranted() override;
  void NotifyInternalCollision() override;
  void GotAck() override;
  void MissedAck() override;
  void Cancel() override;
  void EndTxNoAck() override;
  void RestartAccessIfNeeded() override;
  void StartAccessIfNeeded() override;

  /**
   * Disable transmission.
   */
  void DisableTransmission();
  /**
   * Check if DATA should be re-transmitted if ACK was missed.
   *
   * \return true if DATA should be re-transmitted,
   *         false otherwise
   */
  bool NeedDataRetransmission();

  Ptr<const Packet> m_currentPacket;
  WigigMacHeader m_currentHdr;

  Time m_transmissionStarted; /* The time of the initiation of transmission */
  Time m_atiDuration;         /* The duration of the ATI*/
  Time m_remainingDuration; /* The remaining duration till the end of this CBAP
                               allocation*/
  bool m_allowTransmission;
};

} // namespace ns3

#endif /* DMG_ATI_TXOP_H */
