/*
 * Copyright (c) 2017
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Sébastien Deronne <sebastien.deronne@gmail.com>
 */

#include "wigig-frame-capture-model.h"

#include "wigig-phy.h"

#include "ns3/nstime.h"
#include "ns3/simulator.h"

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(WigigFrameCaptureModel);

TypeId WigigFrameCaptureModel::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigFrameCaptureModel")
          .SetParent<Object>()
          .SetGroupName("Wigig")
          .AddAttribute(
              "CaptureWindow", "The duration of the capture window.",
              TimeValue(NanoSeconds(1891)),
              MakeTimeAccessor(&WigigFrameCaptureModel::m_captureWindow),
              MakeTimeChecker());
  return tid;
}

void WigigFrameCaptureModel::SetPhy(Ptr<WigigPhy> phy) { m_phy = phy; }

bool WigigFrameCaptureModel::IsInCaptureWindow(
    Time timePreambleDetected) const {
  return (timePreambleDetected + m_captureWindow >= Simulator::Now());
}

} // namespace ns3
