/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef DMG_SLS_TXOP_H
#define DMG_SLS_TXOP_H

#include "ext-headers.h"
#include "wigig-txop.h"

#include "ns3/callback.h"
#include "ns3/nstime.h"
#include "ns3/object.h"
#include "ns3/packet.h"
#include "ns3/wifi-mode.h"
#include "ns3/wigig-mac-header.h"

#include <deque>
#include <stdint.h>

namespace ns3 {

enum SlsRole {
  SLS_IDLE,
  SLS_INITIATOR,
  SLS_RESPONDER,
};

class DmgSlsTxop : public WigigTxop {
public:
  static TypeId GetTypeId();

  DmgSlsTxop();
  ~DmgSlsTxop() override;

  typedef std::deque<Mac48Address> SLS_REQUESTS_DEQUE;

  /**
   * Append a new SLS Request.
   * \param peerAddress The MAC address of the responder station.
   */
  void AppendSlsRequest(Mac48Address peerAddress);
  /**
   * Start channel access procedure to obtain transmit opprtunity (TXOP) to
   * perform beamforming training in the DTI access period. \param peerAddress
   * The MAC address of the responder station.
   */
  void InitiateTxopSectorSweep(Mac48Address peerAddress);
  /**
   * Start responder sector sweep phase.
   * \param peerAddress The MAC address of the initiator station.
   */
  void StartResponderSectorSweep(Mac48Address peerAddress);
  /**
   * Start initiator sector sweep feedback.
   * \param peerAddress The MAC address of the responder station.
   */
  void StartInitiatorFeedback(Mac48Address peerAddress);
  /**
   * Initialize SLS related variables.
   */
  void InitializeVariables();
  /**
   * Resume any pending TXSS TXOP or SSW-FBCK TXOP.
   */
  void ResumeTXSS();
  /**
   * Sector sweep phase failed (Initiator did not receive any RXSS-SSW frame).
   */
  void SectorSweepPhaseFailed();
  /**
   * Failed to receive SSW-ACK frame from the initiator.
   */
  void RxSswAckFailed();
  /**
   * This function is called when TXSS SLS beamforming training has successfully
   * completed.
   */
  void SlsBftCompleted();
  /**
   * This function is called when TXSS SLS beamforming training has failed
   * (Exceeded dot11BFRetryLimit limit).
   */
  void SlsBftFailed(bool retryAccess = true);
  /**
   * Transmit single  packet.
   * \param packet the packet we want to transmit.
   * \param hdr header of the packet to send.
   * \param duration The duration in Microseconds.
   */
  void TransmitFrame(Ptr<const Packet> packet, const WigigMacHeader &hdr,
                     Time duration);
  /**
   * typedef for a callback to invoke when access to the channel is granted to
   * start SLS TxOP.
   */
  typedef Callback<void, Mac48Address, SlsRole, bool> AccessGranted;
  /**
   * Access is granted to the channel
   * \param callback the callback to invoke when access is granted
   */
  void SetAccessGrantedCallback(AccessGranted callback);

  DmgSlsTxop &operator=(const DmgSlsTxop &) = delete;
  DmgSlsTxop(const DmgSlsTxop &o) = delete;

private:
  void NotifyAccessGranted() override;
  void NotifyInternalCollision() override;
  void Cancel() override;
  void EndTxNoAck() override;
  void RestartAccessIfNeeded() override;

  AccessGranted m_accessGrantedCallback;
  SLS_REQUESTS_DEQUE
      m_slsRequestsDeque; //!< Deque for SLS Beamforming Requests.
  bool m_servingSls;      //!< Flag to indicate if we are in SLS phase.
  SlsRole m_slsRole;      //!< The current SLS role.
  bool m_isFeedback; //!< Flag to indicate whether the initiator is in SSW-FBCK
                     //!< state.
};

} // namespace ns3

#endif /* DMG_SLS_TXOP_H */
