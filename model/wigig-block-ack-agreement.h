/*
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mirko Banchi <mk.banchi@gmail.com>
 */

#ifndef WIGIG_BLOCK_ACK_AGREEMENT_H
#define WIGIG_BLOCK_ACK_AGREEMENT_H

#include "ns3/event-id.h"
#include "ns3/mac48-address.h"

namespace ns3 {

class MacLow;

/**
 * \brief Maintains information for a block ack agreement.
 * \ingroup wigig
 */
class WigigBlockAckAgreement {
  /// Provide access to MacLow class
  friend class ns3::MacLow;

public:
  /**
   * Constructor for WigigBlockAckAgreement with given peer and TID.
   *
   * \param peer the peer station
   * \param tid the TID
   */
  WigigBlockAckAgreement(Mac48Address peer, uint8_t tid);
  ~WigigBlockAckAgreement();
  /**
   * Set buffer size.
   *
   * \param bufferSize the buffer size (in number of MPDUs)
   */
  void SetBufferSize(uint16_t bufferSize);
  /**
   * Set timeout.
   *
   * \param timeout the timeout value
   */
  void SetTimeout(uint16_t timeout);
  /**
   * Set starting sequence number.
   *
   * \param seq the starting sequence number
   */
  void SetStartingSequence(uint16_t seq);
  /**
   * Set starting sequence control.
   *
   * \param seq the starting sequence control
   */
  void SetStartingSequenceControl(uint16_t seq);
  /**
   * Set block ack policy to immediate Ack.
   */
  void SetImmediateBlockAck();
  /**
   * Set block ack policy to delayed Ack.
   */
  void SetDelayedBlockAck();
  /**
   * Enable or disable A-MSDU support.
   *
   * \param supported enable or disable A-MSDU support
   */
  void SetAmsduSupport(bool supported);
  /**
   * Return the Traffic ID (TID).
   *
   * \return TID
   */
  uint8_t GetTid() const;
  /**
   * Return the peer address.
   *
   * \return the peer MAC address
   */
  Mac48Address GetPeer() const;
  /**
   * Return the buffer size.
   *
   * \return the buffer size (in number of MPDUs)
   */
  uint16_t GetBufferSize() const;
  /**
   * Return the timeout.
   *
   * \return the timeout
   */
  uint16_t GetTimeout() const;
  /**
   * Return the starting sequence number.
   *
   * \return starting sequence number
   */
  virtual uint16_t GetStartingSequence() const;
  /**
   * Return the starting sequence control
   *
   * \return starting sequence control
   */
  uint16_t GetStartingSequenceControl() const;
  /**
   * Return the last sequence number covered by the ack window
   *
   * \return ending sequence number
   */
  uint16_t GetWinEnd() const;
  /**
   * Check whether the current ack policy is immediate BlockAck.
   *
   * \return true if the current ack policy is immediate BlockAck,
   *         false otherwise
   */
  bool IsImmediateBlockAck() const;
  /**
   * Check whether A-MSDU is supported
   *
   * \return true if A-MSDU is supported,
   *         false otherwise
   */
  bool IsAmsduSupported() const;

protected:
  Mac48Address m_peer;       //!< Peer address
  uint8_t m_amsduSupported;  //!< Flag whether MSDU aggregation is supported
  uint8_t m_blockAckPolicy;  //!< Type of block ack: immediate or delayed
  uint8_t m_tid;             //!< Traffic ID
  uint16_t m_bufferSize;     //!< Buffer size
  uint16_t m_timeout;        //!< Timeout
  uint16_t m_startingSeq;    //!< Starting sequence control
  uint16_t m_winEnd;         //!< Ending sequence number
  EventId m_inactivityEvent; //!< inactivity event
};

} // namespace ns3

#endif /* WIGIG_BLOCK_ACK_AGREEMENT_H */
