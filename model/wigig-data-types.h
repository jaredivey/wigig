/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_DATA_TYPES_H
#define WIGIG_DATA_TYPES_H

#include <stdint.h>
#include <vector>

namespace ns3 {

/* IEEE 802.11ad BRP PHY Parameters */
#define aBrpMinSCblocks 18
#define aSCGILength 64
#define aBrpMinOFDMblocks 20
#define aSCBlockSize 512
#define OFDMSCMin (aBrpMinSCblocks * aSCBlockSize + aSCGILength) * 0.57
#define OFDMBrpMin aBrpMinOFDMblocks * 242

/* DMG TRN parameters */
#define AGC_SF_DURATION NanoSeconds(182) /* AGC Subfield Duration. */
#define TRN_CE_DURATION                                                        \
  NanoSeconds(655) /* TRN Channel Estimation Subfield Duration. */
#define TRN_SUBFIELD_DURATION NanoSeconds(364) /* TRN Subfield Duration. */
#define TRN_UNIT_SIZE 4 /* The number of TRN Subfield within TRN Unit. */

/* DMG MAC Parameters */
#define MAX_DMG_AMSDU_LENGTH                                                   \
  "7935" /* The maximum length of an A-MSDU in an DMG MPDU is 7935 octets. */
#define MAX_DMG_AMPDU_LENGTH                                                   \
  "262143" /* The maximum length of an A-MPDU in an DMG PPDU is 262,143        \
              octets. */
#define WIGIG_OFDM_SUBCARRIER_SPACING 5156250 /* Subcarrier frequency spacing  \
                                               */
#define WIGIG_GUARD_BANDWIDTH                                                  \
  1980 /* The guard bandwidth in MHz for 2.16 GHz channel */

typedef uint8_t
    RfChainId; //!< Typedef for Radio Frequency (RF) Chain ID in the Codebook.
typedef uint8_t AntennaId; //!< Typedef for Antenna Array ID in the Codebook.
typedef uint8_t SectorId;  //!< Typedef for Sector ID within the Codebook.
typedef uint8_t AwvId;     //!< Typedef for AWV ID.
typedef float
    Directivity; //!< Typedef for direcitvity gain for a certain angle.
typedef std::vector<AntennaId>
    AntennaList; //!< Typedef for list of antennas IDs.
typedef AwvId AwvId_TX;
typedef AwvId AwvId_RX;
typedef std::pair<AntennaId, SectorId>
    ANTENNA_CONFIGURATION;    //!< Typedef for antenna configuration (AntennaId,
                              //!< SectorId).
typedef uint8_t AllocationId; //!< Typedef for the allocation ID.

enum AllocationType { SERVICE_PERIOD_ALLOCATION = 0, CBAP_ALLOCATION = 1 };

/**
 * \ingroup wigig
 * Identifies the TRN Field to appended to the end of the packet.
 */
enum PacketType {
  TRN_R = 0,
  TRN_T = 1,
  TRN_RT = 2,
};

} // namespace ns3

#endif // WIGIG_DATA_TYPES_H
