/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef ADHOC_WIGIG_MAC_H
#define ADHOC_WIGIG_MAC_H

#include "wigig-mac.h"

namespace ns3 {

/**
 * \ingroup wigig
 *
 * Experimental Station mode for P2P communication (No beacon header interval,
 * only data transmission).
 */
class AdhocWigigMac : public WigigMac {
public:
  static TypeId GetTypeId();

  AdhocWigigMac();
  ~AdhocWigigMac() override;

  uint16_t GetAssociationId() override;
  void SetAddress(Mac48Address address) override;
  void SetLinkUpCallback(Callback<void> linkUp) override;
  void Enqueue(Ptr<Packet> packet, Mac48Address to) override;
  /**
   * Add manually the best antenna configuration for communication with a
   * specific DMG AD-HOC STA. \param txSectorId The ID of the transmit sector.
   * \param txAntennaId The ID of the transmit phased antenna array.
   * \param rxSectorId The ID of the receive sector.
   * \param rxAntennaId The ID of the receive phased antenna array..
   * \param address The MAC address of the peer DMG AD-HOC STA.
   */
  void AddAntennaConfig(SectorId txSectorId, AntennaId txAntennaId,
                        SectorId rxSectorId, AntennaId rxAntennaId,
                        Mac48Address address);
  /**
   * Add manually the best antenna configuration for communication with a
   * specific DMG AD-HOC STA. \param txSectorId The ID of the transmit sector.
   * \param txAntennaId The ID of the transmit phased antenna array.
   * \param address The MAC address of the peer DMG AD-HOC STA.
   */
  void AddAntennaConfig(SectorId txSectorId, AntennaId txAntennaId,
                        Mac48Address address);

protected:
  void DoInitialize() override;

  void StartBeaconInterval() override;
  void EndBeaconInterval() override;
  void StartBeaconTransmissionInterval() override;
  void StartAssociationBeamformTraining() override;
  void StartAnnouncementTransmissionInterval() override;
  void StartDataTransmissionInterval() override;
  void FrameTxOk(const WigigMacHeader &hdr) override;
  void BrpSetupCompleted(Mac48Address address) override;
  void NotifyBrpPhaseCompleted() override;

private:
  void Receive(Ptr<WigigMacQueueItem> mpdu) override;
  /**
   * Return the DMG capability of the current STA.
   *
   * \return the DMG capability that we support
   */
  DmgCapabilities GetDmgCapabilities() const override;
  /**
   * The packet we sent was successfully received by the receiver
   * (i.e. we received an ACK from the receiver). If the packet
   * was an association response to the receiver, we record that
   * the receiver is now associated with us.
   *
   * \param hdr the header of the packet that we successfully sent
   */
  void TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) override;
};

} // namespace ns3

#endif /* ADHOC_WIGIG_MAC_H */
