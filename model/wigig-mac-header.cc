/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-mac-header.h"

#include "ns3/address-utils.h"
#include "ns3/nstime.h"

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(WigigMacHeader);

/// type enumeration
enum {
  TYPE_MGT = 0,
  TYPE_CTL = 1,
  TYPE_DATA = 2,
  TYPE_Extension = 3 // Extension frame type for DMG.
};

/// subtype enumeration
enum {
  SUBTYPE_CTL_EXTENSION =
      6, // Extension subtype for DMG Control Frame Extension.
  SUBTYPE_CTL_CTLWRAPPER = 7,
  SUBTYPE_CTL_BACKREQ = 8,
  SUBTYPE_CTL_BACKRESP = 9,
  SUBTYPE_CTL_RTS = 11,
  SUBTYPE_CTL_CTS = 12,
  SUBTYPE_CTL_ACK = 13,
  SUBTYPE_CTL_END = 14,
  SUBTYPE_CTL_END_ACK = 15
};

/*
 * Enumeration for DMG Control Frame Extension.
 */
enum {
  SUBTYPE_CTL_EXTENSION_POLL = 2,
  SUBTYPE_CTL_EXTENSION_SPR = 3,
  SUBTYPE_CTL_EXTENSION_GRANT = 4,
  SUBTYPE_CTL_EXTENSION_DMG_CTS = 5,
  SUBTYPE_CTL_EXTENSION_DMG_DTS = 6,
  SUBTYPE_CTL_EXTENSION_GRANT_ACK = 7,
  SUBTYPE_CTL_EXTENSION_SSW = 8,
  SUBTYPE_CTL_EXTENSION_SSW_FBCK = 9,
  SUBTYPE_CTL_EXTENSION_SSW_ACK = 10
};

WigigMacHeader::WigigMacHeader()
    : WifiMacHeader(), m_dmgPpdu(false), m_beamTrackingRequired(false),
      m_trainingFieldLength(0) {}

WigigMacHeader::~WigigMacHeader() {}

void WigigMacHeader::SetType(WifiMacType type, bool resetToDsFromDs) {
  switch (type) {
  case WIFI_MAC_CTL_DMG_POLL:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_POLL;
    break;
  case WIFI_MAC_CTL_DMG_SPR:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_SPR;
    break;
  case WIFI_MAC_CTL_DMG_GRANT:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_GRANT;
    break;
  case WIFI_MAC_CTL_DMG_CTS:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_DMG_CTS;
    break;
  case WIFI_MAC_CTL_DMG_DTS:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_DMG_DTS;
    break;
  case WIFI_MAC_CTL_DMG_SSW:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_SSW;
    break;
  case WIFI_MAC_CTL_DMG_SSW_FBCK:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_SSW_FBCK;
    break;
  case WIFI_MAC_CTL_DMG_SSW_ACK:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_SSW_ACK;
    break;
  case WIFI_MAC_CTL_DMG_GRANT_ACK:
    m_ctrlType = TYPE_CTL;
    m_ctrlSubtype = SUBTYPE_CTL_EXTENSION;
    m_ctrlFrameExtension = SUBTYPE_CTL_EXTENSION_GRANT_ACK;
    break;
  case WIFI_MAC_EXTENSION_DMG_BEACON:
    m_ctrlType = TYPE_Extension;
    m_ctrlSubtype = 0;
    break;
  default:
    break;
  }
  WifiMacHeader::SetType(type, resetToDsFromDs);
}

WifiMacType WigigMacHeader::GetType() const {
  switch (m_ctrlType) {
  case TYPE_CTL:
    switch (m_ctrlSubtype) {
    case SUBTYPE_CTL_EXTENSION:
      switch (m_ctrlFrameExtension) {
      case SUBTYPE_CTL_EXTENSION_POLL:
        return WIFI_MAC_CTL_DMG_POLL;
      case SUBTYPE_CTL_EXTENSION_SPR:
        return WIFI_MAC_CTL_DMG_SPR;
      case SUBTYPE_CTL_EXTENSION_GRANT:
        return WIFI_MAC_CTL_DMG_GRANT;
      case SUBTYPE_CTL_EXTENSION_DMG_CTS:
        return WIFI_MAC_CTL_DMG_CTS;
      case SUBTYPE_CTL_EXTENSION_DMG_DTS:
        return WIFI_MAC_CTL_DMG_DTS;
      case SUBTYPE_CTL_EXTENSION_GRANT_ACK:
        return WIFI_MAC_CTL_DMG_GRANT_ACK;
      case SUBTYPE_CTL_EXTENSION_SSW:
        return WIFI_MAC_CTL_DMG_SSW;
      case SUBTYPE_CTL_EXTENSION_SSW_FBCK:
        return WIFI_MAC_CTL_DMG_SSW_FBCK;
      case SUBTYPE_CTL_EXTENSION_SSW_ACK:
        return WIFI_MAC_CTL_DMG_SSW_ACK;
      }
    default:
      break;
    }
    break;
  case TYPE_Extension:
    switch (m_ctrlSubtype) {
    case 0:
      return WIFI_MAC_EXTENSION_DMG_BEACON;
    }
    break;
  }
  return WifiMacHeader::GetType();
}

bool WigigMacHeader::IsDmgCts() const {
  return (GetType() == WIFI_MAC_CTL_DMG_CTS);
}

bool WigigMacHeader::IsDmgBeacon() const {
  return (GetType() == WIFI_MAC_EXTENSION_DMG_BEACON);
}

bool WigigMacHeader::IsSsw() const {
  return (GetType() == WIFI_MAC_CTL_DMG_SSW);
}

bool WigigMacHeader::IsSswFbck() const {
  return (GetType() == WIFI_MAC_CTL_DMG_SSW_FBCK);
}

bool WigigMacHeader::IsSswAck() const {
  return (GetType() == WIFI_MAC_CTL_DMG_SSW_ACK);
}

bool WigigMacHeader::IsPollFrame() const {
  return (GetType() == WIFI_MAC_CTL_DMG_POLL);
}

bool WigigMacHeader::IsSprFrame() const {
  return (GetType() == WIFI_MAC_CTL_DMG_SPR);
}

bool WigigMacHeader::IsGrantFrame() const {
  return (GetType() == WIFI_MAC_CTL_DMG_GRANT);
}

uint16_t WigigMacHeader::GetFrameControl() const {
  uint16_t val = WifiMacHeader::GetFrameControl();
  if ((m_ctrlType == 1) && (m_ctrlSubtype == 6)) {
    val |= (m_ctrlFrameExtension << 8) & (0xf << 8);
  }
  return val;
}

uint16_t WigigMacHeader::GetQosControl() const {
  uint16_t val = WifiMacHeader::GetQosControl();
  if (m_dmgPpdu) {
    val |= m_qosRdg << 9;
  }
  return val;
}

void WigigMacHeader::SetFrameControl(uint16_t ctrl) {
  m_ctrlType = (ctrl >> 2) & 0x03;
  m_ctrlSubtype = (ctrl >> 4) & 0x0f;

  if ((m_ctrlType == 1) && (m_ctrlSubtype == 6)) {
    m_ctrlFrameExtension = (ctrl >> 8) & 0x0F;
    m_ctrlMoreData = (ctrl >> 13) & 0x01;
    m_ctrlWep = (ctrl >> 14) & 0x01;
    m_ctrlOrder = (ctrl >> 15) & 0x01;
  } else {
    WifiMacHeader::SetFrameControl(ctrl);
  }
}

void WigigMacHeader::SetQosControl(uint16_t qos) {
  m_qosTid = qos & 0x000f;
  m_qosEosp = (qos >> 4) & 0x0001;
  m_qosAckPolicy = (qos >> 5) & 0x0003;
  m_amsduPresent = (qos >> 7) & 0x0001;
  if (m_dmgPpdu) {
    m_qosRdg = (qos >> 9) & 0x1;
  } else {
    WifiMacHeader::SetQosControl(qos);
  }
}

uint32_t WigigMacHeader::GetSize() const {
  switch (m_ctrlType) {
  case TYPE_CTL:
    switch (m_ctrlSubtype) {
    case SUBTYPE_CTL_EXTENSION:
      switch (m_ctrlFrameExtension) {
      case SUBTYPE_CTL_EXTENSION_POLL:
      case SUBTYPE_CTL_EXTENSION_SPR:
      case SUBTYPE_CTL_EXTENSION_GRANT:
      case SUBTYPE_CTL_EXTENSION_DMG_CTS:
      case SUBTYPE_CTL_EXTENSION_SSW:
      case SUBTYPE_CTL_EXTENSION_SSW_FBCK:
      case SUBTYPE_CTL_EXTENSION_SSW_ACK:
      case SUBTYPE_CTL_EXTENSION_GRANT_ACK:
        return 2 + 2 + 6 + 6;
      case SUBTYPE_CTL_EXTENSION_DMG_DTS:
        return 2 + 2 + 6;
      }
    default:
      break;
    }
    break;
  case TYPE_Extension:
    return 2 + 2 + 6;
  default:
    break;
  }
  return WifiMacHeader::GetSize();
}

void WigigMacHeader::SetAsDmgPpdu() {
  m_dmgPpdu = true;
  m_ctrlOrder = 0;
}

bool WigigMacHeader::IsDmgPpdu() const { return m_dmgPpdu; }

void WigigMacHeader::SetQosRdGrant(bool value) { m_qosRdg = value; }

bool WigigMacHeader::IsQosRdGrant() const {
  NS_ASSERT(m_dmgPpdu && IsQosData());
  return (m_qosRdg == 1);
}

const char *WigigMacHeader::GetTypeString() const {
#define FOO(x)                                                                 \
  case WIFI_MAC_##x:                                                           \
    return #x;                                                                 \
    break;

  switch (GetType()) {
    FOO(CTL_DMG_POLL);
    FOO(CTL_DMG_SPR);
    FOO(CTL_DMG_GRANT);
    FOO(CTL_DMG_CTS);
    FOO(CTL_DMG_DTS);
    FOO(CTL_DMG_GRANT_ACK);
    FOO(CTL_DMG_SSW);
    FOO(CTL_DMG_SSW_FBCK);
    FOO(CTL_DMG_SSW_ACK);
    FOO(EXTENSION_DMG_BEACON);
  default:
    return WifiMacHeader::GetTypeString();
  }
#undef FOO
#ifndef _WIN32
  // needed to make gcc 4.0.1 ppc darwin happy.
  return "BIG_ERROR";
#endif
}

TypeId WigigMacHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMacHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMacHeader>();
  return tid;
}

void WigigMacHeader::Serialize(Buffer::Iterator i) const {
  switch (m_ctrlType) {
  case TYPE_CTL:
    switch (m_ctrlSubtype) {
    case SUBTYPE_CTL_EXTENSION:
      i.WriteHtolsbU16(GetFrameControl());
      i.WriteHtolsbU16(m_duration);
      WriteTo(i, m_addr1);
      switch (m_ctrlFrameExtension) {
      case SUBTYPE_CTL_EXTENSION_POLL:
      case SUBTYPE_CTL_EXTENSION_SPR:
      case SUBTYPE_CTL_EXTENSION_GRANT:
      case SUBTYPE_CTL_EXTENSION_DMG_CTS:
      case SUBTYPE_CTL_EXTENSION_GRANT_ACK:
      case SUBTYPE_CTL_EXTENSION_SSW:
      case SUBTYPE_CTL_EXTENSION_SSW_FBCK:
      case SUBTYPE_CTL_EXTENSION_SSW_ACK:
        WriteTo(i, m_addr2); // TA Address Field.
        break;
        ;
      case SUBTYPE_CTL_EXTENSION_DMG_DTS:
        break;
      }
      break;
    default:
      WifiMacHeader::Serialize(i);
      break;
    }
    break;
  case TYPE_Extension:
    i.WriteHtolsbU16(GetFrameControl());
    i.WriteHtolsbU16(m_duration);
    WriteTo(i, m_addr1);
    break;
  default:
    WifiMacHeader::Serialize(i);
    break;
  }
}

uint32_t WigigMacHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  WifiMacHeader::Deserialize(i);
  switch (m_ctrlType) {
  case TYPE_CTL:
    switch (m_ctrlSubtype) {
    case SUBTYPE_CTL_EXTENSION:
      switch (m_ctrlFrameExtension) {
      case SUBTYPE_CTL_EXTENSION_POLL:
      case SUBTYPE_CTL_EXTENSION_SPR:
      case SUBTYPE_CTL_EXTENSION_GRANT:
      case SUBTYPE_CTL_EXTENSION_DMG_CTS:
      case SUBTYPE_CTL_EXTENSION_GRANT_ACK:
      case SUBTYPE_CTL_EXTENSION_SSW:
      case SUBTYPE_CTL_EXTENSION_SSW_FBCK:
      case SUBTYPE_CTL_EXTENSION_SSW_ACK:
        ReadFrom(i, m_addr2); // TA Address Field.
        break;
      case SUBTYPE_CTL_EXTENSION_DMG_DTS:
        break;
      }
    default:
      break;
    }
    break;
  case TYPE_Extension:
  default:
    break;
  }
  return i.GetDistanceFrom(start);
}

void WigigMacHeader::SetPacketType(PacketType type) { m_brpPacketType = type; }

PacketType WigigMacHeader::GetPacketType() const { return m_brpPacketType; }

void WigigMacHeader::SetTrainingFieldLength(uint8_t length) {
  m_trainingFieldLength = length;
}

uint8_t WigigMacHeader::GetTrainingFieldLength() const {
  return m_trainingFieldLength;
}

void WigigMacHeader::RequestBeamTracking() { m_beamTrackingRequired = true; }

bool WigigMacHeader::IsBeamTrackingRequested() const {
  return (m_beamTrackingRequired);
}

} // namespace ns3
