/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef STA_WIGIG_MAC_H
#define STA_WIGIG_MAC_H

#include "wigig-mac.h"

#include "ns3/event-id.h"
#include "ns3/mgt-headers.h"
#include "ns3/sta-wifi-mac.h"
#include "ns3/traced-callback.h"
#include "ns3/traced-value.h"

namespace ns3 {

class UniformRandomVariable;

#define dot11RSSRetryLimit 8
#define dot11RSSBackoff 8

/**
 * \ingroup wigig
 *
 * The Wifi MAC high model for a non-DMG PCP/AP STA in a DMG BSS.
 */
class StaWigigMac : public WigigMac {
public:
  static TypeId GetTypeId();

  StaWigigMac();
  ~StaWigigMac() override;

  /**
   * Struct to hold information regarding DMG beacons observed.
   */
  struct DmgApInfo : public StaWifiMac::ApInfo {
    ExtDmgBeacon m_beacon; //!< DMG Beacon header.
  };

  typedef std::vector<DmgApInfo> DmgApInfoList; //!< List of DMG PCP/APs.

  enum TransmissionLink { DIRECT_LINK = 0, RELAY_LINK = 1 };

  enum StaRole { SOURCE_STA = 0, DESTINATION_STA = 1, RELAY_STA = 2 };

  struct RelayLinkInfo {
    bool relayForwardingActivated; //!< Flag to indicate if a relay link has
                                   //!< been aactivated.
    bool relayLinkEstablished; //!< Flag to indicate if a relay link has been
                               //!< established.
    bool rdsDuplexMode;        //!< The duplex mode of the RDS.
    bool
        waitingDestinationRedsReports; //!< Flag to indicate that we are waiting
                                       //!< for the destination REDS report.
    bool switchTransmissionLink; //!< Flag to indicate that we want to change
                                 //!< the current transmission link.
    bool tearDownRelayLink;

    uint8_t relayLinkChangeInterval; //!< Relay Link Change Interval reported by
                                     //!< the source REDS (MicroSeconds).
    uint8_t relayDataSensingTime; //!< Relay Data Sensing Time reported by the
                                  //!< source REDS (MicroSeconds).
    uint16_t relayFirstPeriod;  //!< Relay First Period reported by the source
                                //!< REDS (MicroSeconds).
    uint16_t relaySecondPeriod; //!< Relay Second Period reported by the source
                                //!< REDS (MicroSeconds).

    /* Identifiers of the relay protected service period */
    uint16_t srcRedsAid;         //!< The AID of the source REDS.
    uint16_t dstRedsAid;         //!< The AID of the destination REDS.
    uint16_t selectedRelayAid;   //!< The AID of the selected RDS.
    Mac48Address srcRedsAddress; //!< The MAC address of the Source REDS.
    Mac48Address dstRedsAddress; //!< The MAC address of the destination REDS.
    Mac48Address selectedRelayAddress; //!< The MAC address of the selected RDS.
    TransmissionLink
        transmissionLink; //!< Current transmission link for the service period.

    RelayCapabilitiesInfo rdsCapabilitiesInfo; //!< The relay capabilities
                                               //!< Information for the RDS.
    RelayCapabilitiesInfo
        dstRedsCapabilitiesInfo; //!< The relay capabilities Information for
                                 //!< the destination REDS.
    ChannelMeasurementInfoList
        channelMeasurementList; //!< The channel measurement list between
                                //!< the source REDS and the RDS.
  };

  typedef std::pair<uint8_t, uint8_t>
      REDS_PAIR; //!< Typedef to identify source and destination REDS protected
                 //!< by an RDS.

  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   *
   * The packet should be enqueued in a tx queue, and should be
   * dequeued as soon as the channel access function determines that
   * access is granted to this MAC.
   */
  void Enqueue(Ptr<Packet> packet, Mac48Address to) override;
  /**
   * \param lost The number of beacons which must be lost
   * before a new association sequence is started.
   */
  void SetMaxLostBeacons(uint32_t lost);
  /**
   * Request Information regarding station capabilities.
   * \param stationAddress The address of the station to obtain its
   * capabilities. \param list A list of the requested element IDs.
   */
  void RequestInformation(Mac48Address stationAddress,
                          WifiInformationElementIdList &list);
  /**
   * Get Association Identifier (AID).
   * \return The AID of the station.
   */
  uint16_t GetAssociationId() override;
  /**
   * Create or modify DMG allocation for the transmission of frames between DMG
   * STA that are members of a PBSS or a DMG infrastructure. \param elem The DMG
   * TSPEC information element.
   */
  void CreateAllocation(DmgTspecElement elem);
  /**
   * Delete existing DMG Allocation.
   * \param reason The reason for deleting the DMG Allocation.
   * \param allocationInfo Information about the allocation to be deleted.
   */
  void DeleteAllocation(uint16_t reason, DmgAllocationInfo &allocationInfo);
  /**
   * Return whether we are associated with a DMG AP.
   *
   * \return true if we are associated with a DMG AP, false otherwise
   */
  bool IsAssociated() const;
  /**
   * Initiate TXSS in CBAP allocation using TxOP.
   * \param peerAddress The address of the DMG STA to perform beamforming
   * training with.
   */
  void PerformTxssTxop(Mac48Address peerAddress) override;

protected:
  void DoInitialize() override;
  void StartBeaconInterval() override;
  void EndBeaconInterval() override;
  void StartBeaconTransmissionInterval() override;
  void StartAssociationBeamformTraining() override;
  void StartAnnouncementTransmissionInterval() override;
  void StartDataTransmissionInterval() override;
  void FrameTxOk(const WigigMacHeader &hdr) override;
  void BrpSetupCompleted(Mac48Address address) override;
  void NotifyBrpPhaseCompleted() override;

  /**
   * Resume any Pending TXSS requests in the current CBAP allocation.
   */
  void ResumePendingTxss() override;
  /**
   * End Association Beamform Training (A-BFT) period.
   */
  void EndAssociationBeamformTraining();

  /**
   * Remove Relay Entry
   * \param sourceAid The AID of the source REDS.
   * \param destinationAid The AID of the destination REDS.
   */
  void RemoveRelayEntry(uint16_t sourceAid, uint16_t destinationAid);
  /**
   * Set the current MAC state.
   *
   * \param value the new state
   */
  void SetState(StaWifiMac::MacState value);
  /**
   * Get STA Availability Element.
   * \return Pointer to the STA availabiltiy element.
   */
  StaAvailabilityElement GetStaAvailabilityElement() const;
  /**
   * Forward data frame to another DMG STA. This function is called during HD-DF
   * relay operation ode. \param hdr The MAC Header. \param packet \param
   * destAddress The MAC address of the receiving station.
   */
  void ForwardDataFrame(WigigMacHeader hdr, Ptr<Packet> packet,
                        Mac48Address destAddress);
  /**
   * Do Association Beamforming Training in the A-BFT period.
   * \param currentSlotIndex The index of the  current SSW slot in A-BFT.
   */
  void DoAssociationBeamformingTraining(uint8_t currentSlotIndex);
  /**
   * Send Channel Measurement Report.
   * \param to The address of the DMG STA which sent the measurement request.
   * \param token The token dialog.
   * \param List of channel measurement information between sending station and
   * other stations.
   */
  void
  SendChannelMeasurementReport(Mac48Address to, uint8_t token,
                               ChannelMeasurementInfoList &measurementList);
  /**
   * Schedule all the allocation blocks.
   * \param field The allocation field information..
   * \param role The role of the STA in the relay period.
   */
  void ScheduleAllocationBlocks(AllocationField &field, StaRole role);
  /**
   * Schedule an allocation block protected by relay operation mode.
   * \param id The allocation identifier.
   * \param role The role of the STA in the relay period.
   */
  void InitiateAllocationPeriod(AllocationId id, uint8_t srcAid, uint8_t dstAid,
                                Time spLength, StaRole role);
  /**
   * Initiate and schedule periods related to relay operation.
   * \param info Information regarding relay link.
   */
  void InitiateRelayPeriods(RelayLinkInfo &info);
  /**
   * End Relay Period.
   * \param pair The pair of REDS to which we established the relay period.
   */
  void EndRelayPeriods(REDS_PAIR &pair);
  /**
   * This function is called upon the expiration of
   * RelayLinkChangeIntervalTimeout. \param offset
   */
  void RelayLinkChangeIntervalTimeout();
  /**
   * Check if there is an available time for a given period in the current
   * allocated Service Period. \param periodDuration The duration of the period.
   * \return True if the period can exist in the current service period.
   */
  bool CheckTimeAvailabilityForPeriod(Time servicePeriodDuration,
                                      Time partialDuration);
  /**
   * Start Full Duplex Relay operation.
   * \param allocationId The ID of the current allocation.
   * \param servicePeriodLength The length of the service period for which we
   * are using the relay. \param involved Flag to indicate if we are involved in
   * relay forwarding.
   */
  void StartFullDuplexRelay(AllocationId allocationId, Time length,
                            uint8_t peerAid, Mac48Address peerAddress,
                            bool isSource);
  /**
   * Start Half Duplex Relay operation.
   * \param allocationId The ID of the current allocation.
   * \param servicePeriodLength The length of the service period for which we
   * are using the relay. \param involved Flag to indicate if we are involved in
   * relay forwarding.
   */
  void StartHalfDuplexRelay(AllocationId allocationId, Time servicePeriodLength,
                            bool involved);

  void StartRelayFirstPeriodAfterSwitching();
  /**
   * Start the First Period related to the HD-DF Relay operation mode.
   */
  void StartRelayFirstPeriod();
  /**
   * Start the Second Period related to the HD-DF Relay operation mode.
   */
  void StartRelaySecondPeriod();
  /**
   * Suspend transmission in the current period related to the HD-DF Relay
   * operation mode.
   */
  void SuspendRelayPeriod();
  /**
   * Relay First Period Timeout.
   */
  void RelayFirstPeriodTimeout();
  /**
   * Relay Second Period Timeout.
   */
  void RelaySecondPeriodTimeout();
  /**
   * MissedAck
   * \param hdr
   */
  void MissedAck(const WigigMacHeader &hdr);
  /**
   * This function is called upon the expiration of Relay Data Sensing Timeout.
   */
  void RelayDataSensingTimeout();
  /**
   * Switch to Relay Operational Mode. This method is called by the RDS.
   */
  void SwitchToRelayOperationalMode();
  /**
   * Relay Operation Timeout.
   */
  void RelayOperationTimeout();
  /**
   * The packet we sent was successfully received by the receiver
   * (i.e. we received an ACK from the receiver).  If the packet
   * was an association response to the receiver, we record that
   * the receiver is now associated with us.
   *
   * \param hdr the header of the packet that we successfully sent
   */
  void TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) override;
  /**
   * BeamLink Maintenance Timeout.
   */
  void BeamLinkMaintenanceTimeout() override;
  /**
   * Return the DMG capability of the current STA.
   *
   * \return the DMG capability that we support
   */
  DmgCapabilities GetDmgCapabilities() const override;

private:
  /**
   * Enable or disable active probing.
   *
   * \param enable enable or disable active probing
   */
  void SetActiveProbing(bool enable);
  /**
   * Return whether active probing is enabled.
   *
   * \return true if active probing is enabled, false otherwise
   */
  bool GetActiveProbing() const;
  /**
   * Handle a received packet.
   *
   * \param mpdu the received MPDU
   */
  void Receive(Ptr<WigigMacQueueItem> mpdu) override;

  /**
   * Update list of candidate DMG PCP/APs to associate. The list should contain
   * DmgApInfo sorted from best to worst SNR, with no duplicate.
   *
   * \param newApInfo the new DmgApInfo to be inserted
   */
  void UpdateCandidateApList(DmgApInfo &newApInfo);
  /**
   * Forward a probe request packet to the non-QoS WigigTxop. The standard is
   * not clear on the correct queue for management frames if QoS is supported.
   * We always use the non-QoS WigigTxop.
   */
  void SendProbeRequest();
  /**
   * Forward an association or reassociation request packet to the non-QoS
   * WigigTxop. The standard is not clear on the correct queue for management
   * frames if QoS is supported. We always use the non-QoS WigigTxop. \param
   * isReassoc flag whether it is a reassociation request
   */
  void SendAssociationRequest(bool isReassoc);
  /**
   * Try to ensure that we are associated with an DMG PCP/AP by taking an
   * appropriate action depending on the current association status.
   */
  void TryToEnsureAssociated();
  /**
   * This method is called after the association timeout occurred. We switch the
   * state to WAIT_ASSOC_RESP and re-send an association request.
   */
  void AssocRequestTimeout();
  /**
   * Return whether we have completed beamforming training with an AP.
   *
   * \return true if we have completed beamforming training with an AP, false
   * otherwise
   */
  bool IsBeamformedTrained() const;
  /**
   * This method is called after we have not received a beacon from the AP
   */
  void MissedBeacons();
  /**
   * Restarts the beacon timer.
   *
   * \param delay the delay before the watchdog fires
   */
  void RestartBeaconWatchdog(Time delay);
  /**
   * Failed to perform RSS in A-BFT period.
   */
  void FailedRssAttempt();
  /**
   * Missed SSW-Feedback from PCP/AP during A-BFT.
   */
  void MissedSswFeedback();
  /**
   * Start A-BFT Sector Sweep Slot.
   */
  void StartSectorSweepSlot();
  /**
   * Start Responder Sector Sweep (RSS) Phase during A-BFT access period.
   * \param stationAddress The address of the station.
   */
  void StartAbftResponderSectorSweep(Mac48Address address);
  /**
   * Start the scanning process which trigger active or passive scanning based
   * on the active probing flag.
   */
  void StartScanning();
  /**
   * This method is called after wait beacon timeout or wait probe request
   * timeout has occurred. This will trigger association process from beacons or
   * probe responses gathered while scanning.
   */
  void ScanningTimeout();

  /** Association Variables and Trace Sources **/
  StaWifiMac::MacState m_state; //!< The state of the MAC layer.
  uint16_t m_aid;               //!< AID of the STA.
  Time m_waitBeaconTimeout;     //!< wait beacon timeout
  Time m_probeRequestTimeout;   //!< probe request timeout.
  Time m_assocRequestTimeout;   //!< Association request timeout
  EventId m_waitBeaconEvent;    //!< Wait Event for DMG Beacon Reception.
  EventId m_probeRequestEvent;  //!< Event for Probe request.
  EventId m_assocRequestEvent;  //!< Event for association request.
  EventId m_beaconWatchdog;     //!< Event for  watch dog timer.
  Time m_beaconWatchdogEnd;     //!< Watch dog timer end time.
  uint32_t
      m_maxMissedBeacons; //!< Maximum number of beacons we are allowed to miss.
  bool m_activeProbing; //!< Flag to indicate whether to perform active probing.
  bool m_activeScanning; //!< Flag to indicate whether to perform active network
                         //!< scanning.
  TracedValue<Time> m_beaconArrival; //!< Time in queue.
  std::vector<DmgApInfo>
      m_candidateAps; //!< list of candidate APs to associate to.
  // Note: std::multiset<ApInfo> might be a candidate container to implement
  // this sorted list, but we are using a std::vector because we want to sort
  // based on SNR but find duplicates based on BSSID, and in practice this
  // candidate vector should not be too large.

  /* Data transmission variables  */
  bool m_moreData; //! More data field in the last received Data Frame to
                   //! indicate that the STA has MSDUs or A-MSDUs buffered for
                   //! transmission to the frame’s recipient during the current
                   //! SP or TXOP.

  /** BI Parameters **/
  EventId m_abftEvent; //!< Event for scheduling Association Beamforming
                       //!< Training channel access period.
  uint8_t m_TXSSSpan;      //!< The number of BIs to cover all the sectors.
  uint8_t m_remainingBis;  //!< The number of remaining BIs to cover the TXSS of
                           //!< the AP.
  bool m_completedBi;      //!< Flag to indicate if we have completed BI.
  Time m_nextDti;          //!< The relative starting time of the next DTI.
  EventId m_dtiStartEvent; //!< Event for scheduling DTI channel access period.

  /** BTI Beamforming **/
  bool m_receivedDmgBeacon;
  Ptr<UniformRandomVariable>
      m_abftSlot; //!< Random variable for selecting A-BFT slot.
  uint8_t m_remainingSlotsPerAbft; //!< Remaining Slots in the current A-BFT.
  uint8_t m_currentSlotIndex;      //!< Current SSW Slot in A-BFT.
  uint8_t m_selectedSlotIndex;     //!< Selected SSW slot in A-BFT..

  uint32_t m_failedRssAttemptsCounter; //!< Counter for Failed RSS Attempts
                                       //!< during A-BFT.
  uint32_t m_rssAttemptsLimit;    //!< Maximum Failed RSS Attempts during A-BFT.
  uint32_t m_rssBackoffRemaining; //!< Remaining BTIs for the RSS in A-BFT.
  uint32_t m_rssBackoffLimit;     //!< Maximum RSS Backoff value.
  Ptr<UniformRandomVariable>
      m_rssBackoffVariable;      //!< Random variable for the RSS Backoff value.
  bool m_staAvailabilityElement; //!< Flag to indicate whether we include STA
                                 //!< Availability element in DMG Beacon.
  bool m_immediateAbft; //!< Flag to indicate if we start A-BFT after receiving
                        //!< fragmented A-BFT.

  /* DMG Relay Support Variables */
  bool m_relayMode;     //!< Flag to indicate if we are in relay mode (For RDS).
  bool m_rdsDuplexMode; //!< The duplex mode of the RDS.
  uint8_t m_relayDataSensingTime; //!< Relay Data Sensing Time reported by the
                                  //!< source REDS (MicroSeconds).

  TracedCallback<Mac48Address>
      m_channelReportReceived;  //!< Trace callback for Channel Measurement
                                //!< completion.
  EventId m_linkChangeInterval; //!< Event ID for the link change interval.
  EventId m_firstPeriod;        //!< Event ID for the first period.
  EventId m_secondPeriod;       //!< Event ID for the second period.
  bool m_relayDataExchanged;    //!< Flag to indicate a data has been exchanged
                             //!< during the relay operation mode.
  bool m_periodProtected; //!< Flag to indicate if the current SP allocation is
                          //!< protected by relay operation.

  typedef std::map<REDS_PAIR, RelayLinkInfo>
      RELAY_LINK_MAP; //!< Typedef to identify relay link information for pair
                      //!< of REDS.
  RELAY_LINK_MAP
      m_relayLinkMap; //!< List to store information related to relay links.
  RelayLinkInfo
      m_relayLinkInfo; //!< Information about the relay link being established.

  TracedCallback<Mac48Address, TransmissionLink> m_transmissionLinkChanged;
  TracedCallback<RelayCapableStaList> m_rdsDiscoveryCompletedCallback;

  /** Dynamic Allocation of Service Period **/
  bool m_pollingPhase; //!< Flag to indicate if we participate in the polling
                       //!< phase.

  /* Spatial Sharing and Interference Mitigation */
  bool m_supportSpsh; //!< Flag to indicate whether we support Spatial Sharing
                      //!< and Interference Mitigation.

  /** DMG BSS peer and service discovery **/
  TracedCallback<Mac48Address> m_informationReceived;

  enum AbftState {
    WAIT_BEAMFORMING_TRAINING = 0,
    BEAMFORMING_TRAINING_COMPLETED = 1,
    BEAMFORMING_TRAINING_PARTIALLY_COMPLETED = 2,
    FAILED_BEAMFORMING_TRAINING = 3,
    START_RSS_BACKOFF_STATE = 4,
    IN_RSS_BACKOFF_STATE = 5,
  };

  AbftState m_abftState; //!< Beamforming Training State in A-BFT.
};

} // namespace ns3

#endif /* STA_WIGIG_MAC_H */
