/*
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-ctrl-headers.h"

#include "ns3/address-utils.h"

namespace ns3 {

/*************************
 *  Poll Frame (8.3.1.11)
 *************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgPoll);

CtrlDmgPoll::CtrlDmgPoll() : m_responseOffset(0) {}

CtrlDmgPoll::~CtrlDmgPoll() {}

TypeId CtrlDmgPoll::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgPoll")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgPoll>();
  return tid;
}

TypeId CtrlDmgPoll::GetInstanceTypeId() const { return GetTypeId(); }

void CtrlDmgPoll::Print(std::ostream &os) const {
  os << "Response Offset=" << m_responseOffset;
}

uint32_t CtrlDmgPoll::GetSerializedSize() const {
  return 2; // Response Offset Field.
}

void CtrlDmgPoll::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteHtolsbU16(m_responseOffset);
}

uint32_t CtrlDmgPoll::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_responseOffset = i.ReadLsbtohU16();
  return i.GetDistanceFrom(start);
}

void CtrlDmgPoll::SetResponseOffset(uint16_t value) {
  m_responseOffset = value;
}

uint16_t CtrlDmgPoll::GetResponseOffset() const { return m_responseOffset; }

/*************************************************
 *  Service Period Request (SPR) Frame (8.3.1.12)
 *************************************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgSpr);

CtrlDmgSpr::CtrlDmgSpr() {}

CtrlDmgSpr::~CtrlDmgSpr() {}

TypeId CtrlDmgSpr::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgSpr")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgSpr>();
  return tid;
}

TypeId CtrlDmgSpr::GetInstanceTypeId() const { return GetTypeId(); }

void CtrlDmgSpr::Print(std::ostream &os) const {
  m_dynamic.Print(os);
  m_bfControl.Print(os);
}

uint32_t CtrlDmgSpr::GetSerializedSize() const {
  return 7; // Dynamic Allocation Info Field + BF Control.
}

void CtrlDmgSpr::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;

  i = m_dynamic.Serialize(i);
  i = m_bfControl.Serialize(i);
}

uint32_t CtrlDmgSpr::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  i = m_dynamic.Deserialize(i);
  i = m_bfControl.Deserialize(i);

  return i.GetDistanceFrom(start);
}

void CtrlDmgSpr::SetDynamicAllocationInfo(DynamicAllocationInfoField field) {
  m_dynamic = field;
}

void CtrlDmgSpr::SetBfControl(BfControlField value) { m_bfControl = value; }

DynamicAllocationInfoField
CtrlDmgSpr::CtrlDmgSpr::GetDynamicAllocationInfo() const {
  return m_dynamic;
}

BfControlField CtrlDmgSpr::GetBfControl() const { return m_bfControl; }

/*************************
 * Grant Frame (8.3.1.13)
 *************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgGrant);

CtrlDmgGrant::CtrlDmgGrant() {}

CtrlDmgGrant::~CtrlDmgGrant() {}

TypeId CtrlDmgGrant::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgGrant")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgGrant>();
  return tid;
}

TypeId CtrlDmgGrant::GetInstanceTypeId() const { return GetTypeId(); }

/****************************************
 *  Sector Sweep (SSW) Frame (8.3.1.16)
 ****************************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgSsw);

CtrlDmgSsw::CtrlDmgSsw() {}

CtrlDmgSsw::~CtrlDmgSsw() {}

TypeId CtrlDmgSsw::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgSsw")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgSsw>();
  return tid;
}

TypeId CtrlDmgSsw::GetInstanceTypeId() const { return GetTypeId(); }

void CtrlDmgSsw::Print(std::ostream &os) const {
  m_ssw.Print(os);
  m_sswFeedback.Print(os);
}

uint32_t CtrlDmgSsw::GetSerializedSize() const {
  return 6; // SSW Field + SSW Feedback Field.
}

void CtrlDmgSsw::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;

  i = m_ssw.Serialize(i);
  i = m_sswFeedback.Serialize(i);
}

uint32_t CtrlDmgSsw::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  i = m_ssw.Deserialize(i);
  i = m_sswFeedback.Deserialize(i);

  return i.GetDistanceFrom(start);
}

void CtrlDmgSsw::SetSswField(DmgSswField &field) { m_ssw = field; }

void CtrlDmgSsw::SetSswFeedbackField(DmgSswFbckField &field) {
  m_sswFeedback = field;
}

DmgSswField CtrlDmgSsw::GetSswField() const { return m_ssw; }

DmgSswFbckField CtrlDmgSsw::GetSswFeedbackField() const {
  return m_sswFeedback;
}

/*********************************************************
 *  Sector Sweep Feedback (SSW-Feedback) Frame (8.3.1.17)
 *********************************************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgSswFbck);

CtrlDmgSswFbck::CtrlDmgSswFbck() {}

CtrlDmgSswFbck::~CtrlDmgSswFbck() {}

TypeId CtrlDmgSswFbck::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgSswFbck")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgSswFbck>();
  return tid;
}

TypeId CtrlDmgSswFbck::GetInstanceTypeId() const { return GetTypeId(); }

void CtrlDmgSswFbck::Print(std::ostream &os) const {
  m_sswFeedback.Print(os);
  m_brpRequest.Print(os);
  m_linkMaintenance.Print(os);
}

uint32_t CtrlDmgSswFbck::GetSerializedSize() const {
  return 8; // SSW Feedback Field + BRP Request + Beamformed Link Maintenance.
}

void CtrlDmgSswFbck::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;

  i = m_sswFeedback.Serialize(i);
  i = m_brpRequest.Serialize(i);
  i = m_linkMaintenance.Serialize(i);
}

uint32_t CtrlDmgSswFbck::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  i = m_sswFeedback.Deserialize(i);
  i = m_brpRequest.Deserialize(i);
  i = m_linkMaintenance.Deserialize(i);

  return i.GetDistanceFrom(start);
}

void CtrlDmgSswFbck::SetSswFeedbackField(DmgSswFbckField &field) {
  m_sswFeedback = field;
}

void CtrlDmgSswFbck::SetBrpRequestField(BrpRequestField &field) {
  m_brpRequest = field;
}

void CtrlDmgSswFbck::SetBfLinkMaintenanceField(BfLinkMaintenanceField &field) {
  m_linkMaintenance = field;
}

DmgSswFbckField CtrlDmgSswFbck::GetSswFeedbackField() const {
  return m_sswFeedback;
}

BrpRequestField CtrlDmgSswFbck::GetBrpRequestField() const {
  return m_brpRequest;
}

BfLinkMaintenanceField CtrlDmgSswFbck::GetBfLinkMaintenanceField() const {
  return m_linkMaintenance;
}

/**********************************************
 * Sector Sweep ACK (SSW-ACK) Frame (8.3.1.18)
 **********************************************/

NS_OBJECT_ENSURE_REGISTERED(CtrlDmgSswAck);

CtrlDmgSswAck::CtrlDmgSswAck() {}

CtrlDmgSswAck::~CtrlDmgSswAck() {}

TypeId CtrlDmgSswAck::GetTypeId() {
  static TypeId tid = TypeId("ns3::CtrlDmgSswAck")
                          .SetParent<Header>()
                          .AddConstructor<CtrlDmgSswAck>();
  return tid;
}

TypeId CtrlDmgSswAck::GetInstanceTypeId() const { return GetTypeId(); }

} // namespace ns3
