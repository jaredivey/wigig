/*
 * Copyright (c) 2005, 2006 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef MAC_LOW_H
#define MAC_LOW_H

#include "block-ack-cache.h"
#include "mac-low-transmission-parameters.h"
#include "wigig-channel-access-manager.h"
#include "wigig-data-types.h"
#include "wigig-mac-header.h"
#include "wigig-tx-vector.h"

#include "ns3/block-ack-type.h"
#include "ns3/nstime.h"
#include "ns3/object.h"
#include "ns3/qos-utils.h"
#include "ns3/wifi-mpdu-type.h"

#include <map>

namespace ns3 {

class WigigMacQueueItem;
class WigigMacQueue;
class WigigMac;
class WigigPhy;
class WigigTxop;
class WigigQosTxop;
class WigigPsdu;
class WigigBlockAckAgreement;
class WigigRemoteStationManager;
class WigigMsduAggregator;
class WigigMpduAggregator;
class MgtAddBaResponseHeader;
class CtrlBAckRequestHeader;
class CtrlBAckResponseHeader;

/**
 * \ingroup wigig
 * \brief handle RTS/CTS/Data/Ack transactions.
 */
class MacLow : public Object {
public:
  /**
   * typedef for a callback for MacLowRx
   */
  typedef Callback<void, Ptr<WigigMacQueueItem>> MacLowRxCallback;

  MacLow();
  ~MacLow() override;

  /**
   * Register this type.
   * \return The TypeId.
   */
  static TypeId GetTypeId();

  /**
   * Set up WigigPhy associated with this MacLow.
   *
   * \param phy WigigPhy associated with this MacLow
   */
  void SetPhy(const Ptr<WigigPhy> phy);
  /**
   * \return current attached PHY device
   */
  Ptr<WigigPhy> GetPhy() const;
  /**
   * \param tid the Traffic ID
   * \return the WigigQosTxop corresponding to the given TID
   */
  Ptr<WigigQosTxop> GetEdca(uint8_t tid) const;
  /**
   * Set up WigigMac associated with this MacLow.
   *
   * \param mac WigigMac associated with this MacLow
   */
  void SetMac(const Ptr<WigigMac> mac);
  /**
   * Set up WigigRemoteStationManager associated with this MacLow.
   *
   * \param manager WigigRemoteStationManager associated with this MacLow
   */
  void
  SetWifiRemoteStationManager(const Ptr<WigigRemoteStationManager> manager);
  /**
   * Set MAC address of this MacLow.
   *
   * \param ad Mac48Address of this MacLow
   */
  void SetAddress(Mac48Address ad);
  /**
   * Set Ack timeout of this MacLow.
   *
   * \param ackTimeout Ack timeout of this MacLow
   */
  void SetAckTimeout(Time ackTimeout);
  /**
   * Set Basic BlockAck timeout of this MacLow.
   *
   * \param blockAckTimeout Basic BlockAck timeout of this MacLow
   */
  void SetBasicBlockAckTimeout(Time blockAckTimeout);
  /**
   * Set Compressed BlockAck timeout of this MacLow.
   *
   * \param blockAckTimeout Compressed BlockAck timeout of this MacLow
   */
  void SetCompressedBlockAckTimeout(Time blockAckTimeout);
  /**
   * Enable or disable CTS-to-self capability.
   *
   * \param enable Enable or disable CTS-to-self capability
   */
  void SetCtsToSelfSupported(bool enable);
  /**
   * Set Short Interframe Space (SIFS) of this MacLow.
   *
   * \param sifs SIFS of this MacLow
   */
  void SetSifs(Time sifs);
  /**
   * Set Reduced Interframe Space (RIFS) of this MacLow.
   *
   * \param rifs RIFS of this MacLow
   */
  void SetRifs(Time rifs);
  /**
   * Set slot duration of this MacLow.
   *
   * \param slotTime slot duration of this MacLow
   */
  void SetSlotTime(Time slotTime);
  /**
   * Set PCF Interframe Space (PIFS) of this MacLow.
   *
   * \param pifs PIFS of this MacLow
   */
  void SetPifs(Time pifs);
  /**
   * \param interval the expected interval between two beacon transmissions.
   */
  void SetBeaconInterval(Time interval);
  /**
   * Set Short Beamforming Interframe Space (SBIFS) of this MacLow.
   *
   * \param sbifs SBIFS of this MacLow
   */
  void SetSbifs(Time sbifs);
  /**
   * Set Medimum Beamforming Interframe Space (SBIFS) of this MacLow.
   *
   * \param mbifs MBIFS of this MacLow
   */
  void SetMbifs(Time mbifs);
  /**
   * Set Long Beamforming Interframe Space (LBIFS) of this MacLow.
   *
   * \param lbifs LBIFS of this MacLow
   */
  void SetLbifs(Time lbifs);
  /**
   * Set the Basic Service Set Identification.
   *
   * \param ad the BSSID
   */
  void SetBssid(Mac48Address ad);
  /**
   * Enable promiscuous mode.
   */
  void SetPromisc();
  /**
   * Return whether CTS-to-self capability is supported.
   *
   * \return true if CTS-to-self is supported, false otherwise
   */
  bool GetCtsToSelfSupported() const;
  /**
   * Return the MAC address of this MacLow.
   *
   * \return Mac48Address of this MacLow
   */
  Mac48Address GetAddress() const;
  /**
   * Return Ack timeout of this MacLow.
   *
   * \return Ack timeout
   */
  Time GetAckTimeout() const;
  /**
   * Return Basic BlockAck timeout of this MacLow.
   *
   * \return Basic BlockAck timeout
   */
  Time GetBasicBlockAckTimeout() const;
  /**
   * Return Compressed BlockAck timeout of this MacLow.
   *
   * \return Compressed BlockAck timeout
   */
  Time GetCompressedBlockAckTimeout() const;
  /**
   * Return Short Interframe Space (SIFS) of this MacLow.
   *
   * \return SIFS
   */
  Time GetSifs() const;
  /**
   * Return slot duration of this MacLow.
   *
   * \return slot duration
   */
  Time GetSlotTime() const;
  /**
   * Return PCF Interframe Space (PIFS) of this MacLow.
   *
   * \return PIFS
   */
  Time GetPifs() const;
  /**
   * Return Reduced Interframe Space (RIFS) of this MacLow.
   *
   * \return RIFS
   */
  Time GetRifs() const;
  /**
   * \return the expected interval between two beacon transmissions.
   */
  Time GetBeaconInterval() const;
  /**
   * Return Short Beamforming Interframe Space (SBIFS) of this MacLow.
   *
   * \return SBIFS
   */
  Time GetSbifs() const;
  /**
   * Return Medium Beamforming Interframe Space (SBIFS) of this MacLow.
   *
   * \return MBIFS
   */
  Time GetMbifs() const;
  /**
   * Return Large Beamforming Interframe Space (LBIFS) of this MacLow.
   *
   * \return LBIFS
   */
  Time GetLbifs() const;
  /**
   * Return the Basic Service Set Identification.
   *
   * \return BSSID
   */
  Mac48Address GetBssid() const;

  /**
   * \param callback the callback which receives every incoming packet.
   *
   * This callback typically forwards incoming packets to
   * an instance of ns3::WigigMacRxMiddle.
   */
  void SetRxCallback(Callback<void, Ptr<WigigMacQueueItem>> callback);
  /**
   * \param channelAccessManager pointer to WigigChannelAccessManager in order
   * to listen to NAV events for every incoming and outgoing packet.
   */
  void RegisterWigigChannelAccessManager(
      Ptr<WigigChannelAccessManager> channelAccessManager);

  /**
   * Check whether the given MPDU, if transmitted according to the given TX
   * vector, meets the constraint on the maximum A-MPDU size (by assuming that
   * the frame has to be aggregated to an existing A-MPDU of the given size) and
   * its transmission time exceeds neither the max PPDU duration (depending on
   * the PPDU format) nor the given PPDU duration limit (if strictly positive).
   * The given MPDU needs to be a QoS Data frame.
   *
   * \param mpdu the MPDU.
   * \param txVector the TX vector used to transmit the MPDU
   * \param ampduSize the size of the existing A-MPDU in bytes, if any
   * \param ppduDurationLimit the limit on the PPDU duration
   * \returns true if constraints on size and duration limit are met.
   */
  bool IsWithinSizeAndTimeLimits(Ptr<const WigigMacQueueItem> mpdu,
                                 const WigigTxVector &txVector,
                                 uint32_t ampduSize, Time ppduDurationLimit);
  /**
   * Check whether an MPDU of the given size, destined to the given receiver and
   * belonging to the given TID, if transmitted according to the given TX
   * vector, meets the constraint on the maximum A-MPDU size (by assuming that
   * the frame has to be aggregated to an existing A-MPDU of the given size) and
   * its transmission time exceeds neither the max PPDU duration (depending on
   * the PPDU format) nor the given PPDU duration limit (if strictly positive).
   *
   * \param mpduSize the MPDU size.
   * \param receiver the receiver
   * \param tid the TID
   * \param txVector the TX vector used to transmit the MPDU
   * \param ampduSize the size of the existing A-MPDU in bytes, if any
   * \param ppduDurationLimit the limit on the PPDU duration
   * \returns true if constraints on size and duration limit are met.
   */
  bool IsWithinSizeAndTimeLimits(uint32_t mpduSize, Mac48Address receiver,
                                 uint8_t tid, const WigigTxVector &txVector,
                                 uint32_t ampduSize, Time ppduDurationLimit);
  /**
   * \param packet to send (does not include the 802.11 MAC header and checksum)
   * \param hdr header associated to the packet to send.
   * \param parameters transmission parameters of packet.
   * \return the transmission time that includes the time for the next packet
   * transmission
   *
   * This transmission time includes the time required for
   * the next packet transmission if one was selected.
   */
  Time CalculateTransmissionTime(
      Ptr<const Packet> packet, const WigigMacHeader *hdr,
      const MacLowTransmissionParameters &parameters) const;

  /**
   * \param packet to send (does not include the 802.11 MAC header and checksum)
   * \param hdr header associated to the packet to send.
   * \param params transmission parameters of packet.
   * \param fragmentSize the packet fragment size (if fragmentation is used) in
   * bytes \return the transmission time that includes the time for the next
   * packet transmission
   *
   * This transmission time does not include the time required for
   * the next packet transmission if one was selected.
   */
  Time CalculateOverallTxTime(Ptr<const Packet> packet,
                              const WigigMacHeader *hdr,
                              const MacLowTransmissionParameters &params,
                              uint32_t fragmentSize = 0) const;

  /**
   * \param item packet to send (does not include the 802.11 MAC header and
   * checksum) \param params transmission parameters of packet. \return the
   * transmission time that does not include the time required to transmit the
   * frame
   *
   * This transmission time  only includes the time for the RTS/CTS exchange (if
   * any) and for the Ack frame (if any).
   */
  Time
  CalculateOverheadTxTime(Ptr<const WigigMacQueueItem> item,
                          const MacLowTransmissionParameters &params) const;

  /**
   * \param mpdu packet to send
   * \param parameters the transmission parameters to use for this packet.
   * \param txop pointer to the calling WigigTxop.
   *
   * Start the transmission of the input packet and notify the listener
   * of transmission events.
   */
  virtual void StartTransmission(Ptr<WigigMacQueueItem> mpdu,
                                 MacLowTransmissionParameters parameters,
                                 Ptr<WigigTxop> txop);

  typedef Callback<void, const WigigMacHeader &> TransmissionOkCallback;
  typedef Callback<void> TransmissionShortSswOkCallback;

  /**
   * \param mpdu packet to send
   * \param parameters the transmission parameters to use for this packet.
   * \param txop pointer to the calling WigigTxop.
   *
   * Start the transmission of the input packet and notify the listener
   * of transmission events.
   * Note: This function is used for DMG management and control frames
   * transmission.
   */
  void TransmitSingleFrame(Ptr<WigigMacQueueItem> mpdu,
                           MacLowTransmissionParameters params,
                           Ptr<WigigTxop> txop);
  /**
   * \param mpdu packet to send
   * \param parameters the transmission parameters to use for the packet.
   * \param callback the callback to trigger upon completed transmission of the
   * packet.
   *
   * Start the transmission of the input packet immediately and trigger
   * the callback function once the packet transmission is completed.
   */
  virtual void
  StartTransmissionImmediately(Ptr<WigigMacQueueItem> mpdu,
                               MacLowTransmissionParameters parameters,
                               TransmissionOkCallback callback);

  /**
   * IsCurrentAllocationEmpty
   * \return True if the current allocation pointer is empty, otherwise false.
   */
  bool IsCurrentAllocationEmpty() const;
  /**
   * Resume Transmission for the current allocation if transmission has been
   * suspended. \param duration The remaining duration of the current resumed
   * allocation. \param txop Pointer to the transmit opprtunity associated with
   * the suspedned transmission.
   */
  void ResumeTransmission(Time duration, Ptr<WigigTxop> txop);
  /**
   * Abort suspended transmission - in case the suspended packet lifetime has
   * passed.
   */
  void AbortSuspendedTransmission();

  /**
   * Restore Allocation Parameters for specific allocation SP or CBAP.
   * \param allocationId The ID of the allocation.
   */
  void RestoreAllocationParameters(AllocationId allocationId);
  /**
   * Store parameters related to the current allocation.
   */
  void StoreAllocationParameters();
  /**
   * This function is called upon the end of the current allocation period.
   */
  void EndAllocationPeriod();
  /**
   * Check whether a transmission has been suspended due to time constraints for
   * the restored or current allocation. \return True if transmission has been
   * suspended, otherwise false.
   */
  bool IsTransmissionSuspended() const;
  /**
   * Check whether we have completed the transmission of a suspended PSDU for
   * the given WigigTxop. \param txop A pointer to the WigigTxop utilizing the
   * current allocation for which the PSDU transmission has been suspended.
   * \return True if we've completed the transmission for the restored suspended
   * transmission corresponding to the given WigigTxop, otherwise false.
   */
  bool CompletedSuspendedPsduTransmission(Ptr<WigigTxop> txop) const;

  /**
   * \param txVector the TXVECTOR decoded from PHY header.
   * \param psduDuration the duration of the PSDU that is about to be received.
   *
   * This method is typically invoked by the lower PHY layer to notify
   * the MAC layer that the reception of a PSDU is starting.
   * This is equivalent to the PHY-RXSTART primitive.
   * If the reception is correct for at least one MPDU of the PSDU
   * the DeaggregateAmpduAndReceive will be called after \p psduDuration.
   * Otherwise, ReceiveError will be called after the same duration.
   */
  void RxStartIndication(const WigigTxVector &txVector, Time psduDuration);

  /**
   * \param mpdu MPDU received
   * \param rxSnr snr of MPDU received in linear scale
   * \param txVector TXVECTOR of MPDU received
   * \param ampduSubframe true if this MPDU is part of an A-MPDU
   *
   * This method is typically invoked by the lower PHY layer to notify
   * the MAC layer that an MPDU was successfully received.
   */
  void ReceiveOk(Ptr<WigigMacQueueItem> mpdu, double rxSnr,
                 const WigigTxVector &txVector, bool ampduSubframe);

  /**
   * \param psdu PSDU received.
   *
   * This method is typically invoked by the lower PHY layer to notify
   * the MAC layer that a PSDU was unsuccessfully received.
   */
  void ReceiveError(Ptr<WigigPsdu> psdu);
  /**
   * \param duration switching delay duration.
   *
   * This method is typically invoked by the PhyMacLowListener to notify
   * the MAC layer that a channel switching occurred. When a channel switching
   * occurs, pending MAC transmissions (RTS, CTS, Data and Ack) are cancelled.
   */
  void NotifySwitchingStartNow(Time duration);
  /**
   * This method is typically invoked by the PhyMacLowListener to notify
   * the MAC layer that the device has been put into sleep mode. When the device
   * is put into sleep mode, pending MAC transmissions (RTS, CTS, Data and Ack)
   * are cancelled.
   */
  void NotifySleepNow();
  /**
   * This method is typically invoked by the PhyMacLowListener to notify
   * the MAC layer that the device has been put into off mode. When the device
   * is put into off mode, pending MAC transmissions (RTS, CTS, Data and Ack)
   * are cancelled.
   */
  void NotifyOffNow();
  /**
   * \param respHdr Add block ack response from originator (action
   *        frame).
   * \param originator Address of peer station involved in block ack
   *        mechanism.
   * \param startingSeq Sequence number of the first MPDU of all
   *        packets for which block ack was negotiated.
   *
   * This function is typically invoked only by ns3::WigigMac
   * when the STA (which may be non-AP in ESS, or in an IBSS) has
   * received an ADDBA Request frame and is transmitting an ADDBA
   * Response frame. At this point MacLow must allocate buffers to
   * collect all correctly received packets belonging to the category
   * for which block ack was negotiated.
   */
  void CreateWigigBlockAckAgreement(const MgtAddBaResponseHeader *respHdr,
                                    Mac48Address originator,
                                    uint16_t startingSeq);
  /**
   * \param originator Address of peer participating in block ack mechanism.
   * \param tid TID for which block ack was created.
   *
   * Checks if exists an established block ack agreement with <i>originator</i>
   * for TID <i>tid</i>. If the agreement exists, tears down it. This function
   * is typically invoked when a DELBA frame is received from <i>originator</i>.
   */
  void DestroyWigigBlockAckAgreement(Mac48Address originator, uint8_t tid);
  /**
   * \param ac Access class managed by the queue.
   * \param edca the WigigQosTxop for the queue.
   *
   * The lifetime of the registered WigigQosTxop is typically equal to the
   * lifetime of the queue associated to this AC.
   */
  void RegisterEdcaForAc(AcIndex ac, Ptr<WigigQosTxop> edca);
  /**
   * \param aggregatedPacket which is the current A-MPDU
   * \param rxSnr SNR of packet received in linear scale
   * \param txVector TXVECTOR of packet received
   * \param statusPerMpdu reception status per MPDU
   *
   * This function de-aggregates an A-MPDU and decide if each MPDU is received
   * correctly or not
   *
   */
  void DeaggregateAmpduAndReceive(Ptr<WigigPsdu> aggregatedPacket, double rxSnr,
                                  const WigigTxVector &txVector,
                                  std::vector<bool> statusPerMpdu);

  /**
   * Return a TXVECTOR for the Data frame given the destination.
   * The function consults WigigRemoteStationManager, which controls the rate
   * to different destinations.
   *
   * \param item the item being asked for TXVECTOR
   * \return TXVECTOR for the given item
   */
  virtual WigigTxVector
  GetDataTxVector(Ptr<const WigigMacQueueItem> item) const;
  /**
   * Start NAV with the given duration.
   *
   * \param duration the duration
   * \return true if NAV is reset
   */
  bool DoNavStartNow(Time duration);

  /**
   * Return a TXVECTOR for the DMG Control frame given the destination.
   * The function consults WigigRemoteStationManager, which controls the rate
   * to different destinations.
   *
   * \param item the item being asked for TXVECTOR
   * \return TXVECTOR for the given item
   */
  WigigTxVector GetDmgTxVector(Ptr<const WigigMacQueueItem> item) const;
  /**
   * SLS phase has started.
   */
  void SlsPhaseStarted();
  /**
   * SLS phase has ended.
   */
  void SlsPhaseEnded();
  /**
   * Return whether we are performing SLS or not.
   * \return True if we are performing SLS, otherwise false.
   */
  bool IsPerformingSls() const;

  /**
   * Returns the aggregator used to construct A-MSDU subframes.
   *
   * \return the aggregator used to construct A-MSDU subframes.
   */
  Ptr<WigigMsduAggregator> GetWigigMsduAggregator() const;
  /**
   * Returns the aggregator used to construct A-MPDU subframes.
   *
   * \return the aggregator used to construct A-MPDU subframes.
   */
  Ptr<WigigMpduAggregator> GetWigigMpduAggregator() const;

  /**
   * Set the aggregator used to construct A-MSDU subframes.
   *
   * \param aggr pointer to the MSDU aggregator.
   */
  void SetMsduAggregator(const Ptr<WigigMsduAggregator> aggr);
  /**
   * Set the aggregator used to construct A-MPDU subframes.
   *
   * \param aggr pointer to the MPDU aggregator.
   */
  void SetMpduAggregator(const Ptr<WigigMpduAggregator> aggr);
  /**
   * Calculate DMG transaction time including RTS/CTS, PSDU transmission time,
   * acknowledgemen , and interframe spacing. \param psdu Pointer to the PSDU to
   * be transmitted. \return The total transaction time.
   */
  Time CalculateWiGigTransactionTime(Ptr<WigigPsdu> psdu);

private:
  /**
   * Cancel all scheduled events. Called before beginning a transmission
   * or switching channel.
   */
  void CancelAllEvents();
  /**
   * Forward a PSDU down to WigigPhy for transmission.
   *
   * \param psdu the PSDU
   * \param txVector the transmit vector
   */
  void ForwardDown(Ptr<const WigigPsdu> psdu, const WigigTxVector &txVector);
  /**
   * Return a TXVECTOR for the RTS frame given the destination.
   * The function consults WigigRemoteStationManager, which controls the rate
   * to different destinations.
   *
   * \param item the item being asked for RTS TXVECTOR
   * \return TXVECTOR for the RTS of the given item
   */
  WigigTxVector GetRtsTxVector(Ptr<const WigigMacQueueItem> item) const;
  /**
   * Return the total DMG CTS size (including FCS trailer).
   *
   * \return the total DMG CTS size
   */
  static uint32_t GetDmgCtsSize();
  /**
   * Get DMG Control TxVector
   * \return
   */
  WigigTxVector GetDmgControlTxVector() const;
  /**
   * Return a TXVECTOR for the CTS frame given the destination and the mode of
   * the RTS used by the sender. The function consults
   * WigigRemoteStationManager, which controls the rate to different
   * destinations.
   *
   * \param to the MAC address of the CTS receiver
   * \param rtsTxMode the mode of the RTS used by the sender
   * \return TXVECTOR for the CTS
   */
  WigigTxVector GetCtsTxVector(Mac48Address to, WifiMode rtsTxMode) const;
  /**
   * Return a TXVECTOR for the Ack frame given the destination and the mode of
   * the Data used by the sender. The function consults
   * WigigRemoteStationManager, which controls the rate to different
   * destinations.
   *
   * \param to the MAC address of the Ack receiver
   * \param dataTxMode the mode of the Data used by the sender
   * \return TXVECTOR for the Ack
   */
  WigigTxVector GetAckTxVector(Mac48Address to, WifiMode dataTxMode) const;
  /**
   * Return a TXVECTOR for the BlockAck frame given the destination and the mode
   * of the Data used by the sender. The function consults
   * WigigRemoteStationManager, which controls the rate to different
   * destinations.
   *
   * \param to the MAC address of the BlockAck receiver
   * \param dataTxMode the mode of the Data used by the sender
   * \return TXVECTOR for the BlockAck
   */
  WigigTxVector GetBlockAckTxVector(Mac48Address to, WifiMode dataTxMode) const;
  /**
   * Return a TXVECTOR for the CTS frame given the destination and the mode of
   * the RTS used by the sender. The function consults
   * WigigRemoteStationManager, which controls the rate to different
   * destinations.
   *
   * \param to the MAC address of the CTS receiver
   * \param rtsTxMode the mode of the RTS used by the sender
   * \return TXVECTOR for the CTS
   */
  WigigTxVector GetCtsTxVectorForRts(Mac48Address to, WifiMode rtsTxMode) const;
  /**
   * Return a TXVECTOR for the BlockAck frame given the destination and the mode
   * of the Data used by the sender. The function consults
   * WigigRemoteStationManager, which controls the rate to different
   * destinations.
   *
   * \param to the MAC address of the BlockAck receiver
   * \param dataTxMode the mode of the Data used by the sender
   * \return TXVECTOR for the BlockAck
   */
  WigigTxVector GetAckTxVectorForData(Mac48Address to,
                                      WifiMode dataTxMode) const;
  /**
   * Get control answer mode function.
   *
   * \param reqMode request mode
   *
   * \return control answer mode
   */
  WifiMode GetControlAnswerMode(WifiMode reqMode) const;
  /**
   * Return the time required to transmit the CTS (including preamble and FCS).
   *
   * \param ctsTxVector the TXVECTOR used to transmit the CTS
   * \return the time required to transmit the CTS (including preamble and FCS)
   */
  Time GetCtsDuration(const WigigTxVector &ctsTxVector) const;
  /**
   * Return the time required to transmit the CTS to the specified address
   * given the TXVECTOR of the RTS (including preamble and FCS).
   *
   * \param to the receiver MAC address
   * \param rtsTxVector the TXVECTOR used to transmit the RTS
   * \return the time required to transmit the CTS (including preamble and FCS)
   */
  Time GetCtsDuration(Mac48Address to, const WigigTxVector &rtsTxVector) const;
  /**
   * Return the time required to transmit the DMG CTS (including preamble and
   * FCS).
   *
   * \return the time required to transmit the DMG CTS (including preamble and
   * FCS)
   */
  Time GetDmgCtsDuration() const;
  /**
   * Return the time required to transmit the Ack (including preamble and FCS).
   *
   * \param ackTxVector the TXVECTOR used to transmit the Ack
   * \return the time required to transmit the Ack (including preamble and FCS)
   */
  Time GetAckDuration(const WigigTxVector &ackTxVector) const;
  /**
   * Return the time required to transmit the Ack to the specified address
   * given the TXVECTOR of the Data (including preamble and FCS).
   *
   * \param to the receiver MAC address
   * \param dataTxVector the TXVECTOR used to transmit the Data
   * \return the time required to transmit the Ack (including preamble and FCS)
   */
  Time GetAckDuration(Mac48Address to, const WigigTxVector &dataTxVector) const;
  /**
   * Return the time required to transmit the BlockAck to the specified address
   * given the TXVECTOR of the BAR (including preamble and FCS).
   *
   * \param blockAckReqTxVector the TXVECTOR used to transmit the BAR
   * \param type the BlockAck type
   * \return the time required to transmit the BlockAck (including preamble and
   * FCS)
   */
  Time GetBlockAckDuration(const WigigTxVector &blockAckReqTxVector,
                           BlockAckType type) const;
  /**
   * Return the time required to transmit the BlockAckRequest to the specified
   * address given the TXVECTOR (including preamble and FCS).
   *
   * \param blockAckReqTxVector the TX vector used to transmit the BAR
   * \param type the BlockAckRequest type
   * \return the time required to transmit the BlockAckRequest (including
   * preamble and FCS)
   */
  Time GetBlockAckRequestDuration(const WigigTxVector &blockAckReqTxVector,
                                  BlockAckReqType type) const;
  /**
   * Return the time required to transmit the response frames (Ack or BAR+BA
   * following the policy configured in the transmit parameters).
   *
   * \param params the transmission parameters
   * \param dataTxVector the TX vector used to transmit the data frame
   * \param receiver the station from which a response is expected
   * \return the time required to transmit the response (Ack or BAR+BA)
   */
  Time GetResponseDuration(const MacLowTransmissionParameters &params,
                           const WigigTxVector &dataTxVector,
                           Mac48Address receiver) const;
  /**
   * Check if CTS-to-self mechanism should be used for the current packet.
   *
   * \return true if CTS-to-self mechanism should be used for the current
   * packet, false otherwise
   */
  bool NeedCtsToSelf() const;

  /**
   * Notify NAV function.
   *
   * \param packet the packet
   * \param hdr the header
   */
  void NotifyNav(Ptr<const Packet> packet, const WigigMacHeader &hdr);
  /**
   * Reset NAV with the given duration.
   *
   * \param duration the duration to set
   */
  void DoNavResetNow(Time duration);
  /**
   * Check if NAV is zero.
   *
   * \return true if NAV is zero,
   *         false otherwise
   */
  bool IsNavZero() const;
  /**
   * Notify WigigChannelAccessManager that Ack timer should be started for the
   * given duration.
   *
   * \param duration the duration of the timer
   */
  void NotifyAckTimeoutStartNow(Time duration);
  /**
   * Notify WigigChannelAccessManager that Ack timer should be reset.
   */
  void NotifyAckTimeoutResetNow();
  /**
   * Notify WigigChannelAccessManager that CTS timer should be started for the
   * given duration.
   *
   * \param duration the duration of the timer
   */
  void NotifyCtsTimeoutStartNow(Time duration);
  /**
   * Notify WigigChannelAccessManager that CTS timer should be reset.
   */
  void NotifyCtsTimeoutResetNow();
  /* Event handlers */
  /**
   * Event handler when normal Ack timeout occurs.
   */
  void NormalAckTimeout();
  /**
   * Event handler when BlockAck timeout occurs.
   */
  void BlockAckTimeout();
  /**
   * Event handler when CTS timeout occurs.
   */
  void CtsTimeout();
  /**
   * Send CTS for a CTS-to-self mechanism.
   */
  void SendCtsToSelf();
  /**
   * Send CTS after receiving RTS.
   *
   * \param source the transmitter of the RTS
   * \param duration the NAV of the RTS
   * \param rtsTxVector the TXVECTOR used to transmit the RTS
   * \param rtsSnr the SNR of the RTS in linear scale
   */
  void SendCtsAfterRts(Mac48Address source, Time duration,
                       const WigigTxVector &rtsTxVector, double rtsSnr);
  /**
   * Send CTS after receiving RTS.
   *
   * \param source
   * \param duration
   * \param rtsTxVector
   * \param rtsSnr
   */
  void SendDmgCtsAfterRts(Mac48Address source, Time duration,
                          const WigigTxVector &rtsTxVector, double rtsSnr);
  /**
   * Send Ack after receiving Data.
   *
   * \param source the transmitter of the Data
   * \param duration the NAV of the Data
   * \param dataTxMode the TXVECTOR used to transmit the Data
   * \param dataSnr the SNR of the Data in linear scale
   */
  void SendAckAfterData(Mac48Address source, Time duration, WifiMode dataTxMode,
                        double dataSnr);
  /**
   * Send Data after receiving CTS.
   *
   * \param duration the NAV of the CTS
   */
  void SendDataAfterCts(Time duration);

  /**
   * Event handler that is usually scheduled to fired at the appropriate time
   * after completing transmissions.
   */
  void WaitIfsAfterEndTxFragment();
  /**
   * Event handler that is usually scheduled to fired at the appropriate time
   * after sending a packet.
   */
  void WaitIfsAfterEndTxPacket();

  /**
   * A transmission that does not require an Ack has completed.
   */
  void EndTxNoAck();
  /**
   * Send RTS to begin RTS-CTS-Data-Ack transaction.
   */
  void SendRtsForPacket();
  /**
   * Send Data packet, which can be Data-Ack or
   * RTS-CTS-Data-Ack transaction.
   */
  void SendDataPacket();
  /**
   * Start a Data timer by scheduling appropriate
   * Ack timeout.
   *
   * \param dataTxVector the TXVECTOR used to transmit the Data
   */
  void StartDataTxTimers(const WigigTxVector &dataTxVector);

  void DoDispose() override;

  /**
   * \param originator Address of peer participating in block ack mechanism.
   * \param tid TID for which block ack was created.
   * \param seq Starting sequence control
   *
   * This function forward up all completed "old" packets with sequence number
   * smaller than <i>seq</i>. All comparison are performed circularly modulo
   * 4096.
   */
  void RxCompleteBufferedPacketsWithSmallerSequence(uint16_t seq,
                                                    Mac48Address originator,
                                                    uint8_t tid);
  /**
   * \param originator Address of peer participating in block ack mechanism.
   * \param tid TID for which block ack was created.
   *
   * This method is typically invoked when a MPDU with ack policy
   * subfield set to Normal Ack is received and a block ack agreement
   * for that packet exists.
   * This happens when the originator of block ack has only few MPDUs to send.
   * All completed MSDUs starting with starting sequence number of block ack
   * agreement are forward up to WigigMac until there is an incomplete or
   * missing MSDU. See section 9.10.4 in IEEE 802.11 standard for more details.
   */
  void RxCompleteBufferedPacketsUntilFirstLost(Mac48Address originator,
                                               uint8_t tid);
  /**
   * \param mpdu the MPDU
   * \returns true if MPDU received
   *
   * This method updates the reorder buffer and the scoreboard when an MPDU is
   * received in an HT station and stores the MPDU if needed when an MPDU is
   * received in an non-HT Station (implements HT immediate BlockAck)
   */
  bool ReceiveMpdu(Ptr<WigigMacQueueItem> mpdu);
  /**
   * \param mpdu the MPDU
   * \returns true if the MPDU stored
   *
   * This method checks if exists a valid established block ack agreement.
   * If there is, store the packet without pass it up to WigigMac. The packet is
   * buffered in order of increasing sequence control field. All comparison are
   * performed circularly modulo 2^12.
   */
  bool StoreMpduIfNeeded(Ptr<WigigMacQueueItem> mpdu);
  /**
   * Invoked after that a BlockAckRequest has been received. Looks for
   * corresponding block ack agreement and creates a block ack bitmap on a
   * received packets basis.
   *
   * \param reqHdr the BAR header
   * \param originator the transmitter of the BAR
   * \param duration the NAV of the BAR
   * \param blockAckReqTxMode the TXVECTOR used to transmit the BAR
   * \param rxSnr the SNR of the BAR in linear scale
   */
  void SendBlockAckAfterBlockAckRequest(const CtrlBAckRequestHeader reqHdr,
                                        Mac48Address originator, Time duration,
                                        WifiMode blockAckReqTxMode,
                                        double rxSnr);
  /**
   * Invoked after an A-MPDU has been received. Looks for corresponding
   * block ack agreement and creates a block ack bitmap on a received packets
   * basis.
   *
   * \param tid the Traffic ID
   * \param originator the originator MAC address
   * \param duration the remaining NAV duration
   * \param blockAckReqTxVector the transmit vector
   * \param rxSnr the receive SNR in linear scale
   */
  void SendBlockAckAfterAmpdu(uint8_t tid, Mac48Address originator,
                              Time duration,
                              const WigigTxVector &blockAckReqTxVector,
                              double rxSnr);
  /**
   * This method creates BlockAck frame with header equals to <i>blockAck</i>
   * and start its transmission.
   *
   * \param blockAck the BA response header to send
   * \param originator the station to send a BA to
   * \param immediate use immediate BA policy if true
   * \param duration the NAV duration
   * \param blockAckReqTxMode the TXVECTOR used to transmit the BAR
   * \param rxSnr the received SNR in linear scale
   */
  void SendBlockAckResponse(const CtrlBAckResponseHeader *blockAck,
                            Mac48Address originator, bool immediate,
                            Time duration, WifiMode blockAckReqTxMode,
                            double rxSnr);
  /**
   * Every time that a BlockAckRequest or a packet with Ack Policy equals to
   * <i>Block Ack</i> are received, if a relative block ack agreement exists and
   * the value of inactivity timeout is not 0, the timer is reset. see
   * section 11.5.3 in IEEE 802.11e for more details.
   *
   * \param agreement the BA agreement
   */
  void ResetBlockAckInactivityTimerIfNeeded(WigigBlockAckAgreement &agreement);

  /**
   * Set up WigigPhy listener for this MacLow.
   *
   * \param phy the WigigPhy this MacLow is connected to
   */
  void SetupPhyMacLowListener(const Ptr<WigigPhy> phy);
  /**
   * Remove current WigigPhy listener for this MacLow.
   *
   * \param phy the WigigPhy this MacLow is connected to
   */
  void RemovePhyMacLowListener(Ptr<WigigPhy> phy);

  Ptr<WigigPhy> m_phy; //!< Pointer to WigigPhy (actually send/receives frames)
  Ptr<WigigMac> m_mac; //!< Pointer to WigigMac (to fetch configuration)
  Ptr<WigigRemoteStationManager>
      m_stationManager; //!< Pointer to WigigRemoteStationManager (rate control)
  MacLowRxCallback m_rxCallback; //!< Callback to pass packet up

  /**
   * typedef for an iterator for a list of WigigChannelAccessManager.
   */
  typedef std::vector<Ptr<WigigChannelAccessManager>>::const_iterator
      WigigChannelAccessManagersCI;
  /**
   * typedef for a list of WigigChannelAccessManager.
   */
  typedef std::vector<Ptr<WigigChannelAccessManager>>
      WigigChannelAccessManagers;
  WigigChannelAccessManagers
      m_channelAccessManagers; //!< List of WigigChannelAccessManager

  Ptr<WigigMsduAggregator> m_msduAggregator; //!< A-MSDU aggregator
  Ptr<WigigMpduAggregator> m_mpduAggregator; //!< A-MPDU aggregator

  EventId m_normalAckTimeoutEvent; //!< Normal Ack timeout event
  EventId m_blockAckTimeoutEvent;  //!< BlockAck timeout event
  EventId m_ctsTimeoutEvent;       //!< CTS timeout event
  EventId m_sendCtsEvent;          //!< Event to send CTS
  EventId m_sendAckEvent;          //!< Event to send Ack
  EventId m_sendDataEvent;         //!< Event to send Data
  EventId m_waitIfsEvent;          //!< Wait for IFS event
  EventId m_endTxNoAckEvent; //!< Event for finishing transmission that does not
                             //!< require Ack
  EventId m_navCounterResetCtsMissed; //!< Event to reset NAV when CTS is not
                                      //!< received

  Ptr<WigigPsdu>
      m_currentPacket; //!< Current packet transmitted/to be transmitted
  Ptr<WigigTxop> m_currentTxop; //!< Current TXOP
  MacLowTransmissionParameters
      m_txParams;       //!< Transmission parameters of the current packet
  Mac48Address m_self;  //!< Address of this MacLow (Mac48Address)
  Mac48Address m_bssid; //!< BSSID address (Mac48Address)
  Time m_ackTimeout;    //!< Ack timeout duration
  Time m_basicBlockAckTimeout;      //!< Basic BlockAck timeout duration
  Time m_compressedBlockAckTimeout; //!< Compressed BlockAck timeout duration
  Time m_sifs;                      //!< Short Interframe Space (SIFS) duration
  Time m_slotTime;                  //!< Slot duration
  Time m_pifs;                      //!< PCF Interframe Space (PIFS) duration
  Time m_rifs; //!< Reduced Interframe Space (RIFS) duration

  Time m_sbifs; //!< Short Beamforming Interframe Space (SBIFS) duration.
  Time m_mbifs; //!< Medium Beamforming Interframe Space (MBIS) duration.
  Time m_lbifs; //!< Long Beamforming Interframe Space (LBIFS) duration.
  Time m_beaconInterval; //!< Expected interval between two beacon transmissions

  Time m_lastNavStart;    //!< The time when the latest NAV started
  Time m_lastNavDuration; //!< The duration of the latest NAV

  Time m_lastBeacon; //!< The time when the last beacon frame transmission
                     //!< started

  bool m_promisc; //!< Flag if the device is operating in promiscuous mode

  class PhyMacLowListener
      *m_phyMacLowListener; //!< Listener needed to monitor when a channel
                            //!< switching occurs.

  /*
   * BlockAck data structures.
   */
  typedef std::list<Ptr<WigigMacQueueItem>>::iterator
      BufferedPacketI; //!< buffered packet iterator typedef

  typedef std::pair<Mac48Address, uint8_t>
      AgreementKey; //!< agreement key typedef
  typedef std::pair<WigigBlockAckAgreement, std::list<Ptr<WigigMacQueueItem>>>
      AgreementValue; //!< agreement value typedef

  typedef std::map<AgreementKey, AgreementValue> Agreements; //!< agreements
  typedef std::map<AgreementKey, AgreementValue>::iterator
      AgreementsI; //!< agreements iterator

  typedef std::map<AgreementKey, BlockAckCache>
      BlockAckCaches; //!< block ack caches typedef
  typedef std::map<AgreementKey, BlockAckCache>::iterator
      BlockAckCachesI; //!< block ack caches iterator typedef

  Agreements m_bAckAgreements; //!< block ack agreements
  BlockAckCaches m_bAckCaches; //!< block ack caches

  typedef std::map<AcIndex, Ptr<WigigQosTxop>>
      QueueEdcas;    //!< EDCA queues typedef
  QueueEdcas m_edca; //!< EDCA queues

  bool m_ctsToSelfSupported; //!< Flag whether CTS-to-self is supported
  WigigTxVector
      m_currentTxVector; //!< TXVECTOR used for the current packet transmission

  TransmissionOkCallback m_transmissionCallback;
  TransmissionShortSswOkCallback m_transmissionShortSswCallback;

  /* Variables for suspended data transmission for different allocation periods
   */
  struct AllocationParameters : public SimpleRefCount<AllocationParameters> {
    MacLowTransmissionParameters
        txParams; //!< Suspended transmission MacLow parameters.
    WigigTxVector
        txVector; //!< TxVector associated with the suspended transmission.
    Ptr<WigigTxop> txop; //!< Transmit opportunity responsible for this
                         //!< suspended transmission.
    Ptr<WigigPsdu> psdu; //!< PSDU to to be transmitted.
  };

  typedef std::map<AllocationId, Ptr<AllocationParameters>>
      AllocationPeriodsTable;
  typedef AllocationPeriodsTable::const_iterator AllocationPeriodsTableCI;
  typedef AllocationPeriodsTable::iterator AllocationPeriodsTableI;
  AllocationPeriodsTable
      m_allocationPeriodsTable; //!< Table to store allocation periods
                                //!< (CBAP/SP) related transmission parameters.
  AllocationId m_currentAllocationId; //!< The ID of the current allocation
                                      //!< served by the High MAC.
  Ptr<AllocationParameters>
      m_currentAllocation;      //!< Current allocation parameters.
  bool m_transmissionSuspended; //!< Flag to indicate that we have suspended
                                //!< transmission for the current allocation.
  bool m_restoredSuspendedTransmission; //!< Flag to indicate that we have more
                                        //!< time to transmit more packets.
  bool m_servingSls; //!< Flag to indicate that we are performing BFT.
};

} // namespace ns3

#endif /* MAC_LOW_H */
