/*
 * Copyright (c) 2009 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicola Baldo <nbaldo@cttc.es>
 */

#include "wigig-spectrum-phy-interface.h"

#include "spectrum-wigig-phy.h"

#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/net-device.h"
#include "ns3/spectrum-value.h"

NS_LOG_COMPONENT_DEFINE("WigigSpectrumPhyInterface");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(WigigSpectrumPhyInterface);

TypeId WigigSpectrumPhyInterface::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigSpectrumPhyInterface")
                          .SetParent<SpectrumPhy>()
                          .SetGroupName("Wigig");
  return tid;
}

WigigSpectrumPhyInterface::WigigSpectrumPhyInterface() {
  NS_LOG_FUNCTION(this);
}

void WigigSpectrumPhyInterface::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_spectrumWigigPhy = nullptr;
  m_netDevice = nullptr;
  m_channel = nullptr;
}

void WigigSpectrumPhyInterface::SetSpectrumWigigPhy(
    const Ptr<SpectrumWigigPhy> spectrumWigigPhy) {
  m_spectrumWigigPhy = spectrumWigigPhy;
}

Ptr<NetDevice> WigigSpectrumPhyInterface::GetDevice() const {
  return m_netDevice;
}

Ptr<MobilityModel> WigigSpectrumPhyInterface::GetMobility() const {
  return m_spectrumWigigPhy->GetMobility();
}

void WigigSpectrumPhyInterface::SetDevice(const Ptr<NetDevice> d) {
  m_netDevice = d;
}

void WigigSpectrumPhyInterface::SetMobility(const Ptr<MobilityModel> m) {
  m_spectrumWigigPhy->SetMobility(m);
}

void WigigSpectrumPhyInterface::SetChannel(const Ptr<SpectrumChannel> c) {
  NS_LOG_FUNCTION(this << c);
  m_channel = c;
}

Ptr<const SpectrumModel> WigigSpectrumPhyInterface::GetRxSpectrumModel() const {
  return m_spectrumWigigPhy->GetRxSpectrumModel();
}

Ptr<Object> WigigSpectrumPhyInterface::GetAntenna() const { return nullptr; }

void WigigSpectrumPhyInterface::StartRx(Ptr<SpectrumSignalParameters> params) {
  m_spectrumWigigPhy->StartRx(params);
}

} // namespace ns3
