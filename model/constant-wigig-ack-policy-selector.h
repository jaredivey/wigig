/*
 * Copyright (c) 2019 Universita' degli Studi di Napoli Federico II
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Stefano Avallone <stavallo@unina.it>
 */

#ifndef CONSTANT_WIGIG_ACK_POLICY_SELECTOR_H
#define CONSTANT_WIGIG_ACK_POLICY_SELECTOR_H

#include "wigig-ack-policy-selector.h"

namespace ns3 {

/**
 * \ingroup wigig
 *
 * A constant ack policy selector operating based on the values of its
 * attributes.
 */
class ConstantWigigAckPolicySelector : public WigigAckPolicySelector {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  ConstantWigigAckPolicySelector();
  ~ConstantWigigAckPolicySelector() override;

  /**
   * Get a temporary transmission parameters related to the acknowledgment
   * policy for the given PSDU. This method is typically called before the MPDU
   * aggregator when trying to aggregate another MPDU to the current A-MPDU. In
   * fact, the AckPolicySelector may switch to a different acknowledgment policy
   * when a new MPDU is aggregated to an A-MPDU.
   *
   * \param psdu the given PSDU.
   * \param params the MacLow parameters to update.
   * \return Temporary MacLow parameters to use for TxopLimit calculation.
   */
  MacLowTransmissionParameters
  GetTemporaryParams(Ptr<WigigPsdu> psdu,
                     MacLowTransmissionParameters params) override;
  /**
   * Update the transmission parameters related to the acknowledgment policy for
   * the given PSDU. This method is typically called by the MPDU aggregator when
   * trying to aggregate another MPDU to the current A-MPDU. In fact, the
   * AckPolicySelector may switch to a different acknowledgment policy when a
   * new MPDU is aggregated to an A-MPDU.
   * Note that multi-TID A-MPDUs are currently not supported by this method.
   *
   * \param psdu the given PSDU.
   * \param params the MacLow parameters to update.
   */
  void UpdateTxParams(Ptr<WigigPsdu> psdu,
                      MacLowTransmissionParameters &params) override;

private:
  bool m_useExplicitBar; //!< true for sending BARs, false for using Implicit
                         //!< BAR ack policy
  double m_baThreshold;  //!< Threshold to determine when a BlockAck must be
                         //!< requested
};

} // namespace ns3

#endif /* CONSTANT_WIGIG_ACK_POLICY_SELECTOR_H */
