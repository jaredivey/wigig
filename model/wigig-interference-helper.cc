/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 */

#include "wigig-interference-helper.h"

#include "wigig-phy.h"
#include "wigig-ppdu.h"
#include "wigig-psdu.h"

#include "ns3/error-rate-model.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/wifi-phy-common.h"
#include "ns3/wifi-utils.h"

#include <algorithm>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigInterferenceHelper");

/****************************************************************
 *       PHY event class
 ****************************************************************/

WigigSignalEvent::WigigSignalEvent(const WigigTxVector &txVector, Time duration,
                                   double rxPower)
    : m_txVector(txVector), m_startTime(Simulator::Now()),
      m_endTime(m_startTime + duration), m_rxPowerW(rxPower) {}

WigigSignalEvent::WigigSignalEvent(Ptr<const WigigPpdu> ppdu,
                                   const WigigTxVector &txVector, Time duration,
                                   double rxPower)
    : m_ppdu(ppdu), m_txVector(txVector), m_startTime(Simulator::Now()),
      m_endTime(m_startTime + duration), m_rxPowerW(rxPower) {}

WigigSignalEvent::WigigSignalEvent(Ptr<const WigigPpdu> ppdu,
                                   const WigigTxVector &txVector, Time duration,
                                   double rxPower,
                                   std::vector<double> mimoRxPower)
    : m_ppdu(ppdu), m_txVector(txVector), m_startTime(Simulator::Now()),
      m_endTime(m_startTime + duration), m_rxPowerW(rxPower),
      m_mimoRxPowerW(mimoRxPower) {}

WigigSignalEvent::~WigigSignalEvent() {}

Ptr<const WigigPsdu> WigigSignalEvent::GetPsdu() const {
  return m_ppdu->GetPsdu();
}

Ptr<const WigigPpdu> WigigSignalEvent::GetPpdu() const { return m_ppdu; }

Time WigigSignalEvent::GetStartTime() const { return m_startTime; }

Time WigigSignalEvent::GetEndTime() const { return m_endTime; }

Time WigigSignalEvent::GetDuration() const { return m_endTime - m_startTime; }

double WigigSignalEvent::GetRxPowerW() const { return m_rxPowerW; }

WigigTxVector WigigSignalEvent::GetTxVector() const { return m_txVector; }

WifiMode WigigSignalEvent::GetPayloadMode() const {
  return m_txVector.GetMode();
}

std::ostream &operator<<(std::ostream &os, const WigigSignalEvent &event) {
  os << "start=" << event.GetStartTime() << ", end=" << event.GetEndTime()
     << ", TXVECTOR=" << event.GetTxVector()
     << ", power=" << event.GetRxPowerW() << "W"
     << ", PPDU=" << event.GetPpdu();
  return os;
}

/****************************************************************
 *       Class which records SNIR change events for a
 *       short period of time.
 ****************************************************************/

WigigInterferenceHelper::NiChange::NiChange(double power,
                                            Ptr<WigigSignalEvent> event)
    : m_power(power), m_event(event) {}

double WigigInterferenceHelper::NiChange::GetPower() const { return m_power; }

void WigigInterferenceHelper::NiChange::AddPower(double power) {
  m_power += power;
}

Ptr<WigigSignalEvent> WigigInterferenceHelper::NiChange::GetEvent() const {
  return m_event;
}

/****************************************************************
 *       The actual WigigInterferenceHelper
 ****************************************************************/

WigigInterferenceHelper::WigigInterferenceHelper()
    : m_wifiPhy(nullptr), m_noiseFigure(0), m_errorRateModel(nullptr),
      m_numRxAntennas(1), m_firstPower(0), m_rxing(false) {
  // Always have a zero power noise event in the list
  AddNiChangeEvent(Time(0), NiChange(0.0, nullptr));
}

WigigInterferenceHelper::~WigigInterferenceHelper() {
  EraseEvents();
  m_errorRateModel = nullptr;
}

void WigigInterferenceHelper::SetWifiPhy(Ptr<WigigPhy> wifiPhy) {
  m_wifiPhy = wifiPhy;
}

Ptr<WigigSignalEvent>
WigigInterferenceHelper::Add(const WigigTxVector &txVector, Time duration,
                             double rxPowerW) {
  Ptr<WigigSignalEvent> event =
      Create<WigigSignalEvent>(txVector, duration, rxPowerW);
  AppendEvent(event);
  return event;
}

Ptr<WigigSignalEvent>
WigigInterferenceHelper::Add(Ptr<const WigigPpdu> ppdu,
                             const WigigTxVector &txVector, Time duration,
                             double rxPowerW) {
  Ptr<WigigSignalEvent> event =
      Create<WigigSignalEvent>(ppdu, txVector, duration, rxPowerW);
  AppendEvent(event);
  return event;
}

Ptr<WigigSignalEvent>
WigigInterferenceHelper::Add(Ptr<const WigigPpdu> ppdu,
                             const WigigTxVector &txVector, Time duration,
                             double rxPowerW, std::vector<double> mimoRxPower) {
  Ptr<WigigSignalEvent> event =
      Create<WigigSignalEvent>(ppdu, txVector, duration, rxPowerW, mimoRxPower);
  AppendEvent(event);
  return event;
}

void WigigInterferenceHelper::AddForeignSignal(Time duration, double rxPowerW) {
  // Parameters other than duration and rxPowerW are unused for this type
  // of signal, so we provide dummy versions
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_QOSDATA);
  Ptr<WigigPpdu> fakePpdu = Create<WigigPpdu>(
      Create<WigigPsdu>(Create<Packet>(0), hdr), WigigTxVector(), duration, 0);
  Add(fakePpdu, WigigTxVector(), duration, rxPowerW);
}

void WigigInterferenceHelper::SetNoiseFigure(double value) {
  m_noiseFigure = value;
}

void WigigInterferenceHelper::SetErrorRateModel(
    const Ptr<ErrorRateModel> rate) {
  m_errorRateModel = rate;
}

double WigigInterferenceHelper::GetNoiseFigure() const { return m_noiseFigure; }

Ptr<ErrorRateModel> WigigInterferenceHelper::GetErrorRateModel() const {
  return m_errorRateModel;
}

void WigigInterferenceHelper::SetNumberOfReceiveAntennas(uint8_t rx) {
  m_numRxAntennas = rx;
}

Time WigigInterferenceHelper::GetEnergyDuration(double energyW) const {
  Time now = Simulator::Now();
  auto i = GetPreviousPosition(now);
  Time end = i->first;
  for (; i != m_niChanges.end(); ++i) {
    double noiseInterferenceW = i->second.GetPower();
    end = i->first;
    if (noiseInterferenceW < energyW) {
      break;
    }
  }
  return end > now ? end - now : MicroSeconds(0);
}

void WigigInterferenceHelper::AppendEvent(Ptr<WigigSignalEvent> event) {
  NS_LOG_FUNCTION(this);
  double previousPowerStart = 0;
  double previousPowerEnd = 0;
  previousPowerStart =
      GetPreviousPosition(event->GetStartTime())->second.GetPower();
  previousPowerEnd =
      GetPreviousPosition(event->GetEndTime())->second.GetPower();

  if (!m_rxing) {
    m_firstPower = previousPowerStart;
    // Always leave the first zero power noise event in the list
    m_niChanges.erase(++(m_niChanges.begin()),
                      GetNextPosition(event->GetStartTime()));
  }
  auto first = AddNiChangeEvent(event->GetStartTime(),
                                NiChange(previousPowerStart, event));
  auto last =
      AddNiChangeEvent(event->GetEndTime(), NiChange(previousPowerEnd, event));
  for (auto i = first; i != last; ++i) {
    i->second.AddPower(event->GetRxPowerW());
  }
}

double
WigigInterferenceHelper::CalculateSnr(double signal, double noiseInterference,
                                      const WigigTxVector &txVector) const {
  // thermal noise at 290K in J/s = W
  static const double BOLTZMANN = 1.3803e-23;
  uint16_t channelWidth = txVector.GetChannelWidth();
  // Nt is the power of thermal noise in W
  double Nt = BOLTZMANN * 290 * channelWidth * 1e6;
  // receiver noise Floor (W) which accounts for thermal noise and
  // non-idealities of the receiver
  double noiseFloor = m_noiseFigure * Nt;
  double noise = noiseFloor + noiseInterference;
  double snr = signal / noise; // linear scale
  NS_LOG_DEBUG("bandwidth(MHz)=" << channelWidth << ", signal(W)= " << signal
                                 << ", noise(W)=" << noiseFloor
                                 << ", interference(W)=" << noiseInterference
                                 << ", snr=" << RatioToDb(snr) << "dB");
  double gain = 1;
  if (m_numRxAntennas > txVector.GetNss()) {
    gain = static_cast<double>(m_numRxAntennas) /
           txVector.GetNss(); // compute gain offered by diversity for AWGN
  }
  NS_LOG_DEBUG("SNR improvement thanks to diversity: " << 10 * std::log10(gain)
                                                       << "dB");
  snr *= gain;
  return snr;
}

double WigigInterferenceHelper::CalculateNoiseInterferenceW(
    Ptr<WigigSignalEvent> event, NiChanges *ni) const {
  double noiseInterferenceW = m_firstPower;
  auto it = m_niChanges.find(event->GetStartTime());
  for (; it != m_niChanges.end() && it->first < Simulator::Now(); ++it) {
    if (it->second.GetEvent()->GetEndTime() == event->GetStartTime()) {
      /* This is to handle IEEE 802.11ad AGC and TRN Subfields */
      continue;
    }
    noiseInterferenceW = it->second.GetPower() - event->GetRxPowerW();
  }
  it = m_niChanges.find(event->GetStartTime());
  for (; it != m_niChanges.end() && it->second.GetEvent() != event; ++it) {
    ;
  }
  ni->emplace(event->GetStartTime(), NiChange(0, event));
  while (++it != m_niChanges.end() && it->second.GetEvent() != event) {
    ni->insert(*it);
  }
  ni->emplace(event->GetEndTime(), NiChange(0, event));
  NS_ASSERT_MSG(noiseInterferenceW >= 0,
                "CalculateNoiseInterferenceW returns negative value "
                    << noiseInterferenceW);
  return noiseInterferenceW;
}

double WigigInterferenceHelper::CalculateChunkSuccessRate(
    double snir, Time duration, WifiMode mode,
    const WigigTxVector &txVector) const {
  if (duration.IsZero()) {
    return 1.0;
  }
  uint64_t rate = mode.GetDataRate(txVector.GetChannelWidth());
  uint64_t nbits = static_cast<uint64_t>(rate * duration.GetSeconds());
  double csr =
      m_errorRateModel->GetChunkSuccessRate(mode, txVector, snir, nbits);
  return csr;
}

double WigigInterferenceHelper::CalculatePayloadChunkSuccessRate(
    double snir, Time duration, const WigigTxVector &txVector) const {
  if (duration.IsZero()) {
    return 1.0;
  }
  WifiMode mode = txVector.GetMode();
  uint64_t rate = mode.GetDataRate(txVector);
  uint64_t nbits = static_cast<uint64_t>(rate * duration.GetSeconds());
  nbits /= txVector.GetNss(); // divide effective number of bits by NSS to
                              // achieve same chunk error rate as SISO for AWGN
  double csr =
      m_errorRateModel->GetChunkSuccessRate(mode, txVector, snir, nbits);
  return csr;
}

double WigigInterferenceHelper::CalculatePayloadPer(
    Ptr<const WigigSignalEvent> event, NiChanges *ni,
    std::pair<Time, Time> window) const {
  NS_LOG_FUNCTION(this << window.first << window.second);
  const WigigTxVector txVector = event->GetTxVector();
  double psr = 1.0; /* Packet Success Rate */
  auto j = ni->begin();
  Time previous = j->first;
  WifiMode payloadMode = event->GetTxVector().GetMode();
  Time phyHeaderStart = j->first + WigigPhy::GetPhyPreambleDuration(
                                       txVector); // PPDU start time + preamble
  Time phyPayloadStart =
      phyHeaderStart + WigigPhy::GetPhyHeaderDuration(
                           txVector); // PPDU start time + preamble + L-SIG
  Time windowStart = phyPayloadStart + window.first;
  Time windowEnd = phyPayloadStart + window.second;
  double noiseInterferenceW = m_firstPower;
  double powerW = event->GetRxPowerW();
  while (++j != ni->end()) {
    Time current = j->first;
    NS_LOG_DEBUG("previous= " << previous << ", current=" << current);
    NS_ASSERT(current >= previous);
    /* Get a vector of per stream SNRs (in the case of SISO there is only value
     * in it) and calculate the chunk success rate per stream */
    std::vector<double> snrPerStream =
        CalculatePerStreamSnr(event, noiseInterferenceW);
    // Case 1: Both previous and current point to the windowed payload
    if (previous >= windowStart) {
      for (auto snr : snrPerStream) {
        psr *= CalculatePayloadChunkSuccessRate(
            snr, Min(windowEnd, current) - previous, txVector);
      }
      NS_LOG_DEBUG(
          "Both previous and current point to the windowed payload: mode="
          << payloadMode << ", psr=" << psr);
    }
    // Case 2: previous is before windowed payload and current is in the
    // windowed payload
    else if (current >= windowStart) {
      for (auto snr : snrPerStream) {
        psr *= CalculatePayloadChunkSuccessRate(
            snr, Min(windowEnd, current) - windowStart, txVector);
      }
      NS_LOG_DEBUG("previous is before windowed payload and current is in the "
                   "windowed payload: mode="
                   << payloadMode << ", psr=" << psr);
    }
    noiseInterferenceW = j->second.GetPower() - powerW;
    previous = j->first;
    if (previous > windowEnd) {
      NS_LOG_DEBUG("Stop: new previous="
                   << previous << " after time window end=" << windowEnd);
      break;
    }
  }
  double per = 1 - psr;
  return per;
}

double WigigInterferenceHelper::CalculateNonHtPhyHeaderPer(
    Ptr<const WigigSignalEvent> event, NiChanges *ni) const {
  NS_LOG_FUNCTION(this);
  const WigigTxVector txVector = event->GetTxVector();
  double psr = 1.0; /* Packet Success Rate */
  auto j = ni->begin();
  Time previous = j->first;
  WifiMode headerMode = WigigPhy::GetPhyHeaderMode(txVector);
  Time phyHeaderStart = j->first + WigigPhy::GetPhyPreambleDuration(
                                       txVector); // PPDU start time + preamble
  Time phyPayloadStart =
      phyHeaderStart + WigigPhy::GetPhyHeaderDuration(
                           txVector); // PPDU start time + preamble + L-SIG
  double noiseInterferenceW = m_firstPower;
  double powerW = event->GetRxPowerW();
  while (++j != ni->end()) {
    Time current = j->first;
    NS_LOG_DEBUG("previous= " << previous << ", current=" << current);
    NS_ASSERT(current >= previous);
    double snr = CalculateSnr(powerW, noiseInterferenceW, txVector);
    // Case 1: previous and current after payload start
    if (previous >= phyPayloadStart) {
      psr *= 1;
      NS_LOG_DEBUG(
          "Case 1 - previous and current after payload start: nothing to do");
    }
    // Case 4: previous in L-SIG: HT GF will not reach here because it will
    // execute the previous if and exit
    else if (previous >= phyHeaderStart) {
      // Case 4a: current after payload start
      if (current >= phyPayloadStart) {
        psr *= CalculateChunkSuccessRate(snr, phyHeaderStart - previous,
                                         headerMode, txVector);
        NS_LOG_DEBUG(
            "Case 4a - previous in L-SIG and current after payload start: mode="
            << headerMode << ", psr=" << psr);
      }
      // Case 4d: current with previous in L-SIG
      else {
        psr *= CalculateChunkSuccessRate(snr, current - previous, headerMode,
                                         txVector);
        NS_LOG_DEBUG("Case 4d - current with previous in L-SIG: mode="
                     << headerMode << ", psr=" << psr);
      }
    }
    // Case 5: previous is in the preamble works for all cases
    else {
      // Case 5a: current after payload start
      if (current >= phyPayloadStart) {
        psr *= CalculateChunkSuccessRate(snr, phyPayloadStart - phyHeaderStart,
                                         headerMode, txVector);
        NS_LOG_DEBUG("Case 5aii - previous is in the preamble and current is "
                     "after payload "
                     "start: mode="
                     << headerMode << ", psr=" << psr);
      }
      // Case 5d: current is in L-SIG.
      else if (current >= phyHeaderStart) {
        psr *= CalculateChunkSuccessRate(snr, current - phyHeaderStart,
                                         headerMode, txVector);
        NS_LOG_DEBUG("Case 5d - previous is in the preamble and current is in "
                     "L-SIG: mode="
                     << headerMode << ", psr=" << psr);
      }
    }

    noiseInterferenceW = j->second.GetPower() - powerW;
    previous = j->first;
  }

  double per = 1 - psr;
  return per;
}

double WigigInterferenceHelper::CalculateHtPhyHeaderPer(
    Ptr<const WigigSignalEvent> event, NiChanges *ni) const {
  NS_LOG_FUNCTION(this);
  const WigigTxVector txVector = event->GetTxVector();
  double psr = 1.0; /* Packet Success Rate */
  auto j = ni->begin();
  Time previous = j->first;
  WifiPreamble preamble = txVector.GetPreambleType();
  Time phyHeaderStart = j->first + WigigPhy::GetPhyPreambleDuration(
                                       txVector); // PPDU start time + preamble
  Time phyPayloadStart =
      phyHeaderStart + WigigPhy::GetPhyHeaderDuration(
                           txVector); // PPDU start time + preamble + L-SIG
  while (++j != ni->end()) {
    Time current = j->first;
    NS_LOG_DEBUG("previous= " << previous << ", current=" << current);
    NS_ASSERT(current >= previous);
    // Case 1: previous and current after payload start: nothing to do
    if (previous >= phyPayloadStart) {
      psr *= 1;
      NS_LOG_DEBUG(
          "Case 1 - previous and current after payload start: nothing to do");
    }
    // Case 4: previous in L-SIG: HT GF will not reach here because it will
    // execute the previous if and exit
    else if (previous >= phyHeaderStart) {
      // Case 4a: current after payload start
      if (current >= phyPayloadStart) {
        // Case 4ai: non-HT format
        if (preamble == WIFI_PREAMBLE_LONG || preamble == WIFI_PREAMBLE_SHORT) {
          psr *= 1;
          NS_LOG_DEBUG(
              "Case 4ai - previous in L-SIG and current after payload start: "
              "nothing to do");
        }
      }
      // Case 4d: current with previous in L-SIG
      else {
        psr *= 1;
        NS_LOG_DEBUG("Case 4d - current with previous in L-SIG: nothing to do");
      }
    }
    // Case 5: previous is in the preamble works for all cases
    else {
      // Case 5a: current after payload start
      if (current >= phyPayloadStart) {
        // Case 5ai: non-HT format (No HT-SIG or Training Symbols)
        if (preamble == WIFI_PREAMBLE_LONG || preamble == WIFI_PREAMBLE_SHORT) {
          psr *= 1;
          NS_LOG_DEBUG(
              "Case 5ai - previous is in the preamble and current is after "
              "payload start: nothing to do");
        }
      }
      // Case 5d: current is in L-SIG. HT-GF will not come here
      else if (current >= phyHeaderStart) {
        psr *= 1;
        NS_LOG_DEBUG("Case 5d - previous is in the preamble and current is in "
                     "L-SIG: nothing to do");
      }
    }
    previous = j->first;
  }

  double per = 1 - psr;
  return per;
}

double WigigInterferenceHelper::CalculateDmgPhyHeaderPer(
    Ptr<const WigigSignalEvent> event, NiChanges *ni) const {
  NS_LOG_FUNCTION(this);
  const WigigTxVector txVector = event->GetTxVector();
  double psr = 1.0; /* Packet Success Rate */
  auto j = ni->begin();
  Time previous = j->first;
  WifiMode headerMode = WigigPhy::GetPhyHeaderMode(txVector);
  Time phyHeaderStart = j->first + WigigPhy::GetPhyPreambleDuration(
                                       txVector); // PPDU start time + preamble
  Time phyHeaderEnd =
      phyHeaderStart + WigigPhy::GetPhyHeaderDuration(
                           txVector); // PPDU start time + preamble + header
  double noiseInterferenceW = m_firstPower;
  double powerW = event->GetRxPowerW();
  while (++j != ni->end()) {
    Time current = j->first;
    NS_LOG_DEBUG("previous= " << previous << ", current=" << current);
    NS_ASSERT(current >= previous);
    double snr = CalculateSnr(powerW, noiseInterferenceW, txVector);
    // Case 1: previous and current after payload start
    if (previous >= phyHeaderEnd) {
      psr *= 1;
      NS_LOG_DEBUG(
          "Case 1 - previous and current after payload start: nothing to do");
    }
    // Case 2: previous in DMG PHY Header
    else if (previous >= phyHeaderStart) {
      // Case 2a: current after payload start
      if (current >= phyHeaderEnd) {
        psr *= CalculateChunkSuccessRate(snr, phyHeaderEnd - previous,
                                         headerMode, txVector);
        NS_LOG_DEBUG("Case 2a - previous in DMG PHY Header and current after "
                     "payload start: mode="
                     << headerMode << ", psr=" << psr);
      }
      // Case 2b: current with previous in DMG PHY Header
      else {
        psr *= CalculateChunkSuccessRate(snr, current - previous, headerMode,
                                         txVector);
        NS_LOG_DEBUG("Case 2b - current with previous in DMG PHY Header: mode="
                     << headerMode << ", psr=" << psr);
      }
    }
    // Case 3: previous is in the preamble works for all cases
    else {
      // Case 3a: current after payload start/PHY header end
      if (current >= phyHeaderEnd) {
        psr *= CalculateChunkSuccessRate(snr, phyHeaderEnd - phyHeaderStart,
                                         headerMode, txVector);
        NS_LOG_DEBUG("Case 3a - previous is in the preamble and current is "
                     "after payload "
                     "start: mode="
                     << headerMode << ", psr=" << psr);
      }
      // Case 3b: current is in DMG PHY Header.
      else if (current >= phyHeaderStart) {
        psr *= CalculateChunkSuccessRate(snr, current - phyHeaderStart,
                                         headerMode, txVector);
        NS_LOG_DEBUG("Case 3b - previous is in the preamble and current is in "
                     "DMG PHY Header: mode="
                     << headerMode << ", psr=" << psr);
      }
    }

    noiseInterferenceW = j->second.GetPower() - powerW;
    previous = j->first;
  }

  double per = 1 - psr;
  return per;
}

struct WigigInterferenceHelper::SnrPer
WigigInterferenceHelper::CalculatePayloadSnrPer(
    Ptr<WigigSignalEvent> event,
    std::pair<Time, Time> relativeMpduStartStop) const {
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());

  /* calculate the SNIR at the start of the MPDU (located through windowing) and
   * accumulate all SNIR changes in the SNIR vector.
   */
  double per = CalculatePayloadPer(event, &ni, relativeMpduStartStop);

  struct SnrPer snrPer;
  snrPer.snr = snr;
  snrPer.per = per;
  return snrPer;
}

double
WigigInterferenceHelper::CalculatePlcpTrnSnr(Ptr<WigigSignalEvent> event) {
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());
  return snr;
}

double WigigInterferenceHelper::CalculatePayloadSnr(
    Ptr<WigigSignalEvent> event) const {
  NS_LOG_FUNCTION(this);
  /* Calculate the SINR per stream */
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  std::vector<double> snrPerStream =
      CalculatePerStreamSnr(event, noiseInterferenceW);
  double snr;
  /* In the case of SISO simply return the SNR, in the case of MIMO return the
   * minimum SNR per stream */
  if (snrPerStream.size() == 1) {
    snr = snrPerStream.at(0);
  } else {
    snr = *(std::min_element(snrPerStream.begin(), snrPerStream.end()));
  }
  return snr;
}

std::vector<double> WigigInterferenceHelper::CalculatePerStreamSnr(
    Ptr<const WigigSignalEvent> event, double noiseInterferenceW) const {
  NS_LOG_FUNCTION(this);
  std::vector<double> perStreamSnr;
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());
  perStreamSnr.push_back(snr);
  return perStreamSnr;
}

double
WigigInterferenceHelper::CalculateSnr(Ptr<WigigSignalEvent> event) const {
  NS_LOG_FUNCTION(this);
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());
  return snr;
}

struct WigigInterferenceHelper::SnrPer
WigigInterferenceHelper::CalculateNonHtPhyHeaderSnrPer(
    Ptr<WigigSignalEvent> event) const {
  NS_LOG_FUNCTION(this);
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());

  /* calculate the SNIR at the start of the PHY header and accumulate
   * all SNIR changes in the SNIR vector.
   */
  double per = CalculateNonHtPhyHeaderPer(event, &ni);

  struct SnrPer snrPer;
  snrPer.snr = snr;
  snrPer.per = per;
  return snrPer;
}

struct WigigInterferenceHelper::SnrPer
WigigInterferenceHelper::CalculateHtPhyHeaderSnrPer(
    Ptr<WigigSignalEvent> event) const {
  NS_LOG_FUNCTION(this);
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());

  /* calculate the SNIR at the start of the PHY header and accumulate
   * all SNIR changes in the SNIR vector.
   */
  double per = CalculateHtPhyHeaderPer(event, &ni);

  struct SnrPer snrPer;
  snrPer.snr = snr;
  snrPer.per = per;
  return snrPer;
}

struct WigigInterferenceHelper::SnrPer
WigigInterferenceHelper::CalculateDmgPhyHeaderSnrPer(
    Ptr<WigigSignalEvent> event) const {
  NS_LOG_FUNCTION(this);
  NiChanges ni;
  double noiseInterferenceW = CalculateNoiseInterferenceW(event, &ni);
  double snr = CalculateSnr(event->GetRxPowerW(), noiseInterferenceW,
                            event->GetTxVector());

  /* calculate the SNIR at the start of the PHY header and accumulate
   * all SNIR changes in the SNIR vector.
   */
  double per = CalculateDmgPhyHeaderPer(event, &ni);

  struct SnrPer snrPer;
  snrPer.snr = snr;
  snrPer.per = per;
  return snrPer;
}

void WigigInterferenceHelper::EraseEvents() {
  m_niChanges.clear();
  // Always have a zero power noise event in the list
  AddNiChangeEvent(Time(0), NiChange(0.0, nullptr));
  m_rxing = false;
  m_firstPower = 0;
}

WigigInterferenceHelper::NiChanges::const_iterator
WigigInterferenceHelper::GetNextPosition(Time moment) const {
  return m_niChanges.upper_bound(moment);
}

WigigInterferenceHelper::NiChanges::const_iterator
WigigInterferenceHelper::GetPreviousPosition(Time moment) const {
  auto it = GetNextPosition(moment);
  // This is safe since there is always an NiChange at time 0,
  // before moment.
  --it;
  return it;
}

WigigInterferenceHelper::NiChanges::iterator
WigigInterferenceHelper::AddNiChangeEvent(Time moment, NiChange change) {
  return m_niChanges.insert(GetNextPosition(moment),
                            std::make_pair(moment, change));
}

void WigigInterferenceHelper::NotifyRxStart() {
  NS_LOG_FUNCTION(this);
  m_rxing = true;
}

void WigigInterferenceHelper::NotifyRxEnd() {
  NS_LOG_FUNCTION(this);
  m_rxing = false;
  // Update m_firstPower for frame capture
  auto it = GetPreviousPosition(Simulator::Now());
  it--;
  m_firstPower = it->second.GetPower();
}

} // namespace ns3
