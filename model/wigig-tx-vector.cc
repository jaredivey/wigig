/*
 * Copyright (c) 2010 CTTC
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Nicola Baldo <nbaldo@cttc.es>
 *          Ghada Badawy <gbadawy@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-tx-vector.h"

namespace ns3 {

WigigTxVector::WigigTxVector()
    : WifiTxVector(), m_traingFieldLength(0), m_beamTrackingRequest(false),
      m_lastRssi(0), m_isDMGBeacon(false) {}

WigigTxVector::WigigTxVector(WifiMode mode, uint8_t powerLevel,
                             WifiPreamble preamble, uint16_t channelWidth,
                             bool aggregation)
    : WifiTxVector(mode, powerLevel, preamble, 800, 1, 1, 0, channelWidth,
                   aggregation, false, false, 0, 0),
      m_traingFieldLength(0), m_beamTrackingRequest(false), m_lastRssi(0),
      m_isDMGBeacon(false) {}

WigigTxVector::WigigTxVector(WifiMode mode, uint8_t powerLevel,
                             WifiPreamble preamble, uint16_t guardInterval,
                             uint8_t nTx, uint8_t nss, uint8_t ness,
                             uint16_t channelWidth, bool aggregation, bool stbc,
                             uint8_t bssColor)
    : WifiTxVector(mode, powerLevel, preamble, guardInterval, nTx, nss, ness,
                   channelWidth, aggregation, stbc, false, bssColor, 0),
      m_traingFieldLength(0), m_beamTrackingRequest(false), m_lastRssi(0),
      m_isDMGBeacon(false) {}

void WigigTxVector::SetPacketType(PacketType type) { m_packetType = type; }

PacketType WigigTxVector::GetPacketType() const { return m_packetType; }

void WigigTxVector::SetTrainingFieldLength(uint8_t length) {
  m_traingFieldLength = length;
}

uint8_t WigigTxVector::GetTrainingFieldLength() const {
  return m_traingFieldLength;
}

void WigigTxVector::RequestBeamTracking() { m_beamTrackingRequest = true; }

bool WigigTxVector::IsBeamTrackingRequested() const {
  return m_beamTrackingRequest;
}

void WigigTxVector::SetLastRssi(uint8_t level) { m_lastRssi = level; }

uint8_t WigigTxVector::GetLastRssi() const { return m_lastRssi; }

void WigigTxVector::SetSender(Mac48Address sender) { m_sender = sender; }

Mac48Address WigigTxVector::GetSender() const { return m_sender; }

void WigigTxVector::SetDmgBeacon(bool beacon) { m_isDMGBeacon = beacon; }

bool WigigTxVector::IsDmgBeacon() const { return m_isDMGBeacon; }

} // namespace ns3
