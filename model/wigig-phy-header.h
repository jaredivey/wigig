/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_PHY_HEADER_H
#define WIGIG_PHY_HEADER_H

#include "wigig-tx-vector.h"

#include "ns3/header.h"
#include "ns3/wifi-standards.h"

namespace ns3 {

/**
 * \ingroup wigig
 *
 * Implements the IEEE 802.11ad DMG Control PHY header.
 * See section 20.4.3.2 in IEEE 802.11-2016.
 */
class DmgControlHeader : public Header {
public:
  DmgControlHeader();
  ~DmgControlHeader() override;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set the number of data octets in the PSDU.
   *
   * \param length the number of data octets in the PSDU.
   *        Range 14–1023 (in bytes).
   * \param isShortSsw Flag that specifies if the PSDU is a Short SSW packet.
   *        Short SSW packets are allowed to have a length of 6 bytes.
   */
  void SetLength(uint16_t length, bool isShortSsw = false);
  /**
   * Get the number of data octets in the PSDU.
   *
   * \return the number of data octets in the PSDU.
   */
  uint16_t GetLength() const;
  /**
   * — Packet Type = 0 (BRP-RX packet, see 20.10.2.2.3), indicates
   *   either a PPDU whose data part is followed by one or more
   *   TRN subfields (when the Beam Tracking Request field is 0 or
   *   in DMG control mode), or a PPDU that contains a request for
   *   TRN subfields to be appended to a future response PPDU
   *   (when the Beam Tracking Request field is 1).
   * — Packet Type = 1 (BRP-TX packet, see 20.10.2.2.3), indicates a
   *   PPDU whose data part is followed by one or more TRN sub-
   *   fields. The transmitter may change AWV at the beginning of
   *   each TRN subfield.
   * The field is reserved when the Training Length field is 0.
   *
   * \param type
   */
  void SetPacketType(PacketType type);
  /**
   * Get packet type.
   *
   * \return The type of the TRN subfields appended to the packet.
   */
  PacketType GetPacketType() const;
  /**
   * Set the length of the training field (The number of TRN units).
   *
   * \param length The length of the training field.
   */
  void SetTrainingLength(uint16_t length);
  /**
   * Get the length of the training field (The number of TRN units).
   *
   * \return the length of the training field.
   */
  uint16_t GetTrainingLength() const;

private:
  uint16_t m_length;         //!< The length of the PSDU in bytes.
  PacketType m_packetType;   //!< The type of the TRN subfields.
  uint16_t m_trainingLength; //!< The number of the TRN units.
};

/**
 * \ingroup wigig
 *
 * Implements the IEEE 802.11ad DMG OFDM PHY header.
 * See section 20.5.3.1 in IEEE 802.11-2016.
 */
class DmgOfdmHeader : public Header {
public:
  DmgOfdmHeader();
  ~DmgOfdmHeader() override;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set modulation and coding scheme based on Table 20-19 in IEEE 802.11-2016
   *
   * \param mcs The modulation and coding scheme.
   */
  void SetBaseMcs(uint8_t mcs);
  /**
   * Get modulation and coding scheme.
   *
   * \return The base modulation and coding scheme.
   */
  uint8_t GetBaseMcs() const;
  /**
   * If the Extended SC MCS Indication field is 0, indicates the number
   * of data octets in the PSDU; range 1–262 143.
   *
   * If the Extended SC MCS Indication field is 1, the Length field value
   * is set to Base_Length1 |_(Base_Length2 - N)/4_| , where N
   * is the number of data octets in the PSDU, and Base_Length1 and
   * Base_Length2 are computed according to Table 20-18. The number
   * of data octets in the PSDU shall not exceed 262 143.
   *
   * \param length the number of data octets in the PSDU.
   */
  void SetLength(uint32_t length);
  /**
   * Get the number of data octets in the PSDU.
   *
   * \return the number of data octets in the PSDU.
   */
  uint32_t GetLength() const;
  /**
   * — Packet Type = 0 (BRP-RX packet, see 20.10.2.2.3), indicates
   *   either a PPDU whose data part is followed by one or more
   *   TRN subfields (when the Beam Tracking Request field is 0 or
   *   in DMG control mode), or a PPDU that contains a request for
   *   TRN subfields to be appended to a future response PPDU
   *   (when the Beam Tracking Request field is 1).
   * — Packet Type = 1 (BRP-TX packet, see 20.10.2.2.3), indicates a
   *   PPDU whose data part is followed by one or more TRN sub-
   *   fields. The transmitter may change AWV at the beginning of
   *   each TRN subfield.
   * The field is reserved when the Training Length field is 0.
   *
   * \param type
   */
  void SetPacketType(PacketType type);
  /**
   * Get packet type.
   *
   * \return The type of the TRN subfields appended to the packet.
   */
  PacketType GetPacketType() const;
  /**
   * Set the length of the training field (The number of TRN units).
   *
   * \param length The length of the training field.
   */
  void SetTrainingLength(uint16_t length);
  /**
   * Get the length of the training field (The number of TRN units).
   *
   * \return the length of the training field.
   */
  uint16_t GetTrainingLength() const;
  /**
   * Set to 1 to indicate that the PPDU in the data portion of the packet
   * contains an A-MPDU; otherwise, set to 0.
   *
   * \param aggregation
   */
  void SetAggregation(bool aggregation);
  /**
   * Get aggregation field in DMG SC Header.
   *
   * \return True if the PPDU in the data portion of the packet contains
   * an A-MPDU; otherwise false.
   */
  bool GetAggregation() const;
  /**
   * Set to 1 to indicate the need for beam tracking (10.38.7); otherwise,
   * set to 0. The Beam Tracking Request field is reserved when the Training
   * Length field is 0.
   *
   * \param request
   */
  void SetBeamTrackingRequest(bool request);
  /**
   * Get BeamTrackingRequest field in DMG SC Header.
   *
   * \return True if beam tracking is requested; otherwise false.
   */
  bool GetBeamTrackingRequest() const;
  /**
   * Contains a copy of the parameter LAST_RSSI from the TXVECTOR.
   * The value is an unsigned integer:
   * - Values 2 to 14 represent power levels (–71 + value×2) dBm.
   * - A value of 15 represents a power greater than or equal to –42 dBm.
   * - A value of 1 represents a power less than or equal to –68 dBm.
   * - A value of 0 indicates that the previous packet was not received a SIFS
   *  before the current transmission.
   *
   * \param rssi
   */
  void SetLastRssi(uint8_t rssi);
  /**
   * Get LastRssi field  in DMG SC Header.
   *
   * \return
   */
  uint8_t GetLastRssi() const;

protected:
  uint8_t m_baseMcs;          //!< Modulation and coding scheme.
  uint32_t m_length;          //!< The length of the PSDU in bytes.
  PacketType m_packetType;    //!< The type of the TRN subfields.
  uint16_t m_trainingLength;  //!< The number of the TRN units.
  bool m_aggregation;         //!< Flag to indicate if the PSDU contains A-MPDU.
  bool m_beamTrackingRequest; //!< Flag to indicate is beam tracking is
                              //!< requested.
  uint8_t m_lastRssi;         //!< Last RSSI value.
};

/**
 * \ingroup wigig
 *
 * Implements the IEEE 802.11ad DMG SC PHY header.
 * See section 20.6.3.1 in IEEE 802.11-2016.
 */
class DmgScHeader : public DmgOfdmHeader {
public:
  DmgScHeader();
  ~DmgScHeader() override;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;
  /**
   * The Extended SC MCS Indication field combined with the Base MCS field
   * indicates the MCS.
   * The Extended SC MCS Indication field indicates whether the Length field
   * shall be calculated according to Table 20-18.
   *
   * \param extended
   */
  void SetExtendedScMcsIndication(bool extended);
  /**
   * Get Extended SC MCS Indication field in DMG SC Header.
   *
   * \return
   */
  bool GetExtendedScMcsIndication() const;

private:
  bool m_extendedScMcsIndication; //!< Flad to indicate if we are using extended
                                  //!< SC MCS values.
};

} // namespace ns3

#endif /* WIGIG_PHY_HEADER_H */
