/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "ap-wigig-mac.h"

#include "dmg-beacon-txop.h"
#include "ext-headers.h"
#include "mac-low.h"
#include "wigig-channel-access-manager.h"
#include "wigig-mac-rx-middle.h"
#include "wigig-msdu-aggregator.h"
#include "wigig-phy.h"

#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/mac-tx-middle.h"
#include "ns3/mgt-headers.h"
#include "ns3/pointer.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("ApWigigMac");

NS_OBJECT_ENSURE_REGISTERED(ApWigigMac);

TypeId ApWigigMac::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::ApWigigMac")
          .SetParent<WigigMac>()
          .SetGroupName("Wigig")
          .AddConstructor<ApWigigMac>()
          /* DMG Beacon Control Interval */
          .AddAttribute("AllowBeaconing",
                        "Allow PCP/AP to start Beaconing upon initialization.",
                        BooleanValue(true),
                        MakeBooleanAccessor(&ApWigigMac::m_allowBeaconing),
                        MakeBooleanChecker())
          .AddAttribute("BeaconInterval",
                        "The interval between two Target Beacon Transmission "
                        "Times (TBTTs).",
                        TimeValue(aMaxBIDuration),
                        MakeTimeAccessor(&ApWigigMac::GetBeaconInterval,
                                         &ApWigigMac::SetBeaconInterval),
                        MakeTimeChecker(TU, aMaxBIDuration))
          .AddAttribute(
              "BeaconJitter",
              "A uniform random variable to cause the initial DMG Beaconing "
              "starting "
              "time (after simulation time 0) "
              "to be randomly distributed with a X delay of microseconds.",
              StringValue("ns3::ConstantRandomVariable[Constant=1.0]"),
              MakePointerAccessor(&ApWigigMac::m_beaconJitter),
              MakePointerChecker<RandomVariableStream>())
          .AddAttribute("EnableBeaconJitter",
                        "If beacons are enabled, whether to jitter the initial "
                        "send event.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&ApWigigMac::m_enableBeaconJitter),
                        MakeBooleanChecker())

          .AddAttribute("EnableBeaconRandomization",
                        "Whether the DMG PCP/AP shall change the sequence of "
                        "directions through "
                        "which a DMG Beacon frame"
                        "is transmitted after it has transmitted a DMG Beacon "
                        "frame through each "
                        "direction in the"
                        "current sequence of directions.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&ApWigigMac::m_beaconRandomization),
                        MakeBooleanChecker())
          .AddAttribute("NextBeacon",
                        "The number of beacon intervals following the current "
                        "beacon interval during"
                        "which the DMG Beacon is not be present.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&ApWigigMac::m_nextBeacon),
                        MakeUintegerChecker<uint8_t>(0, 15))
          .AddAttribute("NextAbft",
                        "The number of beacon intervals during which the A-BFT "
                        "is not be present.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&ApWigigMac::GetAbftPeriodicity,
                                             &ApWigigMac::SetAbftPeriodicity),
                        MakeUintegerChecker<uint8_t>(0, 15))
          .AddAttribute("SSSlotsPerAbft",
                        "Number of Sector Sweep Slots Per A-BFT.",
                        UintegerValue(aMinSSSlotsPerAbft),
                        MakeUintegerAccessor(&ApWigigMac::m_ssSlotsPerAbft),
                        MakeUintegerChecker<uint8_t>(1, 8))
          .AddAttribute("SSFramesPerSlot",
                        "Number of SSW Frames per Sector Sweep Slot.",
                        UintegerValue(aSSFramesPerSlot),
                        MakeUintegerAccessor(&ApWigigMac::m_ssFramesPerSlot),
                        MakeUintegerChecker<uint8_t>(1, 16))
          .AddAttribute("IsResponderTxss",
                        "Indicates whether the A-BFT period is TXSS or RXSS",
                        BooleanValue(true),
                        MakeBooleanAccessor(&ApWigigMac::m_isABftResponderTxss),
                        MakeBooleanChecker())
          .AddAttribute(
              "AnnounceCapabilities",
              "Whether to include DMG Capabilities in DMG Beacons.",
              BooleanValue(true),
              MakeBooleanAccessor(&ApWigigMac::m_announceDmgCapabilities),
              MakeBooleanChecker())
          .AddAttribute(
              "OperationElement",
              "Whether to include DMG Operation Element in DMG Beacons.",
              BooleanValue(true),
              MakeBooleanAccessor(&ApWigigMac::m_announceOperationElement),
              MakeBooleanChecker())
          .AddAttribute(
              "ScheduleElement",
              "Whether to include Extended Schedule Element in DMG Beacons.",
              BooleanValue(true),
              MakeBooleanAccessor(&ApWigigMac::m_scheduleElement),
              MakeBooleanChecker())
          .AddAttribute("ATIPresent",
                        "The BI period contains ATI access period.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&ApWigigMac::m_atiPresent),
                        MakeBooleanChecker())
          .AddAttribute("ATIDuration", "The duration of the ATI Period.",
                        TimeValue(MicroSeconds(0)),
                        MakeTimeAccessor(&ApWigigMac::m_atiDuration),
                        MakeTimeChecker())
          .AddAttribute(
              "AnnounceTrainingSchedule",
              "Whether to include EDMG Training Field Schedule Element in DMG "
              "Beacons",
              BooleanValue(true),
              MakeBooleanAccessor(&ApWigigMac::m_announceTrainingSchedule),
              MakeBooleanChecker())
          /* DMG PCP/AP Clustering */
          .AddAttribute(
              "EnableDecentralizedClustering",
              "Enable/Disable decentralized clustering.", BooleanValue(false),
              MakeBooleanAccessor(&ApWigigMac::m_enableDecentralizedClustering),
              MakeBooleanChecker())
          .AddAttribute(
              "EnableCentralizedClustering",
              "Enable/Disable centralized clustering.", BooleanValue(false),
              MakeBooleanAccessor(&ApWigigMac::m_enableCentralizedClustering),
              MakeBooleanChecker())
          .AddAttribute("ClusterMaxMem",
                        "The maximum number of PCPs and/or APs, including the "
                        "S-PCP/S-AP.",
                        UintegerValue(2),
                        MakeUintegerAccessor(&ApWigigMac::m_clusterMaxMem),
                        MakeUintegerChecker<uint8_t>(2, 8))
          .AddAttribute("BeaconSPDuration",
                        "The size of a Beacon SP used for PCP/AP clustering in "
                        "microseconds.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&ApWigigMac::m_beaconSpDuration),
                        MakeUintegerChecker<uint8_t>(0, 255))
          .AddAttribute("ClusterRole", "The role of the PCP/AP in the cluster.",
                        EnumValue(NOT_PARTICIPATING),
                        MakeEnumAccessor(&ApWigigMac::m_clusterRole),
                        MakeEnumChecker(SYNC_PCP_AP, "S-PCP/S-AP",
                                        NOT_PARTICIPATING, "NotParticipating",
                                        PARTICIPATING, "Participating"))
          .AddAttribute("ChannelMonitorDuration",
                        "The amount of time to spend monitoring a channel for "
                        "activities.",
                        TimeValue(aMinChannelTime),
                        MakeTimeAccessor(&ApWigigMac::m_channelMonitorTime),
                        MakeTimeChecker())
          /* DMG Parameters */
          .AddAttribute("CBAPSource",
                        "Indicates that PCP/AP has a higher priority for "
                        "transmission in CBAP",
                        BooleanValue(false),
                        MakeBooleanAccessor(&ApWigigMac::m_isCbapSource),
                        MakeBooleanChecker())
          /* Association Information */
          .AddTraceSource("StationAssociated",
                          "A station got associated with the access point.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_assocLogger),
                          "ns3::WigigMac::AssociationTracedCallback")
          .AddTraceSource("StationDeAssociated",
                          "A station deassoicated with the access point.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_deAssocLogger),
                          "ns3::Mac48Address::TracedCallback")
          /* Beacon Interval Traces */
          .AddTraceSource("BIStarted", "A new Beacon Interval has started.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_biStarted),
                          "ns3::Mac48Address::TracedCallback")
          .AddTraceSource("BtiStarted", "A new BTI has started.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_btiStart),
                          "ns3::ApWigigMac::BtiStartedCallback")
          .AddTraceSource("AbftStarted", "A new A-BFT has started.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_abftStarted),
                          "ns3::ApWigigMac::AbftStartedCallback")
          /* DMG PCP/AP Clustering */
          .AddTraceSource("JoinedCluster", "The PCP/AP joined a cluster.",
                          MakeTraceSourceAccessor(&ApWigigMac::m_joinedCluster),
                          "ns3::ApWigigMac::JoinedClusterTracedCallback")
          /* DMG TS Traces */
          .AddTraceSource(
              "ADDTSReceived", "The PCP/AP received DMG ADDTS Request.",
              MakeTraceSourceAccessor(&ApWigigMac::m_addTsRequestReceived),
              "ns3::ApWigigMac::AddTsRequestReceivedTracedCallback");
  return tid;
}

ApWigigMac::ApWigigMac()
    : m_staList(), m_beaconEvent(), m_btiStarted(Seconds(0)),
      m_dmgBeaconDurationUs(Seconds(0)), m_nextDmgBeaconDelay(Seconds(0)),
      m_btiDuration(Seconds(0)), m_beamformingInDti(), m_firstBeacon(false),
      m_ClusterId(), m_spStatus(), m_monitoringChannel(false),
      m_beaconReceived(false), m_selectedBeaconSp(0),
      m_clusterTimeInterval(Seconds(0)), m_startedMonitoringChannel(Seconds(0)),
      m_clusterBeaconSpDuration(Seconds(0)), m_sswFbckEvent(),
      m_receivedOneSsw(false), m_abftCollision(false), m_peerAbftStation(),
      m_remainingSlots(0), m_stationBrpMap() {
  NS_LOG_FUNCTION(this);

  /* DMG Beacon DCF Manager */
  m_beaconTxop = CreateObject<DmgBeaconTxop>();
  m_beaconTxop->SetAifsn(1);
  m_beaconTxop->SetMinCw(0);
  m_beaconTxop->SetMaxCw(0);
  m_beaconTxop->SetMacLow(m_low);
  m_beaconTxop->SetChannelAccessManager(m_channelAccessManager);
  m_beaconTxop->SetTxOkNoAckCallback(
      MakeCallback(&ApWigigMac::FrameTxOk, this));
  m_beaconTxop->SetAccessGrantedCallback(
      MakeCallback(&ApWigigMac::StartBeaconHeaderInterval, this));

  // Let the lower layers know that we are acting as an AP.
  SetTypeOfStation(AP);
}

ApWigigMac::~ApWigigMac() { NS_LOG_FUNCTION(this); }

void ApWigigMac::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_beaconTxop = nullptr;
  m_beaconEvent.Cancel();
  WigigMac::DoDispose();
}

void ApWigigMac::SetAddress(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  // As an AP, our MAC address is also the BSSID. Hence we are
  // overriding this function and setting both in our parent class.
  WigigMac::SetAddress(address);
  WigigMac::SetBssid(address);
}

Time ApWigigMac::GetBeaconInterval() const { return m_beaconInterval; }

void ApWigigMac::SetAbftPeriodicity(uint8_t periodicity) {
  NS_LOG_FUNCTION(this << periodicity);
  m_abftPeriodicity = periodicity;
  m_nextAbft = m_abftPeriodicity;
}

uint8_t ApWigigMac::GetAbftPeriodicity() const { return m_abftPeriodicity; }

uint16_t ApWigigMac::GetAssociationId() { return AID_AP; }

void ApWigigMac::SetWifiRemoteStationManager(
    Ptr<WigigRemoteStationManager> stationManager) {
  NS_LOG_FUNCTION(this << stationManager);
  m_beaconTxop->SetWifiRemoteStationManager(stationManager);
  WigigMac::SetWifiRemoteStationManager(stationManager);
}

void ApWigigMac::SetLinkUpCallback(Callback<void> linkUp) {
  NS_LOG_FUNCTION(this << &linkUp);
  WigigMac::SetLinkUpCallback(linkUp);

  // The approach taken here is that, from the point of view of an AP,
  // the link is always up, so we immediately invoke the callback if
  // one is set.
  linkUp();
}

void ApWigigMac::SetBeaconInterval(Time interval) {
  NS_LOG_FUNCTION(this << interval);
  if ((interval.GetMicroSeconds() % 1024) != 0) {
    NS_LOG_WARN("beacon interval should be multiple of 1024us (802.11 time "
                "unit), see IEEE "
                "Std. 802.11-2012");
  }
  m_beaconInterval = interval;
}

void ApWigigMac::ForwardDown(Ptr<Packet> packet, Mac48Address from,
                             Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << from << to);
  // If we are not a QoS AP then we definitely want to use AC_BE to
  // transmit the packet. A TID of zero will map to AC_BE (through \c
  // QosUtilsMapTidToAc()), so we use that as our default here.
  uint8_t tid = 0;

  tid = QosUtilsGetTidForPacket(packet);
  // Any value greater than 7 is invalid and likely indicates that
  // the packet had no QoS tag, so we revert to zero, which'll
  // mean that AC_BE is used.
  if (tid > 7) {
    tid = 0;
  }

  ForwardDown(packet, from, to, tid);
}

void ApWigigMac::ForwardDown(Ptr<Packet> packet, Mac48Address from,
                             Mac48Address to, uint8_t tid) {
  NS_LOG_FUNCTION(this << packet << from << to << +tid);
  WigigMacHeader hdr;
  /* The HT Control field is not present in frames transmitted by a DMG STA. */
  hdr.SetAsDmgPpdu();
  hdr.SetType(WIFI_MAC_QOSDATA);
  hdr.SetQosTid(tid);
  hdr.SetQosNoEosp();
  hdr.SetQosAckPolicy(WigigMacHeader::NORMAL_ACK);
  hdr.SetQosNoAmsdu();
  hdr.SetQosRdGrant(m_supportRdp);

  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(from);
  hdr.SetDsFrom();
  hdr.SetDsNotTo();
  hdr.SetAsDmgPpdu();

  // Sanity check that the TID is valid
  NS_ASSERT(tid < 8);

  m_edca[QosUtilsMapTidToAc(tid)]->Queue(packet, hdr);
}

void ApWigigMac::Enqueue(Ptr<Packet> packet, Mac48Address to,
                         Mac48Address from) {
  NS_LOG_FUNCTION(this << packet << to << from);
  if (to.IsBroadcast() || m_stationManager->IsAssociated(to)) {
    ForwardDown(packet, from, to);
  }
}

void ApWigigMac::Enqueue(Ptr<Packet> packet, Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << to);
  // We're sending this packet with a from address that is our own. We
  // get that address from the lower MAC and make use of the
  // from-spoofing Enqueue() method to avoid duplicated code.
  Enqueue(packet, to, m_low->GetAddress());
}

bool ApWigigMac::SupportsSendFrom() const { return true; }

void ApWigigMac::SendProbeResp(Mac48Address to) {
  NS_LOG_FUNCTION(this << to);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_PROBE_RESPONSE);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  Ptr<Packet> packet = Create<Packet>();

  WigigMgtProbeResponseHeader probe;
  probe.SetBeaconIntervalUs(m_beaconInterval.GetMicroSeconds());

  probe.Get<Ssid>() = GetSsid();
  probe.Get<DmgCapabilities>() = GetDmgCapabilities();

  packet->AddHeader(probe);
  // The standard is not clear on the correct queue for management
  // frames if we are a QoS AP. The approach taken here is to always
  // use the DCF for these regardless of whether we have a QoS
  // association or not.
  m_txop->Queue(packet, hdr);
}

uint16_t ApWigigMac::SendAssocResp(Mac48Address to, bool success) {
  NS_LOG_FUNCTION(this << to << success);
  uint16_t aid = 0;
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ASSOCIATION_RESPONSE);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  Ptr<Packet> packet = Create<Packet>();
  WigigMgtAssocResponseHeader assoc;
  StatusCode code;
  if (success) {
    code.SetSuccess();
    aid = GetNextAssociationId();
    m_staList.insert(std::make_pair(aid, to));
    assoc.SetAssociationId(aid);
  } else {
    code.SetFailure();
  }

  assoc.SetStatusCode(code);

  /* Add DMG Capabilities to Association Response Frame */
  assoc.Get<DmgCapabilities>().emplace_back(GetDmgCapabilities());

  packet->AddHeader(assoc);

  /* For now, we assume one station that talks to the DMG AP */
  SteerAntennaToward(to);
  m_txop->Queue(packet, hdr);

  return aid;
}

DmgCapabilities ApWigigMac::GetDmgCapabilities() const {
  DmgCapabilities capabilities;
  capabilities.SetStaAddress(GetAddress());
  capabilities.SetAid(AID_AP);

  /* DMG STA Capability Information Field */
  capabilities.SetReverseDirection(m_supportRdp);
  capabilities.SetHigherLayerTimerSynchronization(false);
  capabilities.SetNumberOfRxDmgAntennas(m_codebook->GetTotalNumberOfAntennas());
  capabilities.SetNumberOfSectors(
      m_codebook->GetTotalNumberOfTransmitSectors());
  capabilities.SetRxssLength(m_codebook->GetTotalNumberOfReceiveSectors());
  capabilities.SetAmpduParameters(
      5, 0); /* Hardcoded Now (Maximum A-MPDU + No restriction) */
  capabilities.SetSupportedMcs(
      GetPhy()->GetMaxScRxMcs(), GetPhy()->GetMaxOfdmRxMcs(),
      GetPhy()->GetMaxScTxMcs(), GetPhy()->GetMaxOfdmTxMcs(),
      GetPhy()->GetSupportLpScPhy(), false);
  capabilities.SetAppduSupported(
      false); /* Currently A-PPDU Agregatio is not supported*/
  capabilities.SetAntennaPatternReciprocity(m_antennaPatternReciprocity);

  /* DMG PCP/AP Capability Information Field */
  capabilities.SetTddti(true);
  capabilities.SetPseudoStaticAllocations(true);
  capabilities.SetMaxAssociatedStaNumber(254);
  capabilities.SetPowerSource(true); /* Not battery powered */
  capabilities.SetPcpForwarding(true);
  capabilities.SetDecentralizedClustering(m_enableDecentralizedClustering);
  capabilities.SetCentralizedClustering(m_enableCentralizedClustering);

  return capabilities;
}

DmgOperationElement ApWigigMac::GetDmgOperationElement() const {
  DmgOperationElement operation;
  /* DMG Operation Information */
  operation.SetTddti(
      true); // We are able to provide time division channel access
  operation.SetPseudoStaticAllocations(true);
  operation.SetPcpHandover(m_pcpHandoverSupport);
  /* DMG BSS Parameter Configuration */
  operation.SetMinBhiDuration(GetBhiDuration().GetMicroSeconds());
  operation.SetMaxLostBeacons(10);
  return operation;
}

NextDmgAti ApWigigMac::GetNextDmgAtiElement() const {
  NextDmgAti ati;
  Time atiStart = m_btiDuration + GetMbifs() + m_abftDuration;
  ati.SetStartTime(atiStart.GetMicroSeconds());
  ati.SetAtiDuration(m_atiDuration.GetMicroSeconds());
  return ati;
}

ExtendedScheduleElement ApWigigMac::GetExtendedScheduleElement() const {
  ExtendedScheduleElement scheduleElement;
  scheduleElement.SetAllocationFieldList(m_allocationList);
  return scheduleElement;
}

void ApWigigMac::CleanupAllocations() {
  NS_LOG_FUNCTION(this);
  AllocationField allocation;
  for (AllocationFieldList::iterator iter = m_allocationList.begin();
       iter != m_allocationList.end();) {
    allocation = (*iter);
    if (!allocation.IsPseudoStatic() && iter->IsAllocationAnnounced()) {
      iter = m_allocationList.erase(iter);
    } else {
      ++iter;
    }
  }
}

uint32_t ApWigigMac::AllocateSingleContiguousBlock(
    AllocationId allocationId, AllocationType allocationType,
    bool staticAllocation, uint8_t sourceAid, uint8_t destAid,
    uint32_t allocationStart, uint16_t blockDuration) {
  NS_LOG_FUNCTION(this);
  return AddAllocationPeriod(allocationId, allocationType, staticAllocation,
                             sourceAid, destAid, allocationStart, blockDuration,
                             0, 1);
}

void ApWigigMac::AllocateDtiAsServicePeriod(AllocationId id, uint8_t srcAid,
                                            uint8_t dstAid) {
  NS_LOG_FUNCTION(this << +id << +srcAid << +dstAid);
  uint16_t spDuration =
      floor(GetDtiDuration().GetMicroSeconds() / MAX_NUM_BLOCKS);
  AddAllocationPeriod(id, SERVICE_PERIOD_ALLOCATION, true, srcAid, dstAid, 0,
                      spDuration, 0, MAX_NUM_BLOCKS);
}

uint32_t ApWigigMac::AddAllocationPeriod(
    AllocationId id, AllocationType allocationType, bool staticAllocation,
    uint8_t sourceAid, uint8_t destAid, uint32_t allocationStart,
    uint16_t blockDuration, uint16_t blockPeriod, uint8_t blocks) {
  NS_LOG_FUNCTION(this << +id << allocationType << staticAllocation
                       << +sourceAid << +destAid << allocationStart
                       << blockDuration << blockPeriod << +blocks);
  AllocationField field;
  /* Allocation Control Field */
  field.SetAllocationId(id);
  field.SetAllocationType(allocationType);
  field.SetAsPseudoStatic(staticAllocation);
  /* Allocation Field */
  field.SetSourceAid(sourceAid);
  field.SetDestinationAid(destAid);
  field.SetAllocationStart(allocationStart);
  field.SetAllocationBlockDuration(blockDuration);
  field.SetAllocationBlockPeriod(blockPeriod);
  field.SetNumberOfBlocks(blocks);
  /**
   * When scheduling two adjacent SPs, the PCP/AP should allocate the SPs
   * separated by at least aDmgPPMinListeningTime if one or more of the source
   * or destination DMG STAs participate in both SPs.
   */
  m_allocationList.push_back(field);

  return (allocationStart + blockDuration);
}

uint32_t ApWigigMac::AllocateBeamformingServicePeriod(uint8_t sourceAid,
                                                      uint8_t destAid,
                                                      uint32_t allocationStart,
                                                      bool isTXSS) {
  return AllocateBeamformingServicePeriod(sourceAid, destAid, allocationStart,
                                          2000, isTXSS, isTXSS);
}

uint32_t ApWigigMac::AllocateBeamformingServicePeriod(
    uint8_t sourceAid, uint8_t destAid, uint32_t allocationStart,
    uint16_t allocationDuration, bool isInitiatorTXSS, bool isResponderTxss) {
  NS_LOG_FUNCTION(this << +sourceAid << +destAid << allocationStart
                       << allocationDuration << isInitiatorTXSS
                       << isResponderTxss);
  AllocationField field;
  /* Allocation Control Field */
  field.SetAllocationType(SERVICE_PERIOD_ALLOCATION);
  field.SetAsPseudoStatic(false);
  /* Allocation Field */
  field.SetSourceAid(sourceAid);
  field.SetDestinationAid(destAid);
  field.SetAllocationStart(allocationStart);
  field.SetAllocationBlockDuration(allocationDuration); // Microseconds
  field.SetNumberOfBlocks(1);

  BfControlField bfField;
  bfField.SetBeamformTraining(true);
  bfField.SetAsInitiatorTXSS(isInitiatorTXSS);
  bfField.SetAsResponderTxss(isResponderTxss);

  field.SetBfControl(bfField);
  m_allocationList.push_back(field);

  return (allocationStart + allocationDuration +
          1000); // 1000 = 1 us protection period
}

AllocationFieldList ApWigigMac::GetAllocationList() const {
  return m_allocationList;
}

void ApWigigMac::CalculateBtiVariables() {
  /* Make DMG Beacon Template with minimum settings to calculate its duration */
  Ptr<Packet> packet = Create<Packet>();
  ExtDmgBeacon beacon;
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_EXTENSION_DMG_BEACON);

  /* Service Set Identifier Information Element */
  // WifiMac::AddInformationElement (packet, GetSsid ());
  beacon.Get<Ssid>() = GetSsid();

  if (m_announceDmgCapabilities) {
    // WifiMac::AddInformationElement (packet, DmgCapabilities ());
    beacon.Get<DmgCapabilities>() = DmgCapabilities();
  }

  if (m_announceOperationElement) {
    // WifiMac::AddInformationElement (packet, DmgOperationElement ());
    beacon.Get<DmgOperationElement>() = DmgOperationElement();
  }

  if (m_atiPresent) {
    // WifiMac::AddInformationElement (packet, NextDmgAti ());
    beacon.Get<NextDmgAti>() = NextDmgAti();
  }

  if (m_redsActivated || m_rdsActivated) {
    // WifiMac::AddInformationElement (packet, RelayCapabilitiesElement ());
    beacon.Get<RelayCapabilitiesElement>() = RelayCapabilitiesElement();
  }

  if (m_scheduleElement) {
    // WifiMac::AddInformationElement (packet, GetExtendedScheduleElement ());
    beacon.Get<ExtendedScheduleElement>() = GetExtendedScheduleElement();
  }

  packet->AddHeader(beacon);

  /* Calculate durations */
  Time dmgBeaconDuration = m_phy->CalculateTxDuration(
      packet->GetSize() + hdr.GetSize() +
          4, // We can change it to WIFI_MAC_FCS_LENGTH
      m_stationManager->GetDmgControlTxVector(), m_phy->GetFrequency());
  m_dmgBeaconDurationUs = MicroSeconds(
      ceil(static_cast<double>(dmgBeaconDuration.GetNanoSeconds()) / 1000));
  m_nextDmgBeaconDelay = m_dmgBeaconDurationUs - dmgBeaconDuration;
  /* Calculate Beacon Transmission Interval Length */
  m_btiDuration =
      m_dmgBeaconDurationUs * m_codebook->GetNumberOfSectorsInBHI() +
      GetSbifs() * (m_codebook->GetNumberOfSectorsInBHI() - 1);
}

void ApWigigMac::SendOneDMGBeacon() {
  NS_LOG_FUNCTION(this);
  // If we are not sending the first DMG Beacon, switch the sector.
  if (m_firstBeacon) {
    m_firstBeacon = false;
  } else {
    m_codebook->GetNextSectorInBti();
  }
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_EXTENSION_DMG_BEACON);
  hdr.SetAddr1(GetBssid()); /* BSSID */
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetNoMoreFragments();
  hdr.SetNoRetry();

  Ptr<Packet> packet = Create<Packet>();

  ExtDmgBeacon beacon;

  /* Timestamp */
  /**
   * A STA sending a DMG Beacon or an Announce frame shall set the value of the
   * frame’s timestamp field to equal the value of the STA’s TSF timer at the
   * time that the transmission of the data symbol containing the first bit of
   * the MPDU is started on the air (which can be derived from the
   * PHY-TXPLCPEND.indication primitive), including any transmitting STA’s
   * delays through its local PHY from the MAC-PHY interface to its interface
   * with the WM.
   */
  beacon.SetTimestamp(m_biStartTime.GetMicroSeconds());

  /* Sector Sweep Field */
  DmgSswField ssw;
  ssw.SetDirection(BeamformingInitiator);
  ssw.SetCountDown(m_codebook->GetRemainingSectorCount());
  ssw.SetSectorId(m_codebook->GetActiveTxSectorId());
  ssw.SetDmgAntennaId(m_codebook->GetActiveAntennaId());
  beacon.SetSswField(ssw);

  /* Beacon Interval */
  beacon.SetBeaconIntervalUs(m_beaconInterval.GetMicroSeconds());

  /* Beacon Interval Control Field */
  ExtDmgBeaconIntervalCtrlField ctrl;
  ctrl.SetCcPresent(m_enableCentralizedClustering ||
                    m_enableDecentralizedClustering);
  ctrl.SetDiscoveryMode(
      false); /* Discovery Mode = 0 when transmitted by PCP/AP */
  ctrl.SetNextBeacon(m_nextBeacon);
  /* Signal the presence of an ATI interval */
  m_isCbapOnly = (m_allocationList.empty());
  ctrl.SetAtiPresent(m_atiPresent);
  ctrl.SetAbftLength(m_ssSlotsPerAbft); /* Length of the following A-BFT*/
  ctrl.SetFss(m_ssFramesPerSlot);
  if (m_nextAbft == 0) {
    ctrl.SetIsResponderTxss(m_isABftResponderTxss);
  }
  ctrl.SetNextAbft(m_nextAbft);
  ctrl.SetFragmentedTxss(false); /* To fragment the initiator TXSS over multiple
                                    consecutive BTIs, do not support */
  ctrl.SetTxssSpan(m_codebook->GetNumberOfBIs());
  ctrl.SetNBi(1);
  ctrl.SetAbftCount(10);
  ctrl.SetNAbftAnt(0);
  ctrl.SetPcpAssociationReady(false);

  beacon.SetBeaconIntervalControlField(ctrl);

  /* DMG Parameters*/
  ExtDmgParameters parameters;
  parameters.SetBssType(
      InfrastructureBSS); // An AP sets the BSS Type subfield to 3 within
                          // transmitted DMG Beacon,
  parameters.SetCbapOnly(m_isCbapOnly);
  parameters.SetCbapSource(m_isCbapSource);
  parameters.SetDmgPrivacy(false);
  parameters.SetEcpacPolicyEnforced(false); // Decentralized clustering
  beacon.SetDmgParameters(parameters);

  /* Cluster Control Field */
  if (ctrl.IsCcPresent()) {
    ExtDmgClusteringControlField cluster;
    cluster.SetDiscoveryMode(ctrl.IsDiscoveryMode());
    cluster.SetBeaconSpDuration(m_beaconSpDuration);
    NS_ASSERT_MSG(
        m_beaconInterval.GetMicroSeconds() % m_clusterMaxMem == 0,
        "ClusterMaxMem subfield shall be chosen to keep the result of (beacon "
        "interval length/ClusterMaxMem) as "
        "an integer number of microseconds.");
    cluster.SetClusterMaxMem(m_clusterMaxMem);
    cluster.SetClusterMemberRole(m_clusterRole);
    if (m_clusterRole == SYNC_PCP_AP) {
      m_ClusterId = GetAddress();
    }
    cluster.SetClusterId(m_ClusterId);
    beacon.SetClusterControlField(cluster);
  }

  beacon.Get<Ssid>() = GetSsid();
  /* Service Set Identifier Information Element */
  // WifiMac::AddInformationElement (packet, GetSsid ());

  /* DMG Capabilities Information Element */
  if (m_announceDmgCapabilities) {
    beacon.Get<DmgCapabilities>() = GetDmgCapabilities();
    // WifiMac::AddInformationElement (packet, GetDmgCapabilities ());
  }

  /* DMG Operation Element */
  if (m_announceOperationElement) {
    beacon.Get<DmgOperationElement>() = GetDmgOperationElement();
    // WifiMac::AddInformationElement (packet, GetDmgOperationElement ());
  }

  /* Next DMG ATI Information Element */
  if (m_atiPresent) {
    beacon.Get<NextDmgAti>() = GetNextDmgAtiElement();
    // WifiMac::AddInformationElement (packet, GetNextDmgAtiElement ());
  }

  /* Add Relay Capability Element */
  if (m_redsActivated || m_rdsActivated) {
    beacon.Get<RelayCapabilitiesElement>() = GetRelayCapabilitiesElement();
    // WifiMac::AddInformationElement (packet, GetRelayCapabilitiesElement ());
  }

  /* Extended Schedule Element */
  if (m_scheduleElement) {
    beacon.Get<ExtendedScheduleElement>() = GetExtendedScheduleElement();
    // WifiMac::AddInformationElement (packet, GetExtendedScheduleElement ());
  }

  packet->AddHeader(beacon);

  Time btiRemaining = GetBtiRemainingTime();
  NS_LOG_DEBUG("Bti Remaining Time=" << btiRemaining);
  NS_ASSERT_MSG(btiRemaining.IsStrictlyPositive(),
                "Remaining BTI Period should not be negative.");

  /* The DMG beacon has it's own special queue, so we load it in there */
  m_beaconTxop->TransmitDmgBeacon(packet, hdr,
                                  btiRemaining - m_dmgBeaconDurationUs);
}

void ApWigigMac::SendDmgAddTsResponse(Mac48Address to, StatusCode code,
                                      TsDelayElement &delayElem,
                                      DmgTspecElement &elem) {
  NS_LOG_FUNCTION(this << to << code);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  DmgAddTsResponseFrame frame;
  frame.SetStatusCode(code);
  frame.SetTsDelay(delayElem);
  frame.SetDmgTspecElement(elem);

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.qos = WifiActionHeader::ADDTS_RESPONSE;
  actionHdr.SetAction(WifiActionHeader::QOS, action);

  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(frame);
  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

uint8_t ApWigigMac::GetStationAid(Mac48Address address) const {
  const auto it = m_macMap.find(address);
  if (it != m_macMap.cend()) {
    return it->second;
  } else {
    return -1;
  }
}

Mac48Address ApWigigMac::GetStationAddress(uint8_t aid) const {
  const auto it = m_aidMap.find(aid);
  if (it != m_aidMap.cend()) {
    return it->second;
  } else {
    return Mac48Address();
  }
}

Time ApWigigMac::GetBhiDuration() const {
  return m_btiDuration + m_abftDuration + m_atiDuration + 2 * GetMbifs();
}

Time ApWigigMac::GetDtiDuration() const {
  return m_beaconInterval - GetBhiDuration();
}

Time ApWigigMac::GetDtiRemainingTime() const {
  return GetDtiDuration() - (Simulator::Now() - m_dtiStartTime);
}

Time ApWigigMac::GetBtiRemainingTime() const {
  return m_btiDuration - (Simulator::Now() - m_btiStarted);
}

void ApWigigMac::FrameTxOk(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this << hdr.GetType());
  if (hdr.IsDmgBeacon()) {
    /* Check whether we start a new access phase or schedule a new DMG Beacon */
    if (m_codebook->GetRemainingSectorCount() != 0) {
      /* The DMG PCP/AP shall not change DMG Antennas within a BTI */
      m_beaconEvent = Simulator::Schedule(m_nextDmgBeaconDelay + GetSbifs(),
                                          &ApWigigMac::SendOneDMGBeacon, this);
    } else {
      NS_LOG_DEBUG(
          "DMG PCP/AP completed the transmission of the last DMG Beacon at "
          << Simulator::Now());
      Time startTime = m_nextDmgBeaconDelay + GetMbifs();
      /* Schedule A-BFT access period */
      if (m_nextAbft != 0) {
        /* Following the end of a BTI, the PCP/AP shall decrement the value of
         * the Next A-BFT field by one provided it is not equal to zero and
         * shall announce this value in the next BTI.*/
        m_nextAbft--;
        if (m_atiPresent) {
          NS_LOG_DEBUG("Next A-BFT= " << +m_nextAbft << " schedule ATI at "
                                      << Simulator::Now() + startTime);
          Simulator::Schedule(
              startTime, &ApWigigMac::StartAnnouncementTransmissionInterval,
              this);
        } else {
          NS_LOG_DEBUG("Next A-BFT= " << +m_nextAbft << " schedule DTI at "
                                      << Simulator::Now() + startTime);
          Simulator::Schedule(startTime,
                              &ApWigigMac::StartDataTransmissionInterval, this);
        }
      } else {
        /* The PCP/AP may increase the Next A-BFT field value following
         *  a BTI in which the Next A-BFT field was equal to zero. */
        m_nextAbft = m_abftPeriodicity;
        NS_LOG_DEBUG("Next A-BFT= " << +m_nextAbft << " schedule A-BFT at "
                                    << Simulator::Now() + startTime);

        /* The PCP/AP shall allocate an A-BFT period MBIFS time following the
         * end of a BTI that included a DMG Beacon frame transmission with Next
         * A-BFT equal to 0.*/
        Simulator::Schedule(
            startTime, &ApWigigMac::StartAssociationBeamformTraining, this);
      }
    }
  } else if (hdr.IsSsw()) {
    bool changeAntenna;
    if (m_codebook->GetNextSector(changeAntenna)) {
      /* Check if we change antenna so we use different spacing value */
      Time spacing;
      if (changeAntenna) {
        spacing = m_lbifs;
      } else {
        spacing = m_sbifs;
      }

      if (m_accessPeriod == CHANNEL_ACCESS_DTI) {
        /* We are performing BF during DTI period */
        if (m_isBeamformingInitiator) {
          Simulator::Schedule(
              spacing, &ApWigigMac::SendInitiatorTransmitSectorSweepFrame, this,
              hdr.GetAddr1());
        } else {
          Simulator::Schedule(
              spacing, &ApWigigMac::SendRespodnerTransmitSectorSweepFrame, this,
              hdr.GetAddr1());
        }
      }
    } else {
      if (m_isBeamformingInitiator) {
        if (m_isResponderTxss) {
          m_codebook->SetReceivingInQuasiOmniMode();
        } else {
          /* I-RXSS so initiator switches between different receiving sectors */
          m_codebook->StartSectorSweeping(hdr.GetAddr1(), ReceiveSectorSweep,
                                          1);
        }
        /* Start Timeout value: The initiator may restart the ISS up to
         * dot11BFRetryLimit times if it does not receive an SSW frame from the
         * responder in dot11BFTXSSTime time following the end of the ISS. */
        m_restartISSEvent = Simulator::Schedule(
            MilliSeconds(dot11BFTXSSTime),
            &ApWigigMac::RestartInitiatorSectorSweep, this, hdr.GetAddr1());
      } else {
        SteerAntennaToward(hdr.GetAddr1());
        /* Start Timeout value: The responder goes to Idle state if it does not
         * receive SSW-FBCK from the initiator in dot11MaxBFTime time following
         * the end of the RSS.
         */
        m_sswFbckTimeout =
            Simulator::Schedule(dot11MaxBFTime * m_beaconInterval,
                                &ApWigigMac::ResetSlsResponderVariables, this);
      }
    }
  } else if (hdr.IsSswFbck()) {
    if (m_accessPeriod == CHANNEL_ACCESS_ABFT) {
      ANTENNA_CONFIGURATION antennaConfig;
      Mac48Address address = hdr.GetAddr1();
      if (m_receivedOneSsw) {
        BEST_ANTENNA_CONFIGURATION info = m_bestAntennaConfig[address];
        antennaConfig = std::get<0>(info);
      } else {
        antennaConfig.first = NO_ANTENNA_CONFIG;
        antennaConfig.second = NO_ANTENNA_CONFIG;
      }

      /* We add the station to the list of the stations we can directly
       * communicate with */
      AddForwardingEntry(hdr.GetAddr1());
      m_slsInitiatorStateMachine = SLS_INITIATOR_TXSS_PHASE_COMPELTED;

      /* Raise an event that we selected the best Tx sector to the DMG STA (in
       * BHI only STA chooses) */
      m_slsCompleted(SlsCompletionAttrbitutes(
          address, CHANNEL_ACCESS_BHI, BeamformingInitiator, m_isInitiatorTxss,
          m_isResponderTxss, m_bftIdMap[address], antennaConfig.first,
          antennaConfig.second, m_maxSnr));
    } else {
      Time sswAckTimeout = SSW_ACK_TIMEOUT;
      /* Schedule event for not receiving SSW-ACK, so we restart SSW Feedback
       * process again */
      NS_LOG_INFO("Schedule SSW-ACK Timeout at "
                  << Simulator::Now() + sswAckTimeout);
      m_slsInitiatorStateMachine = SLS_INITIATOR_SSW_ACK;
      m_sswAckTimeoutEvent = Simulator::Schedule(
          sswAckTimeout, &ApWigigMac::ResendSswFbckFrame, this);
    }
  } else if (hdr.IsSswAck()) {
    /* We are SLS Responder, raise callback for SLS phase completion. */
    Mac48Address address = hdr.GetAddr1();
    BEST_ANTENNA_CONFIGURATION info = m_bestAntennaConfig[address];
    ANTENNA_CONFIGURATION antennaConfig;
    if (m_isResponderTxss) {
      antennaConfig = std::get<0>(info);
    } else if (!m_isInitiatorTxss) {
      antennaConfig = std::get<1>(info);
    }

    /* Note: According to the standard, the Responder transits to the TXSS Phase
       Complete when we receive non SSW frame and non-SSW-FBCK frame */
    m_dmgSlsTxop->SlsBftCompleted();
    m_performingBft = false;
    m_slsResponderStateMachine = SLS_RESPONDER_TXSS_PHASE_COMPELTED;
    m_slsCompleted(SlsCompletionAttrbitutes(
        address, CHANNEL_ACCESS_DTI, BeamformingResponder, m_isInitiatorTxss,
        m_isResponderTxss, m_bftIdMap[address], antennaConfig.first,
        antennaConfig.second, m_maxSnr));
    /* Resume data transmission after SLS operation */
    if (m_currentAllocation == CBAP_ALLOCATION) {
      m_txop->ResumeTxopTransmission();
      for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
        i->second->ResumeTxopTransmission();
      }
    }
  } else {
    WigigMac::FrameTxOk(hdr);
  }
}

void ApWigigMac::StartBeaconInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting BI at " << Simulator::Now());

  /* Invoke callback */
  m_biStarted(GetAddress());

  /* Timing variables */
  m_biStartTime = Simulator::Now();

  /* Schedule the end of the Beacon Interval */
  Simulator::Schedule(m_beaconInterval, &ApWigigMac::EndBeaconInterval, this);
  NS_LOG_DEBUG("Next BI will start at " << Simulator::Now() + m_beaconInterval);

  NS_LOG_DEBUG("Performing CCA before starting BHI access period.");
  m_beaconTxop->PerformCca();
}

void ApWigigMac::EndBeaconInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Ending BI at " << Simulator::Now());
  /* Cleanup non-static allocations */
  CleanupAllocations();
  /** Reset SLS state machines **/
  /* Cancel any ISS Retry event in the BI to avoid performing SLS in case no DMG
   * beacon is received in the next BI */
  if (m_restartISSEvent.IsRunning()) {
    m_restartISSEvent.Cancel();
  }

  /* Reset State at MacLow */
  m_low->SlsPhaseEnded();

  /* Start New Beacon Interval */
  Simulator::ScheduleNow(&ApWigigMac::StartBeaconInterval, this);
}

void ApWigigMac::StartBeaconHeaderInterval() {
  NS_LOG_FUNCTION(this);
  /* Make sure we do not overlap with static-SPs or shift until the end of
   * Beacon Interval */
  if (Simulator::Now() + m_btiDuration + m_atiDuration + m_abftDuration >
      m_beaconInterval + m_biStartTime) {
    NS_LOG_DEBUG("Medium is very busy we could not start BHI and we are "
                 "exceeding BI Boundary");
    return;
  }

  /* Schedule the first Access Period in the current Beacon Interval */
  if (m_btiPeriodicity == 0) {
    m_btiPeriodicity = m_nextBeacon;
    StartBeaconTransmissionInterval();
  } else {
    /* We will not have BTI access period during this BI */
    m_btiPeriodicity--;
    if (m_atiPresent) {
      StartAnnouncementTransmissionInterval();
      NS_LOG_DEBUG("ATI for Station:" << GetAddress() << " is scheduled at "
                                      << Simulator::Now());
    } else {
      StartDataTransmissionInterval();
      NS_LOG_DEBUG("DTI for Station:" << GetAddress() << " is scheduled at "
                                      << Simulator::Now());
    }
  }
}

void ApWigigMac::StartBeaconTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting BTI at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_BTI;

  /* Re-initialize variables */
  m_sectorFeedbackScheduled = false;

  /* Start DMG Beaconing */
  m_codebook->StartBtiAccessPeriod();
  m_firstBeacon = true;

  /* Timing variables */
  CalculateBtiVariables();
  m_btiStart(GetAddress(), m_btiDuration);
  m_btiStarted = Simulator::Now();
  m_beaconEvent = Simulator::ScheduleNow(&ApWigigMac::SendOneDMGBeacon, this);
}

void ApWigigMac::StartAssociationBeamformTraining() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting A-BFT at " << Simulator::Now());
  m_abftStarted(GetAddress(), m_abftDuration);
  m_accessPeriod = CHANNEL_ACCESS_ABFT;
  /* Schedule next period */
  if (m_atiPresent) {
    Simulator::Schedule(m_abftDuration + m_mbifs,
                        &ApWigigMac::StartAnnouncementTransmissionInterval,
                        this);
  } else {
    Simulator::Schedule(m_abftDuration + m_mbifs,
                        &ApWigigMac::StartDataTransmissionInterval, this);
  }
  /* Reinitialize variables */
  m_isBeamformingInitiator = true;
  m_isInitiatorTxss = true; /* DMG-AP always performs TXSS in BTI */
  m_isResponderTxss = m_isABftResponderTxss;

  // m_codebook->SetReceivingInQuasiOmniMode ();
  /* Check if there is an ongoing reception */
  Time endRx = m_phy->GetDelayUntilEndRx();
  /* Check the type of the RSS in A-BFT */
  if (m_isResponderTxss) {
    if (endRx == NanoSeconds(0)) {
      /* If there is not an ongoing reception switch to quasi-omni mode */
      m_codebook->SetReceivingInQuasiOmniMode();
    } else {
      /* If there is an ongoing reception wait until the current reception is
       * finished before switching the antenna configuration */
      void (Codebook::*sn)() = &Codebook::SetReceivingInQuasiOmniMode;
      Simulator::Schedule(endRx, sn, m_codebook);
    }
  } else {
    if (endRx == NanoSeconds(0)) {
      /* If there is not an ongoing reception set the antenna to directional
       * receiving mode */
      m_codebook->SetReceivingInDirectionalMode();
    } else {
      /* If there is wait until the current reception is finished before
       * switching the antenna configuration */
      Simulator::Schedule(endRx, &Codebook::SetReceivingInDirectionalMode,
                          m_codebook);
    }
  }
  /* Schedule the beginning of the first A-BFT Slot */
  m_remainingSlots = m_ssSlotsPerAbft;
  Simulator::ScheduleNow(&ApWigigMac::StartSectorSweepSlot, this);
}

void ApWigigMac::StartSectorSweepSlot() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting A-BFT SSW Slot ["
              << m_ssSlotsPerAbft - m_remainingSlots << "] at "
              << Simulator::Now());
  m_receivedOneSsw = false;
  m_remainingSlots--;
  /* Schedule the beginning of the next A-BFT Slot */
  if (m_remainingSlots > 0) {
    Simulator::Schedule(GetSectorSweepSlotTime(m_ssFramesPerSlot),
                        &ApWigigMac::StartSectorSweepSlot, this);
  }
}

/**
 * During the ATI STAs shall not transmit frames that are not request or
 * response frames. Request and response frames transmitted during the ATI shall
 * be one of the following:
 * 1. A frame of type Management
 * 2. An ACK frame
 * 3. A Grant, Poll, RTS or DMG CTS frame when transmitted as a request frame
 * 4. An SPR or DMG CTS frame when transmitted as a response frame
 * 5. A frame of type Data only as part of an authentication exchange to reach a
 * RSNA security association
 * 6. The Announce frame is designed to be used primarily during the ATI and can
 * perform functions of a DMG Beacon frame.
 */
void ApWigigMac::StartAnnouncementTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting ATI at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_ATI;
  /* Schedule DTI Period Starting Time */
  Simulator::Schedule(m_atiDuration, &ApWigigMac::StartDataTransmissionInterval,
                      this);
  /* Initiate BRP Setup Subphase, currently ATI is used for BRP Setup + Training
   */
  m_dmgAtiTxop->InitiateTransmission(m_atiDuration);
  /* Check if there is currently an ongoing reception at the PHY layer. */
  Time endRx = m_phy->GetDelayUntilEndRx();
  if (endRx == NanoSeconds(0)) {
    /* If there is not start BRP setup subphase  */
    DoBrpSetupSubphase();
  } else {
    /* If there is wait until the current reception is finished before starting
     * BRP setup subphase */
    Simulator::Schedule(endRx, &ApWigigMac::DoBrpSetupSubphase, this);
  }
}

void ApWigigMac::BrpSetupCompleted(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  /* Initiate BRP Transaction (We do Receive Sector Training using BRP
   * Transactions) */
  InitiateBrpTransaction(address, m_codebook->GetTotalNumberOfReceiveSectors(),
                         false);
}

void ApWigigMac::DoBrpSetupSubphase() {
  NS_LOG_FUNCTION(this);
  for (auto iter = m_stationBrpMap.begin(); iter != m_stationBrpMap.end();
       iter++) {
    if (iter->second) {
      /* Request for receive beam training with each sation */
      InitiateBrpSetupSubphase(BRP_TRN_R, iter->first);
      iter->second = false;
      return;
    }
  }
}

void ApWigigMac::NotifyBrpPhaseCompleted() {
  NS_LOG_FUNCTION(this);
  DoBrpSetupSubphase();
}

void ApWigigMac::StartDataTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_LOG_INFO("DMG AP Starting DTI at " << Simulator::Now());
  m_accessPeriod = CHANNEL_ACCESS_DTI;

  /* Schedule the beginning of next BHI interval */
  m_dtiStartTime = Simulator::Now();
  m_dtiDuration = m_beaconInterval - (Simulator::Now() - m_biStartTime);

  /* Start CBAPs and SPs */
  if (m_isCbapOnly) {
    /* Check if there is currently an ongoing reception at the PHY layer. */
    Time endRx = m_phy->GetDelayUntilEndRx();
    if (endRx == NanoSeconds(0)) {
      /* If there is not set the antenna in quasi-omni mode.  */
      m_codebook->SetReceivingInQuasiOmniMode();
    } else {
      /* If there is wait until the current reception is finished before setting
       * the quasi-omni mode. */
      void (Codebook::*sn)() = &Codebook::SetReceivingInQuasiOmniMode;
      Simulator::Schedule(endRx, sn, m_codebook);
    }

    NS_LOG_INFO("CBAP allocation only in DTI");
    if (endRx == NanoSeconds(0)) {
      /* If there is not start the contention period.  */
      Simulator::ScheduleNow(&ApWigigMac::StartContentionPeriod, this,
                             BROADCAST_CBAP, m_dtiDuration);
    } else {
      /* If there is wait until the current reception is finished before
       * starting the contention period. */
      Simulator::Schedule(endRx, &ApWigigMac::StartContentionPeriod, this,
                          BROADCAST_CBAP, m_dtiDuration - endRx);
    }
  } else {
    /* Check if there is currently a reception on the PHY layer */
    Time endRx = m_phy->GetDelayUntilEndRx();
    AllocationField field;
    for (AllocationFieldList::iterator iter = m_allocationList.begin();
         iter != m_allocationList.end(); iter++) {
      (*iter).SetAllocationAnnounced();
      field = (*iter);
      Time spStartNew = MicroSeconds(field.GetAllocationStart());
      Time spLengthNew = MicroSeconds(field.GetAllocationBlockDuration());
      /* Check if there is an ongoing reception when the allocation period will
       * start - based on information from the PHY abut current receptions. if
       * the PHY will still be receiving when the allocation starts, delay the
       * start until the reception is finished and shorten the duration so that
       * the end time remains the same */
      /**
       * Note NINA: Temporary solution for when we are in the middle of
       * receiving a packet from a station from another BSS when a service
       * period is supposed to start. The standard is not clear about whether we
       * end the reception or finish it. For now, the reception is completed and
       * the service period will start after the end of the reception (it will
       * still finish at the same time and have a reduced duration).
       */
      if (spStartNew < endRx) {
        if (spStartNew + spLengthNew < endRx) {
          spLengthNew = NanoSeconds(0);
        } else {
          spLengthNew = spLengthNew - (endRx - spStartNew);
        }
        spStartNew = endRx;
      }
      if (field.GetAllocationType() == SERVICE_PERIOD_ALLOCATION) {
        Time spStart = MicroSeconds(field.GetAllocationStart());
        Time spLength = MicroSeconds(field.GetAllocationBlockDuration());
        Time spPeriod = MicroSeconds(field.GetAllocationBlockPeriod());
        if ((field.GetSourceAid() == AID_AP)) {
          uint8_t destAid = field.GetDestinationAid();
          Mac48Address destAddress = m_aidMap[destAid];
          if (field.GetBfControl().IsBeamformTraining()) {
            Simulator::Schedule(
                spStartNew, &ApWigigMac::StartBeamformingTraining, this,
                destAid, destAddress, true,
                field.GetBfControl().IsInitiatorTxss(),
                field.GetBfControl().IsResponderTxss(), spLengthNew);
          } else {
            DataForwardingTableIterator forwardingIterator =
                m_dataForwardingTable.find(destAddress);
            if (forwardingIterator == m_dataForwardingTable.end()) {
              NS_LOG_ERROR("Did not perform Beamforming Training with "
                           << destAddress);
              continue;
            } else {
              forwardingIterator->second.isCbapPeriod = false;
            }
            uint8_t destAid = field.GetDestinationAid();
            Mac48Address destAddress = m_aidMap[destAid];
            ScheduleServicePeriod(field.GetNumberOfBlocks(), spStart, spLength,
                                  spPeriod, field.GetAllocationId(), destAid,
                                  destAddress, true);
          }
        } else if ((field.GetSourceAid() == AID_BROADCAST) &&
                   (field.GetDestinationAid() == AID_BROADCAST)) {
          NS_LOG_INFO("No transmission is allowed from "
                      << field.GetAllocationStart() << " till "
                      << field.GetAllocationBlockDuration());
        } else if ((field.GetDestinationAid() == AID_AP) ||
                   (field.GetDestinationAid() == AID_BROADCAST)) {
          uint8_t sourceAid = field.GetSourceAid();
          Mac48Address sourceAddress = m_aidMap[sourceAid];
          if (field.GetBfControl().IsBeamformTraining()) {
            Simulator::Schedule(spStartNew, &WigigMac::StartBeamformingTraining,
                                this, sourceAid, sourceAddress, false,
                                field.GetBfControl().IsInitiatorTxss(),
                                field.GetBfControl().IsResponderTxss(),
                                spLengthNew);
          } else {
            ScheduleServicePeriod(field.GetNumberOfBlocks(), spStart, spLength,
                                  spPeriod, field.GetAllocationId(), sourceAid,
                                  sourceAddress, false);
          }
        }
      } else if ((field.GetAllocationType() == CBAP_ALLOCATION) &&
                 ((field.GetSourceAid() == AID_BROADCAST) ||
                  (field.GetSourceAid() == AID_AP) ||
                  (field.GetDestinationAid() == AID_AP)))

      {
        Simulator::Schedule(spStartNew, &ApWigigMac::StartContentionPeriod,
                            this, field.GetAllocationId(), spLengthNew);
      }
    }
  }

  /* Raise a callback that we have started DTI */
  m_dtiStarted(GetAddress(), m_dtiDuration);
}

void ApWigigMac::TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  /* For receive beam steering */
  if (m_currentAllocation == CBAP_ALLOCATION) {
    /* After transmitting a packet successfully, the DMG PCP/AP returns to the
     * quasi-omni receive mode */
    m_codebook->SetReceivingInQuasiOmniMode();
  }
  /* For association */
  if (hdr.IsAssocResp() && m_stationManager->IsWaitAssocTxOk(hdr.GetAddr1())) {
    NS_LOG_DEBUG("Associated with DMG STA=" << hdr.GetAddr1());
    uint8_t aid = m_stationManager->RecordGotAssocTxOk(hdr.GetAddr1());
    m_assocLogger(hdr.GetAddr1(), aid);
  }
  WigigMac::TxOk(packet, hdr);
}

void ApWigigMac::TxFailed(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  WigigMac::TxFailed(hdr);

  if (hdr.IsAssocResp() && m_stationManager->IsWaitAssocTxOk(hdr.GetAddr1())) {
    NS_LOG_DEBUG("assoc failed with sta=" << hdr.GetAddr1());
    m_stationManager->RecordGotAssocTxFailed(hdr.GetAddr1());
  }
}

/* Decentralized Clustering Functions */
void ApWigigMac::StartMonitoringBeaconSP(uint8_t beaconSPIndex) {
  NS_LOG_FUNCTION(this << +beaconSPIndex);
  m_beaconReceived = false;
  if (beaconSPIndex == m_clusterMaxMem - 1) {
    NS_LOG_DEBUG("We started monitoring last BeaconSP");
    Time remainingMonitoringTime =
        m_channelMonitorTime - (Simulator::Now() - m_startedMonitoringChannel);
    if (remainingMonitoringTime.IsStrictlyPositive()) {
      NS_LOG_DEBUG("Schedule further monitoring periods");
      Time clusterTimeOffset;
      for (uint8_t n = 1; n < m_clusterMaxMem; n++) {
        clusterTimeOffset = m_clusterTimeInterval * (n + 1);
        /* Schedule the beginning and end of a BeaconSP */
        Simulator::Schedule(clusterTimeOffset,
                            &ApWigigMac::StartMonitoringBeaconSP, this, n);
        Simulator::Schedule(clusterTimeOffset + m_clusterBeaconSpDuration,
                            &ApWigigMac::EndMonitoringBeaconSP, this, n);
      }
    }
  }
}

void ApWigigMac::EndMonitoringBeaconSP(uint8_t beaconSPIndex) {
  NS_LOG_FUNCTION(this << beaconSPIndex << m_beaconReceived);
  if (!m_spStatus[beaconSPIndex]) {
    NS_LOG_DEBUG("Received DMG Beacon during BeaconSP=" << +beaconSPIndex);
    m_spStatus[beaconSPIndex] = m_beaconReceived;
  }
}

void ApWigigMac::EndChannelMonitoring(Mac48Address clusterID) {
  NS_LOG_FUNCTION(this << clusterID);
  m_monitoringChannel = false;
  /* Search for empty BeaconSP */
  for (auto it = m_spStatus.begin(); it != m_spStatus.end(); ++it) {
    if (!it->second) {
      /* Join the cluster upon finding an empty BeaconSP */
      m_ClusterId = clusterID;
      m_clusterRole = PARTICIPATING;
      m_selectedBeaconSp = it->first;
      m_joinedCluster(m_ClusterId, m_selectedBeaconSp);
      NS_LOG_INFO("DMG PCP/AP " << GetAddress() << " Joined ClusterId="
                                << clusterID << ", Sending DMG Beacons in ["
                                << +m_selectedBeaconSp << "] BeaconSP");
      return;
    }
  }
  NS_LOG_DEBUG("Did not find an empty BeaconSP during channel monitoring time");
}

void ApWigigMac::StartSynBeaconInterval() {
  NS_LOG_FUNCTION(this);
  if (m_clusterRole == PARTICIPATING) {
    /* We joined cluster so we start DMG Beaconning in the specified BeaconSP */
    NS_LOG_DEBUG("Joined cluster, start DMG Beaconning at "
                 << Simulator::Now() +
                        m_clusterTimeInterval * m_selectedBeaconSp);
    m_enableDecentralizedClustering = true;
    Simulator::Schedule(m_clusterTimeInterval * m_selectedBeaconSp,
                        &ApWigigMac::StartBeaconInterval, this);
  } else {
    /* We keep scheduling SYN Beacon Interval until we find an empty BeaconSP
     * and join a cluster */
    NS_LOG_DEBUG("Keep scheduling SYN Beacon Interval until we find an empty "
                 "BeaconSP and "
                 "join a cluster");
    Simulator::Schedule(m_beaconInterval, &ApWigigMac::StartSynBeaconInterval,
                        this);
  }
}

void ApWigigMac::Receive(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);

  const WigigMacHeader *hdr = &mpdu->GetHeader();
  Ptr<Packet> packet = mpdu->GetPacket()->Copy();
  Mac48Address from = hdr->GetAddr2();

  if (hdr->IsData()) {
    Mac48Address bssid = hdr->GetAddr1();
    if (!hdr->IsFromDs() && hdr->IsToDs() && bssid == GetAddress() &&
        m_stationManager->IsAssociated(from)) {
      Mac48Address to = hdr->GetAddr3();
      if (to == GetAddress()) {
        if (hdr->IsQosData()) {
          if (hdr->IsQosAmsdu()) {
            NS_LOG_DEBUG("Received A-MSDU from=" << from << ", size="
                                                 << packet->GetSize());
            DeaggregateAmsduAndForward(mpdu);
            packet = nullptr;
          } else {
            ForwardUp(packet, from, bssid);
          }
        } else {
          ForwardUp(packet, from, bssid);
        }
      } else if (to.IsGroup() || m_stationManager->IsAssociated(to)) {
        NS_LOG_DEBUG("forwarding frame from=" << from << ", to=" << to);
        Ptr<Packet> copy = packet->Copy();

        // If the frame we are forwarding is of type QoS Data,
        // then we need to preserve the UP in the QoS control
        // header...
        if (hdr->IsQosData()) {
          ForwardDown(packet, from, to, hdr->GetQosTid());
        } else {
          ForwardDown(packet, from, to);
        }
        ForwardUp(copy, from, to);
      } else {
        ForwardUp(packet, from, to);
      }
    } else if (hdr->IsFromDs() && hdr->IsToDs()) {
      // this is an AP-to-AP frame
      // we ignore for now.
      NotifyRxDrop(packet);
    } else {
      // we can ignore these frames since
      // they are not targeted at the AP
      NotifyRxDrop(packet);
    }
    return;
  } else if (hdr->IsSsw()) {
    if (m_accessPeriod == CHANNEL_ACCESS_ABFT) {
      NS_LOG_INFO("Received SSW frame during A-BFT from=" << hdr->GetAddr2());

      /* Check if we have received any SSW frame during the current SSW-Slot */
      if (!m_receivedOneSsw) {
        m_receivedOneSsw = true;
        m_abftCollision = false;
        m_peerAbftStation = hdr->GetAddr2();
      }

      if (m_abftCollision) {
        NS_LOG_INFO("Collision detected in the current A-BFT slot no further "
                    "processing");
        return;
      }

      /* We have received a frame and there is no collision. */
      if (m_receivedOneSsw && !m_abftCollision && m_isResponderTxss) {
        /* Check for collisions */
        if (m_peerAbftStation != hdr->GetAddr2()) {
          /* If we have received SSW Frame in this slot and the newly received
             SSW frame is not from the previous transmitter, this is an
             indication of a collision */
          NS_LOG_INFO("Collision detected in the current A-BFT slot");
          m_sswFbckEvent.Cancel();
          m_sectorFeedbackScheduled = false;
          m_abftCollision = true;
        } else {
          CtrlDmgSsw sswFrame;
          packet->RemoveHeader(sswFrame);
          DmgSswField ssw = sswFrame.GetSswField();
          /* Map the antenna Tx configuration for the frame received by SLS of
           * the DMG-STA
           */
          MapTxSnr(from, ssw.GetDmgAntennaId(), ssw.GetSectorId(),
                   m_stationManager->GetRxSnr());

          /* If we receive one SSW Frame at least, then we schedule SSW-FBCK
           * frame */
          if (!m_sectorFeedbackScheduled) {
            m_sectorFeedbackScheduled = true;

            /* Record the best TX antenna configuration reported by the SSW-FBCK
             * Field
             */
            DmgSswFbckField sswFeedback = sswFrame.GetSswFeedbackField();
            sswFeedback.IsPartOfIss(false);

            /* The Sector Sweep Frame contains feedback about the the best Tx
             * Sector in the DMG-AP with the sending DMG-STA */
            ANTENNA_CONFIGURATION_TX antennaConfigTx = std::make_pair(
                sswFeedback.GetDmgAntenna(), sswFeedback.GetSector());
            ANTENNA_CONFIGURATION_RX antennaConfigRx =
                std::make_pair(NO_ANTENNA_CONFIG, NO_ANTENNA_CONFIG);
            m_bestAntennaConfig[hdr->GetAddr2()] = std::make_tuple(
                antennaConfigTx, antennaConfigRx, sswFeedback.GetSnrReport());

            NS_LOG_INFO(
                "Best TX Antenna Sector Config by this DMG AP to DMG STA="
                << from << ": AntennaId=" << +antennaConfigTx.first
                << ", SectorId=" << +antennaConfigTx.second);

            /* Indicate this DMG-STA as waiting for Beam Refinement Phase */
            m_stationBrpMap[from] = true;

            Time sswFbckTime =
                GetSectorSweepDuration(ssw.GetCountDown()) + GetMbifs();
            NS_LOG_INFO("Scheduled SSW-FBCK Frame to "
                        << hdr->GetAddr2() << " at "
                        << Simulator::Now() + sswFbckTime);
            /* The Duration field is set to 0, when the SSW-Feedback frame is
             * transmitted within an A-BFT */
            m_sswFbckEvent =
                Simulator::Schedule(sswFbckTime, &ApWigigMac::SendSswFbckFrame,
                                    this, from, MicroSeconds(0));
          }
        }
      }
    } else if (m_accessPeriod == CHANNEL_ACCESS_DTI) {
      NS_LOG_INFO("Received SSW frame during Dti from=" << hdr->GetAddr2());
      ReceiveSectorSweepFrame(packet, hdr);
    }
    return;
  } else if (hdr->IsSswFbck()) {
    NS_LOG_LOGIC("Responder: Received SSW-FBCK frame from=" << hdr->GetAddr2());

    if (m_performingBft && (m_peerStationAddress != hdr->GetAddr2())) {
      NS_LOG_LOGIC("Responder: Received SSW-FBCK frame from different "
                   "initiator, so ignore it");
      return;
    }

    /* As this might be a retry SSW-FBCK frame, we need to infrom the MacLow
     * that we are serving SLS Need to check why MacLow receive even though we
     * set the NAV duration correctly */
    m_low->SlsPhaseStarted();

    /* We are the SLS Respodner */
    CtrlDmgSswFbck fbck;
    packet->RemoveHeader(fbck);

    /* Check Beamformed link maintenance */
    RecordBeamformedLinkMaintenanceValue(fbck.GetBfLinkMaintenanceField());

    /* The SSW-FBCK contains the best TX antenna by this station */
    DmgSswFbckField sswFeedback = fbck.GetSswFeedbackField();
    sswFeedback.IsPartOfIss(false);

    /* Record best antenna configuration */
    ANTENNA_CONFIGURATION_TX antennaConfigTx =
        std::make_pair(sswFeedback.GetDmgAntenna(), sswFeedback.GetSector());
    ANTENNA_CONFIGURATION_RX antennaConfigRx =
        std::make_pair(NO_ANTENNA_CONFIG, NO_ANTENNA_CONFIG);
    m_bestAntennaConfig[hdr->GetAddr2()] = std::make_tuple(
        antennaConfigTx, antennaConfigRx, sswFeedback.GetSnrReport());

    /* We add the station to the list of the stations we can directly
     * communicate with */
    AddForwardingEntry(hdr->GetAddr2());

    /* Cancel SSW-FBCK timeout */
    m_sswFbckTimeout.Cancel();

    NS_LOG_LOGIC("Best TX Antenna Config by this DMG STA to DMG STA="
                 << hdr->GetAddr2() << ": AntennaId=" << +antennaConfigTx.first
                 << ", SectorId=" << +antennaConfigTx.second);
    NS_LOG_LOGIC("Scheduled SSW-ACK Frame to "
                 << hdr->GetAddr2() << " at " << Simulator::Now() + GetMbifs());
    Simulator::Schedule(GetMbifs(), &ApWigigMac::SendSswAckFrame, this,
                        hdr->GetAddr2(), hdr->GetDuration());

    /* Change State as we received SSW-FBCK within Dti for DMG PCP/AP */
    m_slsResponderStateMachine = SLS_RESPONDER_TXSS_PHASE_PRECOMPLETE;

    return;
  } else if (hdr->IsDmgBeacon()) {
    if (m_enableDecentralizedClustering && !m_monitoringChannel &&
        (m_clusterRole == NOT_PARTICIPATING)) {
      NS_LOG_LOGIC("Received DMG Beacon frame with BSSID=" << hdr->GetAddr1());

      ExtDmgBeacon beacon;
      packet->RemoveHeader(beacon);

      ExtDmgBeaconIntervalCtrlField beaconInterval =
          beacon.GetBeaconIntervalControlField();
      /* Cluster Control Field */
      if (beaconInterval.IsCcPresent()) {
        ExtDmgParameters parameters = beacon.GetDmgParameters();
        ExtDmgClusteringControlField cluster = beacon.GetClusterControlField();
        cluster.SetDiscoveryMode(beaconInterval.IsDiscoveryMode());
        NS_LOG_DEBUG(
            "Received DMG Beacon with Clustering Control Element Present");
        if ((!parameters.GetEcpacPolicyEnforced()) &&
            (cluster.GetClusterMemberRole() == SYNC_PCP_AP)) {
          /**
           * A decentralized clustering enabled PCP/AP that receives a DMG
           * Beacon frame with the ECPAC Policy Enforced subfield in the DMG
           * Parameters field set to 0 from an S-PCP/S-AP on the channel the
           * PCP/AP selects to establish a BSS shall monitor the channel for DMG
           * Beacon transmissions during each Beacon SP for an interval of
           * length at least aMinChannelTime.
           */

          /* Next DMG ATI Element */
          Time atiDuration(0);
          std::optional<NextDmgAti> atiElement = beacon.Get<NextDmgAti>();
          if (atiElement) {
            atiDuration = MicroSeconds(atiElement->GetAtiDuration());
          }
          m_biStartTime = MicroSeconds(beacon.GetTimestamp());
          m_beaconInterval = MicroSeconds(beacon.GetBeaconIntervalUs());

          /* Schedule Beacon SPs */
          Time clusterTimeOffset;
          m_monitoringChannel = true;
          m_clusterMaxMem = cluster.GetClusterMaxMem();
          m_clusterBeaconSpDuration = MicroSeconds(
              static_cast<uint32_t>(cluster.GetBeaconSpDuration()) *
              8); /* Units of 8 us*/
          m_clusterTimeInterval = (m_beaconInterval / m_clusterMaxMem);
          m_startedMonitoringChannel = Simulator::Now();
          NS_LOG_DEBUG("Cluster: BeaconSP Duration="
                       << m_clusterBeaconSpDuration
                       << ", Cluster Time Interval=" << m_clusterTimeInterval
                       << ", BI Start Time of the received DMG Beacon="
                       << m_biStartTime);

          Time timeShift = (Simulator::Now() - m_biStartTime);
          m_spStatus[0] =
              true; /* The first Beacon SP is reserved for S-PCP/S-AP */
          /* Initialize each SP Status to false and schedule monitoring period
           * for each BeaconSP */
          for (uint8_t n = 1; n < m_clusterMaxMem; n++) {
            m_spStatus[n] = false;
            clusterTimeOffset = m_clusterTimeInterval * n -
                                timeShift; /* Cluster offset is with respect
                                              to the beginning of the BI */
            /* Schedule the beginning and the end of a BeaconSP */
            Simulator::Schedule(clusterTimeOffset,
                                &ApWigigMac::StartMonitoringBeaconSP, this, n);
            Simulator::Schedule(clusterTimeOffset + m_clusterBeaconSpDuration,
                                &ApWigigMac::EndMonitoringBeaconSP, this, n);
          }

          /* Schedule the beginning of the next BI of the S-PCP/S-AP */
          Simulator::Schedule(m_beaconInterval - timeShift,
                              &ApWigigMac::StartSynBeaconInterval, this);

          /* Schedule the end of the monitoring period */
          Simulator::Schedule(m_channelMonitorTime,
                              &ApWigigMac::EndChannelMonitoring, this,
                              cluster.GetClusterId());
        }
      }
    } else if (m_monitoringChannel) {
      NS_LOG_LOGIC(
          "Received DMG Beacon frame during monitoring period with BSSID="
          << hdr->GetAddr1());
      m_beaconReceived = true;
    }
    return;
  } else if (hdr->IsMgt()) {
    if (hdr->IsProbeReq()) {
      NS_ASSERT(hdr->GetAddr1().IsBroadcast());
      SendProbeResp(from);
      return;
    } else if (hdr->GetAddr1() == GetAddress()) {
      if (hdr->IsAssocReq()) {
        // First, verify that the the station's supported
        // rate set is compatible with our Basic Rate set
        WigigMgtAssocRequestHeader assocReq;
        packet->RemoveHeader(assocReq);
        bool problem = false;
        if (m_dmgSupported) {
          // check that the DMG STA supports all MCSs in Basic MCS Set
        }
        if (problem) {
          // One of the Basic Rate set mode is not
          // supported by the station. So, we return an assoc
          // response with an error status.
          SendAssocResp(hdr->GetAddr2(), false);
        } else {
          /* Send assocication response with success status. */
          uint16_t aid = SendAssocResp(hdr->GetAddr2(), true);

          /* Inform the Remote Station Manager that association is OK */
          m_stationManager->RecordWaitAssocTxOk(from, aid);

          /** Record DMG STA Capabilities **/
          /* Set AID of the station */
          std::optional<DmgCapabilities> dmgCapabilities =
              assocReq.Get<DmgCapabilities>();
          NS_ASSERT(dmgCapabilities);
          dmgCapabilities->SetAid(aid & 0xFF);
          MapAidToMacAddress(aid, hdr->GetAddr2());

          /* Record DMG SC MCSs (1-4) as mandatory modes for data communication
           */
          AddMcsSupport(from, 1, 4);
          /* Record SC MCSs range */
          AddMcsSupport(from, 5, dmgCapabilities->GetMaximumScTxMcs());
          /* Record OFDM MCSs range */
          if ((GetPhy()->GetSupportOfdmPhy()) &&
              (dmgCapabilities->GetMaximumOfdmTxMcs() != 0)) {
            AddMcsSupport(from, 13, dmgCapabilities->GetMaximumOfdmTxMcs());
          }

          /* Record DMG Capabilities */
          m_informationMap[hdr->GetAddr2()] = *dmgCapabilities;
          m_stationManager->AddStationDmgCapabilities(hdr->GetAddr2(),
                                                      *dmgCapabilities);

          /** Check Relay Capabilities **/
          std::optional<RelayCapabilitiesElement> relayElement =
              assocReq.Get<RelayCapabilitiesElement>();

          /* Check if the DMG STA supports RDS */
          if (relayElement && (relayElement->GetRelayCapabilitiesInfo()
                                   .GetRelaySupportability())) {
            m_rdsList[aid] = relayElement->GetRelayCapabilitiesInfo();
            NS_LOG_DEBUG("Station=" << from << " with AID=" << aid
                                    << " supports RDS operation");
          }
        }
        return;
      } else if (hdr->IsDisassociation()) {
        m_stationManager->RecordDisassociated(from);
        for (std::map<uint16_t, Mac48Address>::const_iterator j =
                 m_staList.begin();
             j != m_staList.end(); j++) {
          if (j->second == from) {
            m_staList.erase(j);
            m_deAssocLogger(from);
            break;
          }
        }
        return;
      }
      /* Received Action Frame */
      else if (hdr->IsAction()) {
        WifiActionHeader actionHdr;
        packet->RemoveHeader(actionHdr);
        switch (actionHdr.GetCategory()) {
        case WifiActionHeader::QOS:
          switch (actionHdr.GetAction().qos) {
          case WifiActionHeader::ADDTS_REQUEST: {
            DmgAddTsRequestFrame frame;
            packet->RemoveHeader(frame);

            std::optional<DmgTspecElement> dmgTspecElement =
                frame.Get<DmgTspecElement>();
            NS_ASSERT(dmgTspecElement);

            /* Callback to the user, so can take decision */
            m_addTsRequestReceived(hdr->GetAddr2(), *dmgTspecElement);
            return;
          }
          case WifiActionHeader::DELTS: {
            DelTsFrame frame;
            packet->RemoveHeader(frame);
            /* Search for the allocation */
            DmgAllocationInfo info = frame.GetDmgAllocationInfo();
            AllocationField allocation;
            for (AllocationFieldList::iterator iter = m_allocationList.begin();
                 iter != m_allocationList.end();) {
              allocation = (*iter);
              if ((allocation.GetAllocationId() == info.GetAllocationId()) &&
                  (allocation.GetSourceAid() ==
                   GetStationAid(hdr->GetAddr2())) &&
                  (allocation.GetDestinationAid() ==
                   info.GetDestinationAid())) {
                iter = m_allocationList.erase(iter);
                break;
              } else {
                ++iter;
              }
            }
            return;
          }
          default:
            packet->AddHeader(actionHdr);
            WigigMac::Receive(mpdu);
            return;
          }
        case WifiActionHeader::DMG:
          switch (actionHdr.GetAction().dmgAction) {
          case WifiActionHeader::DMG_INFORMATION_REQUEST: {
            ExtInformationRequest requestHdr;
            packet->RemoveHeader(requestHdr);

            Mac48Address subjectAddress = requestHdr.GetSubjectAddress();
            NS_LOG_INFO("Received Information Request Frame from "
                        << from << " with Subject=" << subjectAddress);

            Ptr<Packet> response = Create<Packet>();
            ExtInformationResponse responseHdr;

            /* Parse the requested IEs in the Request Information Element
             * Subfield */
            std::optional<RequestElement> requestElement =
                requestHdr.Get<RequestElement>();
            NS_ASSERT(requestElement);
            WifiInformationElementIdList elementList =
                requestElement->GetWifiInformationElementIdList();
            for (WifiInformationElementIdList::const_iterator infoElement =
                     elementList.begin();
                 infoElement != elementList.end(); infoElement++) {
              if (subjectAddress == Mac48Address::GetBroadcast()) {
                for (const auto &kv : m_informationMap) {
                  if (kv.first != from) {
                    if (infoElement->first == IE_DMG_CAPABILITIES) {
                      responseHdr.Get<DmgCapabilities>().push_back(kv.second);
                    } else {
                      NS_ASSERT(false);
                    }
                  }
                }
              } else {
                if (infoElement->first == IE_DMG_CAPABILITIES) {
                  auto iter = m_informationMap.find(subjectAddress);
                  NS_ASSERT(iter != m_informationMap.end());
                  responseHdr.Get<DmgCapabilities>().push_back(iter->second);
                  ;
                } else {
                  NS_ASSERT(false);
                }
              }
            }

            /* The Information Response frame shall carry DMGCapabilities
             * Element for the transmitter STA and other STAs known to the
             * transmitter STA. */
            for (const auto &[infoAddress, dmgCapabilities] :
                 m_informationMap) {
              if ((infoAddress != from) && (infoAddress != subjectAddress)) {
                responseHdr.Get<DmgCapabilities>().push_back(dmgCapabilities);
                // response->AddHeader (infoSta.first);
              }
            }

            responseHdr.Get<DmgCapabilities>().push_back(GetDmgCapabilities());
            responseHdr.SetSubjectAddress(subjectAddress);
            response->AddHeader(responseHdr);

            /* Send Information Response Frame */
            SendInformationResponse(from, response);
            return;
          }
          case WifiActionHeader::DMG_INFORMATION_RESPONSE: {
            ExtInformationResponse responseHdr;
            packet->RemoveHeader(responseHdr);

            /* Record the Information Obtained */
            Mac48Address stationAddress = responseHdr.GetSubjectAddress();

            /* If this field is set to the broadcast address, then the STA is
             * providing information regarding all associated STAs.*/
            if (stationAddress.IsBroadcast()) {
              NS_LOG_LOGIC(
                  "Received DMG Information Response frame regarding all "
                  "DMG STAs in the DMG BSS.");
            } else if (stationAddress == GetAddress()) {
              NS_LOG_LOGIC(
                  "Received Unsolicited Information Response with update on "
                  "Best TX Configuration");
            } else {
              NS_LOG_LOGIC(
                  "Received DMG Information Response frame regarding DMG STA: "
                  << stationAddress);
            }

            /* Store all the DMG Capabilities */
            std::size_t count = responseHdr.Get<DmgCapabilities>().size();
            for (std::size_t i = 0; i < count; ++i) {
              DmgCapabilities dmgCapabilities =
                  responseHdr.Get<DmgCapabilities>()[i];
              m_informationMap[dmgCapabilities.GetStaAddress()] =
                  dmgCapabilities;
              InformationMapIterator infoIter =
                  m_informationMap.find(dmgCapabilities.GetStaAddress());
              if (infoIter == m_informationMap.end()) {
                m_informationMap[dmgCapabilities.GetStaAddress()] =
                    dmgCapabilities;
              } else {
                m_informationMap[dmgCapabilities.GetStaAddress()] =
                    dmgCapabilities;
                MapAidToMacAddress(dmgCapabilities.GetAid(),
                                   dmgCapabilities.GetStaAddress());
              }
            }

            return;
          }
          default:
            packet->AddHeader(actionHdr);
            WigigMac::Receive(mpdu);
            return;
          }
        default:
          packet->AddHeader(actionHdr);
          WigigMac::Receive(mpdu);
          return;
        }
      } else if (hdr->IsActionNoAck()) {
        WigigMac::Receive(mpdu);
        return;
      }
    }
    return;
  }
  WigigMac::Receive(mpdu);
}

void ApWigigMac::DeaggregateAmsduAndForward(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);
  for (auto &i : *PeekPointer(mpdu)) {
    if (i.second.GetDestinationAddr() == GetAddress()) {
      ForwardUp(i.first, i.second.GetSourceAddr(),
                i.second.GetDestinationAddr());
    } else {
      Mac48Address from = i.second.GetSourceAddr();
      Mac48Address to = i.second.GetDestinationAddr();
      NS_LOG_DEBUG("forwarding QoS frame from=" << from << ", to=" << to);
      ForwardDown(i.first->Copy(), from, to, mpdu->GetHeader().GetQosTid());
    }
  }
}

void ApWigigMac::DoInitialize() {
  NS_LOG_FUNCTION(this);
  m_beaconEvent.Cancel();
  m_beaconTxop->Initialize();

  /* Calculate A-BFT Duration (Constant during the entire simulation) */
  m_abftDuration = m_ssSlotsPerAbft * GetSectorSweepSlotTime(m_ssFramesPerSlot);

  /* Set Beacon randomization flag*/
  m_codebook->RandomizeBeacon(m_beaconRandomization);

  /* Initialize Upper layers */
  WigigMac::DoInitialize();

  /* Decentralzied Clustering */
  if (m_enableDecentralizedClustering) {
    m_clusterTimeInterval = (m_beaconInterval / m_clusterMaxMem);
    m_clusterBeaconSpDuration = MicroSeconds(
        static_cast<uint32_t>(m_beaconSpDuration * 8)); /* Units of 8 us*/
  }

  /* Start Beacon Interval */
  if (m_allowBeaconing) {
    if (m_enableBeaconJitter) {
      int64_t jitter = m_beaconJitter->GetValue();
      NS_LOG_DEBUG("Scheduling BI for AP " << GetAddress() << " at time "
                                           << jitter << " microseconds");
      m_beaconEvent = Simulator::Schedule(
          MicroSeconds(jitter), &ApWigigMac::StartBeaconInterval, this);
    } else {
      Simulator::ScheduleNow(&ApWigigMac::StartBeaconInterval, this);
    }
  }
  m_phy->SetIsAp(true);
}

uint16_t ApWigigMac::GetNextAssociationId() {
  // Return the first free AID value between 1 and 255
  for (uint16_t nextAid = 1; nextAid <= 255; nextAid++) {
    if (m_staList.find(nextAid) == m_staList.end()) {
      return nextAid;
    }
  }
  NS_ASSERT_MSG(false, "No free association ID available!");
  return 0;
}

} // namespace ns3
