/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-phy-header.h"

namespace ns3 {

//////////////////// IEEE 802.11ad DMG Control PHY Header ////////////////////

NS_OBJECT_ENSURE_REGISTERED(DmgControlHeader);

DmgControlHeader::DmgControlHeader()
    : m_length(14), m_packetType(TRN_T), m_trainingLength(0) {}

DmgControlHeader::~DmgControlHeader() {}

TypeId DmgControlHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgControlHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgControlHeader>();
  return tid;
}

TypeId DmgControlHeader::GetInstanceTypeId() const { return GetTypeId(); }

void DmgControlHeader::Print(std::ostream &os) const {
  os << "LENGTH=" << m_length << " PACKET_TYPE=" << m_packetType
     << " TRAINING_LENGTH=" << m_trainingLength;
}

uint32_t DmgControlHeader::GetSerializedSize() const { return 5; }

void DmgControlHeader::SetLength(uint16_t length, bool isShortSsw) {
  if (isShortSsw) {
    NS_ASSERT_MSG(length == 6,
                  "PSDU Size for Short SSW packets should be 6 octets.");
  } else {
    NS_ASSERT_MSG((length >= 14) && (length <= 1023),
                  "PSDU size should be between 14 and 1023 octets.");
  }
  m_length = length;
}

uint16_t DmgControlHeader::GetLength() const { return m_length; }

void DmgControlHeader::SetPacketType(PacketType type) { m_packetType = type; }

PacketType DmgControlHeader::GetPacketType() const { return m_packetType; }

void DmgControlHeader::SetTrainingLength(uint16_t length) {
  NS_ASSERT_MSG(length <= 16, "The maximum number of TRN-Units is 16.");
  m_trainingLength = length;
}

uint16_t DmgControlHeader::GetTrainingLength() const {
  return m_trainingLength;
}

void DmgControlHeader::Serialize(Buffer::Iterator start) const {
  uint16_t value1 = 0;
  value1 |= (m_length & 0x3FF) << 5;
  value1 |= (m_packetType & 0x1) << 15;
  start.WriteU16(value1);
  start.WriteU8(m_trainingLength & 0x1F);
  start.WriteU16(0);
}

uint32_t DmgControlHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint16_t value1 = i.ReadU16();
  m_length = (value1 >> 5) & 0x3FF;
  m_packetType = static_cast<PacketType>((value1 >> 15) & 0x1);
  m_trainingLength = (i.ReadU8() & 0x1F);
  i.ReadU16();
  return i.GetDistanceFrom(start);
}

//////////////////// IEEE 802.11ad DMG OFDM PHY Header ////////////////////

NS_OBJECT_ENSURE_REGISTERED(DmgOfdmHeader);

DmgOfdmHeader::DmgOfdmHeader()
    : m_baseMcs(1), m_length(1), m_packetType(TRN_T), m_trainingLength(0),
      m_aggregation(false), m_beamTrackingRequest(false), m_lastRssi(0) {}

DmgOfdmHeader::~DmgOfdmHeader() {}

TypeId DmgOfdmHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgOfdmHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgOfdmHeader>();
  return tid;
}

TypeId DmgOfdmHeader::GetInstanceTypeId() const { return GetTypeId(); }

void DmgOfdmHeader::Print(std::ostream &os) const {
  os << "BASE_MCS=" << +m_baseMcs << " LENGTH=" << m_length
     << " PACKET_TYPE=" << m_packetType
     << " TRAINING_LENGTH=" << m_trainingLength
     << " AGGREGATION=" << m_aggregation
     << " BEAM_TRACKING_REQUEST=" << m_beamTrackingRequest
     << " LAST_RSSI=" << +m_lastRssi;
}

uint32_t DmgOfdmHeader::GetSerializedSize() const { return 8; }

void DmgOfdmHeader::SetBaseMcs(uint8_t mcs) { m_baseMcs = mcs; }

uint8_t DmgOfdmHeader::GetBaseMcs() const { return m_baseMcs; }

void DmgOfdmHeader::SetLength(uint32_t length) { m_length = length; }

uint32_t DmgOfdmHeader::GetLength() const { return m_length; }

void DmgOfdmHeader::SetPacketType(PacketType type) { m_packetType = type; }

PacketType DmgOfdmHeader::GetPacketType() const { return m_packetType; }

void DmgOfdmHeader::SetTrainingLength(uint16_t length) {
  m_trainingLength = length;
}

uint16_t DmgOfdmHeader::GetTrainingLength() const { return m_trainingLength; }

void DmgOfdmHeader::SetAggregation(bool aggregation) {
  m_aggregation = aggregation;
}

bool DmgOfdmHeader::GetAggregation() const { return m_aggregation; }

void DmgOfdmHeader::SetBeamTrackingRequest(bool request) {
  m_beamTrackingRequest = request;
}

bool DmgOfdmHeader::GetBeamTrackingRequest() const {
  return m_beamTrackingRequest;
}

void DmgOfdmHeader::SetLastRssi(uint8_t rssi) { m_lastRssi = rssi; }

uint8_t DmgOfdmHeader::GetLastRssi() const { return m_lastRssi; }

void DmgOfdmHeader::Serialize(Buffer::Iterator start) const {
  uint32_t bytes1 = (m_baseMcs & 0x1F) << 7;
  bytes1 |= ((m_length & 0x3FFFF) << 12);
  start.WriteU32(bytes1);
  uint16_t bytes2 = (m_packetType & 0x1);
  bytes2 |= ((m_trainingLength & 0x1F) << 1);
  bytes2 |= ((m_aggregation & 0x1) << 6);
  bytes2 |= ((m_beamTrackingRequest & 0x1) << 7);
  bytes2 |= ((m_lastRssi & 0xF) << 10);
  start.WriteU16(bytes2);
  start.WriteU16(0);
}

uint32_t DmgOfdmHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint32_t bytes1 = i.ReadU32();
  m_baseMcs = (bytes1 >> 7) & 0x1F;
  m_length = (bytes1 >> 12) & 0x3FFFF;
  uint16_t bytes2 = i.ReadU32();
  m_packetType = static_cast<PacketType>(bytes2 & 0x1);
  m_trainingLength = (bytes2 >> 1) & 0x1F;
  m_aggregation = (bytes2 >> 6) & 0x1;
  m_beamTrackingRequest = (bytes2 >> 7) & 0x1;
  m_lastRssi = (bytes2 >> 10) & 0xF;
  start.ReadU16();
  return i.GetDistanceFrom(start);
}

//////////////////// IEEE 802.11ad DMG SC PHY Header ////////////////////

NS_OBJECT_ENSURE_REGISTERED(DmgScHeader);

DmgScHeader::DmgScHeader() : m_extendedScMcsIndication(false) {}

DmgScHeader::~DmgScHeader() {}

TypeId DmgScHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgScHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgScHeader>();
  return tid;
}

TypeId DmgScHeader::GetInstanceTypeId() const { return GetTypeId(); }

void DmgScHeader::Print(std::ostream &os) const {
  os << " EXTENDED_SC_MCS_INDICATION=" << m_extendedScMcsIndication;
}

uint32_t DmgScHeader::GetSerializedSize() const { return 6; }

void DmgScHeader::Serialize(Buffer::Iterator start) const {
  uint32_t bytes1 = (m_baseMcs & 0x1F) << 7;
  bytes1 |= ((m_length & 0x3FFFF) << 12);
  start.WriteU32(bytes1);
  uint16_t bytes2 = (m_packetType & 0x1);
  bytes2 |= ((m_trainingLength & 0x1F) << 1);
  bytes2 |= ((m_aggregation & 0x1) << 6);
  bytes2 |= ((m_beamTrackingRequest & 0x1) << 7);
  bytes2 |= ((m_lastRssi & 0xF) << 10);
  bytes2 |= ((m_extendedScMcsIndication & 0xF) << 14);
  start.WriteU16(bytes2);
  start.WriteU16(0);
}

uint32_t DmgScHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint32_t bytes1 = i.ReadU32();
  m_baseMcs = (bytes1 >> 7) & 0x1F;
  m_length = (bytes1 >> 12) & 0x3FFFF;
  uint16_t bytes2 = i.ReadU32();
  m_packetType = static_cast<PacketType>(bytes2 & 0x1);
  m_trainingLength = (bytes2 >> 1) & 0x1F;
  m_aggregation = (bytes2 >> 6) & 0x1;
  m_beamTrackingRequest = (bytes2 >> 7) & 0x1;
  m_lastRssi = (bytes2 >> 10) & 0xF;
  m_extendedScMcsIndication = (bytes2 >> 14) & 0xF;
  start.ReadU16();
  return i.GetDistanceFrom(start);
}

void DmgScHeader::SetExtendedScMcsIndication(bool extended) {
  m_extendedScMcsIndication = extended;
}

bool DmgScHeader::GetExtendedScMcsIndication() const {
  return m_extendedScMcsIndication;
}

} // namespace ns3
