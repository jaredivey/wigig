/*
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_CTRL_HEADERS_H
#define WIGIG_CTRL_HEADERS_H

#include "fields-headers.h"

#include "ns3/header.h"

namespace ns3 {

/*************************
 *  Poll Frame (8.3.1.11)
 *************************/

/**
 * \ingroup wigig
 * \brief Header for Poll Frame.
 */
class CtrlDmgPoll : public Header {
public:
  CtrlDmgPoll();
  ~CtrlDmgPoll() override;
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set the offset in units of 1 microseconds.
   *
   * \param value The offset in units of 1 microseconds.
   */
  void SetResponseOffset(uint16_t value);

  /**
   * Return the offset in units of 1 microseconds.
   *
   * \return the offset in units of 1 microseconds.
   */
  uint16_t GetResponseOffset() const;

private:
  uint16_t m_responseOffset;
};

/***********************************************
 * Service Period Request (SPR) Frame (8.3.1.12)
 ***********************************************/

/**
 * \ingroup wigig
 * \brief Header for Service Period Request (SPR) Frame.
 */
class CtrlDmgSpr : public Header {
public:
  CtrlDmgSpr();
  ~CtrlDmgSpr() override;
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set the Dynamic Allocation Information Field.
   *
   * \param value The Dynamic Allocation Information Field.
   */
  void SetDynamicAllocationInfo(DynamicAllocationInfoField field);
  /**
   * Set the offset in units of 1 microseconds.
   *
   * \param value The offset in units of 1 microseconds.
   */
  void SetBfControl(BfControlField value);

  /**
   * Return the offset in units of 1 microseconds.
   *
   * \return the offset in units of 1 microseconds.
   */
  DynamicAllocationInfoField GetDynamicAllocationInfo() const;
  /**
   * Return the offset in units of 1 microseconds.
   *
   * \return the offset in units of 1 microseconds.
   */
  BfControlField GetBfControl() const;

private:
  DynamicAllocationInfoField m_dynamic;
  BfControlField m_bfControl;
};

/*************************
 * Grant Frame (8.3.1.13)
 *************************/

/**
 * \ingroup wigig
 * \brief Header for Grant Frame.
 */
class CtrlDmgGrant : public CtrlDmgSpr {
public:
  CtrlDmgGrant();
  ~CtrlDmgGrant() override;
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
};

/****************************************
 *  Sector Sweep (SSW) Frame (8.3.1.16)
 ****************************************/

/**
 * \ingroup wigig
 * \brief Header for Sector Sweep (SSW) Frame.
 */
class CtrlDmgSsw : public Header {
public:
  CtrlDmgSsw();
  ~CtrlDmgSsw() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetSswField(DmgSswField &field);
  void SetSswFeedbackField(DmgSswFbckField &field);
  DmgSswField GetSswField() const;
  DmgSswFbckField GetSswFeedbackField() const;

private:
  DmgSswField m_ssw;
  DmgSswFbckField m_sswFeedback;
};

/*********************************************************
 *  Sector Sweep Feedback (SSW-Feedback) Frame (8.3.1.17)
 *********************************************************/

/**
 * \ingroup wigig
 * \brief Header for Sector Sweep Feedback (SSW-Feedback) Frame.
 */
class CtrlDmgSswFbck : public Header {
public:
  CtrlDmgSswFbck();
  ~CtrlDmgSswFbck() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const override;
  uint32_t GetSerializedSize() const override;
  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  void SetSswFeedbackField(DmgSswFbckField &field);
  void SetBrpRequestField(BrpRequestField &field);
  void SetBfLinkMaintenanceField(BfLinkMaintenanceField &field);

  DmgSswFbckField GetSswFeedbackField() const;
  BrpRequestField GetBrpRequestField() const;
  BfLinkMaintenanceField GetBfLinkMaintenanceField() const;

private:
  DmgSswFbckField m_sswFeedback;
  BrpRequestField m_brpRequest;
  BfLinkMaintenanceField m_linkMaintenance;
};

/**********************************************
 * Sector Sweep ACK (SSW-ACK) Frame (8.3.1.18)
 **********************************************/

/**
 * \ingroup wigig
 * \brief Header for Sector Sweep ACK (SSW-ACK) Frame.
 */
class CtrlDmgSswAck : public CtrlDmgSswFbck {
public:
  CtrlDmgSswAck();
  ~CtrlDmgSswAck() override;
  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
};

} // namespace ns3

#endif /* WIGIG_CTRL_HEADERS_H */
