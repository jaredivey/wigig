/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "dmg-beacon-txop.h"

#include "mac-low.h"
#include "wigig-mac-queue.h"

#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

#undef NS_LOG_APPEND_CONTEXT
#define NS_LOG_APPEND_CONTEXT                                                  \
  if (m_low) {                                                                 \
    std::clog << "[mac=" << m_low->GetAddress() << "] ";                       \
  }

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("DmgBeaconTxop");

NS_OBJECT_ENSURE_REGISTERED(DmgBeaconTxop);

TypeId DmgBeaconTxop::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgBeaconTxop")
                          .SetParent<WigigTxop>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgBeaconTxop>();
  return tid;
}

DmgBeaconTxop::DmgBeaconTxop() { NS_LOG_FUNCTION(this); }

DmgBeaconTxop::~DmgBeaconTxop() { NS_LOG_FUNCTION(this); }

void DmgBeaconTxop::PerformCca() {
  NS_LOG_FUNCTION(this);
  ResetCw();
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void DmgBeaconTxop::SetAccessGrantedCallback(AccessGranted callback) {
  m_accessGrantedCallback = callback;
}

void DmgBeaconTxop::TransmitDmgBeacon(Ptr<Packet> packet,
                                      const WigigMacHeader &hdr,
                                      Time btiRemainingTime) {
  NS_LOG_FUNCTION(this << packet << &hdr << btiRemainingTime);
  m_currentHdr = hdr;

  /* The Duration field is set to the time remaining until the end of the BTI.
   */
  m_currentParams.EnableOverrideDurationId(btiRemainingTime);
  m_currentParams.DisableRts();
  m_currentParams.DisableAck();
  m_currentParams.DisableNextData();

  GetLow()->TransmitSingleFrame(Create<WigigMacQueueItem>(packet, m_currentHdr),
                                m_currentParams, this);
}

void DmgBeaconTxop::RestartAccessIfNeeded() {
  NS_LOG_FUNCTION(this);
  if (!IsAccessRequested()) {
    m_channelAccessManager->RequestAccess(this);
  }
}

void DmgBeaconTxop::NotifyAccessGranted() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT(m_accessRequested);
  m_accessRequested = false;
  m_accessGrantedCallback();
}

void DmgBeaconTxop::NotifyInternalCollision() {
  NS_LOG_FUNCTION(this);
  GenerateBackoff();
  RestartAccessIfNeeded();
}

void DmgBeaconTxop::Cancel() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("Transmission cancelled");
}

void DmgBeaconTxop::EndTxNoAck() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("a transmission that did not require an ACK just finished");
  if (!m_txOkNoAckCallback.IsNull()) {
    m_txOkNoAckCallback(m_currentHdr);
  }
}

} // namespace ns3
