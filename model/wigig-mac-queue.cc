/*
 * Copyright (c) 2005, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Stefano Avallone <stavallo@unina.it>
 */

#include "wigig-mac-queue.h"

#include "ns3/qos-blocked-destinations.h"
#include "ns3/simulator.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigMacQueue");

NS_OBJECT_ENSURE_REGISTERED(WigigMacQueue);
NS_OBJECT_TEMPLATE_CLASS_DEFINE(Queue, WigigMacQueueItem);

TypeId WigigMacQueue::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigMacQueue")
          .SetParent<Queue<WigigMacQueueItem>>()
          .SetGroupName("Wigig")
          .AddConstructor<WigigMacQueue>()
          .AddAttribute("MaxSize", "The max queue size",
                        QueueSizeValue(QueueSize("500p")),
                        MakeQueueSizeAccessor(&QueueBase::SetMaxSize,
                                              &QueueBase::GetMaxSize),
                        MakeQueueSizeChecker())
          .AddAttribute("MaxDelay",
                        "If a packet stays longer than this delay in the "
                        "queue, it is dropped.",
                        TimeValue(MilliSeconds(500)),
                        MakeTimeAccessor(&WigigMacQueue::SetMaxDelay),
                        MakeTimeChecker())
          .AddAttribute("DropPolicy",
                        "Upon enqueue with full queue, drop oldest "
                        "(DropOldest) or newest "
                        "(DropNewest) packet",
                        EnumValue(FcfsWifiQueueScheduler::DROP_NEWEST),
                        MakeEnumAccessor(&WigigMacQueue::m_dropPolicy),
                        MakeEnumChecker(
                            FcfsWifiQueueScheduler::DROP_OLDEST, "DropOldest",
                            FcfsWifiQueueScheduler::DROP_NEWEST, "DropNewest"))
          .AddTraceSource(
              "Expired", "MPDU dropped because its lifetime expired.",
              MakeTraceSourceAccessor(&WigigMacQueue::m_traceExpired),
              "ns3::WigigMacQueueItem::TracedCallback");
  return tid;
}

WigigMacQueue::WigigMacQueue()
    : m_expiredPacketsPresent(false), NS_LOG_TEMPLATE_DEFINE("WigigMacQueue") {}

WigigMacQueue::~WigigMacQueue() { NS_LOG_FUNCTION_NOARGS(); }

static std::list<Ptr<WigigMacQueueItem>> g_emptyWigigMacQueue;

const WigigMacQueue::ConstIterator WigigMacQueue::EMPTY =
    g_emptyWigigMacQueue.end();

void WigigMacQueue::SetMaxDelay(Time delay) {
  NS_LOG_FUNCTION(this << delay);
  m_maxDelay = delay;
}

Time WigigMacQueue::GetMaxDelay() const { return m_maxDelay; }

bool WigigMacQueue::TtlExceeded(ConstIterator &it) {
  NS_LOG_FUNCTION(this);

  if (Simulator::Now() > (*it)->GetTimeStamp() + m_maxDelay) {
    NS_LOG_DEBUG("Removing packet that stayed in the queue for too long ("
                 << Simulator::Now() - (*it)->GetTimeStamp() << ")");
    m_traceExpired(*it);
    auto curr = it++;
    DoRemove(curr);
    return true;
  }
  return false;
}

bool WigigMacQueue::Enqueue(Ptr<WigigMacQueueItem> item) {
  NS_LOG_FUNCTION(this << *item);

  return Insert(GetContainer().end(), item);
}

bool WigigMacQueue::PushFront(Ptr<WigigMacQueueItem> item) {
  NS_LOG_FUNCTION(this << *item);

  return Insert(GetContainer().begin(), item);
}

bool WigigMacQueue::Insert(ConstIterator pos, Ptr<WigigMacQueueItem> item) {
  NS_LOG_FUNCTION(this << *item);
  NS_ASSERT_MSG(GetMaxSize().GetUnit() == QueueSizeUnit::PACKETS,
                "WigigMacQueues must be in packet mode");

  // insert the item if the queue is not full
  if (QueueBase::GetNPackets() < GetMaxSize().GetValue()) {
    return DoEnqueue(pos, item);
  }

  // the queue is full; scan the list in the attempt to remove stale packets
  ConstIterator it = GetContainer().begin();
  while (it != GetContainer().end()) {
    if (it == pos && TtlExceeded(it)) {
      return DoEnqueue(it, item);
    }
    if (TtlExceeded(it)) {
      return DoEnqueue(pos, item);
    }
    it++;
  }

  // the queue is still full, remove the oldest item if the policy is drop
  // oldest
  if (m_dropPolicy == FcfsWifiQueueScheduler::DROP_OLDEST) {
    NS_LOG_DEBUG("Remove the oldest item in the queue");
    DoRemove(GetContainer().begin());
  }

  return DoEnqueue(pos, item);
}

Ptr<WigigMacQueueItem> WigigMacQueue::Dequeue() {
  NS_LOG_FUNCTION(this);
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      return DoDequeue(it);
    }
  }
  NS_LOG_DEBUG("The queue is empty");
  return nullptr;
}

Ptr<WigigMacQueueItem> WigigMacQueue::DequeueByAddress(Mac48Address dest) {
  NS_LOG_FUNCTION(this << dest);
  ConstIterator it = PeekByAddress(dest);

  if (it == GetContainer().end()) {
    return nullptr;
  }
  return Dequeue(it);
}

Ptr<WigigMacQueueItem> WigigMacQueue::DequeueByTid(uint8_t tid) {
  NS_LOG_FUNCTION(this << +tid);
  ConstIterator it = PeekByTid(tid);

  if (it == GetContainer().end()) {
    return nullptr;
  }
  return Dequeue(it);
}

Ptr<WigigMacQueueItem>
WigigMacQueue::DequeueByTidAndAddress(uint8_t tid, Mac48Address dest) {
  NS_LOG_FUNCTION(this << +tid << dest);
  ConstIterator it = PeekByTidAndAddress(tid, dest);

  if (it == GetContainer().end()) {
    return nullptr;
  }
  return Dequeue(it);
}

Ptr<WigigMacQueueItem> WigigMacQueue::DequeueFirstAvailable(
    const Ptr<QosBlockedDestinations> blockedPackets) {
  NS_LOG_FUNCTION(this);
  ConstIterator it = PeekFirstAvailable(blockedPackets);

  if (it == GetContainer().end()) {
    return nullptr;
  }
  return Dequeue(it);
}

Ptr<WigigMacQueueItem> WigigMacQueue::Dequeue(ConstIterator pos) {
  NS_LOG_FUNCTION(this);

  if (!m_expiredPacketsPresent) {
    if (TtlExceeded(pos)) {
      NS_LOG_DEBUG("Packet lifetime expired");
      return nullptr;
    }
    return DoDequeue(pos);
  }

  // remove stale items queued before the given position
  ConstIterator it = GetContainer().begin();
  while (it != GetContainer().end()) {
    if (it == pos) {
      // reset the flag signaling the presence of expired packets before
      // returning
      m_expiredPacketsPresent = false;

      if (TtlExceeded(it)) {
        return nullptr;
      }
      return DoDequeue(it);
    } else if (!TtlExceeded(it)) {
      it++;
    }
  }
  NS_LOG_DEBUG("Invalid iterator");
  return nullptr;
}

Ptr<const WigigMacQueueItem> WigigMacQueue::Peek() const {
  NS_LOG_FUNCTION(this);
  for (auto it = GetContainer().begin(); it != GetContainer().end(); it++) {
    // skip packets that stayed in the queue for too long. They will be
    // actually removed from the queue by the next call to a non-const method
    if (Simulator::Now() <= (*it)->GetTimeStamp() + m_maxDelay) {
      return DoPeek(it);
    }
    // signal the presence of expired packets
    m_expiredPacketsPresent = true;
  }
  NS_LOG_DEBUG("The queue is empty");
  return nullptr;
}

WigigMacQueue::ConstIterator
WigigMacQueue::PeekByAddress(Mac48Address dest, ConstIterator pos) const {
  NS_LOG_FUNCTION(this << dest);
  ConstIterator it = (pos != EMPTY ? pos : GetContainer().begin());
  while (it != GetContainer().end()) {
    // skip packets that stayed in the queue for too long. They will be
    // actually removed from the queue by the next call to a non-const method
    if (Simulator::Now() <= (*it)->GetTimeStamp() + m_maxDelay) {
      if (((*it)->GetHeader().IsData() || (*it)->GetHeader().IsQosData()) &&
          (*it)->GetDestinationAddress() == dest) {
        return it;
      }
    } else {
      // signal the presence of expired packets
      m_expiredPacketsPresent = true;
    }
    it++;
  }
  NS_LOG_DEBUG("The queue is empty");
  return GetContainer().end();
}

WigigMacQueue::ConstIterator WigigMacQueue::PeekByTid(uint8_t tid,
                                                      ConstIterator pos) const {
  NS_LOG_FUNCTION(this << +tid);
  ConstIterator it = (pos != EMPTY ? pos : GetContainer().begin());
  while (it != GetContainer().end()) {
    // skip packets that stayed in the queue for too long. They will be
    // actually removed from the queue by the next call to a non-const method
    if (Simulator::Now() <= (*it)->GetTimeStamp() + m_maxDelay) {
      if ((*it)->GetHeader().IsQosData() &&
          (*it)->GetHeader().GetQosTid() == tid) {
        return it;
      }
    } else {
      // signal the presence of expired packets
      m_expiredPacketsPresent = true;
    }
    it++;
  }
  NS_LOG_DEBUG("The queue is empty");
  return GetContainer().end();
}

WigigMacQueue::ConstIterator
WigigMacQueue::PeekByTidAndAddress(uint8_t tid, Mac48Address dest,
                                   ConstIterator pos) const {
  NS_LOG_FUNCTION(this << +tid << dest);
  ConstIterator it = (pos != EMPTY ? pos : GetContainer().begin());
  while (it != GetContainer().end()) {
    // skip packets that stayed in the queue for too long. They will be
    // actually removed from the queue by the next call to a non-const method
    if (Simulator::Now() <= (*it)->GetTimeStamp() + m_maxDelay) {
      if ((*it)->GetHeader().IsQosData() &&
          (*it)->GetDestinationAddress() == dest &&
          (*it)->GetHeader().GetQosTid() == tid) {
        return it;
      }
    } else {
      // signal the presence of expired packets
      m_expiredPacketsPresent = true;
    }
    it++;
  }
  NS_LOG_DEBUG("The queue is empty");
  return GetContainer().end();
}

WigigMacQueue::ConstIterator WigigMacQueue::PeekFirstAvailable(
    const Ptr<QosBlockedDestinations> blockedPackets, ConstIterator pos) const {
  NS_LOG_FUNCTION(this);
  ConstIterator it = (pos != EMPTY ? pos : GetContainer().begin());
  while (it != GetContainer().end()) {
    // skip packets that stayed in the queue for too long. They will be
    // actually removed from the queue by the next call to a non-const method
    if (Simulator::Now() <= (*it)->GetTimeStamp() + m_maxDelay) {
      if (!(*it)->GetHeader().IsQosData() || !blockedPackets ||
          !blockedPackets->IsBlocked((*it)->GetHeader().GetAddr1(),
                                     (*it)->GetHeader().GetQosTid())) {
        return it;
      }
    } else {
      // signal the presence of expired packets
      m_expiredPacketsPresent = true;
    }
    it++;
  }
  NS_LOG_DEBUG("The queue is empty");
  return GetContainer().end();
}

Ptr<WigigMacQueueItem> WigigMacQueue::Remove() {
  NS_LOG_FUNCTION(this);

  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      return DoRemove(it);
    }
  }
  NS_LOG_DEBUG("The queue is empty");
  return nullptr;
}

bool WigigMacQueue::Remove(Ptr<const Packet> packet) {
  NS_LOG_FUNCTION(this << packet);
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      if ((*it)->GetPacket() == packet) {
        DoRemove(it);
        return true;
      }

      it++;
    }
  }
  NS_LOG_DEBUG("Packet " << packet << " not found in the queue");
  return false;
}

WigigMacQueue::ConstIterator WigigMacQueue::Remove(ConstIterator pos,
                                                   bool removeExpired) {
  NS_LOG_FUNCTION(this);

  if (!removeExpired) {
    ConstIterator curr = pos++;
    DoRemove(curr);
    return pos;
  }

  // remove stale items queued before the given position
  ConstIterator it = GetContainer().begin();
  while (it != GetContainer().end()) {
    if (it == pos) {
      // reset the flag signaling the presence of expired packets before
      // returning
      m_expiredPacketsPresent = false;

      ConstIterator curr = pos++;
      DoRemove(curr);
      return pos;
    } else if (!TtlExceeded(it)) {
      it++;
    }
  }
  NS_LOG_DEBUG("Invalid iterator");
  return GetContainer().end();
}

uint32_t WigigMacQueue::GetNPacketsByAddress(Mac48Address dest) {
  NS_LOG_FUNCTION(this << dest);

  uint32_t nPackets = 0;

  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      if ((*it)->GetHeader().IsData() &&
          (*it)->GetDestinationAddress() == dest) {
        nPackets++;
      }

      it++;
    }
  }
  NS_LOG_DEBUG("returns " << nPackets);
  return nPackets;
}

uint32_t WigigMacQueue::GetNPacketsByTidAndAddress(uint8_t tid,
                                                   Mac48Address dest) {
  NS_LOG_FUNCTION(this << dest);
  uint32_t nPackets = 0;
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      if ((*it)->GetHeader().IsQosData() &&
          (*it)->GetDestinationAddress() == dest &&
          (*it)->GetHeader().GetQosTid() == tid) {
        nPackets++;
      }

      it++;
    }
  }
  NS_LOG_DEBUG("returns " << nPackets);
  return nPackets;
}

bool WigigMacQueue::IsEmpty() {
  NS_LOG_FUNCTION(this);
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      NS_LOG_DEBUG("returns false");
      return false;
    }
  }
  NS_LOG_DEBUG("returns true");
  return true;
}

uint32_t WigigMacQueue::GetNPackets() {
  NS_LOG_FUNCTION(this);
  // remove packets that stayed in the queue for too long
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      it++;
    }
  }
  return QueueBase::GetNPackets();
}

uint32_t WigigMacQueue::GetNBytes() {
  NS_LOG_FUNCTION(this);
  // remove packets that stayed in the queue for too long
  for (ConstIterator it = GetContainer().begin(); it != GetContainer().end();) {
    if (!TtlExceeded(it)) {
      it++;
    }
  }
  return QueueBase::GetNBytes();
}

} // namespace ns3
