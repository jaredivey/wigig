/*
 * Copyright (c) 2015-2019 IMDEA Networks Insitute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_PHY_H
#define WIGIG_PHY_H

#include "wigig-data-types.h"
#include "wigig-interference-helper.h"
#include "wigig-phy-state-helper.h"

#include "ns3/deprecated.h"
#include "ns3/error-model.h"
#include "ns3/event-id.h"
#include "ns3/phy-entity.h"
#include "ns3/wifi-mode.h"
#include "ns3/wifi-mpdu-type.h"
#include "ns3/wifi-standards.h"

namespace ns3 {

class Channel;
class NetDevice;
class MobilityModel;
class PreambleDetectionModel;
class WifiRadioEnergyModel;
class UniformRandomVariable;
class WigigPhyStateHelper;
class WigigFrameCaptureModel;
class WigigPsdu;
class WigigPpdu;
class Codebook;
class WigigChannel;

enum PlcpFieldType {
  PLCP_80211AD_STF = 0,
  PLCP_80211AD_CEF = 1,
  PLCP_80211AD_HDR = 2,
  PLCP_80211AD_DATA = 3,
  PLCP_80211AD_PREAMBLE_HDR_DATA = 4,
  PLCP_80211AD_AGC_SF = 5,
  PLCP_80211AD_TRN_CE_SF = 6,
  PLCP_80211AD_TRN_SF = 7
};

/**
 * \brief 802.11ad PHY layer model
 * \ingroup wigig
 */
class WigigPhy : public Object {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  WigigPhy();
  ~WigigPhy() override;

  /**
   * Return the WigigPhyStateHelper of this PHY
   *
   * \return the WigigPhyStateHelper of this PHY
   */
  Ptr<WigigPhyStateHelper> GetState() const;

  /**
   * \param callback the callback to invoke
   *        upon successful packet reception.
   */
  void SetReceiveOkCallback(WigigPhyStateHelper::RxOkCallback callback);
  /**
   * \param callback the callback to invoke
   *        upon erroneous packet reception.
   */
  void SetReceiveErrorCallback(WigigPhyStateHelper::RxErrorCallback callback);

  /**
   * \param listener the new listener
   *
   * Add the input listener to the list of objects to be notified of
   * PHY-level events.
   */
  void RegisterListener(WifiPhyListener *listener);
  /**
   * \param listener the listener to be unregistered
   *
   * Remove the input listener from the list of objects to be notified of
   * PHY-level events.
   */
  void UnregisterListener(WifiPhyListener *listener);
  /**
   * Set the WigigChannel this WigigPhy is to be connected to.
   * \param channel the WigigChannel this WigigPhy is to be connected to
   */
  void SetChannel(const Ptr<WigigChannel> channel);
  /**
   * This method is called at initialization to specify whether the node is an
   * AP or not \param ap True if the node is an AP, false otherwise.
   */
  void SetIsAp(bool ap);
  /**
   * Get the delay until the PHY layer ends the reception of a packet.
   * \return The time until the end of the reception (0 if not currently
   * receiving).
   */
  Time GetDelayUntilEndRx();

  /**
   * A pair of a ChannelNumber and WifiStandard
   */
  typedef std::pair<uint8_t, WifiStandard> ChannelNumberStandardPair;
  /**
   * A pair of a center Frequency (MHz) and a ChannelWidth (MHz)
   */
  typedef std::pair<uint16_t, uint16_t> FrequencyWidthPair;

  /**
   * Get pointer to the current DMG Wifi Channel.
   * \return A pointer to the current DMG Wifi Channel.
   */
  virtual Ptr<Channel> GetChannel() const;

  /**
   * The WigigPhy::GetNModes() and WigigPhy::GetMode() methods are used
   * (e.g., by a WigigRemoteStationManager) to determine the set of
   * transmission/reception modes that this WigigPhy(-derived class)
   * can support - a set of WifiMode objects which we call the
   * DeviceRateSet, and which is stored as WigigPhy::m_deviceRateSet.
   *
   * It is important to note that the DeviceRateSet is a superset (not
   * necessarily proper) of the OperationalRateSet (which is
   * logically, if not actually, a property of the associated
   * WigigRemoteStationManager), which itself is a superset (again, not
   * necessarily proper) of the BSSBasicRateSet.
   *
   * \return the number of transmission modes supported by this PHY.
   *
   * \sa WigigPhy::GetMode()
   */
  uint8_t GetNModes() const;
  /**
   * The WigigPhy::GetNModes() and WigigPhy::GetMode() methods are used
   * (e.g., by a WigigRemoteStationManager) to determine the set of
   * transmission/reception modes that this WigigPhy(-derived class)
   * can support - a set of WifiMode objects which we call the
   * DeviceRateSet, and which is stored as WigigPhy::m_deviceRateSet.
   *
   * It is important to note that the DeviceRateSet is a superset (not
   * necessarily proper) of the OperationalRateSet (which is
   * logically, if not actually, a property of the associated
   * WigigRemoteStationManager), which itself is a superset (again, not
   * necessarily proper) of the BSSBasicRateSet.
   *
   * \param mode index in array of supported modes
   *
   * \return the mode whose index is specified.
   *
   * \sa WigigPhy::GetNModes()
   */
  WifiMode GetMode(uint8_t mode) const;
  /**
   * Check if the given WifiMode is supported by the PHY.
   *
   * \param mode the wifi mode to check
   *
   * \return true if the given mode is supported,
   *         false otherwise
   */
  bool IsModeSupported(WifiMode mode) const;

  /**
   * \param txVector the transmission vector
   * \param ber the probability of bit error rate
   *
   * \return the minimum SNR which is required to achieve
   *          the requested BER for the specified transmission vector. (W/W)
   */
  double CalculateSnr(const WigigTxVector &txVector, double ber) const;

  /**
   * \brief Set channel number.
   *
   * Channel center frequency = Channel starting frequency + 5 MHz * (nch - 1)
   *
   * where Starting channel frequency is standard-dependent,
   * as defined in (Section 18.3.8.4.2 "Channel numbering"; IEEE Std
   * 802.11-2012). This method may fail to take action if the PHY model
   * determines that the channel number cannot be switched for some reason (e.g.
   * sleep state)
   *
   * \param id the channel number
   */
  virtual void SetChannelNumber(uint8_t id);
  /**
   * Return current channel number.
   *
   * \return the current channel number
   */
  uint8_t GetChannelNumber() const;

  /**
   * \return the required time for channel switch operation of this WigigPhy
   */
  Time GetChannelSwitchDelay() const;

  /**
   * Configure the PHY-level parameters for different Wi-Fi standard.
   *
   * \param standard the Wi-Fi standard
   */
  virtual void ConfigureStandard(WifiStandard standard);
  /**
   * Configure WigigPhy with appropriate channel frequency and
   * supported rates for 802.11ad standard.
   */
  void Configure80211ad();
  /**
   * Get the configured Wi-Fi standard
   *
   * \return the Wi-Fi standard that has been configured
   */
  WifiStandard GetStandard() const;

  /**
   * Set Codebook
   * \param codebook The codebook for antennas and sectors patterns.
   */
  void SetCodebook(Ptr<Codebook> codebook);
  /**
   * Get Codebook
   * \return A pointer to the current codebook.
   */
  Ptr<Codebook> GetCodebook() const;
  /**
   * Set whether the DMG STA supports OFDM PHY layer.
   * \param value
   */
  void SetSupportOfdmPhy(bool value);
  /**
   * Get whether the DMG STA supports OFDM PHY layer.
   * \return True if the DMG STA supports OFDM PHY layer; otherwise false.
   */
  bool GetSupportOfdmPhy() const;
  /**
   * Set whether the DMG STA supports LP-SC PHY layer.
   * \param value
   */
  void SetSupportLpScPhy(bool value);
  /**
   * Get whether the DMG STA supports LP-SC PHY layer.
   * \return True if the DMG STA supports LP-SC PHY layer; otherwise false.
   */
  bool GetSupportLpScPhy() const;
  /**
   * Get the maximum supported MCS for transmission by SC PHY.
   * \return
   */
  uint8_t GetMaxScTxMcs() const;
  /**
   * Get the maximum supported MCS for reception by SC PHY.
   * \return
   */
  uint8_t GetMaxScRxMcs() const;
  /**
   * Get the maximum supported MCS for transmission by OFDM PHY.
   * \return
   */
  uint8_t GetMaxOfdmTxMcs() const;
  /**
   * Get the maximum supported MCS for reception by OFDM PHY.
   * \return
   */
  uint8_t GetMaxOfdmRxMcs() const;

  /**
   * End current allocation period.
   */
  void EndAllocationPeriod();
  /**
   * Typedef for SNR Callback.
   */
  typedef Callback<void, AntennaId, SectorId, uint8_t, uint8_t, uint8_t, double,
                   bool>
      ReportSnrCallback;
  /**
   * Register Report SNR Callback.
   * \param callback The SNR callback.
   */
  void RegisterReportSnrCallback(ReportSnrCallback callback);

  /**
   * \param size the number of bytes in the packet to send
   * \param txVector the TXVECTOR used for the transmission of this packet
   * \param frequency the channel center frequency (MHz)
   *
   * \return the total amount of time this PHY will stay busy for the
   * transmission of these bytes.
   */
  static Time CalculateTxDuration(uint32_t size, const WigigTxVector &txVector,
                                  uint16_t frequency);
  /**
   * \param txVector the transmission parameters used for this packet
   *
   * \return the total amount of time this PHY will stay busy for the
   * transmission of the PHY preamble and PHY header.
   */
  static Time
  CalculatePhyPreambleAndHeaderDuration(const WigigTxVector &txVector);
  /**
   *
   * \return the preamble detection duration, which is the time correlation
   * needs to detect the start of an incoming frame.
   */
  virtual Time GetPreambleDetectionDuration();
  /**
   * \param txVector the transmission parameters used for this packet
   *
   * \return the WifiMode used for the transmission of the PHY header
   */
  static WifiMode GetPhyHeaderMode(const WigigTxVector &txVector);
  /**
   * \param txVector the transmission parameters used for this packet
   *
   * \return the duration of the PHY header
   */
  static Time GetPhyHeaderDuration(const WigigTxVector &txVector);
  /**
   * \param txVector the transmission parameters used for this packet
   *
   * \return the duration of the PHY preamble
   */
  static Time GetPhyPreambleDuration(const WigigTxVector &txVector);
  /**
   * \param size the number of bytes in the packet to send
   * \param txVector the TXVECTOR used for the transmission of this packet
   * \param frequency the channel center frequency (MHz)
   * \param mpdutype the type of the MPDU as defined in WigigPhy::MpduType.
   *
   * \return the duration of the payload
   */
  static Time GetPayloadDuration(uint32_t size, const WigigTxVector &txVector,
                                 uint16_t frequency,
                                 MpduType mpdutype = NORMAL_MPDU);
  /**
   * \param size the number of bytes in the packet to send
   * \param txVector the TXVECTOR used for the transmission of this packet
   * \param frequency the channel center frequency (MHz)
   * \param mpdutype the type of the MPDU as defined in WigigPhy::MpduType.
   * \param incFlag this flag is used to indicate that the variables need to be
   * update or not This function is called a couple of times for the same packet
   * so variables should not be increased each time. \param totalAmpduSize the
   * total size of the previously transmitted MPDUs for the concerned A-MPDU. If
   * incFlag is set, this parameter will be updated. \param totalAmpduNumSymbols
   * the number of symbols previously transmitted for the MPDUs in the concerned
   * A-MPDU, used for the computation of the number of symbols needed for the
   * last MPDU. If incFlag is set, this parameter will be updated.
   *
   * \return the duration of the payload
   */
  static Time GetPayloadDuration(uint32_t size, const WigigTxVector &txVector,
                                 uint16_t frequency, MpduType mpdutype,
                                 bool incFlag, uint32_t &totalAmpduSize,
                                 double &totalAmpduNumSymbols);
  /**
   * \return the duration of the last received packet.
   */
  Time GetLastRxDuration() const;
  /**
   * \return the duration of the last transmitted packet.
   */
  Time GetLastTxDuration() const;

  /**
   * Get TRN field duration.
   * \param txVector
   * \return TRN field duration.
   */
  Time GetTrnFieldDuration(const WigigTxVector &txVector);
  /**
   * Start receiving the PHY preamble of a PPDU (i.e. the first bit of the
   * preamble has arrived).
   *
   * \param ppdu the arriving PPDU
   * \param rxPowerList a list of receive power in W between each pair of active
   * Tx and Rx antennas (has size 1 for SISO, > 1 for MIMO)
   */
  void StartReceivePreamble(Ptr<WigigPpdu> ppdu,
                            std::vector<double> rxPowerList);

  /**
   * Start receiving the PHY header of a PPDU (i.e. after the end of receiving
   * the preamble).
   *
   * \param event the event holding incoming PPDU's information
   */
  virtual void StartReceiveHeader(Ptr<WigigSignalEvent> event);

  /**
   * Continue receiving the PHY header of a PPDU (i.e. after the end of
   * receiving the non-HT header part).
   *
   * \param event the event holding incoming PPDU's information
   */
  virtual void ContinueReceiveHeader(Ptr<WigigSignalEvent> event);

  /**
   * Start receiving the PSDU (i.e. the first symbol of the PSDU has arrived).
   *
   * \param event the event holding incoming PPDU's information
   */
  void StartReceivePayload(Ptr<WigigSignalEvent> event);

  /**
   * The last symbol of the PPDU has arrived.
   *
   * \param event the corresponding event of the first time the packet arrives
   * (also storing packet and TxVector information)
   */
  void EndReceive(Ptr<WigigSignalEvent> event);
  /**
   * \param psdu the PSDU to send
   * \param txVector the TXVECTOR that has TX parameters such as mode, the
   * transmission mode to use to send this PSDU, and txPowerLevel, a power level
   * to use to send the whole PPDU. The real transmission power is calculated as
   * txPowerMin + txPowerLevel * (txPowerMax - txPowerMin) / nTxLevels
   */
  virtual void Send(Ptr<const WigigPsdu> psdu, const WigigTxVector &txVector);

  /**
   * \param ppdu the PPDU to send
   */
  virtual void StartTx(Ptr<WigigPpdu> ppdu);

  /**
   * \return true of the current state of the PHY layer is WigigPhy::IDLE, false
   * otherwise.
   */
  bool IsStateIdle() const;
  /**
   * \return true of the current state of the PHY layer is WigigPhy::CCA_BUSY,
   * false otherwise.
   */
  bool IsStateCcaBusy() const;
  /**
   * \return true of the current state of the PHY layer is WigigPhy::RX, false
   * otherwise.
   */
  bool IsStateRx() const;
  /**
   * \return true of the current state of the PHY layer is WigigPhy::TX, false
   * otherwise.
   */
  bool IsStateTx() const;
  /**
   * \return true of the current state of the PHY layer is WigigPhy::SWITCHING,
   * false otherwise.
   */
  bool IsStateSwitching() const;
  /**
   * \return true if the current state of the PHY layer is WigigPhy::SLEEP,
   * false otherwise.
   */
  bool IsStateSleep() const;
  /**
   * \return true if the current state of the PHY layer is WigigPhy::OFF, false
   * otherwise.
   */
  bool IsStateOff() const;
  /**
   * \return the current state of the PHY layer.
   */
  WifiPhyState GetPhyState() const;

  /**
   * \return the predicted delay until this PHY can become WigigPhy::IDLE.
   *
   * The PHY will never become WigigPhy::IDLE _before_ the delay returned by
   * this method but it could become really idle later.
   */
  Time GetDelayUntilIdle();

  /**
   * Return the start time of the last received packet.
   *
   * \return the start time of the last received packet
   */
  Time GetLastRxStartTime() const;
  /**
   * Return the end time of the last received packet.
   *
   * \return the end time of the last received packet
   */
  Time GetLastRxEndTime() const;

  /**
   * Convert WifiCodeRate to a ratio, e.g., code ratio of WIFI_CODE_RATE_1_2 is
   * 0.5.
   *
   * \param codeRate the WifiCodeRate
   * \return the ratio form of WifiCodeRate.
   */
  static double GetCodeRatio(WifiCodeRate codeRate);
  static WifiCodeRate GetCodeRate(uint8_t mcsValue);
  static uint16_t GetConstellationSize(uint8_t mcsValue);
  static uint16_t GetUsableSubcarriers(WifiModulationClass modulationClass);
  static uint64_t CalculatePhyRate(WifiCodeRate codeRate, uint64_t dataRate);
  static uint64_t GetPhyRate(uint8_t mcsValue,
                             WifiModulationClass modulationClass,
                             uint16_t channelWidth, uint16_t guardInterval,
                             uint8_t nss);
  static uint64_t GetDataRate(uint8_t mcsValue,
                              WifiModulationClass modulationClass,
                              uint16_t channelWidth, uint16_t guardInterval,
                              uint8_t nss);
  static uint64_t CalculateDataRate(WifiModulationClass modulationClass,
                                    uint16_t numberOfBitsPerSubcarrier,
                                    double codingRate);
  static uint64_t GetPhyRateFromTxVector(const WifiTxVector &txVector,
                                         uint16_t staId);
  static uint64_t GetDataRateFromTxVector(const WifiTxVector &txVector,
                                          uint16_t staId);
  static bool IsAllowed(const WifiTxVector &txVector);
  static uint64_t GetNonHtReferenceRate();

  /**
   * Return the corresponding WifiMode for the given MCS index.
   * \param index The index of the MCS.
   * \return The corresponding WifiMode for the given MCS index.
   */
  static WifiMode GetDmgMcs(uint8_t index);
  /**
   * Return a WifiMode for Control PHY.
   *
   * \return a WifiMode for Control PHY.
   */
  static WifiMode GetDmgMcs0();
  /**
   * Return a WifiMode for SC PHY with MCS1.
   *
   * \return a WifiMode for SC PHY with MCS1.
   */
  static WifiMode GetDmgMcs1();
  /**
   * Return a WifiMode for SC PHY with MCS2.
   *
   * \return a WifiMode for SC PHY with MCS2.
   */
  static WifiMode GetDmgMcs2();
  /**
   * Return a WifiMode for SC PHY with MCS3.
   *
   * \return a WifiMode for SC PHY with MCS3.
   */
  static WifiMode GetDmgMcs3();
  /**
   * Return a WifiMode for SC PHY with MCS4.
   *
   * \return a WifiMode for SC PHY with MCS4.
   */
  static WifiMode GetDmgMcs4();
  /**
   * Return a WifiMode for SC PHY with MCS5.
   *
   * \return a WifiMode for SC PHY with MCS5.
   */
  static WifiMode GetDmgMcs5();
  /**
   * Return a WifiMode for SC PHY with MCS6.
   *
   * \return a WifiMode for SC PHY with MCS6.
   */
  static WifiMode GetDmgMcs6();
  /**
   * Return a WifiMode for SC PHY with MCS7.
   *
   * \return a WifiMode for SC PHY with MCS7.
   */
  static WifiMode GetDmgMcs7();
  /**
   * Return a WifiMode for SC PHY with MCS8.
   *
   * \return a WifiMode for SC PHY with MCS8.
   */
  static WifiMode GetDmgMcs8();
  /**
   * Return a WifiMode for SC PHY with MCS9.
   *
   * \return a WifiMode for SC PHY with MCS9.
   */
  static WifiMode GetDmgMcs9();
  /**
   * Return a WifiMode for SC PHY with MCS9.1 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS9.1 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs9_1();
  /**
   * Return a WifiMode for SC PHY with MCS10.
   *
   * \return a WifiMode for SC PHY with MCS10.
   */
  static WifiMode GetDmgMcs10();
  /**
   * Return a WifiMode for SC PHY with MCS11.
   *
   * \return a WifiMode for SC PHY with MCS11.
   */
  static WifiMode GetDmgMcs11();
  /**
   * Return a WifiMode for SC PHY with MCS12.
   *
   * \return a WifiMode for SC PHY with MCS12.
   */
  static WifiMode GetDmgMcs12();
  /**
   * Return a WifiMode for SC PHY with MCS12.1 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.1 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_1();
  /**
   * Return a WifiMode for SC PHY with MCS12.2 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.2 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_2();
  /**
   * Return a WifiMode for SC PHY with MCS12.3 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.3 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_3();
  /**
   * Return a WifiMode for SC PHY with MCS12.4 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.4 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_4();
  /**
   * Return a WifiMode for SC PHY with MCS12.5 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.5 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_5();
  /**
   * Return a WifiMode for SC PHY with MCS12.6 defined 802.11-2016.
   *
   * \return a WifiMode for SC PHY with MCS12.6 defined 802.11-2016.
   */
  static WifiMode GetDmgMcs12_6();
  /**
   * Return a WifiMode for OFDM PHY with MCS13.
   *
   * \return a WifiMode for OFDM PHY with MCS13.
   */
  static WifiMode GetDmgMcs13();
  /**
   * Return a WifiMode for OFDM PHY with MCS14.
   *
   * \return a WifiMode for OFDM PHY with MCS14.
   */
  static WifiMode GetDmgMcs14();
  /**
   * Return a WifiMode for OFDM PHY with MCS15.
   *
   * \return a WifiMode for OFDM PHY with MCS15.
   */
  static WifiMode GetDmgMcs15();
  /**
   * Return a WifiMode for OFDM PHY with MCS16.
   *
   * \return a WifiMode for OFDM PHY with MCS16.
   */
  static WifiMode GetDmgMcs16();
  /**
   * Return a WifiMode for OFDM PHY with MCS17.
   *
   * \return a WifiMode for OFDM PHY with MCS17.
   */
  static WifiMode GetDmgMcs17();
  /**
   * Return a WifiMode for OFDM PHY with MCS18.
   *
   * \return a WifiMode for OFDM PHY with MCS18.
   */
  static WifiMode GetDmgMcs18();
  /**
   * Return a WifiMode for OFDM PHY with MCS19.
   *
   * \return a WifiMode for OFDM PHY with MCS19.
   */
  static WifiMode GetDmgMcs19();
  /**
   * Return a WifiMode for OFDM PHY with MCS20.
   *
   * \return a WifiMode for OFDM PHY with MCS20.
   */
  static WifiMode GetDmgMcs20();
  /**
   * Return a WifiMode for OFDM PHY with MCS21.
   *
   * \return a WifiMode for OFDM PHY with MCS21.
   */
  static WifiMode GetDmgMcs21();
  /**
   * Return a WifiMode for OFDM PHY with MCS22.
   *
   * \return a WifiMode for OFDM PHY with MCS22.
   */
  static WifiMode GetDmgMcs22();
  /**
   * Return a WifiMode for OFDM PHY with MCS23.
   *
   * \return a WifiMode for OFDM PHY with MCS23.
   */
  static WifiMode GetDmgMcs23();
  /**
   * Return a WifiMode for OFDM PHY with MCS24.
   *
   * \return a WifiMode for OFDM PHY with MCS24.
   */
  static WifiMode GetDmgMcs24();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS25.
   *
   * \return a WifiMode for Low Power SC PHY with MCS25.
   */
  static WifiMode GetDmgMcs25();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS26.
   *
   * \return a WifiMode for Low Power SC PHY with MCS26.
   */
  static WifiMode GetDmgMcs26();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS27.
   *
   * \return a WifiMode for Low Power SC PHY with MCS27.
   */
  static WifiMode GetDmgMcs27();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS28.
   *
   * \return a WifiMode for Low Power SC PHY with MCS28.
   */
  static WifiMode GetDmgMcs28();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS29.
   *
   * \return a WifiMode for Low Power SC PHY with MCS29.
   */
  static WifiMode GetDmgMcs29();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS30.
   *
   * \return a WifiMode for Low Power SC PHY with MCS30.
   */
  static WifiMode GetDmgMcs30();
  /**
   * Return a WifiMode for Low Power SC PHY with MCS31.
   *
   * \return a WifiMode for Low Power SC PHY with MCS31.
   */
  static WifiMode GetDmgMcs31();

  /**
   * Public method used to fire a PhyTxBegin trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   * \param txPowerW the transmit power in Watts
   */
  void NotifyTxBegin(Ptr<const WigigPsdu> psdu, double txPowerW);
  /**
   * Public method used to fire a PhyTxEnd trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   */
  void NotifyTxEnd(Ptr<const WigigPsdu> psdu);
  /**
   * Public method used to fire a PhyTxDrop trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   */
  void NotifyTxDrop(Ptr<const WigigPsdu> psdu);
  /**
   * Public method used to fire a PhyRxBegin trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   */
  void NotifyRxBegin(Ptr<const WigigPsdu> psdu);
  /**
   * Public method used to fire a PhyRxEnd trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   */
  void NotifyRxEnd(Ptr<const WigigPsdu> psdu);
  /**
   * Public method used to fire a PhyRxDrop trace.
   * Implemented for encapsulation purposes.
   *
   * \param psdu the PSDU being transmitted
   * \param reason the reason the packet was dropped
   */
  void NotifyRxDrop(Ptr<const WigigPsdu> psdu, WifiPhyRxfailureReason reason);

  /**
   * Public method used to fire a MonitorSniffer trace for a wifi PSDU being
   * received. Implemented for encapsulation purposes. This method will extract
   * all MPDUs if packet is an A-MPDU and will fire tracedCallback. The A-MPDU
   * reference number (RX side) is set within the method. It must be a different
   * value for each A-MPDU but the same for each subframe within one A-MPDU.
   *
   * \param psdu the PSDU being received
   * \param channelFreqMhz the frequency in MHz at which the packet is
   *        received. Note that in real devices this is normally the
   *        frequency to which  the receiver is tuned, and this can be
   *        different than the frequency at which the packet was originally
   *        transmitted. This is because it is possible to have the receiver
   *        tuned on a given channel and still to be able to receive packets
   *        on a nearby channel.
   * \param txVector the TXVECTOR that holds RX parameters
   * \param signalNoise signal power and noise power in dBm (noise power
   * includes the noise figure) \param statusPerMpdu reception status per MPDU
   */
  void NotifyMonitorSniffRx(Ptr<const WigigPsdu> psdu, uint16_t channelFreqMhz,
                            const WigigTxVector &txVector,
                            SignalNoiseDbm signalNoise,
                            std::vector<bool> statusPerMpdu);

  /**
   * Public method used to fire a MonitorSniffer trace for a wifi PSDU being
   * transmitted. Implemented for encapsulation purposes. This method will
   * extract all MPDUs if packet is an A-MPDU and will fire tracedCallback. The
   * A-MPDU reference number (RX side) is set within the method. It must be a
   * different value for each A-MPDU but the same for each subframe within one
   * A-MPDU.
   *
   * \param psdu the PSDU being received
   * \param channelFreqMhz the frequency in MHz at which the packet is
   *        transmitted.
   * \param txVector the TXVECTOR that holds TX parameters
   */
  void NotifyMonitorSniffTx(Ptr<const WigigPsdu> psdu, uint16_t channelFreqMhz,
                            const WigigTxVector &txVector);

  /**
   * TracedCallback signature for PSDU transmit events.
   *
   * \param psdu the PSDU being transmitted
   * \param txVector the TXVECTOR holding the TX parameters
   * \param txPowerW the transmit power in Watts
   */
  typedef void (*PsduTxBeginCallback)(Ptr<const WigigPsdu> psdu,
                                      const WigigTxVector &txVector,
                                      double txPowerW);
  /**
   * TracedCallback signature for start of PSDU reception events.
   *
   * \param txVector the TXVECTOR decoded from the PHY header
   * \param psduDuration the duration of the PSDU
   */
  typedef void (*PhyRxPayloadBeginTracedCallback)(const WigigTxVector &txVector,
                                                  Time psduDuration);

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by this model. Return the number of streams (possibly zero) that
   * have been assigned.
   *
   * \param stream first stream index to use
   * \return the number of stream indices assigned by this model
   */
  virtual int64_t AssignStreams(int64_t stream);

  /**
   * Sets the energy detection threshold (dBm).
   * The energy of a received signal should be higher than
   * this threshold (dBm) to allow the PHY layer to detect the signal.
   *
   * \param threshold the energy detection threshold in dBm
   *
   * \deprecated
   */
  void SetEdThreshold(double threshold);
  /**
   * Sets the receive sensitivity threshold (dBm).
   * The energy of a received signal should be higher than
   * this threshold to allow the PHY layer to detect the signal.
   *
   * \param threshold the receive sensitivity threshold in dBm
   */
  void SetRxSensitivity(double threshold);
  /**
   * Return the receive sensitivity threshold (dBm).
   *
   * \return the receive sensitivity threshold in dBm
   */
  double GetRxSensitivity() const;
  /**
   * Sets the CCA threshold (dBm). The energy of a received signal
   * should be higher than this threshold to allow the PHY
   * layer to declare CCA BUSY state.
   *
   * \param threshold the CCA threshold in dBm
   */
  void SetCcaEdThreshold(double threshold);
  /**
   * Return the CCA threshold (dBm).
   *
   * \return the CCA threshold in dBm
   */
  double GetCcaEdThreshold() const;
  /**
   * Sets the RX loss (dB) in the Signal-to-Noise-Ratio due to non-idealities in
   * the receiver.
   *
   * \param noiseFigureDb noise figure in dB
   */
  void SetRxNoiseFigure(double noiseFigureDb);
  /**
   * Return the RX noise figure (dBm).
   *
   * \return the RX noise figure in dBm
   */
  double GetRxNoiseFigure() const;
  /**
   * Sets the minimum available transmission power level (dBm).
   *
   * \param start the minimum transmission power level (dBm)
   */
  void SetTxPowerStart(double start);
  /**
   * Return the minimum available transmission power level (dBm).
   *
   * \return the minimum available transmission power level (dBm)
   */
  double GetTxPowerStart() const;
  /**
   * Sets the maximum available transmission power level (dBm).
   *
   * \param end the maximum transmission power level (dBm)
   */
  void SetTxPowerEnd(double end);
  /**
   * Return the maximum available transmission power level (dBm).
   *
   * \return the maximum available transmission power level (dBm)
   */
  double GetTxPowerEnd() const;
  /**
   * Sets the number of transmission power levels available between the
   * minimum level and the maximum level. Transmission power levels are
   * equally separated (in dBm) with the minimum and the maximum included.
   *
   * \param n the number of available levels
   */
  void SetNTxPower(uint8_t n);
  /**
   * Return the number of available transmission power levels.
   *
   * \return the number of available transmission power levels
   */
  uint8_t GetNTxPower() const;
  /**
   * Sets the transmission gain (dB).
   *
   * \param gain the transmission gain in dB
   */
  void SetTxGain(double gain);
  /**
   * Return the transmission gain (dB).
   *
   * \return the transmission gain in dB
   */
  double GetTxGain() const;
  /**
   * Sets the reception gain (dB).
   *
   * \param gain the reception gain in dB
   */
  void SetRxGain(double gain);
  /**
   * Return the reception gain (dB).
   *
   * \return the reception gain in dB
   */
  double GetRxGain() const;

  /**
   * Sets the device this PHY is associated with.
   *
   * \param device the device this PHY is associated with
   */
  void SetDevice(const Ptr<NetDevice> device);
  /**
   * Return the device this PHY is associated with
   *
   * \return the device this PHY is associated with
   */
  Ptr<NetDevice> GetDevice() const;
  /**
   * \brief assign a mobility model to this device
   *
   * This method allows a user to specify a mobility model that should be
   * associated with this physical layer.  Calling this method is optional
   * and only necessary if the user wants to override the mobility model
   * that is aggregated to the node.
   *
   * \param mobility the mobility model this PHY is associated with
   */
  void SetMobility(const Ptr<MobilityModel> mobility);
  /**
   * Return the mobility model this PHY is associated with.
   *
   * \return the mobility model this PHY is associated with
   */
  Ptr<MobilityModel> GetMobility() const;

  /**
   * \param freq the operating center frequency (MHz) on this node.
   */
  virtual void SetFrequency(uint16_t freq);
  /**
   * \return the operating center frequency (MHz)
   */
  uint16_t GetFrequency() const;
  /**
   * \param antennas the number of antennas on this node.
   */
  void SetNumberOfAntennas(uint8_t antennas);
  /**
   * \return the number of antennas on this device
   */
  uint8_t GetNumberOfAntennas() const;
  /**
   * \param streams the maximum number of supported TX spatial streams.
   */
  void SetMaxSupportedTxSpatialStreams(uint8_t streams);
  /**
   * \return the maximum number of supported TX spatial streams
   */
  uint8_t GetMaxSupportedTxSpatialStreams() const;
  /**
   * \param streams the maximum number of supported RX spatial streams.
   */
  void SetMaxSupportedRxSpatialStreams(uint8_t streams);
  /**
   * \return the maximum number of supported RX spatial streams
   */
  uint8_t GetMaxSupportedRxSpatialStreams() const;
  /**
   * Sets the error rate model.
   *
   * \param rate the error rate model
   */
  void SetErrorRateModel(const Ptr<ErrorRateModel> rate);
  /**
   * Attach a receive ErrorModel to the WigigPhy.
   *
   * The WigigPhy may optionally include an ErrorModel in
   * the packet receive chain. The error model is additive
   * to any modulation-based error model based on SNR, and
   * is typically used to force specific packet losses or
   * for testing purposes.
   *
   * \param em Pointer to the ErrorModel.
   */
  void SetPostReceptionErrorModel(const Ptr<ErrorModel> em);
  /**
   * Sets the frame capture model.
   *
   * \param frameCaptureModel the frame capture model
   */
  void
  SetFrameCaptureModel(const Ptr<WigigFrameCaptureModel> frameCaptureModel);
  /**
   * Sets the preamble detection model.
   *
   * \param preambleDetectionModel the preamble detection model
   */
  void SetPreambleDetectionModel(
      const Ptr<PreambleDetectionModel> preambleDetectionModel);
  /**
   * Sets the wifi radio energy model.
   *
   * \param wifiRadioEnergyModel the wifi radio energy model
   */
  void
  SetWifiRadioEnergyModel(const Ptr<WifiRadioEnergyModel> wifiRadioEnergyModel);

  /**
   * \return the channel width in MHz
   */
  uint16_t GetChannelWidth() const;
  /**
   * \param channelWidth the channel width (in MHz)
   */
  virtual void SetChannelWidth(uint16_t channelWidth);

  /**
   * Get the power of the given power level in dBm.
   * In SpectrumWigigPhy implementation, the power levels are equally spaced (in
   * dBm).
   *
   * \param power the power level
   *
   * \return the transmission power in dBm at the given power level
   */
  double GetPowerDbm(uint8_t power) const;

  /**
   * Compute the transmit power (in dBm) for the next transmission.
   *
   * \param txVector the TXVECTOR
   * \return the transmit power in dBm for the next transmission
   */
  double GetTxPowerForTransmission(const WigigTxVector &txVector) const;

  /**
   * Start receiving IEEE 802.11ad AGC Subfield.
   * \param txVector
   * \param rxPowerDbm The received power in dBm.
   */
  void StartReceiveAgcSubfield(const WigigTxVector &txVector,
                               double rxPowerDbm);
  /**
   * Start receiving CE Subfield.
   * \param txVector
   * \param rxPowerDbm The received power in dBm.
   */
  void StartReceiveCeSubfield(const WigigTxVector &txVector, double rxPowerDbm);
  /**
   * Start receiving TRN Subfield.
   * \param txVector
   * \param rxPowerDbm The received power in dBm.
   */
  void StartReceiveTrnSubfield(const WigigTxVector &txVector,
                               double rxPowerDbm);

protected:
  void DoInitialize() override;
  void DoDispose() override;

  /**
   * The default implementation does nothing and returns true.  This method
   * is typically called internally by SetChannelNumber ().
   *
   * \brief Perform any actions necessary when user changes channel number
   * \param id channel number to try to switch to
   * \return true if WigigPhy can actually change the number; false if not
   * \see SetChannelNumber
   */
  bool DoChannelSwitch(uint8_t id);
  /**
   * The default implementation does nothing and returns true.  This method
   * is typically called internally by SetFrequency ().
   *
   * \brief Perform any actions necessary when user changes frequency
   * \param frequency frequency to try to switch to in MHz
   * \return true if WigigPhy can actually change the frequency; false if not
   * \see SetFrequency
   */
  bool DoFrequencySwitch(uint16_t frequency);

  /**
   * Check if PHY state should move to CCA busy state based on current
   * state of interference tracker.  In this model, CCA becomes busy when
   * the aggregation of all signals as tracked by the WigigInterferenceHelper
   * class is higher than the CcaEdThreshold
   */
  void SwitchMaybeToCcaBusy();
  /**
   * Due to newly arrived signal, the current reception cannot be continued and
   * has to be aborted \param reason the reason the reception is aborted
   *
   */
  void AbortCurrentReception(WifiPhyRxfailureReason reason);
  /**
   * Eventually switch to CCA busy
   */
  void MaybeCcaBusyDuration();

  /**
   * \brief post-construction setting of frequency and/or channel number
   *
   * This method exists to handle the fact that two attribute values,
   * Frequency and ChannelNumber, are coupled.  The initialization of
   * these values needs to be deferred until after attribute construction
   * time, to avoid static initialization order issues.  This method is
   * typically called either when ConfigureStandard () is called or when
   * DoInitialize () is called.
   */
  void InitializeFrequencyChannelNumber();
  /**
   * Configure the PHY-level parameters for different Wi-Fi standard.
   * This method is called when defaults for each standard must be
   * selected.
   *
   * \param standard the Wi-Fi standard
   */
  void ConfigureDefaultsForStandard(WifiStandard standard);
  /**
   * Configure the PHY-level parameters for different Wi-Fi standard.
   * This method is called when the Frequency or ChannelNumber attributes
   * are set by the user.  If the Frequency or ChannelNumber are valid for
   * the standard, they are used instead.
   *
   * \param standard the Wi-Fi standard
   */
  void ConfigureChannelForStandard(WifiStandard standard);

  /**
   * Look for channel number matching the frequency and width
   * \param frequency The center frequency to use in MHz
   * \param width The channel width to use in MHz
   * \return the channel number if found, zero if not
   */
  uint8_t FindChannelNumberForFrequencyWidth(uint16_t frequency,
                                             uint16_t width) const;
  /**
   * Lookup frequency/width pair for channelNumber/standard pair
   * \param channelNumber The channel number to check
   * \param standard The WifiStandard to check
   * \return the FrequencyWidthPair found
   */
  FrequencyWidthPair
  GetFrequencyWidthForChannelNumberStandard(uint8_t channelNumber,
                                            WifiStandard standard) const;

  /**
   * Start IEEE 802.11ad AGC Subfields transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  void StartAgcSubfieldsTx(const WigigTxVector &txVector);
  /**
   * Send IEEE 802.11ad AGC Subfield.
   * \param txVector TxVector companioned by this transmission.
   * \param fieldsRemaining The number of remaining AGC Subfields.
   */
  void SendAgcSubfield(const WigigTxVector &txVector, uint8_t fieldsRemaining);
  /**
   * Start IEEE 802.11ad AGC Subfield transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  virtual void StartAgcSubfieldTx(const WigigTxVector &txVector);
  /**
   * Start IEEE 802.11ad TRN Unit transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  void StartTrnUnitTx(const WigigTxVector &txVector);
  /**
   * Send IEEE 802.11ad Channel Estimation (CE) subfield.
   * \param txVector TxVector companioned by this transmission.
   */
  void SendCeSubfield(const WigigTxVector &txVector);
  /**
   * Start IEEE 802.11ad TRN-CE Subfield transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  virtual void StartCeSubfieldTx(const WigigTxVector &txVector);
  /**
   * Start IEEE 802.11ad TRN Subfields of the same TRN-Unit transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  void StartTrnSubfieldsTx(const WigigTxVector &txVector);
  /**
   * Send TRN Field to the destinated station.
   * \param txVector TxVector companioned by this transmission.
   */
  void SendTrnSubfield(const WigigTxVector &txVector);
  /**
   * Start TRN-SF transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  virtual void StartTrnSubfieldTx(const WigigTxVector &txVector);
  /**
   * End receiving TRN Subfield.
   * \param event The event related to the reception of this TRN Field.
   */
  void EndReceiveTrnSubfield(SectorId sectorId, AntennaId antennaId,
                             const WigigTxVector &txVector,
                             Ptr<WigigSignalEvent> event);
  /**
   * This method is called once all the TRN Units are received.
   */
  void EndReceiveTrnField();
  /**
   * Starting receiving the PPDU after having detected the medium is idle or
   * after a reception switch.
   *
   * \param event the event holding incoming PPDU's information
   * \param rxPowerW the receive power in W
   */
  void StartRxPreamble(Ptr<WigigSignalEvent> event, double rxPowerW);
  /**
   * Get the reception status for the provided MPDU and notify.
   *
   * \param psdu the arriving MPDU formatted as a PSDU
   * \param event the event holding incoming PPDU's information
   * \param relativeMpduStart the relative start time of the MPDU within the
   * A-MPDU. 0 for normal MPDUs \param mpduDuration the duration of the MPDU
   *
   * \return information on MPDU reception: status, signal power (dBm), and
   * noise power (in dBm)
   */
  std::pair<bool, SignalNoiseDbm>
  GetReceptionStatus(Ptr<const WigigPsdu> psdu, Ptr<WigigSignalEvent> event,
                     Time relativeMpduStart, Time mpduDuration);

  WigigInterferenceHelper
      m_interference; //!< Pointer to WigigInterferenceHelper

private:
  /**
   * Special function for AGC-Rx Subfields Reception.
   * \param remainingRxFields The number of remaining AGC RX Subfields
   */
  void PrepareForAgcRxReception(uint8_t remainingAgcRxSubields);

  Ptr<UniformRandomVariable> m_random; //!< Provides uniform random variables.
  Ptr<WigigPhyStateHelper> m_state;    //!< Pointer to WigigPhyStateHelper

  uint32_t m_txMpduReferenceNumber; //!< A-MPDU reference number to identify all
                                    //!< transmitted subframes belonging to the
                                    //!< same received A-MPDU
  uint32_t m_rxMpduReferenceNumber; //!< A-MPDU reference number to identify all
                                    //!< received subframes belonging to the
                                    //!< same received A-MPDU

  EventId m_endRxEvent;                //!< the end of receive event
  EventId m_endPhyRxEvent;             //!< the end of PHY receive event
  EventId m_endPreambleDetectionEvent; //!< the end of preamble detection event

  EventId m_endTxEvent; //!< the end of transmit event

  Time m_rxDuration; //!< Duration of the last received packet.

  /**
   * This vector holds the set of transmission modes that this
   * WigigPhy(-derived class) can support. In conversation we call this
   * the DeviceRateSet (not a term you'll find in the standard), and
   * it is a superset of standard-defined parameters such as the
   * OperationalRateSet, and the BSSBasicRateSet (which, themselves,
   * have a superset/subset relationship).
   *
   * Mandatory rates relevant to this WigigPhy can be found by
   * iterating over this vector looking for WifiMode objects for which
   * WifiMode::IsMandatory() is true.
   *
   * A quick note is appropriate here (well, here is as good a place
   * as any I can find)...
   *
   * In the standard there is no text that explicitly precludes
   * production of a device that does not support some rates that are
   * mandatory (according to the standard) for PHYs that the device
   * happens to fully or partially support.
   *
   * This approach is taken by some devices which choose to only support,
   * for example, 6 and 9 Mbps ERP-OFDM rates for cost and power
   * consumption reasons (i.e., these devices don't need to be designed
   * for and waste current on the increased linearity requirement of
   * higher-order constellations when 6 and 9 Mbps more than meet their
   * data requirements). The wording of the standard allows such devices
   * to have an OperationalRateSet which includes 6 and 9 Mbps ERP-OFDM
   * rates, despite 12 and 24 Mbps being "mandatory" rates for the
   * ERP-OFDM PHY.
   *
   * Now this doesn't actually have any impact on code, yet. It is,
   * however, something that we need to keep in mind for the
   * future. Basically, the key point is that we can't be making
   * assumptions like "the Operational Rate Set will contain all the
   * mandatory rates".
   */
  WifiModeList m_deviceRateSet;

  Ptr<WigigSignalEvent> m_currentEvent;             //!< Hold the current event
  Ptr<WifiRadioEnergyModel> m_wifiRadioEnergyModel; //!< Wifi radio energy model
  Ptr<WigigFrameCaptureModel> m_frameCaptureModel;  //!< Frame capture model

  Time m_lastTxDuration;
  bool m_isConstructed; //!< true when ready to set frequency

  /**
   * The trace source fired when a packet begins the transmission process on
   * the medium.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>, double> m_phyTxBeginTrace;
  /**
   * The trace source fired when a PSDU begins the transmission process on
   * the medium.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const WigigPsdu>, WigigTxVector, double /* TX power (W) */>
      m_phyTxPsduBeginTrace;

  /**
   * The trace source fired when a packet ends the transmission process on
   * the medium.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_phyTxEndTrace;

  /**
   * The trace source fired when the PHY layer drops a packet as it tries
   * to transmit it.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_phyTxDropTrace;

  /**
   * The trace source fired when a packet begins the reception process from
   * the medium.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_phyRxBeginTrace;

  /**
   * The trace source fired when the reception of the PHY payload (PSDU) begins.
   *
   * This traced callback models the behavior of the PHY-RXSTART
   * primitive which is launched upon correct decoding of
   * the PHY header and support of modes within.
   * We thus assume that it is sent just before starting
   * the decoding of the payload, since it's there that
   * support of the header's content is checked. In addition,
   * it's also at that point that the correct decoding of
   * HT-SIG are checked.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<const WigigTxVector &, Time> m_phyRxPayloadBeginTrace;

  /**
   * The trace source fired when a packet ends the reception process from
   * the medium.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_phyRxEndTrace;

  /**
   * The trace source fired when the PHY layer drops a packet it has received.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>, WifiPhyRxfailureReason> m_phyRxDropTrace;

  /**
   * A trace source that emulates a Wi-Fi device in monitor mode
   * sniffing a packet being received.
   *
   * As a reference with the real world, firing this trace
   * corresponds in the madwifi driver to calling the function
   * ieee80211_input_monitor()
   *
   * \see class CallBackTraceSource
   * \todo WigigTxVector and signalNoiseDbm should be be passed as
   *       const references because of their sizes.
   */
  TracedCallback<Ptr<const Packet>, uint16_t, const WigigTxVector &, MpduInfo,
                 SignalNoiseDbm>
      m_phyMonitorSniffRxTrace;

  /**
   * A trace source that emulates a Wi-Fi device in monitor mode
   * sniffing a packet being transmitted.
   *
   * As a reference with the real world, firing this trace
   * corresponds in the madwifi driver to calling the function
   * ieee80211_input_monitor()
   *
   * \see class CallBackTraceSource
   * \todo WigigTxVector should be passed by const reference because
   * of its size.
   */
  TracedCallback<Ptr<const Packet>, uint16_t, const WigigTxVector &, MpduInfo>
      m_phyMonitorSniffTxTrace;

  WifiStandard m_standard;           //!< WifiStandard
  uint16_t m_channelCenterFrequency; //!< Center frequency in MHz
  uint16_t m_initialFrequency; //!< Store frequency until initialization (MHz)
  bool m_frequencyChannelNumberInitialized; //!< Store initialization state
  uint16_t m_channelWidth;                  //!< Channel width (MHz)

  double m_rxSensitivityW; //!< Receive sensitivity threshold in watts
  double
      m_ccaEdThresholdW; //!< Clear channel assessment (CCA) threshold in watts
  double m_txGainDb;     //!< Transmission gain (dB)
  double m_rxGainDb;     //!< Reception gain (dB)
  double m_txPowerBaseDbm; //!< Minimum transmission power (dBm)
  double m_txPowerEndDbm;  //!< Maximum transmission power (dBm)
  uint8_t m_nTxPower;      //!< Number of available transmission power levels

  uint8_t m_numberOfAntennas; //!< Number of transmitters
  uint8_t m_txSpatialStreams; //!< Number of supported TX spatial streams
  uint8_t m_rxSpatialStreams; //!< Number of supported RX spatial streams

  typedef std::map<ChannelNumberStandardPair, FrequencyWidthPair>
      ChannelToFrequencyWidthMap; //!< channel to frequency width map typedef
  static ChannelToFrequencyWidthMap
      m_channelToFrequencyWidth; //!< the channel to frequency width map

  uint8_t m_channelNumber;        //!< Operating channel number
  uint8_t m_initialChannelNumber; //!< Initial channel number

  Time m_channelSwitchDelay; //!< Time required to switch between channel

  Ptr<NetDevice> m_device;       //!< Pointer to the device
  Ptr<MobilityModel> m_mobility; //!< Pointer to the mobility model

  Ptr<PreambleDetectionModel>
      m_preambleDetectionModel; //!< Preamble detection model
  Ptr<ErrorModel>
      m_postReceptionErrorModel;   //!< Error model for receive packet events
  Time m_timeLastPreambleDetected; //!< Record the time the last preamble was
                                   //!< detected

  Ptr<WigigChannel> m_channel; //!< Pointer to the WigigChannel class that this
                               //!< WigigPhy is connected to.
  Ptr<Codebook> m_codebook;    //!< Pointer to the beamforming codebook.

  /* DMG Relay Variables */
  ReportSnrCallback m_reportSnrCallback; //!< Callback to support.

  /* Reception status variables */
  bool m_psduSuccess; //!< Flag to indicate if the PSDU has been received
                      //!< successfully.

  /* DMG PHY Layer Parameters */
  bool m_supportOfdm; //!< Flag to indicate whether we support OFDM PHY layer.
  bool m_supportLpSc; //!< Flag to indicate whether we support LP-SC PHY layer.
  uint8_t
      m_maxScTxMcs; //!< The maximum supported MCS for transmission by SC PHY.
  uint8_t m_maxScRxMcs; //!< The maximum supported MCS for reception by SC PHY.
  uint8_t m_maxOfdmTxMcs; //!< The maximum supported MCS for transmission by
                          //!< OFDM PHY.
  uint8_t
      m_maxOfdmRxMcs; //!< The maximum supported MCS for reception by OFDM PHY.
  Mac48Address m_currentSender; //!< MAC address of the STA that we are
                                //!< currently receiving from.
  bool m_isAp; //!< Flag to specify whether the STA is a PCP/AP or STA.
};

} // namespace ns3

#endif /* WIGIG_PHY_H */
