/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "ext-headers.h"

#include "wigig-mgt-headers.h"

#include "ns3/address-utils.h"
#include "ns3/fatal-error.h"
#include "ns3/simulator.h"

namespace ns3 {

/**********************************************************
 *  Channel Measurement Info field format (Figure 8-502h)
 **********************************************************/

ExtChannelMeasurementInfo::ExtChannelMeasurementInfo()
    : m_aid(0), m_snr(0), m_angle(0), m_recommend(0) {}

ExtChannelMeasurementInfo::~ExtChannelMeasurementInfo() {}

uint32_t ExtChannelMeasurementInfo::GetSerializedSize() const { return 4; }

void ExtChannelMeasurementInfo::Print(std::ostream &os) const {}

Buffer::Iterator
ExtChannelMeasurementInfo::Serialize(Buffer::Iterator start) const {
  uint8_t buffer = 0;

  start.WriteU8(m_aid);
  start.WriteU8(m_snr);
  buffer |= m_angle & 0x7F;
  buffer |= (m_recommend & 0x1) << 7;
  start.WriteU8(buffer);
  start.WriteU8(0);

  return start;
}

Buffer::Iterator
ExtChannelMeasurementInfo::Deserialize(Buffer::Iterator start) {
  uint8_t buffer;

  m_aid = start.ReadU8();
  m_snr = start.ReadU8();
  buffer = start.ReadU8();
  m_angle = buffer & 0x7F;
  m_recommend = (buffer >> 1) & 0x1;

  return start;
}

void ExtChannelMeasurementInfo::SetPeerStaAid(uint16_t aid) { m_aid = aid; }

void ExtChannelMeasurementInfo::SetSnr(uint8_t snr) { m_snr = snr; }

void ExtChannelMeasurementInfo::SetInternalAngle(uint8_t angle) {
  m_angle = angle;
}

void ExtChannelMeasurementInfo::SetRecommendSubField(bool value) {
  m_recommend = value;
}

uint16_t ExtChannelMeasurementInfo::GetPeerStaAid() const { return m_aid; }

uint8_t ExtChannelMeasurementInfo::GetSnr() const { return m_snr; }

uint8_t ExtChannelMeasurementInfo::GetInternalAngle() const { return m_angle; }

bool ExtChannelMeasurementInfo::GetRecommendSubField() const {
  return m_recommend;
}

/******************************************
 *  DMG Parameters Field (8.4.1.46)
 *******************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtDmgParameters);

ExtDmgParameters::ExtDmgParameters() {}

ExtDmgParameters::~ExtDmgParameters() {}

TypeId ExtDmgParameters::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtDmgParameters")
                          .SetParent<Header>()
                          .AddConstructor<ExtDmgParameters>();
  return tid;
}

TypeId ExtDmgParameters::GetInstanceTypeId() const { return GetTypeId(); }

uint32_t ExtDmgParameters::GetSerializedSize() const { return 1; }

void ExtDmgParameters::Print(std::ostream &os) const {}

Buffer::Iterator ExtDmgParameters::Serialize(Buffer::Iterator start) const {
  uint8_t buffer = 0;

  buffer |= m_bssType & 0x3;
  buffer |= ((m_cbapOnly & 0x1) << 2);
  buffer |= ((m_cbapSource & 0x1) << 3);
  buffer |= ((m_dmgPrivacy & 0x1) << 4);
  buffer |= ((m_ecpacPolicyEnforced & 0x1) << 5);

  start.WriteU8(buffer);
  return start;
}

Buffer::Iterator ExtDmgParameters::Deserialize(Buffer::Iterator start) {
  uint8_t buffer = start.ReadU8();

  m_bssType = static_cast<BssType>(buffer & 0x3);
  m_cbapOnly = (buffer >> 2) & 0x1;
  m_cbapSource = (buffer >> 3) & 0x1;
  m_dmgPrivacy = (buffer >> 4) & 0x1;
  m_ecpacPolicyEnforced = (buffer >> 5) & 0x1;

  return start;
}

void ExtDmgParameters::SetBssType(BssType type) { m_bssType = type; }

void ExtDmgParameters::SetCbapOnly(bool value) { m_cbapOnly = value; }

void ExtDmgParameters::SetCbapSource(bool value) { m_cbapSource = value; }

void ExtDmgParameters::SetDmgPrivacy(bool value) { m_dmgPrivacy = value; }

void ExtDmgParameters::SetEcpacPolicyEnforced(bool value) {
  m_ecpacPolicyEnforced = value;
}

BssType ExtDmgParameters::GetBssType() const { return m_bssType; }

bool ExtDmgParameters::GetCbapOnly() const { return m_cbapOnly; }

bool ExtDmgParameters::GetCbapSource() const { return m_cbapSource; }

bool ExtDmgParameters::Get_DMG_Privacy() const { return m_dmgPrivacy; }

bool ExtDmgParameters::GetEcpacPolicyEnforced() const {
  return m_ecpacPolicyEnforced;
}

/******************************************
 *   Beacon Interval Control Field (8-34b)
 *******************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtDmgBeaconIntervalCtrlField);

ExtDmgBeaconIntervalCtrlField::ExtDmgBeaconIntervalCtrlField()
    : m_ccPresent(false), m_discoveryMode(false), m_nextBeacon(1),
      m_atiPresent(true), m_abftLength(0), m_fss(0), m_isResponderTxss(false),
      m_nextAbft(0), m_fragmentedTxss(false), m_txssSpan(0), m_nBi(0),
      m_abftCount(0), m_nAbftAnt(0), m_pcpAssociationReady(false) {}

ExtDmgBeaconIntervalCtrlField::~ExtDmgBeaconIntervalCtrlField() {}

TypeId ExtDmgBeaconIntervalCtrlField::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtDmgBeaconIntervalCtrlField")
                          .SetParent<Header>()
                          .AddConstructor<ExtDmgBeaconIntervalCtrlField>();
  return tid;
}

TypeId ExtDmgBeaconIntervalCtrlField::GetInstanceTypeId() const {
  return GetTypeId();
}

uint32_t ExtDmgBeaconIntervalCtrlField::GetSerializedSize() const { return 6; }

void ExtDmgBeaconIntervalCtrlField::Print(std::ostream &os) const {}

Buffer::Iterator
ExtDmgBeaconIntervalCtrlField::Serialize(Buffer::Iterator start) const {
  uint32_t ctrl1 = 0;
  uint16_t ctrl2 = 0;

  ctrl1 |= m_ccPresent & 0x1;
  ctrl1 |= ((m_discoveryMode & 0x1) << 1);
  ctrl1 |= ((m_nextBeacon & 0xF) << 2);
  ctrl1 |= ((m_atiPresent & 0x1) << 6);
  ctrl1 |= ((m_abftLength & 0x7) << 7);
  ctrl1 |= ((m_fss & 0xF) << 10);
  if (m_nextAbft == 0) {
    ctrl1 |= ((m_isResponderTxss & 0x1) << 14);
  }
  ctrl1 |= ((m_nextAbft & 0xF) << 15);
  ctrl1 |= ((m_fragmentedTxss & 0x1) << 19);
  ctrl1 |= ((m_txssSpan & 0x3F) << 20);
  ctrl1 |= ((m_nBi & 0xF) << 27);
  ctrl1 |= ((m_abftCount & 0x1) << 31);

  ctrl2 |= (m_abftCount >> 1) & 0x1F;
  ctrl2 |= ((m_nAbftAnt & 0x3F) << 5);
  ctrl2 |= ((m_pcpAssociationReady & 0x1) << 11);

  start.WriteHtolsbU32(ctrl1);
  start.WriteHtolsbU16(ctrl2);

  return start;
}

Buffer::Iterator
ExtDmgBeaconIntervalCtrlField::Deserialize(Buffer::Iterator start) {
  uint32_t ctrl1 = start.ReadLsbtohU32();
  uint16_t ctrl2 = start.ReadLsbtohU16();

  m_ccPresent = ctrl1 & 0x1;
  m_discoveryMode = (ctrl1 >> 1) & 0x1;
  m_nextBeacon = (ctrl1 >> 2) & 0xF;
  m_atiPresent = (ctrl1 >> 6) & 0x1;
  m_abftLength = (ctrl1 >> 7) & 0x7;
  m_fss = (ctrl1 >> 10) & 0xF;
  bool b14 = (ctrl1 >> 14) & 0x1;
  m_nextAbft = (ctrl1 >> 15) & 0xF;
  if (m_nextAbft == 0) {
    m_isResponderTxss = b14;
  }
  m_fragmentedTxss = (ctrl1 >> 19) & 0x1;
  m_txssSpan = (ctrl1 >> 20) & 0x3F;
  m_nBi = (ctrl1 >> 27) & 0xF;
  m_abftCount = ((ctrl1 >> 31) & 0x1) | ((ctrl2 << 1) & 0x3E);
  m_nAbftAnt = (ctrl2 >> 5) & 0x3F;
  m_pcpAssociationReady = (ctrl2 >> 11) & 0x1;

  return start;
}

void ExtDmgBeaconIntervalCtrlField::SetCcPresent(bool value) {
  m_ccPresent = value;
}

void ExtDmgBeaconIntervalCtrlField::SetDiscoveryMode(bool value) {
  m_discoveryMode = value;
}

void ExtDmgBeaconIntervalCtrlField::SetNextBeacon(uint8_t value) {
  NS_ASSERT((value >= 0) && (value <= 15));
  m_nextBeacon = value;
}

void ExtDmgBeaconIntervalCtrlField::SetAtiPresent(bool value) {
  m_atiPresent = value;
}

void ExtDmgBeaconIntervalCtrlField::SetAbftLength(uint8_t length) {
  NS_ASSERT((1 <= length) && (length <= 8));
  m_abftLength = length - 1;
}

void ExtDmgBeaconIntervalCtrlField::SetFss(uint8_t number) {
  NS_ASSERT((1 <= number) && (number <= 16));
  m_fss = number - 1;
}

void ExtDmgBeaconIntervalCtrlField::SetIsResponderTxss(bool value) {
  m_isResponderTxss = value;
}

void ExtDmgBeaconIntervalCtrlField::SetNextAbft(uint8_t value) {
  m_nextAbft = value;
}

void ExtDmgBeaconIntervalCtrlField::SetFragmentedTxss(bool value) {
  m_fragmentedTxss = value;
}

void ExtDmgBeaconIntervalCtrlField::SetTxssSpan(uint8_t value) {
  m_txssSpan = value;
}

void ExtDmgBeaconIntervalCtrlField::SetNBi(uint8_t value) { m_nBi = value; }

void ExtDmgBeaconIntervalCtrlField::SetAbftCount(uint8_t value) {
  m_abftCount = value;
}

void ExtDmgBeaconIntervalCtrlField::SetNAbftAnt(uint8_t value) {
  m_nAbftAnt = value;
}

void ExtDmgBeaconIntervalCtrlField::SetPcpAssociationReady(bool value) {
  m_pcpAssociationReady = value;
}

bool ExtDmgBeaconIntervalCtrlField::IsCcPresent() const { return m_ccPresent; }

bool ExtDmgBeaconIntervalCtrlField::IsDiscoveryMode() const {
  return m_discoveryMode;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetNextBeacon() const {
  return m_nextBeacon;
}

bool ExtDmgBeaconIntervalCtrlField::IsAtiPresent() const {
  return m_atiPresent;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetAbftLength() const {
  return m_abftLength + 1;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetFss() const { return m_fss + 1; }

bool ExtDmgBeaconIntervalCtrlField::IsResponderTxss() const {
  return m_isResponderTxss;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetNextAbft() const {
  return m_nextAbft;
}

bool ExtDmgBeaconIntervalCtrlField::GetFragmentedTxss() const {
  return m_fragmentedTxss;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetTxssSpan() const {
  return m_txssSpan;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetNBi() const { return m_nBi; }

uint8_t ExtDmgBeaconIntervalCtrlField::GetAbftCount() const {
  return m_abftCount;
}

uint8_t ExtDmgBeaconIntervalCtrlField::GetNAbftAnt() const {
  return m_nAbftAnt;
}

bool ExtDmgBeaconIntervalCtrlField::GetPcpAssociationReady() const {
  return m_pcpAssociationReady;
}

/******************************************
 *     DMG Beacon (8.3.4.1)
 *******************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtDmgBeacon);

ExtDmgBeacon::ExtDmgBeacon() : m_timestamp(0), m_beaconInterval(0) {}

ExtDmgBeacon::~ExtDmgBeacon() {}

TypeId ExtDmgBeacon::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtDmgBeacon")
                          .SetParent<Header>()
                          .AddConstructor<ExtDmgBeacon>();
  return tid;
}

TypeId ExtDmgBeacon::GetInstanceTypeId() const { return GetTypeId(); }

uint32_t ExtDmgBeacon::GetSerializedSizeImpl() const {
  uint32_t size = 0;
  size += 8;                         // Timestamp (See 8.4.1.10)
  size += m_ssw.GetSerializedSize(); // Sector Sweep (See 8.4a.1)
  size += 2;                         // Beacon Interval (See 8.4.1.3)
  size += m_beaconIntervalCtrl
              .GetSerializedSize(); // Beacon Interval Control (See 8.4.1.3)
  size += m_dmgParameters.GetSerializedSize(); // DMG Parameters (See 8.4.1.46)
  if (m_beaconIntervalCtrl.IsCcPresent())      // Cluster Control Information
  {
    size += m_cluster.GetSerializedSize();
  }
  size += WifiMgtHeader<ExtDmgBeacon,
                        WigigProbeResponseElems>::GetSerializedSizeImpl();
  return size;
}

void ExtDmgBeacon::PrintImpl(std::ostream &os) const {
  WifiMgtHeader<ExtDmgBeacon, WigigProbeResponseElems>::PrintImpl(os);
}

void ExtDmgBeacon::SerializeImpl(Buffer::Iterator start) const {
  /* Fixed Parameters */
  // 1. Timestamp.
  // 2. Sector Sweep.
  // 3. Beacon Interval.
  // 4. Beacon Interval Control.
  // 5. DMG Parameters.
  /* Other Information Elements */
  Buffer::Iterator i = start;

  i.WriteHtolsbU64(m_timestamp);
  i = m_ssw.Serialize(i);
  i.WriteHtolsbU16(m_beaconInterval / 1024);
  i = m_beaconIntervalCtrl.Serialize(i);
  i = m_dmgParameters.Serialize(i);
  if (m_beaconIntervalCtrl.IsCcPresent()) {
    i = m_cluster.Serialize(i);
  }
  WifiMgtHeader<ExtDmgBeacon, WigigProbeResponseElems>::SerializeImpl(i);
}

uint32_t ExtDmgBeacon::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;

  m_timestamp = i.ReadLsbtohU64();
  i = m_ssw.Deserialize(i);
  m_beaconInterval = i.ReadLsbtohU16();
  m_beaconInterval *= 1024;
  i = m_beaconIntervalCtrl.Deserialize(i);
  i = m_dmgParameters.Deserialize(i);
  if (m_beaconIntervalCtrl.IsCcPresent()) {
    i = m_cluster.Deserialize(i);
  }
  auto distance =
      i.GetDistanceFrom(start) +
      WifiMgtHeader<ExtDmgBeacon, WigigProbeResponseElems>::DeserializeImpl(i);

  return distance;
}

void ExtDmgBeacon::SetBssid(Mac48Address bssid) { m_bssid = bssid; }

void ExtDmgBeacon::SetTimestamp(uint64_t timestamp) { m_timestamp = timestamp; }

void ExtDmgBeacon::SetSswField(DmgSswField &ssw) { m_ssw = ssw; }

void ExtDmgBeacon::SetBeaconIntervalUs(uint64_t interval) {
  m_beaconInterval = interval;
}

void ExtDmgBeacon::SetBeaconIntervalControlField(
    ExtDmgBeaconIntervalCtrlField &ctrl) {
  m_beaconIntervalCtrl = ctrl;
}

void ExtDmgBeacon::SetBeaconIntervalControlField(ExtDmgParameters &parameters) {
  m_dmgParameters = parameters;
}

void ExtDmgBeacon::SetDmgParameters(ExtDmgParameters &parameters) {
  m_dmgParameters = parameters;
}

void ExtDmgBeacon::SetClusterControlField(
    ExtDmgClusteringControlField &cluster) {
  m_cluster = cluster;
}

Mac48Address ExtDmgBeacon::GetBssid() const { return m_bssid; }

uint64_t ExtDmgBeacon::GetTimestamp() const { return m_timestamp; }

DmgSswField ExtDmgBeacon::GetSswField() const { return m_ssw; }

uint64_t ExtDmgBeacon::GetBeaconIntervalUs() const { return m_beaconInterval; }

ExtDmgBeaconIntervalCtrlField
ExtDmgBeacon::GetBeaconIntervalControlField() const {
  return m_beaconIntervalCtrl;
}

ExtDmgParameters ExtDmgBeacon::GetDmgParameters() const {
  return m_dmgParameters;
}

ExtDmgClusteringControlField ExtDmgBeacon::GetClusterControlField() const {
  return m_cluster;
}

} // namespace ns3
