/*
 * Copyright (c) 2019 Orange Labs
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Rediet <getachew.redieteab@orange.com>
 */

#include "wigig-ppdu.h"

#include "wigig-phy.h"
#include "wigig-psdu.h"

#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigPpdu");

WigigPpdu::WigigPpdu(Ptr<const WigigPsdu> psdu, const WigigTxVector &txVector,
                     Time ppduDuration, uint16_t frequency)
    : m_preamble(txVector.GetPreambleType()),
      m_modulation(txVector.IsValid() ? txVector.GetMode().GetModulationClass()
                                      : WIFI_MOD_CLASS_UNKNOWN),
      m_psdu(psdu), m_truncatedTx(false), m_frequency(frequency),
      m_channelWidth(txVector.GetChannelWidth()),
      m_txPowerLevel(txVector.GetTxPowerLevel()) {
  if (!txVector.IsValid()) {
    return;
  }
  NS_LOG_FUNCTION(this << psdu << txVector << ppduDuration << frequency);
  switch (m_modulation) {
  case WIFI_MOD_CLASS_DMG_CTRL: {
    m_dmgCtrlHeader.SetLength(psdu->GetSize(), psdu->IsShortSsw());
    m_dmgCtrlHeader.SetTrainingLength(txVector.GetTrainingFieldLength());
    m_dmgCtrlHeader.SetPacketType(txVector.GetPacketType());
    break;
  }
  case WIFI_MOD_CLASS_DMG_SC: {
    m_dmgScHeader.SetLength(psdu->GetSize());
    m_dmgScHeader.SetTrainingLength(txVector.GetTrainingFieldLength());
    m_dmgScHeader.SetPacketType(txVector.GetPacketType());
    m_dmgScHeader.SetAggregation(psdu->IsAggregate());
    m_dmgScHeader.SetBeamTrackingRequest(txVector.IsBeamTrackingRequested());
    m_dmgScHeader.SetBaseMcs(txVector.GetMode().GetMcsValue());
    m_dmgScHeader.SetLastRssi(txVector.GetLastRssi());
    break;
  }
  case WIFI_MOD_CLASS_DMG_OFDM: {
    m_dmgOfdmHeader.SetLength(psdu->GetSize());
    m_dmgOfdmHeader.SetTrainingLength(txVector.GetTrainingFieldLength());
    m_dmgOfdmHeader.SetPacketType(txVector.GetPacketType());
    m_dmgOfdmHeader.SetAggregation(psdu->IsAggregate());
    m_dmgOfdmHeader.SetBeamTrackingRequest(txVector.IsBeamTrackingRequested());
    m_dmgOfdmHeader.SetBaseMcs(txVector.GetMode().GetMcsValue());
    m_dmgOfdmHeader.SetLastRssi(txVector.GetLastRssi());
    break;
  }
  default:
    NS_FATAL_ERROR("unsupported modulation class");
    break;
  }
}

WigigPpdu::~WigigPpdu() {}

WigigTxVector WigigPpdu::GetTxVector() const {
  WigigTxVector txVector;
  txVector.SetPreambleType(m_preamble);
  switch (m_modulation) {
  case WIFI_MOD_CLASS_DMG_CTRL: {
    txVector.SetMode(WifiMode("DmgMcs0"));
    txVector.SetTrainingFieldLength(m_dmgCtrlHeader.GetTrainingLength());
    txVector.SetPacketType(m_dmgCtrlHeader.GetPacketType());
    txVector.SetChannelWidth(2160);
    break;
  }
  case WIFI_MOD_CLASS_DMG_SC: {
    txVector.SetTrainingFieldLength(m_dmgScHeader.GetTrainingLength());
    txVector.SetPacketType(m_dmgScHeader.GetPacketType());
    txVector.SetAggregation(m_dmgScHeader.GetAggregation());
    if (m_dmgScHeader.GetBeamTrackingRequest()) {
      txVector.RequestBeamTracking();
    }
    txVector.SetLastRssi(m_dmgScHeader.GetLastRssi());
    txVector.SetMode(WigigPhy::GetDmgMcs(m_dmgScHeader.GetBaseMcs()));
    txVector.SetChannelWidth(2160);
    break;
  }
  case WIFI_MOD_CLASS_DMG_OFDM: {
    txVector.SetTrainingFieldLength(m_dmgOfdmHeader.GetTrainingLength());
    txVector.SetPacketType(m_dmgOfdmHeader.GetPacketType());
    txVector.SetAggregation(m_dmgOfdmHeader.GetAggregation());
    if (m_dmgOfdmHeader.GetBeamTrackingRequest()) {
      txVector.RequestBeamTracking();
    }
    txVector.SetLastRssi(m_dmgOfdmHeader.GetLastRssi());
    txVector.SetMode(WigigPhy::GetDmgMcs(m_dmgOfdmHeader.GetBaseMcs()));
    txVector.SetChannelWidth(2160);
    break;
  }
  default:
    NS_FATAL_ERROR("unsupported modulation class");
    break;
  }
  txVector.SetTxPowerLevel(m_txPowerLevel);
  return txVector;
}

Ptr<const WigigPsdu> WigigPpdu::GetPsdu() const { return m_psdu; }

bool WigigPpdu::IsTruncatedTx() const { return m_truncatedTx; }

void WigigPpdu::SetTruncatedTx() {
  NS_LOG_FUNCTION(this);
  m_truncatedTx = true;
}

Time WigigPpdu::GetTxDuration() const {
  Time ppduDuration = Seconds(0);
  WigigTxVector txVector = GetTxVector();
  switch (m_modulation) {
  case WIFI_MOD_CLASS_DMG_CTRL:
    ppduDuration = WigigPhy::CalculateTxDuration(m_dmgCtrlHeader.GetLength(),
                                                 txVector, m_frequency);
    break;
  case WIFI_MOD_CLASS_DMG_SC:
    ppduDuration = WigigPhy::CalculateTxDuration(m_dmgScHeader.GetLength(),
                                                 txVector, m_frequency);
    break;
  case WIFI_MOD_CLASS_DMG_OFDM:
    ppduDuration = WigigPhy::CalculateTxDuration(m_dmgOfdmHeader.GetLength(),
                                                 txVector, m_frequency);
    break;
  default:
    NS_FATAL_ERROR("unsupported modulation class");
    break;
  }
  return ppduDuration;
}

void WigigPpdu::Print(std::ostream &os) const {
  os << "preamble=" << m_preamble << ", modulation=" << m_modulation
     << ", truncatedTx=" << (m_truncatedTx ? "Y" : "N") << ", PSDU=" << m_psdu;
}

std::ostream &operator<<(std::ostream &os, const WigigPpdu &ppdu) {
  ppdu.Print(os);
  return os;
}

} // namespace ns3
