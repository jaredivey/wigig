/*
 * Copyright (c) 2005 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef WIGIG_TXOP_H
#define WIGIG_TXOP_H

#include "mac-low-transmission-parameters.h"
#include "wigig-data-types.h"
#include "wigig-mac-header.h"

#include "ns3/object.h"
#include "ns3/traced-value.h"

#include <map>

namespace ns3 {

class Packet;
class MacTxMiddle;
class MacLow;
class WifiMode;
class WigigMacQueue;
class WigigMacQueueItem;
class UniformRandomVariable;
class CtrlBAckResponseHeader;
class WigigTxVector;
class WigigChannelAccessManager;
class WigigRemoteStationManager;

/**
 * \brief Handle packet fragmentation and retransmissions
 * for data and management frames.
 * \ingroup wigig
 *
 * This class implements the packet fragmentation and
 * retransmission policy for data and management frames.
 * It uses the ns3::MacLow and ns3::WigigChannelAccessManager helper
 * classes to respectively send packets and decide when
 * to send them. Packets are stored in a ns3::WigigMacQueue
 * until they can be sent.
 *
 * The policy currently implemented uses a simple fragmentation
 * threshold: any packet bigger than this threshold is fragmented
 * in fragments whose size is smaller than the threshold.
 *
 * The retransmission policy is also very simple: every packet is
 * retransmitted until it is either successfully transmitted or
 * it has been retransmitted up until the SSRC or SLRC thresholds.
 *
 * The RTS/CTS policy is similar to the fragmentation policy: when
 * a packet is bigger than a threshold, the RTS/CTS protocol is used.
 */

class WigigTxop : public Object {
public:
  /// allow MacLowTransmissionListener class access
  friend class MacLowTransmissionListener;

  WigigTxop();
  ~WigigTxop() override;

  /**
   * typedef for a callback to invoke when a
   * packet transmission was completed successfully.
   */
  typedef Callback<void, Ptr<const Packet>, const WigigMacHeader &> TxPacketOk;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  /**
   * typedef for a callback to invoke when a
   * packet transmission was completed successfully.
   */
  typedef Callback<void, const WigigMacHeader &> TxOk;
  /**
   * typedef for a callback to invoke when a
   * packet transmission was failed.
   */
  typedef Callback<void, const WigigMacHeader &> TxFailed;
  /**
   * typedef for a callback to invoke when a
   * packet is dropped.
   */
  typedef Callback<void, Ptr<const Packet>> TxDropped;

  /**
   * Check for QoS TXOP.
   *
   * \returns true if QoS TXOP.
   */
  virtual bool IsWigigQosTxop() const;

  /**
   * Set MacLow associated with this WigigTxop.
   *
   * \param low MacLow to associate.
   */
  void SetMacLow(const Ptr<MacLow> low);
  /**
   * Set WigigChannelAccessManager this WigigTxop is associated to.
   *
   * \param manager WigigChannelAccessManager to associate.
   */
  void SetChannelAccessManager(const Ptr<WigigChannelAccessManager> manager);
  /**
   * Set WigigRemoteStationsManager this WigigTxop is associated to.
   *
   * \param remoteManager WigigRemoteStationManager to associate.
   */
  virtual void SetWifiRemoteStationManager(
      const Ptr<WigigRemoteStationManager> remoteManager);
  /**
   * Set MacTxMiddle this WigigTxop is associated to.
   *
   * \param txMiddle MacTxMiddle to associate.
   */
  void SetTxMiddle(const Ptr<MacTxMiddle> txMiddle);

  /**
   * \param callback the callback to invoke when a
   * packet transmission was completed successfully.
   */
  void SetTxOkCallback(TxPacketOk callback);
  /**
   * \param callback the callback to invoke when a
   * transmission without ACK was completed successfully.
   */
  void SetTxOkNoAckCallback(TxOk callback);
  /**
   * \param callback the callback to invoke when a
   *        packet transmission was completed unsuccessfully.
   */
  void SetTxFailedCallback(TxFailed callback);
  /**
   * \param callback the callback to invoke when a
   *        packet is dropped.
   */
  void SetTxDroppedCallback(TxDropped callback);

  /**
   * Return the MacLow associated with this WigigTxop.
   *
   * \return the associated MacLow
   */
  Ptr<MacLow> GetLow() const;

  /**
   * Return the packet queue associated with this WigigTxop.
   *
   * \return the associated WigigMacQueue
   */
  Ptr<WigigMacQueue> GetWigigMacQueue() const;

  /**
   * Set the minimum contention window size.
   *
   * \param minCw the minimum contention window size.
   */
  void SetMinCw(uint32_t minCw);
  /**
   * Set the maximum contention window size.
   *
   * \param maxCw the maximum contention window size.
   */
  void SetMaxCw(uint32_t maxCw);
  /**
   * Set the number of slots that make up an AIFS.
   *
   * \param aifsn the number of slots that make up an AIFS.
   */
  void SetAifsn(uint8_t aifsn);
  /**
   * Set the TXOP limit.
   *
   * \param txopLimit the TXOP limit.
   *        Value zero corresponds to default WigigTxop.
   */
  void SetTxopLimit(Time txopLimit);
  /**
   * Return the minimum contention window size.
   *
   * \return the minimum contention window size.
   */
  uint32_t GetMinCw() const;
  /**
   * Return the maximum contention window size.
   *
   * \return the maximum contention window size.
   */
  uint32_t GetMaxCw() const;
  /**
   * Return the number of slots that make up an AIFS.
   *
   * \return the number of slots that make up an AIFS.
   */
  uint8_t GetAifsn() const;
  /**
   * Return the TXOP limit.
   *
   * \return the TXOP limit.
   */
  Time GetTxopLimit() const;
  /**
   * Reset state of the current EDCA.
   */
  void ResetState();

  /**
   * When a channel switching occurs, enqueued packets are removed.
   */
  virtual void NotifyChannelSwitching();
  /**
   * When sleep operation occurs, if there is a pending packet transmission,
   * it will be reinserted to the front of the queue.
   */
  virtual void NotifySleep();
  /**
   * When off operation occurs, the queue gets cleaned up.
   */
  virtual void NotifyOff();
  /**
   * When wake up operation occurs, channel access will be restarted.
   */
  virtual void NotifyWakeUp();
  /**
   * When on operation occurs, channel access will be started.
   */
  virtual void NotifyOn();

  /* Event handlers */
  /**
   * \param packet packet to send.
   * \param hdr header of packet to send.
   *
   * Store the packet in the internal queue until it
   * can be sent safely.
   */
  virtual void Queue(Ptr<Packet> packet, const WigigMacHeader &hdr);

  /* Event handlers */
  /**
   * Event handler when a CTS timeout has occurred.
   */
  virtual void MissedCts();
  /**
   * Event handler when an Ack is received.
   */
  virtual void GotAck();
  /**
   * Event handler when an Ack is missed.
   */
  virtual void MissedAck();
  /**
   * Event handler when a BlockAck is received.
   *
   * \param blockAck BlockAck header.
   * \param recipient address of the recipient.
   * \param rxSnr SNR of the BlockAck itself in linear scale.
   * \param dataSnr reported data SNR from the peer in linear scale.
   * \param dataTxVector TXVECTOR used to send the Data.
   */
  virtual void GotBlockAck(const CtrlBAckResponseHeader *blockAck,
                           Mac48Address recipient, double rxSnr, double dataSnr,
                           const WigigTxVector &dataTxVector);
  /**
   * Event handler when a BlockAck timeout has occurred.
   * \param nMpdus the number of MPDUs sent in the A-MPDU transmission that
   * results in a BlockAck timeout.
   */
  virtual void MissedBlockAck(uint8_t nMpdus);

  /**
   * Start transmission for the next fragment.
   * This is called for fragment only.
   */
  virtual void StartNextFragment();
  /**
   * Cancel the transmission.
   */
  virtual void Cancel();
  /**
   * Start transmission for the next packet if allowed by the TxopLimit.
   */
  virtual void StartNextPacket();
  /**
   * Event handler when a transmission that
   * does not require an Ack has completed.
   */
  virtual void EndTxNoAck();

  /**
   * Return the remaining duration in the current TXOP.
   *
   * \return the remaining duration in the current TXOP.
   */
  virtual Time GetWigigTxopRemaining() const;
  /**
   * Update backoff and restart access if needed.
   */
  virtual void TerminateWigigTxop();

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by this model. Return the number of streams (possibly zero) that
   * have been assigned.
   *
   * \param stream first stream index to use.
   *
   * \return the number of stream indices assigned by this model.
   */
  int64_t AssignStreams(int64_t stream);

  /**
   * Start a new allocation period in the DTI channel access period.
   * \param allocationType The type of the allocation (SP/CBAP)
   * \param allocationId The ID of the allocation.
   * \param peerStation The MAC address of the peer station.
   * \param allocationDuration The total duration of the current allocation.
   */
  virtual void StartAllocationPeriod(AllocationType allocationType,
                                     AllocationId allocationId,
                                     Mac48Address peerStation,
                                     Time allocationDuration);
  /**
   * Resume TXOP normal operation.
   * Note: We call this function after performing SLS.
   */
  void ResumeTxopTransmission();
  /**
   * End Current Contention Period.
   */
  void EndAllocationPeriod();
  /**
   * Get the remaining duration in the current allocation.
   * \return The remaining time in the current allocation.
   */
  Time GetAllocationRemaining() const;
  /**
   * Get the remaining time for transmission in the current allocation/WigigTxop
   * period. \return The remaining time for transmission in the current
   * allocation/WigigTxop period.
   */
  Time GetRemainingTimeForTransmission();
  /**
   * Get duration limit for the PPDU to be transmitted.
   * \param item
   * \param params The MacLow transmission parameters for the given frame.
   * \return PPDU duration limit.
   */
  virtual Time GetPpduDurationLimit(Ptr<const WigigMacQueueItem> item,
                                    const MacLowTransmissionParameters &params);

  /**
   * \returns true if access has been requested for this function and
   *          has not been granted already, false otherwise.
   */
  virtual bool IsAccessRequested() const;

  /**
   * \param nSlots the number of slots of the backoff.
   *
   * Start a backoff by initializing the backoff counter to the number of
   * slots specified.
   */
  void StartBackoffNow(uint32_t nSlots);

  /**
   * This function is used to set the type of the allocation used for
   * communication during the data transmission interval. \param allocationType
   * CBAP or SP.
   */
  void SetAllocationType(AllocationType allocationType);

protected:
  ///< WigigChannelAccessManager associated class
  friend class WigigChannelAccessManager;

  void DoDispose() override;
  void DoInitialize() override;

  /* WigigTxop notifications forwarded here */
  /**
   * Notify that access request has been received.
   */
  virtual void NotifyAccessRequested();
  /**
   * Notify the WigigTxop that access has been granted.
   */
  virtual void NotifyAccessGranted();
  /**
   * Notify the WigigTxop that internal collision has occurred.
   */
  virtual void NotifyInternalCollision();

  /**
   * Check if the WigigTxop has frames to transmit.
   * \return true if the WigigTxop has frames to transmit.
   */
  virtual bool HasFramesToTransmit();
  /**
   * Generate a new backoff now.
   */
  virtual void GenerateBackoff();
  /**
   * Restart access request if needed.
   */
  virtual void RestartAccessIfNeeded();
  /**
   * Request access from WigigTxop if needed.
   */
  virtual void StartAccessIfNeeded();

  /**
   * \returns the current value of the CW variable. The initial value is
   *          minCW.
   */
  uint32_t GetCw() const;
  /**
   * Update the value of the CW variable to take into account
   * a transmission success or a transmission abort (stop transmission
   * of a packet after the maximum number of retransmissions has been
   * reached). By default, this resets the CW variable to minCW.
   */
  void ResetCw();
  /**
   * Update the value of the CW variable to take into account
   * a transmission failure. By default, this triggers a doubling
   * of CW (capped by maxCW).
   */
  void UpdateFailedCw();
  /**
   * Return the current number of backoff slots.
   *
   * \return the current number of backoff slots
   */
  uint32_t GetBackoffSlots() const;
  /**
   * Return the time when the backoff procedure started.
   *
   * \return the time when the backoff procedure started
   */
  Time GetBackoffStart() const;
  /**
   * Update backoff slots that nSlots has passed.
   *
   * \param nSlots the number of slots to decrement
   * \param backoffUpdateBound the time at which backoff should start
   */
  void UpdateBackoffSlotsNow(uint32_t nSlots, Time backoffUpdateBound);

  /**
   * Check if RTS should be re-transmitted if CTS was missed.
   *
   * \param packet current packet being transmitted.
   * \param hdr current header being transmitted.
   * \return true if RTS should be re-transmitted,
   *         false otherwise.
   */
  bool NeedRtsRetransmission(Ptr<const Packet> packet,
                             const WigigMacHeader &hdr);
  /**
   * Check if Data should be re-transmitted if Ack was missed.
   *
   * \param packet current packet being transmitted.
   * \param hdr current header being transmitted.
   * \return true if Data should be re-transmitted,
   *         false otherwise.
   */
  bool NeedDataRetransmission(Ptr<const Packet> packet,
                              const WigigMacHeader &hdr);
  /**
   * Check if the current packet should be fragmented.
   *
   * \return true if the current packet should be fragmented,
   *         false otherwise
   */
  virtual bool NeedFragmentation() const;

  /**
   * Continue to the next fragment. This method simply
   * increments the internal variable that keep track
   * of the current fragment number.
   */
  void NextFragment();
  /**
   * Get the next fragment from the packet with
   * appropriate Wifi header for the fragment.
   *
   * \param hdr Wi-Fi header.
   *
   * \return the fragment with the current fragment number.
   */
  virtual Ptr<Packet> GetFragmentPacket(WigigMacHeader *hdr);
  /**
   * Calculate the size of the next fragment.
   *
   * \return the size of the next fragment in bytes.
   */
  virtual uint32_t GetNextFragmentSize() const;
  /**
   * Calculate the size of the current fragment.
   *
   * \return the size of the current fragment in bytes.
   */
  virtual uint32_t GetFragmentSize() const;
  /**
   * Calculate the offset for the current fragment.
   *
   * \return the offset for the current fragment in bytes.
   */
  virtual uint32_t GetFragmentOffset() const;
  /**
   * Check if the current fragment is the last fragment.
   *
   * \return true if the current fragment is the last fragment,
   *         false otherwise.
   */
  virtual bool IsLastFragment() const;
  /**
   *
   * Pass the packet included in the wifi MAC queue item to the
   * packet dropped callback.
   *
   * \param item the wifi MAC queue item.
   */
  void TxDroppedPacket(Ptr<const WigigMacQueueItem> item);

  TxOk m_txOkNoAckCallback; //!< the transmit OK No ACK callback
  Ptr<WigigChannelAccessManager>
      m_channelAccessManager;    //!< the channel access manager
  TxPacketOk m_txOkCallback;     //!< the transmit OK callback
  TxFailed m_txFailedCallback;   //!< the transmit failed callback
  TxDropped m_txDroppedCallback; //!< the packet dropped callback
  Ptr<WigigMacQueue> m_queue;    //!< the wifi MAC queue
  Ptr<MacTxMiddle> m_txMiddle;   //!< the MacTxMiddle
  Ptr<MacLow> m_low;             //!< the MacLow
  Ptr<WigigRemoteStationManager>
      m_stationManager;             //!< the wifi remote station manager
  Ptr<UniformRandomVariable> m_rng; //!< the random stream

  uint32_t m_cwMin;        //!< the minimum contention window
  uint32_t m_cwMax;        //!< the maximum contention window
  uint32_t m_cw;           //!< the current contention window
  uint32_t m_backoff;      //!< the current backoff
  bool m_accessRequested;  //!< flag whether channel access is already requested
  uint32_t m_backoffSlots; //!< the number of backoff slots
  /**
   * the backoffStart variable is used to keep track of the
   * time at which a backoff was started or the time at which
   * the backoff counter was last updated.
   */
  Time m_backoffStart;

  uint8_t m_aifsn;  //!< the AIFSN
  Time m_txopLimit; //!< the TXOP limit time

  Ptr<const Packet> m_currentPacket; //!< the current packet
  WigigMacHeader m_currentHdr;       //!< the current header
  MacLowTransmissionParameters
      m_currentParams;                     //!< current transmission parameters
  uint8_t m_fragmentNumber;                //!< the fragment number
  TracedCallback<uint32_t> m_backoffTrace; //!< backoff trace value
  TracedValue<uint32_t> m_cwTrace;         //!< CW trace value

  /* Store packet and header for Allocation Period */
  typedef std::pair<Ptr<const Packet>, WigigMacHeader> PacketInformation;
  typedef std::map<AllocationId, PacketInformation> StoredPackets;
  typedef StoredPackets::iterator StoredPacketsI;
  typedef StoredPackets::const_iterator StoredPacketsCI;
  StoredPackets m_storedPackets; //!< Stored packets based on allocation ID.

  /** DMG Allocation Variables **/
  AllocationId m_allocationId; //!< The ID for the current allocation.
  AllocationType
      m_allocationType; //!< The type of the current allocation. (CBAP/SP)
  Mac48Address
      m_peerStation; //!< The address of the peer station for SP allocation.
  Time m_allocationStart;    //!< The start time of the current allocation.
  Time m_allocationDuration; //!< The duration of the current allocation.
  bool m_firstTransmission;  //!< Flag to indicate if this is the first
                            //!< transmission in the current allocation period.
  bool m_accessAllowed; //!< Flag to indicate whether the access is allowed for
                        //!< the current EDCA Queue.
};

} // namespace ns3

#endif /* WIGIG_TXOP_H */
