/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_CHANNEL_H
#define WIGIG_CHANNEL_H

#include "wigig-phy.h"

#include "ns3/channel.h"

namespace ns3 {

class NetDevice;
class PropagationLossModel;
class PropagationDelayModel;
class Packet;
class Time;
class WigigTxVector;
class WigigPpdu;

/**
 * \brief a channel to interconnect ns3::WigigPhy objects.
 * \ingroup wigig
 *
 * This class is expected to be used in tandem with the ns3::WigigPhy
 * class and supports an ns3::PropagationLossModel and an
 * ns3::PropagationDelayModel.  By default, no propagation models are set;
 * it is the caller's responsibility to set them before using the channel.
 */
class WigigChannel : public Channel {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  WigigChannel();
  ~WigigChannel() override;

  enum ActivityType {
    TX_ACTIVITY = 0,
    RX_ACTIVITY = 1,
  };

  std::size_t GetNDevices() const override;
  Ptr<NetDevice> GetDevice(std::size_t i) const override;

  /**
   * Adds the given WigigPhy to the PHY list
   *
   * \param phy the WigigPhy to be added to the PHY list
   */
  void Add(Ptr<WigigPhy> phy);

  /**
   * \param loss the new propagation loss model.
   */
  void SetPropagationLossModel(const Ptr<PropagationLossModel> loss);
  /**
   * \param delay the new propagation delay model.
   */
  void SetPropagationDelayModel(const Ptr<PropagationDelayModel> delay);

  /**
   * \param sender the PHY object from which the packet is originating.
   * \param ppdu the PPDU to send
   * \param txPowerDbm the TX power associated to the packet, in dBm
   *
   * This method should not be invoked by normal users. It is
   * currently invoked only from WigigPhy::StartTx.  The channel
   * attempts to deliver the PPDU to all other WigigPhy objects
   * on the channel (except for the sender).
   */
  void Send(Ptr<WigigPhy> sender, Ptr<const WigigPpdu> ppdu,
            double txPowerDbm) const;
  /**
   * Send AGC Subfield
   * \param sender
   * \param txPowerDbm
   * \param txVector
   */
  void SendAgcSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                       const WigigTxVector &txVector) const;
  /**
   * Send Channel Estimation (CE) Subfield of the TRN-Unit.
   * \param sender
   * \param txPowerDbm
   * \param txVector
   */
  void SendTrnCeSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                         const WigigTxVector &txVector) const;
  /**
   * Send TRN Subfield.
   * \param sender the device from which the packet is originating.
   * \param txPowerDbm the tx power associated to the packet.
   * \param txVector the TXVECTOR associated to the packet.
   */
  void SendTrnSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                       const WigigTxVector &txVector) const;

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by this model.  Return the number of streams (possibly zero) that
   * have been assigned.
   *
   * \param stream first stream index to use
   *
   * \return the number of stream indices assigned by this model
   */
  int64_t AssignStreams(int64_t stream);

  /**
   * Add bloackage on a certain path between two WigigPhy objects.
   * \param srcWigigPhy
   * \param dstWigigPhy
   */
  void AddBlockage(double (*blockage)(), Ptr<WigigPhy> srcWigigPhy,
                   Ptr<WigigPhy> dstWigigPhy);
  void RemoveBlockage();
  /**
   * Record PHY Activity.
   * \param srcID the ID of the transmitting node.
   * \param dstID the ID of the receiving node.
   * \param duration the duration of the activity in nanoseconds.
   * \param power the power of the transmitted or received part of the PLCP.
   * \param fieldType the type of the PLCP field being transmitted or received.
   * \param activityType the type of the PHY activity.
   */
  void RecordPhyActivity(uint32_t srcID, uint32_t dstID, Time duration,
                         double power, PlcpFieldType fieldType,
                         ActivityType activityType) const;

private:
  /**
   * A vector of pointers to WigigPhy.
   */
  typedef std::vector<Ptr<WigigPhy>> PhyList;

  /**
   * This method is scheduled by Send for each associated WigigPhy.
   * The method PPDU calls the corresponding WigigPhy that the first
   * bit of the packet has arrived.
   *
   * \param receiver the device to which the packet is destined
   * \param ppdu the PPDU being sent
   * \param txPowerDbm the TX power associated to the packet being sent (dBm)
   */
  static void Receive(Ptr<WigigPhy> receiver, Ptr<WigigPpdu> ppdu,
                      double txPowerDbm);
  /**
   * Generic function for receiving any subfield in TRN-Block.
   * \param i
   * \param sender
   * \param txVector
   * \param txPowerDbm
   * \param txAntennaGainDbi The transmit gain of the antenna at the sender once
   * this subfield is transmitted. \param type PLCP field type. \return Received
   * power over the subfield.
   */
  double ReceiveSubfield(uint32_t i, Ptr<WigigPhy> sender,
                         const WigigTxVector &txVector, double txPowerDbm,
                         double txAntennaGainDbi, Time duration,
                         PlcpFieldType type) const;
  /**
   * Receive AGC Subfield.
   * \param i
   * \param sender
   * \param txVector
   * \param txPowerDbm
   */
  void ReceiveAgcSubfield(uint32_t i, Ptr<WigigPhy> sender,
                          const WigigTxVector &txVector, double txPowerDbm,
                          double txAntennaGainDbi) const;
  /**
   * Receive TRN-CE Subfield.
   * \param i
   * \param sender
   * \param txVector
   * \param txPowerDbm
   */
  void ReceiveTrnCeSubfield(uint32_t i, Ptr<WigigPhy> sender,
                            const WigigTxVector &txVector, double txPowerDbm,
                            double txAntennaGainDbi) const;
  /**
   * Receive TRN Subfield in TRN Unit.
   * \param i index of the corresponding WigigPhy in the PHY list.
   * \param txVector the TXVECTOR of the packet.
   * \param txPowerDbm the transmitted signal strength [dBm].
   * \param txAntennaGainDbi The gain of the transmit antenna in dBi.
   */
  void ReceiveTrnSubfield(uint32_t i, Ptr<WigigPhy> sender,
                          const WigigTxVector &txVector, double txPowerDbm,
                          double txAntennaGainDbi) const;

  PhyList m_phyList; //!< List of WigigPhys connected to this WigigChannel
  Ptr<PropagationLossModel> m_loss;   //!< Propagation loss model
  Ptr<PropagationDelayModel> m_delay; //!< Propagation delay model
  double (*m_blockage)();             //!< Blockage model.
  Ptr<WigigPhy> m_srcWigigPhy;
  Ptr<WigigPhy> m_dstWigigPhy;

  /**
   * TracedCallback signature for reporting PHY activities.
   *
   * \param srcID the ID of the transmitting node.
   * \param dstID the ID of the receiving node.
   * \param duration the duration of the activity in nanoseconds.
   * \param power the power of the transmitted or received part of the PLCP.
   * \param fieldType the type of the PLCP field being transmitted or received.
   * \param activityType the type of the PHY activity.
   */
  typedef void (*PhyActivityTraceCallback)(uint32_t srcID, uint32_t dstID,
                                           Time duration, double power,
                                           uint16_t fieldType,
                                           uint16_t activityType);
  /**
   * A trace source that emulates a channel activity tracker to monitor power
   * changes.
   */
  typedef TracedCallback<uint32_t, uint32_t, Time, double, uint16_t, uint16_t>
      PhyActivityTrace;
  PhyActivityTrace m_phyActivityTrace;
};

} // namespace ns3

#endif /* WIGIG_CHANNEL_H */
