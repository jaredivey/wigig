/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef CONSTANT_RATE_WIGIG_MANAGER_H
#define CONSTANT_RATE_WIGIG_MANAGER_H

#include "wigig-remote-station-manager.h"

namespace ns3 {

/**
 * \ingroup wigig
 * \brief use constant rates for data and RTS transmissions
 *
 * This class uses always the same transmission rate for every
 * packet sent.
 */
class ConstantRateWigigManager : public WigigRemoteStationManager {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();
  ConstantRateWigigManager();
  ~ConstantRateWigigManager() override;
  virtual void SetDataMode(WifiMode mode);
  virtual void SetControlMode(WifiMode mode);

private:
  WigigRemoteStation *DoCreateStation() const override;
  void DoReportRxOk(WigigRemoteStation *station, double rxSnr,
                    WifiMode txMode) override;
  void DoReportRtsFailed(WigigRemoteStation *station) override;
  void DoReportDataFailed(WigigRemoteStation *station) override;
  void DoReportRtsOk(WigigRemoteStation *station, double ctsSnr,
                     WifiMode ctsMode, double rtsSnr) override;
  void DoReportDataOk(WigigRemoteStation *station, double ackSnr,
                      WifiMode ackMode, double dataSnr,
                      uint16_t dataChannelWidth, uint8_t dataNss) override;
  void DoReportFinalRtsFailed(WigigRemoteStation *station) override;
  void DoReportFinalDataFailed(WigigRemoteStation *station) override;
  WigigTxVector DoGetDataTxVector(WigigRemoteStation *station) override;
  WigigTxVector DoGetRtsTxVector(WigigRemoteStation *station) override;

  WifiMode m_dataMode; //!< Wifi mode for unicast Data frames
  WifiMode m_ctlMode;  //!< Wifi mode for RTS frames
};

} // namespace ns3

#endif /* CONSTANT_RATE_WIGIG_MANAGER_H */
