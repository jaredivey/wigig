/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "codebook-parametric.h"

#include "ns3/log.h"
#include "ns3/string.h"

#include <algorithm>
#include <fstream>
#include <string>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("CodebookParametric");

NS_OBJECT_ENSURE_REGISTERED(CodebookParametric);

float CalculateNormalizationFactor(const WeightsVector &weightsVector) {
  float normlizationValue = 0;
  for (auto it = weightsVector.begin(); it != weightsVector.end(); ++it) {
    normlizationValue += std::pow(std::abs(*it), 2.0);
  }
  normlizationValue = sqrt(normlizationValue);
  return normlizationValue;
}

/****** Parametric Pattern Configuration ******/

void ParametricPatternConfig::SetWeights(const WeightsVector &weights) {
  NS_LOG_FUNCTION(this);
  m_weights = weights;
  m_normalizationFactor = CalculateNormalizationFactor(weights);
}

WeightsVector ParametricPatternConfig::GetWeights() const { return m_weights; }

float ParametricPatternConfig::GetNormalizationFactor() const {
  return m_normalizationFactor;
}

void ParametricPatternConfig::SetNormalizationFactor(float factor) {
  m_normalizationFactor = factor;
}

ArrayPattern ParametricPatternConfig::GetArrayPattern() const {
  return m_arrayPattern;
}

void ParametricPatternConfig::SetArrayPattern(
    const ArrayPattern &arrayPattern) {
  NS_LOG_FUNCTION(this);
  m_arrayPattern = arrayPattern;
}

Complex ParametricPatternConfig::GetArrayPattern(uint16_t azimuthAngle,
                                                 uint16_t elevationAngle) {
  return m_arrayPatternMap[std::make_pair(azimuthAngle, elevationAngle)];
}

void ParametricPatternConfig::CalculateArrayPattern(
    Ptr<ParametricAntennaConfig> antennaConfig, uint16_t azimuthAngle,
    uint16_t elevationAngle) {
  auto angles = std::make_pair(azimuthAngle, elevationAngle);
  const auto iter = m_arrayPatternMap.find(angles);
  if (iter == m_arrayPatternMap.cend()) {
    Complex value = 0;
    uint16_t j = 0;
    for (auto it = m_weights.begin(); it != m_weights.end(); ++it, ++j) {
      value += (*it) *
               antennaConfig->steeringVector[azimuthAngle][elevationAngle][j];
    }
    value *=
        antennaConfig->singleElementDirectivity[azimuthAngle][elevationAngle];
    m_arrayPatternMap[angles] = value;
  }
}

/****** Parametric Antenna Configuration ******/

ArrayPattern
ParametricAntennaConfig::CalculateArrayPattern(WeightsVector weights) {
  ArrayPattern arrayPattern;
  for (uint16_t m = 0; m < AZIMUTH_CARDINALITY; m++) {
    for (uint16_t n = 0; n < ELEVATION_CARDINALITY; n++) {
      Complex value = 0;
      uint16_t j = 0;
      for (auto it = weights.begin(); it != weights.end(); ++it, ++j) {
        value += (*it) * steeringVector[m][n][j];
      }
      value *= singleElementDirectivity[m][n];
      arrayPattern[m][n] = value;
    }
  }
  return arrayPattern;
}

Ptr<ParametricPatternConfig>
ParametricAntennaConfig::GetQuasiOmniConfig() const {
  return DynamicCast<ParametricPatternConfig>(m_quasiOmniConfig);
}

void ParametricAntennaConfig::SetPhaseQuantizationBits(uint8_t num) {
  m_phaseQuantizationBits = num;
  m_phaseQuantizationStepSize = 2 * M_PI / (std::pow(2, num));
}

void ParametricAntennaConfig::CopyAntennaArray(
    Ptr<ParametricAntennaConfig> srcAntennaConfig) {
  NS_LOG_FUNCTION(this);
  azimuthOrientationDegree = srcAntennaConfig->azimuthOrientationDegree;
  elevationOrientationDegree = srcAntennaConfig->elevationOrientationDegree;
  orientation = srcAntennaConfig->orientation;
  numElements = srcAntennaConfig->numElements;
  singleElementDirectivity = srcAntennaConfig->singleElementDirectivity;
  steeringVector = srcAntennaConfig->steeringVector;
  amplitudeQuantizationBits = srcAntennaConfig->amplitudeQuantizationBits;
  m_phaseQuantizationBits = srcAntennaConfig->m_phaseQuantizationBits;
  m_phaseQuantizationStepSize = srcAntennaConfig->m_phaseQuantizationStepSize;
}

/****** Parametric Codebook Configuration ******/

TypeId CodebookParametric::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::CodebookParametric")
          .SetGroupName("Wigig")
          .SetParent<Codebook>()
          .AddConstructor<CodebookParametric>()
          .AddAttribute(
              "PrecalculatePatterns",
              "Whether we precalculate all the beam patterns over the 3D space "
              "within our "
              "simulation or"
              " we calculate only beam patterns based on the utilized angles "
              "in our Q-D traces.",
              BooleanValue(true),
              MakeBooleanAccessor(&CodebookParametric::m_precalculatedPatterns),
              MakeBooleanChecker())
          .AddAttribute(
              "TotalAntennas",
              "The total number of PAAs. This attribute is used together with "
              "MimoCodebook attribute.",
              UintegerValue(1),
              MakeUintegerAccessor(&CodebookParametric::m_totalAntennas),
              MakeUintegerChecker<uint8_t>(1, 8))
          .AddAttribute(
              "FileName", "The name of the codebook file to load.",
              StringValue(""),
              MakeStringAccessor(&CodebookParametric::SetCodebookFileName),
              MakeStringChecker());
  return tid;
}

CodebookParametric::CodebookParametric() : m_cloned(false) {
  NS_LOG_FUNCTION(this);
}

CodebookParametric::~CodebookParametric() { NS_LOG_FUNCTION(this); }

void CodebookParametric::SetCodebookFileName(std::string fileName) {
  NS_LOG_FUNCTION(this << fileName);
  if (!fileName.empty()) {
    m_fileName = fileName;
    LoadCodebook(m_fileName);
  }
}

WeightsVector CodebookParametric::ReadAntennaWeightsVector(std::ifstream &file,
                                                           uint16_t elements) {
  WeightsVector weights;
  std::string amp;
  std::string phase;
  std::string line;
  std::getline(file, line);
  std::istringstream split(line);
  for (uint16_t i = 0; i < elements; i++) {
    getline(split, amp, ',');
    getline(split, phase, ',');
    weights.push_back(std::polar(std::stof(amp), std::stof(phase)));
  }
  return weights;
}

void CodebookParametric::LoadCodebook(std::string filename) {
  NS_LOG_FUNCTION(this << "Loading Parametric Codebook file " << filename);
  std::ifstream file;
  file.open(filename.c_str(), std::ifstream::in);
  NS_ASSERT_MSG(file.good(), " Codebook file not found in " + filename);
  std::string line;

  RfChainId rfChainID;
  uint8_t totalRfChains; /* The total number of RF Chains. */
  uint8_t
      nSectors; /* The total number of sectors within a phased antenna array */
  AntennaId antennaID;
  SectorId sectorID;
  std::string amp;
  std::string phaseDelay;
  std::string directivity;

  /** Create RF Chain List **/

  /* The first line determines the number of RF Chains within the device */
  Ptr<RfChain> rfChainConfig;
  std::getline(file, line);
  totalRfChains = std::stod(line);
  for (RfChainId rfChainID = 1; rfChainID <= totalRfChains; rfChainID++) {
    rfChainConfig = Create<RfChain>();
    m_rfChainList[rfChainID] = rfChainConfig;
  }

  /** Create Antenna Array List **/

  /* The following line determines the number of phased antenna arrays within
   * the device */
  std::getline(file, line);
  m_totalAntennas = std::stod(line);

  for (uint8_t antennaIndex = 0; antennaIndex < m_totalAntennas;
       antennaIndex++) {
    Ptr<ParametricAntennaConfig> antennaConfig =
        Create<ParametricAntennaConfig>();
    SectorIdList bhiSectors;
    SectorIdList txBeamformingSectors;
    SectorIdList rxBeamformingSectors;

    /* Read phased antenna array ID */
    std::getline(file, line);
    antennaID = std::stoul(line);

    /* Read RF Chain ID (To which RF Chain we connect this phased antenna
     * Array). */
    std::getline(file, line);
    rfChainID = std::stoul(line);
    rfChainConfig = m_rfChainList[rfChainID];
    rfChainConfig->ConnectPhasedAntennaArray(antennaID, antennaConfig);
    antennaConfig->rfChain = rfChainConfig;

    /* Read phased antenna array azimuth orientation degree */
    std::getline(file, line);
    antennaConfig->azimuthOrientationDegree = std::stod(line);

    /* Read phased antenna array elevation orientation degree */
    std::getline(file, line);
    antennaConfig->elevationOrientationDegree = std::stod(line);

    /* Initialize rotation axes */
    antennaConfig->orientation.psi = 0;
    antennaConfig->orientation.theta = 0;
    antennaConfig->orientation.phi = 0;

    /* Read the number of antenna elements */
    std::getline(file, line);
    antennaConfig->numElements = std::stod(line);

    /* Read the number of quantization bits for phase */
    std::getline(file, line);
    antennaConfig->SetPhaseQuantizationBits(std::stoul(line));

    /* Read the number of quantization bits for amplitude */
    std::getline(file, line);
    antennaConfig->amplitudeQuantizationBits = std::stod(line);

    /* Read the directivity of a single antenna element */
    for (uint16_t m = 0; m < AZIMUTH_CARDINALITY; m++) {
      std::getline(file, line);
      std::istringstream split(line);
      for (uint16_t n = 0; n < ELEVATION_CARDINALITY; n++) {
        std::getline(split, directivity, ',');
        antennaConfig->singleElementDirectivity[m][n] = std::stod(directivity);
      }
    }

    /* Allocate the steering vector 3D Matrix. */
    for (uint16_t m = 0; m < AZIMUTH_CARDINALITY; m++) {
      for (uint16_t n = 0; n < ELEVATION_CARDINALITY; n++) {
        antennaConfig->steeringVector[m][n].resize(antennaConfig->numElements);
      }
    }

    /* Read the 3D steering vector of the antenna array */
    for (uint16_t l = 0; l < antennaConfig->numElements; l++) {
      for (uint16_t m = 0; m < AZIMUTH_CARDINALITY; m++) {
        std::getline(file, line);
        std::istringstream split(line);
        for (uint16_t n = 0; n < ELEVATION_CARDINALITY; n++) {
          std::getline(split, amp, ',');
          std::getline(split, phaseDelay, ',');
          antennaConfig->steeringVector[m][n][l] =
              std::polar(std::stod(amp), std::stod(phaseDelay));
        }
      }
    }

    /* Read Quasi-omni antenna weights and calculate its directivity */
    Ptr<ParametricPatternConfig> quasiOmni = Create<ParametricPatternConfig>();
    quasiOmni->SetWeights(
        ReadAntennaWeightsVector(file, antennaConfig->numElements));
    if (m_precalculatedPatterns) {
      quasiOmni->SetArrayPattern(
          antennaConfig->CalculateArrayPattern(quasiOmni->GetWeights()));
    }
    antennaConfig->SetQuasiOmniConfig(quasiOmni);

    /* Read the number of sectors within this antenna array */
    std::getline(file, line);
    nSectors = std::stoul(line);
    m_totalSectors += nSectors;

    for (uint8_t sector = 0; sector < nSectors; sector++) {
      Ptr<ParametricSectorConfig> sectorConfig =
          Create<ParametricSectorConfig>();

      /* Read Sector ID */
      std::getline(file, line);
      sectorID = std::stoul(line);

      /* Read Sector Type */
      std::getline(file, line);
      sectorConfig->sectorType = static_cast<SectorType>(std::stoul(line));

      /* Read Sector Usage */
      std::getline(file, line);
      sectorConfig->sectorUsage = static_cast<SectorUsage>(std::stoul(line));

      if ((sectorConfig->sectorUsage == BHI_SECTOR) ||
          (sectorConfig->sectorUsage == BHI_SLS_SECTOR)) {
        bhiSectors.push_back(sectorID);
      }
      if ((sectorConfig->sectorUsage == SLS_SECTOR) ||
          (sectorConfig->sectorUsage == BHI_SLS_SECTOR)) {
        if ((sectorConfig->sectorType == TX_SECTOR) ||
            (sectorConfig->sectorType == TX_RX_SECTOR)) {
          txBeamformingSectors.push_back(sectorID);
          m_totalTxSectors++;
        }
        if ((sectorConfig->sectorType == RX_SECTOR) ||
            (sectorConfig->sectorType == TX_RX_SECTOR)) {
          rxBeamformingSectors.push_back(sectorID);
          m_totalRxSectors++;
        }
      }

      /* Read sector antenna weights vector and calculate its directivity */
      sectorConfig->SetWeights(
          ReadAntennaWeightsVector(file, antennaConfig->numElements));
      if (m_precalculatedPatterns) {
        sectorConfig->SetArrayPattern(
            antennaConfig->CalculateArrayPattern(sectorConfig->GetWeights()));
      }
      antennaConfig->sectorList[sectorID] = sectorConfig;
    }

    if (!bhiSectors.empty()) {
      m_bhiAntennaList[antennaID] = bhiSectors;
    }

    if (!txBeamformingSectors.empty()) {
      m_txBeamformingSectors[antennaID] = txBeamformingSectors;
    }

    if (!rxBeamformingSectors.empty()) {
      m_rxBeamformingSectors[antennaID] = rxBeamformingSectors;
    }

    m_antennaArrayList[antennaID] = antennaConfig;
  }

  /* Close the file */
  file.close();
}

uint8_t
CodebookParametric::GetNumberSectorsPerAntenna(AntennaId antennaID) const {
  const auto iter = m_antennaArrayList.find(antennaID);
  if (iter != m_antennaArrayList.cend()) {
    Ptr<ParametricAntennaConfig> antennaConfig =
        StaticCast<ParametricAntennaConfig>(iter->second);
    return antennaConfig->sectorList.size();
  } else {
    NS_ABORT_MSG("Cannot find the specified antenna ID=" << +antennaID);
  }
}

double CodebookParametric::GetTxGainDbi(double angle) {
  return GetTxGainDbi(angle, 0);
}

double CodebookParametric::GetRxGainDbi(double angle) {
  return GetRxGainDbi(angle, 0);
}

double CodebookParametric::GetTxGainDbi(double azimuth, double elevation) {
  return -1;
}

double CodebookParametric::GetRxGainDbi(double azimuth, double elevation) {
  return -1;
}

void CodebookParametric::CalculateArrayPatterns(AntennaId antennaID,
                                                uint16_t azimuthAngle,
                                                uint16_t elevationAngle) {
  NS_LOG_FUNCTION(this << +antennaID << azimuthAngle << elevationAngle);
  const auto iter = m_antennaArrayList.find(antennaID);
  Ptr<ParametricAntennaConfig> antennaConfig;
  Ptr<ParametricSectorConfig> sectorConfig;
  Ptr<ParametricAwvConfig> awvConfig;
  if (iter != m_antennaArrayList.cend()) {
    antennaConfig = StaticCast<ParametricAntennaConfig>(iter->second);
    for (auto sectorIter = antennaConfig->sectorList.begin();
         sectorIter != antennaConfig->sectorList.end(); ++sectorIter) {
      sectorConfig = DynamicCast<ParametricSectorConfig>(sectorIter->second);
      sectorConfig->CalculateArrayPattern(antennaConfig, azimuthAngle,
                                          elevationAngle);
      for (auto awvIt = sectorConfig->awvList.begin();
           awvIt != sectorConfig->awvList.end(); ++awvIt) {
        awvConfig = DynamicCast<ParametricAwvConfig>(*awvIt);
        awvConfig->CalculateArrayPattern(antennaConfig, azimuthAngle,
                                         elevationAngle);
      }
    }
    antennaConfig->GetQuasiOmniConfig()->CalculateArrayPattern(
        antennaConfig, azimuthAngle, elevationAngle);
  } else {
    NS_ABORT_MSG("Cannot find the specified Antenna ID=" << +antennaID);
  }
}

bool CodebookParametric::ArrayPatternsPrecalculated() const {
  return m_precalculatedPatterns;
}

void CodebookParametric::PrintWeights(WeightsVector weightsVector) {
  NS_LOG_FUNCTION(this);
  for (auto it = weightsVector.begin(); it != weightsVector.end() - 1; ++it) {
    std::cout << *it << ",";
  }
  std::cout << *(weightsVector.end() - 1) << std::endl;
}

void CodebookParametric::AppendSector(AntennaId antennaID, SectorId sectorID,
                                      SectorUsage sectorUsage,
                                      SectorType sectorType,
                                      WeightsVector &weightsVector) {
  const auto iter = m_antennaArrayList.find(antennaID);
  if (iter != m_antennaArrayList.cend()) {
    Ptr<ParametricAntennaConfig> antennaConfig =
        StaticCast<ParametricAntennaConfig>(iter->second);
    Ptr<ParametricSectorConfig> sectorConfig = Create<ParametricSectorConfig>();
    sectorConfig->sectorType = sectorType;
    sectorConfig->sectorUsage = sectorUsage;
    if ((sectorConfig->sectorUsage == SLS_SECTOR) ||
        (sectorConfig->sectorUsage == BHI_SLS_SECTOR)) {
      if ((sectorConfig->sectorType == TX_SECTOR) ||
          (sectorConfig->sectorType == TX_RX_SECTOR)) {
        m_totalTxSectors++;
      }
      if ((sectorConfig->sectorType == RX_SECTOR) ||
          (sectorConfig->sectorType == TX_RX_SECTOR)) {
        m_totalRxSectors++;
      }
    }

    /* Read sector weights and calculate directivity */
    sectorConfig->SetWeights(weightsVector);

    /* Check if the sector exists*/
    const auto sectorIter = antennaConfig->sectorList.find(sectorID);
    if (sectorIter != antennaConfig->sectorList.cend()) {
      /* If the sector exists, we need to remove it from the current sector
       * lists above*/
      NS_LOG_DEBUG("Updating existing sector in the codebook");
    } else {
      m_totalSectors += 1;
      NS_LOG_DEBUG("Appending new sector to the codebook");
    }
    antennaConfig->sectorList[sectorID] = sectorConfig;
  } else {
    NS_ABORT_MSG("Cannot find the specified Antenna ID=" << +antennaID);
  }
}

void CodebookParametric::CopyCodebook(const Ptr<Codebook> codebook) {
  NS_LOG_FUNCTION(this << codebook);
  Ptr<CodebookParametric> srcCodebook =
      DynamicCast<CodebookParametric>(codebook);
  Ptr<ParametricAntennaConfig> srcAntennaConfig;
  Ptr<ParametricSectorConfig> srcSectorConfig;
  /* Copy antenna arrays */
  for (auto arrayIt = srcCodebook->m_antennaArrayList.begin();
       arrayIt != srcCodebook->m_antennaArrayList.end(); ++arrayIt) {
    Ptr<ParametricAntennaConfig> dstAntennaConfig =
        Create<ParametricAntennaConfig>();
    /* Copy antenna array config */
    srcAntennaConfig = StaticCast<ParametricAntennaConfig>(arrayIt->second);
    dstAntennaConfig->CopyAntennaArray(srcAntennaConfig);
    /* Copy RF chain (Temporary) */
    Ptr<RfChain> dstRfChain = Create<RfChain>();
    dstRfChain->ConnectPhasedAntennaArray(arrayIt->first, dstAntennaConfig);
    dstAntennaConfig->rfChain = dstRfChain;
    m_rfChainList[arrayIt->first] = dstRfChain;
    /* Copy quasi-omni config */
    Ptr<ParametricPatternConfig> quasiPattern =
        Create<ParametricPatternConfig>();
    quasiPattern->SetWeights(
        srcAntennaConfig->GetQuasiOmniConfig()->GetWeights());
    quasiPattern->SetNormalizationFactor(
        srcAntennaConfig->GetQuasiOmniConfig()->GetNormalizationFactor());
    quasiPattern->SetArrayPattern(
        srcAntennaConfig->GetQuasiOmniConfig()->GetArrayPattern());
    dstAntennaConfig->SetQuasiOmniConfig(quasiPattern);
    for (auto sectorIter = srcAntennaConfig->sectorList.begin();
         sectorIter != srcAntennaConfig->sectorList.end(); ++sectorIter) {
      Ptr<ParametricSectorConfig> dstSectorConfig =
          Create<ParametricSectorConfig>();
      /* Copy sector config */
      srcSectorConfig = DynamicCast<ParametricSectorConfig>(sectorIter->second);
      dstSectorConfig->SetNormalizationFactor(
          srcSectorConfig->GetNormalizationFactor());
      dstSectorConfig->sectorType = srcSectorConfig->sectorType;
      dstSectorConfig->sectorUsage = srcSectorConfig->sectorUsage;
      dstSectorConfig->SetWeights(srcSectorConfig->GetWeights());
      dstSectorConfig->SetArrayPattern(srcSectorConfig->GetArrayPattern());
      dstAntennaConfig->sectorList[sectorIter->first] = dstSectorConfig;
    }
    m_antennaArrayList[arrayIt->first] = dstAntennaConfig;
  }
  m_precalculatedPatterns = srcCodebook->m_precalculatedPatterns;
  /* Set that we have cloned another codebook to avoid*/
  m_cloned = true;
  /* Call parent class. */
  Codebook::CopyCodebook(srcCodebook);
}

Complex
CodebookParametric::GetAntennaArrayPattern(Ptr<PatternConfig> config,
                                           uint16_t azimuthAngle,
                                           uint16_t elevationAngle) const {
  Ptr<ParametricPatternConfig> parametricConfig =
      DynamicCast<ParametricPatternConfig>(config);
  if (m_precalculatedPatterns) {
    return parametricConfig->GetArrayPattern()[azimuthAngle][elevationAngle];
  } else {
    return parametricConfig->GetArrayPattern(azimuthAngle, elevationAngle);
  }
}

} // namespace ns3
