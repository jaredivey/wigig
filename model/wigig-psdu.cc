/*
 * Copyright (c) 2019 Universita' degli Studi di Napoli Federico II
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Stefano Avallone <stavallo@unina.it>
 */

#include "wigig-psdu.h"

#include "wigig-mpdu-aggregator.h"

#include "ns3/ampdu-subframe-header.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/wifi-mac-trailer.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigPsdu");

WigigPsdu::WigigPsdu(Ptr<const Packet> p, const WigigMacHeader &header)
    : m_isSingle(false), m_isShortSsw(false) {
  m_mpduList.push_back(Create<WigigMacQueueItem>(p, header));
  m_size = header.GetSerializedSize() + p->GetSize() + WIFI_MAC_FCS_LENGTH;
}

WigigPsdu::WigigPsdu(Ptr<WigigMacQueueItem> mpdu, bool isSingle)
    : m_isSingle(isSingle) {
  m_mpduList.push_back(mpdu);
  m_size = mpdu->GetSize();
  /* Short SSW frames have a payload of 6 octets */
  m_isShortSsw = m_size == 6;

  if (isSingle) {
    m_size += 4; // A-MPDU Subframe header size
  }
}

WigigPsdu::WigigPsdu(Ptr<const WigigMacQueueItem> mpdu, bool isSingle)
    : WigigPsdu(Create<WigigMacQueueItem>(*mpdu), isSingle) {}

WigigPsdu::WigigPsdu(std::vector<Ptr<WigigMacQueueItem>> mpduList)
    : m_isSingle(mpduList.size() == 1), m_mpduList(mpduList),
      m_isShortSsw(false) {
  NS_ABORT_MSG_IF(mpduList.empty(),
                  "Cannot initialize a WigigPsdu with an empty MPDU list");

  m_size = 0;
  for (auto &mpdu : m_mpduList) {
    m_size = WigigMpduAggregator::GetSizeIfAggregated(mpdu->GetSize(), m_size);
  }
}

WigigPsdu::~WigigPsdu() {}

bool WigigPsdu::IsSingle() const { return m_isSingle; }

bool WigigPsdu::IsAggregate() const {
  return (m_mpduList.size() > 1 || m_isSingle);
}

bool WigigPsdu::IsShortSsw() const { return m_isShortSsw; }

Ptr<const Packet> WigigPsdu::GetPacket() const {
  Ptr<Packet> packet = Create<Packet>();
  if (m_mpduList.size() == 1 && !m_isSingle) {
    packet = m_mpduList.at(0)->GetPacket()->Copy();
    packet->AddHeader(m_mpduList.at(0)->GetHeader());
    AddWifiMacTrailer(packet);
  } else if (m_isSingle) {
    WigigMpduAggregator::Aggregate(m_mpduList.at(0), packet, true);
  } else {
    for (auto &mpdu : m_mpduList) {
      WigigMpduAggregator::Aggregate(mpdu, packet, false);
    }
  }
  return packet;
}

Mac48Address WigigPsdu::GetAddr1() const {
  Mac48Address ra = m_mpduList.at(0)->GetHeader().GetAddr1();
  // check that the other MPDUs have the same RA
  for (std::size_t i = 1; i < m_mpduList.size(); i++) {
    if (m_mpduList.at(i)->GetHeader().GetAddr1() != ra) {
      NS_ABORT_MSG("MPDUs in an A-AMPDU must have the same receiver address");
    }
  }
  return ra;
}

Mac48Address WigigPsdu::GetAddr2() const {
  Mac48Address ta = m_mpduList.at(0)->GetHeader().GetAddr2();
  // check that the other MPDUs have the same TA
  for (std::size_t i = 1; i < m_mpduList.size(); i++) {
    if (m_mpduList.at(i)->GetHeader().GetAddr2() != ta) {
      NS_ABORT_MSG(
          "MPDUs in an A-AMPDU must have the same transmitter address");
    }
  }
  return ta;
}

Time WigigPsdu::GetDuration() const {
  Time duration = m_mpduList.at(0)->GetHeader().GetDuration();
  // check that the other MPDUs have the same Duration/ID
  for (std::size_t i = 1; i < m_mpduList.size(); i++) {
    if (m_mpduList.at(i)->GetHeader().GetDuration() != duration) {
      NS_ABORT_MSG("MPDUs in an A-AMPDU must have the same Duration/ID");
    }
  }
  return duration;
}

void WigigPsdu::SetDuration(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  for (auto &mpdu : m_mpduList) {
    mpdu->GetHeader().SetDuration(duration);
  }
}

std::set<uint8_t> WigigPsdu::GetTids() const {
  std::set<uint8_t> s;
  for (auto &mpdu : m_mpduList) {
    if (mpdu->GetHeader().IsQosData()) {
      s.insert(mpdu->GetHeader().GetQosTid());
    }
  }
  return s;
}

WigigMacHeader::QosAckPolicy WigigPsdu::GetAckPolicyForTid(uint8_t tid) const {
  NS_LOG_FUNCTION(this << +tid);
  WigigMacHeader::QosAckPolicy policy;
  auto it = m_mpduList.begin();
  bool found = false;

  // find the first QoS Data frame with the given TID
  do {
    if ((*it)->GetHeader().IsQosData() &&
        (*it)->GetHeader().GetQosTid() == tid) {
      policy = (*it)->GetHeader().GetQosAckPolicy();
      found = true;
    }
    it++;
  } while (!found && it != m_mpduList.end());

  NS_ABORT_MSG_IF(!found, "No QoS Data frame in the PSDU");

  // check that the other QoS Data frames with the given TID have the same ack
  // policy
  while (it != m_mpduList.end()) {
    if ((*it)->GetHeader().IsQosData() &&
        (*it)->GetHeader().GetQosTid() == tid &&
        (*it)->GetHeader().GetQosAckPolicy() != policy) {
      NS_ABORT_MSG("QoS Data frames with the same TID must have the same QoS "
                   "Ack Policy");
    }
    it++;
  }
  return policy;
}

void WigigPsdu::SetAckPolicyForTid(uint8_t tid,
                                   WigigMacHeader::QosAckPolicy policy) {
  NS_LOG_FUNCTION(this << +tid << policy);
  for (auto &mpdu : m_mpduList) {
    if (mpdu->GetHeader().IsQosData() && mpdu->GetHeader().GetQosTid() == tid) {
      mpdu->GetHeader().SetQosAckPolicy(policy);
    }
  }
}

uint16_t WigigPsdu::GetMaxDistFromStartingSeq(uint16_t startingSeq) const {
  NS_LOG_FUNCTION(this << startingSeq);

  uint16_t maxDistFromStartingSeq = 0;
  bool foundFirst = false;

  for (auto &mpdu : m_mpduList) {
    uint16_t currSeqNum = mpdu->GetHeader().GetSequenceNumber();

    if (mpdu->GetHeader().IsQosData() &&
        !QosUtilsIsOldPacket(startingSeq, currSeqNum)) {
      uint16_t currDistToStartingSeq =
          (currSeqNum - startingSeq + SEQNO_SPACE_SIZE) % SEQNO_SPACE_SIZE;

      if (!foundFirst || currDistToStartingSeq > maxDistFromStartingSeq) {
        foundFirst = true;
        maxDistFromStartingSeq = currDistToStartingSeq;
      }
    }
  }

  if (!foundFirst) {
    NS_LOG_DEBUG("All QoS Data frames in this PSDU are old frames");
    return SEQNO_SPACE_SIZE;
  }
  NS_LOG_DEBUG("Returning " << maxDistFromStartingSeq);
  return maxDistFromStartingSeq;
}

uint32_t WigigPsdu::GetSize() const { return m_size; }

const WigigMacHeader &WigigPsdu::GetHeader(std::size_t i) const {
  return m_mpduList.at(i)->GetHeader();
}

WigigMacHeader &WigigPsdu::GetHeader(std::size_t i) {
  return m_mpduList.at(i)->GetHeader();
}

Ptr<const Packet> WigigPsdu::GetPayload(std::size_t i) const {
  return m_mpduList.at(i)->GetPacket();
}

Time WigigPsdu::GetTimeStamp(std::size_t i) const {
  return m_mpduList.at(i)->GetTimeStamp();
}

Ptr<Packet> WigigPsdu::GetAmpduSubframe(std::size_t i) const {
  NS_ASSERT(i < m_mpduList.size());
  Ptr<Packet> subframe = m_mpduList.at(i)->GetProtocolDataUnit();
  subframe->AddHeader(WigigMpduAggregator::GetAmpduSubframeHeader(
      subframe->GetSize(), m_isSingle));
  size_t padding = GetAmpduSubframeSize(i) - subframe->GetSize();
  if (padding > 0) {
    Ptr<Packet> pad = Create<Packet>(padding);
    subframe->AddAtEnd(pad);
  }
  return subframe;
}

std::size_t WigigPsdu::GetAmpduSubframeSize(std::size_t i) const {
  NS_ASSERT(i < m_mpduList.size());
  size_t subframeSize = 4; // A-MPDU Subframe header size
  subframeSize += m_mpduList.at(i)->GetSize();
  if (i != m_mpduList.size() - 1) // add padding if not last
  {
    subframeSize += WigigMpduAggregator::CalculatePadding(subframeSize);
  }
  return subframeSize;
}

std::size_t WigigPsdu::GetNMpdus() const { return m_mpduList.size(); }

std::vector<Ptr<WigigMacQueueItem>>::const_iterator WigigPsdu::begin() const {
  return m_mpduList.begin();
}

std::vector<Ptr<WigigMacQueueItem>>::iterator WigigPsdu::begin() {
  return m_mpduList.begin();
}

std::vector<Ptr<WigigMacQueueItem>>::const_iterator WigigPsdu::end() const {
  return m_mpduList.end();
}

std::vector<Ptr<WigigMacQueueItem>>::iterator WigigPsdu::end() {
  return m_mpduList.end();
}

void WigigPsdu::Print(std::ostream &os) const {
  os << "size=" << m_size;
  if (IsAggregate()) {
    os << ", A-MPDU of " << GetNMpdus() << " MPDUs";
    for (const auto &mpdu : m_mpduList) {
      os << " (" << *mpdu << ")";
    }
  } else {
    os << ", " << ((m_isSingle) ? "S-MPDU" : "normal MPDU") << " ("
       << *(m_mpduList.at(0)) << ")";
  }
}

std::ostream &operator<<(std::ostream &os, const WigigPsdu &psdu) {
  psdu.Print(os);
  return os;
}

} // namespace ns3
