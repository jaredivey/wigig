/*
 * Copyright (c) 2005,2006 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Stefano Avallone <stavallo@unina.it>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "mac-low.h"

#include "sta-wigig-mac.h"
#include "wigig-ack-policy-selector.h"
#include "wigig-mac-queue.h"
#include "wigig-mac.h"
#include "wigig-mpdu-aggregator.h"
#include "wigig-msdu-aggregator.h"
#include "wigig-net-device.h"
#include "wigig-phy-common.h"
#include "wigig-phy.h"
#include "wigig-psdu.h"
#include "wigig-qos-txop.h"
#include "wigig-remote-station-manager.h"

#include "ns3/ampdu-tag.h"
#include "ns3/ctrl-headers.h"
#include "ns3/log.h"
#include "ns3/mgt-headers.h"
#include "ns3/simulator.h"
#include "ns3/snr-tag.h"
#include "ns3/wifi-mac-trailer.h"
#include "ns3/wifi-phy-listener.h"
#include "ns3/wifi-utils.h"

#include <algorithm>

#undef NS_LOG_APPEND_CONTEXT
#define NS_LOG_APPEND_CONTEXT std::clog << "[mac=" << m_self << "] "

// Time (in nanoseconds) to be added to the PSDU duration to yield the duration
// of the timer that is started when the PHY indicates the start of the
// reception of a frame and we are waiting for a response.
#define PSDU_DURATION_SAFEGUARD 400

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("MacLow");

/**
 * Listener for PHY events. Forwards to MacLow
 */
class PhyMacLowListener : public ns3::WifiPhyListener {
public:
  /**
   * Create a PhyMacLowListener for the given MacLow.
   *
   * \param macLow
   */
  PhyMacLowListener(ns3::MacLow *macLow) : m_macLow(macLow) {}

  ~PhyMacLowListener() override {}

  void NotifyRxStart(Time duration) override {}

  void NotifyRxEndOk() override {}

  void NotifyRxEndError() override {}

  void NotifyTxStart(Time duration, double txPowerDbm) override {}

  void NotifyCcaBusyStart(Time duration, WifiChannelListType channelType,
                          const std::vector<Time> &per20MhzDurations) override {
  }

  void NotifySwitchingStart(Time duration) override {
    m_macLow->NotifySwitchingStartNow(duration);
  }

  void NotifySleep() override { m_macLow->NotifySleepNow(); }

  void NotifyOff() override { m_macLow->NotifyOffNow(); }

  void NotifyWakeup() override {}

  void NotifyOn() override {}

private:
  ns3::MacLow *m_macLow; ///< the MAC
};

MacLow::MacLow()
    : m_msduAggregator(nullptr), m_mpduAggregator(nullptr),
      m_normalAckTimeoutEvent(), m_blockAckTimeoutEvent(), m_ctsTimeoutEvent(),
      m_sendCtsEvent(), m_sendAckEvent(), m_sendDataEvent(), m_waitIfsEvent(),
      m_endTxNoAckEvent(), m_currentPacket(nullptr), m_currentTxop(nullptr),
      m_lastNavStart(Seconds(0)), m_lastNavDuration(Seconds(0)),
      m_lastBeacon(Seconds(0)), m_promisc(false), m_phyMacLowListener(nullptr),
      m_ctsToSelfSupported(false), m_transmissionSuspended(false),
      m_restoredSuspendedTransmission(false), m_servingSls(false) {
  NS_LOG_FUNCTION(this);
}

MacLow::~MacLow() { NS_LOG_FUNCTION(this); }

TypeId MacLow::GetTypeId() {
  static TypeId tid = TypeId("ns3::MacLow")
                          .SetParent<Object>()
                          .SetGroupName("Wigig")
                          .AddConstructor<MacLow>();
  return tid;
}

void MacLow::SetupPhyMacLowListener(const Ptr<WigigPhy> phy) {
  m_phyMacLowListener = new PhyMacLowListener(this);
  phy->RegisterListener(m_phyMacLowListener);
}

void MacLow::RemovePhyMacLowListener(Ptr<WigigPhy> phy) {
  if (m_phyMacLowListener) {
    phy->UnregisterListener(m_phyMacLowListener);
    delete m_phyMacLowListener;
    m_phyMacLowListener = nullptr;
  }
}

void MacLow::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_normalAckTimeoutEvent.Cancel();
  m_blockAckTimeoutEvent.Cancel();
  m_ctsTimeoutEvent.Cancel();
  m_sendCtsEvent.Cancel();
  m_sendAckEvent.Cancel();
  m_sendDataEvent.Cancel();
  m_waitIfsEvent.Cancel();
  m_endTxNoAckEvent.Cancel();
  m_msduAggregator = nullptr;
  m_mpduAggregator = nullptr;
  m_phy = nullptr;
  m_stationManager = nullptr;
  if (m_phyMacLowListener) {
    delete m_phyMacLowListener;
    m_phyMacLowListener = nullptr;
  }
}

void MacLow::CancelAllEvents() {
  NS_LOG_FUNCTION(this);
  bool oneRunning = false;
  if (m_normalAckTimeoutEvent.IsRunning()) {
    m_normalAckTimeoutEvent.Cancel();
    oneRunning = true;
  }
  if (m_blockAckTimeoutEvent.IsRunning()) {
    m_blockAckTimeoutEvent.Cancel();
    oneRunning = true;
  }
  if (m_ctsTimeoutEvent.IsRunning()) {
    m_ctsTimeoutEvent.Cancel();
    oneRunning = true;
  }
  if (m_sendCtsEvent.IsRunning()) {
    m_sendCtsEvent.Cancel();
    oneRunning = true;
  }
  if (m_sendAckEvent.IsRunning()) {
    m_sendAckEvent.Cancel();
    oneRunning = true;
  }
  if (m_sendDataEvent.IsRunning()) {
    m_sendDataEvent.Cancel();
    oneRunning = true;
  }
  if (m_waitIfsEvent.IsRunning()) {
    m_waitIfsEvent.Cancel();
    oneRunning = true;
  }
  if (m_endTxNoAckEvent.IsRunning()) {
    m_endTxNoAckEvent.Cancel();
    oneRunning = true;
  }
  if (oneRunning && m_currentTxop) {
    m_currentTxop->Cancel();
    m_currentTxop = nullptr;
  }
}

void MacLow::SetPhy(const Ptr<WigigPhy> phy) {
  m_phy = phy;
  m_phy->TraceConnectWithoutContext(
      "PhyRxPayloadBegin", MakeCallback(&MacLow::RxStartIndication, this));
  m_phy->SetReceiveOkCallback(
      MakeCallback(&MacLow::DeaggregateAmpduAndReceive, this));
  m_phy->SetReceiveErrorCallback(MakeCallback(&MacLow::ReceiveError, this));
  SetupPhyMacLowListener(phy);
}

Ptr<WigigPhy> MacLow::GetPhy() const { return m_phy; }

Ptr<WigigQosTxop> MacLow::GetEdca(uint8_t tid) const {
  auto it = m_edca.find(QosUtilsMapTidToAc(tid));
  NS_ASSERT(it != m_edca.end());
  return it->second;
}

void MacLow::SetMac(const Ptr<WigigMac> mac) { m_mac = mac; }

void MacLow::SetWifiRemoteStationManager(
    const Ptr<WigigRemoteStationManager> manager) {
  m_stationManager = manager;
}

Ptr<WigigMsduAggregator> MacLow::GetWigigMsduAggregator() const {
  return m_msduAggregator;
}

Ptr<WigigMpduAggregator> MacLow::GetWigigMpduAggregator() const {
  return m_mpduAggregator;
}

void MacLow::SetMsduAggregator(const Ptr<WigigMsduAggregator> aggr) {
  NS_LOG_FUNCTION(this << aggr);
  m_msduAggregator = aggr;
}

void MacLow::SetMpduAggregator(const Ptr<WigigMpduAggregator> aggr) {
  NS_LOG_FUNCTION(this << aggr);
  m_mpduAggregator = aggr;
}

void MacLow::SetAddress(Mac48Address ad) { m_self = ad; }

void MacLow::SetAckTimeout(Time ackTimeout) { m_ackTimeout = ackTimeout; }

void MacLow::SetBasicBlockAckTimeout(Time blockAckTimeout) {
  m_basicBlockAckTimeout = blockAckTimeout;
}

void MacLow::SetCompressedBlockAckTimeout(Time blockAckTimeout) {
  m_compressedBlockAckTimeout = blockAckTimeout;
}

void MacLow::SetCtsToSelfSupported(bool enable) {
  m_ctsToSelfSupported = enable;
}

bool MacLow::GetCtsToSelfSupported() const { return m_ctsToSelfSupported; }

void MacLow::SetSifs(Time sifs) { m_sifs = sifs; }

void MacLow::SetSbifs(Time sbifs) { m_sbifs = sbifs; }

void MacLow::SetMbifs(Time mbifs) { m_mbifs = mbifs; }

void MacLow::SetLbifs(Time lbifs) { m_lbifs = lbifs; }

void MacLow::SetSlotTime(Time slotTime) { m_slotTime = slotTime; }

void MacLow::SetPifs(Time pifs) { m_pifs = pifs; }

void MacLow::SetRifs(Time rifs) { m_rifs = rifs; }

void MacLow::SetBeaconInterval(Time interval) { m_beaconInterval = interval; }

void MacLow::SetBssid(Mac48Address bssid) { m_bssid = bssid; }

void MacLow::SetPromisc() { m_promisc = true; }

Mac48Address MacLow::GetAddress() const { return m_self; }

Time MacLow::GetAckTimeout() const { return m_ackTimeout; }

Time MacLow::GetBasicBlockAckTimeout() const { return m_basicBlockAckTimeout; }

Time MacLow::GetCompressedBlockAckTimeout() const {
  return m_compressedBlockAckTimeout;
}

Time MacLow::GetSifs() const { return m_sifs; }

Time MacLow::GetRifs() const { return m_rifs; }

Time MacLow::GetSlotTime() const { return m_slotTime; }

Time MacLow::GetPifs() const { return m_pifs; }

Time MacLow::GetSbifs() const { return m_sbifs; }

Time MacLow::GetMbifs() const { return m_mbifs; }

Time MacLow::GetLbifs() const { return m_lbifs; }

Mac48Address MacLow::GetBssid() const { return m_bssid; }

Time MacLow::GetBeaconInterval() const { return m_beaconInterval; }

void MacLow::SetRxCallback(Callback<void, Ptr<WigigMacQueueItem>> callback) {
  m_rxCallback = callback;
}

void MacLow::RegisterWigigChannelAccessManager(
    Ptr<WigigChannelAccessManager> channelAccessManager) {
  m_channelAccessManagers.push_back(channelAccessManager);
}

bool MacLow::IsCurrentAllocationEmpty() const { return !m_currentAllocation; }

void MacLow::ResumeTransmission(Time duration, Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << duration << txop);

  NS_ASSERT_MSG(!IsCurrentAllocationEmpty(),
                "Restored allocation should not be empty");

  NS_LOG_DEBUG(
      "IsAmpdu=" << m_currentAllocation->psdu->IsAggregate()
                 << ", PacketSize=" << m_currentAllocation->psdu->GetSize()
                 << ", seq=0x" << std::hex
                 << m_currentAllocation->psdu->GetHeader(0).GetSequenceControl()
                 << std::dec);

  /* Restore the variables associated to the current allocation */
  m_restoredSuspendedTransmission = false;
  m_currentPacket = m_currentAllocation->psdu;
  m_txParams = m_currentAllocation->txParams;
  m_currentTxVector = m_currentAllocation->txVector;

  /* Check if the remaining time is enough to resume previously suspended
   * transmission */
  Time transactionTime = CalculateWiGigTransactionTime(m_currentPacket);
  NS_LOG_DEBUG("TransactionTime=" << transactionTime << ", RemainingTime="
                                  << txop->GetAllocationRemaining());

  if (transactionTime <= duration) {
    /* This only applies for service period */
    CancelAllEvents();
    m_currentTxop = txop;

    if (m_txParams.MustSendRts()) {
      SendRtsForPacket();
    } else {
      SendDataPacket();
    }

    /* When this method completes, either we have taken ownership of the medium
     * or the device switched off in the meantime. */
    NS_ASSERT(m_phy->IsStateTx() || m_phy->IsStateOff());
  } else {
    m_transmissionSuspended = true;
  }

  /* Remove suspended allocation related parameters as we've restored it */
  m_allocationPeriodsTable.erase(m_currentAllocationId);
}

void MacLow::AbortSuspendedTransmission() {
  NS_LOG_FUNCTION(this);
  m_restoredSuspendedTransmission = false;
  m_currentAllocation = nullptr;
  m_allocationPeriodsTable.erase(m_currentAllocationId);
}

void MacLow::RestoreAllocationParameters(AllocationId allocationId) {
  NS_LOG_FUNCTION(this << +allocationId);
  m_transmissionSuspended = false; /* Transmission is not suspended anymore */
  m_currentAllocationId = allocationId;
  /* Find the stored parameters and packets for the provided allocation */
  AllocationPeriodsTableCI it =
      m_allocationPeriodsTable.find(m_currentAllocationId);
  if (it != m_allocationPeriodsTable.end()) {
    NS_LOG_DEBUG(
        "Restored allocation parameters for AllocationId=" << +allocationId);
    m_currentAllocation = it->second;
    m_restoredSuspendedTransmission = true;
  } else {
    NS_LOG_DEBUG(
        "No allocation parameters stored for AllocationId=" << +allocationId);
    m_restoredSuspendedTransmission = false;
    m_currentAllocation = nullptr;
  }
}

void MacLow::StoreAllocationParameters() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("CurrentPacket=" << m_currentPacket);
  if (m_currentPacket && m_currentPacket->GetHeader(0).IsQosData()) {
    /* Since CurrentPacket is not empty it means we've suspended an ongoing
     * transmission */
    Ptr<AllocationParameters> allocationParams = Create<AllocationParameters>();
    allocationParams->psdu = m_currentPacket;
    allocationParams->txParams = m_txParams;
    allocationParams->txVector = m_currentTxVector;
    allocationParams->txop = m_currentTxop;
    m_allocationPeriodsTable[m_currentAllocationId] = allocationParams;
    NS_LOG_DEBUG("PSDU Size="
                 << m_currentPacket->GetSize() << ", seq=0x" << std::hex
                 << m_currentPacket->GetHeader(0).GetSequenceControl()
                 << std::dec << ", Txop=" << m_currentTxop);
  }
  m_currentPacket = nullptr;
  m_currentAllocation = nullptr;
}

void MacLow::EndAllocationPeriod() {
  NS_LOG_FUNCTION(this);
  CancelAllEvents();
  StoreAllocationParameters();
  if (m_navCounterResetCtsMissed.IsRunning()) {
    m_navCounterResetCtsMissed.Cancel();
  }
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = Seconds(0);
  m_currentTxop = nullptr;
}

bool MacLow::IsTransmissionSuspended() const { return m_transmissionSuspended; }

bool MacLow::CompletedSuspendedPsduTransmission(Ptr<WigigTxop> txop) const {
  NS_LOG_FUNCTION(this << txop);
  NS_LOG_DEBUG(
      "Restored Suspended Transmission=" << m_restoredSuspendedTransmission);
  return !(m_restoredSuspendedTransmission && !IsCurrentAllocationEmpty() &&
           m_currentAllocation->txop == txop);
}

void MacLow::StartTransmission(Ptr<WigigMacQueueItem> mpdu,
                               MacLowTransmissionParameters params,
                               Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << *mpdu << params << txop);
  if (m_phy->IsStateOff()) {
    NS_LOG_DEBUG("Cannot start TX because device is OFF");
    return;
  }
  /* m_currentPacket is not NULL because someone started
   * a transmission and was interrupted before one of:
   *   - ctsTimeout
   *   - sendDataAfterCTS
   * expired. This means that one of these timers is still
   * running. They are all cancelled below anyway by the
   * call to CancelAllEvents (because of at least one
   * of these two timers) which will trigger a call to the
   * previous listener's cancel method.
   *
   * This typically happens because the high-priority
   * QapScheduler has taken access to the channel from
   * one of the EDCA of the QAP.
   */
  m_currentPacket = Create<WigigPsdu>(mpdu, false);
  const WigigMacHeader &hdr = mpdu->GetHeader();
  CancelAllEvents();
  m_currentTxop = txop;
  m_txParams = params;
  if (hdr.IsCtl()) {
    m_currentTxVector = GetRtsTxVector(mpdu);
  } else {
    m_currentTxVector = GetDataTxVector(mpdu);
  }

  /* The packet received by this function can be any of the following:
   * (a) a management frame dequeued from the WigigTxop
   * (b) a non-QoS data frame dequeued from the WigigTxop
   * (c) a non-broadcast QoS Data frame peeked or dequeued from a WigigQosTxop
   * (d) a broadcast QoS data or DELBA Request frame dequeued from a
   * WigigQosTxop (e) a BlockAckReq or ADDBA Request frame (f) a fragment of
   * non-QoS/QoS Data frame dequeued from the WigigTxop/WigigQosTxop
   */
  if (hdr.IsQosData() && !hdr.GetAddr1().IsBroadcast() &&
      !hdr.IsMoreFragments() && hdr.GetFragmentNumber() == 0) {
    // We get here if the received packet is a non-broadcast QoS data frame
    uint8_t tid = hdr.GetQosTid();
    Ptr<WigigQosTxop> qosWigigTxop =
        m_edca.find(QosUtilsMapTidToAc(tid))->second;

    // if a TXOP limit exists, compute the remaining TXOP duration
    Time txopLimit = Time::Min();
    if (m_currentTxop->GetTxopLimit().IsStrictlyPositive() ||
        m_currentTxop->GetAllocationRemaining().IsStrictlyPositive()) {
      txopLimit = m_currentTxop->GetPpduDurationLimit(mpdu, params);
    }
    // WigigQosTxop may send us a peeked frame
    Ptr<const WigigMacQueueItem> tmp = qosWigigTxop->PeekFrameForTransmission();
    bool isPeeked = (tmp && tmp->GetPacket() == mpdu->GetPacket());

    Ptr<WigigMacQueueItem> newMpdu;
    // If the frame has been peeked, dequeue it if it meets the size and
    // duration constraints
    if (isPeeked) {
      newMpdu = qosWigigTxop->DequeuePeekedFrame(mpdu, m_currentTxVector, true,
                                                 0, txopLimit);
    } else if (IsWithinSizeAndTimeLimits(mpdu, m_currentTxVector, 0,
                                         txopLimit)) {
      newMpdu = mpdu;
    }

    if (!newMpdu) {
      // if the frame has been dequeued, then there is no BA agreement with the
      // receiver (otherwise the frame would have been peeked). Hence, the frame
      // has been sent under Normal Ack policy, not acknowledged and now
      // retransmitted. If we cannot send it now, let the WigigQosTxop
      // retransmit it again. If the frame has been just peeked, reset the
      // current packet at WigigQosTxop.
      if (isPeeked) {
        m_currentPacket = nullptr;
        qosWigigTxop->UpdateCurrentPacket(
            Create<WigigMacQueueItem>(nullptr, WigigMacHeader()));
      }
      return;
    }
    // Update the current packet at WigigQosTxop, given that A-MSDU aggregation
    // may have been performed on the peeked frame
    qosWigigTxop->UpdateCurrentPacket(newMpdu);

    /* Since we might perform A-MPDU aggregation, update txopLimit to take into
     * account the correct size of A-MPDU aggregation */
    if (m_currentTxop->GetTxopLimit().IsStrictlyPositive() ||
        m_currentTxop->GetAllocationRemaining().IsStrictlyPositive()) {
      /* Get temporary TransmissionLow Parameters for A-MPDU */
      MacLowTransmissionParameters tempParams =
          qosWigigTxop->GetAckPolicySelector()->GetTemporaryParams(
              m_currentPacket, params);
      txopLimit = m_currentTxop->GetPpduDurationLimit(mpdu, tempParams);
    }

    // Perform MPDU aggregation if possible
    std::vector<Ptr<WigigMacQueueItem>> mpduList;
    if (m_mpduAggregator) {
      mpduList =
          m_mpduAggregator->GetNextAmpdu(newMpdu, m_currentTxVector, txopLimit);
    }

    if (mpduList.size() > 1) {
      m_currentPacket = Create<WigigPsdu>(mpduList);

      NS_LOG_DEBUG("tx unicast A-MPDU containing " << mpduList.size()
                                                   << " MPDUs");
      qosWigigTxop->SetAmpduExist(hdr.GetAddr1(), true);
    } else // HT
    {
      m_currentPacket = Create<WigigPsdu>(newMpdu, false);
    }

    // A QoS WigigTxop must have an installed ack policy selector
    NS_ASSERT(qosWigigTxop->GetAckPolicySelector());
    qosWigigTxop->GetAckPolicySelector()->UpdateTxParams(m_currentPacket,
                                                         m_txParams);
    qosWigigTxop->GetAckPolicySelector()->SetAckPolicy(m_currentPacket,
                                                       m_txParams);
  }

  NS_LOG_DEBUG("startTx size=" << m_currentPacket->GetSize()
                               << ", to=" << m_currentPacket->GetAddr1()
                               << ", txop=" << m_currentTxop);

  if (m_txParams.MustSendRts()) {
    SendRtsForPacket();
  } else {
    if (m_ctsToSelfSupported && NeedCtsToSelf()) {
      SendCtsToSelf();
    } else {
      SendDataPacket();
    }
  }

  /* When this method completes, either we have taken ownership of the medium or
   * the device switched off in the meantime. */
  NS_ASSERT(m_phy->IsStateTx() || m_phy->IsStateOff());
}

void MacLow::TransmitSingleFrame(Ptr<WigigMacQueueItem> mpdu,
                                 MacLowTransmissionParameters params,
                                 Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << *mpdu << params << txop);
  if (m_phy->IsStateOff()) {
    NS_LOG_DEBUG("Cannot start TX because device is OFF");
    return;
  }
  m_currentPacket = Create<WigigPsdu>(mpdu, false);
  CancelAllEvents();
  m_currentTxop = txop;
  m_txParams = params;
  m_currentTxVector = GetDmgTxVector(mpdu);
  SendDataPacket();

  /* When this method completes, either we have taken ownership of the medium or
   * the device switched off in the meantime. */
  NS_ASSERT(m_phy->IsStateTx() || m_phy->IsStateOff());
}

void MacLow::StartTransmissionImmediately(Ptr<WigigMacQueueItem> mpdu,
                                          MacLowTransmissionParameters params,
                                          TransmissionOkCallback callback) {
  NS_LOG_FUNCTION(this << *mpdu << params);
  if (m_phy->IsStateOff()) {
    NS_LOG_DEBUG("Cannot start TX because device is OFF");
    return;
  }
  m_currentPacket = Create<WigigPsdu>(mpdu, false);
  CancelAllEvents();
  m_currentTxop = nullptr;
  m_transmissionCallback = callback;
  m_txParams = params;
  m_currentTxVector = GetDmgTxVector(mpdu);
  SendDataPacket();

  /* When this method completes, either we have taken ownership of the medium or
   * the device switched off in the meantime. */
  NS_ASSERT(m_phy->IsStateTx() || m_phy->IsStateOff());
}

void MacLow::SlsPhaseStarted() {
  NS_LOG_FUNCTION(this);
  m_servingSls = true;
  /* We always prioritize SLS over any data transmission, so we cancel any
   * events. */
  if (m_normalAckTimeoutEvent.IsRunning()) {
    m_normalAckTimeoutEvent.Cancel();
  }
  if (m_blockAckTimeoutEvent.IsRunning()) {
    m_blockAckTimeoutEvent.Cancel();
  }
  if (m_sendAckEvent.IsRunning()) {
    m_sendAckEvent.Cancel();
  }
}

void MacLow::SlsPhaseEnded() {
  NS_LOG_FUNCTION(this);
  m_servingSls = false;
}

bool MacLow::IsPerformingSls() const { return m_servingSls; }

bool MacLow::NeedCtsToSelf() const {
  WigigTxVector dataTxVector = GetDataTxVector(*m_currentPacket->begin());
  return m_stationManager->NeedCtsToSelf(dataTxVector);
}

bool MacLow::IsWithinSizeAndTimeLimits(Ptr<const WigigMacQueueItem> mpdu,
                                       const WigigTxVector &txVector,
                                       uint32_t ampduSize,
                                       Time ppduDurationLimit) {
  NS_ASSERT(mpdu && mpdu->GetHeader().IsQosData());

  return IsWithinSizeAndTimeLimits(
      mpdu->GetSize(), mpdu->GetHeader().GetAddr1(),
      mpdu->GetHeader().GetQosTid(), txVector, ampduSize, ppduDurationLimit);
}

bool MacLow::IsWithinSizeAndTimeLimits(uint32_t mpduSize, Mac48Address receiver,
                                       uint8_t tid,
                                       const WigigTxVector &txVector,
                                       uint32_t ampduSize,
                                       Time ppduDurationLimit) {
  NS_LOG_FUNCTION(this << mpduSize << receiver << +tid << txVector << ampduSize
                       << ppduDurationLimit);

  if (ppduDurationLimit != Time::Min() && ppduDurationLimit.IsNegative()) {
    return false;
  }

  WifiModulationClass modulation = txVector.GetMode().GetModulationClass();

  uint32_t maxAmpduSize = 0;
  if (GetWigigMpduAggregator()) {
    maxAmpduSize =
        GetWigigMpduAggregator()->GetMaxAmpduSize(receiver, tid, modulation);
  }

  // If maxAmpduSize is null, then ampduSize must be null as well
  NS_ASSERT(maxAmpduSize || ampduSize == 0);

  uint32_t ppduPayloadSize = mpduSize;

  // compute the correct size for A-MPDUs and S-MPDUs
  if (ampduSize > 0) {
    ppduPayloadSize =
        GetWigigMpduAggregator()->GetSizeIfAggregated(mpduSize, ampduSize);
  }

  if (maxAmpduSize > 0 && ppduPayloadSize > maxAmpduSize) {
    NS_LOG_DEBUG("the frame does not meet the constraint on max A-MPDU size");
    return false;
  }

  // Get the maximum PPDU Duration based on the preamble type
  Time maxPpduDuration = GetPpduMaxTime(txVector.GetPreambleType());

  Time txTime = m_phy->CalculateTxDuration(ppduPayloadSize, txVector,
                                           m_phy->GetFrequency());

  if ((ppduDurationLimit.IsStrictlyPositive() && txTime > ppduDurationLimit) ||
      (maxPpduDuration.IsStrictlyPositive() && txTime > maxPpduDuration)) {
    NS_LOG_DEBUG("the frame does not meet the constraint on max PPDU duration");
    return false;
  }

  return true;
}

void MacLow::RxStartIndication(const WigigTxVector &txVector,
                               Time psduDuration) {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("PSDU reception started for " << psduDuration.ToDouble(Time::US)
                                             << " us (txVector: " << txVector
                                             << ")");
  NS_ASSERT(psduDuration.IsStrictlyPositive());

  if (m_normalAckTimeoutEvent.IsRunning()) {
    // we are waiting for a Normal Ack and something arrived
    NS_LOG_DEBUG("Rescheduling Normal Ack timeout");
    m_normalAckTimeoutEvent.Cancel();
    NotifyAckTimeoutResetNow();
    m_normalAckTimeoutEvent =
        Simulator::Schedule(psduDuration + NanoSeconds(PSDU_DURATION_SAFEGUARD),
                            &MacLow::NormalAckTimeout, this);
  } else if (m_blockAckTimeoutEvent.IsRunning()) {
    // we are waiting for a BlockAck and something arrived
    NS_LOG_DEBUG("Rescheduling Block Ack timeout");
    m_blockAckTimeoutEvent.Cancel();
    NotifyAckTimeoutResetNow();
    m_blockAckTimeoutEvent =
        Simulator::Schedule(psduDuration + NanoSeconds(PSDU_DURATION_SAFEGUARD),
                            &MacLow::BlockAckTimeout, this);
  } else if (m_ctsTimeoutEvent.IsRunning()) {
    // we are waiting for a CTS and something arrived
    NS_LOG_DEBUG("Rescheduling CTS timeout");
    m_ctsTimeoutEvent.Cancel();
    NotifyCtsTimeoutResetNow();
    m_ctsTimeoutEvent =
        Simulator::Schedule(psduDuration + NanoSeconds(PSDU_DURATION_SAFEGUARD),
                            &MacLow::CtsTimeout, this);
  } else if (m_navCounterResetCtsMissed.IsRunning()) {
    NS_LOG_DEBUG("Cannot reset NAV");
    m_navCounterResetCtsMissed.Cancel();
  }
}

void MacLow::ReceiveError(Ptr<WigigPsdu> psdu) {
  NS_LOG_FUNCTION(this << *psdu);
  NS_LOG_DEBUG("rx failed");
}

void MacLow::NotifySwitchingStartNow(Time duration) {
  NS_LOG_DEBUG("switching channel. Cancelling MAC pending events");
  m_stationManager->Reset();
  CancelAllEvents();
  if (m_navCounterResetCtsMissed.IsRunning()) {
    m_navCounterResetCtsMissed.Cancel();
  }
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = Seconds(0);
  m_currentPacket = nullptr;
  m_currentTxop = nullptr;
}

void MacLow::NotifySleepNow() {
  NS_LOG_DEBUG("Device in sleep mode. Cancelling MAC pending events");
  CancelAllEvents();
  if (m_navCounterResetCtsMissed.IsRunning()) {
    m_navCounterResetCtsMissed.Cancel();
  }
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = Seconds(0);
  m_currentPacket = nullptr;
  m_currentTxop = nullptr;
}

void MacLow::NotifyOffNow() {
  NS_LOG_DEBUG("Device is switched off. Cancelling MAC pending events");
  CancelAllEvents();
  if (m_navCounterResetCtsMissed.IsRunning()) {
    m_navCounterResetCtsMissed.Cancel();
  }
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = Seconds(0);
  m_currentPacket = nullptr;
  m_currentTxop = nullptr;
}

void MacLow::ReceiveOk(Ptr<WigigMacQueueItem> mpdu, double rxSnr,
                       const WigigTxVector &txVector, bool ampduSubframe) {
  NS_LOG_FUNCTION(this << *mpdu << rxSnr << txVector);
  /* An MPDU is received from the PHY.
   * When we have handled this MPDU,
   * we handle any packet present in the
   * packet queue.
   */
  const WigigMacHeader &hdr = mpdu->GetHeader();
  Ptr<Packet> packet = mpdu->GetPacket()->Copy();

  bool isPrevNavZero = IsNavZero();
  NS_LOG_DEBUG("duration/id=" << hdr.GetDuration());
  NotifyNav(packet, hdr);
  if (hdr.IsRts()) {
    /* see section 9.2.5.7 802.11-1999
     * A STA that is addressed by an RTS frame shall transmit a CTS frame after
     * a SIFS period if the NAV at the STA receiving the RTS frame indicates
     * that the medium is idle. If the NAV at the STA receiving the RTS
     * indicates the medium is not idle, that STA shall not respond to the RTS
     * frame.
     */
    if (ampduSubframe) {
      NS_FATAL_ERROR("Received RTS as part of an A-MPDU");
    } else {
      if (isPrevNavZero && hdr.GetAddr1() == m_self) {
        NS_LOG_DEBUG("rx RTS from=" << hdr.GetAddr2() << ", schedule CTS");
        NS_ASSERT(m_sendCtsEvent.IsExpired());
        m_stationManager->ReportRxOk(hdr.GetAddr2(), &hdr, rxSnr,
                                     txVector.GetMode());
        if (m_phy->GetStandard() == WIFI_STANDARD_80211ad) {
          m_sendCtsEvent = Simulator::Schedule(
              GetSifs(), &MacLow::SendDmgCtsAfterRts, this, hdr.GetAddr2(),
              hdr.GetDuration(), txVector, rxSnr);
        } else {
          m_sendCtsEvent = Simulator::Schedule(
              GetSifs(), &MacLow::SendCtsAfterRts, this, hdr.GetAddr2(),
              hdr.GetDuration(), txVector, rxSnr);
        }
      } else {
        NS_LOG_DEBUG("rx RTS from=" << hdr.GetAddr2()
                                    << ", cannot schedule CTS");
      }
    }
  } else if ((hdr.IsCts() || hdr.IsDmgCts()) && hdr.GetAddr1() == m_self &&
             m_ctsTimeoutEvent.IsRunning() && m_currentPacket) {
    if (ampduSubframe) {
      NS_FATAL_ERROR("Received CTS as part of an A-MPDU");
    }

    NS_LOG_DEBUG("received cts from=" << m_currentPacket->GetAddr1());

    SnrTag tag;
    packet->RemovePacketTag(tag);
    m_stationManager->ReportRxOk(m_currentPacket->GetAddr1(), &hdr, rxSnr,
                                 txVector.GetMode());
    m_stationManager->ReportRtsOk(m_currentPacket->GetAddr1(),
                                  &m_currentPacket->GetHeader(0), rxSnr,
                                  txVector.GetMode(), tag.Get());

    m_ctsTimeoutEvent.Cancel();
    NotifyCtsTimeoutResetNow();
    NS_ASSERT(m_sendDataEvent.IsExpired());
    m_sendDataEvent = Simulator::Schedule(GetSifs(), &MacLow::SendDataAfterCts,
                                          this, hdr.GetDuration());
  } else if (hdr.IsAck() && hdr.GetAddr1() == m_self &&
             m_normalAckTimeoutEvent.IsRunning() &&
             m_txParams.MustWaitNormalAck()) {
    NS_LOG_DEBUG("receive ack from=" << m_currentPacket->GetAddr1());
    SnrTag tag;
    packet->RemovePacketTag(tag);
    // When fragmentation is used, only update manager when the last fragment is
    // acknowledged
    if (!m_txParams.HasNextPacket()) {
      m_stationManager->ReportRxOk(m_currentPacket->GetAddr1(), &hdr, rxSnr,
                                   txVector.GetMode());
      m_stationManager->ReportDataOk(
          m_currentPacket->GetAddr1(), &m_currentPacket->GetHeader(0), rxSnr,
          txVector.GetMode(), tag.Get(), m_currentTxVector,
          m_currentPacket->GetSize());
    }
    // cancel the Normal Ack timer
    m_normalAckTimeoutEvent.Cancel();
    NotifyAckTimeoutResetNow();
    m_currentTxop->GotAck();

    if (m_txParams.HasNextPacket()) {
      m_waitIfsEvent = Simulator::Schedule(
          GetSifs(), &MacLow::WaitIfsAfterEndTxFragment, this);
    } else if (m_currentPacket->GetHeader(0).IsQosData() &&
               m_currentTxop->IsWigigQosTxop() &&
               m_currentTxop->GetTxopLimit().IsStrictlyPositive() &&
               m_currentTxop->GetRemainingTimeForTransmission() > GetSifs()) {
      m_waitIfsEvent = Simulator::Schedule(
          GetSifs(), &MacLow::WaitIfsAfterEndTxPacket, this);
    } else if (m_currentTxop->IsWigigQosTxop()) {
      m_currentTxop->TerminateWigigTxop();
    }
    /* Set the current packet to zero to avoid storing it for the next access
     * period */
    m_currentPacket = nullptr;
  } else if (hdr.IsBlockAck() && hdr.GetAddr1() == m_self &&
             m_txParams.MustWaitBlockAck() &&
             m_blockAckTimeoutEvent.IsRunning()) {
    NS_LOG_DEBUG("got block ack from " << hdr.GetAddr2());
    SnrTag tag;
    packet->RemovePacketTag(tag);
    CtrlBAckResponseHeader blockAck;
    packet->RemoveHeader(blockAck);
    m_blockAckTimeoutEvent.Cancel();
    NotifyAckTimeoutResetNow();
    m_currentTxop->GotBlockAck(&blockAck, hdr.GetAddr2(), rxSnr, tag.Get(),
                               m_currentTxVector);
    // start next packet if TXOP remains, otherwise contend for accessing the
    // channel again
    if (m_currentTxop->IsWigigQosTxop() &&
        m_currentTxop->GetTxopLimit().IsStrictlyPositive() &&
        m_currentTxop->GetRemainingTimeForTransmission() > GetSifs()) {
      m_waitIfsEvent = Simulator::Schedule(
          GetSifs(), &MacLow::WaitIfsAfterEndTxPacket, this);
    } else if (m_currentTxop->IsWigigQosTxop()) {
      m_currentTxop->TerminateWigigTxop();
    }
    /* Set the current packet to zero to avoid storing it for the next access
     * period */
    m_currentPacket = nullptr;
  } else if (hdr.IsBlockAckReq() && hdr.GetAddr1() == m_self) {
    if (m_servingSls) {
      NS_LOG_DEBUG("We are serving SLS, so ignore BlockAckRequest frame");
      return;
    }
    CtrlBAckRequestHeader blockAckReq;
    packet->RemoveHeader(blockAckReq);
    if (!blockAckReq.IsMultiTid()) {
      uint8_t tid = blockAckReq.GetTidInfo();
      AgreementsI it =
          m_bAckAgreements.find(std::make_pair(hdr.GetAddr2(), tid));
      if (it != m_bAckAgreements.end()) {
        // Update block ack cache
        BlockAckCachesI i =
            m_bAckCaches.find(std::make_pair(hdr.GetAddr2(), tid));
        NS_ASSERT(i != m_bAckCaches.end());
        (*i).second.UpdateWithBlockAckReq(blockAckReq.GetStartingSequence());

        NS_ASSERT(m_sendAckEvent.IsExpired());
        m_sendAckEvent.Cancel();
        /* See section 11.5.3 in IEEE 802.11 for mean of this timer */
        ResetBlockAckInactivityTimerIfNeeded(it->second.first);
        if ((*it).second.first.IsImmediateBlockAck()) {
          NS_LOG_DEBUG("rx blockAckRequest/sendImmediateBlockAck from="
                       << hdr.GetAddr2());
          m_sendAckEvent = Simulator::Schedule(
              GetSifs(), &MacLow::SendBlockAckAfterBlockAckRequest, this,
              blockAckReq, hdr.GetAddr2(), hdr.GetDuration(),
              txVector.GetMode(), rxSnr);
        } else {
          NS_FATAL_ERROR("Delayed block ack not supported.");
        }
      } else {
        NS_LOG_DEBUG(
            "There's not a valid agreement for this block ack request.");
      }
    } else {
      NS_FATAL_ERROR("Multi-tid block ack is not supported.");
    }
  } else if (hdr.IsDmgBeacon()) {
    NS_LOG_DEBUG("Received DMG Beacon with BSSID=" << hdr.GetAddr1());
    m_stationManager->ReportRxOk(hdr.GetAddr1(), &hdr, rxSnr,
                                 txVector.GetMode());
    goto rxPacket;
  } else if ((hdr.GetAddr1() == m_self) &&
             (hdr.IsSsw() || hdr.IsSswFbck() || hdr.IsSswAck())) {
    NS_LOG_DEBUG("Received " << hdr.GetTypeString());
    m_stationManager->ReportRxOk(hdr.GetAddr2(), &hdr, rxSnr,
                                 txVector.GetMode());
    goto rxPacket;
  } else if (hdr.IsCtl()) {
    NS_LOG_DEBUG("rx drop " << hdr.GetTypeString());
  } else if (hdr.GetAddr1() == m_self) {
    if (m_servingSls) {
      NS_LOG_DEBUG(
          "We are serving SLS, so ignore any data or management frame");
      if (m_sendAckEvent.IsRunning()) {
        m_sendAckEvent.Cancel();
      }
      return;
    }
    m_stationManager->ReportRxOk(hdr.GetAddr2(), &hdr, rxSnr,
                                 txVector.GetMode());
    if (hdr.IsActionNoAck()) {
      NS_LOG_DEBUG("Received Action No ACK Frame");
      goto rxPacket;
    } else if (hdr.IsQosData() && ReceiveMpdu(mpdu)) {
      /* From section 9.10.4 in IEEE 802.11:
         Upon the receipt of a QoS data frame from the originator for which
         the block ack agreement exists, the recipient shall buffer the MSDU
         regardless of the value of the Ack Policy subfield within the
         QoS Control field of the QoS data frame. */
      if (hdr.IsQosAck() && !ampduSubframe) {
        NS_LOG_DEBUG("rx QoS unicast/sendAck from=" << hdr.GetAddr2());
        AgreementsI it = m_bAckAgreements.find(
            std::make_pair(hdr.GetAddr2(), hdr.GetQosTid()));

        RxCompleteBufferedPacketsWithSmallerSequence(
            it->second.first.GetStartingSequenceControl(), hdr.GetAddr2(),
            hdr.GetQosTid());
        RxCompleteBufferedPacketsUntilFirstLost(hdr.GetAddr2(),
                                                hdr.GetQosTid());
        NS_ASSERT(m_sendAckEvent.IsExpired());
        m_sendAckEvent = Simulator::Schedule(
            GetSifs(), &MacLow::SendAckAfterData, this, hdr.GetAddr2(),
            hdr.GetDuration(), txVector.GetMode(), rxSnr);
      } else if (hdr.IsQosBlockAck()) {
        AgreementsI it = m_bAckAgreements.find(
            std::make_pair(hdr.GetAddr2(), hdr.GetQosTid()));
        /* See section 11.5.3 in IEEE 802.11 for mean of this timer */
        ResetBlockAckInactivityTimerIfNeeded(it->second.first);
      }
      return;
    } else if (hdr.IsQosData() && hdr.IsQosBlockAck()) {
      /* This happens if a packet with ack policy Block Ack is received and a
         block ack agreement for that packet doesn't exist.

         From section 11.5.3 in IEEE 802.11e:
         When a recipient does not have an active block ack for a TID, but
         receives data MPDUs with the Ack Policy subfield set to Block Ack, it
         shall discard them and shall send a DELBA frame using the normal access
         mechanisms. */
      AcIndex ac = QosUtilsMapTidToAc(hdr.GetQosTid());
      m_edca[ac]->SendDelbaFrame(hdr.GetAddr2(), hdr.GetQosTid(), false);
      return;
    } else if (hdr.IsQosData() && hdr.IsQosNoAck()) {
      if (ampduSubframe) {
        NS_LOG_DEBUG("rx Ampdu with No Ack Policy from=" << hdr.GetAddr2());
      } else {
        NS_LOG_DEBUG("rx unicast/noAck from=" << hdr.GetAddr2());
      }
    } else if (hdr.IsData() || hdr.IsMgt()) {
      if (hdr.IsProbeResp()) {
        // Apply SNR tag for probe response quality measurements
        SnrTag tag;
        tag.Set(rxSnr);
        packet->AddPacketTag(tag);
        mpdu = Create<WigigMacQueueItem>(packet, hdr);
      }
      if (hdr.IsMgt() && ampduSubframe) {
        NS_FATAL_ERROR("Received management packet as part of an A-MPDU");
      } else {
        NS_LOG_DEBUG("rx unicast/sendAck from=" << hdr.GetAddr2());
        NS_ASSERT(m_sendAckEvent.IsExpired());
        m_sendAckEvent = Simulator::Schedule(
            GetSifs(), &MacLow::SendAckAfterData, this, hdr.GetAddr2(),
            hdr.GetDuration(), txVector.GetMode(), rxSnr);
      }
    }
    goto rxPacket;
  } else if (hdr.GetAddr1().IsGroup()) {
    if (ampduSubframe) {
      NS_FATAL_ERROR("Received group addressed packet as part of an A-MPDU");
    } else {
      if (hdr.IsData() || hdr.IsMgt()) {
        NS_LOG_DEBUG("rx group from=" << hdr.GetAddr2());
        if (hdr.IsBeacon()) {
          // Apply SNR tag for beacon quality measurements
          SnrTag tag;
          tag.Set(rxSnr);
          packet->AddPacketTag(tag);
          mpdu = Create<WigigMacQueueItem>(packet, hdr);
        }
        goto rxPacket;
      }
    }
  } else if (m_promisc) {
    NS_ASSERT(hdr.GetAddr1() != m_self);
    if (hdr.IsData()) {
      goto rxPacket;
    }
  } else {
    NS_LOG_DEBUG("rx not for me from=" << hdr.GetAddr2());
  }
  return;

rxPacket:
  m_rxCallback(mpdu);
}

Time MacLow::GetAckDuration(Mac48Address to,
                            const WigigTxVector &dataTxVector) const {
  WigigTxVector ackTxVector = GetAckTxVectorForData(to, dataTxVector.GetMode());
  return GetAckDuration(ackTxVector);
}

Time MacLow::GetAckDuration(const WigigTxVector &ackTxVector) const {
  return m_phy->CalculateTxDuration(GetAckSize(), ackTxVector,
                                    m_phy->GetFrequency());
}

Time MacLow::GetBlockAckDuration(const WigigTxVector &blockAckReqTxVector,
                                 BlockAckType type) const {
  /*
   * For immediate Basic BlockAck we should transmit the frame with the same
   * WifiMode as the BlockAckReq.
   */
  return m_phy->CalculateTxDuration(GetBlockAckSize(type), blockAckReqTxVector,
                                    m_phy->GetFrequency());
}

Time MacLow::GetBlockAckRequestDuration(
    const WigigTxVector &blockAckReqTxVector, BlockAckReqType type) const {
  return m_phy->CalculateTxDuration(GetBlockAckRequestSize(type),
                                    blockAckReqTxVector, m_phy->GetFrequency());
}

Time MacLow::GetCtsDuration(Mac48Address to,
                            const WigigTxVector &rtsTxVector) const {
  if (rtsTxVector.GetMode().GetModulationClass() == WIFI_MOD_CLASS_DMG_CTRL) {
    return GetDmgCtsDuration();
  } else {
    WigigTxVector ctsTxVector = GetCtsTxVectorForRts(to, rtsTxVector.GetMode());
    return GetCtsDuration(ctsTxVector);
  }
}

Time MacLow::GetCtsDuration(const WigigTxVector &ctsTxVector) const {
  return m_phy->CalculateTxDuration(GetCtsSize(), ctsTxVector,
                                    m_phy->GetFrequency());
}

Time MacLow::GetDmgCtsDuration() const {
  WigigTxVector ctsTxVector = GetDmgControlTxVector();
  NS_ASSERT(ctsTxVector.GetMode().GetModulationClass() ==
            WIFI_MOD_CLASS_DMG_CTRL);
  return m_phy->CalculateTxDuration(GetDmgCtsSize(), ctsTxVector,
                                    m_phy->GetFrequency());
}

uint32_t MacLow::GetDmgCtsSize() {
  WigigMacHeader cts;
  cts.SetType(WIFI_MAC_CTL_DMG_CTS);
  uint32_t dmgCtsSize = cts.GetSize() + 4;
  return dmgCtsSize;
}

WigigTxVector MacLow::GetDmgTxVector(Ptr<const WigigMacQueueItem> item) const {
  Mac48Address to = item->GetHeader().GetAddr1();
  return m_stationManager->GetDmgTxVector(to, &item->GetHeader(),
                                          item->GetPacket());
}

WigigTxVector MacLow::GetRtsTxVector(Ptr<const WigigMacQueueItem> item) const {
  return m_stationManager->GetRtsTxVector(item->GetHeader().GetAddr1());
}

WigigTxVector MacLow::GetDataTxVector(Ptr<const WigigMacQueueItem> item) const {
  return m_stationManager->GetDataTxVector(item->GetHeader());
}

Time MacLow::GetResponseDuration(const MacLowTransmissionParameters &params,
                                 const WigigTxVector &dataTxVector,
                                 Mac48Address receiver) const {
  NS_LOG_FUNCTION(this << receiver << dataTxVector << params);

  Time duration = Seconds(0);
  if (params.MustWaitNormalAck()) {
    duration += GetSifs();
    duration += GetAckDuration(receiver, dataTxVector);
  } else if (params.MustWaitBlockAck()) {
    duration += GetSifs();
    WigigTxVector blockAckReqTxVector =
        GetBlockAckTxVector(m_self, dataTxVector.GetMode());
    duration +=
        GetBlockAckDuration(blockAckReqTxVector, params.GetBlockAckType());
  } else if (params.MustSendBlockAckRequest()) {
    duration += 2 * GetSifs();
    WigigTxVector blockAckReqTxVector =
        GetBlockAckTxVector(m_self, dataTxVector.GetMode());
    duration += GetBlockAckRequestDuration(blockAckReqTxVector,
                                           params.GetBlockAckRequestType());
    duration +=
        GetBlockAckDuration(blockAckReqTxVector, params.GetBlockAckType());
  }
  return duration;
}

WifiMode MacLow::GetControlAnswerMode(WifiMode reqMode) const {
  NS_LOG_FUNCTION(this << reqMode);
  WifiMode mode = m_stationManager->GetDefaultMode();
  bool found = false;
  if (m_stationManager->HasDmgSupported()) {
    /**
     * Rules for selecting a control response rate from IEEE 802.11ad-2012,
     * Section 9.7.5a Multirate support for DMG STAs:
     */
    WifiMode thismode;
    /* We start from SC PHY Rates, This is for transmitting an ACK frame or a BA
     * frame */
    for (uint32_t idx = 0; idx < m_phy->GetNModes(); idx++) {
      thismode = m_phy->GetMode(idx);
      if (thismode.IsMandatory() &&
          (thismode.GetDataRate(2160) <= reqMode.GetDataRate(2160))) {
        mode = thismode;
        found = true;
      } else {
        break;
      }
    }
  } else {
    /**
     * The standard has relatively unambiguous rules for selecting a
     * control response rate (the below is quoted from IEEE 802.11-2012,
     * Section 9.7):
     *
     * To allow the transmitting STA to calculate the contents of the
     * Duration/ID field, a STA responding to a received frame shall
     * transmit its Control Response frame (either CTS or Ack), other
     * than the BlockAck control frame, at the highest rate in the
     * BSSBasicRateSet parameter that is less than or equal to the
     * rate of the immediately previous frame in the frame exchange
     * sequence (as defined in Annex G) and that is of the same
     * modulation class (see Section 9.7.8) as the received frame...
     */
    // First, search the BSS Basic Rate set
    for (uint8_t i = 0; i < m_stationManager->GetNBasicModes(); i++) {
      WifiMode testMode = m_stationManager->GetBasicMode(i);
      if ((!found || testMode.IsHigherDataRate(mode)) &&
          (!testMode.IsHigherDataRate(reqMode)) &&
          (IsAllowedControlAnswerModulationClass(
              reqMode.GetModulationClass(), testMode.GetModulationClass()))) {
        mode = testMode;
        // We've found a potentially-suitable transmit rate, but we
        // need to continue and consider all the basic rates before
        // we can be sure we've got the right one.
        found = true;
      }
    }
    // If we found a suitable rate in the BSSBasicRateSet, then we are
    // done and can return that mode.
    if (found) {
      NS_LOG_DEBUG("MacLow::GetControlAnswerMode returning " << mode);
      return mode;
    }

    /**
     * If no suitable basic rate was found, we search the mandatory
     * rates. The standard (IEEE 802.11-2007, Section 9.6) says:
     *
     *   ...If no rate contained in the BSSBasicRateSet parameter meets
     *   these conditions, then the control frame sent in response to a
     *   received frame shall be transmitted at the highest mandatory
     *   rate of the PHY that is less than or equal to the rate of the
     *   received frame, and that is of the same modulation class as the
     *   received frame. In addition, the Control Response frame shall
     *   be sent using the same PHY options as the received frame,
     *   unless they conflict with the requirement to use the
     *   BSSBasicRateSet parameter.
     *
     * \todo Note that we're ignoring the last sentence for now, because
     * there is not yet any manipulation here of PHY options.
     */
    for (uint8_t idx = 0; idx < m_phy->GetNModes(); idx++) {
      WifiMode thismode = m_phy->GetMode(idx);
      /* If the rate:
       *
       *  - is a mandatory rate for the PHY, and
       *  - is equal to or faster than our current best choice, and
       *  - is less than or equal to the rate of the received frame, and
       *  - is of the same modulation class as the received frame
       *
       * ...then it's our best choice so far.
       */
      if (thismode.IsMandatory() &&
          (!found || thismode.IsHigherDataRate(mode)) &&
          (!thismode.IsHigherDataRate(reqMode)) &&
          (IsAllowedControlAnswerModulationClass(
              reqMode.GetModulationClass(), thismode.GetModulationClass()))) {
        mode = thismode;
        // As above; we've found a potentially-suitable transmit
        // rate, but we need to continue and consider all the
        // mandatory rates before we can be sure we've got the right one.
        found = true;
      }
    }
  }

  /**
   * If we still haven't found a suitable rate for the response then
   * someone has messed up the simulation configuration. This probably means
   * that the WifiStandard is not set correctly, or that a rate that
   * is not supported by the PHY has been explicitly requested.
   *
   * Either way, it is serious - we can either disobey the standard or
   * fail, and I have chosen to do the latter...
   */
  if (!found) {
    NS_FATAL_ERROR("Can't find response rate for " << reqMode);
  }

  NS_LOG_DEBUG("MacLow::GetControlAnswerMode returning " << mode);
  return mode;
}

WigigTxVector MacLow::GetCtsTxVector(Mac48Address to,
                                     WifiMode rtsTxMode) const {
  NS_ASSERT(!to.IsGroup());
  WifiMode ctsMode = GetControlAnswerMode(rtsTxMode);
  WigigTxVector v;
  v.SetMode(ctsMode);
  v.SetPreambleType(
      GetPreambleForTransmission(ctsMode.GetModulationClass(), false));
  v.SetTxPowerLevel(m_stationManager->GetDefaultTxPowerLevel());
  v.SetChannelWidth(
      GetChannelWidthForTransmission(ctsMode, m_phy->GetChannelWidth()));
  v.SetNss(1);
  return v;
}

WigigTxVector MacLow::GetAckTxVector(Mac48Address to,
                                     WifiMode dataTxMode) const {
  NS_ASSERT(!to.IsGroup());
  WifiMode ackMode = GetControlAnswerMode(dataTxMode);
  WigigTxVector v;
  v.SetMode(ackMode);
  v.SetPreambleType(
      GetPreambleForTransmission(ackMode.GetModulationClass(), false));
  v.SetTxPowerLevel(m_stationManager->GetDefaultTxPowerLevel());
  v.SetChannelWidth(
      GetChannelWidthForTransmission(ackMode, m_phy->GetChannelWidth()));
  v.SetNss(1);
  return v;
}

WigigTxVector MacLow::GetDmgControlTxVector() const {
  return m_stationManager->GetDmgControlTxVector();
}

WigigTxVector MacLow::GetBlockAckTxVector(Mac48Address to,
                                          WifiMode dataTxMode) const {
  NS_ASSERT(!to.IsGroup());
  WifiMode blockAckMode = GetControlAnswerMode(dataTxMode);
  WigigTxVector v;
  v.SetMode(blockAckMode);
  v.SetPreambleType(
      GetPreambleForTransmission(blockAckMode.GetModulationClass(), false));
  v.SetTxPowerLevel(m_stationManager->GetDefaultTxPowerLevel());
  v.SetChannelWidth(
      GetChannelWidthForTransmission(blockAckMode, m_phy->GetChannelWidth()));
  v.SetNss(1);
  return v;
}

WigigTxVector MacLow::GetCtsTxVectorForRts(Mac48Address to,
                                           WifiMode rtsTxMode) const {
  return GetCtsTxVector(to, rtsTxMode);
}

WigigTxVector MacLow::GetAckTxVectorForData(Mac48Address to,
                                            WifiMode dataTxMode) const {
  return GetAckTxVector(to, dataTxMode);
}

Time MacLow::CalculateOverallTxTime(Ptr<const Packet> packet,
                                    const WigigMacHeader *hdr,
                                    const MacLowTransmissionParameters &params,
                                    uint32_t fragmentSize) const {
  Ptr<const WigigMacQueueItem> item =
      Create<const WigigMacQueueItem>(packet, *hdr);
  Time txTime = CalculateOverheadTxTime(item, params);
  uint32_t dataSize;
  if (fragmentSize > 0) {
    Ptr<const Packet> fragment = Create<Packet>(fragmentSize);
    dataSize = GetSize(fragment, hdr,
                       m_currentPacket && m_currentPacket->IsAggregate());
  } else {
    dataSize =
        GetSize(packet, hdr, m_currentPacket && m_currentPacket->IsAggregate());
  }
  txTime += m_phy->CalculateTxDuration(dataSize, GetDataTxVector(item),
                                       m_phy->GetFrequency());
  return txTime;
}

Time MacLow::CalculateOverheadTxTime(
    Ptr<const WigigMacQueueItem> item,
    const MacLowTransmissionParameters &params) const {
  Time txTime = Seconds(0);
  if (params.MustSendRts()) {
    WigigTxVector rtsTxVector = GetRtsTxVector(item);
    txTime += m_phy->CalculateTxDuration(GetRtsSize(), rtsTxVector,
                                         m_phy->GetFrequency());
    txTime += GetCtsDuration(item->GetHeader().GetAddr1(), rtsTxVector);
    txTime += Time(GetSifs() * 2);
  }
  txTime += GetResponseDuration(params, GetDataTxVector(item),
                                item->GetHeader().GetAddr1());

  return txTime;
}

Time MacLow::CalculateTransmissionTime(
    Ptr<const Packet> packet, const WigigMacHeader *hdr,
    const MacLowTransmissionParameters &params) const {
  Time txTime = CalculateOverallTxTime(packet, hdr, params);
  if (params.HasNextPacket()) {
    WigigTxVector dataTxVector =
        GetDataTxVector(Create<const WigigMacQueueItem>(packet, *hdr));
    txTime += GetSifs();
    txTime += m_phy->CalculateTxDuration(params.GetNextPacketSize(),
                                         dataTxVector, m_phy->GetFrequency());
  }
  return txTime;
}

Time MacLow::CalculateWiGigTransactionTime(Ptr<WigigPsdu> psdu) {
  NS_LOG_FUNCTION(this << psdu);
  Time txTime = m_phy->CalculateTxDuration(psdu->GetSize(), m_currentTxVector,
                                           m_phy->GetFrequency());
  /* Calculate overhead duration */
  if (m_txParams.MustSendRts()) {
    WigigTxVector rtsTxVector = GetDmgControlTxVector();
    txTime += m_phy->CalculateTxDuration(GetRtsSize(), rtsTxVector,
                                         m_phy->GetFrequency());
    txTime += GetCtsDuration(psdu->GetAddr1(), rtsTxVector);
    txTime += Time(GetSifs() * 2);
  }
  txTime +=
      GetResponseDuration(m_txParams, m_currentTxVector, psdu->GetAddr1());
  /* Convert to MicroSeconds since the duration in the headers are in
   * MicroSeconds */
  return MicroSeconds(
      ceil(static_cast<double>(txTime.GetNanoSeconds()) / 1000));
  return Seconds(0);
}

void MacLow::NotifyNav(Ptr<const Packet> packet, const WigigMacHeader &hdr) {
  NS_ASSERT(m_lastNavStart <= Simulator::Now());
  if (hdr.GetRawDuration() > 32767) {
    // All stations process Duration field values less than or equal to 32 767
    // from valid data frames to update their NAV settings as appropriate under
    // the coordination function rules.
    return;
  }
  Time duration = hdr.GetDuration();
  if (hdr.GetAddr1() != m_self) {
    if (hdr.IsGrantFrame()) {
      // see section 9.33.7.3 802.11ad-2012
      Ptr<Packet> newPacket = packet->Copy();
      CtrlDmgGrant grant;
      newPacket->RemoveHeader(grant);
      Ptr<StaWigigMac> highMac = DynamicCast<StaWigigMac>(m_mac);
      if (grant.GetDynamicAllocationInfo().GetSourceAid() ==
              highMac->GetAssociationId() ||
          grant.GetDynamicAllocationInfo().GetDestinationAid() ==
              highMac->GetAssociationId()) {
        return;
      }
    }
    // see section 9.2.5.4 802.11-1999
    bool navUpdated = DoNavStartNow(duration);
    if (hdr.IsRts() && navUpdated) {
      /**
       * A STA that used information from an RTS frame as the most recent basis
       * to update its NAV setting is permitted to reset its NAV if no
       * PHY-RXSTART.indication is detected from the PHY during a period with a
       * duration of (2 * aSIFSTime) + (CTS_Time) + aRxPHYStartDelay + (2 *
       * aSlotTime) starting at the PHY-RXEND.indication corresponding to the
       * detection of the RTS frame. The “CTS_Time” shall be calculated using
       * the length of the CTS frame and the data rate at which the RTS frame
       * used for the most recent NAV update was received.
       */
      WigigMacHeader cts;
      cts.SetType(WIFI_MAC_CTL_CTS);
      WigigTxVector txVector =
          GetRtsTxVector(Create<const WigigMacQueueItem>(packet, hdr));
      Time navCounterResetCtsMissedDelay =
          m_phy->CalculateTxDuration(cts.GetSerializedSize(), txVector,
                                     m_phy->GetFrequency()) +
          Time(2 * GetSifs()) + Time(2 * GetSlotTime()) +
          m_phy->CalculatePhyPreambleAndHeaderDuration(txVector);
      m_navCounterResetCtsMissed =
          Simulator::Schedule(navCounterResetCtsMissedDelay,
                              &MacLow::DoNavResetNow, this, Seconds(0));
    }
  }
}

void MacLow::DoNavResetNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyNavResetNow(duration);
  }
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = duration;
}

bool MacLow::DoNavStartNow(Time duration) {
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyNavStartNow(duration);
  }
  Time newNavEnd = Simulator::Now() + duration;
  Time oldNavEnd = m_lastNavStart + m_lastNavDuration;
  if (newNavEnd > oldNavEnd) {
    m_lastNavStart = Simulator::Now();
    m_lastNavDuration = duration;
    return true;
  }
  return false;
}

void MacLow::NotifyAckTimeoutStartNow(Time duration) {
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyAckTimeoutStartNow(duration);
  }
}

void MacLow::NotifyAckTimeoutResetNow() {
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyAckTimeoutResetNow();
  }
}

void MacLow::NotifyCtsTimeoutStartNow(Time duration) {
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyCtsTimeoutStartNow(duration);
  }
}

void MacLow::NotifyCtsTimeoutResetNow() {
  for (auto i = m_channelAccessManagers.begin();
       i != m_channelAccessManagers.end(); i++) {
    (*i)->NotifyCtsTimeoutResetNow();
  }
}

void MacLow::ForwardDown(Ptr<const WigigPsdu> psdu,
                         const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << psdu << txVector);

  NS_ASSERT(psdu->GetNMpdus());
  const WigigMacHeader &hdr = (*psdu->begin())->GetHeader();

  NS_LOG_DEBUG("send " << hdr.GetTypeString() << ", to=" << hdr.GetAddr1()
                       << ", size=" << psdu->GetSize()
                       << ", mode=" << txVector.GetMode()
                       << ", preamble=" << txVector.GetPreambleType()
                       << ", duration=" << hdr.GetDuration() << ", seq=0x"
                       << std::hex << hdr.GetSequenceControl() << std::dec);

  /* Antenna steering */
  if (m_phy->GetStandard() == WIFI_STANDARD_80211ad) {
    Ptr<WigigMac> wifiMac = DynamicCast<WigigMac>(m_mac);
    /* Change antenna configuration */
    if (((wifiMac->GetCurrentAccessPeriod() == CHANNEL_ACCESS_DTI) &&
         (wifiMac->GetCurrentAllocation() == CBAP_ALLOCATION)) ||
        (wifiMac->GetCurrentAccessPeriod() == CHANNEL_ACCESS_ATI)) {
      if ((wifiMac->GetTypeOfStation() == AP) &&
          (hdr.IsAck() || hdr.IsBlockAck())) {
        wifiMac->SteerTxAntennaToward(hdr.GetAddr1(), (false));
      } else if (!(hdr.IsSsw() || hdr.IsSswAck() ||
                   hdr.IsSswFbck())) /* Special case to handle TXSS CBAP*/
      {
        wifiMac->SteerAntennaToward(hdr.GetAddr1(), hdr.IsData());
      }
    } else if (wifiMac->GetTypeOfStation() == ADHOC_STA) {
      if ((hdr.IsAck() || hdr.IsBlockAck())) {
        wifiMac->SteerTxAntennaToward(hdr.GetAddr1(), true);
      } else {
        wifiMac->SteerAntennaToward(
            hdr.GetAddr1(), (hdr.IsData() || hdr.IsAck() || hdr.IsBlockAck()));
      }
    }
  }

  WigigTxVector txvector = txVector;
  if (psdu->IsSingle()) {
    txvector.SetAggregation(true);
    NS_LOG_DEBUG("Sending S-MPDU");
  } else if (psdu->IsAggregate()) {
    txvector.SetAggregation(true);
    NS_LOG_DEBUG("Sending A-MPDU");
  } else {
    NS_LOG_DEBUG("Sending non aggregate MPDU");
  }

  for (auto &mpdu : *PeekPointer(psdu)) {
    if (mpdu->GetHeader().IsQosData()) {
      auto edcaIt =
          m_edca.find(QosUtilsMapTidToAc(mpdu->GetHeader().GetQosTid()));
      edcaIt->second->CompleteMpduTx(mpdu);
    }
  }
  m_phy->Send(psdu, txvector);
}

void MacLow::CtsTimeout() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("cts timeout");
  /// \todo should check that there was no RX start before now.
  /// we should restart a new CTS timeout now until the expected
  /// end of RX if there was a RX start before now.
  m_stationManager->ReportRtsFailed(m_currentPacket->GetAddr1(),
                                    &m_currentPacket->GetHeader(0));

  Ptr<WigigQosTxop> qosWigigTxop = DynamicCast<WigigQosTxop>(m_currentTxop);
  if (qosWigigTxop) {
    qosWigigTxop->NotifyMissedCts(std::list<Ptr<WigigMacQueueItem>>(
        m_currentPacket->begin(), m_currentPacket->end()));
  } else {
    m_currentTxop->MissedCts();
  }
  m_currentTxop = nullptr;
  m_currentPacket = nullptr;
}

void MacLow::NormalAckTimeout() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("normal ack timeout");
  /// \todo should check that there was no RX start before now.
  /// we should restart a new ack timeout now until the expected
  /// end of RX if there was a RX start before now.
  Ptr<WigigTxop> txop = m_currentTxop;
  m_currentTxop = nullptr;
  txop->MissedAck();
  m_currentPacket = nullptr;
}

void MacLow::BlockAckTimeout() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("block ack timeout");
  Ptr<WigigTxop> txop = m_currentTxop;
  m_currentTxop = nullptr;
  txop->MissedBlockAck(m_currentPacket->GetNMpdus());
  m_currentPacket = nullptr;
}

void MacLow::SendRtsForPacket() {
  NS_LOG_FUNCTION(this);
  /* send an RTS for this packet. */
  WigigMacHeader rts;
  rts.SetType(WIFI_MAC_CTL_RTS);
  rts.SetDsNotFrom();
  rts.SetDsNotTo();
  rts.SetNoRetry();
  rts.SetNoMoreFragments();
  rts.SetAddr1(m_currentPacket->GetAddr1());
  rts.SetAddr2(m_self);
  WigigTxVector rtsTxVector = GetRtsTxVector(*m_currentPacket->begin());

  Time duration = Seconds(0);
  duration += GetSifs();
  duration += GetCtsDuration(m_currentPacket->GetAddr1(), rtsTxVector);
  duration += GetSifs();
  duration += m_phy->CalculateTxDuration(
      m_currentPacket->GetSize(), m_currentTxVector, m_phy->GetFrequency());
  duration += GetResponseDuration(m_txParams, m_currentTxVector,
                                  m_currentPacket->GetAddr1());
  if (m_txParams.HasNextPacket()) {
    duration +=
        m_phy->CalculateTxDuration(m_txParams.GetNextPacketSize(),
                                   m_currentTxVector, m_phy->GetFrequency());
    duration += GetResponseDuration(m_txParams, m_currentTxVector,
                                    m_currentPacket->GetAddr1());
  }
  rts.SetDuration(duration);

  Time txDuration = m_phy->CalculateTxDuration(GetRtsSize(), rtsTxVector,
                                               m_phy->GetFrequency());
  // After transmitting an RTS frame, the STA shall wait for a CTSTimeout
  // interval with a value of aSIFSTime + aSlotTime + aRxPHYStartDelay (IEEE
  // 802.11-2016 sec. 10.3.2.7). aRxPHYStartDelay equals the time to transmit
  // the PHY header.
  Time timerDelay = txDuration + GetSifs() + GetSlotTime() +
                    m_phy->CalculatePhyPreambleAndHeaderDuration(rtsTxVector);
  NS_ASSERT(m_ctsTimeoutEvent.IsExpired());
  NotifyCtsTimeoutStartNow(timerDelay);
  m_ctsTimeoutEvent =
      Simulator::Schedule(timerDelay, &MacLow::CtsTimeout, this);

  ForwardDown(Create<const WigigPsdu>(Create<Packet>(), rts), rtsTxVector);
}

void MacLow::StartDataTxTimers(const WigigTxVector &dataTxVector) {
  NS_LOG_FUNCTION(this);
  Time txDuration = m_phy->CalculateTxDuration(
      m_currentPacket->GetSize(), dataTxVector, m_phy->GetFrequency());
  if (m_txParams.MustWaitNormalAck()) {
    // the timeout duration is "aSIFSTime + aSlotTime + aRxPHYStartDelay,
    // starting at the PHY-TXEND.confirm primitive" (section 10.3.2.9
    // or 10.22.2.2 of 802.11-2016). aRxPHYStartDelay equals the time to
    // transmit the PHY header.
    WigigTxVector ackTxVector =
        GetAckTxVector(m_currentPacket->GetAddr1(), dataTxVector.GetMode());
    Time timerDelay = txDuration + GetSifs() + GetSlotTime() +
                      m_phy->CalculatePhyPreambleAndHeaderDuration(ackTxVector);
    NS_ASSERT(m_normalAckTimeoutEvent.IsExpired());
    NotifyAckTimeoutStartNow(timerDelay);
    m_normalAckTimeoutEvent =
        Simulator::Schedule(timerDelay, &MacLow::NormalAckTimeout, this);
  } else if (m_txParams.MustWaitBlockAck()) {
    // the timeout duration is "aSIFSTime + aSlotTime + aRxPHYStartDelay,
    // starting at the PHY-TXEND.confirm primitive" (section 10.3.2.9
    // or 10.22.2.2 of 802.11-2016). aRxPHYStartDelay equals the time to
    // transmit the PHY header.
    WigigTxVector blockAckTxVector = GetBlockAckTxVector(
        m_currentPacket->GetAddr1(), dataTxVector.GetMode());
    Time timerDelay =
        txDuration + GetSifs() + GetSlotTime() +
        m_phy->CalculatePhyPreambleAndHeaderDuration(blockAckTxVector);
    NS_ASSERT(m_blockAckTimeoutEvent.IsExpired());
    NotifyAckTimeoutStartNow(timerDelay);
    m_blockAckTimeoutEvent =
        Simulator::Schedule(timerDelay, &MacLow::BlockAckTimeout, this);
  } else if (m_txParams.HasNextPacket()) {
    NS_ASSERT(m_waitIfsEvent.IsExpired());
    Time delay = txDuration + GetSifs();
    m_waitIfsEvent =
        Simulator::Schedule(delay, &MacLow::WaitIfsAfterEndTxFragment, this);
  } else if (m_currentPacket->GetHeader(0).IsQosData() &&
             m_currentTxop->IsWigigQosTxop() &&
             m_currentTxop->GetTxopLimit().IsStrictlyPositive() &&
             m_currentTxop->GetRemainingTimeForTransmission() > GetSifs()) {
    Time delay = txDuration + GetSifs();
    m_waitIfsEvent =
        Simulator::Schedule(delay, &MacLow::WaitIfsAfterEndTxPacket, this);
  } else {
    // since we do not expect any timer to be triggered.
    m_endTxNoAckEvent =
        Simulator::Schedule(txDuration, &MacLow::EndTxNoAck, this);
  }
}

void MacLow::SendDataPacket() {
  NS_LOG_FUNCTION(this);
  /* send this packet directly. No RTS is needed. */
  StartDataTxTimers(m_currentTxVector);
  if (m_txParams.HasDurationId()) {
    m_currentPacket->SetDuration(m_txParams.GetDurationId());
  } else {
    Time duration = GetResponseDuration(m_txParams, m_currentTxVector,
                                        m_currentPacket->GetAddr1());
    if (m_txParams.HasNextPacket()) {
      duration += GetSifs();

      duration +=
          m_phy->CalculateTxDuration(m_txParams.GetNextPacketSize(),
                                     m_currentTxVector, m_phy->GetFrequency());
      duration += GetResponseDuration(m_txParams, m_currentTxVector,
                                      m_currentPacket->GetAddr1());
    }
    m_currentPacket->SetDuration(duration);
  }

  if (m_txParams.MustSendBlockAckRequest()) {
    Ptr<WigigQosTxop> qosWigigTxop = DynamicCast<WigigQosTxop>(m_currentTxop);
    NS_ASSERT(qosWigigTxop);
    auto bar = qosWigigTxop->PrepareBlockAckRequest(
        m_currentPacket->GetAddr1(), *m_currentPacket->GetTids().begin());
    qosWigigTxop->ScheduleBar(bar);
  }
  ForwardDown(m_currentPacket, m_currentTxVector);
}

bool MacLow::IsNavZero() const {
  return (m_lastNavStart + m_lastNavDuration < Simulator::Now());
}

void MacLow::SendCtsToSelf() {
  WigigMacHeader cts;
  cts.SetType(WIFI_MAC_CTL_CTS);
  cts.SetDsNotFrom();
  cts.SetDsNotTo();
  cts.SetNoMoreFragments();
  cts.SetNoRetry();
  cts.SetAddr1(m_self);

  WigigTxVector ctsTxVector = GetRtsTxVector(*m_currentPacket->begin());
  Time duration = Seconds(0);

  duration += GetSifs();
  duration += m_phy->CalculateTxDuration(
      m_currentPacket->GetSize(), m_currentTxVector, m_phy->GetFrequency());
  duration += GetResponseDuration(m_txParams, m_currentTxVector,
                                  m_currentPacket->GetAddr1());
  if (m_txParams.HasNextPacket()) {
    duration += GetSifs();
    duration +=
        m_phy->CalculateTxDuration(m_txParams.GetNextPacketSize(),
                                   m_currentTxVector, m_phy->GetFrequency());
    duration += GetResponseDuration(m_txParams, m_currentTxVector,
                                    m_currentPacket->GetAddr1());
  }

  cts.SetDuration(duration);

  ForwardDown(Create<const WigigPsdu>(Create<Packet>(), cts), ctsTxVector);

  Time txDuration = m_phy->CalculateTxDuration(GetCtsSize(), ctsTxVector,
                                               m_phy->GetFrequency());
  txDuration += GetSifs();
  NS_ASSERT(m_sendDataEvent.IsExpired());

  m_sendDataEvent = Simulator::Schedule(txDuration, &MacLow::SendDataAfterCts,
                                        this, duration);
}

void MacLow::SendCtsAfterRts(Mac48Address source, Time duration,
                             const WigigTxVector &rtsTxVector, double rtsSnr) {
  NS_LOG_FUNCTION(this << source << duration << rtsTxVector.GetMode()
                       << rtsSnr);
  /* send a CTS when you receive a RTS
   * right after SIFS.
   */
  WigigTxVector ctsTxVector = GetCtsTxVector(source, rtsTxVector.GetMode());
  WigigMacHeader cts;
  cts.SetType(WIFI_MAC_CTL_CTS);
  cts.SetDsNotFrom();
  cts.SetDsNotTo();
  cts.SetNoMoreFragments();
  cts.SetNoRetry();
  cts.SetAddr1(source);
  duration -= GetCtsDuration(source, rtsTxVector);
  duration -= GetSifs();
  NS_ASSERT(duration.IsPositive());
  cts.SetDuration(duration);

  Ptr<Packet> packet = Create<Packet>();

  SnrTag tag;
  tag.Set(rtsSnr);
  packet->AddPacketTag(tag);

  // CTS should always use non-HT PPDU (HT PPDU cases not supported yet)
  ForwardDown(Create<const WigigPsdu>(packet, cts), ctsTxVector);
}

void MacLow::SendDmgCtsAfterRts(Mac48Address source, Time duration,
                                const WigigTxVector &rtsTxVector,
                                double rtsSnr) {
  NS_LOG_FUNCTION(this << source << duration << rtsTxVector.GetMode()
                       << rtsSnr);
  /* Send a DMG CTS when we receive a RTS right after SIFS. */
  WigigTxVector ctsTxVector = GetDmgControlTxVector();
  WigigMacHeader cts;
  cts.SetType(WIFI_MAC_CTL_DMG_CTS);
  cts.SetDsNotFrom();
  cts.SetDsNotTo();
  cts.SetNoMoreFragments();
  cts.SetNoRetry();
  cts.SetAddr1(source);
  cts.SetAddr2(GetAddress());

  /* Set duration field */
  duration -= GetDmgCtsDuration();
  duration -= GetSifs();
  NS_ASSERT(duration.IsPositive());
  cts.SetDuration(duration);

  Ptr<Packet> packet = Create<Packet>();

  SnrTag tag;
  tag.Set(rtsSnr);
  packet->AddPacketTag(tag);

  ForwardDown(Create<const WigigPsdu>(packet, cts), ctsTxVector);
}

void MacLow::SendDataAfterCts(Time duration) {
  NS_LOG_FUNCTION(this);
  /* send the third step in a
   * RTS/CTS/Data/Ack handshake
   */
  NS_ASSERT(m_currentPacket);

  StartDataTxTimers(m_currentTxVector);
  Time newDuration = GetResponseDuration(m_txParams, m_currentTxVector,
                                         m_currentPacket->GetAddr1());
  if (m_txParams.HasNextPacket()) {
    newDuration += GetSifs();
    newDuration +=
        m_phy->CalculateTxDuration(m_txParams.GetNextPacketSize(),
                                   m_currentTxVector, m_phy->GetFrequency());
    newDuration += GetResponseDuration(m_txParams, m_currentTxVector,
                                       m_currentPacket->GetAddr1());
  }

  Time txDuration = m_phy->CalculateTxDuration(
      m_currentPacket->GetSize(), m_currentTxVector, m_phy->GetFrequency());
  duration -= txDuration;
  duration -= GetSifs();

  duration = std::max(duration, newDuration);
  NS_ASSERT(duration.IsPositive());
  m_currentPacket->SetDuration(duration);
  if (m_txParams.MustSendBlockAckRequest()) {
    Ptr<WigigQosTxop> qosWigigTxop = DynamicCast<WigigQosTxop>(m_currentTxop);
    NS_ASSERT(qosWigigTxop);
    auto bar = qosWigigTxop->PrepareBlockAckRequest(
        m_currentPacket->GetAddr1(), *m_currentPacket->GetTids().begin());
    qosWigigTxop->ScheduleBar(bar);
  }
  ForwardDown(m_currentPacket, m_currentTxVector);
}

void MacLow::WaitIfsAfterEndTxFragment() {
  NS_LOG_FUNCTION(this);
  m_currentTxop->StartNextFragment();
}

void MacLow::WaitIfsAfterEndTxPacket() {
  NS_LOG_FUNCTION(this);
  m_currentTxop->StartNextPacket();
}

void MacLow::EndTxNoAck() {
  NS_LOG_FUNCTION(this);
  if (m_currentTxop) {
    Ptr<WigigTxop> txop = m_currentTxop;
    txop->EndTxNoAck();
    m_currentTxop = nullptr;
  } else {
    if (m_currentPacket->IsShortSsw()) {
      m_transmissionShortSswCallback();
    } else {
      m_transmissionCallback(m_currentPacket->GetHeader(0));
    }
  }
  m_currentPacket = nullptr;
}

void MacLow::SendAckAfterData(Mac48Address source, Time duration,
                              WifiMode dataTxMode, double dataSnr) {
  NS_LOG_FUNCTION(this);
  if (!m_phy->IsStateTx() && !m_phy->IsStateSwitching()) {
    // send an Ack, after SIFS, when you receive a packet
    WigigTxVector ackTxVector = GetAckTxVector(source, dataTxMode);
    WigigMacHeader ack;
    ack.SetType(WIFI_MAC_CTL_ACK);
    ack.SetDsNotFrom();
    ack.SetDsNotTo();
    ack.SetNoRetry();
    ack.SetNoMoreFragments();
    ack.SetAddr1(source);
    // 802.11-2012, Section 8.3.1.4:  Duration/ID is received duration value
    // minus the time to transmit the Ack frame and its SIFS interval
    duration -= GetAckDuration(ackTxVector);
    duration -= GetSifs();
    NS_ASSERT_MSG(
        duration.IsPositive(),
        "Please provide test case to maintainers if this assert is hit.");
    ack.SetDuration(duration);

    Ptr<Packet> packet = Create<Packet>();

    SnrTag tag;
    tag.Set(dataSnr);
    packet->AddPacketTag(tag);

    // Ack should always use non-HT PPDU (HT PPDU cases not supported yet)
    ForwardDown(Create<const WigigPsdu>(packet, ack), ackTxVector);
  } else {
    NS_LOG_DEBUG("Skip ack after data");
  }
}

bool MacLow::ReceiveMpdu(Ptr<WigigMacQueueItem> mpdu) {
  const WigigMacHeader &hdr = mpdu->GetHeader();

  if (m_stationManager->HasDmgSupported()) {
    Mac48Address originator = hdr.GetAddr2();
    uint8_t tid = 0;
    if (hdr.IsQosData()) {
      tid = hdr.GetQosTid();
    }
    uint16_t seqNumber = hdr.GetSequenceNumber();
    AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
    if (it != m_bAckAgreements.end()) {
      // Implement HT immediate BlockAck support for HT Delayed BlockAck is not
      // added yet
      if (!QosUtilsIsOldPacket((*it).second.first.GetStartingSequence(),
                               seqNumber)) {
        StoreMpduIfNeeded(mpdu);
        if (!IsInWindow(hdr.GetSequenceNumber(),
                        (*it).second.first.GetStartingSequence(),
                        (*it).second.first.GetBufferSize())) {
          uint16_t delta =
              (seqNumber - (*it).second.first.GetWinEnd() + 4096) % 4096;
          NS_ASSERT(delta > 0);
          uint16_t bufferSize = (*it).second.first.GetBufferSize();
          uint16_t startingSeq = (seqNumber - bufferSize + 1 + 4096) % 4096;
          (*it).second.first.SetStartingSequence(startingSeq);
          RxCompleteBufferedPacketsWithSmallerSequence(
              (*it).second.first.GetStartingSequenceControl(), originator, tid);
        }
        RxCompleteBufferedPacketsUntilFirstLost(
            originator,
            tid); // forwards up packets starting from winstart and set winstart
                  // to last +1
      }
      return true;
    }
    return false;
  }
  return StoreMpduIfNeeded(mpdu);
}

bool MacLow::StoreMpduIfNeeded(Ptr<WigigMacQueueItem> mpdu) {
  const WigigMacHeader &hdr = mpdu->GetHeader();

  AgreementsI it =
      m_bAckAgreements.find(std::make_pair(hdr.GetAddr2(), hdr.GetQosTid()));
  if (it != m_bAckAgreements.end()) {
    uint16_t endSequence =
        ((*it).second.first.GetStartingSequence() + 2047) % 4096;
    uint32_t mappedSeqControl = QosUtilsMapSeqControlToUniqueInteger(
        hdr.GetSequenceControl(), endSequence);

    BufferedPacketI i = (*it).second.second.begin();
    for (; i != (*it).second.second.end() &&
           QosUtilsMapSeqControlToUniqueInteger(
               (*i)->GetHeader().GetSequenceControl(), endSequence) <
               mappedSeqControl;
         i++) {
    }
    (*it).second.second.insert(i, mpdu);

    // Update block ack cache
    BlockAckCachesI j =
        m_bAckCaches.find(std::make_pair(hdr.GetAddr2(), hdr.GetQosTid()));
    NS_ASSERT(j != m_bAckCaches.end());
    (*j).second.UpdateWithMpdu(&hdr);
    return true;
  }
  return false;
}

void MacLow::CreateWigigBlockAckAgreement(const MgtAddBaResponseHeader *respHdr,
                                          Mac48Address originator,
                                          uint16_t startingSeq) {
  NS_LOG_FUNCTION(this);
  uint8_t tid = respHdr->GetTid();
  WigigBlockAckAgreement agreement(originator, tid);
  if (respHdr->IsImmediateBlockAck()) {
    agreement.SetImmediateBlockAck();
  } else {
    agreement.SetDelayedBlockAck();
  }
  agreement.SetAmsduSupport(respHdr->IsAmsduSupported());
  agreement.SetBufferSize(respHdr->GetBufferSize() + 1);
  agreement.SetTimeout(respHdr->GetTimeout());
  agreement.SetStartingSequence(startingSeq);

  std::list<Ptr<WigigMacQueueItem>> buffer(0);
  AgreementKey key(originator, respHdr->GetTid());
  AgreementValue value(agreement, buffer);
  m_bAckAgreements.insert(std::make_pair(key, value));

  BlockAckCache cache;
  cache.Init(startingSeq, respHdr->GetBufferSize() + 1);
  m_bAckCaches.insert(std::make_pair(key, cache));

  if (respHdr->GetTimeout() != 0) {
    AgreementsI it =
        m_bAckAgreements.find(std::make_pair(originator, respHdr->GetTid()));
    Time timeout = MicroSeconds(1024 * agreement.GetTimeout());

    AcIndex ac = QosUtilsMapTidToAc(agreement.GetTid());

    it->second.first.m_inactivityEvent =
        Simulator::Schedule(timeout, &WigigQosTxop::SendDelbaFrame, m_edca[ac],
                            originator, tid, false);
  }
}

void MacLow::DestroyWigigBlockAckAgreement(Mac48Address originator,
                                           uint8_t tid) {
  NS_LOG_FUNCTION(this);
  AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
  if (it != m_bAckAgreements.end()) {
    RxCompleteBufferedPacketsWithSmallerSequence(
        it->second.first.GetStartingSequenceControl(), originator, tid);
    RxCompleteBufferedPacketsUntilFirstLost(originator, tid);
    m_bAckAgreements.erase(it);
    BlockAckCachesI i = m_bAckCaches.find(std::make_pair(originator, tid));
    NS_ASSERT(i != m_bAckCaches.end());
    m_bAckCaches.erase(i);
  }
}

void MacLow::RxCompleteBufferedPacketsWithSmallerSequence(
    uint16_t seq, Mac48Address originator, uint8_t tid) {
  AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
  if (it != m_bAckAgreements.end()) {
    uint16_t endSequence =
        ((*it).second.first.GetStartingSequence() + 2047) % 4096;
    uint32_t mappedStart =
        QosUtilsMapSeqControlToUniqueInteger(seq, endSequence);
    BufferedPacketI last = (*it).second.second.begin();
    uint16_t guard = 0;
    if (last != (*it).second.second.end()) {
      guard = (*(*it).second.second.begin())->GetHeader().GetSequenceControl();
    }
    BufferedPacketI i = (*it).second.second.begin();
    for (; i != (*it).second.second.end() &&
           QosUtilsMapSeqControlToUniqueInteger(
               (*i)->GetHeader().GetSequenceControl(), endSequence) <
               mappedStart;) {
      if (guard == (*i)->GetHeader().GetSequenceControl()) {
        if (!(*i)->GetHeader().IsMoreFragments()) {
          while (last != i) {
            m_rxCallback(*last);
            last++;
          }
          m_rxCallback(*last);
          last++;
          /* go to next packet */
          while (i != (*it).second.second.end() &&
                 guard == (*i)->GetHeader().GetSequenceControl()) {
            i++;
          }
          if (i != (*it).second.second.end()) {
            guard = (*i)->GetHeader().GetSequenceControl();
            last = i;
          }
        } else {
          guard++;
        }
      } else {
        /* go to next packet */
        while (i != (*it).second.second.end() &&
               guard == (*i)->GetHeader().GetSequenceControl()) {
          i++;
        }
        if (i != (*it).second.second.end()) {
          guard = (*i)->GetHeader().GetSequenceControl();
          last = i;
        }
      }
    }
    (*it).second.second.erase((*it).second.second.begin(), i);
  }
}

void MacLow::RxCompleteBufferedPacketsUntilFirstLost(Mac48Address originator,
                                                     uint8_t tid) {
  AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
  if (it != m_bAckAgreements.end()) {
    uint16_t guard = (*it).second.first.GetStartingSequenceControl();
    BufferedPacketI lastComplete = (*it).second.second.begin();
    BufferedPacketI i = (*it).second.second.begin();
    for (; i != (*it).second.second.end() &&
           guard == (*i)->GetHeader().GetSequenceControl();
         i++) {
      if (!(*i)->GetHeader().IsMoreFragments()) {
        while (lastComplete != i) {
          m_rxCallback(*lastComplete);
          lastComplete++;
        }
        m_rxCallback(*lastComplete);
        lastComplete++;
      }
      guard = (*i)->GetHeader().IsMoreFragments() ? (guard + 1)
                                                  : ((guard + 16) & 0xfff0);
    }
    (*it).second.first.SetStartingSequenceControl(guard);
    /* All packets already forwarded to WigigMac must be removed from buffer:
    [begin (), lastComplete) */
    (*it).second.second.erase((*it).second.second.begin(), lastComplete);
  }
}

void MacLow::SendBlockAckResponse(const CtrlBAckResponseHeader *blockAck,
                                  Mac48Address originator, bool immediate,
                                  Time duration, WifiMode blockAckReqTxMode,
                                  double rxSnr) {
  NS_LOG_FUNCTION(this);
  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(*blockAck);

  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_CTL_BACKRESP);
  hdr.SetAddr1(originator);
  hdr.SetAddr2(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoRetry();
  hdr.SetNoMoreFragments();

  WigigTxVector blockAckReqTxVector =
      GetBlockAckTxVector(originator, blockAckReqTxMode);

  if (immediate) {
    m_txParams.DisableAck();
    duration -= GetSifs();
    duration -= GetBlockAckDuration(blockAckReqTxVector, blockAck->GetType());
  } else {
    m_txParams.EnableAck();
    duration += GetSifs();
    duration += GetAckDuration(originator, blockAckReqTxVector);
  }
  m_txParams.DisableNextData();

  if (!immediate) {
    StartDataTxTimers(blockAckReqTxVector);
  }

  NS_ASSERT(duration.IsPositive());
  hdr.SetDuration(duration);
  // here should be present a control about immediate or delayed BlockAck
  // for now we assume immediate
  SnrTag tag;
  tag.Set(rxSnr);
  packet->AddPacketTag(tag);
  ForwardDown(Create<const WigigPsdu>(packet, hdr), blockAckReqTxVector);
}

void MacLow::SendBlockAckAfterAmpdu(uint8_t tid, Mac48Address originator,
                                    Time duration,
                                    const WigigTxVector &blockAckReqTxVector,
                                    double rxSnr) {
  NS_LOG_FUNCTION(this);
  if (!m_phy->IsStateTx() && !m_phy->IsStateSwitching()) {
    NS_LOG_FUNCTION(this << +tid << originator << duration.As(Time::S)
                         << blockAckReqTxVector << rxSnr);
    CtrlBAckResponseHeader blockAck;
    uint16_t seqNumber = 0;
    BlockAckCachesI i = m_bAckCaches.find(std::make_pair(originator, tid));
    NS_ASSERT(i != m_bAckCaches.end());
    seqNumber = (*i).second.GetWinStart();

    bool immediate = true;
    AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
    blockAck.SetTidInfo(tid);
    immediate = (*it).second.first.IsImmediateBlockAck();
    if ((*it).second.first.GetBufferSize() > 64) {
      blockAck.SetType(BlockAckType::EXTENDED_COMPRESSED);
    } else {
      blockAck.SetType(BlockAckType::COMPRESSED);
    }
    blockAck.SetStartingSequence(seqNumber);
    NS_LOG_DEBUG("Got Implicit block Ack Req with seq " << seqNumber);
    (*i).second.FillBlockAckBitmap(&blockAck);

    WigigTxVector blockAckTxVector =
        GetBlockAckTxVector(originator, blockAckReqTxVector.GetMode());

    SendBlockAckResponse(&blockAck, originator, immediate, duration,
                         blockAckTxVector.GetMode(), rxSnr);
  } else {
    NS_LOG_DEBUG("Skip block ack response!");
  }
}

void MacLow::SendBlockAckAfterBlockAckRequest(
    const CtrlBAckRequestHeader reqHdr, Mac48Address originator, Time duration,
    WifiMode blockAckReqTxMode, double rxSnr) {
  NS_LOG_FUNCTION(this);
  if (!m_phy->IsStateTx() && !m_phy->IsStateSwitching()) {
    CtrlBAckResponseHeader blockAck;
    uint8_t tid = 0;
    bool immediate = false;
    if (!reqHdr.IsMultiTid()) {
      tid = reqHdr.GetTidInfo();
      AgreementsI it = m_bAckAgreements.find(std::make_pair(originator, tid));
      if (it != m_bAckAgreements.end()) {
        blockAck.SetTidInfo(tid);
        immediate = (*it).second.first.IsImmediateBlockAck();
        if (reqHdr.IsBasic()) {
          blockAck.SetType(BlockAckType::BASIC);
        } else if (reqHdr.IsCompressed()) {
          blockAck.SetType(BlockAckType::COMPRESSED);
        } else if (reqHdr.IsExtendedCompressed()) {
          blockAck.SetType(BlockAckType::EXTENDED_COMPRESSED);
        }
        blockAck.SetStartingSequence(reqHdr.GetStartingSequence());
        BlockAckCachesI i = m_bAckCaches.find(std::make_pair(originator, tid));
        NS_ASSERT(i != m_bAckCaches.end());
        (*i).second.FillBlockAckBitmap(&blockAck);
        NS_LOG_DEBUG("Got block Ack Req with seq "
                     << reqHdr.GetStartingSequence());

        if (!m_stationManager->HasDmgSupported()) {
          /* All packets with smaller sequence than starting sequence control
           * must be passed up to WigigMac See 9.10.3 in IEEE 802.11e standard.
           */
          RxCompleteBufferedPacketsWithSmallerSequence(
              reqHdr.GetStartingSequenceControl(), originator, tid);
          RxCompleteBufferedPacketsUntilFirstLost(originator, tid);
        } else {
          if (!QosUtilsIsOldPacket((*it).second.first.GetStartingSequence(),
                                   reqHdr.GetStartingSequence())) {
            (*it).second.first.SetStartingSequence(
                reqHdr.GetStartingSequence());
            RxCompleteBufferedPacketsWithSmallerSequence(
                reqHdr.GetStartingSequenceControl(), originator, tid);
            RxCompleteBufferedPacketsUntilFirstLost(originator, tid);
          }
        }
      } else {
        NS_LOG_DEBUG("there's not a valid block ack agreement with "
                     << originator);
      }
    } else {
      NS_FATAL_ERROR("Multi-tid block ack is not supported.");
    }
    SendBlockAckResponse(&blockAck, originator, immediate, duration,
                         blockAckReqTxMode, rxSnr);
  } else {
    NS_LOG_DEBUG("Skip block ack response!");
  }
}

void MacLow::ResetBlockAckInactivityTimerIfNeeded(
    WigigBlockAckAgreement &agreement) {
  if (agreement.GetTimeout() != 0) {
    NS_ASSERT(agreement.m_inactivityEvent.IsRunning());
    agreement.m_inactivityEvent.Cancel();
    Time timeout = MicroSeconds(1024 * agreement.GetTimeout());
    AcIndex ac = QosUtilsMapTidToAc(agreement.GetTid());
    agreement.m_inactivityEvent =
        Simulator::Schedule(timeout, &WigigQosTxop::SendDelbaFrame, m_edca[ac],
                            agreement.GetPeer(), agreement.GetTid(), false);
  }
}

void MacLow::RegisterEdcaForAc(AcIndex ac, Ptr<WigigQosTxop> edca) {
  m_edca.insert(std::make_pair(ac, edca));
}

void MacLow::DeaggregateAmpduAndReceive(Ptr<WigigPsdu> psdu, double rxSnr,
                                        const WigigTxVector &txVector,
                                        std::vector<bool> statusPerMpdu) {
  NS_LOG_FUNCTION(this);
  bool normalAck = false;
  bool ampduSubframe = false; // flag indicating the packet belongs to an A-MPDU
                              // and is not a single MPDU
  if (txVector.IsAggregation()) {
    NS_ASSERT(psdu->IsAggregate());

    ampduSubframe = true;
    auto n = psdu->begin();
    auto status = statusPerMpdu.begin();
    NS_ABORT_MSG_IF(psdu->GetNMpdus() != statusPerMpdu.size(),
                    "Should have one receive status per MPDU");

    WigigMacHeader firsthdr = (*n)->GetHeader();

    /* No need to continue processing the received A-MPDU, if we are performing
     * SLS. */
    if (m_servingSls) {
      NS_LOG_DEBUG("Performing SLS BFT, so ignoe the received A-MPDU from "
                   << firsthdr.GetAddr2()
                   << " with sequence=" << firsthdr.GetSequenceNumber());
      return;
    }

    NS_LOG_DEBUG("duration/id=" << firsthdr.GetDuration());
    NotifyNav((*n)->GetPacket(), firsthdr);

    if (firsthdr.GetAddr1() == m_self) {
      // Iterate over all MPDUs and notify reception only if status OK
      for (; n != psdu->end(); ++n, ++status) {
        firsthdr = (*n)->GetHeader();
        NS_ABORT_MSG_IF(
            firsthdr.GetAddr1() != m_self,
            "All MPDUs of A-MPDU should have the same destination address");
        if (*status) // PER and thus CRC check succeeded
        {
          if (psdu->IsSingle()) {
            // If the MPDU is sent as a single MPDU (EOF=1 in A-MPDU subframe
            // header), then the responder sends an Ack.
            NS_LOG_DEBUG("Receive S-MPDU");
            ampduSubframe = false;
          } else if (!m_sendAckEvent.IsRunning() &&
                     firsthdr.IsQosAck()) // Implicit BAR Ack Policy
          {
            m_sendAckEvent = Simulator::Schedule(
                GetSifs(), &MacLow::SendBlockAckAfterAmpdu, this,
                firsthdr.GetQosTid(), firsthdr.GetAddr2(),
                firsthdr.GetDuration(), txVector, rxSnr);
          }

          if (firsthdr.IsAck() || firsthdr.IsBlockAck() ||
              firsthdr.IsBlockAckReq()) {
            ReceiveOk((*n), rxSnr, txVector, ampduSubframe);
          } else if (firsthdr.IsData() || firsthdr.IsQosData()) {
            NS_LOG_DEBUG("Deaggregate packet from "
                         << firsthdr.GetAddr2()
                         << " with sequence=" << firsthdr.GetSequenceNumber());
            ReceiveOk((*n), rxSnr, txVector, ampduSubframe);
            if (firsthdr.IsQosAck()) {
              NS_LOG_DEBUG("Normal Ack");
              normalAck = true;
            }
          } else {
            NS_FATAL_ERROR("Received A-MPDU with invalid first MPDU type");
          }

          if (!psdu->IsSingle()) {
            if (normalAck) {
              // send BlockAck
              if (firsthdr.IsBlockAckReq()) {
                NS_FATAL_ERROR(
                    "Sending a BlockAckReq with QosPolicy equal to Normal Ack");
              }
              uint8_t tid = firsthdr.GetQosTid();
              AgreementsI it = m_bAckAgreements.find(
                  std::make_pair(firsthdr.GetAddr2(), tid));
              if (it != m_bAckAgreements.end()) {
                /* See section 11.5.3 in IEEE 802.11 for the definition of this
                 * timer */
                ResetBlockAckInactivityTimerIfNeeded(it->second.first);
                NS_LOG_DEBUG("rx A-MPDU/sendImmediateBlockAck from="
                             << firsthdr.GetAddr2());
                NS_ASSERT(m_sendAckEvent.IsRunning());
              } else {
                NS_LOG_DEBUG("There's not a valid agreement for this block ack "
                             "request.");
              }
            }
          }
        }
      }
    }
  } else {
    /* Simple MPDU */
    NS_ASSERT(!psdu->IsAggregate());
    /* Check if the MPDU contains a Short SSW packet */
    ReceiveOk((*psdu->begin()), rxSnr, txVector, ampduSubframe);
  }
}

} // namespace ns3
