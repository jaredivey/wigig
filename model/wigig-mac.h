/*
 * Copyright (c) 2015-2021 IMDEA Networks Institute
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_MAC_H
#define WIGIG_MAC_H

#include "codebook.h"
#include "dmg-ati-txop.h"
#include "dmg-sls-txop.h"
#include "wigig-ctrl-headers.h"
#include "wigig-data-types.h"
#include "wigig-mac-header.h"
#include "wigig-mgt-headers.h"
#include "wigig-qos-txop.h"
#include "wigig-remote-station-manager.h"

#include "ns3/dmg-capabilities.h"
#include "ns3/net-device.h"
#include "ns3/wifi-standards.h"

#include <optional>

namespace ns3 {

#define dot11MaxBFTime                                                         \
  1 /* Maximum Beamforming Time (in units of beacon interval). */
#define dot11BFTXSSTime                                                        \
  10 /* Timeout until the initiator restarts ISS (in Milliseconds)." */
#define dot11BFRetryLimit 10 /* Beamforming Retry Limit. */
/**
 * SSW Frame Size = 26 Bytes
 * Ncw = 2, Ldpcw = 160, Ldplcw = 160, dencodedSymbols  = 328
 * Frames duration precalculated using DMG MCS-0 and reported in microseconds
 */
#define sswTxTime                                                              \
  NanoSeconds(                                                                 \
      14909) // NanoSeconds (4291) + NanoSeconds (4654) + NanoSeconds (5964).
#define sswFbckTxTime                                                          \
  NanoSeconds(                                                                 \
      18255) // NanoSeconds (4291) + NanoSeconds (4654) + NanoSeconds (9310).
#define sswAckTxTime                                                           \
  NanoSeconds(                                                                 \
      18255) // NanoSeconds (4291) + NanoSeconds (4654) + NanoSeconds (9310).
#define aAirPropagationTime NanoSeconds(100)
#define aTSFResolution MicroSeconds(1)
#define GUARD_TIME MicroSeconds(10)
#define MBIFS MicroSeconds(9)
#define SLS_FEEDBACK_PHASE_DURATION                                            \
  (sswFbckTxTime + MBIFS + sswAckTxTime + 2 * aAirPropagationTime)
// Timeout Values
#define SSW_ACK_TIMEOUT 2 * sswAckTxTime + 2 * (aAirPropagationTime + MBIFS)
// Global Association Identifiers.
#define AID_AP 0
#define AID_BROADCAST 255
// Antenna Configuration.
#define NO_ANTENNA_CONFIG 255
#define NO_AwvId 255
// Allocation duration for both SPs and CBAPs.
#define BROADCAST_CBAP 0
#define MAX_NUM_BLOCKS 255

class Ssid;
class WigigTxop;
class MacLow;
class WigigMacRxMiddle;
class MacTxMiddle;
class WigigChannelAccessManager;
class WifiInformationElementContainer;
class BeamRefinementElement;
class WigigPhy;

enum FstState {
  FST_INITIAL_STATE = 0,
  FST_SETUP_COMPLETION_STATE,
  FST_TRANSITION_DONE_STATE,
  FST_TRANSITION_CONFIRMED_STATE
};

struct FstSession {
  uint32_t ID; //!< A unique ID that corresponds to a pair of receiver and
               //!< transmitter.
  FstState CurrentState; //!< Current state of the FST State Machine.
  Time STT;              //!< State transition timer.
  bool IsInitiator; //!< Flag to indicate if we are the FST session initiator.
  uint32_t LLT;     //!< Link Loss Time (LLT) for this FST Session.
  EventId LinkLossCountDownEvent; //!< Event for Link Loss Timeout.
};

/**
 * This type defines a mapping between between the MAC Address of
 * the Peer FST and FST Session Variables.
 */
typedef std::map<Mac48Address, FstSession> FstSessionMap;

/** This type defines a mapping between an Access Category index,
and a pointer to the corresponding channel access function */
typedef std::map<AcIndex, Ptr<WigigQosTxop>> EdcaQueues;

enum ChannelAccessPeriod {
  CHANNEL_ACCESS_BTI = 0,
  CHANNEL_ACCESS_ABFT,
  CHANNEL_ACCESS_ATI,
  CHANNEL_ACCESS_BHI,
  CHANNEL_ACCESS_DTI
};

/** Type definitions **/
typedef std::pair<DynamicAllocationInfoField, BfControlField>
    AllocationData; /* Typedef for dynamic allocation of SPs. */
typedef std::list<AllocationData> AllocationDataList;
/**
 * Typedef for antenna configuration for storing SNR valeus during an SLS phase.
 * \param MyAntennaId The ID of the antenna array used for measuring the signal.
 * \param PeerAntennaId The ID of the peer antenna array transmitting the frame.
 * \param PeerSectorId The ID of the peer sector transmitting the frame.
 */
typedef std::tuple<AntennaId, AntennaId, SectorId>
    ANTENNA_CONFIGURATION_COMBINATION;

/**
 * The SLS completion callback structure.
 */
struct SlsCompletionAttrbitutes {
  SlsCompletionAttrbitutes();

  SlsCompletionAttrbitutes(Mac48Address _peerStation,
                           ChannelAccessPeriod _accessPeriod,
                           BeamformingDirection _beamformingDirection,
                           bool _isInitiatorTXSS, bool _isResponderTxss,
                           uint16_t _bftID, AntennaId _antennaID,
                           SectorId _sectorID, double _maxSnr)
      : peerStation(_peerStation), accessPeriod(_accessPeriod),
        beamformingDirection(_beamformingDirection),
        isInitiatorTXSS(_isInitiatorTXSS), isResponderTxss(_isResponderTxss),
        bftID(_bftID), antennaID(_antennaID), sectorID(_sectorID),
        maxSnr(_maxSnr) {}

  Mac48Address peerStation; //!< The MAC address of the peer station.
  ChannelAccessPeriod
      accessPeriod; //!< The current Channel access period BTI or DTI.
  BeamformingDirection beamformingDirection; //!< Initiator or Responder.
  bool isInitiatorTXSS; //!< Flag to indicate if the initiator is TXSS or RXSS.
  bool isResponderTxss; //!< Flag to indicate if the responder is TXSS or RXSS.
  uint16_t bftID;       //!< The ID of the current BFT.
  AntennaId antennaID;  //!< The ID of the selected antenna.
  SectorId sectorID;    //!< The ID of the selected sector.
  double maxSnr; //!< The maximum snr value as a result fo the SLS procedure.
};

/**
 * The IEEE 802.11ad SLS BFT initiator side state machine.
 */
enum SlsInitiatorState {
  SLS_INITIATOR_IDLE = 0,
  SLS_INITIATOR_SECTOR_SELECTOR = 1,
  SLS_INITIATOR_SSW_ACK = 2,
  SLS_INITIATOR_TXSS_PHASE_COMPELTED = 3,
};

/**
 * \ingroup wigig
 * TracedValue Callback signature for SlsInitiatorState
 *
 * \param [in] oldState original state of the traced variable
 * \param [in] newState new state of the traced variable
 */
typedef void (*SlsInitiatorTracedValueCallback)(
    const SlsInitiatorState oldState, const SlsInitiatorState newState);

/**
 * The IEEE 802.11ad SLS BFT responder side state machine.
 */
enum SlsResponderState {
  SLS_RESPONDER_IDLE = 0,
  SLS_RESPONDER_SECTOR_SELECTOR = 1,
  SLS_RESPONDER_SSW_FBCK = 2,
  SLS_RESPONDER_TXSS_PHASE_PRECOMPLETE = 3,
  SLS_RESPONDER_TXSS_PHASE_COMPELTED = 4,
};

/**
 * \ingroup wigig
 * TracedValue Callback signature for SlsResponderState
 *
 * \param [in] oldState original state of the traced variable
 * \param [in] newState new state of the traced variable
 */
typedef void (*SlsResponderTracedValueCallback)(
    const SlsResponderState oldState, const SlsResponderState newState);

enum BrpTrainingType {
  BRP_TRN_T = 0,
  BRP_TRN_R = 1,
  BRP_TRN_T_R = 2,
};

enum BeamLinkMaintenanceTimerState {
  BEAM_LINK_MAINTENANCE_TIMER_RELEASE,
  BEAM_LINK_MAINTENANCE_TIMER_RESET,
  BEAM_LINK_MAINTENANCE_TIMER_SETUP_RELEASE,
  BEAM_LINK_MAINTENANCE_TIMER_HALT,
  BEAM_LINK_MAINTENANCE_TIMER_EXPIRES
};

/**
 * The Group Beamforming completion callback structure.
 */
struct GroupBfCompletionAttrbitutes {
  GroupBfCompletionAttrbitutes();

  GroupBfCompletionAttrbitutes(Mac48Address _peerStation,
                               BeamformingDirection _beamformingDirection,
                               uint16_t _bftID, AntennaId _antennaID,
                               SectorId _sectorID, AwvId _awvID, double _maxSnr)
      : peerStation(_peerStation), beamformingDirection(_beamformingDirection),
        bftID(_bftID), antennaID(_antennaID), sectorID(_sectorID),
        awvID(_awvID), maxSnr(_maxSnr) {}

  Mac48Address peerStation; //!< The MAC address of the peer station.
  BeamformingDirection beamformingDirection; //!< Initiator or Responder.
  uint16_t bftID;                            //!< The ID of the current BFT.
  AntennaId antennaID; //!< The ID of the selected antenna.
  SectorId sectorID;   //!< The ID of the selected sector.
  AwvId awvID;         //!< The ID of the select AWV.
  double maxSnr; //!< The maximum snr value as a result fo the SLS procedure.
};

/**
 * \brief IEEE 802.11ad/ay Upper MAC.
 * \ingroup wigig
 *
 * Abstract class for Directional Multi Gigabit (DMG) support.
 */
class WigigMac : public Object {
public:
  static TypeId GetTypeId();

  WigigMac();
  ~WigigMac() override;

  /**
   * Sets the device this PHY is associated with.
   *
   * \param device the device this PHY is associated with
   */
  void SetDevice(const Ptr<NetDevice> device);
  /**
   * Return the device this PHY is associated with
   *
   * \return the device this PHY is associated with
   */
  Ptr<NetDevice> GetDevice() const;
  /**
   * \param slotTime the slot duration
   */
  virtual void SetSlot(Time slotTime);
  /**
   * \param sifs the SIFS duration
   */
  virtual void SetSifs(Time sifs);
  /**
   * \param eifsNoDifs the duration of an EIFS minus DIFS.
   *
   * This value is used to calculate the EIFS depending
   * on AIFSN.
   */
  virtual void SetEifsNoDifs(Time eifsNoDifs);
  /**
   * \param pifs the PIFS duration.
   */
  virtual void SetPifs(Time pifs);
  /**
   * \param ackTimeout the duration of an Ack timeout.
   */
  virtual void SetAckTimeout(Time ackTimeout);
  /**
   * \param delay the max propagation delay.
   *
   * Unused for now.
   */
  void SetMaxPropagationDelay(Time delay);
  /**
   * Enable or disable CTS-to-self feature.
   *
   * \param enable true if CTS-to-self is to be supported,
   *               false otherwise
   */
  void SetCtsToSelfSupported(bool enable);
  /**
   * \param bssid the BSSID of the network that this device belongs to.
   */
  void SetBssid(Mac48Address bssid);
  /**
   * \param ssid the current SSID of this MAC layer.
   */
  virtual void SetSsid(Ssid ssid);
  /**
   * \brief Sets the interface in promiscuous mode.
   *
   * Enables promiscuous mode on the interface. Note that any further
   * filtering on the incoming frame path may affect the overall
   * behavior.
   */
  virtual void SetPromisc();
  /**
   * \return the current PIFS duration.
   */
  virtual Time GetPifs() const;
  /**
   * \return the current SIFS duration.
   */
  virtual Time GetSifs() const;
  /**
   * \return the current slot duration.
   */
  virtual Time GetSlot() const;
  /**
   * \return the current EIFS minus DIFS duration
   */
  virtual Time GetEifsNoDifs() const;
  /**
   * \return the current Ack timeout duration.
   */
  virtual Time GetAckTimeout() const;
  /**
   * \return the maximum propagation delay.
   *
   * Unused for now.
   */
  Time GetMaxPropagationDelay() const;
  /**
   * \return the MAC address associated to this MAC layer.
   */
  virtual Mac48Address GetAddress() const;
  /**
   * \return the SSID which this MAC layer is going to try to stay in.
   */
  virtual Ssid GetSsid() const;
  /**
   * \return the BSSID of the network this device belongs to.
   */
  virtual Mac48Address GetBssid() const;
  /**
   * Get Association Identifier (AID).
   * \return The AID of the station.
   */
  virtual uint16_t GetAssociationId() = 0;
  /**
   * \param address the current address of this MAC layer.
   */
  virtual void SetAddress(Mac48Address address);
  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   * \param from the address from which the packet should be sent.
   *
   * The packet should be enqueued in a TX queue, and should be
   * dequeued as soon as the DCF function determines that
   * access it granted to this MAC. The extra parameter "from" allows
   * this device to operate in a bridged mode, forwarding received
   * frames without altering the source address.
   */
  virtual void Enqueue(Ptr<Packet> packet, Mac48Address to, Mac48Address from);
  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   *
   * The packet should be enqueued in a tx queue, and should be
   * dequeued as soon as the channel access function determines that
   * access is granted to this MAC.
   */
  virtual void Enqueue(Ptr<Packet> packet, Mac48Address to) = 0;
  /**
   * \return if this MAC supports sending from arbitrary address.
   *
   * The interface may or may not support sending from arbitrary address.
   * This function returns true if sending from arbitrary address is supported,
   * false otherwise.
   */
  virtual bool SupportsSendFrom() const;
  /**
   * \param phy the physical layer attached to this MAC.
   */
  virtual void SetPhy(Ptr<WigigPhy> phy);
  /**
   * \return the physical layer attached to this MAC
   */
  virtual Ptr<WigigPhy> GetPhy() const;
  /**
   * \param stationManager the station manager attached to this MAC.
   */
  virtual void
  SetWifiRemoteStationManager(Ptr<WigigRemoteStationManager> stationManager);
  /**
   * \return the station manager attached to this MAC.
   */
  virtual Ptr<WigigRemoteStationManager> GetWifiRemoteStationManager() const;

  /**
   * This type defines the callback of a higher layer that a
   * WigigMac(-derived) object invokes to pass a packet up the stack.
   *
   * \param packet the packet that has been received.
   * \param from the MAC address of the device that sent the packet.
   * \param to the MAC address of the device that the packet is destined for.
   */
  typedef Callback<void, Ptr<const Packet>, Mac48Address, Mac48Address>
      ForwardUpCallback;

  /**
   * \param upCallback the callback to invoke when a packet must be
   *        forwarded up the stack.
   */
  virtual void SetForwardUpCallback(
      Callback<void, Ptr<const Packet>, Mac48Address, Mac48Address> upCallback);
  /**
   * \param linkUp the callback to invoke when the link becomes up.
   */
  virtual void SetLinkUpCallback(Callback<void> linkUp);
  /**
   * \param linkDown the callback to invoke when the link becomes down.
   */
  virtual void SetLinkDownCallback(Callback<void> linkDown);

  /**
   * \param blockAckTimeout the duration for basic BlockAck timeout.
   *
   * Sets the timeout for basic BlockAck.
   */
  virtual void SetBasicBlockAckTimeout(Time blockAckTimeout);
  /**
   * \return the current basic BlockAck timeout duration.
   */
  virtual Time GetBasicBlockAckTimeout() const;
  /**
   * \param blockAckTimeout the duration for compressed BlockAck timeout
   *
   * Sets the timeout for compressed BlockAck.
   */
  virtual void SetCompressedBlockAckTimeout(Time blockAckTimeout);
  /**
   * \return the current compressed BlockAck timeout duration.
   */
  virtual Time GetCompressedBlockAckTimeout() const;

  /**
   * \param packet the packet being enqueued
   *
   * Public method used to fire a MacTx trace. Implemented for encapsulation
   * purposes. Note this trace indicates that the packet was accepted by the
   * device only. The packet may be dropped later (e.g. if the queue is full).
   */
  void NotifyTx(Ptr<const Packet> packet);
  /**
   * \param packet the packet being dropped
   *
   * Public method used to fire a MacTxDrop trace. Implemented for encapsulation
   * purposes. This trace indicates that the packet was dropped before it was
   * transmitted (e.g. when a STA is not associated with an AP).
   */
  void NotifyTxDrop(Ptr<const Packet> packet);
  /**
   * \param packet the packet we received
   *
   * Public method used to fire a MacRx trace. Implemented for encapsulation
   * purposes.
   */
  void NotifyRx(Ptr<const Packet> packet);
  /**
   * \param packet the packet we received promiscuously
   *
   * Public method used to fire a MacPromiscRx trace. Implemented for
   * encapsulation purposes.
   */
  void NotifyPromiscRx(Ptr<const Packet> packet);
  /**
   * \param packet the packet we received but is not destined for us
   *
   * Public method used to fire a MacRxDrop trace. Implemented for encapsulation
   * purposes.
   */
  void NotifyRxDrop(Ptr<const Packet> packet);

  /**
   * \param standard the wifi standard to be configured
   *
   * This method sets standards-compliant defaults for WigigMac
   * parameters such as SIFS time, slot time, timeout values, etc.,
   * based on the standard selected.
   *
   * \sa WigigMac::Configure80211ad
   */
  void ConfigureStandard(WifiStandard standard);

  /**
   * Accessor for the DCF object
   *
   * \return a smart pointer to WigigTxop
   */
  Ptr<WigigTxop> GetTxop() const;

  /**
   * Accessor for the AC_VO channel access function
   *
   * \return a smart pointer to WigigQosTxop
   */
  Ptr<WigigQosTxop> GetVOQueue() const;
  /**
   * Accessor for the AC_VI channel access function
   *
   * \return a smart pointer to WigigQosTxop
   */
  Ptr<WigigQosTxop> GetVIQueue() const;
  /**
   * Accessor for the AC_BE channel access function
   *
   * \return a smart pointer to WigigQosTxop
   */
  Ptr<WigigQosTxop> GetBEQueue() const;
  /**
   * Accessor for the AC_BK channel access function
   *
   * \return a smart pointer to WigigQosTxop
   */
  Ptr<WigigQosTxop> GetBKQueue() const;
  /**
   * Accessor for all EDCA Queues.
   */
  EdcaQueues GetEdcaQueues() const;
  /**
   * Get Type Of Station.
   * \return station type
   */
  TypeOfStation GetTypeOfStation() const;

  /**
   * Steer the directional antenna towards specific station for transmission.
   * \param address The MAC address of the peer station.
   */
  void SteerTxAntennaToward(Mac48Address address, bool isData = false);
  /**
   * Steer the directional antenna towards specific station for transmission and
   * reception. \param address The MAC address of the peer station.
   */
  void SteerAntennaToward(Mac48Address address, bool isData = false);
  /**
   * Steer the directional antenna towards specific station for transmission in
   * SISO mode and set the reception in omni-mode. \param address The MAC
   * address of the peer station.
   */
  void SteerSisoTxAntennaToward(Mac48Address address);
  /**
   * Steer the directional antenna towards specific station for transmission and
   * reception in SISO mode. \param address The MAC address of the peer station.
   */
  void SteerSisoAntennaToward(Mac48Address address);

  /**
   * Calculate the duration of a single sweep based on the number of sectors.
   * \param sectors The number of sectors.
   * \return The total duration required for completing a single sweep.
   */
  Time CalculateSectorSweepDuration(uint8_t sectors);
  /**
   * This function is used to calculate the amount of time to stay in a specific
   * Quasi-omni pattern while the peer side is sweeping across all its antennas.
   * \param peerAntennas The total number of antennas in the peer device.
   * \param peerSectors The total number of sectors over all DMG antennas in the
   * peer device. \return The total duration required for completing a single
   * sweep.
   */
  Time CalculateSingleAntennaSweepDuration(uint8_t peerAntennas,
                                           uint8_t peerSectors);
  /**
   * Calculate the duration of a sector sweep based on the number of sectors and
   * antennas. \param peerAntennas The total number of antennas in the peer
   * device. \param myAntennas The total number of antennas in the current
   * device. \param mySectors The total number of sectors over all DMG antennas
   * in the current device. \return The total duration required for completing a
   * single sweep.
   */
  Time CalculateSectorSweepDuration(uint8_t peerAntennas, uint8_t myAntennas,
                                    uint8_t mySectors);
  /**
   * Calculate the total duration for Beamforming Training (SLS Only).
   * \param initiatorAntennas The number of antenna arrays in the initiator
   * station. \param initiatorSectors The number of Tx/Rx sectors in the
   * initiator station. \param initiatorAntennas The number of antenna arrays in
   * the responds stationr. \param responderSectors The number of Tx/Rx sectors
   * in the responder station. \return The total duration required for
   * completing beamforming training.
   */
  Time CalculateTotalBeamformingTrainingDuration(uint8_t initiatorAntennas,
                                                 uint8_t initiatorSectors,
                                                 uint8_t responderAntennas,
                                                 uint8_t responderSectors);
  /**
   * Store the DMG capabilities of a DMG STA.
   * \param wifiMac Pointer to the DMG STA.
   */
  void StorePeerDmgCapabilities(Ptr<WigigMac> wifiMac);
  /**
   * Retrieve the DMG Capbilities of a peer DMG station.
   * \param stationAddress The MAC address of the peer station.
   * \return DMG Capbilities of the Peer station if exists otherwise null.
   */
  std::optional<DmgCapabilities>
  GetPeerStationDmgCapabilities(Mac48Address stationAddress) const;
  /**
   * Calculate the duration of the Beamforming Training (SLS Only) based on the
   * capabilities of the responder. \param responderAddress The MAC address of
   * the responder. \param isInitiatorTXSS Indicate whether the initiator is
   * TXSS or RXSS. \param isResponderTxss Indicate whether the respodner is TXSS
   * or RXSS. \return The total duration required for completing beamforming
   * training.
   */
  Time ComputeBeamformingAllocationSize(Mac48Address responderAddress,
                                        bool isInitiatorTXSS,
                                        bool isResponderTxss);
  /**
   * Initiate Beamforming with a particular DMG STA.
   * \param peerAid The AID of the peer DMG STA.
   * \param peerAddress The address of the DMG STA to perform beamforming
   * training with. \param isInitiator Indicate whether we are the Beamforming
   * Initiator. \param isInitiatorTXSS Indicate whether the initiator is TXSS or
   * RXSS. \param isResponderTxss Indicate whether the respodner is TXSS or
   * RXSS. \param length The length of the Beamforming Service Period.
   */
  void StartBeamformingTraining(uint8_t peerAid, Mac48Address peerAddress,
                                bool isInitiator, bool isInitiatorTXSS,
                                bool isResponderTxss, Time length);

  /**
   * Initiate TXSS in CBAP allocation using TxOP.
   * \param peerAddress The address of the DMG STA to perform beamforming
   * training with.
   */
  virtual void PerformTxssTxop(Mac48Address peerAddress);
  /**
   * Set Codebook
   * \param codebook The codebook for antennas and sectors patterns.
   */
  void SetCodebook(Ptr<Codebook> codebook);
  /**
   * Get Codebook
   * \return A pointer to the current codebook.
   */
  Ptr<Codebook> GetCodebook() const;
  /**
   * Initiate BRP transaction with a peer sation in ATI access period.
   * \param receiver The address of the peer station.
   * \param lRx The compressed number of TRN-R subfields requested by the
   * transmitting STA as part of beam refinement. \param txTrnReq To indicate
   * that the STA needs transmit training as part of beam refinement.
   */
  void InitiateBrpTransaction(Mac48Address receiver, uint8_t lRx,
                              bool txTrnReq);

  /* Temporary Function to store AID mapping */
  void MapAidToMacAddress(uint16_t aid, Mac48Address address);

  /**
   * Get current access period in the current beacon interval
   * \return The type of the access period (BTI/A-BFT/ATI or DTI).
   */
  ChannelAccessPeriod GetCurrentAccessPeriod() const;
  /**
   * Get allocation type for the current period.
   * \return The type of the current allocation (SP or CBAP).
   */
  AllocationType GetCurrentAllocation() const;

protected:
  void DoDispose() override;
  void DoInitialize() override;
  virtual void Configure80211ad();

  /**
   * \param dcf the DCF to be configured
   * \param cwmin the minimum contention window for the DCF
   * \param cwmax the maximum contention window for the DCF
   * \param isDsss flag to indicate whether PHY is DSSS or HR/DSSS
   * \param ac the access category for the DCF
   *
   * Configure the DCF with appropriate values depending on the given access
   * category.
   */
  void ConfigureDcf(Ptr<WigigTxop> dcf, uint32_t cwmin, uint32_t cwmax,
                    bool isDsss, AcIndex ac);

  /**
   * \param cwMin the minimum contention window size
   * \param cwMax the maximum contention window size
   *
   * This method is called to set the minimum and the maximum
   * contention window size.
   */
  void ConfigureContentionWindow(uint32_t cwMin, uint32_t cwMax);

  /**
   * This method is invoked by a subclass to specify what type of
   * station it is implementing. This is something that the channel
   * access functions (instantiated within this class as WigigQosTxop's)
   * need to know.
   *
   * \param type the type of station.
   */
  void SetTypeOfStation(TypeOfStation type);

  /**
   * Initiator SLS TXSS TxOP is granted to the station.
   * \param peerAddress The address of the DMG STA to perform beamforming
   * training with.
   */
  void TxssTxopAccessGranted(Mac48Address peerAddress, SlsRole slsRole,
                             bool isFeedback);
  /**
   * Reset SLS initiator related variables.
   */
  void ResetSlsInitiatorVariables();
  /**
   * Reset SLS responder related variables.
   */
  void ResetSlsResponderVariables();
  /**
   * Reset SLS BFT related variables.
   */
  void ResetSlsStateMachineVariables();
  /**
   * Initialize Sector Sweep Parameters for TXSS.
   * \param peerAddress The address of the DMG STA to perform beamforming
   * training with.
   */
  bool InitializeSectorSweepParameters(Mac48Address peerAddress);
  /**
   * Start Beamforming Initiator Phase.
   */
  void StartBeamformingInitiatorPhase();
  /**
   * Start Beamforming Responder Phase.
   * \address The MAC address of the peer station.
   */
  void StartBeamformingResponderPhase(Mac48Address address);
  /**
   * Terminate service period.
   */
  void EndServicePeriod();
  /**
   * Set whether PCP Handover is supported or not.
   * \param value Set to true if PCP handover is enabled otherwise it is set to
   * false.
   */
  void SetPcpHandoverSupport(bool value);
  /**
   * \return Whether the PCP handover is supported or not.
   */
  bool GetPcpHandoverSupport() const;

  /**
   * Send Information Request frame.
   * \param to The MAC address of the receiving station.
   * \param packet the packet containing the Request Element.
   */
  void SendInformationRequest(Mac48Address to, Ptr<Packet> packet);
  /**
   * Send Information Response to the station that requested the information.
   * \param to The MAC address of the station requested the information.
   * \param packet the packet containing the Response Element.
   */
  void SendInformationResponse(Mac48Address to, Ptr<Packet> packet);
  /**
   * Return the DMG capability of the current STA.
   * \return the DMG capability that we support
   */
  virtual DmgCapabilities GetDmgCapabilities() const = 0;

  /**
   * \param sbifs the value of Short Beamforming Interframe Space (SBIFS).
   */
  void SetSbifs(Time sbifs);
  /**
   * \param mbifs the value of Medium Beamforming Interframe Space (MBIFS).
   */
  void SetMbifs(Time mbifs);
  /**
   * \param lbifs the value of Large Beamforming Interframe Space (LBIFS).
   */
  void SetLbifs(Time lbifs);
  /**
   * \param brpifs the value of Beam Refinement Protocol Interframe Spacing
   * (BRPIFS).
   */
  void SetBrpifs(Time brpifs);
  /**
   * \return The value of of Short Beamforming Interframe Space (SBIFS)
   */
  Time GetSbifs() const;
  /**
   * \return The the value of Medium Beamforming Interframe Space (MBIFS).
   */
  Time GetMbifs() const;
  /**
   * \return The value of Large Beamforming Interframe Space (LBIFS).
   */
  Time GetLbifs() const;
  /**
   * \return The value of Beam Refinement Protocol Interframe Spacing (BRPIFS).
   */
  Time GetBrpifs() const;
  /**
   * Get sector sweep duration either at the initiator or at the responder.
   * \param sectors The number of sectors in the duration.
   * \return The total duration required to complete sector sweep.
   */
  Time GetSectorSweepDuration(uint8_t sectors);
  /**
   * Get sector sweep slot time in A-BFT access period.
   * \param fss The number of sectors in a slot.
   * \return The duration of a single slot in A-BFT access period.
   */
  Time GetSectorSweepSlotTime(uint8_t fss);
  /**
   * Map received SNR value to a specific address and TX antenna configuration
   * (The Tx of the peer station). \param address The MAC address of the
   * receiver. \param antennaID The ID of the transmitting antenna. \param
   * sectorID The ID of the transmitting sector. \param snr The received Signal
   * to Noise Ration in dB.
   */
  void MapTxSnr(Mac48Address address, AntennaId antennaID, SectorId sectorID,
                double snr);
  /**
   * Map received SNR value to a specific address and TX antenna configuration
   * (The Tx of the peer station). \param address The MAC address of the
   * receiver. \param antennaID The ID of the receiving antenna \param antennaID
   * The ID of the transmitting antenna. \param sectorID The ID of the
   * transmitting sector. \param snr The received Signal to Noise Ration in dB.
   */
  void MapTxSnr(Mac48Address address, AntennaId RxAntennaId,
                AntennaId TxAntennaId, SectorId sectorID, double snr);
  /**
   * Map received SNR value to a specific address and RX antenna configuration
   * (The Rx of the calling station). \param address The address of the
   * receiver. \param antennaID The ID of the receiving antenna. \param sectorID
   * The ID of the receiving sector. \param snr The received Signal to Noise
   * Ration in dB.
   */
  void MapRxSnr(Mac48Address address, AntennaId antennaID, SectorId sectorID,
                double snr);
  /**
   * Get the remaining time for the current allocation period.
   * \return The remaining time for the current allocation period.
   */
  Time GetRemainingAllocationTime() const;
  /**
   * Get the remaining time for the current sector sweep.
   * \return The remaining time for the current sector sweep.
   */
  Time GetRemainingSectorSweepTime() const;
  /**
   * Transmit control frame using either CBAP or SP access period.
   * \param packet
   * \param hdr
   * \param duration The duration in the SSW field.
   */
  void TransmitControlFrame(Ptr<const Packet> packet, WigigMacHeader &hdr,
                            Time duration);
  /**
   * Transmit control frame using SP access period.
   * \param packet
   * \param hdr
   * \param duration The duration in the SSW field.
   */
  void TransmitControlFrameImmediately(Ptr<const Packet> packet,
                                       WigigMacHeader &hdr, Time duration);
  /**
   * \brief Receive Sector Sweep Frame.
   * \param packet Pointer to the sector sweep packet.
   * \param hdr The MAC header of the packet.
   */
  void ReceiveSectorSweepFrame(Ptr<Packet> packet, const WigigMacHeader *hdr);
  /**
   * Record Beamformed Link Maintenance Value
   * \param field The BF Link Maintenance Field in SSW-FBCK/ACK.
   */
  void RecordBeamformedLinkMaintenanceValue(BfLinkMaintenanceField field);
  /**
   * Switch to the next Quasi-omni pattern in the codebook
   * \param switchTime The time when to switch to the next Quasi-Omni Pattern.
   */
  void SwitchQuasiOmniPattern(Time switchTime);
  /**
   * Restart Initiator Sector Sweep (ISS) Phase.
   * \param stationAddress The MAC address of the peer station we want to
   * perform ISS with.
   */
  void RestartInitiatorSectorSweep(Mac48Address stationAddress);
  /**
   * Start Transmit Sector Sweep (TXSS) with specific station.
   * \param address The MAC address of the peer DMG STA.
   * \param direction Indicate whether we are initiator or responder.
   */
  void StartTransmitSectorSweep(Mac48Address address,
                                BeamformingDirection direction);
  /**
   * Start Receive Sector Sweep (RXSS) with specific station.
   * \param address The MAC address of the peer DMG STA.
   * \param direction Indicate whether we are beamforming initiator or
   * responder.
   */
  void StartReceiveSectorSweep(Mac48Address address,
                               BeamformingDirection direction);
  /**
   * Send Receive Sector Sweep Frame as Initiator.
   * \param address The MAC address of the respodner.
   * \param count the remaining number of sectors.
   * \param direction Indicate whether we are beamforming initiator or
   * responder.
   */
  void SendReceiveSectorSweepFrame(Mac48Address address, uint16_t count,
                                   BeamformingDirection direction);
  /**
   * Send a Transmit Sector Sweep Frame as an Initiator.
   * \param address The MAC address of the respodner.
   */
  void SendInitiatorTransmitSectorSweepFrame(Mac48Address address);
  /**
   * Send a Transmit Sector Sweep Frame as a Responder.
   * \param address The MAC address of the initiator.
   */
  void SendRespodnerTransmitSectorSweepFrame(Mac48Address address);
  /**
   * Send SSW FBCK Frame for SLS phase in a scheduled service period or after
   * SSW-Slot in A-BFT. \param receiver The MAC address of the responding
   * station. \param duration The duration in the MAC header.
   */
  void SendSswFbckFrame(Mac48Address receiver, Time duration);
  /**
   * Re-Send SSW FBCK Frame for SLS phase in a scheduled service period.
   */
  void ResendSswFbckFrame();
  /**
   * Send SSW ACK Frame for SLS phase in a scheduled service period.
   * \param receiver The MAC address of the peer DMG STA.
   * \param sswFbckDuration The duration in the SSW-FBCK Field.
   */
  void SendSswAckFrame(Mac48Address receiver, Time sswFbckDuration);
  /**
   * The BRP setup subphase is used to set up beam refinement transaction.
   * \param type The type of BRP (Either Transmit, Receive, or both).
   * \param address The MAC address of the receiving station (Responder's MAC
   * Address).
   */
  void InitiateBrpSetupSubphase(BrpTrainingType type, Mac48Address receiver);
  /**
   * Send BRP Frame without TRN Field to a specific station.
   * \param receiver The MAC address of the receiving station.
   * \param requestField The BRP Request Field.
   * \param element The Beam Refinement Element.
   */
  void SendEmptyBrpFrame(Mac48Address receiver, BrpRequestField &requestField,
                         BeamRefinementElement &element);
  /**
   * Send BRP Frame with TRN Field to a specific station.
   * \param receiver The MAC address of the receiving station.
   * \param requestField The BRP Request Field.
   * \param element The Beam Refinement Element.
   */
  void SendBrpFrame(Mac48Address receiver, BrpRequestField &requestField,
                    BeamRefinementElement &element, bool requestBeamRefinement,
                    PacketType packetType, uint8_t trainingFieldLength);

  /**
   * This function is called by derived call to notify that BRP Phase has
   * completed.
   */
  virtual void NotifyBrpPhaseCompleted() = 0;

  /* Typedefs for Recording SNR Value per Antenna Configuration */
  typedef std::map<ANTENNA_CONFIGURATION_COMBINATION, double>
      SNR_MAP; /* Typedef for Map between Antenna Config and SNR. */
  typedef SNR_MAP
      SNR_MAP_TX; /* Typedef for SNR TX for each antenna configuration. */
  typedef SNR_MAP
      SNR_MAP_RX; /* Typedef for SNR RX for each antenna configuration. */
  typedef std::pair<SNR_MAP_TX, SNR_MAP_RX>
      SNR_PAIR; /* Typedef for SNR RX for each antenna configuration. */
  typedef std::map<Mac48Address, SNR_PAIR>
      STATION_SNR_PAIR_MAP; /* Typedef for Map between stations and their SNR
                               Table. */
  typedef STATION_SNR_PAIR_MAP::iterator
      STATION_SNR_PAIR_MAP_I; /* Typedef for iterator over SNR MAPPING Table. */

  /* Typedefs for Recording Best Antenna Configuration per Station */
  typedef ANTENNA_CONFIGURATION
      ANTENNA_CONFIGURATION_TX; /* Typedef for best TX antenna configuration. */
  typedef ANTENNA_CONFIGURATION
      ANTENNA_CONFIGURATION_RX; /* Typedef for best RX antenna configuration. */
  typedef std::tuple<ANTENNA_CONFIGURATION_TX, ANTENNA_CONFIGURATION_RX, double>
      BEST_ANTENNA_CONFIGURATION;
  typedef std::map<Mac48Address, BEST_ANTENNA_CONFIGURATION>
      STATION_ANTENNA_CONFIG_MAP;
  typedef STATION_ANTENNA_CONFIG_MAP::iterator STATION_ANTENNA_CONFIG_MAP_I;
  typedef STATION_ANTENNA_CONFIG_MAP::const_iterator
      STATION_ANTENNA_CONFIG_MAP_CI;
  typedef std::pair<AwvId_TX, AwvId_RX> BEST_AwvId;
  typedef std::map<Mac48Address, BEST_AwvId>
      STATION_Awv_MAP; /* Typedef for mapping the best AWV ID for each STA. */

  /**
   * Update Best Tx antenna configuration towards specific station.
   * \param stationAddress The MAC address of the peer station.
   * \param antennaConfigTx Best transmit antenna configuration.
   * \param snr the SNR value.
   */
  void
  UpdateBestTxAntennaConfiguration(const Mac48Address stationAddress,
                                   ANTENNA_CONFIGURATION_TX antennaConfigTx,
                                   double snr);
  /**
   * Update Best Rx antenna configuration towards specific station.
   * \param stationAddress The MAC address of the peer station.
   * \param antennaConfigRx Best receive antenna configuration.
   * \param snr the SNR value.
   */
  void
  UpdateBestRxAntennaConfiguration(const Mac48Address stationAddress,
                                   ANTENNA_CONFIGURATION_RX antennaConfigRx,
                                   double snr);
  /**
   * Obtain antenna configuration for the highest received SNR to feed it back
   * \param stationAddress The MAC address of the station.
   * \param isTxConfiguration Is the Antenna Tx Configuration we are searching
   * for or not.
   */
  ANTENNA_CONFIGURATION
  GetBestAntennaConfiguration(const Mac48Address stationAddress,
                              bool isTxConfiguration);
  /**
   * Obtain antenna configuration for the highest received SNR to feed it back
   * \param stationAddress The MAC address of the station.
   * \param isTxConfiguration Is the Antenna Tx Configuration we are searching
   * for or not. \param maxSnr The SNR value corresponding to the BEst Antenna
   * Configuration.
   */
  ANTENNA_CONFIGURATION
  GetBestAntennaConfiguration(const Mac48Address stationAddress,
                              bool isTxConfiguration, double &maxSnr);
  /**
   * Get Relay Capabilities Information for this DMG STA.
   * \return
   */
  RelayCapabilitiesInfo GetRelayCapabilitiesInfo() const;
  /**
   * Get DMG Relay Capabilities Element supported by this DMG STA.
   * \return The DMG Relay Capabilities Information Element.
   */
  RelayCapabilitiesElement GetRelayCapabilitiesElement() const;
  /**
   * Start Beacon Interval (BI)
   */
  virtual void StartBeaconInterval() = 0;
  /**
   * End Beacon Interval (BI)
   */
  virtual void EndBeaconInterval() = 0;
  /**
   * Start Beacon Transmission Interval (BTI)
   */
  virtual void StartBeaconTransmissionInterval() = 0;
  /**
   * Start Association Beamform Training (A-BFT).
   */
  virtual void StartAssociationBeamformTraining() = 0;
  /**
   * Start Announcement Transmission Interval (ATI).
   */
  virtual void StartAnnouncementTransmissionInterval() = 0;
  /**
   * Start Data Transmission Interval (DTI).
   */
  virtual void StartDataTransmissionInterval() = 0;
  /**
   * Resume any Pending TXSS requests in the current CBAP allocation.
   */
  virtual void ResumePendingTxss();
  /**
   * Start contention based access period (CBAP).
   * \param contentionDuration The duration of the contention period.
   */
  void StartContentionPeriod(AllocationId allocationId,
                             Time contentionDuration);
  /**
   * End Contention Period.
   */
  void EndContentionPeriod();
  /**
   *BeamLink Maintenance Timeout.
   */
  virtual void BeamLinkMaintenanceTimeout();
  /**
   * Schedule the service period (SP) allocation blocks at the start of DTI.
   * \param blocks The number of time blocks making up the allocation.
   * \param spStart
   * \param spLength
   * \param spPeriod
   * \param length The length of the allocation period.
   * \param peerAid The AID of the peer station in this service period.
   * \param peerStation The MAC Address of the peer station in this service
   * period. \param isSource Whether we are the initiator of the service period.
   */
  void ScheduleServicePeriod(uint8_t blocks, Time spStart, Time spLength,
                             Time spPeriod, AllocationId allocationId,
                             uint8_t peerAid, Mac48Address peerAddress,
                             bool isSource);
  /**
   * Start service period (SP) allocation period.
   * \param length The length of the allocation period.
   * \param peerAid The AID of the peer station in this service period.
   * \param peerStation The MAC Address of the peer station in this service
   * period. \param isSource Whether we are the initiator of the service period.
   */
  void StartServicePeriod(AllocationId allocationId, Time length,
                          uint8_t peerAid, Mac48Address peerStation,
                          bool isSource);
  /**
   * Resume transmission for the current service period.
   */
  void ResumeServicePeriodTransmission();
  /**
   * Suspend ongoing transmission for the current service period.
   */
  void SuspendServicePeriodTransmission();
  /**
   * Add new data forwarding entry.
   * \param nextHopAddress The MAC Address of the next hop.
   */
  void AddForwardingEntry(Mac48Address nextHopAddress);
  /**
   * This function is executed upon the transmission of frame.
   * \param hdr The header of the transmitted frame.
   */
  virtual void FrameTxOk(const WigigMacHeader &hdr);
  /**
   * This method acts as the WigigMacRxMiddle receive callback and is
   * invoked to notify us that a frame has been received. The
   * implementation is intended to capture logic that is going to be
   * common to all (or most) derived classes. Specifically, handling
   * of Block Ack management frames is dealt with here.
   *
   * This method will need, however, to be overridden by derived
   * classes so that they can perform their data handling before
   * invoking the base version.
   *
   * \param mpdu the MPDU that has been received.
   */
  virtual void Receive(Ptr<WigigMacQueueItem> mpdu);
  virtual void BrpSetupCompleted(Mac48Address address) = 0;
  /**
   * The packet we sent was successfully received by the receiver
   * (i.e. we received an ACK from the receiver).  If the packet
   * was an association response to the receiver, we record that
   * the receiver is now associated with us.
   *
   * \param hdr the header of the packet that we successfully sent
   */
  virtual void TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr);
  /**
   * The packet we sent was successfully received by the receiver
   * (i.e. we did not receive an Ack from the receiver).
   *
   * \param hdr the header of the packet that we failed to sent
   */
  virtual void TxFailed(const WigigMacHeader &hdr);
  /**
   * Forward the packet up to the device.
   *
   * \param packet the packet that we are forwarding up to the device
   * \param from the address of the source
   * \param to the address of the destination
   */
  void ForwardUp(Ptr<const Packet> packet, Mac48Address from, Mac48Address to);
  /**
   * This method can be called to de-aggregate an A-MSDU and forward
   * the constituent packets up the stack.
   *
   * \param mpdu the MPDU containing the A-MSDU.
   */
  virtual void DeaggregateAmsduAndForward(Ptr<WigigMacQueueItem> mpdu);
  /**
   * This method can be called to accept a received ADDBA Request. An
   * ADDBA Response will be constructed and queued for transmission.
   *
   * \param reqHdr a pointer to the received ADDBA Request header.
   * \param originator the MAC address of the originator.
   */
  void SendAddBaResponse(const MgtAddBaRequestHeader *reqHdr,
                         Mac48Address originator);
  /**
   * Add range of MCSs to support in transmission/reception towards particular
   * station. \param address The MAC address of the peer station \param firstMcs
   * The first MCS index to support. \param lastMcs The last MCS index to
   * support.
   */
  void AddMcsSupport(Mac48Address address, uint32_t initialMcs,
                     uint32_t lastMcs);

  /**
   * Enable or disable DMG support for the device.
   *
   * \param enable whether DMG is supported
   */
  void SetDmgSupported(bool enable);
  /**
   * Return whether the device supports DMG.
   *
   * \return true if DMG is supported, false otherwise
   */
  bool GetDmgSupported() const;

  /**
   * This Boolean is set \c true if this WigigMac is to model
   * 802.11ad. It is exposed through the attribute system.
   */
  bool m_dmgSupported;

  Ptr<WigigMacRxMiddle> m_rxMiddle; //!< RX middle (defragmentation etc.)
  Ptr<MacTxMiddle> m_txMiddle;      //!< TX middle (aggregation etc.)
  Ptr<MacLow> m_low;                //!< MacLow (RTS, CTS, Data, Ack etc.)
  Ptr<WigigChannelAccessManager>
      m_channelAccessManager; //!< channel access manager
  Ptr<WigigPhy> m_phy;        //!< Wifi PHY

  Ptr<WigigRemoteStationManager>
      m_stationManager; //!< Remote station manager (rate control,
                        //!< RTS/CTS/fragmentation thresholds etc.)

  ForwardUpCallback m_forwardUp; //!< Callback to forward packet up the stack
  Callback<void> m_linkUp;       //!< Callback when a link is up
  Callback<void> m_linkDown;     //!< Callback when a link is down

  Ssid m_ssid; //!< Service Set ID (SSID)

  /** This holds a pointer to the DCF instance for this WigigMac - used
  for transmission of frames to non-QoS peers. */
  Ptr<WigigTxop> m_txop;

  /** This is a map from Access Category index to the corresponding
  channel access function */
  EdcaQueues m_edca;

  STATION_SNR_PAIR_MAP
      m_stationSnrMap; //!< Map between peer stations and their SNR Table.
  STATION_ANTENNA_CONFIG_MAP
  m_bestAntennaConfig; //!< Map between peer stations and the best antenna
                       //!< configuration.
  STATION_Awv_MAP
      m_bestAwvConfig; //!< Map between peer stations and the best AWV - to be
                       //!< used together with m_bestAntennaConfig.
  ANTENNA_CONFIGURATION
      m_feedbackAntennaConfig; //!< Temporary variable to save the best antenna
                               //!< configuration of the peer station.
  double m_maxSnr; //!< Temporary variable to save the SNR corresponding to the
                   //!< best antenna configuration of the peer station.

  /** DMG PHY Layer Information **/
  Time m_sbifs;  //!< Short Beamforming Interframe Space.
  Time m_mbifs;  //!< Medium Beamforming Interframe Space.
  Time m_lbifs;  //!< Long Beamfornnig Interframe Space.
  Time m_brpifs; //!< Beam Refinement Protocol Interframe Spacing.

  /** Channel Access Period Variables **/
  ChannelAccessPeriod
      m_accessPeriod;    //!< The type of the current channel access period.
  Time m_atiDuration;    //!< The length of the ATI period.
  Time m_biStartTime;    //!< The start time of the BI Interval.
  Time m_beaconInterval; //!< Interval between beacons.
  bool m_atiPresent;     //!< Flag to indicate if ATI period is present in the
                         //!< current BI.
  TracedCallback<Mac48Address, Time>
      m_dtiStarted; //!< Trace Callback for starting new DTI access period.

  /** BTI Access Period Variables **/
  uint8_t
      m_nextBeacon; //!< The number of BIs following the current beacon interval
                    //!< during which the DMG Beacon is not be present.
  uint8_t m_btiPeriodicity; //!< The periodicity of the BTI in BI.
  Time m_dtiDuration;       //!< The duration of the DTI access period.
  Time m_dtiStartTime;      //!< The start time of the DTI access period.

  /** A-BFT Access Period Variables **/
  Time m_abftDuration;       //!< The duration of the A-BFT access period.
  uint8_t m_ssSlotsPerAbft;  //!< Number of Sector Sweep Slots Per A-BFT.
  uint8_t m_ssFramesPerSlot; //!< Number of SSW Frames per Sector Sweep Slot.
  uint8_t m_nextAbft;        //!< The value of the next A-BFT in DMG Beacon.

  uint16_t m_totalSectors;   //!< Total number of sectors remaining to cover.
  bool m_pcpHandoverSupport; //!< Flag to indicate if we support PCP Handover.
  bool m_sectorFeedbackScheduled; //!< Flag to indicate if we schedulled SSW
                                  //!< Feedback.

  /** ATI Period Variables **/
  Ptr<DmgAtiTxop> m_dmgAtiTxop; //!< Dedicated WigigTxop for ATI.

  /** SLS Variables  **/
  Ptr<DmgSlsTxop> m_dmgSlsTxop; //!< Dedicated WigigTxop for SLS.

  /* BRP Phase Variables */
  std::map<Mac48Address, bool>
      m_isBrpSetupCompleted; //!< Map to indicate whether BRP Setup is completed
                             //!< with station.
  std::map<Mac48Address, bool> m_raisedBrpSetupCompleted;
  bool m_recordTrnSnrValues; //!< Flag to indicate if we should record reported
                             //!< SNR Values by TRN Fields.
  bool m_requestedBrpTraining; //!< Flag to indicate whether BRP Training has
                               //!< been performed.

  /* DMG Relay Variables */
  RelayDuplexMode m_relayDuplexMode; //!< The duplex mode of the relay.
  bool m_relayMode;                  //!< Flag to indicate we are in relay mode.
  bool m_redsActivated;              //!< DMG Station supports REDS.
  bool m_rdsActivated;               //!< DMG Station supports RDS.
  RelayCapableStaList
      m_rdsList; //!< List of Relay STAs (RDS) in the currernt DMG BSS.
  TracedCallback<Mac48Address>
      m_rlsCompleted; //!< Flag to indicate we are completed RLS setup.

  /* Information Request/Response Storage */
  typedef std::map<Mac48Address, DmgCapabilities> InformationMap;
  typedef InformationMap::iterator InformationMapIterator;
  typedef InformationMap::const_iterator InformationMapCI;
  InformationMap m_informationMap; //!< List of Information Elements for all the
                                   //!< associated stations.

  /* DMG Parameters */
  bool m_isCbapOnly; //!< Flag to indicate whether the DTI is allocated to CBAP.
  bool m_isCbapSource; //!< Flag to indicate that PCP/AP has higher priority for
                       //!< transmission.
  bool m_supportRdp;   //!< Flag to indicate whether we support RDP.

  /* Access Period Allocations */
  AllocationFieldList
      m_allocationList; //!< List of access period allocations in DTI.

  /* Service Period Channel Access */
  AllocationId m_currentAllocationId; //!< The ID of the current allocation.
  AllocationType m_currentAllocation; //!< The current access period allocation.
  Time m_allocationStarted;           //!< The time we initiated the allocation.
  Time m_currentAllocationLength;     //!< The length of the current allocation
                                      //!< period in MicroSeconds.
  uint8_t m_peerStationAid; //!< The AID of the peer DMG STA in the current SP.
  Mac48Address m_peerStationAddress; //!< The MAC address of the peer DMG STA in
                                     //!< the current SP.
  bool m_spSource; //!< Flag to indicate if we are the source of the SP.

  /* DMG Beamforming Variables */
  Ptr<Codebook> m_codebook;     //!< Pointer to the beamforming codebook.
  uint8_t m_bfRetryTimes;       //!< Counter for Beamforming retry times.
  EventId m_restartISSEvent;    //!< Event related to restarting ISS.
  EventId m_sswFbckTimeout;     //!< Timeout Event for receiving SSW FBCK Frame.
  EventId m_sswAckTimeoutEvent; //!< Event related to not receiving SSW ACK.
  bool m_isBeamformingInitiator; //!< Flag to indicate whether we initaited BF.
  bool m_isInitiatorTxss; //!< Flag to indicate whether the initiator is TXSS or
                          //!< RXSS.
  bool m_isResponderTxss; //!< Flag to indicate whether the responder is TXSS or
                          //!< RXSS.
  uint8_t
      m_peerSectors; //!< The total number of the sectors in the peer station.
  uint8_t
      m_peerAntennas; //!< The total number of the antennas in the peer station.
  Time m_sectorSweepStarted;  //!< Variable to store when the sector sweep phase
                              //!< started.
  Time m_sectorSweepDuration; //!< Variable to store when the duration of a
                              //!< sector sweep.
  EventId m_rssEvent;         //!< Event related to scheduling RSS.
  bool m_antennaPatternReciprocity; //!< Flag to indicate whether the STA
                                    //!< supports antenna pattern reciprocity.
  bool m_performingBft; //!< Flag to indicate whether we are performing BFT.
  bool m_useRxSectors;  //!< Flag to indicate whether to use Rx beamforming
                       //!< sectors in the station operation.

  /**
   * Trace callback for SLS phase completion.
   */
  TracedCallback<SlsCompletionAttrbitutes> m_slsCompleted;
  TracedValue<SlsInitiatorState>
      m_slsInitiatorStateMachine; //!< IEEE 802.11ad SLS Initiator state
                                  //!< machine.
  TracedValue<SlsResponderState>
      m_slsResponderStateMachine; //!< IEEE 802.11ad SLS Responder state
                                  //!< machine.
  /**
   * Trace callback for BRP phase completion.
   * \param Mac48Address The MAC address of the peer station.
   * \param BeamRefinementType The type of the beam refinement (BRP-Tx or
   * BRP-Rx). \param AntennaId The ID of the selected antenna. \param SectorId
   * The ID of the selected sector. \param AwvId The ID of the selected custom
   * AWV.
   */
  TracedCallback<Mac48Address, BeamRefinementType, AntennaId, SectorId, AwvId>
      m_brpCompleted;

  /** Link Maintenance Variabeles **/
  BeamLinkMaintenanceUnitIndex
      m_beamlinkMaintenanceUnit;      //!< Link maintenance unit according to
                                      //!< 802.11ad-2012.
  uint8_t m_beamlinkMaintenanceValue; //!< Link maintenance timer value in
                                      //!< MicroSeconds.
  Time
      dot11BeamLinkMaintenanceTime; //!< Link maintenance timer in MicroSeconds.

  /**
   * Typedef for Link Maintenance Information.
   */
  struct BeamLinkMaintenanceInfo {
    void rest() { beamLinkMaintenanceTime = negotiatedValue; }

    Time negotiatedValue;         //!< Negotiated link maintenance time value in
                                  //!< MicroSeconds.
    Time beamLinkMaintenanceTime; //!< Remaining link maintenance time value in
                                  //!< MicroSeconds.
  };

  typedef std::map<uint8_t, BeamLinkMaintenanceInfo> BeamLinkMaintenanceTable;
  typedef BeamLinkMaintenanceTable::iterator BeamLinkMaintenanceTableI;
  BeamLinkMaintenanceTable m_beamLinkMaintenanceTable;
  EventId m_beamLinkMaintenanceTimeout; //!< Event ID related to the timer
                                        //!< associated to the current
                                        //!< beamformed link.
  bool m_currentLinkMaintained; //!< Flag to indicate whether the current
                                //!< beamformed link is maintained.
  BeamLinkMaintenanceInfo m_linkMaintenanceInfo; //!< Current beamformed link
                                                 //!< maintenance information.

  /**
   * TracedCallback signature for BeamLink Maintenance Timer state change.
   *
   * \param state The state of the beam link maintenance timer.
   * \param aid The AID of the peer station.
   * \param address The MAC address of the peer station.
   * \param timeLeft The amount of remaining time in the current beam link
   * maintenance timer.
   */
  typedef void (*BeamLinkMaintenanceTimerStateChanged)(
      BeamLinkMaintenanceTimerState, uint8_t aid, Mac48Address address,
      Time timeLeft);
  TracedCallback<BeamLinkMaintenanceTimerState, uint8_t, Mac48Address, Time>
      m_beamLinkMaintenanceTimerStateChanged;

  /* Beam Refinement variables */
  typedef std::vector<double>
      TRN2SNR;       //!< Typedef for vector of SNR values for TRN Subfields.
  TRN2SNR m_trn2Snr; //!< Variable to store SNR per TRN subfield for ongoing
                     //!< beam refinement phase or beam tracking.
  Mac48Address m_peerStation; /* The address of the station we are waiting BRP
                                 Response from */
  typedef std::map<Mac48Address, uint16_t>
      BFT_ID_MAP; //!< Typedef for a MAP that holds a BFT ID (identifying the
                  //!< current BFT process) per station.
  BFT_ID_MAP m_bftIdMap;

  /* AID to MAC Address mapping */
  typedef std::map<uint16_t, Mac48Address> AID_MAP;
  typedef std::map<Mac48Address, uint16_t> MAC_MAP;
  typedef AID_MAP::const_iterator AID_MAP_CI;
  typedef MAC_MAP::const_iterator MAC_MAP_CI;
  AID_MAP m_aidMap; //!< Map AID to MAC Address.
  MAC_MAP m_macMap; //!< Map MAC Address to AID.

  /* Data Forwarding Table */
  struct AccessPeriodInformation {
    Mac48Address nextHopAddress;
    bool isCbapPeriod;
  };

  typedef std::map<Mac48Address, AccessPeriodInformation> DataForwardingTable;
  typedef DataForwardingTable::iterator DataForwardingTableIterator;
  DataForwardingTable m_dataForwardingTable;

  TracedCallback<Mac48Address, Mac48Address> m_servicePeriodStartedCallback;
  TracedCallback<Mac48Address, Mac48Address> m_servicePeriodEndedCallback;

  TracedCallback<Mac48Address, uint16_t>
      m_assocLogger; //!< Trace callback when a station associates.
  TracedCallback<Mac48Address>
      m_deAssocLogger; //!< Trace callback when a station deassociates.

private:
  /// type conversion operator
  WigigMac(const WigigMac &);
  /**
   * assignment operator
   *
   * \param mac the WigigMac to assign
   * \returns the assigned value
   */
  WigigMac &operator=(const WigigMac &mac);

  /**
   * \return the default maximum propagation delay
   *
   * By default, we get the maximum propagation delay from 1000 m and speed of
   * light (3e8 m/s).
   */
  static Time GetDefaultMaxPropagationDelay();
  /**
   * \return the default slot duration
   *
   * Return a default slot value for 802.11a (9 microseconds).
   */
  static Time GetDefaultSlot();
  /**
   * \return the default short interframe space (SIFS)
   *
   * Return a default SIFS value for 802.11a (16 microseconds).
   */
  static Time GetDefaultSifs();
  /**
   * \return the default extended interframe space (EIFS) without
   *         DCF interframe space (DIFS)
   *
   * Return default SIFS + default CTS-Ack delay
   */
  static Time GetDefaultEifsNoDifs();
  /**
   * \return the default CTS-Ack delay
   *
   * Return a default value for 802.11a at 6Mbps (44 microseconds)
   */
  static Time GetDefaultCtsAckDelay();
  /**
   * \return the default CTS and Ack timeout
   *
   * Return the default CTS and Ack timeout.
   * Cts_Timeout and Ack_Timeout are specified in the Annex C
   * (Formal description of MAC operation, see details on the
   * Trsp timer setting at page 346)
   */
  static Time GetDefaultCtsAckTimeout();
  /**
   * Return the default basic BlockAck delay.
   * Currently it returns 250 microseconds.
   *
   * \return the default basic BlockAck delay
   */
  static Time GetDefaultBasicBlockAckDelay();
  /**
   * Return the default basic BlockAck timeout.
   *
   * \return the default basic BlockAck timeout
   */
  static Time GetDefaultBasicBlockAckTimeout();
  /**
   * Return the default compressed BlockAck delay.
   * Currently it returns 76 microseconds.
   *
   * \return the default compressed BlockAck delay
   */
  static Time GetDefaultCompressedBlockAckDelay();
  /**
   * Return the default compressed BlockAck timeout.
   *
   * \return the default compressed BlockAck timeout
   */
  static Time GetDefaultCompressedBlockAckTimeout();

  /**
   * \param standard the PHY standard to be used
   *
   * This method is called by ns3::WigigMac::ConfigureStandard to complete
   * the configuration process for a requested PHY standard. Subclasses should
   * implement this method to configure their DCF queues according to the
   * requested standard.
   */
  virtual void FinishConfigureStandard(WifiStandard standard);

  /**
   * This method is a private utility invoked to configure the channel
   * access function for the specified Access Category.
   *
   * \param ac the Access Category of the queue to initialise.
   */
  void SetupEdcaQueue(AcIndex ac);

  /**
   * Set the block ack threshold for AC_VO.
   *
   * \param threshold the block ack threshold for AC_VO.
   */
  void SetVoBlockAckThreshold(uint8_t threshold);
  /**
   * Set the block ack threshold for AC_VI.
   *
   * \param threshold the block ack threshold for AC_VI.
   */
  void SetViBlockAckThreshold(uint8_t threshold);
  /**
   * Set the block ack threshold for AC_BE.
   *
   * \param threshold the block ack threshold for AC_BE.
   */
  void SetBeBlockAckThreshold(uint8_t threshold);
  /**
   * Set the block ack threshold for AC_BK.
   *
   * \param threshold the block ack threshold for AC_BK.
   */
  void SetBkBlockAckThreshold(uint8_t threshold);

  /**
   * Set VO block ack inactivity timeout.
   *
   * \param timeout the VO block ack inactivity timeout.
   */
  void SetVoBlockAckInactivityTimeout(uint16_t timeout);
  /**
   * Set VI block ack inactivity timeout.
   *
   * \param timeout the VI block ack inactivity timeout.
   */
  void SetViBlockAckInactivityTimeout(uint16_t timeout);
  /**
   * Set BE block ack inactivity timeout.
   *
   * \param timeout the BE block ack inactivity timeout.
   */
  void SetBeBlockAckInactivityTimeout(uint16_t timeout);
  /**
   * Set BK block ack inactivity timeout.
   *
   * \param timeout the BK block ack inactivity timeout.
   */
  void SetBkBlockAckInactivityTimeout(uint16_t timeout);

  /// Enable aggregation function
  void EnableAggregation();
  /// Disable aggregation function
  void DisableAggregation();

  /**
   * This function is called upon transmission of a 802.11 Management frame.
   * \param hdr The header of the management frame.
   */
  void ManagementTxOk(const WigigMacHeader &hdr);
  /**
   * Report SNR Value in SU-MIMO training, this is a callback to be hooked with
   * WigigPhy class. \param antennaID The ID of the phased antenna array. \param
   * sectorID The ID of the sector within the phased antenna array. \param
   * trnUnitsRemaining The number of remaining TRN Units. \param
   * subfieldsRemaining The number of remaining TRN Subfields within the TRN
   * Unit. \param pSubfieldsRemaining The number of remaining P subfields in the
   * TRN Unit. (Always 0 for DMG TRN Fields). \param snr The measured SNR over
   * specific TRN. \param isTxTrn Flag that indicates whether we are receiving
   * TRN-Tx or TRN-Rx/TX fields (transmit training) or TRN-Rx(no transmit
   * training).
   */
  void ReportSnrValue(AntennaId antennaID, SectorId sectorID,
                      uint8_t trnUnitsRemaining, uint8_t subfieldsRemaining,
                      uint8_t pSubfieldsRemaining, double snr, bool isTxTrn);

  Time m_maxPropagationDelay; ///< maximum propagation delay
  Ptr<NetDevice> m_device;    ///< Pointer to the device

  /**
   * The trace source fired when packets come into the "top" of the device
   * at the L3/L2 transition, before being queued for transmission.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_macTxTrace;
  /**
   * The trace source fired when packets coming into the "top" of the device
   * are dropped at the MAC layer during transmission.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_macTxDropTrace;
  /**
   * The trace source fired for packets successfully received by the device
   * immediately before being forwarded up to higher layers (at the L2/L3
   * transition).  This is a promiscuous trace.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_macPromiscRxTrace;
  /**
   * The trace source fired for packets successfully received by the device
   * immediately before being forwarded up to higher layers (at the L2/L3
   * transition).  This is a non- promiscuous trace.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_macRxTrace;
  /**
   * The trace source fired when packets coming into the "top" of the device
   * are dropped at the MAC layer during reception.
   *
   * \see class CallBackTraceSource
   */
  TracedCallback<Ptr<const Packet>> m_macRxDropTrace;

  uint16_t m_voMaxAmsduSize; ///< maximum A-MSDU size for AC_VO (in bytes)
  uint16_t m_viMaxAmsduSize; ///< maximum A-MSDU size for AC_VI (in bytes)
  uint16_t m_beMaxAmsduSize; ///< maximum A-MSDU size for AC_BE (in bytes)
  uint16_t m_bkMaxAmsduSize; ///< maximum A-MSDU size for AC_BK (in bytes)

  uint32_t m_voMaxAmpduSize; ///< maximum A-MPDU size for AC_VO (in bytes)
  uint32_t m_viMaxAmpduSize; ///< maximum A-MPDU size for AC_VI (in bytes)
  uint32_t m_beMaxAmpduSize; ///< maximum A-MPDU size for AC_BE (in bytes)
  uint32_t m_bkMaxAmpduSize; ///< maximum A-MPDU size for AC_BK (in bytes)

  TracedCallback<const WigigMacHeader &>
      m_txOkCallback; ///< transmit OK callback
  TracedCallback<const WigigMacHeader &>
      m_txErrCallback; ///< transmit error callback

  TypeOfStation m_type; ///< type of the station
};

} // namespace ns3

#endif /* WIGIG_MAC_H */
