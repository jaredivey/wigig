/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef AP_WIGIG_MAC_H
#define AP_WIGIG_MAC_H

#include "wigig-mac.h"

namespace ns3 {

class RandomVariableStream;
class DmgBeaconTxop;

#define TU MicroSeconds(1024)    /* Time Unit defined in 802.11 std */
#define aMaxBIDuration TU * 1024 /* Maximum BI Duration Defined in 802.11ad */
#define aMinChannelTime aMaxBIDuration /* Minimum Channel Time for Clustering  \
                                        */
#define aMinSSSlotsPerAbft                                                     \
  1                        /* Minimum Number of Sector Sweep Slots Per A-BFT */
#define aSSFramesPerSlot 8 /* Number of SSW Frames per Sector Sweep Slot */
#define aDmgPPMinListeningTime                                                 \
  150 /* The minimum time between two adjacent SPs with the same source or     \
         destination AIDs */

/**
 * \brief Wi-Fi DMG AP state machine
 * \ingroup wigig
 *
 * Handle association, dis-association and authentication,
 * of DMG STAs within an infrastructure DMG BSS.
 */
class ApWigigMac : public WigigMac {
public:
  static TypeId GetTypeId();

  ApWigigMac();
  ~ApWigigMac() override;

  uint16_t GetAssociationId() override;
  /**
   * \param stationManager the station manager attached to this MAC.
   */
  void SetWifiRemoteStationManager(
      Ptr<WigigRemoteStationManager> stationManager) override;

  /**
   * \param linkUp the callback to invoke when the link becomes up.
   */
  void SetLinkUpCallback(Callback<void> linkUp) override;
  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   *
   * The packet should be enqueued in a tx queue, and should be
   * dequeued as soon as the channel access function determines that
   * access is granted to this MAC.
   */
  void Enqueue(Ptr<Packet> packet, Mac48Address to) override;
  /**
   * \param packet the packet to send.
   * \param to the address to which the packet should be sent.
   * \param from the address from which the packet should be sent.
   *
   * The packet should be enqueued in a tx queue, and should be
   * dequeued as soon as the channel access function determines that
   * access is granted to this MAC. The extra parameter "from" allows
   * this device to operate in a bridged mode, forwarding received
   * frames without altering the source address.
   */
  void Enqueue(Ptr<Packet> packet, Mac48Address to, Mac48Address from) override;
  bool SupportsSendFrom() const override;
  /**
   * \param address the current address of this MAC layer.
   */
  void SetAddress(Mac48Address address) override;
  /**
   * \param interval the interval between two beacon transmissions.
   */
  void SetBeaconInterval(Time interval);
  /**
   * \return the interval between two beacon transmissions.
   */
  Time GetBeaconInterval() const;
  /**
   * Get Data Transmission Interval Duration
   * \return The duration of Dti.
   */
  Time GetDtiDuration() const;
  /**
   * Get the amount of the remaining time in the Dti access period.
   * \return The remaining time in the Dti.
   */
  Time GetDtiRemainingTime() const;
  /**
   * \param interval the interval.
   */
  void SetBeaconTransmissionInterval(Time interval);
  /**
   * \return the interval between two beacon transmissions.
   */
  Time GetBeaconTransmissionInterval() const;
  /**
   * \param periodicity the A-BFT periodicity.
   */
  void SetAbftPeriodicity(uint8_t periodicity);
  /**
   * \return the A-BFT periodicity.
   */
  uint8_t GetAbftPeriodicity() const;
  /**
   * Add a new allocation with one single block. The duration of the block is
   * limited to 32 767 microseconds for a SP allocation. and to 65 535
   * microseconds for a CBAP allocation. The allocation is announced in the
   * following DMG Beacon or Announce Frame. \param allocationId A unique
   * identifier for the allocation. \param allocationType The type of the
   * allocation (CBAP or SP). \param staticAllocation Is the allocation static.
   * \param srcAid The AID of the source DMG STA. \param dstAid The AID of the
   * destination DMG STA. \param allocationStart The start time of the
   * allocation relative to the beginning of Dti. \param blockDuration The
   * duration of the allocation period. \return The start of the next allocation
   * period.
   */
  uint32_t AllocateSingleContiguousBlock(AllocationId allocationId,
                                         AllocationType allocationType,
                                         bool staticAllocation, uint8_t srcAid,
                                         uint8_t dstAid,
                                         uint32_t allocationStart,
                                         uint16_t blockDuration);

  /**
   * Allocate maximum part of Dti as a service period channel access.
   * \param id A unique identifier for the allocation.
   * \param srcAid The AID of the source DMG STA.
   * \param dstAid The AID of the destination DMG STA.
   */
  void AllocateDtiAsServicePeriod(AllocationId id, uint8_t srcAid,
                                  uint8_t dstAid);
  /**
   * Add a new allocation period to be announced in DMG Beacon or Announce
   * Frame. \param allocationId A unique identifier for the allocation. \param
   * allocationType The type of the allocation (CBAP or SP). \param
   * staticAllocation Is the allocation static. \param srcAid The AID of the
   * source DMG STA. \param dstAid The AID of the destination DMG STA. \param
   * allocationStart The start time of the allocation relative to the beginning
   * of Dti. \param blockDuration The duration of the allocation period. \param
   * blocks The number of blocks making up the allocation. \return The start
   * time of the following allocation period.
   */
  uint32_t AddAllocationPeriod(AllocationId allocationId,
                               AllocationType allocationType,
                               bool staticAllocation, uint8_t srcAid,
                               uint8_t dstAid, uint32_t allocationStart,
                               uint16_t blockDuration, uint16_t blockPeriod,
                               uint8_t blocks);
  /**
   * Allocate SP allocation for Beamforming training.
   * \param srcAid The AID of the source DMG STA.
   * \param dstAid The AID of the destination DMG STA.
   * \param allocationStart The start time of the allocation relative to the
   * beginning of Dti. \param isTXSS Is the Beamforming TXSS or RXSS for both
   * the initiator and the responder. \return The start of the next allocation
   * period.
   */
  uint32_t AllocateBeamformingServicePeriod(uint8_t srcAid, uint8_t dstAid,
                                            uint32_t allocationStart,
                                            bool isTXSS);
  /**
   * Allocate SP allocation for Beamforming training.
   * \param srcAid The AID of the source DMG STA.
   * \param dstAid The AID of the destination DMG STA.
   * \param allocationStart The start time of the allocation relative to the
   * beginning of Dti. \param allocationDuration The duration of the beamforming
   * allocation. \param isInitiatorTXSS Is the Initiator Beamforming TXSS or
   * RXSS. \param isResponderTxss Is the Responder Beamforming TXSS or RXSS.
   * \return The start of the next allocation period.
   */
  uint32_t AllocateBeamformingServicePeriod(uint8_t sourceAid, uint8_t destAid,
                                            uint32_t allocationStart,
                                            uint16_t allocationDuration,
                                            bool isInitiatorTXSS,
                                            bool isResponderTxss);

  /**
   * Get associated station AID from its MAC address.
   * \param address The MAC address of the associated station.
   * \return The AID of the associated station.
   */
  uint8_t GetStationAid(Mac48Address address) const;
  /**
   * Get associated station MAC address from AID.
   * \param aid The AID of the associated station.
   * \return The MAC address of the associated station.
   */
  Mac48Address GetStationAddress(uint8_t aid) const;
  /**
   * Get Allocation List
   * \return
   */
  AllocationFieldList GetAllocationList() const;
  /**
   * Send DMG Add TS Response to DMG STA.
   * \param to The MAC address of the DMG STA which sent the DMG ADDTS Request.
   * \param code The status of the allocation.
   * \param delayElem The TS Delay element.
   * \param elem The TSPEC element.
   */
  void SendDmgAddTsResponse(Mac48Address to, StatusCode code,
                            TsDelayElement &delayElem, DmgTspecElement &elem);

protected:
  Time GetBtiRemainingTime() const;
  /**
   * Start monitoring Beacon SP for DMG Beacons.
   */
  void StartMonitoringBeaconSP(uint8_t beaconSPIndex);
  /**
   * End monitoring Beacon SP for DMG Beacons.
   * \param beaconSPIndex The index of the Beacon SP.
   */
  void EndMonitoringBeaconSP(uint8_t beaconSPIndex);
  /**
   * End channel monitoring for DMG Beacons during Beacon SPs.
   * \param clusterID The MAC address of the cluster.
   */
  void EndChannelMonitoring(Mac48Address clusterID);
  /**
   * Start Syn Beacon Interval.
   */
  void StartSynBeaconInterval();
  /**
   * Return the DMG capability of the current PCP/AP.
   * \return the DMG capabilities the PCP/AP supports.
   */
  DmgCapabilities GetDmgCapabilities() const override;

private:
  void DoDispose() override;
  void DoInitialize() override;

  void StartBeaconInterval() override;
  void EndBeaconInterval() override;
  void StartBeaconTransmissionInterval() override;
  void StartAssociationBeamformTraining() override;
  void StartAnnouncementTransmissionInterval() override;
  void StartDataTransmissionInterval() override;
  void FrameTxOk(const WigigMacHeader &hdr) override;
  void BrpSetupCompleted(Mac48Address address) override;
  void NotifyBrpPhaseCompleted() override;
  void Receive(Ptr<WigigMacQueueItem> mpdu) override;

  /**
   * Start Beacon Header Interval (BHI).
   */
  void StartBeaconHeaderInterval();
  /**
   * The packet we sent was successfully received by the receiver
   * (i.e. we received an ACK from the receiver). If the packet
   * was an association response to the receiver, we record that
   * the receiver is now associated with us.
   *
   * \param hdr the header of the packet that we successfully sent
   */
  void TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) override;
  /**
   * The packet we sent was not successfully received by the receiver
   * (i.e. we did not receive an ACK from the receiver). If the packet
   * was an association response to the receiver, we record that
   * the receiver is not associated with us yet.
   *
   * \param hdr the header of the packet that we failed to sent
   */
  void TxFailed(const WigigMacHeader &hdr) override;
  /**
   * This method can be called to de-aggregate an A-MSDU and forward
   * the constituent packets up the stack.
   *
   * \param mpdu the MPDU containing the A-MSDU.
   */
  void DeaggregateAmsduAndForward(Ptr<WigigMacQueueItem> mpdu) override;
  /**
   * Start A-BFT Sector Sweep Slot.
   */
  void StartSectorSweepSlot();
  /**
   * Establish BRP Setup Subphase
   */
  void DoBrpSetupSubphase();
  /**
   * Forward the packet down to DCF/EDCAF (enqueue the packet). This method
   * is a wrapper for ForwardDown with traffic id.
   *
   * \param packet the packet we are forwarding to DCF/EDCAF
   * \param from the address to be used for Address 3 field in the header
   * \param to the address to be used for Address 1 field in the header
   */
  void ForwardDown(Ptr<Packet> packet, Mac48Address from, Mac48Address to);
  /**
   * Forward the packet down to DCF/EDCAF (enqueue the packet).
   *
   * \param packet the packet we are forwarding to DCF/EDCAF
   * \param from the address to be used for Address 3 field in the header
   * \param to the address to be used for Address 1 field in the header
   * \param tid the traffic id for the packet
   */
  void ForwardDown(Ptr<Packet> packet, Mac48Address from, Mac48Address to,
                   uint8_t tid);
  /**
   * Forward a probe response packet to the DCF. The standard is not clear on
   * the correct queue for management frames if QoS is supported. We always use
   * the DCF.
   *
   * \param to the MAC address of the STA we are sending a probe response to.
   */
  void SendProbeResp(Mac48Address to);
  /**
   * Forward an association response packet to the DCF. The standard is not
   * clear on the correct queue for management frames if QoS is supported. We
   * always use the DCF.
   *
   * \param to the MAC address of the STA we are sending an association response
   * to. \param success indicates whether the association was successful or not.
   * \return If success return the AID of the station, otherwise 0.
   */
  uint16_t SendAssocResp(Mac48Address to, bool success);
  /**
   * Get DMG Operation element.
   * \return Pointer to the DMG Operation element.
   */
  DmgOperationElement GetDmgOperationElement() const;
  /**
   * Get Next DMG ATI Information element.
   * \return The DMG ATI information element.
   */
  NextDmgAti GetNextDmgAtiElement() const;
  /**
   * Get Extended Schedule element.
   * \return The extended schedule element.
   */
  ExtendedScheduleElement GetExtendedScheduleElement() const;
  /**
   * Cleanup non-static allocations. This is method is called after the
   * transmission of the last DMG Beacon.
   */
  void CleanupAllocations();
  /**
   * Calculate Bti access period variables.
   */
  void CalculateBtiVariables();
  /**
   * Send One DMG Beacon frame with the provided arguments.
   */
  void SendOneDMGBeacon();
  /**
   * Get Beacon Header Interval Duration
   * \return The duration of BHI.
   */
  Time GetBhiDuration() const;
  /**
   * \return the next Association ID to be allocated by the DMG PCP/AP.
   */
  uint16_t GetNextAssociationId();

  /** Association Information **/
  std::map<uint16_t, Mac48Address>
      m_staList; //!< Map of all stations currently associated to the
                 //!< DMG PCP/AP with their association ID.

  /** Bti Period Variables **/
  Ptr<DmgBeaconTxop> m_beaconTxop; //!< Dedicated Txop for DMG Beacons.
  EventId m_beaconEvent;           //!< Event to generate one DMG Beacon.
  Time m_btiStarted; //!< The time at which we started Bti access period.
  Time m_dmgBeaconDurationUs; //!< DMG BEacon Duration in Microseconds.
  Time m_nextDmgBeaconDelay;  //!< DMG Beacon transmission delay due to
                             //!< difference in clock + duration of TRN fields
                             //!< if there are any.
  Time m_btiDuration; //!< The length of the Beacon Transmission Interval (Bti).
  bool m_beaconRandomization; //!< Flag to indicate whether we want to randomize
                              //!< selection of DMG Beacon at each BI.
  Ptr<RandomVariableStream>
      m_beaconJitter; //!< RandomVariableStream used to randomize the time
                      //!< of the first DMG beacon.
  bool m_enableBeaconJitter; //!< Flag whether the first beacon should be
                             //!< generated at random time.
  bool m_allowBeaconing;     //!< Flag to indicate whether we want to start
                         //!< Beaconing upon initialization.
  bool m_announceDmgCapabilities; //!< Flag to indicate whether we announce DMG
                                  //!< Capabilities in DMG Beacons.
  bool m_announceOperationElement; //!< Flag to indicate whether we transmit DMG
                                   //!< operation element in DMG Beacons.
  bool m_scheduleElement; //!< Flag to indicate whether we transmit Extended
                          //!< Schedule element in DMG Beacons.
  bool m_isABftResponderTxss; //!< Flag to indicate whether the responder in
                              //!< A-BFT is TXSS or RXSS.
  bool
      m_announceTrainingSchedule; //!< Flag to indicate whether we transmit the
                                  //!< schedule for TRN-R fields in DMG Beacons.
  std::vector<Mac48Address>
      m_beamformingInDti; //!< List of the stations to train in Dti because
                          //!< beamforming is not completed in Bti.
  bool m_firstBeacon; //!< Flag to identify the first DMG Beacon that we send.

  /** DMG PCP/AP Clustering **/
  bool m_enableDecentralizedClustering; //!< Flag to indicate if decentralized
                                        //!< clustering is enabled.
  bool m_enableCentralizedClustering; //!< Flag to indicate if centralized
                                      //!< clustering is enabled.
  Mac48Address m_ClusterId;           //!< The ID of the cluster.
  uint8_t m_clusterMaxMem;    //!< The maximum number of cluster members.
  uint8_t m_beaconSpDuration; //!< The size of a Beacon SP in MicroSeconds.
  ClusterMemberRole
      m_clusterRole; //!< The role of the node in the current cluster.
  std::map<uint8_t, bool>
      m_spStatus; //!< The status of each Beacon SP in the monitor period.
  bool m_monitoringChannel; //!< Flag to indicate if we have started monitoring
                            //!< the channel for cluster formation.
  bool m_beaconReceived; //!< Flag to indicate if we have received beacon during
                         //!< BeaconSP.
  uint8_t m_selectedBeaconSp; //!< Selected Beacon SP for DMG Transmission.
  Time m_clusterTimeInterval; //!< The interval between two consecutive Beacon
                              //!< SPs.
  Time m_channelMonitorTime;  //!< The channel monitor time.
  Time m_startedMonitoringChannel; //!< The time we started monitoring channel
                                   //!< for DMG Beacons.
  Time m_clusterBeaconSpDuration;  //!< The duration of the Beacon SP.

  TracedCallback<Mac48Address, uint8_t>
      m_joinedCluster; //!< The PCP/AP has joined a cluster.

  /** A-BFT Access Period Variables **/
  uint8_t m_abftPeriodicity; //!< The periodicity of the A-BFT in DMG Beacon.
  EventId m_sswFbckEvent;    //!< Event related to sending SSW FBCK.
  /* Ensure only one DMG STA is communicating with us during single A-BFT slot
   */
  bool m_receivedOneSsw; //!< Flag to indicate if we received SSW Frame during
                         //!< SSW-Slot in A-BFT period.
  bool m_abftCollision; //!< Flad to indicate if we experienced any collision in
                        //!< the current A-BFT slot.
  Mac48Address m_peerAbftStation; //!< The MAC address of the station we
                                  //!< received SSW from.
  uint8_t m_remainingSlots;

  /** BRP Phase Variables **/
  std::map<Mac48Address, bool>
      m_stationBrpMap; //!< Map to indicate if a station has conducted BRP Phase
                       //!< or not.

  /** Beacon Interval **/
  TracedCallback<Mac48Address>
      m_biStarted; //!< Trace Callback for starting new Beacon Interval.
  TracedCallback<Mac48Address, Time>
      m_btiStart; //!< Trace Callback for starting A-BFT within the BI.

  /**
   * TracedCallback signature for Bti access period start event.
   *
   * \param address The MAC address of the station.
   * \param duration The duration of the Bti period.
   */
  typedef void (*BtiStartedCallback)(Mac48Address address, Time duration);
  TracedCallback<Mac48Address, Time>
      m_abftStarted; //!< Trace Callback for starting A-BFT within the BI.
  /**
   * TracedCallback signature for A-BFT access period start event.
   *
   * \param address The MAC address of the station.
   * \param duration The duration of the A-BFT period.
   */
  typedef void (*AbftStartedCallback)(Mac48Address address, Time duration);

  /** Traffic Stream Allocation **/
  TracedCallback<Mac48Address, DmgTspecElement>
      m_addTsRequestReceived; //!< DMG ADDTS Request received.
};

} // namespace ns3

#endif /* AP_WIGIG_MAC_H */
