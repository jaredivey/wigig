/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "adhoc-wigig-mac.h"

#include "ext-headers.h"
#include "mac-low.h"
#include "wigig-mac-header.h"
#include "wigig-msdu-aggregator.h"

#include "ns3/dmg-capabilities.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("AdhocWigigMac");

NS_OBJECT_ENSURE_REGISTERED(AdhocWigigMac);

TypeId AdhocWigigMac::GetTypeId() {
  static TypeId tid = TypeId("ns3::AdhocWigigMac")
                          .SetParent<WigigMac>()
                          .AddConstructor<AdhocWigigMac>();
  return tid;
}

AdhocWigigMac::AdhocWigigMac() {
  NS_LOG_FUNCTION(this);
  SetTypeOfStation(ADHOC_STA);
}

AdhocWigigMac::~AdhocWigigMac() { NS_LOG_FUNCTION(this); }

void AdhocWigigMac::DoInitialize() {
  NS_LOG_FUNCTION(this);
  for (EdcaQueues::const_iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->SetAllocationType(CBAP_ALLOCATION);
  }
  /* Initialize Codebook */
  m_codebook->Initialize();
}

uint16_t AdhocWigigMac::GetAssociationId() { return AID_AP; }

void AdhocWigigMac::SetAddress(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  WigigMac::SetAddress(address);
  WigigMac::SetBssid(address);
}

void AdhocWigigMac::Enqueue(Ptr<Packet> packet, Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << to);
  if (m_stationManager->IsBrandNew(to)) {
    m_stationManager->AddAllSupportedModes(to);
    m_stationManager->AddStationDmgCapabilities(to, GetDmgCapabilities());
    m_stationManager->RecordDisassociated(to);
  }

  WigigMacHeader hdr;

  // If we are not a QoS STA then we definitely want to use AC_BE to
  // transmit the packet. A TID of zero will map to AC_BE (through \c
  // QosUtilsMapTidToAc()), so we use that as our default here.
  uint8_t tid = 0;
  hdr.SetAsDmgPpdu();
  hdr.SetType(WIFI_MAC_QOSDATA);
  hdr.SetQosAckPolicy(WigigMacHeader::NORMAL_ACK);
  hdr.SetQosNoEosp();
  hdr.SetQosNoAmsdu();
  // Transmission of multiple frames in the same TXOP is not
  // supported for now
  hdr.SetQosTxopLimit(0);

  // Fill in the QoS control field in the MAC header
  tid = QosUtilsGetTidForPacket(packet);
  // Any value greater than 7 is invalid and likely indicates that
  // the packet had no QoS tag, so we revert to zero, which will
  // mean that AC_BE is used.
  if (tid > 7) {
    tid = 0;
  }
  hdr.SetQosTid(tid);
  /* DMG QoS Control*/
  hdr.SetQosRdGrant(m_supportRdp);
  /* The HT Control field is not present in frames transmitted by a DMG STA.
   * The presence of the HT Control field is determined by the Order
   * subfield of the Frame Control field, as specified in 8.2.4.1.10.*/
  hdr.SetNoOrder();

  /* We are in DMG Ad-Hoc Mode (Experimental Mode) */
  hdr.SetAddr1(to);
  hdr.SetAddr2(m_low->GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();

  m_edca[QosUtilsMapTidToAc(tid)]->Queue(packet, hdr);
}

void AdhocWigigMac::SetLinkUpCallback(Callback<void> linkUp) {
  NS_LOG_FUNCTION(this << &linkUp);
  WigigMac::SetLinkUpCallback(linkUp);

  // The approach taken here is that, from the point of view of a STA
  // in IBSS mode, the link is always up, so we immediately invoke the
  // callback if one is set
  linkUp();
}

void AdhocWigigMac::StartBeaconInterval() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::EndBeaconInterval() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::StartBeaconTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::StartAssociationBeamformTraining() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::StartAnnouncementTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::StartDataTransmissionInterval() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::FrameTxOk(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
}

void AdhocWigigMac::TxOk(Ptr<const Packet> packet, const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  /* After transmission we stay in quasi-omni mode since we do not know which
   * station will transmit to us */
  m_codebook->SetReceivingInQuasiOmniMode();
  WigigMac::TxOk(packet, hdr);
}

void AdhocWigigMac::BrpSetupCompleted(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::NotifyBrpPhaseCompleted() {
  NS_LOG_FUNCTION(this);
  NS_FATAL_ERROR("This method should not be called in AdhocWigigMac class");
}

void AdhocWigigMac::AddAntennaConfig(SectorId txSectorId, AntennaId txAntennaId,
                                     SectorId rxSectorId, AntennaId rxAntennaId,
                                     Mac48Address address) {
  ANTENNA_CONFIGURATION_TX antennaConfigTx =
      std::make_pair(txAntennaId, txSectorId);
  ANTENNA_CONFIGURATION_RX antennaConfigRx =
      std::make_pair(rxAntennaId, rxSectorId);
  m_bestAntennaConfig[address] =
      std::make_tuple(antennaConfigTx, antennaConfigRx, 0.0);
  m_codebook->SetReceivingInDirectionalMode();
}

void AdhocWigigMac::AddAntennaConfig(SectorId txSectorId, AntennaId txAntennaId,
                                     Mac48Address address) {
  ANTENNA_CONFIGURATION_TX antennaConfigTx =
      std::make_pair(txAntennaId, txSectorId);
  ANTENNA_CONFIGURATION_RX antennaConfigRx =
      std::make_pair(NO_ANTENNA_CONFIG, NO_ANTENNA_CONFIG);
  m_bestAntennaConfig[address] =
      std::make_tuple(antennaConfigTx, antennaConfigRx, 0.0);
  m_codebook->SetReceivingInQuasiOmniMode();
}

void AdhocWigigMac::Receive(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);

  const WigigMacHeader *hdr = &mpdu->GetHeader();
  NS_ASSERT(!hdr->IsCtl());
  Mac48Address from = hdr->GetAddr2();
  Mac48Address to = hdr->GetAddr1();

  if (m_stationManager->IsBrandNew(to)) {
    // In ad hoc mode, we assume that every destination supports all
    // the rates we support.
    m_stationManager->AddAllSupportedModes(to);
    m_stationManager->AddStationDmgCapabilities(to, GetDmgCapabilities());
    m_stationManager->RecordDisassociated(to);
  }
  if (hdr->IsData()) {
    if (hdr->IsQosData() && hdr->IsQosAmsdu()) {
      NS_LOG_DEBUG("Received A-MSDU from" << from);
      DeaggregateAmsduAndForward(mpdu);
    } else {
      ForwardUp(mpdu->GetPacket()->Copy(), from, to);
    }
    return;
  }

  WigigMac::Receive(mpdu);
}

DmgCapabilities AdhocWigigMac::GetDmgCapabilities() const {
  DmgCapabilities capabilities;
  return capabilities;
}

} // namespace ns3
