/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef CODEBOOK_H
#define CODEBOOK_H

#include "rf-chain.h"
#include "wigig-data-types.h"

#include "ns3/antenna-model.h"
#include "ns3/mac48-address.h"
#include "ns3/object.h"

#include <cmath>
#include <map>

namespace ns3 {

#define MAXIMUM_NUMBER_OF_ANTENNAS                                             \
  4 //!< The maximum number of antennas is limited to 4 in 802.11ad.
#define MAXIMUM_SECTORS_PER_ANTENNA                                            \
  64 //!< The maximum number of sectors per antenna is limited to 64 sectors.
#define MAXIMUM_NUMBER_OF_SECTORS                                              \
  128 //!< The maximum total number of sectors is limited to 1028 sectors.
#define AZIMUTH_CARDINALITY                                                    \
  361 //!< Number of the azimuth angles (1 Degree granularity from 0 -> 360).
#define ELEVATION_CARDINALITY                                                  \
  181 //!< Number of the elevation angles (1 Degree granularity from -90 -> 90).

class WigigNetDevice;

/**
 * \brief Codebook for defining phased antenna arrays configurations in IEEE
 * 802.11ad/ay devices.
 */
class Codebook : public Object {
public:
  static TypeId GetTypeId();

  Codebook();
  ~Codebook() override;

  typedef std::map<Mac48Address, Antenna2SectorList>
      BeamformingSectorList; //!< Typedef for beamforming sector list (Custom
                             //!< sector set for specific station).

  enum CurrentBeamformingPhase {
    BHI_PHASE = 0,
    SLS_PHASE = 1
  }; //!< Current beamforming Phase in the Beacon Interval.

  /**
   * Load Codebook from a text file.
   */
  virtual void LoadCodebook(std::string filename) = 0;
  /**
   * Get Codebook FileName
   * \return The current loaded codebook.
   */
  std::string GetCodebookFileName() const;

  /**
   * Get transmit antenna array gain in dBi.
   * \param angle The angle towards the intended receiver.
   * \return Transmit antenna array gain in dBi based on the steering angle.
   */
  virtual double GetTxGainDbi(double angle) = 0;
  /**
   * Get receive antenna array array gain in dBi.
   * \param angle The angle towards the intended transmitter.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  virtual double GetRxGainDbi(double angle) = 0;
  /**
   * Get transmit antenna array gain in dBi.
   * \param azimuth The azimuth angle towards the intended receiver.
   * \param elevation The elevation angle towards the intended receiver.
   * \return Transmit antenna gain in dBi based on the steering angle.
   */
  virtual double GetTxGainDbi(double azimuth, double elevation) = 0;
  /**
   * Get receive antenna array gain in dBi.
   * \param azimuth The azimuth angle towards the intended receiver.
   * \param elevation The elevation angle towards the intended receiver.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  virtual double GetRxGainDbi(double azimuth, double elevation) = 0;
  /**
   * Get the total number of transmit sectors in the codebook.
   * \return The total number of transmit sectors in the codebook.
   */
  uint8_t GetTotalNumberOfTransmitSectors() const;
  /**
   * Get the total number of receive sectors in the codebook.
   * \return The total number of receive sectors in the codebook.
   */
  uint8_t GetTotalNumberOfReceiveSectors() const;
  /**
   * Get the total number of sectors in the codebook.
   * \return The total number of sectors in the codebook.
   */
  uint8_t GetTotalNumberOfSectors() const;
  /**
   * Get the total number of sectors for a specific phased antenna array.
   * \param antennaID The ID of the phased antenna array.
   * \return The number of sectors for the specified phased antenna array.
   */
  virtual uint8_t GetNumberSectorsPerAntenna(AntennaId antennaID) const = 0;
  /**
   * Get the total number of antennas in the codebook.
   * \return The total number of antennas in the codebook.
   */
  uint8_t GetTotalNumberOfAntennas() const;

  /**
   * Get the number of sectors utilized during the current beacon header
   * interval (BHI). \return The number of sectors utilized for Beaconing during
   * the current beacon header interval (BHI).
   */
  uint8_t GetNumberOfSectorsInBHI();

  /**
   * Append new sector transmit/receive to the list of SLS sectors.
   * \param address the MAC address of the peer station to have custom
   * beamforming sectors with. \param type The type of sector sweep (Tx/Rx).
   * \param antennaID The ID of the Antenna.
   * \param sectorID The ID of the sector.
   */
  void AppendBeamformingSector(Mac48Address address, SectorSweepType type,
                               AntennaId antennaID, SectorId sectorID);
  /**
   * Get the number of sectors utilized during beamforming training.
   * \param type The type of sector sweep.
   * \return the number of sectors utilized during beamforming training.
   */
  uint8_t GetNumberOfSectorsForBeamforming(SectorSweepType type);
  /**
   * Append new sector transmit/receive to the general list of SLS sectors.
   * \param type The type of sector sweep (Tx/Rx).
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector.
   */
  void AppendBeamformingSector(SectorSweepType type, AntennaId antennaID,
                               SectorId sectorID);
  /**
   * Set the list of transmit/receive sectors utilized during beamforming
   * training (SLS Phase) with specific station. \param type The type of sector
   * sweep. \param address The MAC address of the peer station. \param sectors
   * the list of sectors utilized during an SLS Phase.
   */
  void SetBeamformingSectorList(SectorSweepType type, Mac48Address address,
                                Antenna2SectorList &sectorList);
  /**
   * Remove sector from the list of Beaconing sectors.
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector.
   */
  void RemoveBeamformingSector(AntennaId antennaID, SectorId sectorID);
  /**
   * Get the number of sectors utilized during beamforming training.
   * \param address the MAC address of the peer station.
   * \param type The type of sector sweep.
   * \return the number of sectors utilized during beamforming training.
   */
  uint8_t GetNumberOfSectorsForBeamforming(Mac48Address address,
                                           SectorSweepType type);
  /**
   * Copy the content of an existing codebook to the current codebook.
   * \param codebook A pointer to the codebook that we want to copy its content.
   */
  virtual void CopyCodebook(const Ptr<Codebook> codebook);
  /**
   * Change phased antenna array orientation using Euler transformation.
   * \param antennaID The ID of the antenna array.
   * \param psi The rotation around the z-axis in degrees.
   * \param theta The rotation around the y-axis in degrees.
   * \param phi The rotation around the x-axis in degrees.
   */
  void ChangeAntennaOrientation(AntennaId antennaID, double psi, double theta,
                                double phi);

  /**
   * Get the 3d orientation of an antenna array.
   * \param antennaID The ID of the phased antenna array.
   * \return The antenna 3D orientation vector.
   */
  Orientation GetOrientation(AntennaId antennaId);
  /**
   * Set the WigigNetDevice we are associated with.
   * \param device A pointer to the WigigNetDevice we are associated with.
   */
  void SetDevice(Ptr<WigigNetDevice> device);
  /**
   * Select the quasi-omni receive mode for the current phased antenna array.
   */
  void SetReceivingInQuasiOmniMode();
  /**
   * Begin receive training using TRN-R fields appended to a beacon.
   */
  void StartBeaconTraining();
  /**
   * Set specific antenna pattern to quasi-omni mode when we have multiple
   * antennas but only one of them is active at a time. \param antennaID The ID
   * of the phased antenna array.
   */
  void SetReceivingInDirectionalMode();
  /**
   * Get next sector in BTI access period.
   * \return True, if we still have sectors to cover otherwise false.
   */
  bool GetNextSectorInBti();
  /**
   * Get number of remaining sector in BTI or SLS.
   * \return The total number of remaining sectors in BTI or SLS.
   */
  uint16_t GetRemainingSectorCount() const;
  /**
   * Get the ID of the current active Tx Sector.
   * \return the ID of the current active Tx Sector.
   */
  SectorId GetActiveTxSectorId() const;
  /**
   * Get the ID of the current active Rx Sector.
   * \return the ID of the current active Rx Sector.
   */
  SectorId GetActiveRxSectorId() const;
  /**
   * Get the ID of the current active Tx Antenna.
   * \return the ID of the current active Tx Antenna.
   */
  AntennaId GetActiveAntennaId() const;
  /**
   * Set the ID of the active AWV within the current transmit sector.
   * \param awvID The ID of the AWV within the transmit sector.
   */
  void SetActiveTxAwvId(AwvId awvID);
  /**
   * Set the ID of the active AWV ID within the current receive sector.
   * \param awvID The ID of the AWV within the receive sector.
   */
  void SetActiveRxAwvId(AwvId awvID);
  /**
   * Return the number of AWVs used by the current active Tx/Rx sector.
   * \return The total number of AWVs used by the current active Tx/Rx sector.
   */
  uint8_t GetNumberOfAwvs(AntennaId antennaID, SectorId sectorID) const;
  /**
   * Get the next AWV in the AWV list of the selected transmit/receive sector
   * for all the active RF chains.
   */
  void GetNextAwv();
  /**
   * Set the ID of the active transmit sector.
   * \param antennaID The ID of the phased antenna array.
   * \param sectorID The ID of the new sector wthin the antenna array.
   */
  void SetActiveTxSectorId(AntennaId antennaID, SectorId sectorID);
  /**
   * Set the ID of the active receive sector.
   * \param antennaID The ID of the phased antenna array.
   * \param sectorID The ID of the new sector wthin the antenna array.
   */
  void SetActiveRxSectorId(AntennaId antennaID, SectorId sectorID);
  /**
   * Get the total number of beacon intervals it takes the PCP/AP to cover all
   * sectors in all DMG antennas. \return The total number of beacon intervals.
   */
  uint8_t GetNumberOfBIs() const;
  /**
   * Get the next Sector ID in the Beamforming Sector List.
   * \param changeAntenna True if we are changing the antenna otherwise false.
   * \return True if the antenna has been switched.
   */
  bool GetNextSector(bool &changeAntenna);
  /**
   * Start Sector Sweeping phase. Calling this function lets the code book
   * chooses from the list of sectors to be used in the SLS phase. \param
   * address the MAC address of the peer station. \param type Sector Sweep Type
   * (Transmit/Receive).
   */
  void StartSectorSweeping(Mac48Address address, SectorSweepType type,
                           uint8_t peerAntennas);
  /**
   * Initiate BTI access period. Calling this function lets the codebook chooses
   * from the list of sectors to be used in BTI.
   */
  void StartBtiAccessPeriod();
  /**
   * Randomize beacon transmission in BTI access period.
   * \param beaconRandomization True if we want to randmoize beacon transmission
   * otherwise false.
   */
  void RandomizeBeacon(bool beaconRandomization);
  /**
   * Start receiving in Quasi-omni Mode using the antenna array with the lowest
   * ID.
   */
  void StartReceivingInQuasiOmniMode();
  /**
   * Switch to the next quasi-omni mode.
   * \return True, if switch has been done, false otherwise.
   */
  bool SwitchToNextQuasiPattern();
  /**
   * Initiate A-BFT access period as TXSS
   * \param address The MAC address of the DMG PCP/AP
   */
  void InitiateAbft(Mac48Address address);
  /**
   * Get next sector in A-BFT access period.
   * \return True, if we still have sectors to cover otherwise false.
   */
  bool GetNextSectorInAbft();
  /**
   * Initiate Beam Refinement Protocol after completing SLS phase.
   * \param antennaID The ID of the phased antenna array.
   * \param sectorID The ID of the sector to refine wthin the antenna array.
   * \param type Beam Refinement Type (Transmit/Receive).
   */
  void InitiateBrp(AntennaId antennaID, SectorId sectorID,
                   BeamRefinementType type);
  /**
   * Reuse custom AWV for communication/Beam refinement.
   * This function is called in WigigPhy during beam refinement.
   * \param type Type of Beam refinement - Transmit or Receive.
   */
  void UseCustomAwv(BeamRefinementType type);
  /**
   * Reuse the last used transmit sector for each active antenna array in the
   * list of active RF chains. This function is called after Beam Tracking or
   * initiating Beam Refinement.
   */
  void UseLastTxSector();
  /**
   * Get current active transmit antenna array pattern config.
   * \return A pointer to the pattern conifg of the transmit phased antenna
   * array.
   */
  Ptr<PatternConfig> GetTxPatternConfig() const;
  /**
   * Get current active receive antenna array pattern config.
   * \return A pointer to the pattern conifg of the receive phased antenna
   * array.
   */
  Ptr<PatternConfig> GetRxPatternConfig() const;

protected:
  void DoInitialize() override;

  /**
   * Get a pointer to the WigigNetDevice we are associated with.
   * \return device A pointer to the WigigNetDevice we are associated with.
   */
  Ptr<WigigNetDevice> GetDevice() const;

  /**
   * Set the ID of the active RF chain for SISO transmission.
   * \param chainID The ID of the RF Chain to be activated.
   */
  void SetActiveRfChainId(RfChainId chainID);
  /**
   * Get active Tx pattern ID (Sector ID or AWV ID).
   * \return Active Tx pattern ID
   */
  uint8_t GetActiveTxPatternId() const;
  /**
   * Get active Rx pattern ID (Sector ID or AWV ID).
   * \return Active Rx pattern ID.
   */
  uint8_t GetActiveRxPatternId() const;
  /**
   * Get current active antenna array config.
   * \return A pointer to the current active antenna array config.
   */
  Ptr<PhasedAntennaArrayConfig> GetAntennaArrayConfig() const;

  /**
   * Set specific antenna pattern to quasi-omni mode.
   * \param antennaID The ID of the phased antenna array.
   */
  void SetReceivingInQuasiOmniMode(AntennaId antennaID);
  /**
   * Check the current receiving mode (Quasi = True, Dierctional = False) in the
   * current active RF chain. \return True if the antenna is in quasi-omni
   * reception mode; otherwise false.
   */
  bool IsQuasiOmniMode() const;
  /**
   * Check whether we are using custom AWV for Tx or Rx.
   * \return True if we are using custom AWV, otherwise False if we are using
   * sector.
   */
  bool IsCustomAwvUsed() const;
  /**
   * Get number of remaining AWVs in current sector.
   * \return The total number of remaining AWVs in the current sector.
   */
  uint8_t GetRemainingAwvCount() const;
  /**
   * For the current sector start using different AWVs if possible.
   * Used in when we are training using TRN-R fields appended to DMG Beacons.
   */
  void StartSectorRefinement();

  typedef std::map<RfChainId, Ptr<RfChain>>
      RfChainList; //!< Typedef for list of RF chains.
  typedef std::map<RfChainId, AntennaId>
      ActiveRfChainList; //!< Typedef for list of active RF Chains and their
                         //!< corresponding active antennas.

  /* Codebook Variables */
  std::string m_fileName; //!< The name of the file describing the transmit and
                          //!< receive patterns.
  Ptr<WigigNetDevice>
      m_device; //!< Pointer to the WigigNetDevice we are associated with.

  /* Front End Architecture Variables */
  RfChainList
      m_rfChainList; //!< List of RF Chains associated with this codebook.
  ActiveRfChainList
      m_activeRfChainList;     //!< List of active RF Chain list with their
                               //!< corresponding active antenna array for MIMO.
  RfChainId m_activeRfChainId; //!< Active RF Chain for SISO transmission.
  Ptr<RfChain> m_activeRfChain; //!< Pointer to the current active RF chain.
  AntennaArrayList m_antennaArrayList; //!< List of antenna arrays associated
                                       //!< with this codebook.

  /* Beamforming Variables */
  CurrentBeamformingPhase
      m_currentBFPhase; //!< The current beamforming phase (BTI/A-BFT or SLS).
  SectorSweepType m_sectorSweepType; //!< Sector Sweep Type during SLS Phase
                                     //!< (Transmit/Receive).
  SectorId
      m_currentSectorIndex; //!< The index of the current sector for BTI or SLS.
  uint16_t m_remainingSectors; //!< The total number of sectors remaining to
                               //!< cover in SLS phase.
  Antenna2SectorListI
      m_beamformingAntenna; //!< Iterator to the current utilized antenna in
                            //!< either BTI or SLS.
  SectorIdList m_beamformingSectorList; //!< The list of the sectors within the
                                        //!< current used antenna.
  Antenna2SectorList *m_currentSectorList; //!< Current list of TXSS sectors.
  Mac48Address
      m_peerStation; //!< The MAC address of the peer station in the SLS phase.

  /* Beamforming Variables */
  Antenna2SectorList m_txBeamformingSectors; //!< List of the general transmit
                                             //!< sectors utilized during SLS.
  Antenna2SectorList m_rxBeamformingSectors; //!< List of the general receive
                                             //!< sectors utilized during SLS.
  BeamformingSectorList m_txCustomSectors;   //!< List of transmit sectors
                                           //!< utilized during SLS per Station.
  BeamformingSectorList m_rxCustomSectors; //!< List of receive sectors utilized
                                           //!< during SLS per Station.
  uint8_t m_totalTxSectors; //!< The total number of transmit sectors within the
                            //!< Codebook.
  uint8_t m_totalRxSectors; //!< The total number of receive sectors within the
                            //!< Codebook.
  uint8_t m_totalSectors; //!< The total number of sectors within the Codebook.
  uint8_t
      m_totalAntennas; //!< The total number of antennas within the Codebook.

  /* BHI Access Period Variables */
  Antenna2SectorList m_bhiAntennaList; //!< List of antenna arrays utilized
                                       //!< during the BHI access period.
  bool m_beaconRandomization; //!< Flag to indicate whether we want to randomize
                              //!< the selection of DMG Beacon for each BI.
  uint8_t
      m_btiSectorOffset; //!< The first antenna sector to start the BTI with.
  Antenna2SectorListI
      m_bhiAntennaI; //!< Iterator to the current utilized antenna array and its
                     //!< corresponding sectors in BHI access period.

  /* Beam Refinement and Tracking Variables */
  AWV_LIST_I m_currentAwvI; //!< An iterator for the current AWV list.

private:
  /**
   * Get the number Of sectors with specific station.
   * \param address The MAC address of the peer station.
   * \param list The beamforming Tx/Rx sector list.
   * \return The total number of Tx/Rx sectors.
   */
  uint8_t GetNumberOfSectors(Mac48Address address, BeamformingSectorList &list);
  /**
   * Get the total number of sector for specific beamforming list.
   * \param sectorList The list of antennas with their sectors.
   * \return The total number of sector for specific beamforming list.
   */
  uint8_t CountNumberOfSectors(Antenna2SectorList *sectorList);

  /**
   * Append new sector to a sector list.
   * \param globalList
   * \param antennaID The ID of the Antenna.
   * \param sectorID The ID of the sector.
   */
  void AppendToSectorList(Antenna2SectorList &globalList, AntennaId antennaID,
                          SectorId sectorID);
  /**
   * Set the ID of the active transmit sector.
   * \param sectorID The ID of the transmit sector wthin the antenna array.
   */
  void SetActiveTxSectorId(SectorId sectorID);
  /**
   * Set the ID of the active receive sector.
   * \param sectorID The ID of the receive sector wthin the antenna array.
   */
  void SetActiveRxSectorId(SectorId sectorID);
  /**
   * Activate an RF chain and add it to the last of active RF chains.
   * \param chainID The ID of the RF Chain to be activated.
   */
  void ActivateRfChain(RfChainId chainID);
};

} // namespace ns3

#endif /* CODEBOOK_H */
