/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "fields-headers.h"

#include "ns3/address-utils.h"
#include "ns3/fatal-error.h"
#include "ns3/wifi-utils.h"

namespace ns3 {

/***********************************
 * Sector Sweep (SSW) Field (8.4a.1)
 ***********************************/

NS_OBJECT_ENSURE_REGISTERED(DmgSswField);

DmgSswField::DmgSswField()
    : m_dir(BeamformingInitiator), m_cdown(0), m_sid(0), m_antenna_id(0),
      m_length(0) {}

DmgSswField::~DmgSswField() {}

TypeId DmgSswField::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgSswField")
                          .SetParent<Header>()
                          .AddConstructor<DmgSswField>();
  return tid;
}

TypeId DmgSswField::GetInstanceTypeId() const { return GetTypeId(); }

void DmgSswField::Print(std::ostream &os) const {
  os << "Direction=" << m_dir << ", CDOWN=" << m_cdown << ", SID=" << m_sid
     << ", Antenna ID=" << m_antenna_id << ", RXSS Length=" << m_length;
}

uint32_t DmgSswField::GetSerializedSize() const { return 3; }

Buffer::Iterator DmgSswField::Serialize(Buffer::Iterator start) const {
  uint8_t ssw[3];

  ssw[0] = m_dir & 0x1;
  ssw[0] |= ((m_cdown & 0x7F) << 1);
  ssw[1] = ((m_cdown & 0x180) >> 7);
  ssw[1] |= ((m_sid & 0x3F) << 2);
  ssw[2] = m_antenna_id & 0x3;
  ssw[2] |= ((m_length & 0x3F) << 2);

  start.Write(ssw, 3);

  return start;
}

Buffer::Iterator DmgSswField::Deserialize(Buffer::Iterator start) {
  uint8_t ssw[3];

  start.Read(ssw, 3);
  m_dir = static_cast<BeamformingDirection>(ssw[0] & 0x1);
  m_cdown = (static_cast<uint16_t>(ssw[0] & 0xFE) >> 1) |
            (static_cast<uint16_t>(ssw[1] & 0x03) << 7);
  m_sid = (ssw[1] & 0xFC) >> 2;
  m_antenna_id = ssw[2] & 0x3;
  m_length = (ssw[2] & 0xFC) >> 2;

  return start;
}

void DmgSswField::SetDirection(BeamformingDirection dir) { m_dir = dir; }

void DmgSswField::SetCountDown(uint16_t cdown) {
  NS_ASSERT((0 <= cdown) && (cdown <= 511));
  m_cdown = cdown;
}

void DmgSswField::SetSectorId(uint8_t sid) {
  NS_ASSERT((1 <= sid) && (sid <= 64));
  m_sid = sid - 1;
}

void DmgSswField::SetDmgAntennaId(uint8_t antennaId) {
  NS_ASSERT(1 <= antennaId && antennaId <= 4);
  m_antenna_id = antennaId - 1;
}

void DmgSswField::SetRxssLength(uint8_t length) { m_length = length; }

BeamformingDirection DmgSswField::GetDirection() const { return m_dir; }

uint16_t DmgSswField::GetCountDown() const { return m_cdown; }

uint8_t DmgSswField::GetSectorId() const { return m_sid + 1; }

uint8_t DmgSswField::GetDmgAntennaId() const { return m_antenna_id + 1; }

uint8_t DmgSswField::GetRxssLength() const { return m_length; }

/*****************************************
 * Dynamic Allocation Info Field (8.4a.2)
 *****************************************/

NS_OBJECT_ENSURE_REGISTERED(DynamicAllocationInfoField);

DynamicAllocationInfoField::DynamicAllocationInfoField()
    : m_tid(0), m_allocationType(SERVICE_PERIOD_ALLOCATION), m_sourceAID(0),
      m_destinationAID(0), m_allocationDuration(0) {}

DynamicAllocationInfoField::~DynamicAllocationInfoField() {}

TypeId DynamicAllocationInfoField::GetTypeId() {
  static TypeId tid = TypeId("ns3::DynamicAllocationInfoField")
                          .SetParent<Header>()
                          .AddConstructor<DynamicAllocationInfoField>();
  return tid;
}

TypeId DynamicAllocationInfoField::GetInstanceTypeId() const {
  return GetTypeId();
}

void DynamicAllocationInfoField::Print(std::ostream &os) const {}

uint32_t DynamicAllocationInfoField::GetSerializedSize() const { return 5; }

Buffer::Iterator
DynamicAllocationInfoField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint32_t field1 = 0;
  uint8_t field2 = 0;

  field1 |= m_tid & 0xF;
  field1 |= (m_allocationType & 0x7) << 4;
  field1 |= m_sourceAID << 7;
  field1 |= m_destinationAID << 15;
  field1 |= static_cast<uint32_t>(m_allocationDuration & 0x1FF) << 23;

  field2 |= (m_allocationDuration >> 9) & 0x7F;

  i.WriteHtolsbU32(field1);
  i.WriteU8(field2);

  return i;
}

Buffer::Iterator
DynamicAllocationInfoField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint32_t field1 = i.ReadLsbtohU32();
  uint8_t field2 = i.ReadU8();

  m_tid = field1 & 0xF;
  m_allocationType = static_cast<AllocationType>((m_allocationType >> 4) & 0x7);
  m_sourceAID = (field1 >> 7) & 0xFF;
  m_destinationAID = (field1 >> 15) & 0xFF;
  m_allocationDuration = (static_cast<uint16_t>(field1 >> 23) & 0x1FF) |
                         (static_cast<uint16_t>(field2 & 0x7F) << 9);

  return i;
}

void DynamicAllocationInfoField::SetTid(uint8_t tid) { m_tid = tid; }

void DynamicAllocationInfoField::SetAllocationType(AllocationType value) {
  m_allocationType = value;
}

void DynamicAllocationInfoField::SetSourceAid(uint8_t aid) {
  m_sourceAID = aid;
}

void DynamicAllocationInfoField::SetDestinationAid(uint8_t aid) {
  m_destinationAID = aid;
}

void DynamicAllocationInfoField::SetAllocationDuration(uint16_t duration) {
  m_allocationDuration = duration;
}

uint8_t DynamicAllocationInfoField::GetTid() const { return m_tid; }

AllocationType DynamicAllocationInfoField::GetAllocationType() const {
  return m_allocationType;
}

uint8_t DynamicAllocationInfoField::GetSourceAid() const { return m_sourceAID; }

uint8_t DynamicAllocationInfoField::GetDestinationAid() const {
  return m_destinationAID;
}

uint16_t DynamicAllocationInfoField::GetAllocationDuration() const {
  return m_allocationDuration;
}

/*********************************************
 * Sector Sweep Feedback (SSW) Field (8.4a.3)
 *********************************************/

NS_OBJECT_ENSURE_REGISTERED(DmgSswFbckField);

DmgSswFbckField::DmgSswFbckField()
    : m_sectors(0), m_antennas(0), m_snrReport(0), m_pollRequired(false),
      m_iss(false) {}

DmgSswFbckField::~DmgSswFbckField() {}

TypeId DmgSswFbckField::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgSswFbckField")
                          .SetParent<Header>()
                          .AddConstructor<DmgSswFbckField>();
  return tid;
}

TypeId DmgSswFbckField::GetInstanceTypeId() const { return GetTypeId(); }

void DmgSswFbckField::Print(std::ostream &os) const {}

uint32_t DmgSswFbckField::GetSerializedSize() const { return 3; }

Buffer::Iterator DmgSswFbckField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint8_t ssw[3];
  memset(ssw, 0, sizeof(ssw));
  if (m_iss) {
    ssw[0] |= m_sectors & 0xFF;
    ssw[1] |= (m_sectors >> 8) & 0x1;
    ssw[1] |= (m_antennas & 0x3) << 1;
    ssw[1] |= (m_snrReport & 0x1F) << 3;
    ssw[2] |= m_pollRequired & 0x1;
  } else {
    ssw[0] |= m_sectors & 0x3F;
    ssw[0] |= (m_antennas & 0x3) << 6;
    ssw[1] |= m_snrReport;
    ssw[2] |= m_pollRequired & 0x1;
  }
  i.Write(ssw, 3);
  return i;
}

Buffer::Iterator DmgSswFbckField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint8_t ssw[3];
  i.Read(ssw, 3);
  if (m_iss) {
    m_sectors = (ssw[0] & 0xFF) | ((ssw[1] & 0x1) << 8);
    m_antennas = (ssw[1] >> 1) & 0x3;
    m_snrReport = (ssw[1] >> 3) & 0x1F;
    m_pollRequired = ssw[2] & 0x1;
  } else {
    m_sectors = ssw[0] & 0x3F;
    m_antennas = (ssw[0] >> 6) & 0x3;
    m_snrReport = ssw[1];
    m_pollRequired = ssw[2] & 0x1;
  }
  return i;
}

void DmgSswFbckField::SetSector(uint16_t value) { m_sectors = value; }

void DmgSswFbckField::SetDmgAntenna(uint8_t value) { m_antennas = value; }

void DmgSswFbckField::SetSnrReport(double value) {
  value = RatioToDb(value);
  if (value >= 50.75) {
    value = 50.75;
  } else if (value <= -13.0) {
    value = -13.0;
  }
  m_snrReport = 4 * (value - 19);
}

void DmgSswFbckField::SetPollRequired(bool value) { m_pollRequired = value; }

void DmgSswFbckField::IsPartOfIss(bool value) { m_iss = value; }

uint16_t DmgSswFbckField::GetSector() const { return m_sectors; }

uint8_t DmgSswFbckField::GetDmgAntenna() const { return m_antennas; }

double DmgSswFbckField::GetSnrReport() const {
  return (static_cast<double>(m_snrReport) / 4 + 19);
}

bool DmgSswFbckField::GetPollRequired() const { return m_pollRequired; }

/***********************************
 *    BRP Request Field (8.4a.4).
 ***********************************/

NS_OBJECT_ENSURE_REGISTERED(BrpRequestField);

BrpRequestField::BrpRequestField()
    : m_lRx(0), m_txTrnReq(false), m_midReq(false), m_bcReq(false),
      m_midGrant(false), m_bcGrant(false), m_channelFbckCap(false),
      m_txSectorId(0), m_otherAid(0), m_txAntennaId(0) {}

BrpRequestField::~BrpRequestField() {}

TypeId BrpRequestField::GetTypeId() {
  static TypeId tid = TypeId("ns3::BrpRequestField")
                          .SetParent<Header>()
                          .AddConstructor<BrpRequestField>();
  return tid;
}

TypeId BrpRequestField::GetInstanceTypeId() const { return GetTypeId(); }

void BrpRequestField::Print(std::ostream &os) const {
  os << "L_RX=" << m_lRx << ", TX_TRN_REQ=" << m_txTrnReq
     << ", MID_REQ=" << m_midReq << ", BC_REQ=" << m_bcReq
     << ", MID_Grant=" << m_midGrant << ", BC_Grant=" << m_bcGrant
     << ", Channel_FBCK_CAP=" << m_channelFbckCap
     << ", TXSectorId=" << m_txSectorId << ", OtherAID=" << m_otherAid
     << ", TXAntennaId=" << m_txAntennaId;
}

uint32_t BrpRequestField::GetSerializedSize() const { return 4; }

Buffer::Iterator BrpRequestField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint32_t brp = 0;

  brp |= m_lRx & 0x1F;
  brp |= ((m_txTrnReq & 0x1) << 5);
  brp |= ((m_midReq & 0x1) << 6);
  brp |= ((m_bcReq & 0x1) << 7);
  brp |= ((m_midGrant & 0x1) << 8);
  brp |= ((m_bcGrant & 0x1) << 9);
  brp |= ((m_channelFbckCap & 0x1) << 10);
  brp |= ((m_txSectorId & 0x3F) << 11);
  brp |= (m_otherAid << 17);
  brp |= ((m_txAntennaId & 0x3) << 25);

  i.WriteHtolsbU32(brp);

  return i;
}

Buffer::Iterator BrpRequestField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint32_t brp = i.ReadLsbtohU32();

  m_lRx = brp & 0x1F;
  m_txTrnReq = (brp >> 5) & 0x1;
  m_midReq = (brp >> 6) & 0x1;
  m_bcReq = (brp >> 7) & 0x1;
  m_midGrant = (brp >> 8) & 0x1;
  m_bcGrant = (brp >> 9) & 0x1;
  m_channelFbckCap = (brp >> 10) & 0x1;
  m_txSectorId = (brp >> 11) & 0x3F;
  m_otherAid = (brp >> 17) & 0xFF;
  m_txAntennaId = (brp >> 25) & 0x3;

  return i;
}

void BrpRequestField::SetLRx(uint8_t value) { m_lRx = value; }

void BrpRequestField::SetTxTrnReq(bool value) { m_txTrnReq = value; }

void BrpRequestField::SetMidReq(bool value) { m_midReq = value; }

void BrpRequestField::SetBcReq(bool value) { m_bcReq = value; }

void BrpRequestField::SetMidGrant(bool value) { m_midGrant = value; }

void BrpRequestField::SetBcGrant(bool value) { m_bcGrant = value; }

void BrpRequestField::SetChannelFbckCap(bool value) {
  m_channelFbckCap = value;
}

void BrpRequestField::SetTxSectorId(uint8_t value) { m_txSectorId = value; }

void BrpRequestField::SetOtherAid(uint8_t value) { m_otherAid = value; }

void BrpRequestField::SetTxAntennaId(uint8_t value) { m_txAntennaId = value; }

uint8_t BrpRequestField::GetLRx() const { return m_lRx; }

bool BrpRequestField::GetTxTrnReq() const { return m_txTrnReq; }

bool BrpRequestField::GetMidReq() const { return m_midReq; }

bool BrpRequestField::GetBcReq() const { return m_bcReq; }

bool BrpRequestField::GetMidGrant() const { return m_midGrant; }

bool BrpRequestField::GetBcGrant() const { return m_bcGrant; }

bool BrpRequestField::GetChannelFbckCap() const { return m_channelFbckCap; }

uint8_t BrpRequestField::GetTxSectorId() const { return m_txSectorId; }

uint8_t BrpRequestField::GetOtherAid() const { return m_otherAid; }

uint8_t BrpRequestField::GetTxAntennaId() const { return m_txAntennaId; }

/***************************************
 * Beamforming Control Field (8.4a.5)
 ***************************************/

NS_OBJECT_ENSURE_REGISTERED(BfControlField);

BfControlField::BfControlField()
    : m_beamformTraining(false), m_isInitiatorTxss(false),
      m_isResponderTxss(false), m_sectors(0), m_antennas(0), m_rxssLength(0),
      m_rxssTXRate(0) {}

BfControlField::~BfControlField() {}

TypeId BfControlField::GetTypeId() {
  static TypeId tid = TypeId("ns3::BfControlField")
                          .SetParent<Header>()
                          .AddConstructor<BfControlField>();
  return tid;
}

TypeId BfControlField::GetInstanceTypeId() const { return GetTypeId(); }

void BfControlField::Print(std::ostream &os) const {
  os << "Beamforming Training=" << m_beamformTraining
     << ", IsInitiatorTxss=" << m_isInitiatorTxss
     << ", IsResponderTxss=" << m_isResponderTxss;

  if (m_isInitiatorTxss && m_isResponderTxss) {
    os << ", Total Number of Sectors=" << m_sectors
       << ", Number of RX DMG Antennas=" << m_antennas;
  } else {
    os << ", RXSS Length=" << m_rxssLength << ", RXSSTxRate=" << m_rxssTXRate;
  }
}

uint32_t BfControlField::GetSerializedSize() const { return 2; }

Buffer::Iterator BfControlField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint16_t value = 0;

  /* Common Subfields */
  value |= m_beamformTraining & 0x1;
  value |= ((m_isInitiatorTxss & 0x1) << 1);
  value |= ((m_isResponderTxss & 0x1) << 2);

  if (m_isInitiatorTxss && m_isResponderTxss) {
    value |= ((m_sectors & 0x7F) << 3);
    value |= ((m_antennas & 0x3) << 10);
  } else {
    value |= ((m_rxssLength & 0x3F) << 3);
    value |= ((m_rxssTXRate & 0x1) << 9);
  }

  i.WriteHtolsbU16(value);

  return i;
}

Buffer::Iterator BfControlField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint8_t value = i.ReadLsbtohU16();

  m_beamformTraining = value & 0x1;
  m_isInitiatorTxss = ((value >> 1) & 0x1);
  m_isResponderTxss = ((value >> 2) & 0x1);

  if (m_isInitiatorTxss && m_isResponderTxss) {
    m_sectors |= ((value >> 3) & 0x7F);
    m_antennas |= ((value >> 10) & 0x3);
  } else {
    m_rxssLength |= ((value >> 3) & 0x3F);
    m_rxssTXRate |= ((value >> 9) & 0x1);
  }

  return i;
}

void BfControlField::SetBeamformTraining(bool value) {
  m_beamformTraining = value;
}

void BfControlField::SetAsInitiatorTXSS(bool value) {
  m_isInitiatorTxss = value;
}

void BfControlField::SetAsResponderTxss(bool value) {
  m_isResponderTxss = value;
}

void BfControlField::SetTotalNumberOfSectors(uint8_t sectors) {
  m_sectors = sectors;
}

void BfControlField::SetNumberOfRxDmgAntennas(uint8_t antennas) {
  m_antennas = antennas;
}

void BfControlField::SetRxssLength(uint8_t length) { m_rxssLength = length; }

void BfControlField::SetRxssTxRate(bool rate) { m_rxssTXRate = rate; }

bool BfControlField::IsBeamformTraining() const { return m_beamformTraining; }

bool BfControlField::IsInitiatorTxss() const { return m_isInitiatorTxss; }

bool BfControlField::IsResponderTxss() const { return m_isResponderTxss; }

uint8_t BfControlField::GetTotalNumberOfSectors() const { return m_sectors; }

uint8_t BfControlField::GetNumberOfRxDmgAntennas() const { return m_antennas; }

uint8_t BfControlField::GetRxssLength() const { return m_rxssLength; }

bool BfControlField::GetRxssTxRate() const { return m_rxssTXRate; }

/***************************************
 * Beamformed Link Maintenance (8.4a.6)
 ***************************************/

NS_OBJECT_ENSURE_REGISTERED(BfLinkMaintenanceField);

BfLinkMaintenanceField::BfLinkMaintenanceField()
    : m_unitIndex(UNIT_32US), m_value(0), m_isMaster(false) {}

BfLinkMaintenanceField::~BfLinkMaintenanceField() {}

TypeId BfLinkMaintenanceField::GetTypeId() {
  static TypeId tid = TypeId("ns3::BfLinkMaintenanceField")
                          .SetParent<Header>()
                          .AddConstructor<BfLinkMaintenanceField>();
  return tid;
}

TypeId BfLinkMaintenanceField::GetInstanceTypeId() const { return GetTypeId(); }

void BfLinkMaintenanceField::Print(std::ostream &os) const {
  os << "Unit Index=" << m_unitIndex << ", Value=" << m_value
     << ", isMaster=" << m_isMaster;
}

uint32_t BfLinkMaintenanceField::GetSerializedSize() const { return 1; }

Buffer::Iterator
BfLinkMaintenanceField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  uint8_t value = 0;

  value |= static_cast<uint8_t>(m_unitIndex) & 0x1;
  value |= ((m_value & 0x3F) << 1);
  value |= ((m_isMaster & 0x1) << 7);

  i.WriteU8(value);

  return i;
}

Buffer::Iterator BfLinkMaintenanceField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  uint8_t value = i.ReadU8();

  m_unitIndex = static_cast<BeamLinkMaintenanceUnitIndex>(value & 0x1);
  m_value = ((value >> 1) & 0x3F);
  m_isMaster = ((value >> 7) & 0x1);

  return i;
}

void BfLinkMaintenanceField::SetUnitIndex(BeamLinkMaintenanceUnitIndex index) {
  m_unitIndex = index;
}

void BfLinkMaintenanceField::SetMaintenanceValue(uint8_t value) {
  m_value = value;
}

void BfLinkMaintenanceField::SetAsMaster(bool value) { m_isMaster = value; }

BeamLinkMaintenanceUnitIndex BfLinkMaintenanceField::GetUnitIndex() const {
  return m_unitIndex;
}

uint8_t BfLinkMaintenanceField::GetMaintenanceValue() const { return m_value; }

bool BfLinkMaintenanceField::IsMaster() const { return m_isMaster; }

/************************************************
 *  DMG Beacon Clustering Control Field (8-34c&d)
 *************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtDmgClusteringControlField);

ExtDmgClusteringControlField::ExtDmgClusteringControlField()
    : m_discoveryMode(false) {}

ExtDmgClusteringControlField::~ExtDmgClusteringControlField() {}

TypeId ExtDmgClusteringControlField::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtDmgClusteringControlField")
                          .SetParent<Header>()
                          .AddConstructor<ExtDmgClusteringControlField>();
  return tid;
}

TypeId ExtDmgClusteringControlField::GetInstanceTypeId() const {
  return GetTypeId();
}

uint32_t ExtDmgClusteringControlField::GetSerializedSize() const { return 8; }

void ExtDmgClusteringControlField::Print(std::ostream &os) const {}

Buffer::Iterator
ExtDmgClusteringControlField::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  if (m_discoveryMode) {
    WriteTo(i, m_responderAddress);
    i.WriteHtolsbU16(0); // reserved
  } else {
    uint8_t value = 0;
    i.WriteU8(m_beaconSpDuration);
    WriteTo(i, m_clusterId);
    value |= m_clusterMemberRole & 0x3;
    value |= (m_clusterMaxMem & 0x1F) << 2;
    i.WriteU8(value);
  }
  return i;
}

Buffer::Iterator
ExtDmgClusteringControlField::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  if (m_discoveryMode) {
    ReadFrom(i, m_responderAddress);
    i.ReadLsbtohU16(); // reserved
  } else {
    m_beaconSpDuration = i.ReadU8();
    ReadFrom(i, m_clusterId);
    uint8_t value = i.ReadU8();
    m_clusterMemberRole = value & 0x3;
    m_clusterMaxMem = (value >> 2) & 0x1F;
  }
  return i;
}

void ExtDmgClusteringControlField::SetDiscoveryMode(bool value) {
  m_discoveryMode = value;
}

bool ExtDmgClusteringControlField::GetDiscoveryMode() const {
  return m_discoveryMode;
}

void ExtDmgClusteringControlField::SetBeaconSpDuration(uint8_t duration) {
  m_beaconSpDuration = duration;
}

void ExtDmgClusteringControlField::SetClusterId(Mac48Address clusterID) {
  m_clusterId = clusterID;
}

void ExtDmgClusteringControlField::SetClusterMemberRole(
    ClusterMemberRole role) {
  m_clusterMemberRole = static_cast<ClusterMemberRole>(role);
}

void ExtDmgClusteringControlField::SetClusterMaxMem(uint8_t max) {
  m_clusterMaxMem = max;
}

uint8_t ExtDmgClusteringControlField::GetBeaconSpDuration() const {
  return m_beaconSpDuration;
}

Mac48Address ExtDmgClusteringControlField::GetClusterId() const {
  return m_clusterId;
}

ClusterMemberRole ExtDmgClusteringControlField::GetClusterMemberRole() const {
  return static_cast<ClusterMemberRole>(m_clusterMemberRole);
}

uint8_t ExtDmgClusteringControlField::GetClusterMaxMem() const {
  return m_clusterMaxMem;
}

void ExtDmgClusteringControlField::SetAbftResponderAddress(
    Mac48Address address) {
  m_responderAddress = address;
}

Mac48Address ExtDmgClusteringControlField::GetAbftResponderAddress() const {
  return m_responderAddress;
}

} // namespace ns3
