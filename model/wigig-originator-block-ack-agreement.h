/*
 * Copyright (c) 2009, 2010 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mirko Banchi <mk.banchi@gmail.com>
 * Author: Tommaso Pecorella <tommaso.pecorella@unifi.it>
 */

#ifndef WIGIG_ORIGINATOR_BLOCK_ACK_AGREEMENT_H
#define WIGIG_ORIGINATOR_BLOCK_ACK_AGREEMENT_H

#include "wigig-block-ack-agreement.h"

#include "ns3/originator-block-ack-agreement.h"

namespace ns3 {

class WigigMacQueueItem;
class WigigBlockAckManager;

/**
 * \ingroup wigig
 * Maintains the state and information about transmitted MPDUs with Ack Policy
 set to Block Ack
 * for an originator station. The state diagram is as follows:
 *
   \verbatim
    /------------\ send ADDBARequest ----------------
    |   START    |------------------>|   PENDING    |-------
    \------------/                   ----------------       \
          ^            receive     /        |                \
          |        ADDBAResponse  /         |                 \
          |          (failure)   v          |                  \
          |        ---------------          | --------------------->
 ---------------- |        |  REJECTED   |          |          receive
 ADDBAResponse (success)  | ESTABLISHED | |        ---------------          | no
 -------------------->
 ---------------- |           receive    ^          | ADDBAResponse     / |
 ADDBAResponse \ |                  / |          (failure)     \        v / |
 ---------------- /
          |-------------------------|   NO_REPLY   |---------
            Reset after timeout     ----------------
   \endverbatim
 *
 * See also WigigOriginatorBlockAckAgreement::State
 */
class WigigOriginatorBlockAckAgreement : public WigigBlockAckAgreement {
  /// allow WigigBlockAckManager class access
  friend class WigigBlockAckManager;

public:
  WigigOriginatorBlockAckAgreement();
  /**
   * Constructor
   *
   * \param recipient MAC address
   * \param tid Traffic ID
   */
  WigigOriginatorBlockAckAgreement(Mac48Address recipient, uint8_t tid);
  ~WigigOriginatorBlockAckAgreement();

  /**
   * Set the current state.
   *
   * \param state to set
   */
  void SetState(OriginatorBlockAckAgreement::State state);
  /**
   * Get the current state.
   *
   * \return state
   */
  OriginatorBlockAckAgreement::State GetState() const;
  /**
   * Check if the current state of this agreement is PENDING.
   *
   * \return true if the current state of this agreement is PENDING,
   *         false otherwise
   */
  bool IsPending() const;
  /**
   * Check if the current state of this agreement is ESTABLISHED.
   *
   * \return true if the current state of this agreement is ESTABLISHED,
   *         false otherwise
   */
  bool IsEstablished() const;
  /**
   * Check if the current state of this agreement is NO_REPLY.
   *
   * \return true if the current state of this agreement is NO_REPLY,
   *         false otherwise
   */
  bool IsNoReply() const;
  /**
   * Check if the current state of this agreement is RESET.
   *
   * \return true if the current state of this agreement is RESET,
   *         false otherwise
   */
  bool IsReset() const;
  /**
   * Check if the current state of this agreement is REJECTED.
   *
   * \return true if the current state of this agreement is REJECTED,
   *         false otherwise
   */
  bool IsRejected() const;

  /**
   * Return the starting sequence number of the transmit window, if a transmit
   * window has been initialized. Otherwise, return the starting sequence number
   * stored by the WigigBlockAckAgreement base class.
   *
   * \return the starting sequence number.
   */
  uint16_t GetStartingSequence() const override;

  /**
   * Get the distance between the current starting sequence number and the
   * given sequence number.
   *
   * \param seqNumber the given sequence number
   * \return the distance of the given sequence number from the current winstart
   */
  std::size_t GetDistance(uint16_t seqNumber) const;

  /**
   * Initialize the originator's transmit window by setting its size and
   * starting sequence number equal to the values stored by the
   * WigigBlockAckAgreement base class.
   */
  void InitTxWindow();

  /**
   * Advance the transmit window so as to include the transmitted MPDU, if the
   * latter is not an old packet and is beyond the current transmit window.
   *
   * \param mpdu the transmitted MPDU
   */
  void NotifyTransmittedMpdu(Ptr<const WigigMacQueueItem> mpdu);
  /**
   * Record that the given MPDU has been acknowledged and advance the transmit
   * window if possible.
   *
   * \param mpdu the acknowledged MPDU
   */
  void NotifyAckedMpdu(Ptr<const WigigMacQueueItem> mpdu);
  /**
   * Advance the transmit window beyond the MPDU that has been reported to
   * be discarded.
   *
   * \param mpdu the discarded MPDU
   */
  void NotifyDiscardedMpdu(Ptr<const WigigMacQueueItem> mpdu);

private:
  /**
   * Advance the transmit window so that the starting sequence number is the
   * nearest unacknowledged MPDU.
   */
  void AdvanceTxWindow();

  OriginatorBlockAckAgreement::State m_state; ///< state
  BlockAckWindow m_txWindow;                  ///< originator's transmit window
};

} // namespace ns3

#endif /* WIGIG_ORIGINATOR_BLOCK_ACK_AGREEMENT_H */
