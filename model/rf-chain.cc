/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "rf-chain.h"

#include "ns3/log.h"
#include "ns3/traced-value.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("RfChain");

NS_OBJECT_ENSURE_REGISTERED(RfChain);

void PhasedAntennaArrayConfig::SetQuasiOmniConfig(
    Ptr<PatternConfig> quasiPattern) {
  m_quasiOmniConfig = quasiPattern;
}

Ptr<PatternConfig> PhasedAntennaArrayConfig::GetQuasiOmniConfig() const {
  return m_quasiOmniConfig;
}

TypeId RfChain::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::RfChain")
          .SetGroupName("Wigig")
          .SetParent<Object>()
          .AddTraceSource("ActiveAntennaId",
                          "Trace source for tracking the active antenna ID",
                          MakeTraceSourceAccessor(&RfChain::m_antennaId),
                          "ns3::TracedValueCallback::Uint8")
          .AddTraceSource("ActiveTxSectorId",
                          "Trace source for tracking the active Tx Sector ID",
                          MakeTraceSourceAccessor(&RfChain::m_txSectorId),
                          "ns3::TracedValueCallback::Uint8")
          .AddTraceSource("ActiveRxSectorId",
                          "Trace source for tracking the active Rx Sector ID",
                          MakeTraceSourceAccessor(&RfChain::m_rxSectorId),
                          "ns3::TracedValueCallback::Uint8");
  return tid;
}

RfChain::RfChain()
    : m_antennaConfig(nullptr), m_txPattern(nullptr), m_rxPattern(nullptr),
      m_useAwv(false), m_beamRefinementType(RefineTransmitSector),
      m_remainingAwvs(0), m_quasiOmniMode(false) {
  NS_LOG_FUNCTION(this);
}

RfChain::~RfChain() { NS_LOG_FUNCTION(this); }

void RfChain::DoInitialize() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(
      !m_antennaArrayList.empty(),
      "At least one antenna array must be connected to this RF chain.");
  Reset();
}

void RfChain::Reset() {
  NS_LOG_FUNCTION(this);
  /* We activate the first antenna array in the RF chain */
  SetActiveAntennaId(m_antennaArrayList.begin()->first);
  SetActiveTxSectorId(m_antennaConfig->sectorList.begin()->first);
  /* Set reception in Quasi-omni pattern */
  SetReceivingInQuasiOmniMode();
}

void RfChain::ConnectPhasedAntennaArray(AntennaId antennaID,
                                        Ptr<PhasedAntennaArrayConfig> array) {
  NS_ASSERT_MSG(!array->isConnected,
                "The antenna array is already connected to an RF Chain.");
  m_antennaArrayList[antennaID] = array;
  array->isConnected = true;
}

void RfChain::SetActiveTxAwvId(AwvId awvId) {
  NS_LOG_FUNCTION(this << +awvId);
  m_useAwv = true;
  Ptr<PhasedAntennaArrayConfig> antennaConfig =
      StaticCast<PhasedAntennaArrayConfig>(m_antennaArrayList[m_antennaId]);
  Ptr<SectorConfig> sectorConfig =
      DynamicCast<SectorConfig>(antennaConfig->sectorList[m_txSectorId]);
  m_currentAwvList = &sectorConfig->awvList;
  m_currentAwvI = m_currentAwvList->begin() + awvId;
  m_txPattern = (*m_currentAwvI);
}

void RfChain::SetActiveRxAwvId(AwvId awvId) {
  NS_LOG_FUNCTION(this << +awvId);
  m_useAwv = true;
  Ptr<PhasedAntennaArrayConfig> antennaConfig =
      StaticCast<PhasedAntennaArrayConfig>(m_antennaArrayList[m_antennaId]);
  Ptr<SectorConfig> sectorConfig =
      DynamicCast<SectorConfig>(antennaConfig->sectorList[m_rxSectorId]);
  m_currentAwvList = &sectorConfig->awvList;
  m_currentAwvI = m_currentAwvList->begin() + awvId;
  m_rxPattern = (*m_currentAwvI);
}

void RfChain::SetActiveTxSectorId(SectorId sectorID) {
  NS_LOG_FUNCTION(this << +sectorID);
  m_txSectorId = sectorID;
  m_txPattern = m_antennaConfig->sectorList[sectorID];
  m_useAwv = false;
}

void RfChain::SetActiveRxSectorId(SectorId sectorID) {
  NS_LOG_FUNCTION(this << +sectorID);
  m_rxSectorId = sectorID;
  m_rxPattern = m_antennaConfig->sectorList[sectorID];
  m_useAwv = false;
}

void RfChain::SetActiveTxSectorId(AntennaId antennaID, SectorId sectorID) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID);
  m_antennaConfig = m_antennaArrayList[antennaID];
  m_antennaId = antennaID;
  m_txSectorId = sectorID;
  m_txPattern = m_antennaConfig->sectorList[sectorID];
  m_useAwv = false;
}

void RfChain::SetActiveRxSectorId(AntennaId antennaID, SectorId sectorID) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID);
  m_antennaConfig = m_antennaArrayList[antennaID];
  m_antennaId = antennaID;
  m_rxSectorId = sectorID;
  m_rxPattern = m_antennaConfig->sectorList[sectorID];
  m_useAwv = false;
}

SectorId RfChain::GetActiveTxSectorId() const { return m_txSectorId; }

SectorId RfChain::GetActiveRxSectorId() const { return m_rxSectorId; }

AntennaId RfChain::GetActiveAntennaId() const { return m_antennaId; }

uint8_t RfChain::GetNumberOfAwvs(AntennaId antennaID, SectorId sectorID) const {
  const auto it = m_antennaArrayList.find(antennaID);
  if (it != m_antennaArrayList.cend()) {
    Ptr<PhasedAntennaArrayConfig> config =
        StaticCast<PhasedAntennaArrayConfig>(it->second);
    const auto sectorIt = config->sectorList.find(sectorID);
    if (sectorIt != config->sectorList.cend()) {
      Ptr<SectorConfig> sectorConfig =
          StaticCast<SectorConfig>(sectorIt->second);
      return sectorConfig->awvList.size();
    } else {
      NS_FATAL_ERROR("Sector [" << +sectorID << "] does not exist");
    }
  } else {
    NS_FATAL_ERROR("Antenna [" << +antennaID << "] does not exist");
  }
}

uint8_t RfChain::GetActiveTxPatternId() const {
  if (!m_useAwv) {
    return GetActiveTxSectorId();
  } else {
    return m_currentAwvI - m_currentAwvList->begin();
  }
}

uint8_t RfChain::GetActiveRxPatternId() const {
  if (!m_useAwv) {
    return GetActiveRxSectorId();
  } else {
    return m_currentAwvI - m_currentAwvList->begin();
  }
}

Ptr<PatternConfig> RfChain::GetTxPatternConfig() const { return m_txPattern; }

Ptr<PatternConfig> RfChain::GetRxPatternConfig() const { return m_rxPattern; }

Ptr<PhasedAntennaArrayConfig> RfChain::GetAntennaArrayConfig() const {
  return m_antennaConfig;
}

bool RfChain::GetNextAwv() {
  NS_LOG_FUNCTION(this << m_currentAwvList->size());
  m_currentAwvI++;
  m_remainingAwvs--;
  if (m_currentAwvI == m_currentAwvList->end()) {
    m_currentAwvI = m_currentAwvList->begin();
    if (m_beamRefinementType == RefineTransmitSector) {
      m_txPattern = (*m_currentAwvI);
    } else {
      m_rxPattern = (*m_currentAwvI);
    }
    return true;
  } else {
    if (m_beamRefinementType == RefineTransmitSector) {
      m_txPattern = (*m_currentAwvI);
    } else {
      m_rxPattern = (*m_currentAwvI);
    }
    return true;
  }
  return false;
}

void RfChain::UseLastTxSector() {
  Ptr<SectorConfig> sectorConfig =
      DynamicCast<SectorConfig>(m_antennaConfig->sectorList[m_txSectorId]);
  m_txPattern = sectorConfig;
  m_useAwv = false;
}

void RfChain::UseCustomAwv(BeamRefinementType type) {
  Ptr<PhasedAntennaArrayConfig> antennaConfig =
      StaticCast<PhasedAntennaArrayConfig>(m_antennaArrayList[m_antennaId]);
  if (type == RefineTransmitSector) {
    Ptr<SectorConfig> sectorConfig =
        StaticCast<SectorConfig>(antennaConfig->sectorList[m_txSectorId]);
    m_currentAwvList = &sectorConfig->awvList;
    m_currentAwvI = m_currentAwvList->begin();
    m_txPattern = (*m_currentAwvI);
  } else {
    Ptr<SectorConfig> sectorConfig =
        StaticCast<SectorConfig>(antennaConfig->sectorList[m_rxSectorId]);
    m_currentAwvList = &sectorConfig->awvList;
    m_currentAwvI = m_currentAwvList->begin();
    m_rxPattern = (*m_currentAwvI);
  }
  m_beamRefinementType = type;
  m_useAwv = true;
}

bool RfChain::IsCustomAwvUsed() const { return m_useAwv; }

bool RfChain::IsQuasiOmniMode() const {
  NS_LOG_FUNCTION(this);
  return m_quasiOmniMode;
}

void RfChain::SetReceivingInQuasiOmniMode() {
  NS_LOG_FUNCTION(this);
  m_quasiOmniMode = true;
  m_useAwv = false;
  m_rxSectorId = 255;
  m_rxPattern = m_antennaConfig->GetQuasiOmniConfig();
}

void RfChain::SetReceivingInQuasiOmniMode(AntennaId antennaID) {
  NS_LOG_FUNCTION(this << +antennaID);
  m_quasiOmniMode = true;
  m_useAwv = false;
  m_rxSectorId = 255;
  m_rxPattern = m_antennaArrayList[antennaID]->GetQuasiOmniConfig();
  SetActiveAntennaId(antennaID);
}

void RfChain::StartReceivingInQuasiOmniMode() {
  NS_LOG_FUNCTION(this);
  m_quasiOmniMode = true;
  m_useAwv = false;
  m_quasiAntennaIter = m_antennaArrayList.begin();
  SetReceivingInQuasiOmniMode(m_quasiAntennaIter->first);
}

bool RfChain::SwitchToNextQuasiPattern() {
  NS_LOG_FUNCTION(this);
  m_quasiAntennaIter++;
  if (m_quasiAntennaIter == m_antennaArrayList.end()) {
    m_quasiAntennaIter = m_antennaArrayList.begin();
    SetReceivingInQuasiOmniMode(m_quasiAntennaIter->first);
    return false;
  } else {
    SetReceivingInQuasiOmniMode(m_quasiAntennaIter->first);
    return true;
  }
}

void RfChain::SetReceivingInDirectionalMode() {
  NS_LOG_FUNCTION(this);
  m_quasiOmniMode = false;
}

void RfChain::SetActiveAntennaId(AntennaId antennaID) {
  NS_LOG_FUNCTION(this << +antennaID);
  m_antennaConfig = m_antennaArrayList[antennaID];
  m_antennaId = antennaID;
}

void RfChain::InitiateBrp(AntennaId antennaID, SectorId sectorID,
                          BeamRefinementType type) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID << type);
  Ptr<PhasedAntennaArrayConfig> antennaConfig =
      StaticCast<PhasedAntennaArrayConfig>(m_antennaArrayList[antennaID]);
  Ptr<SectorConfig> sectorConfig =
      DynamicCast<SectorConfig>(antennaConfig->sectorList[sectorID]);
  NS_ASSERT_MSG(!sectorConfig->awvList.empty(),
                "Cannot initiate BRP or BT, because we have 0 custom AWVs.");
  NS_ASSERT_MSG(sectorConfig->awvList.size() % 4 == 0,
                "The number of AWVs should be multiple of 4.");
  m_useAwv = true;
  m_currentAwvList = &sectorConfig->awvList;
  m_currentAwvI = m_currentAwvList->begin();
  m_beamRefinementType = type;
  if (type == RefineTransmitSector) {
    m_txPattern = (*m_currentAwvI);
  } else {
    m_rxPattern = (*m_currentAwvI);
  }
}

void RfChain::StartSectorRefinement() {
  Ptr<PhasedAntennaArrayConfig> antennaConfig =
      StaticCast<PhasedAntennaArrayConfig>(m_antennaArrayList[m_antennaId]);
  Ptr<SectorConfig> sectorConfig =
      DynamicCast<SectorConfig>(antennaConfig->sectorList[m_rxSectorId]);
  if (!sectorConfig->awvList.empty()) {
    m_useAwv = true;
    m_currentAwvList = &sectorConfig->awvList;
    m_currentAwvI = m_currentAwvList->begin();
    m_beamRefinementType = RefineReceiveSector;
    m_rxPattern = (*m_currentAwvI);
    m_remainingAwvs = m_currentAwvList->size() - 1;
    NS_LOG_DEBUG("AWV Index=" << +GetActiveRxPatternId());
  }
}

uint8_t RfChain::GetRemainingAwvCount() const { return m_remainingAwvs; }

} // namespace ns3
