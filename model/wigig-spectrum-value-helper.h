/*
 * Copyright (c) 2015-2021 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_SPECTRUM_VALUE_HELPER_H
#define WIGIG_SPECTRUM_VALUE_HELPER_H

#include "ns3/wifi-phy-common.h"
#include "ns3/wifi-spectrum-value-helper.h"

namespace ns3 {

/**
 * \ingroup spectrum
 *
 *  This class defines all functions to create a spectrum model for Wigig
 * (802.11ad).
 */
class WigigSpectrumValueHelper : public WifiSpectrumValueHelper {
public:
  /**
   * Destructor
   */
  ~WigigSpectrumValueHelper() override;

  static Ptr<SpectrumValue> CreateRfFilter(uint32_t centerFrequency,
                                           uint16_t channelWidth,
                                           uint32_t bandBandwidth,
                                           uint16_t guardBandwidth);

  /**
   * CreateSingleCarrierTxPowerSpectralDensity
   * \param centerFrequency center frequency (MHz)
   * \param channelWidth channel width (MHz)
   * \param txPowerW  transmit power (W) to allocate
   * \param guardBandwidth width of the guard band (MHz)
   * \return a pointer to a newly allocated SpectrumValue representing the Wigig
   * Contrn and SC Transmit Power Spectral Density in W/Hz for each Band.
   */
  static Ptr<SpectrumValue> CreateSingleCarrierTxPowerSpectralDensity(
      uint32_t centerFrequency, double txPowerW, double guardBandwidth,
      uint8_t ncb = 1);
  /**
   * CreateDmgOfdmTxPowerSpectralDensity
   * \param centerFrequency center frequency (MHz)
   * \param txPowerW  transmit power (W) to allocate
   * \param guardBandwidth width of the guard band (MHz)
   * \return a pointer to a newly allocated SpectrumValue representing the Wigig
   * OFDM Transmit Power Spectral Density in W/Hz for each Band.
   */
  static Ptr<SpectrumValue>
  CreateDmgOfdmTxPowerSpectralDensity(uint32_t centerFrequency, double txPowerW,
                                      double guardBandwidth);

  /**
   * Create a transmit power spectral density corresponding to OFDM
   * transmit spectrum mask requirements for 11a/11g/11n/11ac/11ax
   * Channel width may vary between 5, 10, 20, 40, 80, and 160 MHz.
   * The default (standard) values are illustrated below.
   *
   *   [ guard band  ][    channel width     ][  guard band ]
   *                   __________   __________                  _ 0 dBr
   *                  /          | |          \
   *                 /           |_|           \                _ -20 dBr
   *             . '                             ' .
   *         . '                                     ' .        _ -28 dBr
   *       .'                                           '.
   *     .'                                               '.
   *   .'                                                   '.  _ lowest point
   *
   *   |-----|                                         |-----|  outerBand
   * left/right
   *         |------|                           |-- ---|        middle band
   * left/right
   *                |-|                       |-|               inner band
   * left/right
   *                  |-----------------------|                 allocated
   * sub-bands
   *   |-----------------------------------------------------|  mask band
   *
   * Please take note that, since guard tones are within the allocated band
   * while not being ideally allocated any power, the inner band had to be
   * shifted inwards and a flat junction band (at -20 dBr) had to be added
   * between the inner and the middle bands.
   *
   * \param c spectrumValue to allocate according to transmit power spectral
   * density mask (in W/Hz for each band) \param allocatedSubBands vector of
   * start and stop subcarrier indexes of the allocated sub bands \param
   * maskBand start and stop subcarrier indexes of transmit mask (in case signal
   * doesn't cover whole SpectrumModel) \param txPowerPerBandW power allocated
   * to each subcarrier in the allocated sub bands \param nGuardBands size (in
   * number of subcarriers) of the guard band (left and right) \param
   * innerSlopeWidth size (in number of subcarriers) of the inner band (i.e.
   * slope going from 0 dBr to -20 dBr in the figure above) \param
   * minInnerBandDbr the minimum relative power in the inner band (i.e. -20 dBr
   * in the figure above) \param minOuterbandDbr the minimum relative power in
   * the outer band (i.e. -28 dBr in the figure above) \param lowestPointDbr
   * maximum relative power of the outermost subcarriers of the guard band (in
   * dBr)
   */
  static void
  CreateSpectrumMask(Ptr<SpectrumValue> c,
                     std::vector<WifiSpectrumBandIndices> allocatedSubBands,
                     WifiSpectrumBandIndices maskBand, double txPowerPerBandW,
                     uint32_t nGuardBands, uint32_t flatJunctionWidth,
                     uint32_t innerSlopeWidth, uint32_t middleSlopeWidth,
                     uint32_t outerSlopeWidth);
};

} // namespace ns3

#endif /*  WIGIG_SPECTRUM_VALUE_HELPER_H */
