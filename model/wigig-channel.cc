/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-channel.h"

#include "codebook.h"
#include "wigig-ppdu.h"
#include "wigig-psdu.h"

#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/net-device.h"
#include "ns3/node.h"
#include "ns3/pointer.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/simulator.h"
#include "ns3/wifi-utils.h"

#include <fstream>

namespace ns3 {

namespace {

double CalculateAzimuthAngle(const Vector3D &a, const Vector3D &b) {
  double dx = b.x - a.x;
  double dy = b.y - a.y;
  double azimuth = std::atan2(dy, dx);
  return azimuth;
}

} // anonymous namespace

NS_LOG_COMPONENT_DEFINE("WigigChannel");

NS_OBJECT_ENSURE_REGISTERED(WigigChannel);

TypeId WigigChannel::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigChannel")
          .SetParent<Channel>()
          .SetGroupName("Wigig")
          .AddConstructor<WigigChannel>()
          .AddAttribute("PropagationLossModel",
                        "A pointer to the propagation loss model attached to "
                        "this channel.",
                        PointerValue(),
                        MakePointerAccessor(&WigigChannel::m_loss),
                        MakePointerChecker<PropagationLossModel>())
          .AddAttribute("PropagationDelayModel",
                        "A pointer to the propagation delay model attached to "
                        "this channel.",
                        PointerValue(),
                        MakePointerAccessor(&WigigChannel::m_delay),
                        MakePointerChecker<PropagationDelayModel>())
          .AddTraceSource(
              "PhyActivityTracker",
              "Trace source for transmitting/receiving PLCP field (PHY "
              "Tracker).",
              MakeTraceSourceAccessor(&WigigChannel::m_phyActivityTrace),
              "ns3::WigigChannel::PhyActivityTracedCallback");
  return tid;
}

WigigChannel::WigigChannel()
    : m_blockage(nullptr), m_srcWigigPhy(nullptr), m_dstWigigPhy(nullptr) {
  NS_LOG_FUNCTION(this);
}

WigigChannel::~WigigChannel() { NS_LOG_FUNCTION(this); }

void WigigChannel::SetPropagationLossModel(
    const Ptr<PropagationLossModel> loss) {
  NS_LOG_FUNCTION(this << loss);
  m_loss = loss;
}

void WigigChannel::SetPropagationDelayModel(
    const Ptr<PropagationDelayModel> delay) {
  NS_LOG_FUNCTION(this << delay);
  m_delay = delay;
}

void WigigChannel::AddBlockage(double (*blockage)(), Ptr<WigigPhy> srcWigigPhy,
                               Ptr<WigigPhy> dstWigigPhy) {
  m_blockage = blockage;
  m_srcWigigPhy = srcWigigPhy;
  m_dstWigigPhy = dstWigigPhy;
}

void WigigChannel::RemoveBlockage() {
  m_blockage = nullptr;
  m_srcWigigPhy = nullptr;
  m_dstWigigPhy = nullptr;
}

void WigigChannel::RecordPhyActivity(uint32_t srcID, uint32_t dstID,
                                     Time duration, double power,
                                     PlcpFieldType fieldType,
                                     ActivityType activityType) const {
  m_phyActivityTrace(srcID, dstID, duration, power, fieldType, activityType);
}

void WigigChannel::Send(Ptr<WigigPhy> sender, Ptr<const WigigPpdu> ppdu,
                        double txPowerDbm) const {
  NS_LOG_FUNCTION(this << sender << ppdu << txPowerDbm);
  Ptr<MobilityModel> senderMobility = sender->GetMobility();
  NS_ASSERT(senderMobility);
  for (PhyList::const_iterator i = m_phyList.begin(); i != m_phyList.end();
       i++) {
    if (sender != (*i)) {
      // For now don't account for inter channel interference nor channel
      // bonding
      if ((*i)->GetChannelNumber() != sender->GetChannelNumber()) {
        continue;
      }

      Vector sender_pos = senderMobility->GetPosition();
      Ptr<Codebook> senderCodebook = sender->GetCodebook();
      Ptr<MobilityModel> receiverMobility =
          (*i)->GetMobility()->GetObject<MobilityModel>();
      Time delay = m_delay->GetDelay(senderMobility, receiverMobility);
      double rxPowerDbm;
      double azimuthTx =
          CalculateAzimuthAngle(sender_pos, receiverMobility->GetPosition());
      double azimuthRx =
          CalculateAzimuthAngle(receiverMobility->GetPosition(), sender_pos);
      double gtx = senderCodebook->GetTxGainDbi(
          azimuthTx); // Sender's antenna gain in dBi.
      double grx = (*i)->GetCodebook()->GetRxGainDbi(
          azimuthRx); // Receiver's antenna gain in dBi.

      NS_LOG_DEBUG(
          "POWER: azimuthTx="
          << azimuthTx << ", azimuthRx=" << azimuthRx
          << ", txPowerDbm=" << txPowerDbm << ", RxPower="
          << m_loss->CalcRxPower(txPowerDbm, senderMobility, receiverMobility)
          << ", Gtx=" << gtx << ", Grx=" << grx);

      rxPowerDbm =
          m_loss->CalcRxPower(txPowerDbm, senderMobility, receiverMobility) +
          gtx + grx;

      /* External Attenuator */
      if (m_blockage && ((m_srcWigigPhy == sender && m_dstWigigPhy == (*i)) ||
                         (m_srcWigigPhy == (*i) && m_dstWigigPhy == sender))) {
        rxPowerDbm += m_blockage();
        NS_LOG_DEBUG("RxPower [dBm] with blockage=" << rxPowerDbm);
      }

      NS_LOG_DEBUG("propagation: txPower="
                   << txPowerDbm << "dbm, rxPower=" << rxPowerDbm << "dbm, "
                   << "distance="
                   << senderMobility->GetDistanceFrom(receiverMobility)
                   << "m, delay=" << delay);
      Ptr<WigigPpdu> copy = Copy(ppdu);
      Ptr<NetDevice> dstNetDevice = (*i)->GetDevice();
      uint32_t dstNode =
          dstNetDevice ? dstNetDevice->GetNode()->GetId() : 0xffffffff;

      Simulator::ScheduleWithContext(dstNode, delay, &WigigChannel::Receive,
                                     (*i), copy, rxPowerDbm);

      /* PHY Activity Monitor */
      uint32_t srcNode = sender->GetDevice()->GetNode()->GetId();
      RecordPhyActivity(srcNode, dstNode, ppdu->GetTxDuration(),
                        txPowerDbm + gtx, PLCP_80211AD_PREAMBLE_HDR_DATA,
                        TX_ACTIVITY);
      Simulator::Schedule(delay, &WigigChannel::RecordPhyActivity, this,
                          srcNode, dstNode, ppdu->GetTxDuration(), rxPowerDbm,
                          PLCP_80211AD_PREAMBLE_HDR_DATA, RX_ACTIVITY);
    }
  }
}

void WigigChannel::SendAgcSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                                   const WigigTxVector &txVector) const {
  NS_LOG_FUNCTION(this << sender << txPowerDbm << txVector);
  Ptr<MobilityModel> senderMobility =
      sender->GetMobility()->GetObject<MobilityModel>();
  NS_ASSERT(senderMobility);
  Ptr<MobilityModel> receiverMobility;
  uint32_t j = 0; /* Phy ID */
  Time delay;     /* Propagation delay of the signal */
  for (PhyList::const_iterator i = m_phyList.begin(); i != m_phyList.end();
       i++, j++) {
    if (sender != (*i)) {
      // For now don't account for inter channel interference.
      if ((*i)->GetChannelNumber() != sender->GetChannelNumber()) {
        continue;
      }

      receiverMobility = (*i)->GetMobility()->GetObject<MobilityModel>();
      delay = m_delay->GetDelay(senderMobility, receiverMobility);
      Ptr<Codebook> senderCodebook = sender->GetCodebook();
      double azimuthTx = CalculateAzimuthAngle(senderMobility->GetPosition(),
                                               receiverMobility->GetPosition());
      double gtx = senderCodebook->GetTxGainDbi(azimuthTx);

      Ptr<Object> dstNetDevice = m_phyList[j]->GetDevice();
      /* Destination node (Receiver) */
      uint32_t dstNode =
          dstNetDevice
              ? dstNetDevice->GetObject<NetDevice>()->GetNode()->GetId()
              : 0xffffffff;

      /* PHY Activity Monitor */
      RecordPhyActivity(sender->GetDevice()->GetNode()->GetId(), dstNode,
                        AGC_SF_DURATION, txPowerDbm + gtx, PLCP_80211AD_AGC_SF,
                        TX_ACTIVITY);
      Simulator::ScheduleWithContext(dstNode, delay,
                                     &WigigChannel::ReceiveAgcSubfield, this, j,
                                     sender, txVector, txPowerDbm, gtx);
    }
  }
}

void WigigChannel::SendTrnCeSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                                     const WigigTxVector &txVector) const {
  NS_LOG_FUNCTION(this << sender << txPowerDbm << txVector);
  Ptr<MobilityModel> senderMobility =
      sender->GetMobility()->GetObject<MobilityModel>();
  NS_ASSERT(senderMobility);
  Ptr<MobilityModel> receiverMobility;
  uint32_t j = 0; /* Phy ID */
  Time delay;     /* Propagation delay of the signal */
  for (PhyList::const_iterator i = m_phyList.begin(); i != m_phyList.end();
       i++, j++) {
    if (sender != (*i)) {
      // For now don't account for inter channel interference.
      if ((*i)->GetChannelNumber() != sender->GetChannelNumber()) {
        continue;
      }

      receiverMobility = (*i)->GetMobility()->GetObject<MobilityModel>();
      delay = m_delay->GetDelay(senderMobility, receiverMobility);
      Ptr<Codebook> senderCodebook = sender->GetCodebook();
      double azimuthTx = CalculateAzimuthAngle(senderMobility->GetPosition(),
                                               receiverMobility->GetPosition());
      double gtx = senderCodebook->GetTxGainDbi(azimuthTx);

      Ptr<Object> dstNetDevice = m_phyList[j]->GetDevice();
      /* Destination node (Receiver) */
      uint32_t dstNode =
          dstNetDevice
              ? dstNetDevice->GetObject<NetDevice>()->GetNode()->GetId()
              : 0xffffffff;

      /* PHY Activity Monitor */
      RecordPhyActivity(sender->GetDevice()->GetNode()->GetId(), dstNode,
                        TRN_CE_DURATION, txPowerDbm + gtx,
                        PLCP_80211AD_TRN_CE_SF, TX_ACTIVITY);
      Simulator::ScheduleWithContext(dstNode, delay,
                                     &WigigChannel::ReceiveTrnCeSubfield, this,
                                     j, sender, txVector, txPowerDbm, gtx);
    }
  }
}

void WigigChannel::SendTrnSubfield(Ptr<WigigPhy> sender, double txPowerDbm,
                                   const WigigTxVector &txVector) const {
  NS_LOG_FUNCTION(this << sender << txPowerDbm << txVector);
  Ptr<MobilityModel> senderMobility =
      sender->GetMobility()->GetObject<MobilityModel>();
  NS_ASSERT(senderMobility);
  Ptr<MobilityModel> receiverMobility;
  uint32_t j = 0; /* Phy ID */
  Time delay;     /* Propagation delay of the signal */
  for (PhyList::const_iterator i = m_phyList.begin(); i != m_phyList.end();
       i++, j++) {
    if (sender != (*i)) {
      // For now don't account for inter-channel interference.
      if ((*i)->GetChannelNumber() != sender->GetChannelNumber()) {
        continue;
      }

      receiverMobility = (*i)->GetMobility()->GetObject<MobilityModel>();
      delay = m_delay->GetDelay(senderMobility, receiverMobility);
      Ptr<Codebook> senderCodebook = sender->GetCodebook();
      double azimuthTx = CalculateAzimuthAngle(senderMobility->GetPosition(),
                                               receiverMobility->GetPosition());
      double gtx = senderCodebook->GetTxGainDbi(azimuthTx);

      Ptr<Object> dstNetDevice = m_phyList[j]->GetDevice();
      /* Destination node (Receiver) */
      uint32_t dstNode =
          dstNetDevice
              ? dstNetDevice->GetObject<NetDevice>()->GetNode()->GetId()
              : 0xffffffff;

      /* PHY Activity Monitor */
      RecordPhyActivity(sender->GetDevice()->GetNode()->GetId(), dstNode,
                        TRN_SUBFIELD_DURATION, txPowerDbm + gtx,
                        PLCP_80211AD_TRN_SF, TX_ACTIVITY);

      Simulator::ScheduleWithContext(dstNode, delay,
                                     &WigigChannel::ReceiveTrnSubfield, this, j,
                                     sender, txVector, txPowerDbm, gtx);
    }
  }
}

void WigigChannel::Receive(Ptr<WigigPhy> phy, Ptr<WigigPpdu> ppdu,
                           double rxPowerDbm) {
  NS_LOG_FUNCTION(phy << ppdu << rxPowerDbm);
  // Do no further processing if signal is too weak
  // Current implementation assumes constant RX power over the PPDU duration
  if ((rxPowerDbm + phy->GetRxGain()) < phy->GetRxSensitivity()) {
    NS_LOG_INFO("Received signal too weak to process: " << rxPowerDbm
                                                        << " dBm");
    return;
  }
  std::vector<double> rxPowerW;
  rxPowerW.push_back(DbmToW(rxPowerDbm + phy->GetRxGain()));
  phy->StartReceivePreamble(ppdu, rxPowerW);
}

double WigigChannel::ReceiveSubfield(uint32_t i, Ptr<WigigPhy> sender,
                                     const WigigTxVector &txVector,
                                     double txPowerDbm, double txAntennaGainDbi,
                                     Time duration, PlcpFieldType type) const {
  NS_LOG_FUNCTION(this << i << sender << txVector << txPowerDbm
                       << txAntennaGainDbi);
  /* Calculate SNR upon the reception of the TRN Field */
  Ptr<MobilityModel> senderMobility =
      sender->GetMobility()->GetObject<MobilityModel>();
  Ptr<MobilityModel> receiverMobility =
      m_phyList[i]->GetMobility()->GetObject<MobilityModel>();
  NS_ASSERT((senderMobility) && (receiverMobility));
  double azimuthRx = CalculateAzimuthAngle(receiverMobility->GetPosition(),
                                           senderMobility->GetPosition());
  double rxPowerDbm;

  NS_LOG_DEBUG("POWER: Gtx="
               << txAntennaGainDbi << ", Grx="
               << m_phyList[i]->GetCodebook()->GetRxGainDbi(azimuthRx));

  rxPowerDbm =
      m_loss->CalcRxPower(txPowerDbm, senderMobility, receiverMobility) +
      txAntennaGainDbi + // Sender's antenna gain.
      m_phyList[i]->GetCodebook()->GetRxGainDbi(
          azimuthRx); // Receiver's antenna gain.

  /* PHY Activity Monitor */
  RecordPhyActivity(sender->GetDevice()->GetNode()->GetId(),
                    m_phyList[i]->GetDevice()->GetNode()->GetId(), duration,
                    rxPowerDbm, type, RX_ACTIVITY);

  /* External Attenuator */
  if ((m_blockage) && (m_srcWigigPhy == sender) &&
      (m_dstWigigPhy == m_phyList[i])) {
    rxPowerDbm += m_blockage();
  }

  NS_LOG_DEBUG("propagation: txPower=" << txPowerDbm << "dbm, rxPower="
                                       << rxPowerDbm << "dbm");

  return rxPowerDbm;
}

void WigigChannel::ReceiveAgcSubfield(uint32_t i, Ptr<WigigPhy> sender,
                                      const WigigTxVector &txVector,
                                      double txPowerDbm,
                                      double txAntennaGainDbi) const {
  NS_LOG_FUNCTION(this << i << sender << txVector << txPowerDbm
                       << txAntennaGainDbi);
  double rxPowerDbm =
      ReceiveSubfield(i, sender, txVector, txPowerDbm, txAntennaGainDbi,
                      AGC_SF_DURATION, PLCP_80211AD_AGC_SF);
  m_phyList[i]->StartReceiveAgcSubfield(txVector, rxPowerDbm);
}

void WigigChannel::ReceiveTrnCeSubfield(uint32_t i, Ptr<WigigPhy> sender,
                                        const WigigTxVector &txVector,
                                        double txPowerDbm,
                                        double txAntennaGainDbi) const {
  NS_LOG_FUNCTION(this << i << sender << txVector << txPowerDbm
                       << txAntennaGainDbi);
  double rxPowerDbm =
      ReceiveSubfield(i, sender, txVector, txPowerDbm, txAntennaGainDbi,
                      TRN_CE_DURATION, PLCP_80211AD_TRN_CE_SF);
  m_phyList[i]->StartReceiveCeSubfield(txVector, rxPowerDbm);
}

void WigigChannel::ReceiveTrnSubfield(uint32_t i, Ptr<WigigPhy> sender,
                                      const WigigTxVector &txVector,
                                      double txPowerDbm,
                                      double txAntennaGainDbi) const {
  NS_LOG_FUNCTION(this << i << sender << txVector << txPowerDbm
                       << txAntennaGainDbi);
  double rxPowerDbm;
  rxPowerDbm =
      ReceiveSubfield(i, sender, txVector, txPowerDbm, txAntennaGainDbi,
                      TRN_SUBFIELD_DURATION, PLCP_80211AD_TRN_SF);
  /* Report the received SNR to the higher layers. */
  m_phyList[i]->StartReceiveTrnSubfield(txVector, rxPowerDbm);
}

std::size_t WigigChannel::GetNDevices() const { return m_phyList.size(); }

Ptr<NetDevice> WigigChannel::GetDevice(std::size_t i) const {
  return m_phyList[i]->GetDevice()->GetObject<NetDevice>();
}

void WigigChannel::Add(Ptr<WigigPhy> phy) {
  NS_LOG_FUNCTION(this << phy);
  m_phyList.push_back(phy);
}

int64_t WigigChannel::AssignStreams(int64_t stream) {
  NS_LOG_FUNCTION(this << stream);
  int64_t currentStream = stream;
  currentStream += m_loss->AssignStreams(stream);
  return (currentStream - stream);
}

} // namespace ns3
