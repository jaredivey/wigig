/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Ghada Badawy <gbadawy@gmail.com>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 *
 * Ported from yans-wifi-phy.cc by several contributors starting
 * with Nicola Baldo and Dean Armstrong
 */

#include "spectrum-wigig-phy.h"

#include "wigig-ppdu.h"
#include "wigig-psdu.h"
#include "wigig-spectrum-phy-interface.h"

#include "ns3/abort.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/net-device.h"
#include "ns3/node.h"
#include "ns3/wifi-utils.h"
#include "ns3/wigig-spectrum-value-helper.h"

#include <algorithm>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("SpectrumWigigPhy");

NS_OBJECT_ENSURE_REGISTERED(SpectrumWigigPhy);

WigigSpectrumSignalParameters::WigigSpectrumSignalParameters() {
  NS_LOG_FUNCTION(this);
}

WigigSpectrumSignalParameters::WigigSpectrumSignalParameters(
    const WigigSpectrumSignalParameters &p)
    : SpectrumSignalParameters(p) {
  NS_LOG_FUNCTION(this << &p);
  ppdu = p.ppdu;
  plcpFieldType = p.plcpFieldType;
  txVector = p.txVector;
  antennaId = p.antennaId;
  txPatternConfig = p.txPatternConfig;
}

Ptr<SpectrumSignalParameters> WigigSpectrumSignalParameters::Copy() const {
  NS_LOG_FUNCTION(this);
  // Ideally we would use:
  //   return Copy<WifiSpectrumSignalParametersCopy> (*this);
  // but for some reason it doesn't work. Another alternative is
  //   return Copy<WifiSpectrumSignalParametersCopy> (this);
  // but it causes a double creation of the object, hence it is less efficient.
  // The solution below is copied from the implementation of Copy<> (Ptr<>) in
  // ptr.h
  Ptr<WigigSpectrumSignalParameters> wssp(
      new WigigSpectrumSignalParameters(*this), false);
  return wssp;
}

TypeId SpectrumWigigPhy::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::SpectrumWigigPhy")
          .SetParent<WigigPhy>()
          .SetGroupName("Wigig")
          .AddConstructor<SpectrumWigigPhy>()
          .AddAttribute(
              "DisableWifiReception",
              "Prevent Wi-Fi frame sync from ever happening",
              BooleanValue(false),
              MakeBooleanAccessor(&SpectrumWigigPhy::m_disableWifiReception),
              MakeBooleanChecker())
          .AddTraceSource(
              "SignalArrival", "Signal arrival",
              MakeTraceSourceAccessor(&SpectrumWigigPhy::m_signalCb),
              "ns3::SpectrumWigigPhy::SignalArrivalCallback");
  return tid;
}

SpectrumWigigPhy::SpectrumWigigPhy() { NS_LOG_FUNCTION(this); }

SpectrumWigigPhy::~SpectrumWigigPhy() { NS_LOG_FUNCTION(this); }

void SpectrumWigigPhy::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_channel = nullptr;
  m_wifiSpectrumPhyInterface = nullptr;
  WigigPhy::DoDispose();
}

void SpectrumWigigPhy::DoInitialize() {
  NS_LOG_FUNCTION(this);
  WigigPhy::DoInitialize();
  // This connection is deferred until frequency and channel width are set
  if (m_channel && m_wifiSpectrumPhyInterface) {
    m_channel->AddRx(m_wifiSpectrumPhyInterface);
  } else {
    NS_FATAL_ERROR("SpectrumWigigPhy misses channel and "
                   "WifiSpectrumPhyInterfaceCopy objects "
                   "at initialization time");
  }
}

Ptr<const SpectrumModel> SpectrumWigigPhy::GetRxSpectrumModel() const {
  NS_LOG_FUNCTION(this);
  if (m_rxSpectrumModel) {
    return m_rxSpectrumModel;
  } else {
    if (GetFrequency() == 0) {
      NS_LOG_DEBUG("Frequency is not set; returning 0");
      return nullptr;
    } else {
      uint16_t channelWidth = GetChannelWidth();
      NS_LOG_DEBUG("Creating spectrum model from frequency/width pair of ("
                   << GetFrequency() << ", " << channelWidth << ")");
      m_rxSpectrumModel = WifiSpectrumValueHelper::GetSpectrumModel(
          GetFrequency(), channelWidth, WIGIG_OFDM_SUBCARRIER_SPACING,
          GetGuardBandwidth());
    }
  }
  return m_rxSpectrumModel;
}

Ptr<Channel> SpectrumWigigPhy::GetChannel() const { return m_channel; }

void SpectrumWigigPhy::SetChannel(const Ptr<SpectrumChannel> channel) {
  m_channel = channel;
}

void SpectrumWigigPhy::ResetSpectrumModel() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(IsInitialized(), "Executing method before run-time");
  uint16_t channelWidth = GetChannelWidth();
  NS_LOG_DEBUG(
      "Run-time change of spectrum model from frequency/width pair of ("
      << GetFrequency() << ", " << channelWidth << ")");
  // Replace existing spectrum model with new one, and must call AddRx ()
  // on the SpectrumChannel to provide this new spectrum model to it
  m_rxSpectrumModel = WifiSpectrumValueHelper::GetSpectrumModel(
      GetFrequency(), channelWidth, WIGIG_OFDM_SUBCARRIER_SPACING,
      GetGuardBandwidth());
  m_channel->AddRx(m_wifiSpectrumPhyInterface);
}

void SpectrumWigigPhy::SetChannelNumber(uint8_t nch) {
  NS_LOG_FUNCTION(this << +nch);
  WigigPhy::SetChannelNumber(nch);
  if (IsInitialized()) {
    ResetSpectrumModel();
  }
}

void SpectrumWigigPhy::SetFrequency(uint16_t freq) {
  NS_LOG_FUNCTION(this << freq);
  WigigPhy::SetFrequency(freq);
  if (IsInitialized()) {
    ResetSpectrumModel();
  }
}

void SpectrumWigigPhy::SetChannelWidth(uint16_t channelwidth) {
  NS_LOG_FUNCTION(this << channelwidth);
  WigigPhy::SetChannelWidth(channelwidth);
  if (IsInitialized()) {
    ResetSpectrumModel();
  }
}

void SpectrumWigigPhy::ConfigureStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  WigigPhy::ConfigureStandard(standard);
  if (IsInitialized()) {
    ResetSpectrumModel();
  }
}

double SpectrumWigigPhy::FilterSignal(Ptr<SpectrumValue> filter,
                                      Ptr<SpectrumValue> receivedSignalPsd) {
  SpectrumValue filteredSignal = (*filter) * (*receivedSignalPsd);
  // Add receiver antenna gain
  NS_LOG_DEBUG("Signal power received (watts) before antenna gain: "
               << Integral(filteredSignal));
  double rxPowerW = Integral(filteredSignal) * DbToRatio(GetRxGain());
  NS_LOG_DEBUG("Signal power received after antenna gain: "
               << rxPowerW << " W (" << WToDbm(rxPowerW) << " dBm)");
  return rxPowerW;
}

void SpectrumWigigPhy::StartRx(Ptr<SpectrumSignalParameters> rxParams) {
  NS_LOG_FUNCTION(this << rxParams);
  Time rxDuration = rxParams->duration;
  Ptr<SpectrumValue> receivedSignalPsd = rxParams->psd;
  uint32_t senderNodeId = 0;
  if (rxParams->txPhy) {
    senderNodeId = rxParams->txPhy->GetDevice()->GetNode()->GetId();
  }
  NS_LOG_DEBUG("Received signal from "
               << senderNodeId << " with unfiltered power "
               << WToDbm(Integral(*receivedSignalPsd)) << " dBm");
  // Integrate over our receive bandwidth (i.e., all that the receive
  // spectral mask representing our filtering allows) to find the
  // total energy apparent to the "demodulator".
  uint16_t channelWidth = GetChannelWidth();
  Ptr<SpectrumValue> filter = WigigSpectrumValueHelper::CreateRfFilter(
      GetFrequency(), channelWidth, WIGIG_OFDM_SUBCARRIER_SPACING,
      GetGuardBandwidth());
  double rxPowerW = FilterSignal(filter, receivedSignalPsd);
  Ptr<WigigSpectrumSignalParameters> wifiRxParams =
      DynamicCast<WigigSpectrumSignalParameters>(rxParams);

  // Log the signal arrival to the trace source
  m_signalCb(static_cast<bool>(wifiRxParams), senderNodeId, WToDbm(rxPowerW),
             rxDuration);

  // Do no further processing if signal is too weak
  // Current implementation assumes constant RX power over the PPDU duration
  if (WToDbm(rxPowerW) < GetRxSensitivity()) {
    NS_LOG_INFO("Received signal too weak to process: " << WToDbm(rxPowerW)
                                                        << " dBm");
    return;
  }
  if (!wifiRxParams) {
    NS_LOG_INFO("Received non Wi-Fi signal");
    m_interference.AddForeignSignal(rxDuration, rxPowerW);
    SwitchMaybeToCcaBusy();
    return;
  }
  if (wifiRxParams && m_disableWifiReception) {
    NS_LOG_INFO("Received Wi-Fi signal but blocked from syncing");
    m_interference.AddForeignSignal(rxDuration, rxPowerW);
    SwitchMaybeToCcaBusy();
    return;
  }

  if (wifiRxParams->plcpFieldType == PLCP_80211AD_PREAMBLE_HDR_DATA) {
    NS_LOG_INFO("Received DMG WiFi signal");
    Ptr<WigigPpdu> ppdu = Copy(wifiRxParams->ppdu);
    StartReceivePreamble(ppdu, {rxPowerW});
  } else if (wifiRxParams->plcpFieldType == PLCP_80211AD_AGC_SF) {
    NS_LOG_INFO("Received DMG WiFi AGC-SF signal");
    StartReceiveAgcSubfield(wifiRxParams->txVector, WToDbm(rxPowerW));
  } else if (wifiRxParams->plcpFieldType == PLCP_80211AD_TRN_CE_SF) {
    NS_LOG_INFO("Received DMG WiFi TRN-CE Subfield signal");
    StartReceiveCeSubfield(wifiRxParams->txVector, WToDbm(rxPowerW));
  } else if (wifiRxParams->plcpFieldType == PLCP_80211AD_TRN_SF) {
    NS_LOG_INFO("Received DMG WiFi TRN-SF signal");
    StartReceiveTrnSubfield(wifiRxParams->txVector, WToDbm(rxPowerW));
  }
}

Ptr<WigigSpectrumPhyInterface> SpectrumWigigPhy::GetSpectrumPhy() const {
  return m_wifiSpectrumPhyInterface;
}

void SpectrumWigigPhy::CreateWifiSpectrumPhyInterface(Ptr<NetDevice> device) {
  NS_LOG_FUNCTION(this << device);
  m_wifiSpectrumPhyInterface = CreateObject<WigigSpectrumPhyInterface>();
  m_wifiSpectrumPhyInterface->SetSpectrumWigigPhy(this);
  m_wifiSpectrumPhyInterface->SetDevice(device);
}

Ptr<SpectrumValue> SpectrumWigigPhy::GetTxPowerSpectralDensity(
    uint16_t centerFrequency, uint16_t channelWidth, double txPowerW,
    WifiModulationClass modulationClass) const {
  Ptr<SpectrumValue> v;
  switch (modulationClass) {
  case WIFI_MOD_CLASS_DMG_CTRL:
  case WIFI_MOD_CLASS_DMG_SC:
  case WIFI_MOD_CLASS_DMG_LP_SC:
    NS_ASSERT(channelWidth == 2160);
    v = WigigSpectrumValueHelper::CreateSingleCarrierTxPowerSpectralDensity(
        centerFrequency, txPowerW, GetGuardBandwidth());
    break;
  case WIFI_MOD_CLASS_DMG_OFDM:
    NS_ASSERT(channelWidth == 2160);
    // Create one different function for every different case
    v = WigigSpectrumValueHelper::CreateDmgOfdmTxPowerSpectralDensity(
        centerFrequency, txPowerW, GetGuardBandwidth());
    break;
  default:
    NS_FATAL_ERROR("modulation class unknown: " << modulationClass);
    break;
  }
  return v;
}

uint16_t SpectrumWigigPhy::GetCenterFrequencyForChannelWidth(
    const WigigTxVector &txVector) const {
  uint16_t centerFrequencyForSupportedWidth = GetFrequency();
  uint16_t supportedWidth = GetChannelWidth();
  uint16_t currentWidth = txVector.GetChannelWidth();
  if (currentWidth != supportedWidth) {
    uint16_t startingFrequency =
        centerFrequencyForSupportedWidth - (supportedWidth / 2);
    return startingFrequency +
           (currentWidth /
            2); // primary channel is in the lower part (for the time being)
  }
  return centerFrequencyForSupportedWidth;
}

void SpectrumWigigPhy::StartTx(Ptr<WigigPpdu> ppdu) {
  NS_LOG_FUNCTION(this << ppdu);
  WigigTxVector txVector = ppdu->GetTxVector();
  double txPowerDbm = GetTxPowerForTransmission(txVector) + GetTxGain();
  NS_LOG_DEBUG("Start transmission: signal power before antenna array="
               << txPowerDbm << "dBm");
  double txPowerWatts = DbmToW(txPowerDbm);
  Ptr<SpectrumValue> txPowerSpectrum = GetTxPowerSpectralDensity(
      GetCenterFrequencyForChannelWidth(txVector), txVector.GetChannelWidth(),
      txPowerWatts, txVector.GetMode().GetModulationClass());
  Ptr<WigigSpectrumSignalParameters> txParams =
      Create<WigigSpectrumSignalParameters>();
  txParams->duration = ppdu->GetTxDuration();
  txParams->psd = txPowerSpectrum;
  NS_ASSERT_MSG(m_wifiSpectrumPhyInterface,
                "SpectrumPhy() is not set; maybe forgot to call "
                "CreateWifiSpectrumPhyInterface?");
  txParams->txPhy = m_wifiSpectrumPhyInterface->GetObject<SpectrumPhy>();
  txParams->ppdu = ppdu;
  txParams->plcpFieldType = PLCP_80211AD_PREAMBLE_HDR_DATA;
  txParams->txVector = txVector;
  txParams->antennaId = GetCodebook()->GetActiveAntennaId();
  txParams->txPatternConfig = GetCodebook()->GetTxPatternConfig();
  NS_LOG_DEBUG("Starting transmission with power " << WToDbm(txPowerWatts)
                                                   << " dBm on channel "
                                                   << +GetChannelNumber());
  NS_LOG_DEBUG("Starting transmission with integrated spectrum power "
               << WToDbm(Integral(*txPowerSpectrum))
               << " dBm; spectrum model Uid: "
               << txPowerSpectrum->GetSpectrumModel()->GetUid());
  m_channel->StartTx(txParams);
}

void SpectrumWigigPhy::TxSubfield(const WigigTxVector &txVector,
                                  PlcpFieldType fieldType, Time txDuration) {
  NS_LOG_DEBUG("Start transmission: signal power before antenna gain="
               << GetPowerDbm(txVector.GetTxPowerLevel()) << "dBm");
  double txPowerWatts =
      DbmToW(GetPowerDbm(txVector.GetTxPowerLevel()) + GetTxGain());
  Ptr<SpectrumValue> txPowerSpectrum = GetTxPowerSpectralDensity(
      GetCenterFrequencyForChannelWidth(txVector), txVector.GetChannelWidth(),
      txPowerWatts, txVector.GetMode().GetModulationClass());
  Ptr<WigigSpectrumSignalParameters> txParams =
      Create<WigigSpectrumSignalParameters>();
  txParams->duration = txDuration;
  txParams->psd = txPowerSpectrum;
  NS_ASSERT_MSG(m_wifiSpectrumPhyInterface,
                "SpectrumPhy() is not set; maybe forgot to call "
                "CreateWifiSpectrumPhyInterface?");
  txParams->txPhy = m_wifiSpectrumPhyInterface->GetObject<SpectrumPhy>();
  txParams->plcpFieldType = fieldType;
  txParams->txVector = txVector;
  txParams->antennaId = GetCodebook()->GetActiveAntennaId();
  txParams->txPatternConfig = GetCodebook()->GetTxPatternConfig();
  NS_LOG_DEBUG("Starting transmission with power " << WToDbm(txPowerWatts)
                                                   << " dBm on channel "
                                                   << +GetChannelNumber());
  NS_LOG_DEBUG("Starting transmission with integrated spectrum power "
               << WToDbm(Integral(*txPowerSpectrum))
               << " dBm; spectrum model Uid: "
               << txPowerSpectrum->GetSpectrumModel()->GetUid());
  m_channel->StartTx(txParams);
}

void SpectrumWigigPhy::StartAgcSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this);
  TxSubfield(txVector, PLCP_80211AD_AGC_SF, AGC_SF_DURATION);
}

void SpectrumWigigPhy::StartCeSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this);
  TxSubfield(txVector, PLCP_80211AD_TRN_CE_SF, TRN_CE_DURATION);
}

void SpectrumWigigPhy::StartTrnSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this);
  TxSubfield(txVector, PLCP_80211AD_TRN_SF, TRN_SUBFIELD_DURATION);
}

uint16_t SpectrumWigigPhy::GetGuardBandwidth() const {
  return WIGIG_GUARD_BANDWIDTH;
}

} // namespace ns3
