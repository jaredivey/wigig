/*
 * Copyright (c) 2005, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Stefano Avallone <stavallo@unina.it>
 */

#ifndef WIFI_MAC_QUEUE_ITEM_COPY_H
#define WIFI_MAC_QUEUE_ITEM_COPY_H

#include "wigig-mac-header.h"
#include "wigig-msdu-aggregator.h"

#include "ns3/amsdu-subframe-header.h"
#include "ns3/nstime.h"

namespace ns3 {

class QosBlockedDestinations;
class Packet;

/**
 * \ingroup wigig
 *
 * WigigMacQueueItem stores (const) packets along with their Wifi MAC headers
 * and the time when they were enqueued.
 */
class WigigMacQueueItem : public SimpleRefCount<WigigMacQueueItem> {
public:
  /**
   * \brief Create a Wifi MAC queue item containing a packet and a Wifi MAC
   * header. \param p the const packet included in the created item. \param
   * header the Wifi MAC header included in the created item.
   */
  WigigMacQueueItem(Ptr<const Packet> p, const WigigMacHeader &header);

  /**
   * \brief Create a Wifi MAC queue item containing a packet and a Wifi MAC
   * header. \param p the const packet included in the created item. \param
   * header the Wifi MAC header included in the created item. \param tstamp the
   * timestamp associated with the created item.
   */
  WigigMacQueueItem(Ptr<const Packet> p, const WigigMacHeader &header,
                    Time tstamp);

  /**
   * \brief Create a Wifi MAC queue item containing only a packet and without a
   * Wifi MAC header. Used for Short SSW packets. \param p the const packet
   * included in the created item.
   */
  WigigMacQueueItem(Ptr<const Packet> p);

  virtual ~WigigMacQueueItem();

  /**
   * \brief Get the packet stored in this item
   * \return the packet stored in this item.
   */
  Ptr<const Packet> GetPacket() const;

  /**
   * \brief Get the header stored in this item
   * \return the header stored in this item.
   */
  const WigigMacHeader &GetHeader() const;

  /**
   * \brief Get the header stored in this item
   * \return the header stored in this item.
   */
  WigigMacHeader &GetHeader();

  /**
   * \brief Return the destination address present in the header
   * \return the destination address
   */
  Mac48Address GetDestinationAddress() const;

  /**
   * \brief Get the timestamp included in this item
   * \return the timestamp included in this item.
   */
  Time GetTimeStamp() const;

  /**
   * \brief Return the size of the packet stored by this item, including header
   *        size and trailer size
   *
   * \return the size of the packet stored by this item in bytes.
   */
  uint32_t GetSize() const;

  /**
   * \brief Aggregate the MSDU contained in the given MPDU to this MPDU (thus
   *        constituting an A-MSDU). Note that the given MPDU cannot contain
   *        an A-MSDU.
   * \param msdu the MPDU containing the MSDU to aggregate
   */
  void Aggregate(Ptr<const WigigMacQueueItem> msdu);

  /**
   * \brief Get a constant iterator pointing to the first MSDU in the list of
   * aggregated MSDUs.
   *
   * \return a constant iterator pointing to the first MSDU in the list of
   * aggregated MSDUs
   */
  WigigMsduAggregator::DeaggregatedMsdusCI begin();
  /**
   * \brief Get a constant iterator indicating past-the-last MSDU in the list of
   * aggregated MSDUs.
   *
   * \return a constant iterator indicating past-the-last MSDU in the list of
   * aggregated MSDUs
   */
  WigigMsduAggregator::DeaggregatedMsdusCI end();

  /**
   * \brief Get the MAC protocol data unit (MPDU) corresponding to this item
   *        (i.e. a copy of the packet stored in this item wrapped with MAC
   *        header and trailer)
   * \return the MAC protocol data unit corresponding to this item.
   */
  Ptr<Packet> GetProtocolDataUnit() const;

  /**
   * \brief Print the item contents.
   * \param os output stream in which the data should be printed.
   */
  virtual void Print(std::ostream &os) const;

private:
  /**
   * \brief Aggregate the MSDU contained in the given MPDU to this MPDU (thus
   *        constituting an A-MSDU). Note that the given MPDU cannot contain
   *        an A-MSDU.
   * \param msdu the MPDU containing the MSDU to aggregate
   */
  void DoAggregate(Ptr<const WigigMacQueueItem> msdu);

  Ptr<const Packet>
      m_packet; //!< The packet (MSDU or A-MSDU) contained in this queue item
  WigigMacHeader m_header; //!< Wifi MAC header associated with the packet
  Time m_tstamp;           //!< timestamp when the packet arrived at the queue
  WigigMsduAggregator::DeaggregatedMsdus
      m_msduList;    //!< The list of aggregated MSDUs included in this MPDU
  bool m_isShortSsw; //!< A flag which specifies if the packet is a Short SSW
                     //!< packet and does not have a Wifi Mac Header.
};

/**
 * \brief Stream insertion operator.
 *
 * \param os the output stream
 * \param item the WigigMacQueueItem
 * \returns a reference to the stream
 */
std::ostream &operator<<(std::ostream &os, const WigigMacQueueItem &item);

} // namespace ns3

#endif /* WIFI_MAC_QUEUE_ITEM_COPY_H */
