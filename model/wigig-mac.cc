/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-mac.h"

#include "mac-low.h"
#include "wigig-channel-access-manager.h"
#include "wigig-mac-queue.h"
#include "wigig-mac-rx-middle.h"
#include "wigig-mgt-headers.h"
#include "wigig-mpdu-aggregator.h"
#include "wigig-msdu-aggregator.h"
#include "wigig-net-device.h"
#include "wigig-phy.h"
#include "wigig-txop.h"

#include "ns3/amsdu-subframe-header.h"
#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/mac-tx-middle.h"
#include "ns3/mgt-headers.h"
#include "ns3/packet.h"
#include "ns3/pointer.h"
#include "ns3/simulator.h"
#include "ns3/ssid.h"
#include "ns3/wifi-utils.h"

#include <algorithm>
#include <cmath>
#include <queue>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigMac");

NS_OBJECT_ENSURE_REGISTERED(WigigMac);

TypeId WigigMac::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigMac")
          .SetParent<Object>()
          .SetGroupName("Wigig")
          .AddAttribute(
              "AckTimeout",
              "When this timeout expires, the Data/Ack handshake has failed.",
              TimeValue(GetDefaultCtsAckTimeout()),
              MakeTimeAccessor(&WigigMac::GetAckTimeout,
                               &WigigMac::SetAckTimeout),
              MakeTimeChecker())
          .AddAttribute("BasicBlockAckTimeout",
                        "When this timeout expires, the "
                        "BASIC_BLOCK_ACK_REQ/BASIC_BLOCK_ACK "
                        "handshake has failed.",
                        TimeValue(GetDefaultBasicBlockAckTimeout()),
                        MakeTimeAccessor(&WigigMac::GetBasicBlockAckTimeout,
                                         &WigigMac::SetBasicBlockAckTimeout),
                        MakeTimeChecker())
          .AddAttribute(
              "CompressedBlockAckTimeout",
              "When this timeout expires, the "
              "COMPRESSED_BLOCK_ACK_REQ/COMPRESSED_BLOCK_ACK handshake has "
              "failed.",
              TimeValue(GetDefaultCompressedBlockAckTimeout()),
              MakeTimeAccessor(&WigigMac::GetCompressedBlockAckTimeout,
                               &WigigMac::SetCompressedBlockAckTimeout),
              MakeTimeChecker())
          .AddAttribute(
              "Sifs", "The value of the SIFS constant.",
              TimeValue(GetDefaultSifs()),
              MakeTimeAccessor(&WigigMac::SetSifs, &WigigMac::GetSifs),
              MakeTimeChecker())
          .AddAttribute("EifsNoDifs", "The value of EIFS-DIFS.",
                        TimeValue(GetDefaultEifsNoDifs()),
                        MakeTimeAccessor(&WigigMac::SetEifsNoDifs,
                                         &WigigMac::GetEifsNoDifs),
                        MakeTimeChecker())
          .AddAttribute(
              "Slot", "The duration of a Slot.", TimeValue(GetDefaultSlot()),
              MakeTimeAccessor(&WigigMac::SetSlot, &WigigMac::GetSlot),
              MakeTimeChecker())
          .AddAttribute(
              "Pifs", "The value of the PIFS constant.",
              TimeValue(GetDefaultSifs() + GetDefaultSlot()),
              MakeTimeAccessor(&WigigMac::SetPifs, &WigigMac::GetPifs),
              MakeTimeChecker())
          .AddAttribute("MaxPropagationDelay",
                        "The maximum propagation delay. Unused for now.",
                        TimeValue(GetDefaultMaxPropagationDelay()),
                        MakeTimeAccessor(&WigigMac::SetMaxPropagationDelay),
                        MakeTimeChecker())
          .AddAttribute(
              "Ssid", "The ssid we want to belong to.",
              SsidValue(Ssid("default")),
              MakeSsidAccessor(&WigigMac::GetSsid, &WigigMac::SetSsid),
              MakeSsidChecker())
          .AddAttribute("DmgSupported",
                        "This Boolean attribute is set to enable 802.11ad "
                        "support at this STA",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigMac::SetDmgSupported,
                                            &WigigMac::GetDmgSupported),
                        MakeBooleanChecker())
          .AddAttribute("CtsToSelfSupported",
                        "Use CTS to Self when using a rate that is not in the "
                        "basic rate set.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigMac::SetCtsToSelfSupported),
                        MakeBooleanChecker())
          .AddAttribute(
              "VO_MaxAmsduSize",
              "Maximum length in bytes of an A-MSDU for AC_VO access class "
              "(capped to 7935 for HT PPDUs). "
              "Value 0 means A-MSDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_voMaxAmsduSize),
              MakeUintegerChecker<uint16_t>(0, 11398))
          .AddAttribute(
              "VI_MaxAmsduSize",
              "Maximum length in bytes of an A-MSDU for AC_VI access class "
              "(capped to 7935 for HT PPDUs). "
              "Value 0 means A-MSDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_viMaxAmsduSize),
              MakeUintegerChecker<uint16_t>(0, 11398))
          .AddAttribute(
              "BE_MaxAmsduSize",
              "Maximum length in bytes of an A-MSDU for AC_BE access class "
              "(capped to 7935 for HT PPDUs). "
              "Value 0 means A-MSDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_beMaxAmsduSize),
              MakeUintegerChecker<uint16_t>(0, 11398))
          .AddAttribute(
              "BK_MaxAmsduSize",
              "Maximum length in bytes of an A-MSDU for AC_BK access class "
              "(capped to 7935 for HT PPDUs). "
              "Value 0 means A-MSDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_bkMaxAmsduSize),
              MakeUintegerChecker<uint16_t>(0, 11398))
          .AddAttribute(
              "VO_MaxAmpduSize",
              "Maximum length in bytes of an A-MPDU for AC_VO access class "
              "(capped to 65535 for HT PPDUs). "
              "Value 0 means A-MPDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_voMaxAmpduSize),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "VI_MaxAmpduSize",
              "Maximum length in bytes of an A-MPDU for AC_VI access class "
              "(capped to 65535 for HT PPDUs). "
              "Value 0 means A-MPDU aggregation is disabled for that AC.",
              UintegerValue(65535),
              MakeUintegerAccessor(&WigigMac::m_viMaxAmpduSize),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "BE_MaxAmpduSize",
              "Maximum length in bytes of an A-MPDU for AC_BE access class "
              "(capped to 65535 for HT PPDUs). "
              "Value 0 means A-MPDU aggregation is disabled for that AC.",
              UintegerValue(65535),
              MakeUintegerAccessor(&WigigMac::m_beMaxAmpduSize),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "BK_MaxAmpduSize",
              "Maximum length in bytes of an A-MPDU for AC_BK access class "
              "(capped to 65535 for HT PPDUs). "
              "Value 0 means A-MPDU aggregation is disabled for that AC.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_bkMaxAmpduSize),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute("VO_BlockAckThreshold",
                        "If number of packets in VO queue reaches this value, "
                        "block ack mechanism is used. If this value is 0, "
                        "block ack is never used."
                        "When A-MPDU is enabled, block ack mechanism is used "
                        "regardless of this value.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigMac::SetVoBlockAckThreshold),
                        MakeUintegerChecker<uint8_t>(0, 64))
          .AddAttribute("VI_BlockAckThreshold",
                        "If number of packets in VI queue reaches this value, "
                        "block ack mechanism is used. If this value is 0, "
                        "block ack is never used."
                        "When A-MPDU is enabled, block ack mechanism is used "
                        "regardless of this value.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigMac::SetViBlockAckThreshold),
                        MakeUintegerChecker<uint8_t>(0, 64))
          .AddAttribute("BE_BlockAckThreshold",
                        "If number of packets in BE queue reaches this value, "
                        "block ack mechanism is used. If this value is 0, "
                        "block ack is never used."
                        "When A-MPDU is enabled, block ack mechanism is used "
                        "regardless of this value.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigMac::SetBeBlockAckThreshold),
                        MakeUintegerChecker<uint8_t>(0, 64))
          .AddAttribute("BK_BlockAckThreshold",
                        "If number of packets in BK queue reaches this value, "
                        "block ack mechanism is used. If this value is 0, "
                        "block ack is never used."
                        "When A-MPDU is enabled, block ack mechanism is used "
                        "regardless of this value.",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigMac::SetBkBlockAckThreshold),
                        MakeUintegerChecker<uint8_t>(0, 64))
          .AddAttribute(
              "VO_BlockAckInactivityTimeout",
              "Represents max time (blocks of 1024 microseconds) allowed for "
              "block ack"
              "inactivity for AC_VO. If this value isn't equal to 0 a timer "
              "start after that a"
              "block ack setup is completed and will be reset every time that "
              "a block ack"
              "frame is received. If this value is 0, block ack inactivity "
              "timeout won't be "
              "used.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::SetVoBlockAckInactivityTimeout),
              MakeUintegerChecker<uint16_t>())
          .AddAttribute(
              "VI_BlockAckInactivityTimeout",
              "Represents max time (blocks of 1024 microseconds) allowed for "
              "block ack"
              "inactivity for AC_VI. If this value isn't equal to 0 a timer "
              "start after that a"
              "block ack setup is completed and will be reset every time that "
              "a block ack"
              "frame is received. If this value is 0, block ack inactivity "
              "timeout won't be "
              "used.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::SetViBlockAckInactivityTimeout),
              MakeUintegerChecker<uint16_t>())
          .AddAttribute(
              "BE_BlockAckInactivityTimeout",
              "Represents max time (blocks of 1024 microseconds) allowed for "
              "block ack"
              "inactivity for AC_BE. If this value isn't equal to 0 a timer "
              "start after that a"
              "block ack setup is completed and will be reset every time that "
              "a block ack"
              "frame is received. If this value is 0, block ack inactivity "
              "timeout won't be "
              "used.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::SetBeBlockAckInactivityTimeout),
              MakeUintegerChecker<uint16_t>())
          .AddAttribute(
              "BK_BlockAckInactivityTimeout",
              "Represents max time (blocks of 1024 microseconds) allowed for "
              "block ack"
              "inactivity for AC_BK. If this value isn't equal to 0 a timer "
              "start after that a"
              "block ack setup is completed and will be reset every time that "
              "a block ack"
              "frame is received. If this value is 0, block ack inactivity "
              "timeout won't be "
              "used.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::SetBkBlockAckInactivityTimeout),
              MakeUintegerChecker<uint16_t>())
          .AddAttribute("Txop", "The Txop object.", PointerValue(),
                        MakePointerAccessor(&WigigMac::GetTxop),
                        MakePointerChecker<WigigTxop>())
          .AddAttribute(
              "VO_Txop",
              "Queue that manages packets belonging to AC_VO access class.",
              PointerValue(), MakePointerAccessor(&WigigMac::GetVOQueue),
              MakePointerChecker<WigigQosTxop>())
          .AddAttribute(
              "VI_Txop",
              "Queue that manages packets belonging to AC_VI access class.",
              PointerValue(), MakePointerAccessor(&WigigMac::GetVIQueue),
              MakePointerChecker<WigigQosTxop>())
          .AddAttribute(
              "BE_Txop",
              "Queue that manages packets belonging to AC_BE access class.",
              PointerValue(), MakePointerAccessor(&WigigMac::GetBEQueue),
              MakePointerChecker<WigigQosTxop>())
          .AddAttribute(
              "BK_Txop",
              "Queue that manages packets belonging to AC_BK access class.",
              PointerValue(), MakePointerAccessor(&WigigMac::GetBKQueue),
              MakePointerChecker<WigigQosTxop>())
          /* DMG Operation Element */
          .AddAttribute("PcpHandoverSupport",
                        "Whether we support PCP Handover.", BooleanValue(false),
                        MakeBooleanAccessor(&WigigMac::SetPcpHandoverSupport,
                                            &WigigMac::GetPcpHandoverSupport),
                        MakeBooleanChecker())
          /* Reverse Direction Protocol */
          .AddAttribute(
              "SupportRDP",
              "Whether the DMG STA supports Reverse Direction Protocol (RDP)",
              BooleanValue(false), MakeBooleanAccessor(&WigigMac::m_supportRdp),
              MakeBooleanChecker())
          /* DMG Relay Capabilities common between PCP/AP and DMG STA */
          .AddAttribute("REDSActivated", "Whether the DMG STA is REDS.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigMac::m_redsActivated),
                        MakeBooleanChecker())
          .AddAttribute("RDSActivated", "Whether the DMG STA is RDS.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigMac::m_rdsActivated),
                        MakeBooleanChecker())
          .AddAttribute("RelayDuplexMode", "The duplex mode of the relay.",
                        EnumValue(RELAY_BOTH),
                        MakeEnumAccessor(&WigigMac::m_relayDuplexMode),
                        MakeEnumChecker(RELAY_FD_AF, "Full Duplex", RELAY_HD_DF,
                                        "Half Duplex", RELAY_BOTH, "Both"))
          /* Antenna Pattern Reciprocity */
          .AddAttribute(
              "AntennaPatternReciprocity",
              "Indicates that STA supports reciprocity of the TX/RX antenna "
              "patterns",
              BooleanValue(false),
              MakeBooleanAccessor(&WigigMac::m_antennaPatternReciprocity),
              MakeBooleanChecker())
          /* Use Rx Sectors */
          .AddAttribute("UseRxSectors",
                        "Indicates whether the STA should use the chosen Rx "
                        "sectors during operation",
                        BooleanValue(true),
                        MakeBooleanAccessor(&WigigMac::m_useRxSectors),
                        MakeBooleanChecker())
          /* Link Maintenance Attributes */
          .AddAttribute(
              "BeamLinkMaintenanceUnit",
              "The unit used for dot11BeamLinkMaintenanceTime calculation.",
              EnumValue(UNIT_32US),
              MakeEnumAccessor(&WigigMac::m_beamlinkMaintenanceUnit),
              MakeEnumChecker(UNIT_32US, "32US", UNIT_2000US, "2000US"))
          .AddAttribute(
              "BeamLinkMaintenanceValue",
              "The value of the beamlink maintenance used for "
              "dot11BeamLinkMaintenanceTime calculation.",
              UintegerValue(0),
              MakeUintegerAccessor(&WigigMac::m_beamlinkMaintenanceValue),
              MakeUintegerChecker<uint8_t>(0, 63))
          .AddTraceSource("MacTx",
                          "A packet has been received from higher layers and "
                          "is being processed "
                          "in preparation for "
                          "queueing for transmission.",
                          MakeTraceSourceAccessor(&WigigMac::m_macTxTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "MacTxDrop",
              "A packet has been dropped in the MAC layer before transmission.",
              MakeTraceSourceAccessor(&WigigMac::m_macTxDropTrace),
              "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "MacPromiscRx",
              "A packet has been received by this device, has been passed up "
              "from the physical "
              "layer "
              "and is being forwarded up the local protocol stack.  This is a "
              "promiscuous trace.",
              MakeTraceSourceAccessor(&WigigMac::m_macPromiscRxTrace),
              "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "MacRx",
              "A packet has been received by this device, has been passed up "
              "from "
              "the physical layer "
              "and is being forwarded up the local protocol stack. This is a "
              "non-promiscuous trace.",
              MakeTraceSourceAccessor(&WigigMac::m_macRxTrace),
              "ns3::Packet::TracedCallback")
          .AddTraceSource("MacRxDrop",
                          "A packet has been dropped in the MAC layer after it "
                          "has been passed "
                          "up from the physical layer.",
                          MakeTraceSourceAccessor(&WigigMac::m_macRxDropTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource("TxOkHeader",
                          "The header of successfully transmitted packet.",
                          MakeTraceSourceAccessor(&WigigMac::m_txOkCallback),
                          "ns3::WigigMacHeader::TracedCallback")
          .AddTraceSource("TxErrHeader",
                          "The header of unsuccessfully transmitted packet.",
                          MakeTraceSourceAccessor(&WigigMac::m_txErrCallback),
                          "ns3::WigigMacHeader::TracedCallback")
          /* Beacon Interval Traces */
          .AddTraceSource(
              "DtiStarted",
              "The Data Transmission Interval access period started.",
              MakeTraceSourceAccessor(&WigigMac::m_dtiStarted),
              "ns3::WigigMac::DtiStartedTracedCallback")
          /* Service Period Allocation Traces */
          .AddTraceSource("ServicePeriodStarted",
                          "A service period between two DMG STAs has started.",
                          MakeTraceSourceAccessor(
                              &WigigMac::m_servicePeriodStartedCallback),
                          "ns3::WigigMac::ServicePeriodTracedCallback")
          .AddTraceSource(
              "ServicePeriodEnded",
              "A service period between two DMG STAs has ended.",
              MakeTraceSourceAccessor(&WigigMac::m_servicePeriodEndedCallback),
              "ns3::WigigMac::ServicePeriodTracedCallback")
          /* DMG Beamforming Training Related Traces */
          .AddTraceSource(
              "SLSInitiatorStateMachine",
              "Trace the current state of the SLS Initiator state machine.",
              MakeTraceSourceAccessor(&WigigMac::m_slsInitiatorStateMachine),
              "ns3::SlsInitiatorTracedValueCallback")
          .AddTraceSource(
              "SLSResponderStateMachine",
              "Trace the current state of the SLS Responder state machine.",
              MakeTraceSourceAccessor(&WigigMac::m_slsResponderStateMachine),
              "ns3::SlsResponderTracedValueCallback")
          .AddTraceSource("SLSCompleted",
                          "Sector Level Sweep (SLS) phase is completed.",
                          MakeTraceSourceAccessor(&WigigMac::m_slsCompleted),
                          "ns3::WigigMac::SLSCompletedTracedCallback")
          .AddTraceSource(
              "BrpCompleted",
              "Brp for transmit/receive beam refinement is completed",
              MakeTraceSourceAccessor(&WigigMac::m_brpCompleted),
              "ns3::WigigMac::BrpCompletedTracedCallback")
          .AddTraceSource(
              "BeamLinkMaintenanceTimerStateChanged",
              "The BeamLink maintenance timer associated to a link has "
              "expired.",
              MakeTraceSourceAccessor(
                  &WigigMac::m_beamLinkMaintenanceTimerStateChanged),
              "ns3::StaWigigMac::"
              "BeamLinkMaintenanceTimerStateChangedTracedCallback")
          /* DMG Relaying Related Traces */
          .AddTraceSource("RlsCompleted",
                          "The Relay Link Setup (RLS) procedure is completed",
                          MakeTraceSourceAccessor(&WigigMac::m_rlsCompleted),
                          "ns3::Mac48Address::TracedCallback");
  return tid;
}

WigigMac::WigigMac()
    : m_dmgSupported(false), m_stationSnrMap(), m_bestAntennaConfig(),
      m_bestAwvConfig(), m_feedbackAntennaConfig(), m_maxSnr(0.0),
      m_accessPeriod(CHANNEL_ACCESS_BTI), m_atiDuration(Seconds(0)),
      m_biStartTime(Seconds(0)), m_beaconInterval(Seconds(0)),
      m_atiPresent(false), m_nextBeacon(0), m_btiPeriodicity(0),
      m_dtiDuration(Seconds(0)), m_dtiStartTime(Seconds(0)),
      m_abftDuration(Seconds(0)), m_ssSlotsPerAbft(0), m_ssFramesPerSlot(0),
      m_nextAbft(0), m_totalSectors(0), m_sectorFeedbackScheduled(false),
      m_isBrpSetupCompleted(), m_raisedBrpSetupCompleted(),
      m_recordTrnSnrValues(false), m_requestedBrpTraining(false),
      m_restartISSEvent(), m_sswFbckTimeout(), m_sswAckTimeoutEvent(),
      m_rssEvent(), m_beamLinkMaintenanceTimeout() {
  NS_LOG_FUNCTION(this);

  m_rxMiddle = Create<WigigMacRxMiddle>();
  m_rxMiddle->SetForwardCallback(MakeCallback(&WigigMac::Receive, this));

  m_txMiddle = Create<MacTxMiddle>();

  m_low = CreateObject<MacLow>();
  m_low->SetRxCallback(MakeCallback(&WigigMacRxMiddle::Receive, m_rxMiddle));
  m_low->SetMac(this);

  m_channelAccessManager = CreateObject<WigigChannelAccessManager>();
  m_channelAccessManager->SetupLow(m_low);

  m_txop = CreateObject<WigigTxop>();
  m_txop->SetMacLow(m_low);
  m_txop->SetChannelAccessManager(m_channelAccessManager);
  m_txop->SetTxMiddle(m_txMiddle);
  m_txop->SetTxOkCallback(MakeCallback(&WigigMac::TxOk, this));
  m_txop->SetTxFailedCallback(MakeCallback(&WigigMac::TxFailed, this));
  m_txop->SetTxDroppedCallback(MakeCallback(&WigigMac::NotifyTxDrop, this));

  // Construct the EDCAFs. The ordering is important - highest
  // priority (Table 9-1 UP-to-AC mapping; IEEE 802.11-2012) must be created
  // first.
  SetupEdcaQueue(AC_VO);
  SetupEdcaQueue(AC_VI);
  SetupEdcaQueue(AC_BE);
  SetupEdcaQueue(AC_BK);

  /* DMG Management TXOP */
  m_txop->SetTxOkNoAckCallback(MakeCallback(&WigigMac::ManagementTxOk, this));
  /* DMG ATI TXOP Initialization */
  m_dmgAtiTxop = CreateObject<DmgAtiTxop>();
  m_dmgAtiTxop->SetAifsn(0);
  m_dmgAtiTxop->SetMinCw(0);
  m_dmgAtiTxop->SetMaxCw(0);
  m_dmgAtiTxop->SetMacLow(m_low);
  m_dmgAtiTxop->SetChannelAccessManager(m_channelAccessManager);
  m_dmgAtiTxop->SetTxMiddle(m_txMiddle);
  m_dmgAtiTxop->SetTxOkCallback(MakeCallback(&WigigMac::TxOk, this));
  m_dmgAtiTxop->SetTxOkNoAckCallback(
      MakeCallback(&WigigMac::ManagementTxOk, this));
  /* DMG SLS TXOP Initialization */
  m_dmgSlsTxop = CreateObject<DmgSlsTxop>();
  m_dmgSlsTxop->SetAifsn(1);
  m_dmgSlsTxop->SetMinCw(15);
  m_dmgSlsTxop->SetMaxCw(1023);
  m_dmgSlsTxop->SetMacLow(m_low);
  m_dmgSlsTxop->SetChannelAccessManager(m_channelAccessManager);
  m_dmgSlsTxop->SetTxMiddle(m_txMiddle);
  m_dmgSlsTxop->SetTxOkNoAckCallback(MakeCallback(&WigigMac::FrameTxOk, this));
  m_dmgSlsTxop->SetAccessGrantedCallback(
      MakeCallback(&WigigMac::TxssTxopAccessGranted, this));
}

WigigMac::~WigigMac() { NS_LOG_FUNCTION(this); }

void WigigMac::DoDispose() {
  NS_LOG_FUNCTION(this);

  m_dmgAtiTxop = nullptr;

  m_codebook->Dispose();
  m_codebook = nullptr;

  m_rxMiddle = nullptr;
  m_txMiddle = nullptr;

  m_low->Dispose();
  m_low = nullptr;

  m_phy = nullptr;
  m_stationManager = nullptr;

  m_txop->Dispose();
  m_txop = nullptr;

  for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->Dispose();
    i->second = nullptr;
  }

  m_channelAccessManager->Dispose();
  m_channelAccessManager = nullptr;

  m_device = nullptr;
}

void WigigMac::DoInitialize() {
  NS_LOG_FUNCTION(this);

  /* Initialize beamforming training variables */
  ResetSlsStateMachineVariables();

  /* PHY Layer Information */

  /* Beamforming variables */
  m_currentLinkMaintained = false;
  GetPhy()->RegisterReportSnrCallback(
      MakeCallback(&WigigMac::ReportSnrValue, this));

  /* Beam Link Maintenance */
  if (m_beamlinkMaintenanceUnit == UNIT_32US) {
    dot11BeamLinkMaintenanceTime =
        MicroSeconds(m_beamlinkMaintenanceValue * 32);
  } else {
    dot11BeamLinkMaintenanceTime =
        MicroSeconds(m_beamlinkMaintenanceValue * 2000);
  }

  /* Initialzie Codebook */
  m_codebook->Initialize();

  /* Channel Access Periods */
  m_dmgAtiTxop->Initialize();
  m_dmgSlsTxop->Initialize();

  m_txop->Initialize();

  for (EdcaQueues::const_iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->Initialize();
  }
}

Time WigigMac::GetDefaultMaxPropagationDelay() {
  // 1000m
  return Seconds(1000.0 / 300000000.0);
}

Time WigigMac::GetDefaultSlot() {
  // 802.11-a specific
  return MicroSeconds(9);
}

Time WigigMac::GetDefaultSifs() {
  // 802.11-a specific
  return MicroSeconds(16);
}

Time WigigMac::GetDefaultEifsNoDifs() {
  return GetDefaultSifs() + GetDefaultCtsAckDelay();
}

Time WigigMac::GetDefaultCtsAckDelay() {
  // 802.11-a specific: at 6 Mbit/s
  return MicroSeconds(44);
}

Time WigigMac::GetDefaultCtsAckTimeout() {
  /* Cts_Timeout and Ack_Timeout are specified in the Annex C
     (Formal description of MAC operation, see details on the
     Trsp timer setting at page 346)
  */
  Time ctsTimeout = GetDefaultSifs();
  ctsTimeout += GetDefaultCtsAckDelay();
  ctsTimeout +=
      MicroSeconds(GetDefaultMaxPropagationDelay().GetMicroSeconds() * 2);
  ctsTimeout += GetDefaultSlot();
  return ctsTimeout;
}

Time WigigMac::GetDefaultBasicBlockAckDelay() {
  // This value must be revisited
  return MicroSeconds(250);
}

Time WigigMac::GetDefaultCompressedBlockAckDelay() {
  // This value must be revisited
  // CompressedBlockAckSize 32 * 8 * time it takes to transfer at the lowest
  // rate (at 6 Mbit/s) + aPhy-StartDelay (33)
  return MicroSeconds(76);
}

Time WigigMac::GetDefaultBasicBlockAckTimeout() {
  Time blockAckTimeout = GetDefaultSifs();
  blockAckTimeout += GetDefaultBasicBlockAckDelay();
  blockAckTimeout +=
      MicroSeconds(GetDefaultMaxPropagationDelay().GetMicroSeconds() * 2);
  blockAckTimeout += GetDefaultSlot();
  return blockAckTimeout;
}

Time WigigMac::GetDefaultCompressedBlockAckTimeout() {
  Time blockAckTimeout = GetDefaultSifs();
  blockAckTimeout += GetDefaultCompressedBlockAckDelay();
  blockAckTimeout +=
      MicroSeconds(GetDefaultMaxPropagationDelay().GetMicroSeconds() * 2);
  blockAckTimeout += GetDefaultSlot();
  return blockAckTimeout;
}

void WigigMac::SetPhy(const Ptr<WigigPhy> phy) {
  NS_LOG_FUNCTION(this << phy);
  m_phy = phy;
  m_channelAccessManager->SetupPhyListener(phy);
  m_low->SetPhy(phy);
}

Ptr<WigigPhy> WigigMac::GetPhy() const {
  NS_LOG_FUNCTION(this);
  return m_phy;
}

void WigigMac::SetDevice(const Ptr<NetDevice> device) { m_device = device; }

Ptr<NetDevice> WigigMac::GetDevice() const { return m_device; }

void WigigMac::SetMaxPropagationDelay(Time delay) {
  NS_LOG_FUNCTION(this << delay);
  m_maxPropagationDelay = delay;
}

Time WigigMac::GetMaxPropagationDelay() const { return m_maxPropagationDelay; }

void WigigMac::NotifyTx(Ptr<const Packet> packet) { m_macTxTrace(packet); }

void WigigMac::NotifyTxDrop(Ptr<const Packet> packet) {
  m_macTxDropTrace(packet);
}

void WigigMac::NotifyRx(Ptr<const Packet> packet) { m_macRxTrace(packet); }

void WigigMac::NotifyPromiscRx(Ptr<const Packet> packet) {
  m_macPromiscRxTrace(packet);
}

void WigigMac::NotifyRxDrop(Ptr<const Packet> packet) {
  m_macRxDropTrace(packet);
}

void WigigMac::SetWifiRemoteStationManager(
    Ptr<WigigRemoteStationManager> stationManager) {
  NS_LOG_FUNCTION(this << stationManager);

  m_stationManager = stationManager;

  m_dmgAtiTxop->SetWifiRemoteStationManager(stationManager);
  m_txop->SetWifiRemoteStationManager(stationManager);

  m_stationManager->SetDmgSupported(GetDmgSupported());

  m_low->SetWifiRemoteStationManager(stationManager);
  m_txop->SetWifiRemoteStationManager(stationManager);
  for (EdcaQueues::const_iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->SetWifiRemoteStationManager(stationManager);
  }
}

Ptr<WigigRemoteStationManager> WigigMac::GetWifiRemoteStationManager() const {
  return m_stationManager;
}

void WigigMac::ConfigureDcf(Ptr<WigigTxop> dcf, uint32_t cwmin, uint32_t cwmax,
                            bool isDsss, AcIndex ac) {
  NS_LOG_FUNCTION(this << dcf << cwmin << cwmax << isDsss << ac);
  /* see IEEE 802.11 section 7.3.2.29 */
  switch (ac) {
  case AC_VO:
    dcf->SetMinCw((cwmin + 1) / 4 - 1);
    dcf->SetMaxCw((cwmin + 1) / 2 - 1);
    dcf->SetAifsn(2);
    if (isDsss) {
      dcf->SetTxopLimit(MicroSeconds(3264));
    } else {
      dcf->SetTxopLimit(MicroSeconds(1504));
    }
    break;
  case AC_VI:
    dcf->SetMinCw((cwmin + 1) / 2 - 1);
    dcf->SetMaxCw(cwmin);
    dcf->SetAifsn(2);
    if (isDsss) {
      dcf->SetTxopLimit(MicroSeconds(6016));
    } else {
      dcf->SetTxopLimit(MicroSeconds(3008));
    }
    break;
  case AC_BE:
    dcf->SetMinCw(cwmin);
    dcf->SetMaxCw(cwmax);
    dcf->SetAifsn(3);
    dcf->SetTxopLimit(MicroSeconds(0));
    break;
  case AC_BK:
    dcf->SetMinCw(cwmin);
    dcf->SetMaxCw(cwmax);
    dcf->SetAifsn(7);
    dcf->SetTxopLimit(MicroSeconds(0));
    break;
  case AC_BE_NQOS:
    dcf->SetMinCw(cwmin);
    dcf->SetMaxCw(cwmax);
    dcf->SetAifsn(2);
    dcf->SetTxopLimit(MicroSeconds(0));
    break;
  case AC_BEACON:
  case AC_UNDEF:
    NS_FATAL_ERROR("I don't know what to do with this");
    break;
  }
}

void WigigMac::ConfigureStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  switch (standard) {
  case WIFI_STANDARD_80211ad:
    Configure80211ad();
    break;
  case WIFI_STANDARD_UNSPECIFIED:
  default:
    NS_FATAL_ERROR("Wifi standard not found");
    break;
  }
  FinishConfigureStandard(standard);
}

void WigigMac::FinishConfigureStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  uint32_t cwmin = 0;
  uint32_t cwmax = 0;
  switch (standard) {
  case WIFI_STANDARD_80211ad:
    SetDmgSupported(true);
    cwmin = 15;
    cwmax = 1023;
    break;
  default:
    NS_FATAL_ERROR(
        "Unsupported WifiStandard in WigigMac::FinishConfigureStandard ()");
  }

  ConfigureContentionWindow(cwmin, cwmax);
}

void WigigMac::ConfigureContentionWindow(uint32_t cwMin, uint32_t cwMax) {
  // The special value of AC_BE_NQOS which exists in the Access
  // Category enumeration allows us to configure plain old DCF.
  ConfigureDcf(m_txop, cwMin, cwMax, false, AC_BE_NQOS);

  // Now we configure the EDCA functions
  for (EdcaQueues::const_iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    ConfigureDcf(i->second, cwMin, cwMax, false, i->first);
  }
}

void WigigMac::SetSbifs(Time sbifs) {
  NS_LOG_FUNCTION(this << sbifs);
  m_sbifs = sbifs;
  m_low->SetSbifs(sbifs);
}

void WigigMac::SetMbifs(Time mbifs) {
  NS_LOG_FUNCTION(this << mbifs);
  m_mbifs = mbifs;
  m_low->SetMbifs(mbifs);
}

void WigigMac::SetLbifs(Time lbifs) {
  NS_LOG_FUNCTION(this << lbifs);
  m_lbifs = lbifs;
  m_low->SetLbifs(lbifs);
}

void WigigMac::SetBrpifs(Time brpifs) {
  NS_LOG_FUNCTION(this << brpifs);
  m_brpifs = brpifs;
}

Time WigigMac::GetSbifs() const { return m_sbifs; }

Time WigigMac::GetMbifs() const { return m_mbifs; }

Time WigigMac::GetLbifs() const { return m_lbifs; }

Time WigigMac::GetBrpifs() const { return m_brpifs; }

void WigigMac::SetPcpHandoverSupport(bool value) {
  m_pcpHandoverSupport = value;
}

bool WigigMac::GetPcpHandoverSupport() const { return m_pcpHandoverSupport; }

void WigigMac::Configure80211ad() {
  NS_LOG_FUNCTION(this);

  SetSifs(MicroSeconds(3)); /* aSIFSTime in Table 21-31 */
  SetSlot(MicroSeconds(5)); /* aSlotTime in Table 21-31 */
  SetMaxPropagationDelay(
      NanoSeconds(100)); /* aAirPropagationTime << 0.1 usec in Table 21-31 */
  SetPifs(GetSifs() + GetSlot()); /* 802.11-2007 9.2.10 */
  SetEifsNoDifs(GetSifs() + NanoSeconds(13164));
  /* We need to distinguish between two ACK MCS */
  /* ACK is sent using MCS-0, ACK Size = 14 Bytes, PayloadDuration = 4218 ns,
   * TotalTx = 13164 ns
   */
  SetAckTimeout(GetSifs() + NanoSeconds(13164) + GetSlot() +
                GetMaxPropagationDelay() * 2);
  /* BlockAck is sent using either MCS-0 or SC-MCS-1/4, we assume MCS-0 in our
   * calculation */
  /* BasicBlockAck Size = 152 Bytes */
  SetBasicBlockAckTimeout(GetSifs() + GetSlot() + MicroSeconds(52) +
                          GetDefaultMaxPropagationDelay() * 2);
  /* CompressedBlockAck Size = 32 Bytes = 18.836 ns using MCS-0 */
  SetCompressedBlockAckTimeout(GetSifs() + GetSlot() + MicroSeconds(19) +
                               GetDefaultMaxPropagationDelay() * 2);

  /* DMG Beamforming IFS */
  SetSbifs(MicroSeconds(1));
  SetMbifs(GetSifs() * 3);
  SetLbifs(GetSifs() * 6);
  SetBrpifs(MicroSeconds(40));
}

void WigigMac::MapAidToMacAddress(uint16_t aid, Mac48Address address) {
  NS_LOG_FUNCTION(this << aid << address);
  m_aidMap[aid] = address;
  m_macMap[address] = aid;
}

void WigigMac::AddMcsSupport(Mac48Address address, uint32_t initialMcs,
                             uint32_t lastMcs) {
  for (uint32_t j = initialMcs; j <= lastMcs; j++) {
    m_stationManager->AddSupportedMode(address, m_phy->GetMode(j));
  }
}

ChannelAccessPeriod WigigMac::GetCurrentAccessPeriod() const {
  return m_accessPeriod;
}

AllocationType WigigMac::GetCurrentAllocation() const {
  return m_currentAllocation;
}

void WigigMac::ResumePendingTxss() {
  NS_LOG_FUNCTION(this);
  m_dmgSlsTxop->ResumeTXSS();
}

void WigigMac::StartContentionPeriod(AllocationId allocationId,
                                     Time contentionDuration) {
  NS_LOG_FUNCTION(this << +allocationId << contentionDuration);
  m_currentAllocation = CBAP_ALLOCATION;
  if (GetTypeOfStation() == STA) {
    /* For the time being we assume in CBAP we communicate with the DMG PCP/AP
     * only */
    SteerAntennaToward(GetBssid());
  }
  /* Allow Contention Access */
  m_channelAccessManager->AllowChannelAccess();
  /* Restore previously suspended transmission in LowMac */
  m_low->RestoreAllocationParameters(allocationId);
  /* Signal WigigTxop, WigigQosTxop, and SLS WigigTxop Functions to start
   * channel access */
  m_txop->StartAllocationPeriod(CBAP_ALLOCATION, allocationId, GetBssid(),
                                contentionDuration);
  for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->StartAllocationPeriod(CBAP_ALLOCATION, allocationId, GetBssid(),
                                     contentionDuration);
  }
  ResumePendingTxss();
  /* Schedule the end of the contention period */
  Simulator::Schedule(contentionDuration, &WigigMac::EndContentionPeriod, this);
  NS_ASSERT_MSG(Simulator::Now() + contentionDuration <=
                    m_dtiStartTime + m_dtiDuration,
                "Exceeding DTI Time, Error");
}

void WigigMac::EndContentionPeriod() {
  NS_LOG_FUNCTION(this);
  // End reception of TRN fields on the Physical Layer
  m_channelAccessManager->DisableChannelAccess();
  /* Signal Management DCA to suspend current transmission */
  m_txop->EndAllocationPeriod();
  /* Signal EDCA queues to suspend current transmission */
  for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->EndAllocationPeriod();
  }
  /* Inform MAC Low to store parameters related to this service period
   * (MPDU/A-MPDU) */
  m_low->EndAllocationPeriod();
}

void WigigMac::BeamLinkMaintenanceTimeout() {
  NS_LOG_FUNCTION(this);
  m_beamLinkMaintenanceTimerStateChanged(BEAM_LINK_MAINTENANCE_TIMER_EXPIRES,
                                         m_peerStationAid, m_peerStationAddress,
                                         Seconds(0.0));
}

void WigigMac::ScheduleServicePeriod(uint8_t blocks, Time spStart,
                                     Time spLength, Time spPeriod,
                                     AllocationId allocationId, uint8_t peerAid,
                                     Mac48Address peerAddress, bool isSource) {
  NS_LOG_FUNCTION(this << blocks << spStart << spLength << spPeriod
                       << +allocationId << +peerAid << peerAddress << isSource);
  /* We allocate multiple blocks of this allocation as in (9.33.6 Channel access
   * in scheduled DTI)
   */
  /* A_start + (i – 1) × A_period */
  /* Check if there is currently a reception on the PHY layer */
  Time endRx = m_phy->GetDelayUntilEndRx();
  if (spPeriod.IsStrictlyPositive()) {
    for (uint8_t i = 0; i < blocks; i++) {
      NS_LOG_INFO("Schedule SP Block [" << +i << "] at " << spStart << " till "
                                        << spStart + spLength);
      /* Check if the service period starts while there is an ongoing reception
       */
      /**
       * Note NINA: Temporary solution for when we are in the middle of
       * receiving a packet from a station from another BSS when a service
       * period is supposed to start. The standard is not clear about whether we
       * end the reception or finish it. For now, the reception is completed and
       * the service period will start after the end of the reception (it will
       * still finish at the same time and have a reduced duration).
       */
      Time spLengthNew = spLength;
      Time spStartNew = spStart;
      if (spStart < endRx) {
        /* if does schedule the start after the reception is complete */

        if (spStart + spLength < endRx) {
          spLengthNew = NanoSeconds(0);
        } else {
          spLengthNew = spLength - (endRx - spStart);
        }
        spStartNew = endRx;
      }
      Simulator::Schedule(spStartNew, &WigigMac::StartServicePeriod, this,
                          allocationId, spLengthNew, peerAid, peerAddress,
                          isSource);
      Simulator::Schedule(spStartNew + spLengthNew, &WigigMac::EndServicePeriod,
                          this);
      spStart += spLength + spPeriod + GUARD_TIME;
    }
  } else {
    /* Special case when Allocation Block Period=0 i.e. consecutive blocks *
     * We try to avoid scheduling multiple blocks, so we schedule one big block
     */
    spLength = spLength * blocks;
    if (spStart < endRx) {
      if (spStart + spLength < endRx) {
        spLength = NanoSeconds(0);
      } else {
        spLength = spLength - (endRx - spStart);
      }
      spStart = endRx;
    }
    Simulator::Schedule(spStart, &WigigMac::StartServicePeriod, this,
                        allocationId, spLength, peerAid, peerAddress, isSource);
    Simulator::Schedule(spStart + spLength, &WigigMac::EndServicePeriod, this);
  }
}

void WigigMac::StartServicePeriod(AllocationId allocationId, Time length,
                                  uint8_t peerAid, Mac48Address peerAddress,
                                  bool isSource) {
  NS_LOG_FUNCTION(this << length << +peerAid << peerAddress << isSource
                       << Simulator::Now());
  m_currentAllocationId = allocationId;
  m_currentAllocation = SERVICE_PERIOD_ALLOCATION;
  m_currentAllocationLength = length;
  m_allocationStarted = Simulator::Now();
  m_peerStationAid = peerAid;
  m_peerStationAddress = peerAddress;
  m_spSource = isSource;
  m_servicePeriodStartedCallback(GetAddress(), peerAddress);
  SteerAntennaToward(peerAddress);
  /* Restore previously suspended transmission in LowMac */
  m_low->RestoreAllocationParameters(allocationId);
  m_edca[AC_BE]->StartAllocationPeriod(SERVICE_PERIOD_ALLOCATION, allocationId,
                                       peerAddress, length);
  if (isSource) {
    /* Check if we are maintaining the beamformed link during this service
     * period as initiator
     */
    BeamLinkMaintenanceTableI it = m_beamLinkMaintenanceTable.find(peerAid);
    if (it != m_beamLinkMaintenanceTable.end()) {
      BeamLinkMaintenanceInfo info = it->second;
      m_currentLinkMaintained = true;
      m_linkMaintenanceInfo = info;
      m_beamLinkMaintenanceTimeout =
          Simulator::Schedule(info.beamLinkMaintenanceTime,
                              &WigigMac::BeamLinkMaintenanceTimeout, this);
      m_beamLinkMaintenanceTimerStateChanged(
          BEAM_LINK_MAINTENANCE_TIMER_RELEASE, m_peerStationAid,
          m_peerStationAddress, info.beamLinkMaintenanceTime);
    } else {
      m_currentLinkMaintained = false;
    }

    /* Start data transmission */
    m_edca[AC_BE]->InitiateServicePeriodTransmission();
  }
}

void WigigMac::ResumeServicePeriodTransmission() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(m_currentAllocation == SERVICE_PERIOD_ALLOCATION,
                "The current allocation is not SP");
  m_currentAllocationLength = GetRemainingAllocationTime();
  m_edca[AC_BE]->ResumeTransmission(m_currentAllocationLength);
}

void WigigMac::SuspendServicePeriodTransmission() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(m_currentAllocation == SERVICE_PERIOD_ALLOCATION,
                "The current allocation is not SP");
  m_edca[AC_BE]->DisableChannelAccess();
}

void WigigMac::EndServicePeriod() {
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG(m_currentAllocation == SERVICE_PERIOD_ALLOCATION,
                "The current allocation is not SP");
  m_servicePeriodEndedCallback(GetAddress(), m_peerStationAddress);
  m_edca[AC_BE]->EndAllocationPeriod();
  /* Inform MacLow to store parameters related to this service period
   * (MPDU/A-MPDU) */
  m_low->EndAllocationPeriod();
  /* Check if we have beamlink maintenance timer running */
  if (m_beamLinkMaintenanceTimeout.IsRunning()) {
    BeamLinkMaintenanceTableI it =
        m_beamLinkMaintenanceTable.find(m_peerStationAid);
    BeamLinkMaintenanceInfo info = it->second;
    /* We halt Beam Link Maintenance Timer */
    if (m_beamLinkMaintenanceTimeout.IsRunning()) {
      info.beamLinkMaintenanceTime =
          Simulator::GetDelayLeft(m_beamLinkMaintenanceTimeout);
      m_beamLinkMaintenanceTimeout.Cancel();
      m_beamLinkMaintenanceTimerStateChanged(
          BEAM_LINK_MAINTENANCE_TIMER_HALT, m_peerStationAid,
          m_peerStationAddress, info.beamLinkMaintenanceTime);
    } else {
      info.rest();
    }
    m_beamLinkMaintenanceTable[m_peerStationAid] = info;
  }
  m_currentLinkMaintained = false;
}

void WigigMac::AddForwardingEntry(Mac48Address nextHopAddress) {
  NS_LOG_FUNCTION(this << nextHopAddress);
  DataForwardingTableIterator it = m_dataForwardingTable.find(nextHopAddress);
  if (it == m_dataForwardingTable.end()) {
    AccessPeriodInformation info;
    info.isCbapPeriod = true;
    info.nextHopAddress = nextHopAddress;
    m_dataForwardingTable[nextHopAddress] = info;
  }
}

Time WigigMac::GetRemainingAllocationTime() const {
  return m_currentAllocationLength - (Simulator::Now() - m_allocationStarted);
}

Time WigigMac::GetRemainingSectorSweepTime() const {
  return m_sectorSweepDuration - sswTxTime -
         (Simulator::Now() - m_sectorSweepStarted);
}

/**************************************************
***************************************************
************* Beamforming Functions ***************
***************************************************
***************************************************/

void WigigMac::SetCodebook(Ptr<Codebook> codebook) { m_codebook = codebook; }

Ptr<Codebook> WigigMac::GetCodebook() const { return m_codebook; }

void WigigMac::RecordBeamformedLinkMaintenanceValue(
    BfLinkMaintenanceField field) {
  NS_LOG_FUNCTION(this);
  if (field.GetMaintenanceValue() > 0) {
    BeamLinkMaintenanceInfo maintenanceInfo;
    /* Table 8-190b—The Beamformed Link Maintenance negotiation */
    if (field.IsMaster()) {
      Time beamLinkMaintenanceTime;
      if (m_beamlinkMaintenanceUnit == UNIT_32US) {
        beamLinkMaintenanceTime =
            MicroSeconds(field.GetMaintenanceValue() * 32);
      } else {
        beamLinkMaintenanceTime =
            MicroSeconds(field.GetMaintenanceValue() * 2000);
      }
      maintenanceInfo.beamLinkMaintenanceTime = beamLinkMaintenanceTime;
      maintenanceInfo.negotiatedValue = beamLinkMaintenanceTime;
    } else {
      maintenanceInfo.beamLinkMaintenanceTime = dot11BeamLinkMaintenanceTime;
      maintenanceInfo.negotiatedValue = dot11BeamLinkMaintenanceTime;
    }
    m_beamLinkMaintenanceTable[m_peerStationAid] = maintenanceInfo;
  }
}

void WigigMac::PerformTxssTxop(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);
  m_dmgSlsTxop->InitiateTxopSectorSweep(peerAddress);
}

void WigigMac::TxssTxopAccessGranted(Mac48Address peerAddress, SlsRole slsRole,
                                     bool isFeedback) {
  NS_LOG_FUNCTION(this << peerAddress << slsRole << isFeedback);
  if (slsRole == SLS_INITIATOR) {
    /* We are the SLS initiator */
    if (!isFeedback) {
      /* Initialize Beamforming Training Parameters for TXSS TXOP and make sure
       * we have enough time to execute it */
      if (InitializeSectorSweepParameters(peerAddress)) {
        if (!m_performingBft) // This means that we've started TXSS BFT but it
                              // failed
        {
          /* Remove current Sector Sweep Information with the station we want to
           * perform beamforming training with */
          m_stationSnrMap.erase(peerAddress);
          /* Reset variables */
          m_bfRetryTimes = 0;
          m_isBeamformingInitiator = true;
          m_isInitiatorTxss = true;
          m_isResponderTxss = true;
          m_performingBft = true;
          m_peerStationAddress = peerAddress;
          m_slsInitiatorStateMachine = SLS_INITIATOR_IDLE;
        }
        if (m_restartISSEvent.IsRunning()) {
          /* This happens if we cannot continue beamforming training since the
           * allocation did not have enough time */
          m_restartISSEvent.Cancel();
        }
        /* Start Beamforming Training Training as I-TXSS */
        StartBeamformingInitiatorPhase();
      }
    } else {
      if (Simulator::Now() + SLS_FEEDBACK_PHASE_DURATION <=
          m_dtiStartTime + m_dtiDuration) {
        SendSswFbckFrame(peerAddress, sswAckTxTime + GetMbifs());
      }
    }
  } else {
    /* We are the SLS responder */
    if (InitializeSectorSweepParameters(peerAddress)) {
      m_isBeamformingInitiator = false;
      /* Start Beamforming Training Training as R-TXSS */
      StartBeamformingResponderPhase(peerAddress);
    } else {
      m_dmgSlsTxop->InitializeVariables();
    }
  }
}

void WigigMac::ResetSlsInitiatorVariables() {
  NS_LOG_FUNCTION(this);
  m_slsInitiatorStateMachine = SLS_INITIATOR_IDLE;
  m_performingBft = false;
  m_bfRetryTimes = 0;
}

void WigigMac::ResetSlsResponderVariables() {
  NS_LOG_FUNCTION(this);
  m_slsResponderStateMachine = SLS_RESPONDER_IDLE;
  m_performingBft = false;
}

void WigigMac::ResetSlsStateMachineVariables() {
  NS_LOG_FUNCTION(this);
  m_slsInitiatorStateMachine = SLS_INITIATOR_IDLE;
  m_slsResponderStateMachine = SLS_RESPONDER_IDLE;
  m_performingBft = false;
  m_bfRetryTimes = 0;
}

bool WigigMac::InitializeSectorSweepParameters(Mac48Address peerAddress) {
  NS_LOG_FUNCTION(this << peerAddress);
  NS_ASSERT_MSG(
      m_currentAllocation == CBAP_ALLOCATION,
      "Current Allocation is not CBAP and we are performing SLS within CBAP");
  /* Ensure that we have the capabilities of the peer station */
  auto peerCapabilities = GetPeerStationDmgCapabilities(peerAddress);
  NS_ASSERT_MSG(peerCapabilities.has_value(),
                "To continue beamforming we should have the capabilities of "
                "the peer station.");
  m_peerSectors = peerCapabilities.value().GetNumberOfSectors();
  m_peerAntennas = peerCapabilities.value().GetNumberOfRxDmgAntennas();
  Time duration = CalculateSectorSweepDuration(
      m_peerAntennas, m_codebook->GetTotalNumberOfAntennas(),
      m_codebook->GetTotalNumberOfTransmitSectors());
  if (Simulator::Now() + duration <= m_dtiStartTime + m_dtiDuration) {
    /* Beamforming Allocation Parameters */
    m_allocationStarted = Simulator::Now();
    m_currentAllocationLength = duration;
    return true;
  } else {
    NS_LOG_DEBUG("No enough time to complete TXSS beamforming training");
    return false;
  }
}

void WigigMac::StartBeamformingTraining(uint8_t peerAid,
                                        Mac48Address peerAddress,
                                        bool isInitiator, bool isInitiatorTXSS,
                                        bool isResponderTxss, Time length) {
  NS_LOG_FUNCTION(this << +peerAid << peerAddress << isInitiator
                       << isInitiatorTXSS << isResponderTxss << length);

  /* Ensure that we have the capabilities of the peer station */
  auto peerCapabilities = GetPeerStationDmgCapabilities(peerAddress);
  NS_ASSERT_MSG(peerCapabilities.has_value(),
                "To continue beamforming we should have the capabilities of "
                "the peer station.");
  m_peerSectors = peerCapabilities.value().GetNumberOfSectors();
  m_peerAntennas = peerCapabilities.value().GetNumberOfRxDmgAntennas();

  /* Beamforming Allocation Parameters */
  m_allocationStarted = Simulator::Now();
  m_currentAllocation = SERVICE_PERIOD_ALLOCATION;
  m_currentAllocationLength = length;
  m_peerStationAid = peerAid;
  m_peerStationAddress = peerAddress;
  m_isBeamformingInitiator = isInitiator;
  m_isInitiatorTxss = isInitiatorTXSS;
  m_isResponderTxss = isResponderTxss;

  /* Remove current Sector Sweep Information */
  m_stationSnrMap.erase(peerAddress);

  /* Reset variables */
  m_bfRetryTimes = 0;

  NS_LOG_INFO("DMG STA Initiating Beamforming with " << peerAddress << " at "
                                                     << Simulator::Now());
  StartBeamformingInitiatorPhase();
}

void WigigMac::StartBeamformingInitiatorPhase() {
  NS_LOG_FUNCTION(this);
  m_sectorSweepStarted = Simulator::Now();
  if (m_isBeamformingInitiator) {
    NS_LOG_INFO("DMG STA Starting ISS Phase with Initiator Role at "
                << Simulator::Now());
    /** We are the Initiator of the Beamforming Phase **/
    /* Schedule Beamforming Responder Phase */
    Time rssTime = CalculateSectorSweepDuration(
        m_peerAntennas, m_codebook->GetTotalNumberOfAntennas(),
        m_codebook->GetTotalNumberOfTransmitSectors());
    NS_LOG_DEBUG("Initiator: Schedulled RSS Event at "
                 << Simulator::Now() + rssTime);
    m_rssEvent =
        Simulator::Schedule(rssTime, &WigigMac::StartBeamformingResponderPhase,
                            this, m_peerStationAddress);
    if (m_isInitiatorTxss) {
      m_slsInitiatorStateMachine = SLS_INITIATOR_SECTOR_SELECTOR;
      StartTransmitSectorSweep(m_peerStationAddress, BeamformingInitiator);
    } else {
      StartReceiveSectorSweep(m_peerStationAddress, BeamformingInitiator);
    }
  } else {
    NS_LOG_INFO("DMG STA Starting ISS Phase with Responder Role at "
                << Simulator::Now());
    m_slsResponderStateMachine = SLS_RESPONDER_IDLE;
    /** We are the Responder of the Beamforming Phase **/
    if (m_isInitiatorTxss) {
      /* I-TXSS so responder stays in Quasi-Omni Receiving Mode */
      m_codebook->StartReceivingInQuasiOmniMode();

      /* If an ISS is outside the BTI and if the responder has more than one DMG
       * antenna, the initiator repeats its initiator sector sweep for the
       * number of DMG antennas indicated by the responder in the last
       * negotiated Number of RX DMG Antennas field that was transmitted by the
       * responder. Repetitions of the initiator sector sweep are separated by
       * an interval equal to LBIFS. In this case CDOWN indicates the number of
       * sectors until the end of transmission from all initiator’s DMG antennas
       * to all responder’s DMG antennas. At the start of an initiator TXSS, the
       * responder should have its first receive DMG antenna configured to a
       * quasi- omni pattern and should not change its receive antenna
       * configuration for a time corresponding to the value of the last
       * negotiated Total Number of Sectors field transmitted by the initiator
       * multiplied by the time to transmit a single SSW frame, plus appropriate
       * IFSs (10.3.2.3). After this time, the responder may switch to a
       * quasi-omni pattern in another DMG antenna. */
      if (m_codebook->GetTotalNumberOfAntennas() > 1) {
        Time switchTime =
            CalculateSingleAntennaSweepDuration(m_peerAntennas, m_peerSectors) +
            GetLbifs();
        Simulator::Schedule(switchTime, &WigigMac::SwitchQuasiOmniPattern, this,
                            switchTime);
      }
    } else {
      /* I-RXSS so the responder should have its receive antenna array
       * configured to sweep RXSS Length sectors for each of the initiator’s DMG
       * antennas while attempting to receive SSW frames from the initiator. */
      m_codebook->StartSectorSweeping(m_peerStationAddress, ReceiveSectorSweep,
                                      1);
    }
  }
}

void WigigMac::StartBeamformingResponderPhase(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  m_sectorSweepStarted = Simulator::Now();
  if (m_isBeamformingInitiator) {
    /** We are the Initiator **/
    NS_LOG_INFO("DMG STA Starting RSS Phase with Initiator Role at "
                << Simulator::Now());
    if (m_isInitiatorTxss) {
      /* We performed Initiator Transmit Sector Sweep (I-TXSS) */
      m_codebook->StartReceivingInQuasiOmniMode();

      /* If the initiator has more than one DMG antenna, the responder repeats
       * its responder sector sweep for the number of DMG antennas indicated by
       * the initiator in the last negotiated Number of RX DMG Antennas field
       * transmitted by the initiator. At the start of a responder TXSS, the
       * initiator should have its receive antenna array configured to a
       * quasi-omni antenna pattern in one of its DMG antennas for a time
       * corresponding to the value of the last negotiated Total Number of
       * Sectors field transmitted by the responder multiplied by the time to
       * transmit a single SSW frame, plus any appropriate IFSs (9.3.2.3). After
       * this time, the initiator may switch to a quasi-omni pattern in another
       * DMG antenna. */
      if (m_codebook->GetTotalNumberOfAntennas() > 1) {
        Time switchTime =
            CalculateSectorSweepDuration(m_peerSectors) + GetLbifs();
        Simulator::Schedule(switchTime, &WigigMac::SwitchQuasiOmniPattern, this,
                            switchTime);
      }
    } else {
      /* We performed Initiator Receive Sector Sweep (I-RXSS) */
      ANTENNA_CONFIGURATION_RX rxConfig =
          GetBestAntennaConfiguration(address, false, m_maxSnr);
      UpdateBestRxAntennaConfiguration(address, rxConfig, m_maxSnr);
      m_codebook->SetReceivingInDirectionalMode();
      m_codebook->SetActiveRxSectorId(rxConfig.first, rxConfig.second);
    }
  } else {
    /** We are the Responder **/
    NS_LOG_INFO("DMG STA Starting RSS Phase with Responder Role at "
                << Simulator::Now());
    /* Process the data of the Initiator phase */
    if (m_isInitiatorTxss) {
      /* Obtain antenna configuration for the highest received SNR to feed it
       * back in SSW-FBCK Field */
      m_feedbackAntennaConfig =
          GetBestAntennaConfiguration(address, true, m_maxSnr);
    }
    /* Now start doing the specified sweeping in the Responder Phase */
    if (m_isResponderTxss) {
      StartTransmitSectorSweep(address, BeamformingResponder);
    } else {
      /* The initiator is switching receive sectors at the same time */
      StartReceiveSectorSweep(address, BeamformingResponder);
    }
  }
}

void WigigMac::SwitchQuasiOmniPattern(Time switchTime) {
  NS_LOG_FUNCTION(this << switchTime);
  if (m_codebook->SwitchToNextQuasiPattern()) {
    NS_LOG_INFO("DMG STA Switching to the next quasi-omni pattern");
    Simulator::Schedule(switchTime, &WigigMac::SwitchQuasiOmniPattern, this,
                        switchTime);
  } else {
    NS_LOG_INFO("DMG STA has concluded all the quasi-omni patterns");
  }
}

void WigigMac::RestartInitiatorSectorSweep(Mac48Address stationAddress) {
  NS_LOG_FUNCTION(this << stationAddress);
  m_bfRetryTimes++;
  if (m_bfRetryTimes < dot11BFRetryLimit) {
    NS_LOG_DEBUG("BF Retry Times=" << +m_bfRetryTimes);
    if (m_currentAllocation == CBAP_ALLOCATION) {
      m_dmgSlsTxop->SectorSweepPhaseFailed();
    }
  } else {
    NS_LOG_DEBUG("Beamforming Retry Times exceeded " << dot11BFRetryLimit);
    ResetSlsInitiatorVariables();
    m_dmgSlsTxop->SlsBftFailed();
  }
}

void WigigMac::StartTransmitSectorSweep(Mac48Address address,
                                        BeamformingDirection direction) {
  NS_LOG_FUNCTION(this << address << direction);
  NS_LOG_INFO("DMG STA Starting TXSS at " << Simulator::Now());
  /* Inform the codebook to Initiate SLS phase */
  m_codebook->StartSectorSweeping(address, TransmitSectorSweep, m_peerAntennas);
  /* Calculate the correct duration for the sector sweep frame */
  m_sectorSweepDuration = CalculateSectorSweepDuration(
      m_peerAntennas, m_codebook->GetTotalNumberOfAntennas(),
      m_codebook->GetTotalNumberOfTransmitSectors());
  if (direction == BeamformingInitiator) {
    SendInitiatorTransmitSectorSweepFrame(address);
  } else {
    SendRespodnerTransmitSectorSweepFrame(address);
  }
}

void WigigMac::SendInitiatorTransmitSectorSweepFrame(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_CTL_DMG_SSW);

  /* Other Fields */
  hdr.SetAddr1(address); // MAC address of the STA that is the intended receiver
                         // of the sector sweep.
  hdr.SetAddr2(GetAddress()); // MAC address of the transmitter STA of the
                              // sector sweep frame.
  hdr.SetNoMoreFragments();
  hdr.SetNoRetry();

  Ptr<Packet> packet = Create<Packet>();
  CtrlDmgSsw sswFrame;

  DmgSswField ssw;
  ssw.SetDirection(BeamformingInitiator);
  ssw.SetCountDown(m_codebook->GetRemainingSectorCount());
  ssw.SetSectorId(m_codebook->GetActiveTxSectorId());
  ssw.SetDmgAntennaId(m_codebook->GetActiveAntennaId());

  DmgSswFbckField sswFeedback;
  sswFeedback.IsPartOfIss(true);
  sswFeedback.SetSector(m_codebook->GetTotalNumberOfTransmitSectors());
  sswFeedback.SetDmgAntenna(m_codebook->GetTotalNumberOfAntennas());
  sswFeedback.SetPollRequired(false);

  /* Set the fields in SSW Frame */
  sswFrame.SetSswField(ssw);
  sswFrame.SetSswFeedbackField(sswFeedback);
  packet->AddHeader(sswFrame);

  NS_LOG_INFO("Sending SSW Frame " << Simulator::Now() << " with AntennaId="
                                   << +ssw.GetDmgAntennaId()
                                   << ", SectorId=" << +ssw.GetSectorId());

  /* Transmit control frames directly without DCA + DCF Manager */
  TransmitControlFrame(packet, hdr, GetRemainingSectorSweepTime());
}

void WigigMac::SendRespodnerTransmitSectorSweepFrame(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_CTL_DMG_SSW);

  /* Other Fields */
  hdr.SetAddr1(address); // MAC address of the STA that is the intended receiver
                         // of the sector sweep.
  hdr.SetAddr2(GetAddress()); // MAC address of the transmitter STA of the
                              // sector sweep frame.
  hdr.SetNoMoreFragments();
  hdr.SetNoRetry();

  Ptr<Packet> packet = Create<Packet>();
  CtrlDmgSsw sswFrame;

  DmgSswField ssw;
  ssw.SetDirection(BeamformingResponder);
  ssw.SetCountDown(m_codebook->GetRemainingSectorCount());
  ssw.SetSectorId(m_codebook->GetActiveTxSectorId());
  ssw.SetDmgAntennaId(m_codebook->GetActiveAntennaId());

  DmgSswFbckField sswFeedback;
  sswFeedback.IsPartOfIss(false);
  sswFeedback.SetSector(m_feedbackAntennaConfig.second);
  sswFeedback.SetDmgAntenna(m_feedbackAntennaConfig.first);
  sswFeedback.SetPollRequired(false);
  sswFeedback.SetSnrReport(m_maxSnr);

  /* Set the fields in SSW Frame */
  sswFrame.SetSswField(ssw);
  sswFrame.SetSswFeedbackField(sswFeedback);
  packet->AddHeader(sswFrame);

  NS_LOG_INFO("Sending SSW Frame " << Simulator::Now() << " with AntennaId="
                                   << +ssw.GetDmgAntennaId()
                                   << ", SectorId=" << +ssw.GetSectorId());

  /* Transmit control frames directly without DCA + DCF Manager */
  TransmitControlFrame(packet, hdr, GetRemainingSectorSweepTime());
}

void WigigMac::TransmitControlFrame(Ptr<const Packet> packet,
                                    WigigMacHeader &hdr, Time duration) {
  NS_LOG_FUNCTION(this << packet << &hdr << duration);
  if ((m_accessPeriod == CHANNEL_ACCESS_DTI) &&
      (m_currentAllocation == CBAP_ALLOCATION)) {
    m_dmgSlsTxop->TransmitFrame(packet, hdr, duration);
  } else if ((m_accessPeriod == CHANNEL_ACCESS_ABFT) ||
             (m_currentAllocation == SERVICE_PERIOD_ALLOCATION)) {
    TransmitControlFrameImmediately(packet, hdr, duration);
  }
}

void WigigMac::TransmitControlFrameImmediately(Ptr<const Packet> packet,
                                               WigigMacHeader &hdr,
                                               Time duration) {
  NS_LOG_FUNCTION(this << packet << &hdr << duration);
  /* Send Frame immediately without DCA + DCF Manager */
  MacLowTransmissionParameters params;
  params.EnableOverrideDurationId(duration);
  params.DisableRts();
  params.DisableAck();
  params.DisableNextData();
  m_low->StartTransmissionImmediately(Create<WigigMacQueueItem>(packet, hdr),
                                      params,
                                      MakeCallback(&WigigMac::FrameTxOk, this));
}

void WigigMac::StartReceiveSectorSweep(Mac48Address address,
                                       BeamformingDirection direction) {
  NS_LOG_FUNCTION(this << address << direction);
  NS_LOG_INFO("DMG STA Starting RXSS with " << address);

  /* A RXSS may be requested only when an initiator/respodner is aware of the
   * capabilities of a responder/initiator, which includes the RXSS Length
   * field. */
  auto peerCapabilities = GetPeerStationDmgCapabilities(address);
  if (!peerCapabilities.has_value()) {
    NS_LOG_LOGIC("Cannot start RXSS since the DMG Capabilities of the peer "
                 "station is not available");
    return;
  }

  uint8_t RXSSLength = peerCapabilities.value().GetRxssLength();
  if (direction == BeamformingInitiator) {
    /* During the initiator RXSS, the initiator shall transmit from each of the
     * initiator’s DMG antennas the number of BF frames indicated by the
     * responder in the last negotiated RXSS Length field transmitted by the
     * responder. Each transmitted BF frame shall be transmitted with the same
     * fixed antenna sector or pattern. The initiator shall set the Sector ID
     * and DMG Antenna ID fields in each transmitted BF frame to a value that
     * uniquely identifies the single sector through which the BF frame is
     * transmitted. */
    m_totalSectors = (m_codebook->GetTotalNumberOfAntennas() * RXSSLength) - 1;
  } else {
    /* During the responder RXSS, the responder shall transmit the number of SSW
     * frames indicated by the initiator in the initiator’s most recently
     * transmitted RXSS Length field (non-A-BFT) or FSS field (A-BFT) from each
     * of the responder’s DMG antennas, each time with the same antenna sector
     * or pattern fixed for all SSW frames transmission originating from the
     * same DMG antenna. */
    if (m_accessPeriod == CHANNEL_ACCESS_ABFT) {
      m_totalSectors = std::min(static_cast<uint>(RXSSLength - 1),
                                static_cast<uint>(m_ssFramesPerSlot - 1));
    } else {
      m_totalSectors =
          (m_codebook->GetTotalNumberOfAntennas() * RXSSLength) - 1;
    }
  }

  STATION_ANTENNA_CONFIG_MAP::iterator it = m_bestAntennaConfig.find(address);
  if (it != m_bestAntennaConfig.end()) {
    /* Change Tx Antenna Configuration */
    ANTENNA_CONFIGURATION_TX antennaConfigTx =
        std::get<0>(m_bestAntennaConfig[address]);
    m_codebook->SetActiveTxSectorId(antennaConfigTx.first,
                                    antennaConfigTx.second);
  } else {
    NS_LOG_DEBUG("Cannot start RXSS since no antenna configuration available "
                 "for DMG STA="
                 << address);
    return;
  }

  SendReceiveSectorSweepFrame(address, m_totalSectors, direction);
}

void WigigMac::SendReceiveSectorSweepFrame(Mac48Address address, uint16_t count,
                                           BeamformingDirection direction) {
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_CTL_DMG_SSW);

  /* Header Duration*/
  hdr.SetDuration(GetRemainingAllocationTime());

  /* Other Fields */
  hdr.SetAddr1(address); // MAC address of the STA that is the intended receiver
                         // of the sector sweep.
  hdr.SetAddr2(GetAddress()); // MAC address of the transmitter STA of the
                              // sector sweep frame.
  hdr.SetNoMoreFragments();
  hdr.SetNoRetry();

  Ptr<Packet> packet = Create<Packet>();
  CtrlDmgSsw sswFrame;

  DmgSswField ssw;
  ssw.SetDirection(direction);
  ssw.SetCountDown(count);
  ssw.SetSectorId(m_codebook->GetActiveTxSectorId());
  ssw.SetDmgAntennaId(m_codebook->GetActiveAntennaId());

  DmgSswFbckField sswFeedback;
  sswFeedback.IsPartOfIss(true);
  sswFeedback.SetSector(m_codebook->GetRemainingSectorCount());
  sswFeedback.SetDmgAntenna(m_codebook->GetTotalNumberOfAntennas());
  sswFeedback.SetPollRequired(false);

  /* Set the fields in SSW Frame */
  sswFrame.SetSswField(ssw);
  sswFrame.SetSswFeedbackField(sswFeedback);
  packet->AddHeader(sswFrame);

  NS_LOG_INFO("Sending SSW Frame "
              << Simulator::Now()
              << " with AntennaId=" << +m_codebook->GetActiveAntennaId()
              << ", SectorId=" << +m_codebook->GetActiveTxSectorId());

  /* Transmit control frames directly without DCA + DCF Manager */
  TransmitControlFrameImmediately(packet, hdr, GetRemainingSectorSweepTime());
}

void WigigMac::SendSswFbckFrame(Mac48Address receiver, Time duration) {
  NS_LOG_FUNCTION(this << receiver << duration);
  if (m_channelAccessManager->CanAccess()) {
    WigigMacHeader hdr;
    hdr.SetType(WIFI_MAC_CTL_DMG_SSW_FBCK);
    hdr.SetAddr1(receiver);     // Receiver.
    hdr.SetAddr2(GetAddress()); // Transmitter.

    Ptr<Packet> packet = Create<Packet>();
    packet->AddHeader(hdr);

    CtrlDmgSswFbck fbck;      // SSW-FBCK Frame.
    DmgSswFbckField feedback; // SSW-FBCK Field.

    if (m_isResponderTxss) {
      /* Responder is TXSS so obtain antenna configuration for the highest
       * received SNR to feed it back */
      m_feedbackAntennaConfig =
          GetBestAntennaConfiguration(receiver, true, m_maxSnr);
      feedback.IsPartOfIss(false);
      feedback.SetSector(m_feedbackAntennaConfig.second);
      feedback.SetDmgAntenna(m_feedbackAntennaConfig.first);
      feedback.SetSnrReport(m_maxSnr);
    } else {
      /* At the start of an SSW ACK, the initiator should have its receive
       * antenna array configured to a quasi-omni antenna pattern using the DMG
       * antenna through which it received with the highest quality during the
       * RSS, or the best receive sector if an RXSS has been performed during
       * the RSS, and should not change its receive antenna configuration while
       * it attempts to receive from the responder until the expected end of the
       * SSW ACK. */
      ANTENNA_CONFIGURATION_RX rxConfig =
          GetBestAntennaConfiguration(receiver, false, m_maxSnr);
      UpdateBestRxAntennaConfiguration(receiver, rxConfig, m_maxSnr);
      m_codebook->SetReceivingInDirectionalMode();
      m_codebook->SetActiveRxSectorId(rxConfig.first, rxConfig.second);
    }

    BrpRequestField request;
    /* Currently, we do not support MID + BC Subphases */
    request.SetMidReq(false);
    request.SetBcReq(false);

    BfLinkMaintenanceField maintenance;
    maintenance.SetUnitIndex(m_beamlinkMaintenanceUnit);
    maintenance.SetMaintenanceValue(m_beamlinkMaintenanceValue);
    maintenance.SetAsMaster(true); /* Master of data transfer */

    fbck.SetSswFeedbackField(feedback);
    fbck.SetBrpRequestField(request);
    fbck.SetBfLinkMaintenanceField(maintenance);

    packet->AddHeader(fbck);

    /* Reset Feedback Flag */
    m_sectorFeedbackScheduled = false;
    NS_LOG_INFO("Sending SSW-FBCK Frame to " << receiver << " at "
                                             << Simulator::Now());

    /* The SSW-Feedback frame shall be transmitted through the sector identified
     * by the value of the Sector Select field and DMG Antenna Select field
     * received from the responder during the preceding responder TXSS. */
    SteerTxAntennaToward(receiver);

    /* Transmit control frames directly without the Channel Access Manager */
    TransmitControlFrame(packet, hdr, duration);
  } else {
    NS_LOG_INFO("Medium is busy, Abort Sending SSW-FBCK");
  }
}

void WigigMac::ResendSswFbckFrame() {
  NS_LOG_FUNCTION(this);
  /* The initiator may restart the SSW Feedback up to dot11BFRetryLimit times if
   * it does not receive an SSW-ACK frame from the responder in MBIFS time
   * following the completion of the SSW Feedback. The initiator shall restart
   * the SSW Feedback PIFS time following the expected end of the SSW ACK by the
   * responder, provided there is sufficient time left in the allocation for the
   * initiator to begin the SSW Feedback followed by an SSW ACK from the
   * responder in SIFS time. If there is not sufficient time left in the
   * allocation for the completion of the SSW Feedback and SSW ACK, the
   * initiator shall restart the SSW Feedback at the start of the following
   * allocation between the initiator and the responder.*/
  m_bfRetryTimes++;
  if (m_bfRetryTimes < dot11BFRetryLimit) {
    Time sswFbckDuration;
    if (m_currentAllocation == CBAP_ALLOCATION) {
      sswFbckDuration = sswAckTxTime + GetMbifs();
    } else {
      sswFbckDuration = GetRemainingAllocationTime();
    }
    Simulator::Schedule(GetPifs(), &DmgSlsTxop::RxSswAckFailed, m_dmgSlsTxop);
  } else {
    NS_LOG_DEBUG("Beamforming Retry Times exceeded " << dot11BFRetryLimit);
    ResetSlsInitiatorVariables();
    m_dmgSlsTxop->SlsBftFailed();
  }
}

void WigigMac::SendSswAckFrame(Mac48Address receiver, Time sswFbckDuration) {
  NS_LOG_FUNCTION(this << receiver << sswFbckDuration);
  /* send a SSW Feedback when you receive a SSW Slot after MBIFS. */
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_CTL_DMG_SSW_ACK);
  hdr.SetAddr1(receiver);     // Receiver.
  hdr.SetAddr2(GetAddress()); // Transmitter.
  /* The Duration field is set until the end of the current allocation */
  Time sswAck = sswAckTxTime;
  Time duration = sswFbckDuration - (GetMbifs() + sswAck);
  NS_ASSERT(duration.IsPositive());

  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(hdr);

  CtrlDmgSswFbck ackFrame;  // SSW-ACK Frame.
  DmgSswFbckField feedback; // SSW-FBCK Field.

  if (m_isInitiatorTxss) {
    /* Initiator is TXSS so obtain antenna configuration for the highest
     * received SNR to feed it back */
    m_feedbackAntennaConfig =
        GetBestAntennaConfiguration(receiver, true, m_maxSnr);
    feedback.IsPartOfIss(false);
    feedback.SetSector(m_feedbackAntennaConfig.second);
    feedback.SetDmgAntenna(m_feedbackAntennaConfig.first);
    feedback.SetSnrReport(m_maxSnr);
  }

  BrpRequestField request;
  request.SetMidReq(false);
  request.SetBcReq(false);

  BfLinkMaintenanceField maintenance;
  maintenance.SetUnitIndex(m_beamlinkMaintenanceUnit);
  maintenance.SetMaintenanceValue(m_beamlinkMaintenanceValue);
  maintenance.SetAsMaster(false); /* Slave of data transfer */

  ackFrame.SetSswFeedbackField(feedback);
  ackFrame.SetBrpRequestField(request);
  ackFrame.SetBfLinkMaintenanceField(maintenance);

  packet->AddHeader(ackFrame);
  NS_LOG_INFO("Sending SSW-ACK Frame to " << receiver << " at "
                                          << Simulator::Now());

  /* Set the best sector for transmission */
  SteerAntennaToward(receiver);

  /* Transmit control frames directly without DCA + DCF Manager */
  TransmitControlFrame(packet, hdr, duration);
}

void WigigMac::MapTxSnr(Mac48Address address, AntennaId RxAntennaId,
                        AntennaId TxAntennaId, SectorId sectorID, double snr) {
  NS_LOG_FUNCTION(this << address << +RxAntennaId << +TxAntennaId << +sectorID
                       << RatioToDb(snr));
  STATION_SNR_PAIR_MAP_I it = m_stationSnrMap.find(address);
  ANTENNA_CONFIGURATION_COMBINATION config =
      std::make_tuple(RxAntennaId, TxAntennaId, sectorID);
  if (it != m_stationSnrMap.end()) {
    SNR_PAIR *snrPair = (SNR_PAIR *)(&it->second);
    SNR_MAP_TX *snrMap = (SNR_MAP_TX *)(&snrPair->first);
    (*snrMap)[config] = snr;
  } else {
    SNR_MAP_TX snrTx;
    SNR_MAP_RX snrRx;
    snrTx[config] = snr;
    SNR_PAIR snrPair = std::make_pair(snrTx, snrRx);
    m_stationSnrMap[address] = snrPair;
  }
}

void WigigMac::MapTxSnr(Mac48Address address, AntennaId antennaID,
                        SectorId sectorID, double snr) {
  MapTxSnr(address, m_codebook->GetActiveAntennaId(), antennaID, sectorID, snr);
}

void WigigMac::MapRxSnr(Mac48Address address, AntennaId antennaID,
                        SectorId sectorID, double snr) {
  NS_LOG_FUNCTION(this << address << +antennaID << +sectorID << snr);
  STATION_SNR_PAIR_MAP::iterator it = m_stationSnrMap.find(address);
  ANTENNA_CONFIGURATION_COMBINATION config =
      std::make_tuple(m_codebook->GetActiveAntennaId(), antennaID, sectorID);
  if (it != m_stationSnrMap.end()) {
    SNR_PAIR *snrPair = (SNR_PAIR *)(&it->second);
    SNR_MAP_RX *snrMap = (SNR_MAP_RX *)(&snrPair->second);
    (*snrMap)[config] = snr;
  } else {
    SNR_MAP_TX snrTx;
    SNR_MAP_RX snrRx;
    snrRx[config] = snr;
    SNR_PAIR snrPair = std::make_pair(snrTx, snrRx);
    m_stationSnrMap[address] = snrPair;
  }
}

/* Information Request and Response Exchange */

void WigigMac::SendInformationRequest(Mac48Address to, Ptr<Packet> packet) {
  NS_LOG_FUNCTION(this << to);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.dmgAction = WifiActionHeader::DMG_INFORMATION_REQUEST;
  actionHdr.SetAction(WifiActionHeader::DMG, action);

  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

void WigigMac::SendInformationResponse(Mac48Address to, Ptr<Packet> packet) {
  NS_LOG_FUNCTION(this << to);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(to);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.dmgAction = WifiActionHeader::DMG_INFORMATION_RESPONSE;
  actionHdr.SetAction(WifiActionHeader::DMG, action);

  packet->AddHeader(actionHdr);

  m_txop->Queue(packet, hdr);
}

void WigigMac::SteerTxAntennaToward(Mac48Address address, bool isData) {
  NS_LOG_FUNCTION(this << address);
  SteerSisoTxAntennaToward(address);
}

void WigigMac::SteerAntennaToward(Mac48Address address, bool isData) {
  NS_LOG_FUNCTION(this << address);
  SteerSisoAntennaToward(address);
}

void WigigMac::SteerSisoTxAntennaToward(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  STATION_ANTENNA_CONFIG_MAP::iterator it = m_bestAntennaConfig.find(address);
  if (it != m_bestAntennaConfig.end()) {
    ANTENNA_CONFIGURATION_TX antennaConfigTx =
        std::get<0>(m_bestAntennaConfig[address]);
    /* Change Tx Antenna Configuration */
    NS_LOG_DEBUG("Change Transmit Antenna Sector Config to AntennaId="
                 << +antennaConfigTx.first
                 << ", SectorId=" << +antennaConfigTx.second);
    m_codebook->SetActiveTxSectorId(antennaConfigTx.first,
                                    antennaConfigTx.second);
    /* Check if there is a AWV TX configuration saved for the STA - if there is
     * set the TX AWV ID*/
    STATION_Awv_MAP::iterator it = m_bestAwvConfig.find(address);
    if (it != m_bestAwvConfig.end()) {
      BEST_AwvId *antennaConfig = &(it->second);
      if (antennaConfig->first != NO_AwvId) {
        m_codebook->SetActiveTxAwvId(antennaConfig->first);
      }
    }
  } else {
    NS_LOG_DEBUG("No antenna configuration available for DMG STA=" << address);
  }
}

void WigigMac::SteerSisoAntennaToward(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  STATION_ANTENNA_CONFIG_MAP::iterator it = m_bestAntennaConfig.find(address);
  if (it != m_bestAntennaConfig.end()) {
    ANTENNA_CONFIGURATION_TX antennaConfigTx =
        std::get<0>(m_bestAntennaConfig[address]);
    ANTENNA_CONFIGURATION_RX antennaConfigRx =
        std::get<1>(m_bestAntennaConfig[address]);

    /* Change Tx Antenna Configuration */
    NS_LOG_DEBUG("Change Transmit Antenna Config to AntennaId="
                 << +antennaConfigTx.first
                 << ", SectorId=" << +antennaConfigTx.second);

    m_codebook->SetActiveTxSectorId(antennaConfigTx.first,
                                    antennaConfigTx.second);
    /* Check if there is a AWV TX configuration saved for the STA - if there is
     * set the TX AWV ID*/
    STATION_Awv_MAP::iterator it = m_bestAwvConfig.find(address);
    if (it != m_bestAwvConfig.end()) {
      BEST_AwvId *antennaConfig = &(it->second);
      if (antennaConfig->first != NO_AwvId) {
        m_codebook->SetActiveTxAwvId(antennaConfig->first);
      }
    }
    /* Change Rx Antenna Configuration */
    if ((antennaConfigRx.first != NO_ANTENNA_CONFIG) &&
        (antennaConfigRx.second != NO_ANTENNA_CONFIG) && m_useRxSectors) {
      NS_LOG_DEBUG("Change Receive Antenna Config to AntennaId="
                   << +antennaConfigRx.first
                   << ", SectorId=" << +antennaConfigRx.second);
      m_codebook->SetReceivingInDirectionalMode();
      m_codebook->SetActiveRxSectorId(antennaConfigRx.first,
                                      antennaConfigRx.second);
      /* Check if there is a AWV RX configuration saved for the STA - if there
       * is set the RX AWV ID*/
      if (it != m_bestAwvConfig.end()) {
        BEST_AwvId *antennaConfig = &(it->second);
        if (antennaConfig->second != NO_AwvId) {
          m_codebook->SetActiveRxAwvId(antennaConfig->second);
        }
      }
    } else {
      m_codebook->SetReceivingInQuasiOmniMode();
    }
  } else {
    NS_LOG_DEBUG(
        "No Tx/Rx antenna configuration available for DMG STA=" << address);
    m_codebook->SetReceivingInQuasiOmniMode();
  }
}

RelayCapabilitiesInfo WigigMac::GetRelayCapabilitiesInfo() const {
  RelayCapabilitiesInfo info;
  info.SetRelaySupportability(m_rdsActivated);
  info.SetRelayUsability(m_redsActivated);
  info.SetRelayPermission(true); /* Used by PCP/AP only */
  info.SetAcPower(true);
  info.SetRelayPreference(true);
  info.SetDuplex(m_relayDuplexMode);
  info.SetCooperation(false); /* Only Link Switching Type supported */
  return info;
}

RelayCapabilitiesElement WigigMac::GetRelayCapabilitiesElement() const {
  RelayCapabilitiesElement relayElement;
  RelayCapabilitiesInfo info = GetRelayCapabilitiesInfo();
  relayElement.SetRelayCapabilitiesInfo(info);
  return relayElement;
}

/**
 * Functions for Beam Refinement Protocol Setup and Transaction Execution.
 */
void WigigMac::SendEmptyBrpFrame(Mac48Address receiver,
                                 BrpRequestField &requestField,
                                 BeamRefinementElement &element) {
  NS_LOG_FUNCTION(this << receiver);
  SendBrpFrame(receiver, requestField, element, false, TRN_T, 0);
}

void WigigMac::SendBrpFrame(Mac48Address receiver,
                            BrpRequestField &requestField,
                            BeamRefinementElement &element,
                            bool requestBeamRefinement, PacketType packetType,
                            uint8_t trainingFieldLength) {
  NS_LOG_FUNCTION(this << receiver);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION_NO_ACK);
  hdr.SetAddr1(receiver);
  hdr.SetAddr2(GetAddress());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();
  hdr.SetFragmentNumber(0);
  hdr.SetNoMoreFragments();
  hdr.SetNoRetry();

  /* Special Fields for Beam Refinement */
  if (requestBeamRefinement) {
    hdr.SetPacketType(packetType);
    hdr.SetTrainingFieldLength(trainingFieldLength);
  }

  ExtBrpFrame brpFrame;
  brpFrame.SetDialogToken(0);
  brpFrame.SetBrpRequestField(requestField);

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.unprotectedDmgAction = WifiActionHeader::UNPROTECTED_DMG_BRP;
  actionHdr.SetAction(WifiActionHeader::UNPROTECTED_DMG, action);

  brpFrame.Get<BeamRefinementElement>() = element;

  Ptr<Packet> packet = Create<Packet>();
  // packet->AddHeader (element);
  packet->AddHeader(brpFrame);
  packet->AddHeader(actionHdr);

  /* Set the best sector for tansmission with this station */
  ANTENNA_CONFIGURATION_TX antennaConfigTx =
      std::get<0>(m_bestAntennaConfig[receiver]);
  m_codebook->SetActiveTxSectorId(antennaConfigTx.first,
                                  antennaConfigTx.second);

  NS_LOG_INFO("Sending BRP Frame to "
              << receiver << " at " << Simulator::Now()
              << " with AntennaId=" << +antennaConfigTx.first
              << " SectorId=" << +antennaConfigTx.second);

  if (m_accessPeriod == CHANNEL_ACCESS_ATI) {
    m_dmgAtiTxop->Queue(packet, hdr);
  } else {
    /* Transmit control frames directly without DCA + DCF Manager */
    TransmitControlFrameImmediately(packet, hdr, MicroSeconds(0));
  }
}

/*
 *  Currently, we use BRP to train receive sector since we did not have RXSS in
 * A-BFT.
 */
void WigigMac::InitiateBrpSetupSubphase(BrpTrainingType type,
                                        Mac48Address receiver) {
  NS_LOG_FUNCTION(this);
  NS_LOG_LOGIC("Initiating BRP Setup Subphase with " << receiver << " at "
                                                     << Simulator::Now());

  /* Set flags related to the BRP Setup Subphase */
  m_isBrpSetupCompleted[receiver] = false;
  m_raisedBrpSetupCompleted[receiver] = false;

  BeamRefinementElement element;
  /* The BRP Setup subphase starts with the initiator sending BRP with
   * Capability Request = 1 */
  element.SetAsBeamRefinementInitiator(true);
  element.SetCapabilityRequest(true);

  BrpRequestField requestField;
  /* Currently, we do not support MID + BC Subphases */
  requestField.SetMidReq(false);
  requestField.SetBcReq(false);
  if ((type == BRP_TRN_R) || (type == BRP_TRN_T_R)) {
    requestField.SetLRx(m_codebook->GetTotalNumberOfReceiveSectors());
  }
  if ((type == BRP_TRN_T) || (type == BRP_TRN_T_R)) {
    requestField.SetTxTrnReq(true);
    element.SetSnrRequested(true);
    element.SetChannelMeasurementRequested(true);
    element.SetNumberOfTapsRequested(TAPS_1);
    element.SetSectorIdOrderRequested(true);
  }
  requestField.SetTxSectorId(m_codebook->GetActiveTxSectorId());
  requestField.SetTxAntennaId(m_codebook->GetActiveAntennaId());

  SendEmptyBrpFrame(receiver, requestField, element);
}

void WigigMac::ReportSnrValue(AntennaId antennaID, SectorId sectorID,
                              uint8_t trnUnitsRemaining,
                              uint8_t subfieldsRemaining,
                              uint8_t pSubfieldsRemaining, double snr,
                              bool isTxTrn) {
  NS_LOG_FUNCTION(this << +antennaID << +sectorID << +subfieldsRemaining
                       << +trnUnitsRemaining << snr << isTxTrn);
  if (m_recordTrnSnrValues) {
    /* Add the SNR of the TRN Subfield */
    m_trn2Snr.push_back(snr);

    /* Check if this is the last TRN Subfield, so we extract the best Tx/RX
     * sector/Awv */
    if ((trnUnitsRemaining == 0) && (subfieldsRemaining == 0) &&
        (pSubfieldsRemaining == 0)) {
      /* Iterate over all the SNR values and get the ID of the AWV with the
       * highest AWVs */
      uint8_t awvID =
          std::distance(m_trn2Snr.begin(),
                        std::max_element(m_trn2Snr.begin(), m_trn2Snr.end()));
      m_recordTrnSnrValues = false;

      if (isTxTrn) {
        /* Feedback the ID of the best AWV ID for TRN-TX */
        BrpRequestField requestField;
        requestField.SetTxAntennaId(m_codebook->GetActiveAntennaId());
        requestField.SetTxSectorId(m_codebook->GetActiveTxSectorId());

        BeamRefinementElement element;
        element.SetTxTrainResponse(true);
        element.SetTxTrnOk(true);
        element.SetBsFbck(awvID);
        element.SetBsFbckAntennaId(antennaID);

        Simulator::Schedule(GetSifs(), &WigigMac::SendEmptyBrpFrame, this,
                            m_peerStation, requestField, element);

        NS_LOG_INFO(
            "Received last TRN-T Subfield for transmit beam refinement from "
            << m_peerStation << " to " << GetAddress()
            << ". Send BRP Feedback with Best AWV ID=" << +awvID
            << " with SNR=" << RatioToDb(m_trn2Snr[awvID]) << " dB");
      } else {
        NS_LOG_INFO(
            "Received last TRN-R Subfield for receive beam refinement from "
            << GetAddress() << " to " << m_peerStation
            << ". Best AWV ID=" << +awvID);
      }

      m_trn2Snr.clear();
    }
  }
}

void WigigMac::InitiateBrpTransaction(Mac48Address receiver, uint8_t lRx,
                                      bool txTrnReq) {
  NS_LOG_FUNCTION(this << receiver << +lRx << txTrnReq);
  NS_LOG_INFO("Start BRP Transactions with " << receiver << " at "
                                             << Simulator::Now());

  BrpRequestField requestField;
  requestField.SetMidReq(false);
  requestField.SetBcReq(false);
  requestField.SetLRx(lRx);
  requestField.SetTxTrnReq(txTrnReq);
  requestField.SetTxAntennaId(m_codebook->GetActiveAntennaId());
  requestField.SetTxSectorId(m_codebook->GetActiveTxSectorId());

  BeamRefinementElement element;
  element.SetAsBeamRefinementInitiator(true);
  element.SetTxTrainResponse(false);
  element.SetRxTrainResponse(false);
  element.SetTxTrnOk(false);
  element.SetCapabilityRequest(false);

  /* Transaction Information */
  m_peerStation = receiver;

  if (txTrnReq) {
    /* Inform the codebook to start iterating over the custom AWVs within this
     * sector */
    m_codebook->InitiateBrp(m_codebook->GetActiveAntennaId(),
                            m_codebook->GetActiveTxSectorId(),
                            RefineTransmitSector);
    /* Request transmit training */
    SendBrpFrame(
        receiver, requestField, element, true, TRN_T,
        m_codebook->GetNumberOfAwvs(m_codebook->GetActiveAntennaId(),
                                    m_codebook->GetActiveTxSectorId()));
  } else {
    /* Request receive training */
    SendEmptyBrpFrame(receiver, requestField, element);
  }
}

Time WigigMac::GetSectorSweepDuration(uint8_t sectors) {
  return ((sswTxTime)*sectors + GetSbifs() * (sectors - 1));
}

Time WigigMac::GetSectorSweepSlotTime(uint8_t fss) {
  Time sswFbck = sswFbckTxTime;
  Time time = aAirPropagationTime +
              GetSectorSweepDuration(fss) /* aSSDuration */
              + sswFbck + GetMbifs() * 2;
  time = MicroSeconds(ceil(static_cast<double>(time.GetNanoSeconds()) / 1000));
  return time;
}

Time WigigMac::CalculateSectorSweepDuration(uint8_t sectors) {
  Time ssw = sswTxTime;
  Time duration = (sectors - 1) * GetSbifs();
  duration += sectors * ssw;
  return duration;
}

Time WigigMac::CalculateSingleAntennaSweepDuration(uint8_t antennas,
                                                   uint8_t sectors) {
  Time ssw = sswTxTime;
  Time duration = Seconds(0);
  duration +=
      (antennas - 1) *
      GetLbifs(); /* Inter-time for switching between different antennas. */
  duration += static_cast<uint16_t>((sectors - antennas)) * GetSbifs();
  duration += static_cast<uint16_t>(sectors) * ssw;
  return MicroSeconds(
      (ceil(static_cast<double>(duration.GetNanoSeconds()) / 1000)));
}

Time WigigMac::CalculateSectorSweepDuration(uint8_t peerAntennas,
                                            uint8_t myAntennas,
                                            uint8_t mySectors) {
  Time ssw = sswTxTime;
  Time duration = Seconds(0);
  duration +=
      (myAntennas * peerAntennas - 1) *
      GetLbifs(); /* Inter-time for switching between different antennas. */
  duration += static_cast<uint16_t>((mySectors - myAntennas) * peerAntennas) *
              GetSbifs();
  duration += static_cast<uint16_t>(mySectors * peerAntennas) * ssw;
  duration += GetMbifs();
  return MicroSeconds(
      (ceil(static_cast<double>(duration.GetNanoSeconds()) / 1000)));
}

Time WigigMac::CalculateTotalBeamformingTrainingDuration(
    uint8_t initiatorAntennas, uint8_t initiatorSectors,
    uint8_t responderAntennas, uint8_t responderSectors) {
  Time ssw = sswTxTime;
  Time sswFbck = sswFbckTxTime;
  Time sswAck = sswAckTxTime;
  Time duration = Seconds(0);
  duration += (initiatorAntennas * responderAntennas - 1) *
              GetLbifs(); /* Initiator: Inter-time for switching between
                             different antennas. */
  duration += static_cast<uint16_t>((initiatorSectors - initiatorAntennas) *
                                    responderAntennas) *
              GetSbifs();
  duration += static_cast<uint16_t>(initiatorSectors * responderAntennas) *
              (ssw + aAirPropagationTime);
  duration += GetMbifs(); /* Inter-time between Initiator and Responder */
  duration += (initiatorAntennas * responderAntennas - 1) *
              GetLbifs(); /* Responder: Inter-time for switching between
                             different antennas. */
  duration += static_cast<uint16_t>((responderSectors - responderAntennas) *
                                    initiatorAntennas) *
              GetSbifs();
  duration += static_cast<uint16_t>(responderSectors * initiatorAntennas) *
              (ssw + aAirPropagationTime);
  duration += sswFbck + sswAck + 2 * (GetMbifs() + aAirPropagationTime);
  return duration;
}

void WigigMac::StorePeerDmgCapabilities(Ptr<WigigMac> wifiMac) {
  NS_LOG_FUNCTION(this << wifiMac->GetAddress());
  m_informationMap[wifiMac->GetAddress()] = wifiMac->GetDmgCapabilities();
  MapAidToMacAddress(wifiMac->GetAssociationId(), wifiMac->GetAddress());
  m_stationManager->AddStationDmgCapabilities(wifiMac->GetAddress(),
                                              wifiMac->GetDmgCapabilities());
}

std::optional<DmgCapabilities>
WigigMac::GetPeerStationDmgCapabilities(Mac48Address stationAddress) const {
  NS_LOG_FUNCTION(this << stationAddress);
  InformationMapCI it = m_informationMap.find(stationAddress);
  if (it != m_informationMap.end()) {
    /* We already have information about the DMG STA */
    // StationInformation info = static_cast<StationInformation> (it->second);
    return it->second;
  } else {
    return std::nullopt;
  }
}

Time WigigMac::ComputeBeamformingAllocationSize(Mac48Address responderAddress,
                                                bool isInitiatorTXSS,
                                                bool isResponderTxss) {
  NS_LOG_FUNCTION(this << responderAddress << isInitiatorTXSS
                       << isResponderTxss);
  // An initiator shall determine the capabilities of the responder prior to
  // initiating BF training with the responder if the responder is associated. A
  // STA may obtain the capabilities of other STAs through the Information
  // Request and Information Response frames (10.29.1) or following a STA’s
  // association with the PBSS/infrastructure BSS. The initiator should use its
  // own capabilities and the capabilities of the responder to compute the
  // required allocation size to perform BF training and BF training related
  // timeouts.
  auto capabilities = GetPeerStationDmgCapabilities(responderAddress);
  if (capabilities.has_value()) {
    uint8_t initiatorSectors;
    uint8_t responderSectors;
    if (isInitiatorTXSS && isResponderTxss) {
      initiatorSectors = m_codebook->GetTotalNumberOfTransmitSectors();
      responderSectors = capabilities.value().GetNumberOfSectors();
    } else if (isInitiatorTXSS && !isResponderTxss) {
      initiatorSectors = m_codebook->GetTotalNumberOfTransmitSectors();
      responderSectors = m_codebook->GetTotalNumberOfReceiveSectors();
    } else if (!isInitiatorTXSS && isResponderTxss) {
      initiatorSectors = capabilities.value().GetRxssLength();
      responderSectors = capabilities.value().GetNumberOfSectors();
    } else {
      initiatorSectors = capabilities.value().GetRxssLength();
      responderSectors = m_codebook->GetTotalNumberOfReceiveSectors();
    }
    NS_LOG_DEBUG("InitiatorSectors=" << +initiatorSectors
                                     << ", ResponderSectors="
                                     << +responderSectors);
    return CalculateTotalBeamformingTrainingDuration(
        m_codebook->GetTotalNumberOfAntennas(), initiatorSectors,
        capabilities.value().GetNumberOfRxDmgAntennas(), responderSectors);
  } else {
    return NanoSeconds(0);
  }
}

void WigigMac::UpdateBestTxAntennaConfiguration(
    const Mac48Address stationAddress, ANTENNA_CONFIGURATION_TX antennaConfigTx,
    double snr) {
  NS_LOG_FUNCTION(this << stationAddress << snr);
  STATION_ANTENNA_CONFIG_MAP_I it = m_bestAntennaConfig.find(stationAddress);
  if (it != m_bestAntennaConfig.end()) {
    BEST_ANTENNA_CONFIGURATION *antennaConfig = &(it->second);
    std::get<0>(*antennaConfig) = antennaConfigTx;
    std::get<2>(*antennaConfig) = snr;
  } else {
    ANTENNA_CONFIGURATION_RX antennaConfigRx =
        std::make_pair(NO_ANTENNA_CONFIG, NO_ANTENNA_CONFIG);
    m_bestAntennaConfig[stationAddress] =
        std::make_tuple(antennaConfigTx, antennaConfigRx, snr);
  }
}

void WigigMac::UpdateBestRxAntennaConfiguration(
    const Mac48Address stationAddress, ANTENNA_CONFIGURATION_RX antennaConfigRx,
    double snr) {
  NS_LOG_FUNCTION(this << stationAddress << snr);
  STATION_ANTENNA_CONFIG_MAP_I it = m_bestAntennaConfig.find(stationAddress);
  if (it != m_bestAntennaConfig.end()) {
    BEST_ANTENNA_CONFIGURATION *antennaConfig = &(it->second);
    std::get<1>(*antennaConfig) = antennaConfigRx;
    std::get<2>(*antennaConfig) = snr;
  } else {
    ANTENNA_CONFIGURATION_RX antennaConfigTx =
        std::make_pair(NO_ANTENNA_CONFIG, NO_ANTENNA_CONFIG);
    m_bestAntennaConfig[stationAddress] =
        std::make_tuple(antennaConfigTx, antennaConfigRx, snr);
  }
}

ANTENNA_CONFIGURATION
WigigMac::GetBestAntennaConfiguration(const Mac48Address stationAddress,
                                      bool isTxConfiguration) {
  double maxSnr;
  return GetBestAntennaConfiguration(stationAddress, isTxConfiguration, maxSnr);
}

ANTENNA_CONFIGURATION
WigigMac::GetBestAntennaConfiguration(const Mac48Address stationAddress,
                                      bool isTxConfiguration, double &maxSnr) {
  SNR_PAIR snrPair = m_stationSnrMap[stationAddress];
  SNR_MAP snrMap;
  if (isTxConfiguration) {
    snrMap = snrPair.first;
  } else {
    snrMap = snrPair.second;
  }

  SNR_MAP::iterator highIter = snrMap.begin();
  auto snr = highIter->second;
  maxSnr = snr;
  for (SNR_MAP::iterator iter = snrMap.begin(); iter != snrMap.end(); iter++) {
    if (snr < iter->second) {
      highIter = iter;
      snr = highIter->second;
      maxSnr = snr;
    }
  }
  ANTENNA_CONFIGURATION_COMBINATION config = highIter->first;
  return std::make_pair(std::get<1>(config), std::get<2>(config));
}

void WigigMac::ManagementTxOk(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  /* We need check which ActionFrame it is */
  if (hdr.IsActionNoAck()) {
    if (!m_isBrpSetupCompleted[hdr.GetAddr1()]) {
      /* We finished transmitting BRP Frame in setup phase, switch to quasi omni
       * mode for receiving */
      m_codebook->SetReceivingInQuasiOmniMode();
    } else if (m_isBrpSetupCompleted[hdr.GetAddr1()] &&
               !m_raisedBrpSetupCompleted[hdr.GetAddr1()]) {
      /* BRP Setup is completed from the initiator side */
      m_raisedBrpSetupCompleted[hdr.GetAddr1()] = true;
      BrpSetupCompleted(hdr.GetAddr1());
    }

    if (m_requestedBrpTraining && (GetTypeOfStation() == AP)) {
      /* If we finished BRP Phase i.e. Receive Sector Training, then start BRP
       * with another station*/
      m_requestedBrpTraining = false;
      NotifyBrpPhaseCompleted();
    }
  }
}

void WigigMac::FrameTxOk(const WigigMacHeader &hdr) { NS_LOG_FUNCTION(this); }

void WigigMac::TxOk(Ptr<const Packet> currentPacket,
                    const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this);
  m_txOkCallback(hdr);
  if (m_currentLinkMaintained && hdr.IsQosData() &&
      m_currentAllocation == SERVICE_PERIOD_ALLOCATION) {
    /* Report the current value */
    m_beamLinkMaintenanceTimerStateChanged(
        BEAM_LINK_MAINTENANCE_TIMER_RESET, m_peerStationAid,
        m_peerStationAddress,
        Simulator::GetDelayLeft(m_beamLinkMaintenanceTimeout));
    /* Setup and release the new timer */
    m_beamLinkMaintenanceTimeout.Cancel();
    m_linkMaintenanceInfo.rest();
    m_beamLinkMaintenanceTimeout =
        Simulator::Schedule(m_linkMaintenanceInfo.beamLinkMaintenanceTime,
                            &WigigMac::BeamLinkMaintenanceTimeout, this);
    m_beamLinkMaintenanceTimerStateChanged(
        BEAM_LINK_MAINTENANCE_TIMER_SETUP_RELEASE, m_peerStationAid,
        m_peerStationAddress, m_linkMaintenanceInfo.beamLinkMaintenanceTime);
  }
}

void WigigMac::ReceiveSectorSweepFrame(Ptr<Packet> packet,
                                       const WigigMacHeader *hdr) {
  NS_LOG_FUNCTION(this << packet);

  CtrlDmgSsw sswFrame;
  packet->RemoveHeader(sswFrame);

  /* Ensure that we do not receive sectors from other stations while we are
   * already performing BFT with a particular station */
  if ((m_performingBft) && (m_peerStationAddress != hdr->GetAddr2())) {
    NS_LOG_INFO("Received SSW frame different DMG STA=" << hdr->GetAddr2());
    return;
  }

  DmgSswField ssw = sswFrame.GetSswField();
  DmgSswFbckField sswFeedback = sswFrame.GetSswFeedbackField();

  if (ssw.GetDirection() == BeamformingInitiator) {
    NS_LOG_LOGIC("Responder: Received SSW frame as part of ISS from Initiator="
                 << hdr->GetAddr2());
    if (m_rssEvent.IsExpired()) {
      Time rssTime = hdr->GetDuration();
      if (m_currentAllocation == CBAP_ALLOCATION) {
        /* We received the first SSW from the initiator during CBAP allocation,
         * so we initialize variables. */
        /* Remove current Sector Sweep Information with the station we want to
         * perform beamforming training with */
        m_stationSnrMap.erase(hdr->GetAddr2());
        /* Initialize some of the BFT variables here */
        m_isInitiatorTxss = true;
        m_isResponderTxss = true;
        /* Lock the responder on this initiator */
        m_performingBft = true;
        m_low->SlsPhaseStarted();
        m_peerStationAddress = hdr->GetAddr2();
        /* Cancel any SSW-FBCK frame timeout (This might happen if the initiator
           does not receive SSW frames from the responder but the respodner
           already schedulled SSW-FBCK timeout event.) */
        if (m_sswFbckTimeout.IsRunning()) {
          m_sswFbckTimeout.Cancel();
        }
        m_rssEvent =
            Simulator::Schedule(rssTime, &DmgSlsTxop::StartResponderSectorSweep,
                                m_dmgSlsTxop, hdr->GetAddr2());
      } else {
        m_rssEvent = Simulator::Schedule(
            rssTime, &WigigMac::StartBeamformingResponderPhase, this,
            hdr->GetAddr2());
      }
      NS_LOG_LOGIC("Scheduled RSS Period for Responder="
                   << GetAddress() << " at " << Simulator::Now() + rssTime);
    }

    if (m_isInitiatorTxss) {
      /* Initiator is TXSS and we store SNR to report it back to the initiator
       */
      MapTxSnr(hdr->GetAddr2(), ssw.GetDmgAntennaId(), ssw.GetSectorId(),
               m_stationManager->GetRxSnr());
    } else {
      /* Initiator is RXSS and we store SNR to select the best Rx Sector with
       * the initiator */
      MapRxSnr(hdr->GetAddr2(), m_codebook->GetActiveAntennaId(),
               m_codebook->GetActiveRxSectorId(), m_stationManager->GetRxSnr());
    }
  } else {
    NS_LOG_LOGIC("Initiator: Received SSW frame as part of RSS from Responder="
                 << hdr->GetAddr2());

    /* If we receive one SSW Frame at least from the responder, then we schedule
     * SSW-FBCK */
    if (!m_sectorFeedbackScheduled) {
      NS_LOG_DEBUG("Cancel Restart ISS event.");
      m_restartISSEvent.Cancel();
      m_sectorFeedbackScheduled = true;

      /* The SSW Frame we received is part of RSS */
      /* Not part of ISS i.e. the SSW Feedback Field Contains the Feedbeck of
       * the ISS */
      sswFeedback.IsPartOfIss(false);

      /* Set the best TX antenna configuration reported by the SSW-FBCK Field */
      if (m_isInitiatorTxss) {
        /* The Sector Sweep Frame contains feedback about the the best Tx Sector
         * used by the initiator */
        ANTENNA_CONFIGURATION_TX antennaConfigTx = std::make_pair(
            sswFeedback.GetDmgAntenna(), sswFeedback.GetSector());
        UpdateBestTxAntennaConfiguration(hdr->GetAddr2(), antennaConfigTx,
                                         sswFeedback.GetSnrReport());
        NS_LOG_LOGIC("Best TX Antenna Sector Config by this DMG STA to DMG STA="
                     << hdr->GetAddr2()
                     << ": AntennaId=" << +antennaConfigTx.first
                     << ", SectorId=" << +antennaConfigTx.second);
      }

      Time sswFbckStartTime;
      if (m_currentAllocation == CBAP_ALLOCATION) {
        sswFbckStartTime =
            GetSectorSweepDuration(ssw.GetCountDown()) + GetMbifs();
        Simulator::Schedule(sswFbckStartTime,
                            &DmgSlsTxop::StartInitiatorFeedback, m_dmgSlsTxop,
                            hdr->GetAddr2());
      } else {
        Time sswFbckDuration = GetRemainingAllocationTime();
        sswFbckStartTime = hdr->GetDuration();
        Simulator::Schedule(sswFbckStartTime, &WigigMac::SendSswFbckFrame, this,
                            hdr->GetAddr2(), sswFbckDuration);
      }
      NS_LOG_LOGIC("Scheduled SSW-FBCK Frame to "
                   << hdr->GetAddr2() << " at "
                   << Simulator::Now() + sswFbckStartTime);
    }

    if (m_isResponderTxss) {
      /* Responder is TXSS and we store SNR to report it back to the responder
       */
      MapTxSnr(hdr->GetAddr2(), ssw.GetDmgAntennaId(), ssw.GetSectorId(),
               m_stationManager->GetRxSnr());
    } else {
      /* Responder is RXSS and we store SNR to select the best Rx Sector with
       * the responder */
      MapRxSnr(hdr->GetAddr2(), m_codebook->GetActiveAntennaId(),
               m_codebook->GetActiveRxSectorId(), m_stationManager->GetRxSnr());
    }
  }
}

void WigigMac::Receive(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);

  const WigigMacHeader *hdr = &mpdu->GetHeader();
  Ptr<Packet> packet = mpdu->GetPacket()->Copy();
  Mac48Address to = hdr->GetAddr1();
  Mac48Address from = hdr->GetAddr2();

  if (hdr->IsSsw()) {
    ReceiveSectorSweepFrame(packet, hdr);
    return;
  } else if (hdr->IsSswAck()) {
    NS_LOG_LOGIC("Initiator: Received SSW-ACK frame from=" << from);

    /* We are the SLS Initiator */
    CtrlDmgSswAck sswAck;
    packet->RemoveHeader(sswAck);

    /* Check Beamformed link maintenance */
    RecordBeamformedLinkMaintenanceValue(sswAck.GetBfLinkMaintenanceField());

    /* We add the station to the list of the stations we can directly
     * communicate with */
    AddForwardingEntry(from);

    /* Cancel SSW-Feedback timer */
    m_sswAckTimeoutEvent.Cancel();

    /* Get best antenna configuration */
    Mac48Address address = from;
    BEST_ANTENNA_CONFIGURATION info = m_bestAntennaConfig[address];
    ANTENNA_CONFIGURATION_TX antennaConfigTx = std::get<0>(info);

    m_slsInitiatorStateMachine = SLS_INITIATOR_TXSS_PHASE_COMPELTED;

    /* Raise a callback indicating we've completed the SLS phase */
    m_slsCompleted(SlsCompletionAttrbitutes(
        from, CHANNEL_ACCESS_DTI, BeamformingInitiator, m_isInitiatorTxss,
        m_isResponderTxss, m_bftIdMap[from], antennaConfigTx.first,
        antennaConfigTx.second, m_maxSnr));

    /* Inform DMG SLS TXOP that we've received the SSW-ACK frame */
    m_dmgSlsTxop->SlsBftCompleted();

    /* Check if we need to start BRP phase following SLS phase */
    BrpRequestField brpRequest = sswAck.GetBrpRequestField();
    if ((brpRequest.GetLRx() > 0) || brpRequest.GetTxTrnReq()) {
      /* BRP setup sub-phase is skipped in this case*/
      InitiateBrpTransaction(from, brpRequest.GetLRx(),
                             brpRequest.GetTxTrnReq());
    }

    /* Resume data transmission after SLS operation */
    if (m_currentAllocation == CBAP_ALLOCATION) {
      m_txop->ResumeTxopTransmission();
      for (EdcaQueues::iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
        i->second->ResumeTxopTransmission();
      }
    }

    return;
  } else if (hdr->IsAction() || hdr->IsActionNoAck()) {
    WifiActionHeader actionHdr;
    packet->RemoveHeader(actionHdr);

    switch (actionHdr.GetCategory()) {
    case WifiActionHeader::BLOCK_ACK:
      packet->AddHeader(actionHdr);
      break;
    case WifiActionHeader::DMG:
      switch (actionHdr.GetAction().dmgAction) {
      case WifiActionHeader::DMG_RELAY_ACK_REQUEST: {
        ExtRelaySearchRequestHeader requestHdr;
        packet->RemoveHeader(requestHdr);
        return;
      }
      case WifiActionHeader::DMG_RELAY_ACK_RESPONSE: {
        ExtRelaySearchResponseHeader responseHdr;
        packet->RemoveHeader(responseHdr);
        return;
      }
      default:
        NS_FATAL_ERROR("Unsupported Action frame received");
        return;
      }

    case WifiActionHeader::UNPROTECTED_DMG:
      switch (actionHdr.GetAction().unprotectedDmgAction) {
      case WifiActionHeader::UNPROTECTED_DMG_ANNOUNCE: {
        ExtAnnounceFrame announceHdr;
        packet->RemoveHeader(announceHdr);
        return;
      }

      case WifiActionHeader::UNPROTECTED_DMG_BRP: {
        ExtBrpFrame brpFrame;
        packet->PeekHeader(brpFrame);

        // WifiInformationElementContainer informationElements;
        // packet->RemoveHeader (informationElements);

        BrpRequestField requestField;
        requestField = brpFrame.GetBrpRequestField();

        /* We are in BRP Transaction state */
        if (requestField.GetTxTrnReq()) {
          /* We are the responder of BRP-TX, so we record the SNR values of
           * TRN-Tx */
          m_recordTrnSnrValues = true;
          m_peerStation = from;
        } else {
          std::optional<BeamRefinementElement> beamRefinementElement =
              brpFrame.Get<BeamRefinementElement>();
          NS_ASSERT(beamRefinementElement);
          if (beamRefinementElement->IsTxTrainResponse()) {
            /* We received reply for TRN-Tx Training */
            m_brpCompleted(from, RefineTransmitSector,
                           m_codebook->GetActiveAntennaId(),
                           m_codebook->GetActiveTxSectorId(),
                           beamRefinementElement->GetBsFbck());
          }
        }
        return;
      }

      default:
        packet->AddHeader(actionHdr);
        break;
      }
    default:
      packet->AddHeader(actionHdr);
      break;
    }
  }

  // We don't know how to deal with any frame that is not addressed to
  // us (and odds are there is nothing sensible we could do anyway),
  // so we ignore such frames.
  //
  // The derived class may also do some such filtering, but it doesn't
  // hurt to have it here too as a backstop.
  if (to != GetAddress()) {
    return;
  }

  if (hdr->IsMgt() && hdr->IsAction()) {
    WifiActionHeader actionHdr;
    packet->RemoveHeader(actionHdr);

    switch (actionHdr.GetCategory()) {
    case WifiActionHeader::BLOCK_ACK:

      switch (actionHdr.GetAction().blockAck) {
      case WifiActionHeader::BLOCK_ACK_ADDBA_REQUEST: {
        MgtAddBaRequestHeader reqHdr;
        packet->RemoveHeader(reqHdr);

        // We've received an ADDBA Request. Our policy here is
        // to automatically accept it, so we get the ADDBA
        // Response on it's way immediately.
        SendAddBaResponse(&reqHdr, from);
        // This frame is now completely dealt with, so we're done.
        return;
      }
      case WifiActionHeader::BLOCK_ACK_ADDBA_RESPONSE: {
        MgtAddBaResponseHeader respHdr;
        packet->RemoveHeader(respHdr);

        // We've received an ADDBA Response. We assume that it
        // indicates success after an ADDBA Request we have
        // sent (we could, in principle, check this, but it
        // seems a waste given the level of the current model)
        // and act by locally establishing the agreement on
        // the appropriate queue.
        AcIndex ac = QosUtilsMapTidToAc(respHdr.GetTid());
        m_edca[ac]->GotAddBaResponse(&respHdr, from);
        // This frame is now completely dealt with, so we're done.
        return;
      }
      case WifiActionHeader::BLOCK_ACK_DELBA: {
        MgtDelBaHeader delBaHdr;
        packet->RemoveHeader(delBaHdr);

        if (delBaHdr.IsByOriginator()) {
          // This DELBA frame was sent by the originator, so
          // this means that an ingoing established
          // agreement exists in MacLow and we need to
          // destroy it.
          m_low->DestroyWigigBlockAckAgreement(from, delBaHdr.GetTid());
        } else {
          // We must have been the originator. We need to
          // tell the correct queue that the agreement has
          // been torn down
          AcIndex ac = QosUtilsMapTidToAc(delBaHdr.GetTid());
          m_edca[ac]->GotDelBaFrame(&delBaHdr, from);
        }
        // This frame is now completely dealt with, so we're done.
        return;
      }
      default:
        NS_FATAL_ERROR("Unsupported Action field in Block Ack Action frame");
        return;
      }
    default:
      NS_FATAL_ERROR("Unsupported Action frame received");
      return;
    }
  }
  NS_FATAL_ERROR("Don't know how to handle frame (type=" << hdr->GetType());
}

void WigigMac::DeaggregateAmsduAndForward(Ptr<WigigMacQueueItem> mpdu) {
  NS_LOG_FUNCTION(this << *mpdu);
  for (auto &msduPair : *PeekPointer(mpdu)) {
    ForwardUp(msduPair.first, msduPair.second.GetSourceAddr(),
              msduPair.second.GetDestinationAddr());
  }
}

void WigigMac::SendAddBaResponse(const MgtAddBaRequestHeader *reqHdr,
                                 Mac48Address originator) {
  NS_LOG_FUNCTION(this);
  WigigMacHeader hdr;
  hdr.SetType(WIFI_MAC_MGT_ACTION);
  hdr.SetAddr1(originator);
  hdr.SetAddr2(GetAddress());
  hdr.SetAddr3(GetBssid());
  hdr.SetDsNotFrom();
  hdr.SetDsNotTo();
  hdr.SetNoOrder();

  MgtAddBaResponseHeader respHdr;
  StatusCode code;
  code.SetSuccess();
  respHdr.SetStatusCode(code);
  // Here a control about queues type?
  respHdr.SetAmsduSupport(reqHdr->IsAmsduSupported());

  if (reqHdr->IsImmediateBlockAck()) {
    respHdr.SetImmediateBlockAck();
  } else {
    respHdr.SetDelayedBlockAck();
  }
  respHdr.SetTid(reqHdr->GetTid());
  respHdr.SetBufferSize(63);
  respHdr.SetTimeout(reqHdr->GetTimeout());

  WifiActionHeader actionHdr;
  WifiActionHeader::ActionValue action;
  action.blockAck = WifiActionHeader::BLOCK_ACK_ADDBA_RESPONSE;
  actionHdr.SetAction(WifiActionHeader::BLOCK_ACK, action);

  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader(respHdr);
  packet->AddHeader(actionHdr);

  // We need to notify our MacLow object as it will have to buffer all
  // correctly received packets for this Block Ack session
  m_low->CreateWigigBlockAckAgreement(&respHdr, originator,
                                      reqHdr->GetStartingSequence());

  // It is unclear which queue this frame should go into. For now we
  // bung it into the queue corresponding to the TID for which we are
  // establishing an agreement, and push it to the head.
  m_edca[QosUtilsMapTidToAc(reqHdr->GetTid())]->PushFront(packet, hdr);
}

void WigigMac::TxFailed(const WigigMacHeader &hdr) {
  NS_LOG_FUNCTION(this << hdr);
  m_txErrCallback(hdr);
}

void WigigMac::EnableAggregation() {
  NS_LOG_FUNCTION(this);
  if (!m_low->GetWigigMsduAggregator()) {
    Ptr<WigigMsduAggregator> msduAggregator =
        CreateObject<WigigMsduAggregator>();
    msduAggregator->SetEdcaQueues(m_edca);
    m_low->SetMsduAggregator(msduAggregator);
  }
  if (!m_low->GetWigigMpduAggregator()) {
    Ptr<WigigMpduAggregator> mpduAggregator =
        CreateObject<WigigMpduAggregator>();
    mpduAggregator->SetEdcaQueues(m_edca);
    m_low->SetMpduAggregator(mpduAggregator);
  }
}

void WigigMac::DisableAggregation() {
  NS_LOG_FUNCTION(this);
  m_low->SetMsduAggregator(nullptr);
  m_low->SetMpduAggregator(nullptr);
}

void WigigMac::SetVoBlockAckThreshold(uint8_t threshold) {
  NS_LOG_FUNCTION(this << +threshold);
  GetVOQueue()->SetBlockAckThreshold(threshold);
}

void WigigMac::SetViBlockAckThreshold(uint8_t threshold) {
  NS_LOG_FUNCTION(this << +threshold);
  GetVIQueue()->SetBlockAckThreshold(threshold);
}

void WigigMac::SetBeBlockAckThreshold(uint8_t threshold) {
  NS_LOG_FUNCTION(this << +threshold);
  GetBEQueue()->SetBlockAckThreshold(threshold);
}

void WigigMac::SetBkBlockAckThreshold(uint8_t threshold) {
  NS_LOG_FUNCTION(this << +threshold);
  GetBKQueue()->SetBlockAckThreshold(threshold);
}

void WigigMac::SetVoBlockAckInactivityTimeout(uint16_t timeout) {
  NS_LOG_FUNCTION(this << timeout);
  GetVOQueue()->SetBlockAckInactivityTimeout(timeout);
}

void WigigMac::SetViBlockAckInactivityTimeout(uint16_t timeout) {
  NS_LOG_FUNCTION(this << timeout);
  GetVIQueue()->SetBlockAckInactivityTimeout(timeout);
}

void WigigMac::SetBeBlockAckInactivityTimeout(uint16_t timeout) {
  NS_LOG_FUNCTION(this << timeout);
  GetBEQueue()->SetBlockAckInactivityTimeout(timeout);
}

void WigigMac::SetBkBlockAckInactivityTimeout(uint16_t timeout) {
  NS_LOG_FUNCTION(this << timeout);
  GetBKQueue()->SetBlockAckInactivityTimeout(timeout);
}

void WigigMac::SetupEdcaQueue(AcIndex ac) {
  NS_LOG_FUNCTION(this << ac);

  // Our caller shouldn't be attempting to setup a queue that is
  // already configured.
  NS_ASSERT(m_edca.find(ac) == m_edca.end());

  Ptr<WigigQosTxop> edca = CreateObject<WigigQosTxop>();
  edca->SetMacLow(m_low);
  edca->SetChannelAccessManager(m_channelAccessManager);
  edca->SetTxMiddle(m_txMiddle);
  edca->SetTxOkCallback(MakeCallback(&WigigMac::TxOk, this));
  edca->SetTxFailedCallback(MakeCallback(&WigigMac::TxFailed, this));
  edca->SetTxDroppedCallback(MakeCallback(&WigigMac::NotifyTxDrop, this));
  edca->SetAccessCategory(ac);
  edca->CompleteConfig();

  m_edca.insert(std::make_pair(ac, edca));
}

void WigigMac::SetTypeOfStation(TypeOfStation type) {
  NS_LOG_FUNCTION(this << type);
  for (EdcaQueues::const_iterator i = m_edca.begin(); i != m_edca.end(); ++i) {
    i->second->SetTypeOfStation(type);
  }
  m_type = type;
}

TypeOfStation WigigMac::GetTypeOfStation() const {
  NS_LOG_FUNCTION(this);
  return m_type;
}

Ptr<WigigTxop> WigigMac::GetTxop() const { return m_txop; }

Ptr<WigigQosTxop> WigigMac::GetVOQueue() const {
  return m_edca.find(AC_VO)->second;
}

Ptr<WigigQosTxop> WigigMac::GetVIQueue() const {
  return m_edca.find(AC_VI)->second;
}

Ptr<WigigQosTxop> WigigMac::GetBEQueue() const {
  return m_edca.find(AC_BE)->second;
}

Ptr<WigigQosTxop> WigigMac::GetBKQueue() const {
  return m_edca.find(AC_BK)->second;
}

void WigigMac::SetForwardUpCallback(ForwardUpCallback upCallback) {
  NS_LOG_FUNCTION(this);
  m_forwardUp = upCallback;
}

void WigigMac::SetLinkUpCallback(Callback<void> linkUp) {
  NS_LOG_FUNCTION(this);
  m_linkUp = linkUp;
}

void WigigMac::SetLinkDownCallback(Callback<void> linkDown) {
  NS_LOG_FUNCTION(this);
  m_linkDown = linkDown;
}

void WigigMac::SetDmgSupported(bool enable) {
  NS_LOG_FUNCTION(this);
  m_dmgSupported = enable;
  if (enable) {
    EnableAggregation();
  } else {
    DisableAggregation();
  }
}

bool WigigMac::GetDmgSupported() const { return m_dmgSupported; }

void WigigMac::SetCtsToSelfSupported(bool enable) {
  NS_LOG_FUNCTION(this);
  m_low->SetCtsToSelfSupported(enable);
}

void WigigMac::SetSlot(Time slotTime) {
  NS_LOG_FUNCTION(this << slotTime);
  m_channelAccessManager->SetSlot(slotTime);
  m_low->SetSlotTime(slotTime);
}

Time WigigMac::GetSlot() const { return m_low->GetSlotTime(); }

void WigigMac::SetSifs(Time sifs) {
  NS_LOG_FUNCTION(this << sifs);
  m_channelAccessManager->SetSifs(sifs);
  m_low->SetSifs(sifs);
}

Time WigigMac::GetSifs() const { return m_low->GetSifs(); }

void WigigMac::SetEifsNoDifs(Time eifsNoDifs) {
  NS_LOG_FUNCTION(this << eifsNoDifs);
  m_channelAccessManager->SetEifsNoDifs(eifsNoDifs);
}

Time WigigMac::GetEifsNoDifs() const {
  return m_channelAccessManager->GetEifsNoDifs();
}

void WigigMac::SetPifs(Time pifs) {
  NS_LOG_FUNCTION(this << pifs);
  m_low->SetPifs(pifs);
}

Time WigigMac::GetPifs() const { return m_low->GetPifs(); }

void WigigMac::SetAckTimeout(Time ackTimeout) {
  NS_LOG_FUNCTION(this << ackTimeout);
  m_low->SetAckTimeout(ackTimeout);
}

Time WigigMac::GetAckTimeout() const { return m_low->GetAckTimeout(); }

void WigigMac::SetBasicBlockAckTimeout(Time blockAckTimeout) {
  NS_LOG_FUNCTION(this << blockAckTimeout);
  m_low->SetBasicBlockAckTimeout(blockAckTimeout);
}

Time WigigMac::GetBasicBlockAckTimeout() const {
  return m_low->GetBasicBlockAckTimeout();
}

void WigigMac::SetCompressedBlockAckTimeout(Time blockAckTimeout) {
  NS_LOG_FUNCTION(this << blockAckTimeout);
  m_low->SetCompressedBlockAckTimeout(blockAckTimeout);
}

Time WigigMac::GetCompressedBlockAckTimeout() const {
  return m_low->GetCompressedBlockAckTimeout();
}

void WigigMac::SetAddress(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  m_low->SetAddress(address);
}

Mac48Address WigigMac::GetAddress() const { return m_low->GetAddress(); }

void WigigMac::SetSsid(Ssid ssid) {
  NS_LOG_FUNCTION(this << ssid);
  m_ssid = ssid;
}

Ssid WigigMac::GetSsid() const { return m_ssid; }

void WigigMac::SetBssid(Mac48Address bssid) {
  NS_LOG_FUNCTION(this << bssid);
  m_low->SetBssid(bssid);
}

Mac48Address WigigMac::GetBssid() const { return m_low->GetBssid(); }

void WigigMac::SetPromisc() { m_low->SetPromisc(); }

void WigigMac::Enqueue(Ptr<Packet> packet, Mac48Address to, Mac48Address from) {
  // We expect WigigMac subclasses which do support forwarding (e.g.,
  // AP) to override this method. Therefore, we throw a fatal error if
  // someone tries to invoke this method on a class which has not done
  // this.
  NS_FATAL_ERROR("This MAC entity ("
                 << this << ", " << GetAddress()
                 << ") does not support Enqueue() with from address");
}

bool WigigMac::SupportsSendFrom() const { return false; }

void WigigMac::ForwardUp(Ptr<const Packet> packet, Mac48Address from,
                         Mac48Address to) {
  NS_LOG_FUNCTION(this << packet << from << to);
  m_forwardUp(packet, from, to);
}

} // namespace ns3
