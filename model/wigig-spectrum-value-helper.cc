/*
 * Copyright (c) 2015-2021 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-spectrum-value-helper.h"

#include <cmath>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigSpectrumValueHelper");

static const double WIGIG_BAND_BANDWIDTH_HZ = 5156250;
static const uint16_t DMG_CHANNEL_WIDTH_MHZ = 2160;

Ptr<SpectrumValue>
WigigSpectrumValueHelper::CreateSingleCarrierTxPowerSpectralDensity(
    uint32_t centerFrequency, double txPowerW, double guardBandwidth,
    uint8_t ncb) {
  NS_LOG_FUNCTION(centerFrequency << txPowerW << guardBandwidth);
  Ptr<SpectrumValue> c = Create<SpectrumValue>(
      GetSpectrumModel(centerFrequency, DMG_CHANNEL_WIDTH_MHZ,
                       WIGIG_BAND_BANDWIDTH_HZ, guardBandwidth));
  uint32_t nGuardBands = static_cast<uint32_t>(
      ((2 * guardBandwidth * 1e6) / WIGIG_BAND_BANDWIDTH_HZ) + 0.5);
  uint32_t nAllocatedBands = static_cast<uint32_t>(
      ((DMG_CHANNEL_WIDTH_MHZ * 1e6) / WIGIG_BAND_BANDWIDTH_HZ) + 0.5);
  double txPowerPerBandW = 0.0;
  uint32_t start1 = (nGuardBands / 2);
  uint32_t stop1 = start1 + nAllocatedBands - 1;
  // Prepare spectrum mask specific variables
  uint32_t flatRegionWidth = static_cast<uint32_t>(
      (((ncb - 1) * 1080e6 + 940e6) / WIGIG_BAND_BANDWIDTH_HZ) +
      0.5); // size in number of subcarriers in the flat region
  uint32_t innerSlopeWidth = static_cast<uint32_t>(
      (((ncb - 1) * 120e6 + 260e6) / WIGIG_BAND_BANDWIDTH_HZ) +
      0.5); // size in number of subcarriers of the 0dBr<->20dBr slope
  uint32_t middleSlopeWidth = static_cast<uint32_t>(
      ((ncb * 1500e6) / WIGIG_BAND_BANDWIDTH_HZ) +
      0.5); // size in number of subcarriers of the 17dBr<->22dBr slope
  uint32_t outerSlopeWidth = static_cast<uint32_t>(
      ((ncb * 360e6) / WIGIG_BAND_BANDWIDTH_HZ) +
      0.5); // size in number of subcarriers of the 22dBr<->30dBr slope
  std::vector<WifiSpectrumBandIndices>
      subBands; // list of data/pilot-containing subBands (sent at 0dBr)
  WifiSpectrumBandIndices maskBand(0, nAllocatedBands + nGuardBands);
  txPowerPerBandW = txPowerW / nAllocatedBands;
  subBands.emplace_back(start1, stop1);
  // Build transmit spectrum mask
  CreateSpectrumMask(c, subBands, maskBand, txPowerPerBandW, nGuardBands,
                     flatRegionWidth, innerSlopeWidth, middleSlopeWidth,
                     outerSlopeWidth);
  NormalizeSpectrumMask(c, txPowerW);
  NS_ASSERT_MSG(std::abs(txPowerW - Integral(*c)) < 1e-6,
                "Power allocation failed");
  return c;
}

Ptr<SpectrumValue>
WigigSpectrumValueHelper::CreateDmgOfdmTxPowerSpectralDensity(
    uint32_t centerFrequency, double txPowerW, double guardBandwidth) {
  NS_LOG_FUNCTION(centerFrequency << txPowerW << guardBandwidth);
  const double bandBandwidth =
      5156250; /* DMG OFDM subcarrier frequency spacing 5.15625 MHz (= 2640 MHz
                  / 512) */
  Ptr<SpectrumValue> c = Create<SpectrumValue>(GetSpectrumModel(
      centerFrequency, DMG_CHANNEL_WIDTH_MHZ, bandBandwidth, guardBandwidth));
  Values::iterator vit = c->ValuesBegin();
  Bands::const_iterator bit = c->ConstBandsBegin();
  // Normally, there are 355 subcarriers for 802.11ad
  // 16 pilots, 3 DC and 336 data
  // The pilots are located on carriers 10, 30, 50, 70, 90, 110, 130 and 150,
  // The three DC carriers are located in the middle (-1, 0 and +1) and are
  // suppressed (nulled)
  NS_ASSERT_MSG(c->GetSpectrumModel()->GetNumBands() == 355,
                "Unexpected number of bands");
  const double txPowerPerBand = txPowerW / 355;
  for (size_t i = 0; i < c->GetSpectrumModel()->GetNumBands();
       i++, vit++, bit++) {
    if ((i >= 0 && i <= 9) || (i >= 11 && i <= 29) || (i >= 31 && i <= 49) ||
        (i >= 51 && i <= 69) || (i >= 71 && i <= 89) || (i >= 91 && i <= 109) ||
        (i >= 111 && i <= 129) || (i >= 131 && i <= 149) || (i >= 151)) {
      *vit = txPowerPerBand / (bit->fh - bit->fl);
    } else if ((i == 0) || (i == 1)) {
      *vit = 0;
    } else {
      *vit = txPowerPerBand / (bit->fh - bit->fl);
    }
  }
  NS_LOG_DEBUG("Integrated power " << Integral(*c));
  NS_ASSERT_MSG(std::abs(txPowerW - Integral(*c)) < 1e-6,
                "Power allocation failed");
  return c;
}

Ptr<SpectrumValue> WigigSpectrumValueHelper::CreateRfFilter(
    uint32_t centerFrequency, uint16_t channelWidth, uint32_t bandBandwidth,
    uint16_t guardBandwidth) {
  NS_LOG_FUNCTION(centerFrequency << channelWidth << bandBandwidth
                                  << guardBandwidth);
  Ptr<SpectrumValue> c = Create<SpectrumValue>(GetSpectrumModel(
      centerFrequency, channelWidth, bandBandwidth, guardBandwidth));
  size_t numBands = c->GetSpectrumModel()->GetNumBands();
  Bands::const_iterator bit = c->ConstBandsBegin();
  Values::iterator vit = c->ValuesBegin();
  size_t numBandsInFilter =
      static_cast<size_t>(channelWidth * 1e6 / bandBandwidth);
  if (numBandsInFilter % 2 == 0) {
    numBandsInFilter += 1;
  }
  NS_LOG_INFO("Num bands in filter: " << numBandsInFilter);
  // Set the value of the filter to 1 for the center-most numBandsInFilter
  NS_ASSERT_MSG((numBandsInFilter % 2 == 1) && (numBands % 2 == 1),
                "Should have odd number of bands");
  size_t startIndex = (numBands - numBandsInFilter) / 2;
  vit += startIndex;
  bit += startIndex;
  for (size_t i = startIndex; i < startIndex + numBandsInFilter;
       i++, vit++, bit++) {
    *vit = 1;
  }
  NS_LOG_LOGIC("Added subbands " << startIndex << " to "
                                 << startIndex + numBandsInFilter
                                 << " to filter");
  return c;
}

void WigigSpectrumValueHelper::CreateSpectrumMask(
    Ptr<SpectrumValue> c,
    std::vector<WifiSpectrumBandIndices> allocatedSubBands,
    WifiSpectrumBandIndices maskBand, double txPowerPerBandW,
    uint32_t nGuardBands, uint32_t flatRegionWidth, uint32_t innerSlopeWidth,
    uint32_t middleSlopeWidth, uint32_t outerSlopeWidth) {
  NS_LOG_FUNCTION(c << allocatedSubBands.front().first
                    << allocatedSubBands.back().second << maskBand.first
                    << maskBand.second << txPowerPerBandW << nGuardBands
                    << innerSlopeWidth);

  uint32_t numSubBands = allocatedSubBands.size();
  uint32_t numBands = c->GetSpectrumModel()->GetNumBands();
  uint32_t numMaskBands = maskBand.second - maskBand.first + 1;
  NS_ASSERT(numSubBands && numBands && numMaskBands);
  NS_LOG_LOGIC("Power per band " << txPowerPerBandW << "W");

  // Different power levels
  double txPowerRefDbm = (10.0 * std::log10(txPowerPerBandW * 1000.0));
  double txPowerInnerBandMinDbm = txPowerRefDbm - 17;
  double txPowerMiddleBandMinDbm = txPowerRefDbm - 22;
  double txPowerOuterBandMinDbm =
      txPowerRefDbm - 30; // TODO: also take into account dBm/MHz constraints

  // Different widths (in number of bands)
  WifiSpectrumBandIndices outerBandLeft(maskBand.first,
                                        maskBand.first + outerSlopeWidth);
  WifiSpectrumBandIndices middleBandLeft(
      outerBandLeft.second + 1, outerBandLeft.second + middleSlopeWidth);
  WifiSpectrumBandIndices innerBandLeft(
      middleBandLeft.second + 1, middleBandLeft.second + innerSlopeWidth);
  WifiSpectrumBandIndices flatRegion(
      innerBandLeft.second + 1, innerBandLeft.second + flatRegionWidth * 2);
  WifiSpectrumBandIndices innerBandRight(flatRegion.second + 1,
                                         flatRegion.second + innerSlopeWidth);
  WifiSpectrumBandIndices middleBandRight(
      innerBandRight.second + 1, innerBandRight.second + middleSlopeWidth);
  WifiSpectrumBandIndices outerBandRight(
      middleBandRight.second + 1, middleBandRight.second + outerSlopeWidth);

  NS_LOG_DEBUG("outerBandLeft=["
               << outerBandLeft.first << ";" << outerBandLeft.second << "] "
               << "middleBandLeft=[" << middleBandLeft.first << ";"
               << middleBandLeft.second << "] "
               << "innerBandLeft=[" << innerBandLeft.first << ";"
               << innerBandLeft.second << "] "
               << "flatRegion=[" << flatRegion.first << ";" << flatRegion.second
               << "] "
               << "subBands=[" << allocatedSubBands.front().first << ";"
               << allocatedSubBands.back().second << "] "
               << "innerBandRight=[" << innerBandRight.first << ";"
               << innerBandRight.second << "] "
               << "middleBandRight=[" << middleBandRight.first << ";"
               << middleBandRight.second << "] "
               << "outerBandRight=[" << outerBandRight.first << ";"
               << outerBandRight.second << "] ");

  // Different slopes
  double innerSlope = 17.0 / innerSlopeWidth;  // 0 to 17 dBr
  double middleSlope = 5.0 / middleSlopeWidth; // 17 to 22 dBr
  double outerSlope = 8.0 / outerSlopeWidth;   // 22 to 30 dBr

  // Build spectrum mask
  Values::iterator vit = c->ValuesBegin();
  Bands::const_iterator bit = c->ConstBandsBegin();
  double txPowerW = 0.0;

  for (size_t i = 0; i < numBands; i++, vit++, bit++) {
    if (i < maskBand.first || i > maskBand.second) // outside the spectrum mask
    {
      txPowerW = 0.0;
    } else if (i <= outerBandLeft.second &&
               i >=
                   outerBandLeft
                       .first) // better to put greater first (less computation)
    {
      txPowerW = DbmToW(txPowerOuterBandMinDbm +
                        ((i - outerBandLeft.first) * outerSlope));
    } else if (i <= middleBandLeft.second && i >= middleBandLeft.first) {
      txPowerW = DbmToW(txPowerMiddleBandMinDbm +
                        ((i - middleBandLeft.first) * middleSlope));
    } else if (i <= innerBandLeft.second && i >= innerBandLeft.first) {
      txPowerW = DbmToW(txPowerInnerBandMinDbm +
                        ((i - innerBandLeft.first) * innerSlope));
    } else if (i <= flatRegion.second && i >= flatRegion.first) {
      txPowerW = txPowerPerBandW;
    } else if (i <= innerBandRight.second && i >= innerBandRight.first) {
      txPowerW =
          DbmToW(txPowerRefDbm -
                 ((i - innerBandRight.first + 1) *
                  innerSlope)); // +1 so as to be symmetric with left slope
    } else if (i <= middleBandRight.second && i >= middleBandRight.first) {
      txPowerW =
          DbmToW(txPowerInnerBandMinDbm -
                 ((i - middleBandRight.first + 1) *
                  middleSlope)); // +1 so as to be symmetric with left slope
    } else if (i <= outerBandRight.second && i >= outerBandRight.first) {
      txPowerW =
          DbmToW(txPowerMiddleBandMinDbm -
                 ((i - outerBandRight.first + 1) *
                  outerSlope)); // +1 so as to be symmetric with left slope
    } else {
      NS_FATAL_ERROR("Should have handled all cases, " << i);
    }
    double txPowerDbr = 10 * std::log10(txPowerW / txPowerPerBandW);
    NS_LOG_LOGIC(uint32_t(i) << " -> " << txPowerDbr);
    *vit = txPowerW / (bit->fh - bit->fl);
  }
  NS_LOG_INFO("Added signal power to subbands "
              << allocatedSubBands.front().first << "-"
              << allocatedSubBands.back().second);
}

WigigSpectrumValueHelper::~WigigSpectrumValueHelper() {}

} // namespace ns3
