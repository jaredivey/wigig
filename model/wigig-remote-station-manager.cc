/*
 * Copyright (c) 2005,2006,2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "wigig-remote-station-manager.h"

#include "wigig-mac-header.h"
#include "wigig-mac.h"
#include "wigig-net-device.h"
#include "wigig-phy-common.h"
#include "wigig-phy.h"

#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/uinteger.h"
#include "ns3/wifi-mac-trailer.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigRemoteStationManager");

NS_OBJECT_ENSURE_REGISTERED(WigigRemoteStationManager);

TypeId WigigRemoteStationManager::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigRemoteStationManager")
          .SetParent<Object>()
          .SetGroupName("Wigig")
          .AddAttribute(
              "MaxSsrc",
              "The maximum number of retransmission attempts for any packet "
              "with size "
              "<= RtsCtsThreshold. "
              "This value will not have any effect on some rate control "
              "algorithms.",
              UintegerValue(7),
              MakeUintegerAccessor(&WigigRemoteStationManager::SetMaxSsrc),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "MaxSlrc",
              "The maximum number of retransmission attempts for any packet "
              "with size "
              "> RtsCtsThreshold. "
              "This value will not have any effect on some rate control "
              "algorithms.",
              UintegerValue(4),
              MakeUintegerAccessor(&WigigRemoteStationManager::SetMaxSlrc),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute("RtsCtsThreshold",
                        "If the size of the PSDU is bigger than this value, we "
                        "use an RTS/CTS "
                        "handshake before sending the data frame."
                        "This value will not have any effect on some rate "
                        "control algorithms.",
                        UintegerValue(65535),
                        MakeUintegerAccessor(
                            &WigigRemoteStationManager::SetRtsCtsThreshold),
                        MakeUintegerChecker<uint32_t>())
          .AddAttribute(
              "FragmentationThreshold",
              "If the size of the PSDU is bigger than this value, we fragment "
              "it such that the "
              "size of the fragments are equal or smaller. "
              "This value does not apply when it is carried in an A-MPDU. "
              "This value will not have any effect on some rate control "
              "algorithms.",
              UintegerValue(65535),
              MakeUintegerAccessor(
                  &WigigRemoteStationManager::DoSetFragmentationThreshold,
                  &WigigRemoteStationManager::DoGetFragmentationThreshold),
              MakeUintegerChecker<uint32_t>())
          .AddAttribute("NonUnicastMode",
                        "Wifi mode used for non-unicast transmissions.",
                        WifiModeValue(),
                        MakeWifiModeAccessor(
                            &WigigRemoteStationManager::m_nonUnicastMode),
                        MakeWifiModeChecker())
          .AddAttribute("DefaultTxPowerLevel",
                        "Default power level to be used for transmissions. "
                        "This is the power level that is used by all those "
                        "WifiManagers that do "
                        "not implement TX power control.",
                        UintegerValue(0),
                        MakeUintegerAccessor(
                            &WigigRemoteStationManager::m_defaultTxPowerLevel),
                        MakeUintegerChecker<uint8_t>())
          .AddTraceSource(
              "MacTxRtsFailed",
              "The transmission of a RTS by the MAC layer has failed",
              MakeTraceSourceAccessor(
                  &WigigRemoteStationManager::m_macTxRtsFailed),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "MacTxDataFailed",
              "The transmission of a data packet by the MAC layer has failed",
              MakeTraceSourceAccessor(
                  &WigigRemoteStationManager::m_macTxDataFailed),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "MacTxFinalRtsFailed",
              "The transmission of a RTS has exceeded the maximum number of "
              "attempts",
              MakeTraceSourceAccessor(
                  &WigigRemoteStationManager::m_macTxFinalRtsFailed),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "MacTxFinalDataFailed",
              "The transmission of a data packet has exceeded the maximum "
              "number of attempts",
              MakeTraceSourceAccessor(
                  &WigigRemoteStationManager::m_macTxFinalDataFailed),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "MacTxOK",
              "The transmission of an MPDU packet by the MAC layer has "
              "succeeded",
              MakeTraceSourceAccessor(&WigigRemoteStationManager::m_macTxOk),
              "ns3::Mac48Address::TracedCallback")
          .AddTraceSource(
              "MacRxOK",
              "The reception of an MPDU packet by the MAC layer has succeeded",
              MakeTraceSourceAccessor(&WigigRemoteStationManager::m_macRxOk),
              "ns3::WigigRemoteStationManager::MacRxOkTracedCallback");
  return tid;
}

WigigRemoteStationManager::WigigRemoteStationManager()
    : m_dmgSupported(false), m_rxSnr(0) {
  NS_LOG_FUNCTION(this);
}

WigigRemoteStationManager::~WigigRemoteStationManager() {
  NS_LOG_FUNCTION(this);
}

void WigigRemoteStationManager::DoDispose() {
  NS_LOG_FUNCTION(this);
  Reset();
}

void WigigRemoteStationManager::SetupPhy(const Ptr<WigigPhy> phy) {
  NS_LOG_FUNCTION(this << phy);
  // We need to track our PHY because it is the object that knows the
  // full set of transmit rates that are supported. We need to know
  // this in order to find the relevant mandatory rates when choosing a
  // transmit rate for automatic control responses like
  // acknowledgments.
  m_wifiPhy = phy;
  if (HasDmgSupported()) {
    /* For data transmission DMG MCS-1 is selected */
    m_defaultTxMode = phy->GetMode(1);
  } else {
    m_defaultTxMode = phy->GetMode(0);
  }
  NS_ASSERT(m_defaultTxMode.IsMandatory());
  Reset();
}

void WigigRemoteStationManager::SetupMac(const Ptr<WigigMac> mac) {
  NS_LOG_FUNCTION(this << mac);
  // We need to track our MAC because it is the object that knows the
  // full set of interframe spaces.
  m_wifiMac = mac;
  Reset();
}

void WigigRemoteStationManager::SetMaxSsrc(uint32_t maxSsrc) {
  NS_LOG_FUNCTION(this << maxSsrc);
  m_maxSsrc = maxSsrc;
}

void WigigRemoteStationManager::SetMaxSlrc(uint32_t maxSlrc) {
  NS_LOG_FUNCTION(this << maxSlrc);
  m_maxSlrc = maxSlrc;
}

void WigigRemoteStationManager::SetRtsCtsThreshold(uint32_t threshold) {
  NS_LOG_FUNCTION(this << threshold);
  m_rtsCtsThreshold = threshold;
}

void WigigRemoteStationManager::SetFragmentationThreshold(uint32_t threshold) {
  NS_LOG_FUNCTION(this << threshold);
  DoSetFragmentationThreshold(threshold);
}

void WigigRemoteStationManager::SetDmgSupported(bool enable) {
  m_dmgSupported = enable;
}

bool WigigRemoteStationManager::HasDmgSupported() const {
  return m_dmgSupported;
}

bool WigigRemoteStationManager::IsWiGigSupported() const {
  return (m_dmgSupported);
}

uint32_t WigigRemoteStationManager::GetFragmentationThreshold() const {
  return DoGetFragmentationThreshold();
}

void WigigRemoteStationManager::AddSupportedMode(Mac48Address address,
                                                 WifiMode mode) {
  NS_LOG_FUNCTION(this << address << mode);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStationState *state = LookupState(address);
  for (WifiModeListIterator i = state->m_operationalRateSet.begin();
       i != state->m_operationalRateSet.end(); i++) {
    if ((*i) == mode) {
      // already in.
      return;
    }
  }
  state->m_operationalRateSet.push_back(mode);
}

void WigigRemoteStationManager::AddAllSupportedModes(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStationState *state = LookupState(address);
  state->m_operationalRateSet.clear();
  for (uint8_t i = 0; i < m_wifiPhy->GetNModes(); i++) {
    state->m_operationalRateSet.push_back(m_wifiPhy->GetMode(i));
    if (m_wifiPhy->GetMode(i).IsMandatory()) {
      AddBasicMode(m_wifiPhy->GetMode(i));
    }
  }
}

bool WigigRemoteStationManager::IsBrandNew(Mac48Address address) const {
  if (address.IsGroup()) {
    return false;
  }
  return LookupState(address)->m_state == WigigRemoteStationState::BRAND_NEW;
}

bool WigigRemoteStationManager::IsAssociated(Mac48Address address) const {
  if (address.IsGroup()) {
    return true;
  }
  return LookupState(address)->m_state ==
         WigigRemoteStationState::GOT_ASSOC_TX_OK;
}

bool WigigRemoteStationManager::IsWaitAssocTxOk(Mac48Address address) const {
  if (address.IsGroup()) {
    return false;
  }
  return LookupState(address)->m_state ==
         WigigRemoteStationState::WAIT_ASSOC_TX_OK;
}

void WigigRemoteStationManager::RecordWaitAssocTxOk(Mac48Address address,
                                                    uint8_t associationID) {
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStationState *state = LookupState(address);
  state->m_state = WigigRemoteStationState::WAIT_ASSOC_TX_OK;
  state->m_aid = associationID;
}

uint8_t WigigRemoteStationManager::RecordGotAssocTxOk(Mac48Address address) {
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStationState *state = LookupState(address);
  state->m_state = WigigRemoteStationState::GOT_ASSOC_TX_OK;
  return state->m_aid;
}

void WigigRemoteStationManager::RecordGotAssocTxFailed(Mac48Address address) {
  NS_ASSERT(!address.IsGroup());
  LookupState(address)->m_state = WigigRemoteStationState::DISASSOC;
}

void WigigRemoteStationManager::RecordDisassociated(Mac48Address address) {
  NS_ASSERT(!address.IsGroup());
  LookupState(address)->m_state = WigigRemoteStationState::DISASSOC;
}

WigigTxVector
WigigRemoteStationManager::GetDataTxVector(const WigigMacHeader &header) {
  NS_LOG_FUNCTION(this << header);
  Mac48Address address = header.GetAddr1();
  /* Beam Tracking is Requested */
  if (header.IsBeamTrackingRequested()) {
    WigigTxVector v = DoGetDataTxVector(Lookup(address));
    v.RequestBeamTracking();
    v.SetPacketType(header.GetPacketType());
    v.SetTrainingFieldLength(header.GetTrainingFieldLength());
    return v;
  }
  if (!header.IsMgt() && address.IsGroup()) {
    WifiMode mode = GetNonUnicastMode();
    WigigTxVector v;
    v.SetMode(mode);
    v.SetPreambleType(
        GetPreambleForTransmission(mode.GetModulationClass(), false));
    v.SetTxPowerLevel(m_defaultTxPowerLevel);
    v.SetChannelWidth(
        GetChannelWidthForTransmission(mode, m_wifiPhy->GetChannelWidth()));
    v.SetNTx(1);
    v.SetNss(1);
    v.SetNess(0);
    v.SetStbc(0);
    return v;
  }
  if (header.IsMgt() && m_dmgSupported) {
    WigigTxVector v;
    v.SetMode(WifiMode("DmgMcs0"));
    v.SetPreambleType(WIFI_PREAMBLE_DMG_CTRL);
    v.SetTxPowerLevel(m_defaultTxPowerLevel);
    v.SetChannelWidth(m_wifiPhy->GetChannelWidth());
    return v;
  }
  WigigTxVector txVector;
  if (header.IsMgt() && !m_dmgSupported) {
    // Use the lowest basic rate for management frames
    WifiMode mgtMode;
    if (GetNBasicModes() > 0) {
      mgtMode = GetBasicMode(0);
    } else {
      mgtMode = GetDefaultMode();
    }
    txVector.SetMode(mgtMode);
    txVector.SetPreambleType(
        GetPreambleForTransmission(mgtMode.GetModulationClass(), false));
    txVector.SetTxPowerLevel(m_defaultTxPowerLevel);
    txVector.SetChannelWidth(
        GetChannelWidthForTransmission(mgtMode, m_wifiPhy->GetChannelWidth()));
  } else {
    txVector = DoGetDataTxVector(Lookup(address));
  }
  Ptr<WigigNetDevice> device =
      DynamicCast<WigigNetDevice>(m_wifiPhy->GetDevice());
  return txVector;
}

WigigTxVector
WigigRemoteStationManager::GetDmgTxVector(Mac48Address address,
                                          const WigigMacHeader *header,
                                          Ptr<const Packet> packet) {
  NS_LOG_FUNCTION(this << address << *header << packet);
  WigigTxVector v;

  if (header->IsActionNoAck()) /* BRP Frame */
  {
    if (m_wifiPhy->GetStandard() == WIFI_STANDARD_80211ad) {
      v.SetMode(WifiMode(
          "DmgMcs0")); /* The BRP Frame shall be transmitted at DMG MCS0 */
    }
    v.SetPacketType(header->GetPacketType());
    v.SetTrainingFieldLength(header->GetTrainingFieldLength());
    v.SetSender(m_wifiMac->GetAddress());
  } else if (header->IsDmgBeacon() || header->IsSsw() || header->IsSswFbck() ||
             header->IsSswAck()) /* Beamforming Training (SLS) */
  {
    if (m_wifiPhy->GetStandard() == WIFI_STANDARD_80211ad) {
      v.SetMode(WifiMode("DmgMcs0"));
    }
    v.SetTrainingFieldLength(0);
  } else if (header->IsPollFrame() || header->IsGrantFrame() ||
             header->IsSprFrame()) /* Dynamic Polling */
  {
    if (m_wifiPhy->GetStandard() == WIFI_STANDARD_80211ad) {
      v.SetMode(WifiMode("DmgMcs1"));
    }
    v.SetTrainingFieldLength(0);
  }

  v.SetPreambleType(
      GetPreambleForTransmission(v.GetMode().GetModulationClass(), false));
  v.SetTxPowerLevel(m_defaultTxPowerLevel);
  v.SetChannelWidth(m_wifiPhy->GetChannelWidth());

  return v;
}

WigigTxVector WigigRemoteStationManager::GetRtsTxVector(Mac48Address address) {
  NS_LOG_FUNCTION(this << address);
  if (address.IsGroup()) {
    WifiMode mode = GetNonUnicastMode();
    WigigTxVector v;
    v.SetMode(mode);
    v.SetPreambleType(
        GetPreambleForTransmission(mode.GetModulationClass(), false));
    v.SetTxPowerLevel(m_defaultTxPowerLevel);
    v.SetChannelWidth(
        GetChannelWidthForTransmission(mode, m_wifiPhy->GetChannelWidth()));
    v.SetNTx(1);
    v.SetNss(1);
    v.SetNess(0);
    v.SetStbc(0);
    return v;
  }
  if (HasDmgSupported()) {
    return GetDmgControlTxVector();
  }
  return DoGetRtsTxVector(Lookup(address));
}

void WigigRemoteStationManager::ReportRtsFailed(Mac48Address address,
                                                const WigigMacHeader *header) {
  NS_LOG_FUNCTION(this << address << *header);
  NS_ASSERT(!address.IsGroup());
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  m_ssrc[ac]++;
  m_macTxRtsFailed(address);
  DoReportRtsFailed(Lookup(address));
}

void WigigRemoteStationManager::ReportDataFailed(Mac48Address address,
                                                 const WigigMacHeader *header,
                                                 uint32_t packetSize) {
  NS_LOG_FUNCTION(this << address << *header);
  NS_ASSERT(!address.IsGroup());
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  bool longMpdu = (packetSize + header->GetSize() + WIFI_MAC_FCS_LENGTH) >
                  m_rtsCtsThreshold;
  if (longMpdu) {
    m_slrc[ac]++;
  } else {
    m_ssrc[ac]++;
  }
  m_macTxDataFailed(address);
  DoReportDataFailed(Lookup(address));
}

void WigigRemoteStationManager::ReportRtsOk(Mac48Address address,
                                            const WigigMacHeader *header,
                                            double ctsSnr, WifiMode ctsMode,
                                            double rtsSnr) {
  NS_LOG_FUNCTION(this << address << *header << ctsSnr << ctsMode << rtsSnr);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStation *station = Lookup(address);
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  station->m_state->m_info.NotifyTxSuccess(m_ssrc[ac]);
  m_ssrc[ac] = 0;
  m_macTxOk(address);
  DoReportRtsOk(station, ctsSnr, ctsMode, rtsSnr);
}

void WigigRemoteStationManager::ReportDataOk(Mac48Address address,
                                             const WigigMacHeader *header,
                                             double ackSnr, WifiMode ackMode,
                                             double dataSnr,
                                             const WigigTxVector &dataTxVector,
                                             uint32_t packetSize) {
  NS_LOG_FUNCTION(this << address << *header << ackSnr << ackMode << dataSnr
                       << dataTxVector << packetSize);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStation *station = Lookup(address);
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  bool longMpdu = (packetSize + header->GetSize() + WIFI_MAC_FCS_LENGTH) >
                  m_rtsCtsThreshold;
  if (longMpdu) {
    station->m_state->m_info.NotifyTxSuccess(m_slrc[ac]);
    m_slrc[ac] = 0;
  } else {
    station->m_state->m_info.NotifyTxSuccess(m_ssrc[ac]);
    m_ssrc[ac] = 0;
  }
  m_macTxOk(address);
  DoReportDataOk(station, ackSnr, ackMode, dataSnr,
                 dataTxVector.GetChannelWidth(), dataTxVector.GetNss());
}

void WigigRemoteStationManager::ReportFinalRtsFailed(
    Mac48Address address, const WigigMacHeader *header) {
  NS_LOG_FUNCTION(this << address << *header);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStation *station = Lookup(address);
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  station->m_state->m_info.NotifyTxFailed();
  m_ssrc[ac] = 0;
  m_macTxFinalRtsFailed(address);
  DoReportFinalRtsFailed(station);
}

void WigigRemoteStationManager::ReportFinalDataFailed(
    Mac48Address address, const WigigMacHeader *header, uint32_t packetSize) {
  NS_LOG_FUNCTION(this << address << *header);
  NS_ASSERT(!address.IsGroup());
  WigigRemoteStation *station = Lookup(address);
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  station->m_state->m_info.NotifyTxFailed();
  bool longMpdu = (packetSize + header->GetSize() + WIFI_MAC_FCS_LENGTH) >
                  m_rtsCtsThreshold;
  if (longMpdu) {
    m_slrc[ac] = 0;
  } else {
    m_ssrc[ac] = 0;
  }
  m_macTxFinalDataFailed(address);
  DoReportFinalDataFailed(station);
}

void WigigRemoteStationManager::ReportRxOk(Mac48Address address,
                                           const WigigMacHeader *header,
                                           double rxSnr, WifiMode txMode) {
  NS_LOG_FUNCTION(this << address << rxSnr << txMode);
  if (address.IsGroup()) {
    return;
  }
  m_rxSnr = rxSnr;
  m_macRxOk(header->GetType(), address, rxSnr);
  DoReportRxOk(Lookup(address), rxSnr, txMode);
}

void WigigRemoteStationManager::ReportAmpduTxStatus(
    Mac48Address address, uint8_t nSuccessfulMpdus, uint8_t nFailedMpdus,
    double rxSnr, double dataSnr, const WigigTxVector &dataTxVector) {
  NS_LOG_FUNCTION(this << address << +nSuccessfulMpdus << +nFailedMpdus << rxSnr
                       << dataSnr << dataTxVector);
  NS_ASSERT(!address.IsGroup());
  for (uint8_t i = 0; i < nFailedMpdus; i++) {
    m_macTxDataFailed(address);
  }
  DoReportAmpduTxStatus(Lookup(address), nSuccessfulMpdus, nFailedMpdus, rxSnr,
                        dataSnr, dataTxVector.GetChannelWidth(),
                        dataTxVector.GetNss());
}

bool WigigRemoteStationManager::NeedRts(const WigigMacHeader &header,
                                        uint32_t size) {
  NS_LOG_FUNCTION(this << header << size);
  Mac48Address address = header.GetAddr1();
  if (address.IsGroup()) {
    return false;
  }
  bool normally = (size > m_rtsCtsThreshold);
  return DoNeedRts(Lookup(address), size, normally);
}

bool WigigRemoteStationManager::NeedCtsToSelf(const WigigTxVector &txVector) {
  return false;
}

bool WigigRemoteStationManager::NeedRetransmission(Mac48Address address,
                                                   const WigigMacHeader *header,
                                                   Ptr<const Packet> packet) {
  NS_LOG_FUNCTION(this << address << packet << *header);
  NS_ASSERT(!address.IsGroup());
  AcIndex ac =
      QosUtilsMapTidToAc((header->IsQosData()) ? header->GetQosTid() : 0);
  bool longMpdu = (packet->GetSize() + header->GetSize() +
                   WIFI_MAC_FCS_LENGTH) > m_rtsCtsThreshold;
  uint32_t retryCount;
  uint32_t maxRetryCount;
  if (longMpdu) {
    retryCount = m_slrc[ac];
    maxRetryCount = m_maxSlrc;
  } else {
    retryCount = m_ssrc[ac];
    maxRetryCount = m_maxSsrc;
  }
  bool normally = retryCount < maxRetryCount;
  NS_LOG_DEBUG("WigigRemoteStationManager::NeedRetransmission count: "
               << retryCount << " result: " << std::boolalpha << normally);
  return DoNeedRetransmission(Lookup(address), packet, normally);
}

bool WigigRemoteStationManager::NeedFragmentation(Mac48Address address,
                                                  const WigigMacHeader *header,
                                                  Ptr<const Packet> packet) {
  NS_LOG_FUNCTION(this << address << packet << *header);
  if (address.IsGroup()) {
    return false;
  }
  bool normally = (packet->GetSize() + header->GetSize() +
                   WIFI_MAC_FCS_LENGTH) > GetFragmentationThreshold();
  NS_LOG_DEBUG("WigigRemoteStationManager::NeedFragmentation result: "
               << std::boolalpha << normally);
  return DoNeedFragmentation(Lookup(address), packet, normally);
}

void WigigRemoteStationManager::DoSetFragmentationThreshold(
    uint32_t threshold) {
  NS_LOG_FUNCTION(this << threshold);
  if (threshold < 256) {
    /*
     * ASN.1 encoding of the MAC and PHY MIB (256 ... 8000)
     */
    NS_LOG_WARN(
        "Fragmentation threshold should be larger than 256. Setting to 256.");
    m_nextFragmentationThreshold = 256;
  } else {
    /*
     * The length of each fragment shall be an even number of octets, except for
     * the last fragment if an MSDU or MMPDU, which may be either an even or an
     * odd number of octets.
     */
    if (threshold % 2 != 0) {
      NS_LOG_WARN(
          "Fragmentation threshold should be an even number. Setting to "
          << threshold - 1);
      m_nextFragmentationThreshold = threshold - 1;
    } else {
      m_nextFragmentationThreshold = threshold;
    }
  }
}

void WigigRemoteStationManager::UpdateFragmentationThreshold() {
  m_fragmentationThreshold = m_nextFragmentationThreshold;
}

uint32_t WigigRemoteStationManager::DoGetFragmentationThreshold() const {
  return m_fragmentationThreshold;
}

uint32_t WigigRemoteStationManager::GetNFragments(const WigigMacHeader *header,
                                                  Ptr<const Packet> packet) {
  NS_LOG_FUNCTION(this << *header << packet);
  // The number of bytes a fragment can support is (Threshold - WIFI_HEADER_SIZE
  // - WIFI_FCS).
  uint32_t nFragments =
      (packet->GetSize() /
       (GetFragmentationThreshold() - header->GetSize() - WIFI_MAC_FCS_LENGTH));

  // If the size of the last fragment is not 0.
  if ((packet->GetSize() % (GetFragmentationThreshold() - header->GetSize() -
                            WIFI_MAC_FCS_LENGTH)) > 0) {
    nFragments++;
  }
  NS_LOG_DEBUG("WigigRemoteStationManager::GetNFragments returning "
               << nFragments);
  return nFragments;
}

uint32_t WigigRemoteStationManager::GetFragmentSize(
    Mac48Address address, const WigigMacHeader *header,
    Ptr<const Packet> packet, uint32_t fragmentNumber) {
  NS_LOG_FUNCTION(this << address << *header << packet << fragmentNumber);
  NS_ASSERT(!address.IsGroup());
  uint32_t nFragment = GetNFragments(header, packet);
  if (fragmentNumber >= nFragment) {
    NS_LOG_DEBUG("WigigRemoteStationManager::GetFragmentSize returning 0");
    return 0;
  }
  // Last fragment
  if (fragmentNumber == nFragment - 1) {
    uint32_t lastFragmentSize =
        packet->GetSize() -
        (fragmentNumber * (GetFragmentationThreshold() - header->GetSize() -
                           WIFI_MAC_FCS_LENGTH));
    NS_LOG_DEBUG("WigigRemoteStationManager::GetFragmentSize returning "
                 << lastFragmentSize);
    return lastFragmentSize;
  }
  // All fragments but the last, the number of bytes is (Threshold -
  // WIFI_HEADER_SIZE - WIFI_FCS).
  else {
    uint32_t fragmentSize =
        GetFragmentationThreshold() - header->GetSize() - WIFI_MAC_FCS_LENGTH;
    NS_LOG_DEBUG("WigigRemoteStationManager::GetFragmentSize returning "
                 << fragmentSize);
    return fragmentSize;
  }
}

uint32_t WigigRemoteStationManager::GetFragmentOffset(
    Mac48Address address, const WigigMacHeader *header,
    Ptr<const Packet> packet, uint32_t fragmentNumber) {
  NS_LOG_FUNCTION(this << address << *header << packet << fragmentNumber);
  NS_ASSERT(!address.IsGroup());
  NS_ASSERT(fragmentNumber < GetNFragments(header, packet));
  uint32_t fragmentOffset =
      fragmentNumber *
      (GetFragmentationThreshold() - header->GetSize() - WIFI_MAC_FCS_LENGTH);
  NS_LOG_DEBUG("WigigRemoteStationManager::GetFragmentOffset returning "
               << fragmentOffset);
  return fragmentOffset;
}

bool WigigRemoteStationManager::IsLastFragment(Mac48Address address,
                                               const WigigMacHeader *header,
                                               Ptr<const Packet> packet,
                                               uint32_t fragmentNumber) {
  NS_LOG_FUNCTION(this << address << *header << packet << fragmentNumber);
  NS_ASSERT(!address.IsGroup());
  bool isLast = fragmentNumber == (GetNFragments(header, packet) - 1);
  NS_LOG_DEBUG("WigigRemoteStationManager::IsLastFragment returning "
               << std::boolalpha << isLast);
  return isLast;
}

WigigTxVector WigigRemoteStationManager::GetDmgControlTxVector() {
  WifiMode mode = m_wifiPhy->GetMode(0);
  WigigTxVector v;
  v.SetMode(mode);
  v.SetTxPowerLevel(m_defaultTxPowerLevel);
  v.SetPreambleType(
      GetPreambleForTransmission(mode.GetModulationClass(), false));
  v.SetChannelWidth(m_wifiPhy->GetChannelWidth());
  return v;
}

uint8_t WigigRemoteStationManager::GetDefaultTxPowerLevel() const {
  return m_defaultTxPowerLevel;
}

WigigRemoteStationState *
WigigRemoteStationManager::LookupState(Mac48Address address) const {
  NS_LOG_FUNCTION(this << address);
  for (StationStates::const_iterator i = m_states.begin(); i != m_states.end();
       i++) {
    if ((*i)->m_address == address) {
      NS_LOG_DEBUG(
          "WigigRemoteStationManager::LookupState returning existing state");
      return (*i);
    }
  }
  WigigRemoteStationState *state = new WigigRemoteStationState();
  state->m_state = WigigRemoteStationState::BRAND_NEW;
  state->m_address = address;
  state->m_operationalRateSet.push_back(GetDefaultMode());
  state->m_operationalMcsSet.push_back(GetDefaultMcs());
  state->m_channelWidth = m_wifiPhy->GetChannelWidth();
  state->m_ness = 0;
  state->m_aggregation = false;
  state->m_dmgSupported = false;
  const_cast<WigigRemoteStationManager *>(this)->m_states.push_back(state);
  NS_LOG_DEBUG("WigigRemoteStationManager::LookupState returning new state");
  return state;
}

WigigRemoteStation *
WigigRemoteStationManager::Lookup(Mac48Address address) const {
  NS_LOG_FUNCTION(this << address);
  for (Stations::const_iterator i = m_stations.begin(); i != m_stations.end();
       i++) {
    if ((*i)->m_state->m_address == address) {
      return (*i);
    }
  }
  WigigRemoteStationState *state = LookupState(address);

  WigigRemoteStation *station = DoCreateStation();
  station->m_state = state;
  const_cast<WigigRemoteStationManager *>(this)->m_stations.push_back(station);
  return station;
}

void WigigRemoteStationManager::AddStationDmgCapabilities(
    Mac48Address from, DmgCapabilities dmgCapabilities) {
  // Used by all stations to record DMG capabilities of remote stations
  NS_LOG_FUNCTION(this << from << dmgCapabilities);
  WigigRemoteStationState *state;
  state = LookupState(from);
  state->m_dmgSupported = true;
  state->m_dmgCapabilities = dmgCapabilities;
}

std::optional<std::reference_wrapper<DmgCapabilities>>
WigigRemoteStationManager::GetStationDmgCapabilities(Mac48Address from) {
  WigigRemoteStationState *state = LookupState(from);
  if (state->m_dmgSupported) {
    return state->m_dmgCapabilities;
  }
  return std::nullopt;
}

WifiMode WigigRemoteStationManager::GetDefaultMode() const {
  return m_defaultTxMode;
}

WifiMode WigigRemoteStationManager::GetDefaultMcs() const {
  return m_defaultTxMcs;
}

void WigigRemoteStationManager::Reset() {
  NS_LOG_FUNCTION(this);
  for (StationStates::const_iterator i = m_states.begin(); i != m_states.end();
       i++) {
    delete (*i);
  }
  m_states.clear();
  for (Stations::const_iterator i = m_stations.begin(); i != m_stations.end();
       i++) {
    delete (*i);
  }
  m_stations.clear();
  m_bssBasicRateSet.clear();
  m_bssBasicMcsSet.clear();
  m_ssrc.fill(0);
  m_slrc.fill(0);
}

void WigigRemoteStationManager::AddBasicMode(WifiMode mode) {
  NS_LOG_FUNCTION(this << mode);
  for (uint8_t i = 0; i < GetNBasicModes(); i++) {
    if (GetBasicMode(i) == mode) {
      return;
    }
  }
  m_bssBasicRateSet.push_back(mode);
}

uint8_t WigigRemoteStationManager::GetNBasicModes() const {
  return static_cast<uint8_t>(m_bssBasicRateSet.size());
}

WifiMode WigigRemoteStationManager::GetBasicMode(uint8_t i) const {
  NS_ASSERT(i < GetNBasicModes());
  return m_bssBasicRateSet[i];
}

WifiMode WigigRemoteStationManager::GetNonUnicastMode() const {
  if (m_nonUnicastMode == WifiMode()) {
    if (GetNBasicModes() > 0) {
      return GetBasicMode(0);
    } else {
      return GetDefaultMode();
    }
  } else {
    return m_nonUnicastMode;
  }
}

bool WigigRemoteStationManager::DoNeedRts(WigigRemoteStation *station,
                                          uint32_t size, bool normally) {
  return normally;
}

bool WigigRemoteStationManager::DoNeedRetransmission(
    WigigRemoteStation *station, Ptr<const Packet> packet, bool normally) {
  return normally;
}

bool WigigRemoteStationManager::DoNeedFragmentation(WigigRemoteStation *station,
                                                    Ptr<const Packet> packet,
                                                    bool normally) {
  return normally;
}

void WigigRemoteStationManager::DoReportAmpduTxStatus(
    WigigRemoteStation *station, uint8_t nSuccessfulMpdus, uint8_t nFailedMpdus,
    double rxSnr, double dataSnr, uint16_t dataChannelWidth, uint8_t dataNss) {
  NS_LOG_DEBUG("DoReportAmpduTxStatus received but the manager does not handle "
               "A-MPDUs!");
}

WifiMode
WigigRemoteStationManager::GetSupported(const WigigRemoteStation *station,
                                        uint8_t i) const {
  NS_ASSERT(i < GetNSupported(station));
  return station->m_state->m_operationalRateSet[i];
}

Mac48Address
WigigRemoteStationManager::GetAddress(const WigigRemoteStation *station) const {
  return station->m_state->m_address;
}

uint16_t WigigRemoteStationManager::GetChannelWidth(
    const WigigRemoteStation *station) const {
  return station->m_state->m_channelWidth;
}

bool WigigRemoteStationManager::GetAggregation(
    const WigigRemoteStation *station) const {
  return station->m_state->m_aggregation;
}

uint8_t
WigigRemoteStationManager::GetNess(const WigigRemoteStation *station) const {
  return station->m_state->m_ness;
}

Ptr<WigigPhy> WigigRemoteStationManager::GetPhy() const { return m_wifiPhy; }

Ptr<WigigMac> WigigRemoteStationManager::GetMac() const { return m_wifiMac; }

double WigigRemoteStationManager::GetRxSnr() const { return m_rxSnr; }

uint8_t WigigRemoteStationManager::GetNSupported(
    const WigigRemoteStation *station) const {
  return static_cast<uint8_t>(station->m_state->m_operationalRateSet.size());
}

} // namespace ns3
