/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef WIGIG_PHY_COMMON_H
#define WIGIG_PHY_COMMON_H

#include "ns3/wifi-mode.h"
#include "ns3/wifi-standards.h"

namespace ns3 {

/**
 * Return the channel width that is allowed based on the selected mode and the
 * given maximum channel width. This is especially useful when using non-HT
 * modes with HT/VHT/HE capable stations (with default width above 20 MHz).
 *
 * \param mode selected WifiMode
 * \param maxAllowedChannelWidth maximum channel width allowed for the
 * transmission \return channel width adapted to the selected mode
 */
uint16_t GetChannelWidthForTransmission(WifiMode mode,
                                        uint16_t maxAllowedChannelWidth);
/**
 * Return the channel width that is allowed based on the selected mode, the
 * current width of the operating channel and the maximum channel width
 * supported by the receiver. This is especially useful when using non-HT modes
 * with HT/VHT/HE capable stations (with default width above 20 MHz).
 *
 * \param mode selected WifiMode
 * \param operatingChannelWidth operating channel width
 * \param maxSupportedChannelWidth maximum channel width supported by the
 * receiver \return channel width adapted to the selected mode
 */
uint16_t GetChannelWidthForTransmission(WifiMode mode,
                                        uint16_t operatingChannelWidth,
                                        uint16_t maxSupportedChannelWidth);

} // namespace ns3

#endif
