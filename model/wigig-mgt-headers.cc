/*
 * Copyright (c) 2006 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-mgt-headers.h"

#include "ext-headers.h"

#include "ns3/address-utils.h"
#include "ns3/assert.h"
#include "ns3/simulator.h"

namespace ns3 {

/***************************************************
 *               Add TS Request Frame
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(DmgAddTsRequestFrame);

DmgAddTsRequestFrame::DmgAddTsRequestFrame() : m_dialogToken(1) {}

TypeId DmgAddTsRequestFrame::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgAddTsRequestFrame")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgAddTsRequestFrame>();
  return tid;
}

TypeId DmgAddTsRequestFrame::GetInstanceTypeId() const { return GetTypeId(); }

void DmgAddTsRequestFrame::PrintImpl(std::ostream &os) const {
  WifiMgtHeader<DmgAddTsRequestFrame, WigigAssocRequestElems>::PrintImpl(os);
}

uint32_t DmgAddTsRequestFrame::GetSerializedSizeImpl() const {
  return 1 + WifiMgtHeader<DmgAddTsRequestFrame,
                           WigigAssocRequestElems>::GetSerializedSizeImpl();
}

void DmgAddTsRequestFrame::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
  WifiMgtHeader<DmgAddTsRequestFrame, WigigAssocRequestElems>::SerializeImpl(i);
}

uint32_t DmgAddTsRequestFrame::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_dialogToken = i.ReadU8();
  auto distance = i.GetDistanceFrom(start);
  return distance + WifiMgtHeader<DmgAddTsRequestFrame,
                                  WigigAssocRequestElems>::DeserializeImpl(i);
}

void DmgAddTsRequestFrame::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

uint8_t DmgAddTsRequestFrame::GetDialogToken() const { return m_dialogToken; }

/***************************************************
 *               Add TS Response Frame
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(DmgAddTsResponseFrame);

DmgAddTsResponseFrame::DmgAddTsResponseFrame() : m_dialogToken(1) {}

TypeId DmgAddTsResponseFrame::GetTypeId() {
  static TypeId tid = TypeId("ns3::DmgAddTsResponseFrame")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DmgAddTsResponseFrame>();
  return tid;
}

TypeId DmgAddTsResponseFrame::GetInstanceTypeId() const { return GetTypeId(); }

void DmgAddTsResponseFrame::Print(std::ostream &os) const {}

uint32_t DmgAddTsResponseFrame::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1;                                     // Dialog token
  size += m_status.GetSerializedSize();          // Status Code
  size += m_tsDelayElement.GetSerializedSize();  // TS Delay
  size += m_dmgTspecElement.GetSerializedSize(); // DMG TSPEC
  return size;
}

void DmgAddTsResponseFrame::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
  i = m_status.Serialize(i);
  m_tsDelayElement.Serialize(i);
  i.Next(m_tsDelayElement.GetSerializedSize());
  m_dmgTspecElement.Serialize(i);
  i.Next(m_dmgTspecElement.GetSerializedSize());
}

uint32_t DmgAddTsResponseFrame::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_dialogToken = i.ReadU8();
  i = m_status.Deserialize(i);
  i = m_tsDelayElement.DeserializeIfPresent(i);
  i = m_dmgTspecElement.DeserializeIfPresent(i);
  return i.GetDistanceFrom(start);
}

void DmgAddTsResponseFrame::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

void DmgAddTsResponseFrame::SetStatusCode(StatusCode status) {
  m_status = status;
}

void DmgAddTsResponseFrame::SetTsDelay(TsDelayElement &element) {
  m_tsDelayElement = element;
}

void DmgAddTsResponseFrame::SetDmgTspecElement(DmgTspecElement &element) {
  m_dmgTspecElement = element;
}

uint8_t DmgAddTsResponseFrame::GetDialogToken() const { return m_dialogToken; }

StatusCode DmgAddTsResponseFrame::GetStatusCode() const { return m_status; }

TsDelayElement DmgAddTsResponseFrame::GetTsDelay() const {
  return m_tsDelayElement;
}

DmgTspecElement DmgAddTsResponseFrame::GetDmgTspec() const {
  return m_dmgTspecElement;
}

/***************************************************
 *               Delete TS Frame (8.5.3.4)
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(DelTsFrame);

DelTsFrame::DelTsFrame() : m_reasonCode(0) {}

TypeId DelTsFrame::GetTypeId() {
  static TypeId tid = TypeId("ns3::DelTsFrame")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<DelTsFrame>();
  return tid;
}

TypeId DelTsFrame::GetInstanceTypeId() const { return GetTypeId(); }

void DelTsFrame::Print(std::ostream &os) const {}

uint32_t DelTsFrame::GetSerializedSize() const {
  uint32_t size = 0;
  size += 3;                                       // TS Info
  size += 2;                                       // Reason Code
  size += m_dmgAllocationInfo.GetSerializedSize(); // DMG Allocation Info
  return size;
}

void DelTsFrame::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.Write(m_tsInfo, 3);
  i.WriteHtolsbU16(m_reasonCode);
  i = m_dmgAllocationInfo.Serialize(i);
}

uint32_t DelTsFrame::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  i.Read(m_tsInfo, 3);
  m_reasonCode = i.ReadLsbtohU16();
  i = m_dmgAllocationInfo.Deserialize(i);
  return i.GetDistanceFrom(start);
}

void DelTsFrame::SetReasonCode(uint16_t reason) { m_reasonCode = reason; }

void DelTsFrame::SetDmgAllocationInfo(DmgAllocationInfo info) {
  m_dmgAllocationInfo = info;
}

uint16_t DelTsFrame::GetReasonCode() const { return m_reasonCode; }

DmgAllocationInfo DelTsFrame::GetDmgAllocationInfo() const {
  return m_dmgAllocationInfo;
}

void ExtInformationRequest::SetSubjectAddress(Mac48Address address) {
  m_subjectAddress = address;
}

Mac48Address ExtInformationResponse::GetSubjectAddress() const {
  return m_subjectAddress;
}

void ExtInformationResponse::SetSubjectAddress(Mac48Address address) {
  m_subjectAddress = address;
}

Mac48Address ExtInformationRequest::GetSubjectAddress() const {
  return m_subjectAddress;
}

/***************************************************
 *         Information Request Frame (8.5.20.4)
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtInformationRequest);

TypeId ExtInformationRequest::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtInformationRequest")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtInformationRequest>();
  return tid;
}

TypeId ExtInformationRequest::GetInstanceTypeId() const { return GetTypeId(); }

void ExtInformationRequest::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  WriteTo(i, m_subjectAddress);
  WifiMgtHeader<ExtInformationRequest, WigigAssocRequestElems>::SerializeImpl(
      i);
}

uint32_t ExtInformationRequest::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  ReadFrom(i, m_subjectAddress);

  auto distance = i.GetDistanceFrom(start);
  return distance + WifiMgtHeader<ExtInformationRequest,
                                  WigigAssocRequestElems>::DeserializeImpl(i);
}

uint32_t ExtInformationRequest::GetSerializedSizeImpl() const {
  return 6 + WifiMgtHeader<ExtInformationRequest,
                           WigigAssocRequestElems>::GetSerializedSizeImpl();
}

/***************************************************
 *         Information Response Frame (8.5.20.5)
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtInformationResponse);

TypeId ExtInformationResponse::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtInformationResponse")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtInformationResponse>();
  return tid;
}

TypeId ExtInformationResponse::GetInstanceTypeId() const { return GetTypeId(); }

uint32_t ExtInformationResponse::GetSerializedSizeImpl() const {
  return 6 + WifiMgtHeader<ExtInformationResponse,
                           WigigAssocResponseElems>::GetSerializedSizeImpl();
}

void ExtInformationResponse::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  WriteTo(i, m_subjectAddress);
  WifiMgtHeader<ExtInformationResponse, WigigAssocResponseElems>::SerializeImpl(
      i);
}

uint32_t ExtInformationResponse::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  ReadFrom(i, m_subjectAddress);
  auto distance = i.GetDistanceFrom(start);
  return distance + WifiMgtHeader<ExtInformationResponse,
                                  WigigAssocResponseElems>::DeserializeImpl(i);
}

/***************************************************
 *               Relay Search Request
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtRelaySearchRequestHeader);

ExtRelaySearchRequestHeader::ExtRelaySearchRequestHeader()
    : m_dialogToken(0), m_aid(0) {}

TypeId ExtRelaySearchRequestHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtRelaySearchRequestHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtRelaySearchRequestHeader>();
  return tid;
}

TypeId ExtRelaySearchRequestHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

void ExtRelaySearchRequestHeader::Print(std::ostream &os) const {
  os << "Dialog Token = " << m_dialogToken
     << ", Destination REDS AID = " << m_aid;
}

uint32_t ExtRelaySearchRequestHeader::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1; // Dialog Token
  size += 2; // Destination REDS AID
  return size;
}

void ExtRelaySearchRequestHeader::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
  i.WriteHtolsbU16(m_aid);
}

uint32_t ExtRelaySearchRequestHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_dialogToken = i.ReadU8();
  m_aid = i.ReadLsbtohU16();
  return i.GetDistanceFrom(start);
}

void ExtRelaySearchRequestHeader::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

void ExtRelaySearchRequestHeader::SetDestinationRedsAid(uint16_t aid) {
  m_aid = aid;
}

uint8_t ExtRelaySearchRequestHeader::GetDialogToken() const {
  return m_dialogToken;
}

uint16_t ExtRelaySearchRequestHeader::GetDestinationRedsAid() const {
  return m_aid;
}

/***************************************************
 *               Relay Search Response
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtRelaySearchResponseHeader);

ExtRelaySearchResponseHeader::ExtRelaySearchResponseHeader()
    : m_dialogToken(0), m_statusCode(0) {}

TypeId ExtRelaySearchResponseHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtRelaySearchResponseHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtRelaySearchResponseHeader>();
  return tid;
}

TypeId ExtRelaySearchResponseHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

void ExtRelaySearchResponseHeader::Print(std::ostream &os) const {
  os << "Dialog Token = " << m_dialogToken
     << ", Status Code = " << m_statusCode;
}

uint32_t ExtRelaySearchResponseHeader::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1; // Dialog Token
  size += 2; // Status Code
  if (m_statusCode == 0) {
    size += m_list.size() * 3; // Relay Capable STA Info
  }
  return size;
}

void ExtRelaySearchResponseHeader::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
  i.WriteHtolsbU16(m_statusCode);
  if (m_statusCode == 0) {
    for (RelayCapableStaList::const_iterator item = m_list.begin();
         item != m_list.end(); item++) {
      i.WriteU8((item->first & 0xFF));
      i = item->second.Serialize(i);
    }
  }
}

uint32_t ExtRelaySearchResponseHeader::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  RelayCapabilitiesInfo info;
  uint16_t aid;
  m_dialogToken = i.ReadU8();
  m_statusCode = i.ReadLsbtohU16();
  if (!i.IsEnd()) {
    do {
      aid = i.ReadU8();
      i = info.Deserialize(i);
      m_list[aid] = info;
    } while (!i.IsEnd());
  }
  return i.GetDistanceFrom(start);
}

void ExtRelaySearchResponseHeader::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

void ExtRelaySearchResponseHeader::SetStatusCode(uint16_t code) {
  m_statusCode = code;
}

void ExtRelaySearchResponseHeader::AddRelayCapableStaInfo(
    uint8_t aid, RelayCapabilitiesInfo &element) {
  m_list[aid] = element;
}

void ExtRelaySearchResponseHeader::SetRelayCapableList(
    RelayCapableStaList &list) {
  m_list = list;
}

uint8_t ExtRelaySearchResponseHeader::GetDialogToken() const {
  return m_dialogToken;
}

uint16_t ExtRelaySearchResponseHeader::GetStatusCode() const {
  return m_statusCode;
}

RelayCapableStaList ExtRelaySearchResponseHeader::GetRelayCapableList() const {
  return m_list;
}

/***************************************************
 *     Multi Relay Channel Measurement Request
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtMultiRelayChannelMeasurementRequest);

ExtMultiRelayChannelMeasurementRequest::ExtMultiRelayChannelMeasurementRequest()
    : m_dialogToken(0) {}

TypeId ExtMultiRelayChannelMeasurementRequest::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::ExtMultiRelayChannelMeasurementRequest")
          .SetParent<Header>()
          .SetGroupName("Wigig")
          .AddConstructor<ExtMultiRelayChannelMeasurementRequest>();
  return tid;
}

TypeId ExtMultiRelayChannelMeasurementRequest::GetInstanceTypeId() const {
  return GetTypeId();
}

void ExtMultiRelayChannelMeasurementRequest::Print(std::ostream &os) const {
  os << "Dialog Token = " << m_dialogToken;
}

uint32_t ExtMultiRelayChannelMeasurementRequest::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1; // Dialog Token
  return size;
}

void ExtMultiRelayChannelMeasurementRequest::Serialize(
    Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
}

uint32_t
ExtMultiRelayChannelMeasurementRequest::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_dialogToken = i.ReadU8();
  return i.GetDistanceFrom(start);
}

void ExtMultiRelayChannelMeasurementRequest::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

uint8_t ExtMultiRelayChannelMeasurementRequest::GetDialogToken() const {
  return m_dialogToken;
}

/***************************************************
 *     Multi Relay Channel Measurement Report
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtMultiRelayChannelMeasurementReport);

ExtMultiRelayChannelMeasurementReport::ExtMultiRelayChannelMeasurementReport()
    : m_dialogToken(0) {}

TypeId ExtMultiRelayChannelMeasurementReport::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::ExtMultiRelayChannelMeasurementReport")
          .SetParent<Header>()
          .SetGroupName("Wigig")
          .AddConstructor<ExtMultiRelayChannelMeasurementReport>();
  return tid;
}

TypeId ExtMultiRelayChannelMeasurementReport::GetInstanceTypeId() const {
  return GetTypeId();
}

void ExtMultiRelayChannelMeasurementReport::Print(std::ostream &os) const {
  os << "Dialog Token = " << m_dialogToken;
}

uint32_t ExtMultiRelayChannelMeasurementReport::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1;                 // Dialog Token
  size += m_list.size() * 4; // Channel Measurement Info
  return size;
}

void ExtMultiRelayChannelMeasurementReport::Serialize(
    Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  Ptr<ExtChannelMeasurementInfo> info;
  i.WriteU8(m_dialogToken);
  for (ChannelMeasurementInfoList::const_iterator item = m_list.begin();
       item != m_list.end(); item++) {
    info = *item;
    i = info->Serialize(i);
  }
}

uint32_t
ExtMultiRelayChannelMeasurementReport::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  Ptr<ExtChannelMeasurementInfo> element;
  m_dialogToken = i.ReadU8();
  while (!i.IsEnd()) {
    element = Create<ExtChannelMeasurementInfo>();
    i = element->Deserialize(i);
    m_list.push_back(element);
  }
  return i.GetDistanceFrom(start);
}

void ExtMultiRelayChannelMeasurementReport::SetDialogToken(uint8_t token) {
  m_dialogToken = token;
}

void ExtMultiRelayChannelMeasurementReport::AddChannelMeasurementInfo(
    Ptr<ExtChannelMeasurementInfo> element) {
  m_list.push_back(element);
}

void ExtMultiRelayChannelMeasurementReport::SetChannelMeasurementList(
    ChannelMeasurementInfoList &list) {
  m_list = list;
}

uint8_t ExtMultiRelayChannelMeasurementReport::GetDialogToken() const {
  return m_dialogToken;
}

ChannelMeasurementInfoList
ExtMultiRelayChannelMeasurementReport::GetChannelMeasurementInfoList() const {
  return m_list;
}

/***************************************************
 *             Announce Frame (8.5.22.2)
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtAnnounceFrame);

ExtAnnounceFrame::ExtAnnounceFrame() : m_timestamp(0), m_beaconInterval(0) {}

TypeId ExtAnnounceFrame::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtAnnounceFrame")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtAnnounceFrame>();
  return tid;
}

TypeId ExtAnnounceFrame::GetInstanceTypeId() const { return GetTypeId(); }

void ExtAnnounceFrame::Print(std::ostream &os) const {
  os << "Timestamp = " << m_timestamp << "|"
     << "BeaconInterval = " << m_beaconInterval;
}

uint32_t ExtAnnounceFrame::GetSerializedSize() const {
  uint32_t size = 0;
  size += 1; // Timestamp
  size += 2; // Beacon Interval
  return size;
}

void ExtAnnounceFrame::Serialize(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_timestamp);
  i.WriteHtolsbU16(m_beaconInterval);
}

uint32_t ExtAnnounceFrame::Deserialize(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_timestamp = i.ReadU8();
  m_beaconInterval = i.ReadLsbtohU16();
  return i.GetDistanceFrom(start);
}

void ExtAnnounceFrame::SetTimestamp(uint8_t timestamp) {
  m_timestamp = timestamp;
}

void ExtAnnounceFrame::SetBeaconInterval(uint16_t interval) {
  m_beaconInterval = interval;
}

uint8_t ExtAnnounceFrame::GetTimestamp() const { return m_timestamp; }

uint16_t ExtAnnounceFrame::GetBeaconInterval() const {
  return m_beaconInterval;
}

/***************************************************
 *               BRP Frame (8.5.22.3)
 ****************************************************/

NS_OBJECT_ENSURE_REGISTERED(ExtBrpFrame);

ExtBrpFrame::ExtBrpFrame() {}

TypeId ExtBrpFrame::GetTypeId() {
  static TypeId tid = TypeId("ns3::ExtBrpFrame")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<ExtBrpFrame>();
  return tid;
}

TypeId ExtBrpFrame::GetInstanceTypeId() const { return GetTypeId(); }

void ExtBrpFrame::PrintImpl(std::ostream &os) const {
  os << "Dialog Token = " << m_dialogToken;
  WifiMgtHeader<ExtBrpFrame, WigigAssocRequestElems>::PrintImpl(os);
}

uint32_t ExtBrpFrame::GetSerializedSizeImpl() const {
  uint32_t size = 0;
  size += 1; // Dialog Token
  size += m_brpRequestField.GetSerializedSize();
  return size + WifiMgtHeader<ExtBrpFrame,
                              WigigAssocRequestElems>::GetSerializedSizeImpl();
}

void ExtBrpFrame::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteU8(m_dialogToken);
  i = m_brpRequestField.Serialize(i);
  WifiMgtHeader<ExtBrpFrame, WigigAssocRequestElems>::SerializeImpl(i);
}

uint32_t ExtBrpFrame::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  Buffer::Iterator m;

  m_dialogToken = i.ReadU8();
  m_brpRequestField.Deserialize(i);

  auto distance = i.GetDistanceFrom(start);
  return distance +
         WifiMgtHeader<ExtBrpFrame, WigigAssocRequestElems>::DeserializeImpl(i);
}

void ExtBrpFrame::SetDialogToken(uint8_t token) { m_dialogToken = token; }

void ExtBrpFrame::SetBrpRequestField(BrpRequestField &field) {
  m_brpRequestField = field;
}

uint8_t ExtBrpFrame::GetDialogToken() const { return m_dialogToken; }

BrpRequestField ExtBrpFrame::GetBrpRequestField() const {
  return m_brpRequestField;
}

NS_OBJECT_ENSURE_REGISTERED(WigigMgtAssocRequestHeader);

TypeId WigigMgtAssocRequestHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMgtAssocRequestHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMgtAssocRequestHeader>();
  return tid;
}

TypeId WigigMgtAssocRequestHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

uint16_t WigigMgtAssocRequestHeader::GetListenInterval() const {
  return m_listenInterval;
}

void WigigMgtAssocRequestHeader::SetListenInterval(uint16_t interval) {
  m_listenInterval = interval;
}

const CapabilityInformation &WigigMgtAssocRequestHeader::Capabilities() const {
  return m_capability;
}

CapabilityInformation &WigigMgtAssocRequestHeader::Capabilities() {
  return m_capability;
}

uint32_t WigigMgtAssocRequestHeader::GetSerializedSizeImpl() const {
  uint32_t size = 0;
  size += m_capability.GetSerializedSize();
  size += 2; // listen interval
  size += WifiMgtHeader<WigigMgtAssocRequestHeader,
                        WigigAssocRequestElems>::GetSerializedSizeImpl();
  return size;
}

void WigigMgtAssocRequestHeader::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i = m_capability.Serialize(i);
  i.WriteHtolsbU16(m_listenInterval);
  WifiMgtHeader<WigigMgtAssocRequestHeader,
                WigigAssocRequestElems>::SerializeImpl(i);
}

uint32_t WigigMgtAssocRequestHeader::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  i = m_capability.Deserialize(i);
  m_listenInterval = i.ReadLsbtohU16();
  auto distance = i.GetDistanceFrom(start) +
                  WifiMgtHeader<WigigMgtAssocRequestHeader,
                                WigigAssocRequestElems>::DeserializeImpl(i);
  return distance;
}

void WigigMgtAssocRequestHeader::PrintImpl(std::ostream &os) const {
  WifiMgtHeader<WigigMgtAssocRequestHeader, WigigAssocRequestElems>::PrintImpl(
      os);
}

NS_OBJECT_ENSURE_REGISTERED(WigigMgtProbeResponseHeader);

TypeId WigigMgtProbeResponseHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMgtProbeResponseHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMgtProbeResponseHeader>();
  return tid;
}

TypeId WigigMgtProbeResponseHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

uint64_t WigigMgtProbeResponseHeader::GetBeaconIntervalUs() const {
  return m_beaconInterval;
}

void WigigMgtProbeResponseHeader::SetBeaconIntervalUs(uint64_t us) {
  m_beaconInterval = us;
}

const CapabilityInformation &WigigMgtProbeResponseHeader::Capabilities() const {
  return m_capability;
}

CapabilityInformation &WigigMgtProbeResponseHeader::Capabilities() {
  return m_capability;
}

uint64_t WigigMgtProbeResponseHeader::GetTimestamp() const {
  return m_timestamp;
}

uint32_t WigigMgtProbeResponseHeader::GetSerializedSizeImpl() const {
  uint32_t size = 8 /* timestamp */ + 2 /* beacon interval */;
  size += m_capability.GetSerializedSize();
  size += WifiMgtHeader<WigigMgtProbeResponseHeader,
                        WigigProbeResponseElems>::GetSerializedSizeImpl();
  return size;
}

void WigigMgtProbeResponseHeader::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i.WriteHtolsbU64(Simulator::Now().GetMicroSeconds());
  i.WriteHtolsbU16(static_cast<uint16_t>(m_beaconInterval / 1024));
  i = m_capability.Serialize(i);
  WifiMgtHeader<WigigMgtProbeResponseHeader,
                WigigProbeResponseElems>::SerializeImpl(i);
}

uint32_t WigigMgtProbeResponseHeader::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  m_timestamp = i.ReadLsbtohU64();
  m_beaconInterval = i.ReadLsbtohU16();
  m_beaconInterval *= 1024;
  i = m_capability.Deserialize(i);
  auto distance = i.GetDistanceFrom(start);
  return distance + WifiMgtHeader<WigigMgtProbeResponseHeader,
                                  WigigProbeResponseElems>::DeserializeImpl(i);
}

/***********************************************************
 *          Assoc/Reassoc Response
 ***********************************************************/

NS_OBJECT_ENSURE_REGISTERED(WigigMgtAssocResponseHeader);

TypeId WigigMgtAssocResponseHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMgtAssocResponseHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMgtAssocResponseHeader>();
  return tid;
}

TypeId WigigMgtAssocResponseHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

StatusCode WigigMgtAssocResponseHeader::GetStatusCode() { return m_code; }

void WigigMgtAssocResponseHeader::SetStatusCode(StatusCode code) {
  m_code = code;
}

const CapabilityInformation &WigigMgtAssocResponseHeader::Capabilities() const {
  return m_capability;
}

CapabilityInformation &WigigMgtAssocResponseHeader::Capabilities() {
  return m_capability;
}

void WigigMgtAssocResponseHeader::SetAssociationId(uint16_t aid) {
  m_aid = aid;
}

uint16_t WigigMgtAssocResponseHeader::GetAssociationId() const { return m_aid; }

uint32_t WigigMgtAssocResponseHeader::GetSerializedSizeImpl() const {
  uint32_t size = 0;
  size += m_capability.GetSerializedSize();
  size += m_code.GetSerializedSize();
  size += 2; // aid
  size += WifiMgtHeader<WigigMgtAssocResponseHeader,
                        WigigAssocResponseElems>::GetSerializedSizeImpl();
  return size;
}

void WigigMgtAssocResponseHeader::PrintImpl(std::ostream &os) const {
  os << "status code=" << m_code << ", "
     << "aid=" << m_aid << ", ";
  WifiMgtHeader<WigigMgtAssocResponseHeader,
                WigigAssocResponseElems>::PrintImpl(os);
}

void WigigMgtAssocResponseHeader::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i = m_capability.Serialize(i);
  i = m_code.Serialize(i);
  i.WriteHtolsbU16(m_aid);
  WifiMgtHeader<WigigMgtAssocResponseHeader,
                WigigAssocResponseElems>::SerializeImpl(i);
}

uint32_t WigigMgtAssocResponseHeader::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  i = m_capability.Deserialize(i);
  i = m_code.Deserialize(i);
  m_aid = i.ReadLsbtohU16();
  auto distance = i.GetDistanceFrom(start) +
                  WifiMgtHeader<WigigMgtAssocResponseHeader,
                                WigigAssocResponseElems>::DeserializeImpl(i);

  return distance;
}

/***********************************************************
 *          Ressoc Request
 ***********************************************************/

NS_OBJECT_ENSURE_REGISTERED(WigigMgtReassocRequestHeader);

TypeId WigigMgtReassocRequestHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMgtReassocRequestHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMgtReassocRequestHeader>();
  return tid;
}

TypeId WigigMgtReassocRequestHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

uint16_t WigigMgtReassocRequestHeader::GetListenInterval() const {
  return m_listenInterval;
}

void WigigMgtReassocRequestHeader::SetListenInterval(uint16_t interval) {
  m_listenInterval = interval;
}

const CapabilityInformation &
WigigMgtReassocRequestHeader::Capabilities() const {
  return m_capability;
}

CapabilityInformation &WigigMgtReassocRequestHeader::Capabilities() {
  return m_capability;
}

void WigigMgtReassocRequestHeader::SetCurrentApAddress(
    Mac48Address currentApAddr) {
  m_currentApAddr = currentApAddr;
}

uint32_t WigigMgtReassocRequestHeader::GetSerializedSizeImpl() const {
  uint32_t size = 0;
  size += m_capability.GetSerializedSize();
  size += 2; // listen interval
  size += 6; // current AP address
  size += WifiMgtHeader<WigigMgtReassocRequestHeader,
                        WigigAssocRequestElems>::GetSerializedSizeImpl();
  return size;
}

void WigigMgtReassocRequestHeader::PrintImpl(std::ostream &os) const {
  os << "current AP address=" << m_currentApAddr << ", ";
  WifiMgtHeader<WigigMgtReassocRequestHeader,
                WigigAssocRequestElems>::PrintImpl(os);
}

void WigigMgtReassocRequestHeader::SerializeImpl(Buffer::Iterator start) const {
  Buffer::Iterator i = start;
  i = m_capability.Serialize(i);
  i.WriteHtolsbU16(m_listenInterval);
  WriteTo(i, m_currentApAddr);
  WifiMgtHeader<WigigMgtReassocRequestHeader,
                WigigAssocRequestElems>::SerializeImpl(i);
}

uint32_t WigigMgtReassocRequestHeader::DeserializeImpl(Buffer::Iterator start) {
  Buffer::Iterator i = start;
  i = m_capability.Deserialize(i);
  m_listenInterval = i.ReadLsbtohU16();
  ReadFrom(i, m_currentApAddr);
  auto distance = i.GetDistanceFrom(start) +
                  WifiMgtHeader<WigigMgtReassocRequestHeader,
                                WigigAssocRequestElems>::DeserializeImpl(i);
  return distance;
}

/***********************************************************
 *          Probe Request
 ***********************************************************/

NS_OBJECT_ENSURE_REGISTERED(WigigMgtProbeRequestHeader);

TypeId WigigMgtProbeRequestHeader::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigMgtProbeRequestHeader")
                          .SetParent<Header>()
                          .SetGroupName("Wigig")
                          .AddConstructor<WigigMgtProbeRequestHeader>();
  return tid;
}

TypeId WigigMgtProbeRequestHeader::GetInstanceTypeId() const {
  return GetTypeId();
}

uint32_t WigigMgtProbeRequestHeader::GetSerializedSizeImpl() const {
  return WifiMgtHeader<WigigMgtProbeRequestHeader,
                       WigigProbeRequestElems>::GetSerializedSizeImpl();
}

void WigigMgtProbeRequestHeader::PrintImpl(std::ostream &os) const {
  WifiMgtHeader<WigigMgtProbeRequestHeader, WigigProbeRequestElems>::PrintImpl(
      os);
}

void WigigMgtProbeRequestHeader::SerializeImpl(Buffer::Iterator start) const {
  WifiMgtHeader<WigigMgtProbeRequestHeader,
                WigigProbeRequestElems>::SerializeImpl(start);
}

uint32_t WigigMgtProbeRequestHeader::DeserializeImpl(Buffer::Iterator start) {
  return WifiMgtHeader<WigigMgtProbeRequestHeader,
                       WigigProbeRequestElems>::DeserializeImpl(start);
}

} // namespace ns3
