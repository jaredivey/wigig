/*
 * Copyright (c) 2005,2006,2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#ifndef WIGIG_REMOTE_STATION_MANAGER_H
#define WIGIG_REMOTE_STATION_MANAGER_H

#include "wigig-mac-header.h"

#include "ns3/data-rate.h"
#include "ns3/dmg-capabilities.h"
#include "ns3/mac48-address.h"
#include "ns3/object.h"
#include "ns3/qos-utils.h"
#include "ns3/traced-callback.h"
#include "ns3/wifi-mode.h"
#include "ns3/wifi-phy-common.h"
#include "ns3/wifi-remote-station-info.h"
#include "ns3/wifi-remote-station-manager.h"
#include "ns3/wifi-utils.h"

#include <array>
#include <functional>
#include <optional>

namespace ns3 {

class WigigMacHeader;
class Packet;
class WigigTxVector;
class WigigPhy;
class WigigMac;

struct WigigRemoteStationState;

/**
 * \brief hold per-remote-station state.
 *
 * The state in this class is used to keep track
 * of association status if we are in an infrastructure
 * network and to perform the selection of TX parameters
 * on a per-packet basis.
 *
 * This class is typically subclassed and extended by
 * rate control implementations
 */
struct WigigRemoteStation {
  virtual ~WigigRemoteStation(){};
  WigigRemoteStationState *m_state; //!< Remote station state
};

/**
 * A struct that holds information about each remote station.
 */
struct WigigRemoteStationState : public WifiRemoteStationState {
  DmgCapabilities m_dmgCapabilities; //!< remote station DMG capabilities
  bool m_dmgSupported; //!< Flag if DMG is supported by the station
};

/**
 * \ingroup wigig
 * \brief hold a list of per-remote-station state.
 *
 * \sa ns3::WigigRemoteStation.
 */
class WigigRemoteStationManager : public Object {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  WigigRemoteStationManager();
  ~WigigRemoteStationManager() override;

  /**
   * A vector of WigigRemoteStations
   */
  typedef std::vector<WigigRemoteStation *> Stations;
  /**
   * A vector of WigigRemoteStationStates
   */
  typedef std::vector<WigigRemoteStationState *> StationStates;

  /**
   * Set up PHY associated with this device since it is the object that
   * knows the full set of transmit rates that are supported.
   *
   * \param phy the PHY of this device
   */
  virtual void SetupPhy(const Ptr<WigigPhy> phy);
  /**
   * Set up MAC associated with this device since it is the object that
   * knows the full set of timing parameters (e.g. IFS).
   *
   * \param mac the MAC of this device
   */
  virtual void SetupMac(const Ptr<WigigMac> mac);

  /**
   * Sets the maximum STA short retry count (SSRC).
   *
   * \param maxSsrc the maximum SSRC
   */
  void SetMaxSsrc(uint32_t maxSsrc);
  /**
   * Sets the maximum STA long retry count (SLRC).
   *
   * \param maxSlrc the maximum SLRC
   */
  void SetMaxSlrc(uint32_t maxSlrc);
  /**
   * Sets the RTS threshold.
   *
   * \param threshold the RTS threshold
   */
  void SetRtsCtsThreshold(uint32_t threshold);

  /**
   * Return the fragmentation threshold.
   *
   * \return the fragmentation threshold
   */
  uint32_t GetFragmentationThreshold() const;
  /**
   * Sets a fragmentation threshold. The method calls a private method
   * DoSetFragmentationThreshold that checks the validity of the value given.
   *
   * \param threshold the fragmentation threshold
   */
  void SetFragmentationThreshold(uint32_t threshold);
  /**
   * Typically called to update the fragmentation threshold at the start of a
   * new transmission. This avoid that the fragmentation threshold gets changed
   * during a transmission (see bug 730).
   */
  void UpdateFragmentationThreshold();

  /**
   * Records DMG capabilities of the remote station.
   *
   * \param from the address of the station being recorded
   * \param dmgCapabilities the DMG capabilities of the station
   */
  void AddStationDmgCapabilities(Mac48Address from,
                                 DmgCapabilities dmgCapabilities);
  /**
   * Return the DMG capabilities sent by the remote station.
   *
   * \param from the address of the remote station
   * \return the DMG capabilities sent by the remote station
   */
  std::optional<std::reference_wrapper<DmgCapabilities>>
  GetStationDmgCapabilities(Mac48Address from);

  /**
   * Enable or disable DMG capability support.
   *
   * \param enable enable or disable DMG capability support
   */
  void SetDmgSupported(bool enable);
  /**
   * Return whether the device has DMG capability support enabled.
   *
   * \return true if DMG capability support is enabled, false otherwise
   */
  bool HasDmgSupported() const;
  /**
   * Return whether the device has DMG capability support enabled.
   *
   * \return true if either the DMG capability support is enabled, false
   * otherwise.
   */
  bool IsWiGigSupported() const;
  /**
   * Reset the station, invoked in a STA upon dis-association or in an AP upon
   * reboot.
   */
  void Reset();

  /**
   * Invoked in a STA upon association to store the set of rates which belong to
   * the BSSBasicRateSet of the associated AP and which are supported locally.
   * Invoked in an AP to configure the BSSBasicRateSet.
   *
   * \param mode the WifiMode to be added to the basic mode set
   */
  void AddBasicMode(WifiMode mode);
  /**
   * Return the default transmission mode.
   *
   * \return WifiMode the default transmission mode
   */
  WifiMode GetDefaultMode() const;
  /**
   * Return the number of basic modes we support.
   *
   * \return the number of basic modes we support
   */
  uint8_t GetNBasicModes() const;
  /**
   * Return a basic mode from the set of basic modes.
   *
   * \param i index of the basic mode in the basic mode set
   *
   * \return the basic mode at the given index
   */
  WifiMode GetBasicMode(uint8_t i) const;
  /**
   * Return the default Modulation and Coding Scheme (MCS) index.
   *
   * \return the default WifiMode
   */
  WifiMode GetDefaultMcs() const;

  /**
   * Return a mode for non-unicast packets.
   *
   * \return WifiMode for non-unicast packets
   */
  WifiMode GetNonUnicastMode() const;

  /**
   * Invoked in a STA or AP to store the set of
   * modes supported by a destination which is
   * also supported locally.
   * The set of supported modes includes
   * the BSSBasicRateSet.
   *
   * \param address the address of the station being recorded
   * \param mode the WifiMode supports by the station
   */
  void AddSupportedMode(Mac48Address address, WifiMode mode);
  /**
   * Invoked in a STA or AP to store all of the modes supported
   * by a destination which is also supported locally.
   * The set of supported modes includes the BSSBasicRateSet.
   *
   * \param address the address of the station being recorded
   */
  void AddAllSupportedModes(Mac48Address address);
  /**
   * Return whether the station state is brand new.
   *
   * \param address the address of the station
   *
   * \return true if the state of the station is brand new,
   *         false otherwise
   */
  bool IsBrandNew(Mac48Address address) const;
  /**
   * Return whether the station associated.
   *
   * \param address the address of the station
   *
   * \return true if the station is associated,
   *         false otherwise
   */
  bool IsAssociated(Mac48Address address) const;
  /**
   * Return whether we are waiting for an ACK for
   * the association response we sent.
   *
   * \param address the address of the station
   *
   * \return true if the station is associated,
   *         false otherwise
   */
  bool IsWaitAssocTxOk(Mac48Address address) const;
  /**
   * Records that we are waiting for an ACK for
   * the association response we sent.
   *
   * \param address the address of the station
   * \param assocaitionID The association identifier that we provide to the
   * station.
   */
  void RecordWaitAssocTxOk(Mac48Address address, uint8_t associationID = 0);
  /**
   * Records that we got an ACK for
   * the association response we sent.
   *
   * \param address the address of the station
   * \return Association identifier assigned to the station.
   */
  uint8_t RecordGotAssocTxOk(Mac48Address address);
  /**
   * Records that we missed an ACK for
   * the association response we sent.
   *
   * \param address the address of the station
   */
  void RecordGotAssocTxFailed(Mac48Address address);
  /**
   * Records that the STA was disassociated.
   *
   * \param address the address of the station
   */
  void RecordDisassociated(Mac48Address address);

  /**
   * \param header MAC header
   *
   * \return the TXVECTOR to use to send this packet
   */
  WigigTxVector GetDataTxVector(const WigigMacHeader &header);
  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   *
   * \return the transmission mode to use to send this DMG packet
   */
  WigigTxVector GetDmgTxVector(Mac48Address address,
                               const WigigMacHeader *header,
                               Ptr<const Packet> packet);
  /**
   * \param address remote address
   *
   * \return the TXVECTOR to use to send the RTS prior to the
   *         transmission of the data packet itself.
   */
  WigigTxVector GetRtsTxVector(Mac48Address address);

  /**
   * Should be invoked whenever the RtsTimeout associated to a transmission
   * attempt expires.
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   */
  void ReportRtsFailed(Mac48Address address, const WigigMacHeader *header);
  /**
   * Should be invoked whenever the AckTimeout associated to a transmission
   * attempt expires.
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   * \param packetSize the size of the DATA packet
   */
  void ReportDataFailed(Mac48Address address, const WigigMacHeader *header,
                        uint32_t packetSize);
  /**
   * Should be invoked whenever we receive the CTS associated to an RTS
   * we just sent. Note that we also get the SNR of the RTS we sent since
   * the receiver put a SnrTag in the CTS.
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   * \param ctsSnr the SNR of the CTS we received
   * \param ctsMode the WifiMode the receiver used to send the CTS
   * \param rtsSnr the SNR of the RTS we sent
   */
  void ReportRtsOk(Mac48Address address, const WigigMacHeader *header,
                   double ctsSnr, WifiMode ctsMode, double rtsSnr);
  /**
   * Should be invoked whenever we receive the ACK associated to a data packet
   * we just sent.
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   * \param ackSnr the SNR of the ACK we received
   * \param ackMode the WifiMode the receiver used to send the ACK
   * \param dataSnr the SNR of the DATA we sent
   * \param dataTxVector the TXVECTOR of the DATA we sent
   * \param packetSize the size of the DATA packet
   */
  void ReportDataOk(Mac48Address address, const WigigMacHeader *header,
                    double ackSnr, WifiMode ackMode, double dataSnr,
                    const WigigTxVector &dataTxVector, uint32_t packetSize);
  /**
   * Should be invoked after calling ReportRtsFailed if
   * NeedRetransmission returns false
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   */
  void ReportFinalRtsFailed(Mac48Address address, const WigigMacHeader *header);
  /**
   * Should be invoked after calling ReportDataFailed if
   * NeedRetransmission returns false
   *
   * \param address the address of the receiver
   * \param header MAC header of the DATA packet
   * \param packetSize the size of the DATA packet
   */
  void ReportFinalDataFailed(Mac48Address address, const WigigMacHeader *header,
                             uint32_t packetSize);
  /**
   * Typically called per A-MPDU, either when a Block ACK was successfully
   * received or when a BlockAckTimeout has elapsed.
   *
   * \param address the address of the receiver
   * \param nSuccessfulMpdus number of successfully transmitted MPDUs
   * A value of 0 means that the Block ACK was missed.
   * \param nFailedMpdus number of unsuccessfully transmitted MPDUs
   * \param rxSnr received SNR of the block ack frame itself
   * \param dataSnr data SNR reported by remote station
   * \param dataTxVector the TXVECTOR of the MPDUs we sent
   */
  void ReportAmpduTxStatus(Mac48Address address, uint8_t nSuccessfulMpdus,
                           uint8_t nFailedMpdus, double rxSnr, double dataSnr,
                           const WigigTxVector &dataTxVector);

  /**
   * \param address remote address
   * \param header MAC header
   * \param rxSnr the SNR of the packet received
   * \param txMode the transmission mode used for the packet received
   *
   * Should be invoked whenever a packet is successfully received.
   */
  void ReportRxOk(Mac48Address address, const WigigMacHeader *header,
                  double rxSnr, WifiMode txMode);

  /**
   * \param header MAC header
   * \param size the size of the frame to send in bytes
   *
   * \return true if we want to use an RTS/CTS handshake for this
   *         frame before sending it, false otherwise.
   */
  bool NeedRts(const WigigMacHeader &header, uint32_t size);
  /**
   * Return if we need to do CTS-to-self before sending a DATA.
   *
   * \param txVector the TXVECTOR of the packet to be sent
   *
   * \return true if CTS-to-self is needed,
   *         false otherwise
   */
  bool NeedCtsToSelf(const WigigTxVector &txVector);

  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   *
   * \return true if we want to resend a packet after a failed transmission
   * attempt, false otherwise.
   */
  bool NeedRetransmission(Mac48Address address, const WigigMacHeader *header,
                          Ptr<const Packet> packet);
  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   *
   * \return true if this packet should be fragmented,
   *         false otherwise.
   */
  bool NeedFragmentation(Mac48Address address, const WigigMacHeader *header,
                         Ptr<const Packet> packet);
  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   * \param fragmentNumber the fragment index of the next fragment to send
   * (starts at zero).
   *
   * \return the size of the corresponding fragment.
   */
  uint32_t GetFragmentSize(Mac48Address address, const WigigMacHeader *header,
                           Ptr<const Packet> packet, uint32_t fragmentNumber);
  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   * \param fragmentNumber the fragment index of the next fragment to send
   * (starts at zero).
   *
   * \return the offset within the original packet where this fragment starts.
   */
  uint32_t GetFragmentOffset(Mac48Address address, const WigigMacHeader *header,
                             Ptr<const Packet> packet, uint32_t fragmentNumber);
  /**
   * \param address remote address
   * \param header MAC header
   * \param packet the packet to send
   * \param fragmentNumber the fragment index of the next fragment to send
   * (starts at zero).
   *
   * \return true if this is the last fragment, false otherwise.
   */
  bool IsLastFragment(Mac48Address address, const WigigMacHeader *header,
                      Ptr<const Packet> packet, uint32_t fragmentNumber);

  /**
   * \return the default transmission power
   */
  uint8_t GetDefaultTxPowerLevel() const;

  /**
   * \return the DMG Control transmission mode (MCS0).
   */
  WigigTxVector GetDmgControlTxVector();
  /**
   * Return the value of the last received SNR.
   *
   * \return the value of the last received SNR.
   */
  double GetRxSnr() const;

protected:
  void DoDispose() override;
  /**
   * Return whether mode associated with the specified station at the specified
   * index.
   *
   * \param station the station being queried
   * \param i the index
   *
   * \return WifiMode at the given index of the specified station
   */
  WifiMode GetSupported(const WigigRemoteStation *station, uint8_t i) const;
  /**
   * Return the number of modes supported by the given station.
   *
   * \param station the station being queried
   *
   * \return the number of modes supported by the given station
   */
  uint8_t GetNSupported(const WigigRemoteStation *station) const;
  /**
   * Return the address of the station.
   *
   * \param station the station being queried
   *
   * \return the address of the station
   */
  Mac48Address GetAddress(const WigigRemoteStation *station) const;
  /**
   * Return the channel width supported by the station.
   *
   * \param station the station being queried
   *
   * \return the channel width (in MHz) supported by the station
   */
  uint16_t GetChannelWidth(const WigigRemoteStation *station) const;
  /**
   * Return whether the given station supports A-MPDU.
   *
   * \param station the station being queried
   *
   * \return true if the station supports MPDU aggregation,
   *         false otherwise
   */
  bool GetAggregation(const WigigRemoteStation *station) const;
  /**
   * \returns the number of Ness the station has.
   *
   * \param station the station being queried
   *
   * \return the number of Ness the station has
   */
  uint8_t GetNess(const WigigRemoteStation *station) const;

  /**
   * Return the WigigPhy.
   *
   * \return a pointer to the WigigPhy
   */
  Ptr<WigigPhy> GetPhy() const;
  /**
   * Return the WigigMac.
   *
   * \return a pointer to the WigigMac
   */
  Ptr<WigigMac> GetMac() const;

private:
  /**
   * \param station the station that we need to communicate
   * \param size the size of the frame to send in bytes
   * \param normally indicates whether the normal 802.11 RTS enable mechanism
   * would request that the RTS is sent or not.
   *
   * \return true if we want to use an RTS/CTS handshake for this frame before
   * sending it, false otherwise.
   *
   * Note: This method is called before a unicast packet is sent on the medium.
   */
  virtual bool DoNeedRts(WigigRemoteStation *station, uint32_t size,
                         bool normally);
  /**
   * \param station the station that we need to communicate
   * \param packet the packet to send
   * \param normally indicates whether the normal 802.11 data retransmission
   * mechanism would request that the data is retransmitted or not. \return true
   * if we want to resend a packet after a failed transmission attempt, false
   * otherwise.
   *
   * Note: This method is called after any unicast packet transmission (control,
   * management, or data) has been attempted and has failed.
   */
  virtual bool DoNeedRetransmission(WigigRemoteStation *station,
                                    Ptr<const Packet> packet, bool normally);
  /**
   * \param station the station that we need to communicate
   * \param packet the packet to send
   * \param normally indicates whether the normal 802.11 data fragmentation
   * mechanism would request that the data packet is fragmented or not.
   *
   * \return true if this packet should be fragmented,
   *         false otherwise.
   *
   * Note: This method is called before sending a unicast packet.
   */
  virtual bool DoNeedFragmentation(WigigRemoteStation *station,
                                   Ptr<const Packet> packet, bool normally);
  /**
   * \return a new station data structure
   */
  virtual WigigRemoteStation *DoCreateStation() const = 0;
  /**
   * \param station the station that we need to communicate
   *
   * \return the TXVECTOR to use to send a packet to the station
   *
   * Note: This method is called before sending a unicast packet or a fragment
   *       of a unicast packet to decide which transmission mode to use.
   */
  virtual WigigTxVector DoGetDataTxVector(WigigRemoteStation *station) = 0;
  /**
   * \param station the station that we need to communicate
   *
   * \return the transmission mode to use to send an RTS to the station
   *
   * Note: This method is called before sending an RTS to a station
   *       to decide which transmission mode to use for the RTS.
   */
  virtual WigigTxVector DoGetRtsTxVector(WigigRemoteStation *station) = 0;

  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we failed to send RTS
   */
  virtual void DoReportRtsFailed(WigigRemoteStation *station) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we failed to send DATA
   */
  virtual void DoReportDataFailed(WigigRemoteStation *station) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we successfully sent RTS
   * \param ctsSnr the SNR of the CTS we received
   * \param ctsMode the WifiMode the receiver used to send the CTS
   * \param rtsSnr the SNR of the RTS we sent
   */
  virtual void DoReportRtsOk(WigigRemoteStation *station, double ctsSnr,
                             WifiMode ctsMode, double rtsSnr) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we successfully sent RTS
   * \param ackSnr the SNR of the ACK we received
   * \param ackMode the WifiMode the receiver used to send the ACK
   * \param dataSnr the SNR of the DATA we sent
   * \param dataChannelWidth the channel width (in MHz) of the DATA we sent
   * \param dataNss the number of spatial streams used to send the DATA
   */
  virtual void DoReportDataOk(WigigRemoteStation *station, double ackSnr,
                              WifiMode ackMode, double dataSnr,
                              uint16_t dataChannelWidth, uint8_t dataNss) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we failed to send RTS
   */
  virtual void DoReportFinalRtsFailed(WigigRemoteStation *station) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that we failed to send DATA
   */
  virtual void DoReportFinalDataFailed(WigigRemoteStation *station) = 0;
  /**
   * This method is a pure virtual method that must be implemented by the
   * sub-class. This allows different types of WigigRemoteStationManager to
   * respond differently,
   *
   * \param station the station that sent the DATA to us
   * \param rxSnr the SNR of the DATA we received
   * \param txMode the WifiMode the sender used to send the DATA
   */
  virtual void DoReportRxOk(WigigRemoteStation *station, double rxSnr,
                            WifiMode txMode) = 0;
  /**
   * Typically called per A-MPDU, either when a Block ACK was successfully
   * received or when a BlockAckTimeout has elapsed. This method is a virtual
   * method that must be implemented by the sub-class intended to handle
   * A-MPDUs. This allows different types of WigigRemoteStationManager to
   * respond differently.
   *
   * \param station the station that sent the DATA to us
   * \param nSuccessfulMpdus number of successfully transmitted MPDUs.
   *        A value of 0 means that the Block ACK was missed.
   * \param nFailedMpdus number of unsuccessfully transmitted MPDUs.
   * \param rxSnr received SNR of the block ack frame itself
   * \param dataSnr data SNR reported by remote station
   * \param dataChannelWidth the channel width (in MHz) of the A-MPDU we sent
   * \param dataNss the number of spatial streams used to send the A-MPDU
   */
  virtual void DoReportAmpduTxStatus(WigigRemoteStation *station,
                                     uint8_t nSuccessfulMpdus,
                                     uint8_t nFailedMpdus, double rxSnr,
                                     double dataSnr, uint16_t dataChannelWidth,
                                     uint8_t dataNss);

  /**
   * Return the state of the station associated with the given address.
   *
   * \param address the address of the station
   * \return WigigRemoteStationState corresponding to the address
   */
  WigigRemoteStationState *LookupState(Mac48Address address) const;
  /**
   * Return the station associated with the given address.
   *
   * \param address the address of the station
   *
   * \return WigigRemoteStation corresponding to the address
   */
  WigigRemoteStation *Lookup(Mac48Address address) const;

  /**
   * Actually sets the fragmentation threshold, it also checks the validity of
   * the given threshold.
   *
   * \param threshold the fragmentation threshold
   */
  void DoSetFragmentationThreshold(uint32_t threshold);
  /**
   * Return the current fragmentation threshold
   *
   * \return the fragmentation threshold
   */
  uint32_t DoGetFragmentationThreshold() const;
  /**
   * Return the number of fragments needed for the given packet.
   *
   * \param header MAC header
   * \param packet the packet to be fragmented
   *
   * \return the number of fragments needed
   */
  uint32_t GetNFragments(const WigigMacHeader *header,
                         Ptr<const Packet> packet);

  /**
   * This is a pointer to the WigigPhy associated with this
   * WigigRemoteStationManager that is set on call to
   * WigigRemoteStationManager::SetupPhy(). Through this pointer the
   * station manager can determine PHY characteristics, such as the
   * set of all transmission rates that may be supported (the
   * "DeviceRateSet").
   */
  Ptr<WigigPhy> m_wifiPhy;
  /**
   * This is a pointer to the WigigMac associated with this
   * WigigRemoteStationManager that is set on call to
   * WigigRemoteStationManager::SetupMac(). Through this pointer the
   * station manager can determine MAC characteristics, such as the
   * interframe spaces.
   */
  Ptr<WigigMac> m_wifiMac;

  /**
   * This member is the list of WifiMode objects that comprise the
   * BSSBasicRateSet parameter. This list is constructed through calls
   * to WigigRemoteStationManager::AddBasicMode(), and an API that
   * allows external access to it is available through
   * WigigRemoteStationManager::GetNBasicModes() and
   * WigigRemoteStationManager::GetBasicMode().
   */
  WifiModeList m_bssBasicRateSet; //!< basic rate set
  WifiModeList m_bssBasicMcsSet;  //!< basic MCS set

  StationStates m_states; //!< States of known stations
  Stations m_stations;    //!< Information for each known stations

  WifiMode m_defaultTxMode; //!< The default transmission mode
  WifiMode m_defaultTxMcs;  //!< The default transmission modulation-coding
                            //!< scheme (MCS)

  bool m_dmgSupported;               //!< Flag if DMG capability is supported
  double m_rxSnr;                    //!< The value of the last received SNR.
  uint32_t m_maxSsrc;                //!< Maximum STA short retry count (SSRC)
  uint32_t m_maxSlrc;                //!< Maximum STA long retry count (SLRC)
  uint32_t m_rtsCtsThreshold;        //!< Threshold for RTS/CTS
  uint32_t m_fragmentationThreshold; //!< Current threshold for fragmentation
  uint32_t
      m_nextFragmentationThreshold; //!< Threshold for fragmentation that will
                                    //!< be used for the next transmission
  uint8_t m_defaultTxPowerLevel; //!< Default transmission power level
  WifiMode m_nonUnicastMode; //!< Transmission mode for non-unicast Data frames

  std::array<uint32_t, AC_BE_NQOS> m_ssrc; //!< short retry count per AC
  std::array<uint32_t, AC_BE_NQOS> m_slrc; //!< long retry count per AC

  /**
   * The trace source fired when the transmission of a single RTS has failed
   */
  TracedCallback<Mac48Address> m_macTxRtsFailed;
  /**
   * The trace source fired when the transmission of a single data packet has
   * failed
   */
  TracedCallback<Mac48Address> m_macTxDataFailed;
  /**
   * The trace source fired when the transmission of a RTS has
   * exceeded the maximum number of attempts
   */
  TracedCallback<Mac48Address> m_macTxFinalRtsFailed;
  /**
   * The trace source fired when the transmission of a data packet has
   * exceeded the maximum number of attempts
   */
  TracedCallback<Mac48Address> m_macTxFinalDataFailed;
  /**
   * The trace source fired when the transmission of a single MPDU has
   * succeeded.
   */
  TracedCallback<Mac48Address> m_macTxOk;
  TracedCallback<WifiMacType, Mac48Address, double>
      m_macRxOk; /* Trace for Successful WifiMAC Reception */
  /**
   * TracedCallback signature for MAC Rx OK.
   *
   * \param type The type of the WifiMAC Header.
   * \param address The MAC address of the station.
   * \param snr The snr value in linear scale.
   */
  typedef void (*MacRxOkCallback)(WifiMacType type, Mac48Address address,
                                  double snr);
};

} // namespace ns3

#endif /* WIGIG_REMOTE_STATION_MANAGER_H */
