/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-phy.h"

#include "codebook.h"
#include "wigig-channel.h"
#include "wigig-frame-capture-model.h"
#include "wigig-mpdu-aggregator.h"
#include "wigig-net-device.h"
#include "wigig-ppdu.h"
#include "wigig-psdu.h"

#include "ns3/boolean.h"
#include "ns3/error-model.h"
#include "ns3/error-rate-model.h"
#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/pointer.h"
#include "ns3/preamble-detection-model.h"
#include "ns3/qos-utils.h"
#include "ns3/random-variable-stream.h"
#include "ns3/simulator.h"
#include "ns3/wifi-phy-common.h"
#include "ns3/wifi-radio-energy-model.h"
#include "ns3/wifi-utils.h"

#include <algorithm>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigPhy");

NS_OBJECT_ENSURE_REGISTERED(WigigPhy);

WigigPhy::ChannelToFrequencyWidthMap WigigPhy::m_channelToFrequencyWidth = {
    /* 802.11ad (2.16 GHz channels at the 56.16-64.8 GHz band) */
    {std::make_pair(1, WIFI_STANDARD_80211ad), std::make_pair(58320, 2160)},
    {std::make_pair(2, WIFI_STANDARD_80211ad), std::make_pair(60480, 2160)},
    {std::make_pair(3, WIFI_STANDARD_80211ad), std::make_pair(62640, 2160)},
    {std::make_pair(4, WIFI_STANDARD_80211ad), std::make_pair(64800, 2160)},
    {std::make_pair(5, WIFI_STANDARD_80211ad), std::make_pair(66960, 2160)},
    {std::make_pair(6, WIFI_STANDARD_80211ad), std::make_pair(69120, 2160)},
};

TypeId WigigPhy::GetTypeId() {
  static TypeId tid =
      TypeId("ns3::WigigPhy")
          .SetParent<Object>()
          .SetGroupName("Wigig")
          .AddConstructor<WigigPhy>()
          .AddAttribute("Frequency", "The operating center frequency (MHz)",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigPhy::GetFrequency,
                                             &WigigPhy::SetFrequency),
                        MakeUintegerChecker<uint16_t>())
          .AddAttribute("ChannelWidth", "Only 2160 MHz is supported",
                        UintegerValue(2160),
                        MakeUintegerAccessor(&WigigPhy::GetChannelWidth,
                                             &WigigPhy::SetChannelWidth),
                        MakeUintegerChecker<uint16_t>(2160, 2160))
          .AddAttribute("ChannelNumber",
                        "If set to non-zero defined value, will control "
                        "Frequency and ChannelWidth "
                        "assignment",
                        UintegerValue(0),
                        MakeUintegerAccessor(&WigigPhy::SetChannelNumber,
                                             &WigigPhy::GetChannelNumber),
                        MakeUintegerChecker<uint8_t>(0, 196))
          .AddAttribute("EnergyDetectionThreshold",
                        "The energy of a received signal should be higher than "
                        "this threshold (dBm) to allow the PHY layer to detect "
                        "the signal.",
                        DoubleValue(-101.0),
                        MakeDoubleAccessor(&WigigPhy::SetEdThreshold),
                        MakeDoubleChecker<double>(), TypeId::DEPRECATED,
                        "Replaced by RxSensitivity.")
          .AddAttribute(
              "RxSensitivity",
              "The energy of a received signal should be higher than "
              "this threshold (dBm) for the PHY to detect the signal.",
              DoubleValue(-101.0),
              MakeDoubleAccessor(&WigigPhy::SetRxSensitivity,
                                 &WigigPhy::GetRxSensitivity),
              MakeDoubleChecker<double>())
          .AddAttribute(
              "CcaEdThreshold",
              "The energy of a non Wi-Fi received signal should be higher than "
              "this threshold (dBm) to allow the PHY layer to declare CCA BUSY "
              "state. "
              "This check is performed on the 20 MHz primary channel only.",
              DoubleValue(-62.0),
              MakeDoubleAccessor(&WigigPhy::SetCcaEdThreshold,
                                 &WigigPhy::GetCcaEdThreshold),
              MakeDoubleChecker<double>())
          .AddAttribute(
              "TxGain", "Transmission gain (dB).", DoubleValue(0.0),
              MakeDoubleAccessor(&WigigPhy::SetTxGain, &WigigPhy::GetTxGain),
              MakeDoubleChecker<double>())
          .AddAttribute(
              "RxGain", "Reception gain (dB).", DoubleValue(0.0),
              MakeDoubleAccessor(&WigigPhy::SetRxGain, &WigigPhy::GetRxGain),
              MakeDoubleChecker<double>())
          .AddAttribute("TxPowerLevels",
                        "Number of transmission power levels available between "
                        "TxPowerStart and TxPowerEnd included.",
                        UintegerValue(1),
                        MakeUintegerAccessor(&WigigPhy::m_nTxPower),
                        MakeUintegerChecker<uint8_t>())
          .AddAttribute("TxPowerEnd",
                        "Maximum available transmission level (dBm).",
                        DoubleValue(16.0206),
                        MakeDoubleAccessor(&WigigPhy::SetTxPowerEnd,
                                           &WigigPhy::GetTxPowerEnd),
                        MakeDoubleChecker<double>())
          .AddAttribute("TxPowerStart",
                        "Minimum available transmission level (dBm).",
                        DoubleValue(16.0206),
                        MakeDoubleAccessor(&WigigPhy::SetTxPowerStart,
                                           &WigigPhy::GetTxPowerStart),
                        MakeDoubleChecker<double>())
          .AddAttribute("RxNoiseFigure",
                        "Loss (dB) in the Signal-to-Noise-Ratio due to "
                        "non-idealities in the receiver."
                        " According to Wikipedia "
                        "(http://en.wikipedia.org/wiki/Noise_figure), this is "
                        "\"the difference in decibels (dB) between"
                        " the noise output of the actual receiver to the noise "
                        "output of an "
                        " ideal receiver with the same overall gain and "
                        "bandwidth when the receivers "
                        " are connected to sources at the standard noise "
                        "temperature T0 (usually 290 K)\".",
                        DoubleValue(7),
                        MakeDoubleAccessor(&WigigPhy::SetRxNoiseFigure),
                        MakeDoubleChecker<double>())
          .AddAttribute("State", "The state of the PHY layer.", PointerValue(),
                        MakePointerAccessor(&WigigPhy::m_state),
                        MakePointerChecker<WigigPhyStateHelper>())
          .AddAttribute("ChannelSwitchDelay",
                        "Delay between two short frames transmitted on "
                        "different frequencies.",
                        TimeValue(MicroSeconds(250)),
                        MakeTimeAccessor(&WigigPhy::m_channelSwitchDelay),
                        MakeTimeChecker())
          .AddAttribute("Antennas", "The number of antennas on the device.",
                        UintegerValue(1),
                        MakeUintegerAccessor(&WigigPhy::GetNumberOfAntennas,
                                             &WigigPhy::SetNumberOfAntennas),
                        MakeUintegerChecker<uint8_t>(1, 8))
          .AddAttribute(
              "MaxSupportedTxSpatialStreams",
              "The maximum number of supported TX spatial streams."
              "This parameter is only valuable for 802.11n/ac/ax STAs and APs.",
              UintegerValue(1),
              MakeUintegerAccessor(&WigigPhy::GetMaxSupportedTxSpatialStreams,
                                   &WigigPhy::SetMaxSupportedTxSpatialStreams),
              MakeUintegerChecker<uint8_t>(1, 8))
          .AddAttribute(
              "MaxSupportedRxSpatialStreams",
              "The maximum number of supported RX spatial streams."
              "This parameter is only valuable for 802.11n/ac/ax STAs and APs.",
              UintegerValue(1),
              MakeUintegerAccessor(&WigigPhy::GetMaxSupportedRxSpatialStreams,
                                   &WigigPhy::SetMaxSupportedRxSpatialStreams),
              MakeUintegerChecker<uint8_t>(1, 8))
          .AddAttribute(
              "FrameCaptureModel",
              "Ptr to an object that implements the frame capture model",
              PointerValue(),
              MakePointerAccessor(&WigigPhy::m_frameCaptureModel),
              MakePointerChecker<WigigFrameCaptureModel>())
          .AddAttribute(
              "PreambleDetectionModel",
              "Ptr to an object that implements the preamble detection model",
              PointerValue(),
              MakePointerAccessor(&WigigPhy::m_preambleDetectionModel),
              MakePointerChecker<PreambleDetectionModel>())
          .AddAttribute(
              "PostReceptionErrorModel",
              "An optional packet error model can be added to the receive "
              "packet process after any propagation-based (SNR-based) error "
              "models have been applied. Typically this is used to force "
              "specific packet drops, for testing purposes.",
              PointerValue(),
              MakePointerAccessor(&WigigPhy::m_postReceptionErrorModel),
              MakePointerChecker<ErrorModel>())
          .AddAttribute("SupportOfdmPhy",
                        "Whether the DMG STA supports OFDM PHY layer.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigPhy::SetSupportOfdmPhy,
                                            &WigigPhy::GetSupportOfdmPhy),
                        MakeBooleanChecker())
          .AddAttribute("SupportLpScPhy",
                        "Whether the DMG STA supports LP-SC PHY layer.",
                        BooleanValue(false),
                        MakeBooleanAccessor(&WigigPhy::SetSupportLpScPhy,
                                            &WigigPhy::GetSupportLpScPhy),
                        MakeBooleanChecker())
          .AddAttribute("MaxScTxMcs",
                        "The maximum supported MCS for transmission by SC PHY.",
                        UintegerValue(12),
                        MakeUintegerAccessor(&WigigPhy::m_maxScTxMcs),
                        MakeUintegerChecker<uint8_t>(4, 12))
          .AddAttribute("MaxScRxMcs",
                        "The maximum supported MCS for reception by SC PHY.",
                        UintegerValue(12),
                        MakeUintegerAccessor(&WigigPhy::m_maxScRxMcs),
                        MakeUintegerChecker<uint8_t>(4, 12))
          .AddAttribute(
              "MaxOfdmTxMcs",
              "The maximum supported MCS for transmission by OFDM PHY.",
              UintegerValue(24),
              MakeUintegerAccessor(&WigigPhy::m_maxOfdmTxMcs),
              MakeUintegerChecker<uint8_t>(18, 24))
          .AddAttribute("MaxOfdmRxMcs",
                        "The maximum supported MCS for reception by OFDM PHY.",
                        UintegerValue(24),
                        MakeUintegerAccessor(&WigigPhy::m_maxOfdmRxMcs),
                        MakeUintegerChecker<uint8_t>(18, 24))
          .AddTraceSource("PhyTxBegin",
                          "Trace source indicating a packet "
                          "has begun transmitting over the channel medium",
                          MakeTraceSourceAccessor(&WigigPhy::m_phyTxBeginTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "PhyTxPsduBegin",
              "Trace source indicating a PSDU "
              "has begun transmitting over the channel medium",
              MakeTraceSourceAccessor(&WigigPhy::m_phyTxPsduBeginTrace),
              "ns3::WigigPhy::PsduTxBeginCallback")
          .AddTraceSource("PhyTxEnd",
                          "Trace source indicating a packet "
                          "has been completely transmitted over the channel.",
                          MakeTraceSourceAccessor(&WigigPhy::m_phyTxEndTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource("PhyTxDrop",
                          "Trace source indicating a packet "
                          "has been dropped by the device during transmission",
                          MakeTraceSourceAccessor(&WigigPhy::m_phyTxDropTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource("PhyRxBegin",
                          "Trace source indicating a packet "
                          "has begun being received from the channel medium "
                          "by the device",
                          MakeTraceSourceAccessor(&WigigPhy::m_phyRxBeginTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "PhyRxPayloadBegin",
              "Trace source indicating the reception of the "
              "payload of a PPDU has begun",
              MakeTraceSourceAccessor(&WigigPhy::m_phyRxPayloadBeginTrace),
              "ns3::WigigPhy::PhyRxPayloadBeginTracedCallback")
          .AddTraceSource(
              "PhyRxEnd",
              "Trace source indicating a packet "
              "has been completely received from the channel medium "
              "by the device",
              MakeTraceSourceAccessor(&WigigPhy::m_phyRxEndTrace),
              "ns3::Packet::TracedCallback")
          .AddTraceSource("PhyRxDrop",
                          "Trace source indicating a packet "
                          "has been dropped by the device during reception",
                          MakeTraceSourceAccessor(&WigigPhy::m_phyRxDropTrace),
                          "ns3::Packet::TracedCallback")
          .AddTraceSource(
              "MonitorSnifferRx",
              "Trace source simulating a wifi device in monitor mode "
              "sniffing all received frames",
              MakeTraceSourceAccessor(&WigigPhy::m_phyMonitorSniffRxTrace),
              "ns3::WigigPhy::MonitorSnifferRxTracedCallback")
          .AddTraceSource(
              "MonitorSnifferTx",
              "Trace source simulating the capability of a wifi device "
              "in monitor mode to sniff all frames being transmitted",
              MakeTraceSourceAccessor(&WigigPhy::m_phyMonitorSniffTxTrace),
              "ns3::WigigPhy::MonitorSnifferTxTracedCallback");
  return tid;
}

WigigPhy::WigigPhy()
    : m_txMpduReferenceNumber(0xffffffff), m_rxMpduReferenceNumber(0xffffffff),
      m_endRxEvent(), m_endPhyRxEvent(), m_endPreambleDetectionEvent(),
      m_endTxEvent(), m_rxDuration(Seconds(0)), m_currentEvent(nullptr),
      m_wifiRadioEnergyModel(nullptr), m_lastTxDuration(NanoSeconds(0.0)),
      m_isConstructed(false), m_standard(WIFI_STANDARD_UNSPECIFIED),
      m_channelCenterFrequency(0), m_initialFrequency(0),
      m_frequencyChannelNumberInitialized(false), m_channelWidth(0),
      m_txSpatialStreams(0), m_rxSpatialStreams(0), m_channelNumber(0),
      m_initialChannelNumber(0), m_device(nullptr), m_mobility(nullptr),
      m_timeLastPreambleDetected(Seconds(0)), m_channel(nullptr),
      m_codebook(nullptr), m_psduSuccess(false), m_currentSender(),
      m_isAp(false) {
  NS_LOG_FUNCTION(this);
  m_random = CreateObject<UniformRandomVariable>();
  m_state = CreateObject<WigigPhyStateHelper>();
  m_interference.SetWifiPhy(this);
}

WigigPhy::~WigigPhy() { NS_LOG_FUNCTION(this); }

void WigigPhy::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_channel = nullptr;
  m_endTxEvent.Cancel();
  m_endRxEvent.Cancel();
  m_endPhyRxEvent.Cancel();
  m_endPreambleDetectionEvent.Cancel();
  m_device = nullptr;
  m_mobility = nullptr;
  m_state = nullptr;
  m_wifiRadioEnergyModel = nullptr;
  m_postReceptionErrorModel = nullptr;
  m_deviceRateSet.clear();
}

void WigigPhy::DoInitialize() {
  NS_LOG_FUNCTION(this);
  /* PHY Layer Information */
  if (!m_supportOfdm) {
    m_maxOfdmTxMcs = 0;
    m_maxOfdmRxMcs = 0;
  }
  m_isConstructed = true;
  if (m_frequencyChannelNumberInitialized) {
    NS_LOG_DEBUG("Frequency already initialized");
    return;
  }
  InitializeFrequencyChannelNumber();
}

Ptr<WigigPhyStateHelper> WigigPhy::GetState() const { return m_state; }

void WigigPhy::SetReceiveOkCallback(
    WigigPhyStateHelper::RxOkCallback callback) {
  m_state->SetReceiveOkCallback(callback);
}

void WigigPhy::SetReceiveErrorCallback(
    WigigPhyStateHelper::RxErrorCallback callback) {
  m_state->SetReceiveErrorCallback(callback);
}

void WigigPhy::RegisterListener(WifiPhyListener *listener) {
  m_state->RegisterListener(listener);
}

void WigigPhy::UnregisterListener(WifiPhyListener *listener) {
  m_state->UnregisterListener(listener);
}

void WigigPhy::InitializeFrequencyChannelNumber() {
  NS_LOG_FUNCTION(this);

  NS_ASSERT_MSG(m_frequencyChannelNumberInitialized == false,
                "Initialization called twice");

  // If frequency has been set to a non-zero value during attribute
  // construction phase, the frequency and channel width will drive the
  // initial configuration.  If frequency has not been set, but both
  // standard and channel number have been set, that pair will instead
  // drive the configuration, and frequency and channel number will be
  // aligned
  if (m_initialFrequency != 0) {
    SetFrequency(m_initialFrequency);
  } else if (m_initialChannelNumber != 0 &&
             GetStandard() != WIFI_STANDARD_UNSPECIFIED) {
    SetChannelNumber(m_initialChannelNumber);
  } else if (m_initialChannelNumber != 0 &&
             GetStandard() == WIFI_STANDARD_UNSPECIFIED) {
    NS_FATAL_ERROR(
        "Error, ChannelNumber "
        << +GetChannelNumber()
        << " was set by user, but neither a standard nor a frequency");
  }
  m_frequencyChannelNumberInitialized = true;
}

void WigigPhy::SetEdThreshold(double threshold) { SetRxSensitivity(threshold); }

void WigigPhy::SetRxSensitivity(double threshold) {
  NS_LOG_FUNCTION(this << threshold);
  m_rxSensitivityW = DbmToW(threshold);
}

double WigigPhy::GetRxSensitivity() const { return WToDbm(m_rxSensitivityW); }

void WigigPhy::SetCcaEdThreshold(double threshold) {
  NS_LOG_FUNCTION(this << threshold);
  m_ccaEdThresholdW = DbmToW(threshold);
}

double WigigPhy::GetCcaEdThreshold() const { return WToDbm(m_ccaEdThresholdW); }

void WigigPhy::SetRxNoiseFigure(double noiseFigureDb) {
  NS_LOG_FUNCTION(this << noiseFigureDb);
  m_interference.SetNoiseFigure(DbToRatio(noiseFigureDb));
  m_interference.SetNumberOfReceiveAntennas(GetNumberOfAntennas());
}

double WigigPhy::GetRxNoiseFigure() const {
  return RatioToDb(m_interference.GetNoiseFigure());
}

void WigigPhy::SetTxPowerStart(double start) {
  NS_LOG_FUNCTION(this << start);
  m_txPowerBaseDbm = start;
}

double WigigPhy::GetTxPowerStart() const { return m_txPowerBaseDbm; }

void WigigPhy::SetTxPowerEnd(double end) {
  NS_LOG_FUNCTION(this << end);
  m_txPowerEndDbm = end;
}

double WigigPhy::GetTxPowerEnd() const { return m_txPowerEndDbm; }

void WigigPhy::SetNTxPower(uint8_t n) {
  NS_LOG_FUNCTION(this << +n);
  m_nTxPower = n;
}

uint8_t WigigPhy::GetNTxPower() const { return m_nTxPower; }

void WigigPhy::SetTxGain(double gain) {
  NS_LOG_FUNCTION(this << gain);
  m_txGainDb = gain;
}

double WigigPhy::GetTxGain() const { return m_txGainDb; }

void WigigPhy::SetRxGain(double gain) {
  NS_LOG_FUNCTION(this << gain);
  m_rxGainDb = gain;
}

double WigigPhy::GetRxGain() const { return m_rxGainDb; }

void WigigPhy::SetDevice(const Ptr<NetDevice> device) { m_device = device; }

Ptr<NetDevice> WigigPhy::GetDevice() const { return m_device; }

void WigigPhy::SetMobility(const Ptr<MobilityModel> mobility) {
  m_mobility = mobility;
}

Ptr<MobilityModel> WigigPhy::GetMobility() const {
  if (m_mobility) {
    return m_mobility;
  } else {
    return m_device->GetNode()->GetObject<MobilityModel>();
  }
}

void WigigPhy::SetErrorRateModel(const Ptr<ErrorRateModel> rate) {
  m_interference.SetErrorRateModel(rate);
  m_interference.SetNumberOfReceiveAntennas(GetNumberOfAntennas());
}

void WigigPhy::SetPostReceptionErrorModel(const Ptr<ErrorModel> em) {
  NS_LOG_FUNCTION(this << em);
  m_postReceptionErrorModel = em;
}

void WigigPhy::SetFrameCaptureModel(const Ptr<WigigFrameCaptureModel> model) {
  m_frameCaptureModel = model;
  m_frameCaptureModel->SetPhy(this);
}

void WigigPhy::SetPreambleDetectionModel(
    const Ptr<PreambleDetectionModel> model) {
  m_preambleDetectionModel = model;
}

void WigigPhy::SetWifiRadioEnergyModel(
    const Ptr<WifiRadioEnergyModel> wifiRadioEnergyModel) {
  m_wifiRadioEnergyModel = wifiRadioEnergyModel;
}

Ptr<Channel> WigigPhy::GetChannel() const { return m_channel; }

void WigigPhy::SetChannel(const Ptr<WigigChannel> channel) {
  m_channel = channel;
  m_channel->Add(this);
}

void WigigPhy::SetCodebook(Ptr<Codebook> codebook) { m_codebook = codebook; }

Ptr<Codebook> WigigPhy::GetCodebook() const { return m_codebook; }

void WigigPhy::SetSupportOfdmPhy(bool value) {
  NS_LOG_FUNCTION(this << value);
  m_supportOfdm = value;
}

bool WigigPhy::GetSupportOfdmPhy() const { return m_supportOfdm; }

void WigigPhy::SetSupportLpScPhy(bool value) {
  NS_LOG_FUNCTION(this << value);
  m_supportLpSc = value;
}

bool WigigPhy::GetSupportLpScPhy() const { return m_supportLpSc; }

uint8_t WigigPhy::GetMaxScTxMcs() const { return m_maxScTxMcs; }

uint8_t WigigPhy::GetMaxScRxMcs() const { return m_maxScRxMcs; }

uint8_t WigigPhy::GetMaxOfdmTxMcs() const { return m_maxOfdmTxMcs; }

uint8_t WigigPhy::GetMaxOfdmRxMcs() const { return m_maxOfdmRxMcs; }

void WigigPhy::EndAllocationPeriod() {
  NS_LOG_FUNCTION(this);
  switch (m_state->GetState()) {
  case WifiPhyState::RX:
    AbortCurrentReception(DMG_ALLOCATION_ENDED);
    MaybeCcaBusyDuration();
  case WifiPhyState::TX:
  case WifiPhyState::SWITCHING:
  case WifiPhyState::CCA_BUSY:
  case WifiPhyState::IDLE:
  case WifiPhyState::SLEEP:
    break;
  default:
    NS_ASSERT(false);
    break;
  }
}

void WigigPhy::RegisterReportSnrCallback(ReportSnrCallback callback) {
  NS_LOG_FUNCTION(this);
  m_reportSnrCallback = callback;
}

double
WigigPhy::GetTxPowerForTransmission(const WigigTxVector &txVector) const {
  return GetPowerDbm(txVector.GetTxPowerLevel());
}

void WigigPhy::Send(Ptr<const WigigPsdu> psdu, const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << *psdu << txVector);
  /* Transmission can happen if:
   *  - we are syncing on a packet. It is the responsibility of the
   *    MAC layer to avoid doing this but the PHY does nothing to
   *    prevent it.
   *  - we are idle
   */
  NS_ASSERT(!m_state->IsStateTx() && !m_state->IsStateSwitching());
  NS_ASSERT(m_endTxEvent.IsExpired());

  if (txVector.GetNss() > GetMaxSupportedTxSpatialStreams()) {
    NS_FATAL_ERROR("Unsupported number of spatial streams!");
  }

  if (m_state->IsStateSleep()) {
    NS_LOG_DEBUG("Dropping packet because in sleep mode");
    NotifyTxDrop(psdu);
    return;
  }

  Time frameDuration =
      CalculateTxDuration(psdu->GetSize(), txVector, GetFrequency());
  Time txDuration = frameDuration;
  NS_ASSERT(txDuration.IsStrictlyPositive());

  /* Check if there is a DMG TRN field appended to this PSDU */
  if ((GetStandard() == WIFI_STANDARD_80211ad) &&
      (txVector.GetTrainingFieldLength() > 0)) {
    NS_LOG_DEBUG("Append " << +txVector.GetTrainingFieldLength()
                           << "DMG TRN Subfields");
    txDuration += txVector.GetTrainingFieldLength() *
                      (AGC_SF_DURATION + TRN_SUBFIELD_DURATION) +
                  (txVector.GetTrainingFieldLength() / 4) * TRN_CE_DURATION;
    NS_LOG_DEBUG("TxDuration=" << txDuration);
  }

  if ((m_currentEvent) && (m_currentEvent->GetEndTime() >
                           (Simulator::Now() + m_state->GetDelayUntilIdle()))) {
    // that packet will be noise _after_ the transmission.
    MaybeCcaBusyDuration();
  }

  if (m_currentEvent) {
    AbortCurrentReception(RECEPTION_ABORTED_BY_TX);
  }

  if (m_state->GetState() == WifiPhyState::OFF) {
    NS_LOG_DEBUG("Transmission canceled because device is OFF");
    return;
  }

  double txPowerW = DbmToW(GetTxPowerForTransmission(txVector) + GetTxGain());
  NotifyTxBegin(psdu, txPowerW);
  m_phyTxPsduBeginTrace(psdu, txVector, txPowerW);
  NotifyMonitorSniffTx(psdu, GetFrequency(), txVector);
  m_state->SwitchToTx(txDuration, psdu->GetPacket(),
                      GetPowerDbm(txVector.GetTxPowerLevel()), txVector);
  Ptr<WigigPpdu> ppdu =
      Create<WigigPpdu>(psdu, txVector, txDuration, GetFrequency());

  if (m_wifiRadioEnergyModel && m_wifiRadioEnergyModel->GetMaximumTimeInState(
                                    WifiPhyState::TX) < txDuration) {
    ppdu->SetTruncatedTx();
  }

  m_endTxEvent =
      Simulator::Schedule(txDuration, &WigigPhy::NotifyTxEnd, this, psdu);
  StartTx(ppdu);

  /* Send TRN Units if beam refinement or tracking is requested */
  if (txVector.GetTrainingFieldLength() > 0) {
    /* Prepare transmission of the AGC Subfields */
    Simulator::Schedule(frameDuration, &WigigPhy::StartAgcSubfieldsTx, this,
                        txVector);
  }

  /* Record duration of the current transmission */

  m_lastTxDuration = txDuration;
}

void WigigPhy::StartTx(Ptr<WigigPpdu> ppdu) {
  NS_LOG_FUNCTION(this << ppdu);
  WigigTxVector txVector = ppdu->GetTxVector();
  NS_LOG_DEBUG("Start transmission: signal power before antenna gain="
               << GetPowerDbm(txVector.GetTxPowerLevel()) << "dBm");
  m_channel->Send(this, ppdu,
                  GetTxPowerForTransmission(txVector) + GetTxGain());
}

void WigigPhy::StartAgcSubfieldsTx(const WigigTxVector &txVector) {
  NS_LOG_DEBUG(
      "Start AGC Subfields transmission: signal power before antenna gain="
      << GetPowerDbm(txVector.GetTxPowerLevel()) << " dBm");
  if (txVector.GetPacketType() == TRN_T) {
    /* We are the initiator of the TRN-TX */
    m_codebook->UseCustomAwv(RefineTransmitSector);
  }
  SendAgcSubfield(txVector, txVector.GetTrainingFieldLength());
}

void WigigPhy::SendAgcSubfield(const WigigTxVector &txVector,
                               uint8_t subfieldsRemaining) {
  NS_LOG_FUNCTION(this << txVector.GetMode() << +subfieldsRemaining);

  subfieldsRemaining--;
  StartAgcSubfieldTx(txVector);

  if (subfieldsRemaining > 0) {
    Simulator::Schedule(AGC_SF_DURATION, &WigigPhy::SendAgcSubfield, this,
                        txVector, subfieldsRemaining);
  } else {
    WigigTxVector txvector = txVector;
    txvector.remainingTrnUnits = txVector.GetTrainingFieldLength() / 4;
    Simulator::Schedule(AGC_SF_DURATION, &WigigPhy::StartTrnUnitTx, this,
                        txvector);
  }

  if (txVector.GetPacketType() == TRN_T) {
    /* We are the initiator of the TRN-TX */
    m_codebook->GetNextAwv();
  }
}

void WigigPhy::StartAgcSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << txVector.GetMode());
  m_channel->SendAgcSubfield(
      this, GetPowerDbm(txVector.GetTxPowerLevel()) + GetTxGain(), txVector);
}

void WigigPhy::StartTrnUnitTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << txVector << +txVector.remainingTrnUnits);
  WigigTxVector txvector = txVector;
  txvector.remainingTrnUnits--;
  SendCeSubfield(txvector);
}

void WigigPhy::SendCeSubfield(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this);
  if (txVector.GetPacketType() == TRN_T) {
    /* We are the initiator of the TRN-TX */
    /* The CE Subfield of the TRN-Unit is transmitted using the sector used for
     * transmitting the CEF of the preamble */
    m_codebook->UseLastTxSector();
  }
  StartCeSubfieldTx(txVector);
  Simulator::Schedule(TRN_CE_DURATION, &WigigPhy::StartTrnSubfieldsTx, this,
                      txVector);
}

void WigigPhy::StartCeSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << txVector.GetMode());
  m_channel->SendTrnCeSubfield(
      this, GetPowerDbm(txVector.GetTxPowerLevel()) + GetTxGain(), txVector);
}

void WigigPhy::StartTrnSubfieldsTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << +txVector.remainingTrnUnits);
  m_codebook->UseCustomAwv(RefineTransmitSector);
  WigigTxVector txvector = txVector;
  txvector.remainingTrnSubfields = TRN_UNIT_SIZE;
  SendTrnSubfield(txvector);
}

void WigigPhy::SendTrnSubfield(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << txVector.GetMode() << +txVector.remainingTrnUnits
                       << +txVector.remainingTrnSubfields);

  WigigTxVector txvector = txVector;
  txvector.remainingTrnSubfields--;
  StartTrnSubfieldTx(txvector);

  if (txvector.remainingTrnSubfields != 0) {
    Simulator::Schedule(TRN_SUBFIELD_DURATION, &WigigPhy::SendTrnSubfield, this,
                        txvector);
  } else if ((txvector.remainingTrnUnits > 0) &&
             (txvector.remainingTrnSubfields == 0)) {
    Simulator::Schedule(TRN_SUBFIELD_DURATION, &WigigPhy::StartTrnUnitTx, this,
                        txvector);
  }

  if (txvector.GetPacketType() == TRN_T) {
    /* We are the initiator of the TRN-TX */
    m_codebook->GetNextAwv();
  }
}

void WigigPhy::StartTrnSubfieldTx(const WigigTxVector &txVector) {
  NS_LOG_FUNCTION(this << txVector.GetMode());
  m_channel->SendTrnSubfield(
      this, GetPowerDbm(txVector.GetTxPowerLevel()) + GetTxGain(), txVector);
}

void WigigPhy::StartReceiveAgcSubfield(const WigigTxVector &txVector,
                                       double rxPowerDbm) {
  NS_LOG_FUNCTION(this << txVector.GetMode() << rxPowerDbm << m_psduSuccess
                       << m_state->IsStateRx() << txVector.GetSender()
                       << m_currentSender);
  double rxPowerW = DbmToW(rxPowerDbm);
  if (m_psduSuccess && m_state->IsStateRx() &&
      txVector.GetSender() == m_currentSender) {
    /* Add Interference event for AGC Subfield */
    Ptr<WigigSignalEvent> event;
    event = m_interference.Add(txVector, AGC_SF_DURATION, rxPowerW);
  } else {
    NS_LOG_DEBUG(
        "Drop AGC Subfield because did not receive successfully the PHY frame");
  }
}

void WigigPhy::StartReceiveCeSubfield(const WigigTxVector &txVector,
                                      double rxPowerDbm) {
  NS_LOG_FUNCTION(this << txVector.GetMode() << rxPowerDbm);
  double rxPowerW = DbmToW(rxPowerDbm);
  if (m_psduSuccess && m_state->IsStateRx() &&
      txVector.GetSender() == m_currentSender) {
    /* Add Interference event for TRN-CE Subfield */
    Ptr<WigigSignalEvent> event;
    event = m_interference.Add(txVector, TRN_CE_DURATION, rxPowerW);
    if (txVector.GetPacketType() == TRN_R) {
      /* We are the initiator of the TRN-RX, we switch between different AWVs */
      m_codebook->UseCustomAwv(RefineReceiveSector);
      /* If at the first TRN Unit switch the AWV after ending the last AGC
       * subfield */
      if (txVector.remainingTrnUnits == txVector.GetTrainingFieldLength() / 4) {
        m_codebook->GetNextAwv();
      }
    }
  } else {
    NS_LOG_DEBUG("Drop TRN-CE Subfield because did not receive successfully "
                 "the PHY frame");
  }
}

void WigigPhy::StartReceiveTrnSubfield(const WigigTxVector &txVector,
                                       double rxPowerDbm) {
  NS_LOG_FUNCTION(this << txVector.GetMode() << rxPowerDbm
                       << +txVector.remainingTrnSubfields);
  double rxPowerW = DbmToW(rxPowerDbm);
  if (m_psduSuccess && m_state->IsStateRx() &&
      txVector.GetSender() == m_currentSender) {
    /* Add Interference event for TRN Subfield */
    Ptr<WigigSignalEvent> event;
    event = m_interference.Add(txVector, TRN_SUBFIELD_DURATION, rxPowerW);

    if (txVector.GetPacketType() == TRN_R) {
      /* Schedule an event for the complete reception of this TRN Subfield */
      Simulator::Schedule(TRN_SUBFIELD_DURATION - NanoSeconds(1),
                          &WigigPhy::EndReceiveTrnSubfield, this,
                          m_codebook->GetActiveRxSectorId(),
                          m_codebook->GetActiveAntennaId(), txVector, event);
    } else {
      /**
       * We are the responder of the beam refinement phase and the initiator
       * wants to refine its transmit pattern. The transmitter sends every TRN
       * Subfield using unique beam pattern i.e. the transmitter switches among
       * among the AWVs within the main sector. We record the SNR value of each
       * TRN-SF and feedback the best AWV ID to the initiator.
       */

      /* Schedule an event for the complete reception of this TRN Subfield */
      Simulator::Schedule(TRN_SUBFIELD_DURATION - NanoSeconds(1),
                          &WigigPhy::EndReceiveTrnSubfield, this,
                          m_codebook->GetActiveTxSectorId(),
                          m_codebook->GetActiveAntennaId(), txVector, event);
    }
  } else {
    NS_LOG_DEBUG(
        "Drop TRN Subfield because did not receive successfully the PHY frame");
    if ((txVector.remainingTrnUnits == 0) &&
        (txVector.remainingTrnSubfields == 0) && m_state->IsStateRx() &&
        (txVector.GetSender() == m_currentSender)) {
      Simulator::Schedule(TRN_SUBFIELD_DURATION, &WigigPhy::EndReceiveTrnField,
                          this);
    }
  }
}

void WigigPhy::EndReceiveTrnSubfield(SectorId sectorId, AntennaId antennaId,
                                     const WigigTxVector &txVector,
                                     Ptr<WigigSignalEvent> event) {
  NS_LOG_FUNCTION(this << +sectorId << +antennaId << txVector.GetMode()
                       << +txVector.remainingTrnUnits
                       << +txVector.remainingTrnSubfields
                       << event->GetRxPowerW());
  /* Calculate SNR and report it to the upper layer */
  double snr = m_interference.CalculatePlcpTrnSnr(event);
  m_reportSnrCallback(antennaId, sectorId, txVector.remainingTrnUnits,
                      txVector.remainingTrnSubfields, 0, snr,
                      (txVector.GetPacketType() != TRN_R));
  /* Switch to the next antenna configuration in case of receive training */
  if (txVector.GetPacketType() == TRN_R) {
    /* Change Rx Sector for the next TRN Subfield */
    m_codebook->GetNextAwv();
  }
  /* TRN-RX/TX subfields are used for simultaneous transmit and receive training
   * - the transmitter repeats each Unit several times so that the responder can
   * train it's receive sectors */
  else if (txVector.GetPacketType() == TRN_RT) {
    m_codebook->GetNextAwv();
  }

  /* Check if this is the last TRN subfield in the current transmission */
  if ((txVector.remainingTrnUnits == 0) &&
      (txVector.remainingTrnSubfields == 0)) {
    Simulator::Schedule(NanoSeconds(1), &WigigPhy::EndReceiveTrnField, this);
  }
}

void WigigPhy::EndReceiveTrnField() {
  NS_LOG_FUNCTION(this << GetState()->GetState());
  m_interference.NotifyRxEnd();
  m_currentSender = Mac48Address();
  m_currentEvent = nullptr;
  MaybeCcaBusyDuration();
  if (m_psduSuccess) {
    m_state->SwitchFromRxEndOk();
  } else {
    m_state->SwitchFromRxEndError();
  }
}

void WigigPhy::StartRxPreamble(Ptr<WigigSignalEvent> event, double rxPowerW) {
  NS_LOG_FUNCTION(this << *event << rxPowerW);

  NS_LOG_DEBUG("sync to signal (power=" << rxPowerW << "W)");
  m_interference.NotifyRxStart(); // We need to notify it now so that it starts
                                  // recording events
  if (!m_endPreambleDetectionEvent.IsRunning()) {
    m_currentSender = event->GetTxVector().GetSender();
    Time startOfPreambleDuration = GetPreambleDetectionDuration();
    Time remainingRxDuration = event->GetDuration() - startOfPreambleDuration;
    m_endPreambleDetectionEvent = Simulator::Schedule(
        startOfPreambleDuration, &WigigPhy::StartReceiveHeader, this, event);
  } else if ((m_frameCaptureModel) &&
             (rxPowerW > m_currentEvent->GetRxPowerW())) {
    m_currentSender = event->GetTxVector().GetSender();
    NS_LOG_DEBUG("Received a stronger signal during preamble detection: drop "
                 "current packet "
                 "and switch to new packet");
    NotifyRxDrop(m_currentEvent->GetPsdu(), PREAMBLE_DETECTION_PACKET_SWITCH);
    m_interference.NotifyRxEnd();
    m_endPreambleDetectionEvent.Cancel();
    m_interference.NotifyRxStart();
    Time startOfPreambleDuration = GetPreambleDetectionDuration();
    Time remainingRxDuration = event->GetDuration() - startOfPreambleDuration;
    m_endPreambleDetectionEvent = Simulator::Schedule(
        startOfPreambleDuration, &WigigPhy::StartReceiveHeader, this, event);
  } else {
    NS_LOG_DEBUG("Drop packet because RX is already decoding preamble");
    NotifyRxDrop(event->GetPsdu(), BUSY_DECODING_PREAMBLE);
    return;
  }
  m_currentEvent = event;
}

void WigigPhy::StartReceiveHeader(Ptr<WigigSignalEvent> event) {
  NS_LOG_FUNCTION(this << *event);
  NS_ASSERT(!IsStateRx());
  NS_ASSERT(m_endPhyRxEvent.IsExpired());
  NS_ASSERT(m_currentEvent);
  NS_ASSERT(event->GetStartTime() == m_currentEvent->GetStartTime());
  NS_ASSERT(event->GetEndTime() == m_currentEvent->GetEndTime());

  WigigInterferenceHelper::SnrPer snrPer =
      m_interference.CalculateDmgPhyHeaderSnrPer(
          event); //// WIGIG It should be only SNR calculation
  double snr = snrPer.snr;
  NS_LOG_DEBUG("snr(dB)=" << RatioToDb(snrPer.snr) << ", per=" << snrPer.per);

  if (!m_preambleDetectionModel ||
      (m_preambleDetectionModel->IsPreambleDetected(event->GetRxPowerW(), snr,
                                                    m_channelWidth))) {
    NotifyRxBegin(event->GetPsdu());

    m_timeLastPreambleDetected = Simulator::Now();
    WigigTxVector txVector = event->GetTxVector();

    // Schedule end of DMG PHY header
    Time remainingPreambleAndHeaderDuration = GetPhyPreambleDuration(txVector) +
                                              GetPhyHeaderDuration(txVector) -
                                              GetPreambleDetectionDuration();
    m_state->SwitchMaybeToCcaBusy(remainingPreambleAndHeaderDuration);
    m_endPhyRxEvent =
        Simulator::Schedule(remainingPreambleAndHeaderDuration,
                            &WigigPhy::ContinueReceiveHeader, this, event);
  } else {
    NS_LOG_DEBUG("Drop packet because PHY preamble detection failed");
    NotifyRxDrop(event->GetPsdu(), PREAMBLE_DETECT_FAILURE);
    m_interference.NotifyRxEnd();
    m_currentEvent = nullptr;
    m_psduSuccess = false;

    // Like CCA-SD, CCA-ED is governed by the 4μs CCA window to flag CCA-BUSY
    // for any received signal greater than the CCA-ED threshold.
    if (event->GetEndTime() >
        (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      MaybeCcaBusyDuration();
    }
  }
}

void WigigPhy::ContinueReceiveHeader(Ptr<WigigSignalEvent> event) {
  NS_LOG_FUNCTION(this << *event);
  NS_ASSERT(m_endPhyRxEvent.IsExpired());

  WigigInterferenceHelper::SnrPer snrPer;
  snrPer = m_interference.CalculateDmgPhyHeaderSnrPer(event);

  if (m_random->GetValue() > snrPer.per) // DMG PHY header reception succeeded
  {
    NS_LOG_DEBUG("Received DMG PHY header");
    WigigTxVector txVector = event->GetTxVector();
    Time remainingRxDuration = event->GetEndTime() - Simulator::Now();
    m_state->SwitchMaybeToCcaBusy(remainingRxDuration);
    Time remainingPreambleHeaderDuration =
        CalculatePhyPreambleAndHeaderDuration(txVector) -
        GetPhyPreambleDuration(txVector) - GetPhyHeaderDuration(txVector);
    m_endPhyRxEvent =
        Simulator::Schedule(remainingPreambleHeaderDuration,
                            &WigigPhy::StartReceivePayload, this, event);
  } else // DMG PHY header reception failed
  {
    NS_LOG_DEBUG("Abort reception because DMG PHY header reception failed");
    m_psduSuccess = false;
    AbortCurrentReception(DMG_HEADER_FAILURE);
    if (event->GetEndTime() >
        (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      MaybeCcaBusyDuration();
    }
  }
}

Time WigigPhy::GetPreambleDetectionDuration() { return NanoSeconds(290); }

WifiMode WigigPhy::GetPhyHeaderMode(const WigigTxVector &txVector) {
  WifiPreamble preamble = txVector.GetPreambleType();
  switch (preamble) {
  case WIFI_PREAMBLE_DMG_CTRL:
    return WigigPhy::GetDmgMcs0();
  case WIFI_PREAMBLE_DMG_SC:
    return WigigPhy::GetDmgMcs1();
  case WIFI_PREAMBLE_DMG_OFDM:
    return WigigPhy::GetDmgMcs13();
  default:
    NS_FATAL_ERROR("unsupported preamble type");
    return WifiMode();
  }
}

Time WigigPhy::GetPhyHeaderDuration(const WigigTxVector &txVector) {
  switch (txVector.GetPreambleType()) {
  case WIFI_PREAMBLE_DMG_CTRL:
    /* From Annex L (L.5.2.5) */
    return NanoSeconds(4654);
  case WIFI_PREAMBLE_DMG_SC:
    /* From Table 21-4 in 802.11ad spec 21.3.4 */
    return NanoSeconds(582);
  case WIFI_PREAMBLE_DMG_OFDM:
    /* From Table 21-4 in 802.11ad spec 21.3.4 */
    return NanoSeconds(242);
  default:
    NS_FATAL_ERROR("unsupported preamble type");
    return MicroSeconds(0);
  }
}

Time WigigPhy::GetPhyPreambleDuration(const WigigTxVector &txVector) {
  switch (txVector.GetPreambleType()) {
  case WIFI_PREAMBLE_DMG_CTRL:
    // CTRL Preamble = (6400 + 1152) Samples * Tc (Chip Time for SC), Tc = Tccp
    // = 0.57ns. CTRL Preamble = 4.291 micro seconds.
    return NanoSeconds(4291);

  case WIFI_PREAMBLE_DMG_SC:
    // SC Preamble = 3328 Samples (STF: 2176 + CEF: 1152) * Tc (Chip Time for
    // SC), Tc = 0.57ns. SC Preamble = 1.89 micro seconds.
    return NanoSeconds(1891);

  case WIFI_PREAMBLE_DMG_OFDM:
    // OFDM Preamble = 4992 Samples (STF: 2176 + CEF: 1152) * Ts (Chip Time for
    // OFDM), Tc = 0.38ns. OFDM Preamble = 1.89 micro seconds.
    return NanoSeconds(1891);
  default:
    NS_FATAL_ERROR("unsupported preamble type");
    return MicroSeconds(0);
  }
}

Time WigigPhy::GetPayloadDuration(uint32_t size, const WigigTxVector &txVector,
                                  uint16_t frequency, MpduType mpdutype) {
  uint32_t totalAmpduSize;
  double totalAmpduNumSymbols;
  return GetPayloadDuration(size, txVector, frequency, mpdutype, false,
                            totalAmpduSize, totalAmpduNumSymbols);
}

Time WigigPhy::GetPayloadDuration(uint32_t size, const WigigTxVector &txVector,
                                  uint16_t frequency, MpduType mpdutype,
                                  bool incFlag, uint32_t &totalAmpduSize,
                                  double &totalAmpduNumSymbols) {
  WifiMode payloadMode = txVector.GetMode();
  NS_LOG_FUNCTION(size << txVector);

  if (payloadMode.GetModulationClass() == WIFI_MOD_CLASS_DMG_CTRL) {
    uint32_t ncw;    /* Number of LDPC codewords. */
    uint32_t ldpcw;  /* Number of bits in the second and any subsequent codeword
                        except the last. */
    uint32_t ldplcw; /* Number of bits in the last codeword. */
    uint32_t
        dEncodedSymbols; /* Number of differentially encoded payload symbols. */
    uint32_t chips;      /* Number of chips (After spreading using Ga32 Golay
                            Sequence). */
    uint32_t nbits = (size - 8) * 8; /* Number of bits in the payload part. */

    ncw = 1 + static_cast<uint32_t>(
                  ceil((static_cast<double>(size) - 6) * 8 / 168));
    ldpcw = static_cast<uint32_t>(
        ceil((static_cast<double>(size) - 6) * 8 / (ncw - 1)));
    ldplcw = (size - 6) * 8 - (ncw - 2) * ldpcw;
    dEncodedSymbols =
        (672 - (504 - ldpcw)) * (ncw - 2) + (672 - (504 - ldplcw));
    chips = dEncodedSymbols * 32;
    /* Make sure the result is in nanoseconds. */
    double ret = static_cast<double>(chips) / 1.76;
    NS_LOG_DEBUG("bits " << nbits << " Diff encoded Symbols " << dEncodedSymbols
                         << " rate " << payloadMode.GetDataRate(txVector)
                         << " Payload Time " << ret << " ns");

    return NanoSeconds(ceil(ret));
  } else if (payloadMode.GetModulationClass() == WIFI_MOD_CLASS_DMG_LP_SC) {
    return NanoSeconds(0);
  } else if (payloadMode.GetModulationClass() == WIFI_MOD_CLASS_DMG_SC) {
    /* 21.3.4 Timing Related Parameters, Table 21-4 TData = (Nblks * 512 + 64) *
     * Tc. */
    /* 21.6.3.2.3.3 (4), Compute Nblks = The number of symbol blocks. */

    uint32_t ncbpb; // Number of coded bits per symbol block. Check Table 21-20
                    // for different constellations.
    if (payloadMode.GetConstellationSize() == 2) {
      ncbpb = 448;
    } else if (payloadMode.GetConstellationSize() == 4) {
      ncbpb = 2 * 448;
    } else if (payloadMode.GetConstellationSize() == 16) {
      ncbpb = 4 * 448;
    } else if (payloadMode.GetConstellationSize() == 64) {
      ncbpb = 6 * 448;
    } else {
      NS_FATAL_ERROR("unsupported constellation size");
    }

    uint32_t nbits = (size * 8); /* Number of bits in the payload part. */
    uint32_t ncbits;             /* Number of coded bits in the payload part. */

    if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_1_4) {
      ncbits = nbits * 4;
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_1_2) {
      ncbits = nbits * 2;
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_13_16) {
      ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(nbits) * 16.0 / 13));
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_3_4) {
      ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(nbits) * 4.0 / 3));
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_5_8) {
      ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(nbits) * 8.0 / 5));
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_7_8) {
      ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(nbits) * 8.0 / 7));
    } else {
      NS_FATAL_ERROR("unsupported code rate");
    }

    uint16_t lcw; /* The LDPC codeword length. */
    if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_7_8) {
      lcw = 624;
    } else {
      lcw = 672;
    }

    uint32_t ncw = static_cast<uint32_t>(ceil(
        static_cast<double>(ncbits) / lcw)); /* The number of LDPC codewords. */
    uint32_t nblks = static_cast<uint32_t>(
        ceil(static_cast<double>(ncw) * static_cast<double>(lcw) /
             ncbpb)); /* The number of symbol blocks. */

    /* Make sure the result is in nanoseconds. */
    uint32_t tData = lrint(ceil((static_cast<double>(nblks) * 512 + 64) /
                                1.76)); /* The duration of the data part */
    NS_LOG_DEBUG("bits " << nbits << " cbits " << ncbits << " ncw " << ncw
                         << " nblks " << nblks << " rate "
                         << payloadMode.GetDataRate(txVector)
                         << " Payload Time " << tData << " ns");

    if (txVector.GetTrainingFieldLength() != 0 && tData < OFDMSCMin) {
      tData = OFDMSCMin;
    }
    return NanoSeconds(tData);
  } else if (payloadMode.GetModulationClass() == WIFI_MOD_CLASS_DMG_OFDM) {
    /* 21.3.4 Timing Related Parameters, Table 21-4 TData = Nsym * Tsys(OFDM) */
    /* 21.5.3.2.3.3 (5), Compute Nsym = Number of OFDM Symbols */

    uint32_t Ncbps; // Ncbps = Number of coded bits per symbol. Check Table
                    // 21-20 for different constellations.
    if (payloadMode.GetConstellationSize() == 2) {
      Ncbps = 336;
    } else if (payloadMode.GetConstellationSize() == 4) {
      Ncbps = 2 * 336;
    } else if (payloadMode.GetConstellationSize() == 16) {
      Ncbps = 4 * 336;
    } else if (payloadMode.GetConstellationSize() == 64) {
      Ncbps = 6 * 336;
    } else {
      NS_FATAL_ERROR("unsupported constellation size");
    }

    uint32_t Nbits =
        (size * 8);  /* Nbits = Number of bits in the payload part. */
    uint32_t Ncbits; /* Ncbits = Number of coded bits in the payload part. */

    if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_1_4) {
      Ncbits = Nbits * 4;
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_1_2) {
      Ncbits = Nbits * 2;
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_13_16) {
      Ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(Nbits) * 16.0 / 13));
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_3_4) {
      Ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(Nbits) * 4.0 / 3));
    } else if (payloadMode.GetCodeRate() == WIFI_CODE_RATE_5_8) {
      Ncbits =
          static_cast<uint32_t>(ceil(static_cast<double>(Nbits) * 8.0 / 5));
    } else {
      NS_FATAL_ERROR("unsupported code rate");
    }

    uint32_t Ncw = static_cast<uint32_t>(
        ceil(static_cast<double>(Ncbits) /
             672.0)); /* Ncw = The number of LDPC codewords.  */
    uint32_t Nsym =
        static_cast<uint32_t>(ceil(static_cast<double>(Ncw * 672.0) /
                                   Ncbps)); /* Nsym = Number of OFDM Symbols. */

    /* Make sure the result is in nanoseconds */
    uint32_t tData;     /* The duration of the data part */
    tData = Nsym * 242; /* Tsys(OFDM) = 242ns */
    NS_LOG_DEBUG("bits " << Nbits << " cbits " << Ncbits << " rate "
                         << payloadMode.GetDataRate(txVector)
                         << " Payload Time " << tData << " ns");

    if (txVector.GetTrainingFieldLength() != 0) {
      if (tData < OFDMBrpMin) {
        tData = OFDMBrpMin;
      }
    }
    return NanoSeconds(tData);
  }

  double stbc = 1;
  double Nes = 1;
  Time symbolDuration = Seconds(0);
  switch (payloadMode.GetModulationClass()) {
  case WIFI_MOD_CLASS_OFDM: {
    //(Section 18.3.2.4 "Timing related parameters" Table 18-5 "Timing-related
    //parameters"; IEEE
    // Std 802.11-2012 corresponds to T_{SYM} in the table)
    switch (txVector.GetChannelWidth()) {
    case 20:
    default:
      symbolDuration = MicroSeconds(4);
      break;
    case 10:
      symbolDuration = MicroSeconds(8);
      break;
    case 5:
      symbolDuration = MicroSeconds(16);
      break;
    }
    break;
  } break;
  default:
    break;
  }

  double numDataBitsPerSymbol =
      payloadMode.GetDataRate(txVector) * symbolDuration.GetNanoSeconds() / 1e9;

  double numSymbols = 0;
  if (mpdutype == FIRST_MPDU_IN_AGGREGATE) {
    // First packet in an A-MPDU
    numSymbols =
        (stbc * (16 + size * 8.0 + 6 * Nes) / (stbc * numDataBitsPerSymbol));
    if (incFlag == 1) {
      totalAmpduSize += size;
      totalAmpduNumSymbols += numSymbols;
    }
  } else if (mpdutype == MIDDLE_MPDU_IN_AGGREGATE) {
    // consecutive packets in an A-MPDU
    numSymbols = (stbc * size * 8.0) / (stbc * numDataBitsPerSymbol);
    if (incFlag == 1) {
      totalAmpduSize += size;
      totalAmpduNumSymbols += numSymbols;
    }
  } else if (mpdutype == LAST_MPDU_IN_AGGREGATE) {
    // last packet in an A-MPDU
    uint32_t totalSize = totalAmpduSize + size;
    numSymbols = lrint(stbc * ceil((16 + totalSize * 8.0 + 6 * Nes) /
                                   (stbc * numDataBitsPerSymbol)));
    NS_ASSERT(totalAmpduNumSymbols <= numSymbols);
    numSymbols -= totalAmpduNumSymbols;
    if (incFlag == 1) {
      totalAmpduSize = 0;
      totalAmpduNumSymbols = 0;
    }
  } else if (mpdutype == NORMAL_MPDU || mpdutype == SINGLE_MPDU) {
    // Not an A-MPDU or single MPDU (i.e. the current payload contains both
    // service and padding) The number of OFDM symbols in the data field when
    // BCC encoding is used is given in equation 19-32 of the IEEE 802.11-2016
    // standard.
    numSymbols = lrint(stbc * ceil((16 + size * 8.0 + 6.0 * Nes) /
                                   (stbc * numDataBitsPerSymbol)));
  } else {
    NS_FATAL_ERROR("Unknown MPDU type");
  }

  switch (payloadMode.GetModulationClass()) {
  case WIFI_MOD_CLASS_OFDM: {
    return FemtoSeconds(
        static_cast<uint64_t>(numSymbols * symbolDuration.GetFemtoSeconds()));
  }
  default:
    NS_FATAL_ERROR("unsupported modulation class");
    return MicroSeconds(0);
  }
}

Time WigigPhy::GetLastRxDuration() const { return m_rxDuration; }

Time WigigPhy::CalculatePhyPreambleAndHeaderDuration(
    const WigigTxVector &txVector) {
  return GetPhyPreambleDuration(txVector) + GetPhyHeaderDuration(txVector);
}

Time WigigPhy::CalculateTxDuration(uint32_t size, const WigigTxVector &txVector,
                                   uint16_t frequency) {
  Time duration = CalculatePhyPreambleAndHeaderDuration(txVector) +
                  GetPayloadDuration(size, txVector, frequency);
  return duration;
}

Time WigigPhy::GetTrnFieldDuration(const WigigTxVector &txVector) {
  Time trnFieldDuration = Seconds(0);
  if (txVector.GetTrainingFieldLength() > 0) {
    trnFieldDuration =
        txVector.GetTrainingFieldLength() *
            (AGC_SF_DURATION + TRN_SUBFIELD_DURATION) +
        (txVector.GetTrainingFieldLength() / 4) * TRN_CE_DURATION;
  }
  NS_LOG_DEBUG("TRN Field Duration=" << trnFieldDuration);
  return trnFieldDuration;
}

void WigigPhy::NotifyTxBegin(Ptr<const WigigPsdu> psdu, double txPowerW) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyTxBeginTrace(mpdu->GetProtocolDataUnit(), txPowerW);
  }
}

void WigigPhy::NotifyTxEnd(Ptr<const WigigPsdu> psdu) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyTxEndTrace(mpdu->GetProtocolDataUnit());
  }
}

void WigigPhy::NotifyTxDrop(Ptr<const WigigPsdu> psdu) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyTxDropTrace(mpdu->GetProtocolDataUnit());
  }
}

void WigigPhy::NotifyRxBegin(Ptr<const WigigPsdu> psdu) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyRxBeginTrace(mpdu->GetProtocolDataUnit());
  }
}

void WigigPhy::NotifyRxEnd(Ptr<const WigigPsdu> psdu) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyRxEndTrace(mpdu->GetProtocolDataUnit());
  }
}

void WigigPhy::NotifyRxDrop(Ptr<const WigigPsdu> psdu,
                            WifiPhyRxfailureReason reason) {
  for (auto &mpdu : *PeekPointer(psdu)) {
    m_phyRxDropTrace(mpdu->GetProtocolDataUnit(), reason);
  }
}

void WigigPhy::NotifyMonitorSniffRx(Ptr<const WigigPsdu> psdu,
                                    uint16_t channelFreqMhz,
                                    const WigigTxVector &txVector,
                                    SignalNoiseDbm signalNoise,
                                    std::vector<bool> statusPerMpdu) {
  MpduInfo aMpdu;
  if (psdu->IsAggregate()) {
    // Expand A-MPDU
    NS_ASSERT_MSG(
        txVector.IsAggregation(),
        "TxVector with aggregate flag expected here according to PSDU");
    aMpdu.mpduRefNumber = ++m_rxMpduReferenceNumber;
    size_t nMpdus = psdu->GetNMpdus();
    NS_ASSERT_MSG(statusPerMpdu.size() == nMpdus,
                  "Should have one reception status per MPDU");
    aMpdu.type = (psdu->IsSingle()) ? SINGLE_MPDU : FIRST_MPDU_IN_AGGREGATE;
    for (size_t i = 0; i < nMpdus;) {
      if (statusPerMpdu.at(
              i)) // packet received without error, hand over to sniffer
      {
        m_phyMonitorSniffRxTrace(psdu->GetAmpduSubframe(i), channelFreqMhz,
                                 txVector, aMpdu, signalNoise);
      }
      ++i;
      aMpdu.type = (i == (nMpdus - 1)) ? LAST_MPDU_IN_AGGREGATE
                                       : MIDDLE_MPDU_IN_AGGREGATE;
    }
  } else {
    aMpdu.type = NORMAL_MPDU;
    NS_ASSERT_MSG(statusPerMpdu.size() == 1,
                  "Should have one reception status for normal MPDU");
    m_phyMonitorSniffRxTrace(psdu->GetPacket(), channelFreqMhz, txVector, aMpdu,
                             signalNoise);
  }
}

void WigigPhy::NotifyMonitorSniffTx(Ptr<const WigigPsdu> psdu,
                                    uint16_t channelFreqMhz,
                                    const WigigTxVector &txVector) {
  MpduInfo aMpdu;
  if (psdu->IsAggregate()) {
    // Expand A-MPDU
    NS_ASSERT_MSG(
        txVector.IsAggregation(),
        "TxVector with aggregate flag expected here according to PSDU");
    aMpdu.mpduRefNumber = ++m_rxMpduReferenceNumber;
    size_t nMpdus = psdu->GetNMpdus();
    aMpdu.type = (psdu->IsSingle()) ? SINGLE_MPDU : FIRST_MPDU_IN_AGGREGATE;
    for (size_t i = 0; i < nMpdus;) {
      m_phyMonitorSniffTxTrace(psdu->GetAmpduSubframe(i), channelFreqMhz,
                               txVector, aMpdu);
      ++i;
      aMpdu.type = (i == (nMpdus - 1)) ? LAST_MPDU_IN_AGGREGATE
                                       : MIDDLE_MPDU_IN_AGGREGATE;
    }
  } else {
    aMpdu.type = NORMAL_MPDU;
    m_phyMonitorSniffTxTrace(psdu->GetPacket(), channelFreqMhz, txVector,
                             aMpdu);
  }
}

void WigigPhy::StartReceivePreamble(Ptr<WigigPpdu> ppdu,
                                    std::vector<double> rxPowerList) {
  NS_LOG_FUNCTION(this << *ppdu);
  WigigTxVector txVector = ppdu->GetTxVector();
  Time rxDuration = ppdu->GetTxDuration();
  Ptr<const WigigPsdu> psdu = ppdu->GetPsdu();

  /* Check if the transmission mode is SISO or MIMO */
  double rxPowerW;
  Ptr<WigigSignalEvent> event;
  NS_LOG_INFO("Receiving packet in SISO mode");
  rxPowerW = rxPowerList.at(0);
  event = m_interference.Add(ppdu, txVector, rxDuration, rxPowerW);

  Time totalDuration = rxDuration + GetTrnFieldDuration(txVector);
  m_rxDuration = totalDuration; // Duration of the last received frame including
                                // TRN field/.
  Time endRx = Simulator::Now() + totalDuration;

  if (m_state->GetState() == WifiPhyState::OFF) {
    NS_LOG_DEBUG("Cannot start RX because device is OFF");
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      MaybeCcaBusyDuration();
    }
    return;
  }

  if (ppdu->IsTruncatedTx()) {
    NS_LOG_DEBUG(
        "Packet reception stopped because transmitter has been switched off");
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      MaybeCcaBusyDuration();
    }
    return;
  }

  if (!txVector.GetModeInitialized()) {
    // If SetRate method was not called above when filling in txVector, this
    // means the PHY does support the rate indicated in PHY SIG headers
    NS_LOG_DEBUG("drop packet because of unsupported RX mode");
    NotifyRxDrop(psdu, UNSUPPORTED_SETTINGS);
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      MaybeCcaBusyDuration();
    }
    return;
  }

  switch (m_state->GetState()) {
  case WifiPhyState::SWITCHING:
    NS_LOG_DEBUG("drop packet because of channel switching");
    NotifyRxDrop(psdu, CHANNEL_SWITCHING);
    /*
     * Packets received on the upcoming channel are added to the event list
     * during the switching state. This way the medium can be correctly sensed
     * when the device listens to the channel for the first time after the
     * switching e.g. after channel switching, the channel may be sensed as
     * busy due to other devices' transmissions started before the end of
     * the switching.
     */
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      // that packet will be noise _after_ the completion of the channel
      // switching.
      MaybeCcaBusyDuration();
      return;
    }
    break;
  case WifiPhyState::RX:
    NS_ASSERT(m_currentEvent);
    if (m_frameCaptureModel &&
        m_frameCaptureModel->IsInCaptureWindow(m_timeLastPreambleDetected) &&
        m_frameCaptureModel->CaptureNewFrame(m_currentEvent, event)) {
      AbortCurrentReception(FRAME_CAPTURE_PACKET_SWITCH);
      NS_LOG_DEBUG("Switch to new packet");
      StartRxPreamble(event, rxPowerW);
    } else {
      NS_LOG_DEBUG("Drop packet because already in Rx (power=" << rxPowerW
                                                               << "W)");
      NotifyRxDrop(psdu, RXING);
      if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
        // that packet will be noise _after_ the reception of the
        // currently-received packet.
        MaybeCcaBusyDuration();
      }
    }
    break;
  case WifiPhyState::TX:
    NS_LOG_DEBUG("Drop packet because already in Tx (power=" << rxPowerW
                                                             << "W)");
    NotifyRxDrop(psdu, TXING);
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      // that packet will be noise _after_ the transmission of the
      // currently-transmitted packet.
      MaybeCcaBusyDuration();
    }
    break;
  case WifiPhyState::CCA_BUSY:
    if (m_currentEvent) {
      if (m_frameCaptureModel &&
          m_frameCaptureModel->IsInCaptureWindow(m_timeLastPreambleDetected) &&
          m_frameCaptureModel->CaptureNewFrame(m_currentEvent, event)) {
        AbortCurrentReception(FRAME_CAPTURE_PACKET_SWITCH);
        NS_LOG_DEBUG("Switch to new packet");
        StartRxPreamble(event, rxPowerW);
      } else {
        NS_LOG_DEBUG("Drop packet because already in Rx (power=" << rxPowerW
                                                                 << "W)");
        NotifyRxDrop(psdu, RXING);
        if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
          // that packet will be noise _after_ the reception of the
          // currently-received packet.
          MaybeCcaBusyDuration();
        }
      }
    } else {
      StartRxPreamble(event, rxPowerW);
    }
    break;
  case WifiPhyState::IDLE:
    StartRxPreamble(event, rxPowerW);
    break;
  case WifiPhyState::SLEEP:
    NS_LOG_DEBUG("Drop packet because in sleep mode");
    NotifyRxDrop(psdu, SLEEPING);
    if (endRx > (Simulator::Now() + m_state->GetDelayUntilIdle())) {
      // that packet will be noise _after_ the sleep period.
      MaybeCcaBusyDuration();
    }
    break;
  default:
    NS_FATAL_ERROR("Invalid WigigPhy state.");
    break;
  }
}

void WigigPhy::StartReceivePayload(Ptr<WigigSignalEvent> event) {
  NS_LOG_FUNCTION(this << *event);
  NS_ASSERT(m_endPhyRxEvent.IsExpired());
  NS_ASSERT(m_endRxEvent.IsExpired());
  WigigTxVector txVector = event->GetTxVector();
  WifiMode txMode = txVector.GetMode();

  Time payloadDuration = event->GetEndTime() - event->GetStartTime() -
                         CalculatePhyPreambleAndHeaderDuration(txVector);

  if (txVector.GetNss() > GetMaxSupportedRxSpatialStreams()) {
    NS_LOG_DEBUG(
        "Packet reception could not be started because not enough RX antennas");
    NotifyRxDrop(event->GetPsdu(), UNSUPPORTED_SETTINGS);
  } else if ((txVector.GetChannelWidth() >= 2160) &&
             (txVector.GetChannelWidth() > GetChannelWidth())) {
    NS_LOG_DEBUG("Packet reception could not be started because not enough "
                 "channel width");
    NotifyRxDrop(event->GetPsdu(), UNSUPPORTED_SETTINGS);
  } else if (!IsModeSupported(txMode)) {
    NS_LOG_DEBUG("Drop packet because it was sent using an unsupported mode ("
                 << txMode << ")");
    NotifyRxDrop(event->GetPsdu(), UNSUPPORTED_SETTINGS);
  } else {
    // We switch to Rx for the duration of the payload + TRN field.
    // Need to check if we need to switch to Rx independently at each AGC and
    // TRN subfield.
    m_state->SwitchToRx(payloadDuration + GetTrnFieldDuration(txVector));
    NS_LOG_DEBUG("Rx Duration=" << payloadDuration +
                                       GetTrnFieldDuration(txVector));
    NS_LOG_DEBUG("End Rx=" << Simulator::Now() + payloadDuration +
                                  GetTrnFieldDuration(txVector));
    m_endRxEvent = Simulator::Schedule(payloadDuration, &WigigPhy::EndReceive,
                                       this, event);
    NS_LOG_DEBUG("Receiving PSDU");
    m_phyRxPayloadBeginTrace(
        txVector,
        payloadDuration); // this callback (equivalent to PHY-RXSTART primitive)
                          // is triggered only if headers have been correctly
                          // decoded and that the mode within is supported
    return;
  }
}

void WigigPhy::EndReceive(Ptr<WigigSignalEvent> event) {
  WigigTxVector txVector = event->GetTxVector();
  Time psduDuration = event->GetEndTime() - event->GetStartTime() -
                      CalculatePhyPreambleAndHeaderDuration(txVector);
  NS_LOG_FUNCTION(this << *event << psduDuration);

  double snr = m_interference.CalculatePayloadSnr(event);
  NS_LOG_DEBUG("snr(dB)=" << RatioToDb(snr));
  std::vector<bool> statusPerMpdu;
  SignalNoiseDbm signalNoise;

  Ptr<const WigigPsdu> psdu = event->GetPsdu();
  Time relativeStart = NanoSeconds(0);
  bool receptionOkAtLeastForOneMpdu = false;
  std::pair<bool, SignalNoiseDbm> rxInfo;
  size_t nMpdus = psdu->GetNMpdus();

  NS_LOG_DEBUG("PSDU Size=" << psdu->GetSize() << ", PPDU Duration="
                            << event->GetPpdu()->GetTxDuration()
                            << ", #MPDUs=" << nMpdus);

  if (nMpdus > 1) {
    // Extract all MPDUs of the A-MPDU to compute per-MPDU PER stats
    Time remainingAmpduDuration = psduDuration;
    Time mpduDuration;
    auto mpdu = psdu->begin();
    for (size_t i = 0; i < nMpdus && mpdu != psdu->end(); ++mpdu) {
      if (i == (nMpdus - 1)) {
        mpduDuration = remainingAmpduDuration;
      } else {
        mpduDuration =
            NanoSeconds((static_cast<double>(psdu->GetAmpduSubframeSize(i)) /
                         psdu->GetSize()) *
                        psduDuration.GetNanoSeconds());
      }
      remainingAmpduDuration -= mpduDuration;

      NS_LOG_DEBUG("H1: MPDU Duration=" << mpduDuration);
      NS_LOG_DEBUG("H2: Remaining Duration=" << remainingAmpduDuration);

      rxInfo = GetReceptionStatus(Create<WigigPsdu>(*mpdu, false), event,
                                  relativeStart, mpduDuration);
      NS_LOG_DEBUG("Extracted MPDU #"
                   << i << ": size=" << psdu->GetAmpduSubframeSize(i)
                   << ", duration: " << mpduDuration.GetNanoSeconds() << "ns"
                   << ", correct reception: " << rxInfo.first
                   << ", Signal/Noise: " << rxInfo.second.signal << "/"
                   << rxInfo.second.noise << "dBm");
      signalNoise = rxInfo.second; // same information for all MPDUs
      statusPerMpdu.push_back(rxInfo.first);
      receptionOkAtLeastForOneMpdu |= rxInfo.first;

      // Prepare next iteration
      ++i;
      relativeStart += mpduDuration;
    }
    if (!remainingAmpduDuration.IsZero()) {
      NS_FATAL_ERROR("Remaining A-MPDU duration should be zero");
    }
  } else {
    rxInfo = GetReceptionStatus(psdu, event, relativeStart, psduDuration);
    signalNoise = rxInfo.second; // same information for all MPDUs
    statusPerMpdu.push_back(rxInfo.first);
    receptionOkAtLeastForOneMpdu = rxInfo.first;
  }

  NotifyRxEnd(psdu);

  if (txVector.GetTrainingFieldLength() > 0) {
    if (receptionOkAtLeastForOneMpdu) {
      /* The following two variables will indicate if we can receive the
       * following TRN subfields or drop them */
      m_psduSuccess = true;

      NotifyMonitorSniffRx(psdu, GetFrequency(), txVector, signalNoise,
                           statusPerMpdu);
      m_state->ReportPsduRxOk(Copy(psdu), snr, txVector, statusPerMpdu);

      /* Signal that we are in the middle of receiving TRN fields and save the
       * receive power in case of ending the reception due to end of allocation
       * period */
      if (txVector.GetPacketType() == TRN_R) {
        if (txVector.IsDmgBeacon()) {
          // APs do not train using the TRN fields appended to beacons from
          // other APs
          if (!m_isAp) {
            m_codebook->StartBeaconTraining();
          }
        } else {
          m_codebook->UseCustomAwv(RefineReceiveSector);
        }
        if (txVector.GetTrainingFieldLength() > 0) {
          /* Schedule the next change of the AWV for DMG TRN fields*/
          Simulator::Schedule(AGC_SF_DURATION,
                              &WigigPhy::PrepareForAgcRxReception, this,
                              txVector.GetTrainingFieldLength() - 1);
        }
      }
    } else {
      m_state->ReportPsduEndError(Copy(psdu), snr);
      m_psduSuccess = false;
      // Add interference event for the TRN field.
      event = m_interference.Add(txVector, m_state->GetDelayUntilIdle(),
                                 event->GetRxPowerW());
    }
  } else {
    if (receptionOkAtLeastForOneMpdu) {
      NotifyMonitorSniffRx(psdu, GetFrequency(), txVector, signalNoise,
                           statusPerMpdu);
      m_state->SwitchFromRxEndOk(Copy(psdu), snr, txVector, statusPerMpdu);
    } else {
      m_state->SwitchFromRxEndError(Copy(psdu), snr);
    }
    m_currentEvent = nullptr;
    MaybeCcaBusyDuration();
  }
  m_interference.NotifyRxEnd();
}

std::pair<bool, SignalNoiseDbm>
WigigPhy::GetReceptionStatus(Ptr<const WigigPsdu> psdu,
                             Ptr<WigigSignalEvent> event,
                             Time relativeMpduStart, Time mpduDuration) {
  NS_LOG_FUNCTION(this << *psdu << *event << relativeMpduStart << mpduDuration);
  WigigInterferenceHelper::SnrPer snrPer;
  snrPer = m_interference.CalculatePayloadSnrPer(
      event,
      std::make_pair(relativeMpduStart, relativeMpduStart + mpduDuration));

  NS_LOG_DEBUG(
      "mode=" << (event->GetTxVector().GetMode().GetDataRate(
                     event->GetTxVector()))
              << ", snr(dB)=" << RatioToDb(snrPer.snr) << ", per=" << snrPer.per
              << ", size=" << psdu->GetSize()
              << ", relativeStart = " << relativeMpduStart.GetNanoSeconds()
              << "ns, duration = " << mpduDuration.GetNanoSeconds() << "ns");

  // There are two error checks: PER and receive error model check.
  // PER check models is typical for Wi-Fi and is based on signal modulation;
  // Receive error model is optional, if we have an error model and
  // it indicates that the packet is corrupt, drop the packet.
  SignalNoiseDbm signalNoise;
  signalNoise.signal = WToDbm(event->GetRxPowerW());
  signalNoise.noise = WToDbm(event->GetRxPowerW() / snrPer.snr);
  if (m_random->GetValue() > snrPer.per &&
      !(m_postReceptionErrorModel &&
        m_postReceptionErrorModel->IsCorrupt(psdu->GetPacket()->Copy()))) {
    NS_LOG_DEBUG("Reception succeeded: " << psdu);
    return std::make_pair(true, signalNoise);
  } else {
    NS_LOG_DEBUG("Reception failed: " << psdu);
    return std::make_pair(false, signalNoise);
  }
}

void WigigPhy::PrepareForAgcRxReception(uint8_t remainingAgcRxSubields) {
  NS_LOG_FUNCTION(this << +remainingAgcRxSubields);
  m_codebook->GetNextAwv();
  remainingAgcRxSubields--;
  if (remainingAgcRxSubields > 0) {
    Simulator::Schedule(AGC_SF_DURATION, &WigigPhy::PrepareForAgcRxReception,
                        this, remainingAgcRxSubields);
  }
}

void WigigPhy::SetIsAp(bool ap) { m_isAp = ap; }

void WigigPhy::MaybeCcaBusyDuration() {
  // We are here because we have received the first bit of a packet and we are
  // not going to be able to synchronize on it
  // In this model, CCA becomes busy when the aggregation of all signals as
  // tracked by the WigigInterferenceHelper class is higher than the
  // CcaBusyThreshold

  Time delayUntilCcaEnd = m_interference.GetEnergyDuration(m_ccaEdThresholdW);
  if (!delayUntilCcaEnd.IsZero()) {
    NS_LOG_DEBUG("In CCA Busy State for " << delayUntilCcaEnd);
    m_state->SwitchMaybeToCcaBusy(delayUntilCcaEnd);
  } else {
    NS_LOG_DEBUG("Not in CCA Busy State");
  }
}

Time WigigPhy::GetDelayUntilEndRx() {
  if (m_state->IsStateRx()) {
    return GetDelayUntilIdle();
  } else {
    return NanoSeconds(0);
  }
}

double WigigPhy::GetPowerDbm(uint8_t power) const {
  NS_ASSERT(m_txPowerBaseDbm <= m_txPowerEndDbm);
  NS_ASSERT(m_nTxPower > 0);
  double dbm;
  if (m_nTxPower > 1) {
    dbm = m_txPowerBaseDbm +
          power * (m_txPowerEndDbm - m_txPowerBaseDbm) / (m_nTxPower - 1);
  } else {
    NS_ASSERT_MSG(
        m_txPowerBaseDbm == m_txPowerEndDbm,
        "cannot have TxPowerEnd != TxPowerStart with TxPowerLevels == 1");
    dbm = m_txPowerBaseDbm;
  }
  return dbm;
}

Time WigigPhy::GetChannelSwitchDelay() const { return m_channelSwitchDelay; }

double WigigPhy::CalculateSnr(const WigigTxVector &txVector, double ber) const {
  return m_interference.GetErrorRateModel()->CalculateSnr(txVector, ber);
}

void WigigPhy::ConfigureDefaultsForStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  switch (standard) {
  case WIFI_STANDARD_80211ad:
    SetChannelWidth(2160);
    SetFrequency(58320);
    // Channel number should be aligned by SetFrequency () to 1
    NS_ASSERT(GetChannelNumber() == 1);
    break;
  case WIFI_STANDARD_UNSPECIFIED:
  default:
    NS_LOG_WARN("Configuring unspecified standard; performing no action");
    break;
  }
}

uint8_t WigigPhy::FindChannelNumberForFrequencyWidth(uint16_t frequency,
                                                     uint16_t width) const {
  NS_LOG_FUNCTION(this << frequency << width);
  bool found = false;
  FrequencyWidthPair f = std::make_pair(frequency, width);
  ChannelToFrequencyWidthMap::const_iterator it =
      m_channelToFrequencyWidth.begin();
  while (it != m_channelToFrequencyWidth.end()) {
    if (it->second == f) {
      found = true;
      break;
    }
    ++it;
  }
  if (found) {
    NS_LOG_DEBUG("Found, returning " << +it->first.first);
    return (it->first.first);
  } else {
    NS_LOG_DEBUG("Not found, returning 0");
    return 0;
  }
}

void WigigPhy::ConfigureChannelForStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  // If the user has configured both Frequency and ChannelNumber, Frequency
  // takes precedence
  if (GetFrequency() != 0) {
    // If Frequency is already set, then see whether a ChannelNumber can
    // be found that matches Frequency and ChannelWidth. If so, configure
    // the ChannelNumber to that channel number. If not, set ChannelNumber to
    // zero.
    NS_LOG_DEBUG(
        "Frequency set; checking whether a channel number corresponds");
    uint8_t channelNumberSearched =
        FindChannelNumberForFrequencyWidth(GetFrequency(), GetChannelWidth());
    if (channelNumberSearched) {
      NS_LOG_DEBUG("Channel number found; setting to "
                   << +channelNumberSearched);
      SetChannelNumber(channelNumberSearched);
    } else {
      NS_LOG_DEBUG("Channel number not found; setting to zero");
      SetChannelNumber(0);
    }
  } else if (GetChannelNumber() != 0) {
    // If the channel number is known for this particular standard or for
    // the unspecified standard, configure using the known values;
    // otherwise, this is a configuration error
    NS_LOG_DEBUG("Configuring for channel number " << +GetChannelNumber());
    FrequencyWidthPair f =
        GetFrequencyWidthForChannelNumberStandard(GetChannelNumber(), standard);
    if (f.first == 0) {
      // the specific pair of number/standard is not known
      NS_LOG_DEBUG("Falling back to check WIFI_STANDARD_UNSPECIFIED");
      f = GetFrequencyWidthForChannelNumberStandard(GetChannelNumber(),
                                                    WIFI_STANDARD_UNSPECIFIED);
    }
    if (f.first == 0) {
      NS_FATAL_ERROR("Error, ChannelNumber "
                     << +GetChannelNumber() << " is unknown for this standard");
    } else {
      NS_LOG_DEBUG("Setting frequency to " << f.first << "; width to "
                                           << +f.second);
      SetFrequency(f.first);
      SetChannelWidth(f.second);
    }
  }
}

void WigigPhy::ConfigureStandard(WifiStandard standard) {
  NS_LOG_FUNCTION(this << standard);
  m_standard = standard;
  m_isConstructed = true;
  if (!m_frequencyChannelNumberInitialized) {
    InitializeFrequencyChannelNumber();
  }
  if (GetFrequency() == 0 && GetChannelNumber() == 0) {
    ConfigureDefaultsForStandard(standard);
  } else {
    // The user has configured either (or both) Frequency or ChannelNumber
    ConfigureChannelForStandard(standard);
  }
  switch (standard) {
  case WIFI_STANDARD_80211ad:
    Configure80211ad();
    break;
  default:
    NS_ASSERT(false);
    break;
  }
}

WifiStandard WigigPhy::GetStandard() const { return m_standard; }

void WigigPhy::Configure80211ad() {
  /* CTRL-PHY */
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs0());

  /* SC-PHY */
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs1());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs2());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs3());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs4());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs5());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs6());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs7());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs8());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs9());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs10());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs11());
  m_deviceRateSet.push_back(WigigPhy::GetDmgMcs12());

  /* OFDM-PHY */
  if (m_supportOfdm) {
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs13());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs14());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs15());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs16());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs17());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs18());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs19());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs20());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs21());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs22());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs23());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs24());
  }

  /* LP-SC PHY */
  if (m_supportLpSc) {
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs25());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs26());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs27());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs28());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs29());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs30());
    m_deviceRateSet.push_back(WigigPhy::GetDmgMcs31());
  }
}

void WigigPhy::SetFrequency(uint16_t frequency) {
  NS_LOG_FUNCTION(this << frequency);
  if (!m_isConstructed) {
    NS_LOG_DEBUG("Saving frequency configuration for initialization");
    m_initialFrequency = frequency;
    return;
  }
  if (GetFrequency() == frequency) {
    NS_LOG_DEBUG("No frequency change requested");
    return;
  }
  if (frequency == 0) {
    DoFrequencySwitch(0);
    NS_LOG_DEBUG("Setting frequency and channel number to zero");
    m_channelCenterFrequency = 0;
    m_channelNumber = 0;
    return;
  }
  // If the user has configured both Frequency and ChannelNumber, Frequency
  // takes precedence.  Lookup the channel number corresponding to the
  // requested frequency.
  uint8_t nch =
      FindChannelNumberForFrequencyWidth(frequency, GetChannelWidth());
  if (nch != 0) {
    NS_LOG_DEBUG("Setting frequency " << frequency << " corresponds to channel "
                                      << +nch);
    if (DoFrequencySwitch(frequency)) {
      NS_LOG_DEBUG("Channel frequency switched to "
                   << frequency << "; channel number to " << +nch);
      m_channelCenterFrequency = frequency;
      m_channelNumber = nch;
    } else {
      NS_LOG_DEBUG("Suppressing reassignment of frequency");
    }
  } else {
    NS_LOG_DEBUG("Channel number is unknown for frequency " << frequency);
    if (DoFrequencySwitch(frequency)) {
      NS_LOG_DEBUG("Channel frequency switched to "
                   << frequency << "; channel number to " << 0);
      m_channelCenterFrequency = frequency;
      m_channelNumber = 0;
    } else {
      NS_LOG_DEBUG("Suppressing reassignment of frequency");
    }
  }
}

uint16_t WigigPhy::GetFrequency() const { return m_channelCenterFrequency; }

void WigigPhy::SetChannelWidth(uint16_t channelWidth) {
  NS_LOG_FUNCTION(this << channelWidth);
  NS_ASSERT_MSG(channelWidth == 2160, "wrong channel width value");
  m_channelWidth = channelWidth;
}

uint16_t WigigPhy::GetChannelWidth() const { return m_channelWidth; }

void WigigPhy::SetNumberOfAntennas(uint8_t antennas) {
  NS_ASSERT_MSG(antennas > 0 && antennas <= 4,
                "unsupported number of antennas");
  m_numberOfAntennas = antennas;
  m_interference.SetNumberOfReceiveAntennas(antennas);
}

uint8_t WigigPhy::GetNumberOfAntennas() const { return m_numberOfAntennas; }

void WigigPhy::SetMaxSupportedTxSpatialStreams(uint8_t streams) {
  NS_ASSERT(streams <= GetNumberOfAntennas());
  m_txSpatialStreams = streams;
}

uint8_t WigigPhy::GetMaxSupportedTxSpatialStreams() const {
  return m_txSpatialStreams;
}

void WigigPhy::SetMaxSupportedRxSpatialStreams(uint8_t streams) {
  NS_ASSERT(streams <= GetNumberOfAntennas());
  m_rxSpatialStreams = streams;
}

uint8_t WigigPhy::GetMaxSupportedRxSpatialStreams() const {
  return m_rxSpatialStreams;
}

WigigPhy::FrequencyWidthPair
WigigPhy::GetFrequencyWidthForChannelNumberStandard(
    uint8_t channelNumber, WifiStandard standard) const {
  ChannelNumberStandardPair p = std::make_pair(channelNumber, standard);
  FrequencyWidthPair f = m_channelToFrequencyWidth[p];
  return f;
}

void WigigPhy::SetChannelNumber(uint8_t nch) {
  NS_LOG_FUNCTION(this << +nch);
  if (!m_isConstructed) {
    NS_LOG_DEBUG("Saving channel number configuration for initialization");
    m_initialChannelNumber = nch;
    return;
  }
  if (GetChannelNumber() == nch) {
    NS_LOG_DEBUG("No channel change requested");
    return;
  }
  if (nch == 0) {
    // This case corresponds to when there is not a known channel
    // number for the requested frequency.  There is no need to call
    // DoChannelSwitch () because DoFrequencySwitch () should have been
    // called by the client
    NS_LOG_DEBUG("Setting channel number to zero");
    m_channelNumber = 0;
    return;
  }

  // First make sure that the channel number is defined for the standard
  // in use
  FrequencyWidthPair f =
      GetFrequencyWidthForChannelNumberStandard(nch, GetStandard());
  if (f.first == 0) {
    f = GetFrequencyWidthForChannelNumberStandard(nch,
                                                  WIFI_STANDARD_UNSPECIFIED);
  }
  if (f.first != 0) {
    if (DoChannelSwitch(nch)) {
      NS_LOG_DEBUG("Setting frequency to " << f.first << "; width to "
                                           << +f.second);
      m_channelCenterFrequency = f.first;
      SetChannelWidth(f.second);
      m_channelNumber = nch;
    } else {
      // Subclass may have suppressed (e.g. waiting for state change)
      NS_LOG_DEBUG("Channel switch suppressed");
    }
  } else {
    NS_FATAL_ERROR("Frequency not found for channel number " << +nch);
  }
}

uint8_t WigigPhy::GetChannelNumber() const { return m_channelNumber; }

bool WigigPhy::DoChannelSwitch(uint8_t nch) {
  if (!IsInitialized()) {
    // this is not channel switch, this is initialization
    NS_LOG_DEBUG("initialize to channel " << +nch);
    return true;
  }

  NS_ASSERT(!IsStateSwitching());
  switch (m_state->GetState()) {
  case WifiPhyState::RX:
    NS_LOG_DEBUG("drop packet because of channel switching while reception");
    m_endPhyRxEvent.Cancel();
    m_endRxEvent.Cancel();
    m_endPreambleDetectionEvent.Cancel();
    goto switchChannel;
    break;
  case WifiPhyState::TX:
    NS_LOG_DEBUG(
        "channel switching postponed until end of current transmission");
    Simulator::Schedule(GetDelayUntilIdle(), &WigigPhy::SetChannelNumber, this,
                        nch);
    break;
  case WifiPhyState::CCA_BUSY:
  case WifiPhyState::IDLE:
    if (m_endPreambleDetectionEvent.IsRunning()) {
      m_endPreambleDetectionEvent.Cancel();
      m_endRxEvent.Cancel();
    }
    goto switchChannel;
    break;
  case WifiPhyState::SLEEP:
    NS_LOG_DEBUG("channel switching ignored in sleep mode");
    break;
  default:
    NS_ASSERT(false);
    break;
  }

  return false;

switchChannel:

  NS_LOG_DEBUG("switching channel " << +GetChannelNumber() << " -> " << +nch);
  m_state->SwitchToChannelSwitching(GetChannelSwitchDelay());
  m_interference.EraseEvents();
  /*
   * Needed here to be able to correctly sensed the medium for the first
   * time after the switching. The actual switching is not performed until
   * after m_channelSwitchDelay. Packets received during the switching
   * state are added to the event list and are employed later to figure
   * out the state of the medium after the switching.
   */
  return true;
}

bool WigigPhy::DoFrequencySwitch(uint16_t frequency) {
  if (!IsInitialized()) {
    // this is not channel switch, this is initialization
    NS_LOG_DEBUG("start at frequency " << frequency);
    return true;
  }

  NS_ASSERT(!IsStateSwitching());
  switch (m_state->GetState()) {
  case WifiPhyState::RX:
    NS_LOG_DEBUG(
        "drop packet because of channel/frequency switching while reception");
    m_endPhyRxEvent.Cancel();
    m_endRxEvent.Cancel();
    m_endPreambleDetectionEvent.Cancel();
    goto switchFrequency;
    break;
  case WifiPhyState::TX:
    NS_LOG_DEBUG("channel/frequency switching postponed until end of current "
                 "transmission");
    Simulator::Schedule(GetDelayUntilIdle(), &WigigPhy::SetFrequency, this,
                        frequency);
    break;
  case WifiPhyState::CCA_BUSY:
  case WifiPhyState::IDLE:
    if (m_endPreambleDetectionEvent.IsRunning()) {
      m_endPreambleDetectionEvent.Cancel();
      m_endRxEvent.Cancel();
    }
    goto switchFrequency;
    break;
  case WifiPhyState::SLEEP:
    NS_LOG_DEBUG("frequency switching ignored in sleep mode");
    break;
  default:
    NS_ASSERT(false);
    break;
  }

  return false;

switchFrequency:

  NS_LOG_DEBUG("switching frequency " << GetFrequency() << " -> " << frequency);
  m_state->SwitchToChannelSwitching(GetChannelSwitchDelay());
  m_interference.EraseEvents();
  /*
   * Needed here to be able to correctly sensed the medium for the first
   * time after the switching. The actual switching is not performed until
   * after m_channelSwitchDelay. Packets received during the switching
   * state are added to the event list and are employed later to figure
   * out the state of the medium after the switching.
   */
  return true;
}

bool WigigPhy::IsModeSupported(WifiMode mode) const {
  for (uint8_t i = 0; i < GetNModes(); i++) {
    if (mode == GetMode(i)) {
      return true;
    }
  }
  return false;
}

uint8_t WigigPhy::GetNModes() const {
  return static_cast<uint8_t>(m_deviceRateSet.size());
}

WifiMode WigigPhy::GetMode(uint8_t mode) const { return m_deviceRateSet[mode]; }

bool WigigPhy::IsStateCcaBusy() const { return m_state->IsStateCcaBusy(); }

bool WigigPhy::IsStateIdle() const { return m_state->IsStateIdle(); }

bool WigigPhy::IsStateRx() const { return m_state->IsStateRx(); }

bool WigigPhy::IsStateTx() const { return m_state->IsStateTx(); }

bool WigigPhy::IsStateSwitching() const { return m_state->IsStateSwitching(); }

bool WigigPhy::IsStateSleep() const { return m_state->IsStateSleep(); }

bool WigigPhy::IsStateOff() const { return m_state->IsStateOff(); }

Time WigigPhy::GetDelayUntilIdle() { return m_state->GetDelayUntilIdle(); }

Time WigigPhy::GetLastRxStartTime() const {
  return m_state->GetLastRxStartTime();
}

Time WigigPhy::GetLastRxEndTime() const { return m_state->GetLastRxEndTime(); }

void WigigPhy::SwitchMaybeToCcaBusy() {
  NS_LOG_FUNCTION(this);
  // We are here because we have received the first bit of a packet and we are
  // not going to be able to synchronize on it
  // In this model, CCA becomes busy when the aggregation of all signals as
  // tracked by the WigigInterferenceHelper class is higher than the
  // CcaBusyThreshold

  Time delayUntilCcaEnd = m_interference.GetEnergyDuration(m_ccaEdThresholdW);
  if (!delayUntilCcaEnd.IsZero()) {
    NS_LOG_DEBUG("Calling SwitchMaybeToCcaBusy for "
                 << delayUntilCcaEnd.As(Time::S));
    m_state->SwitchMaybeToCcaBusy(delayUntilCcaEnd);
  }
}

void WigigPhy::AbortCurrentReception(WifiPhyRxfailureReason reason) {
  NS_LOG_FUNCTION(this << reason);
  if (m_endPreambleDetectionEvent.IsRunning()) {
    m_endPreambleDetectionEvent.Cancel();
  }
  if (m_endPhyRxEvent.IsRunning()) {
    m_endPhyRxEvent.Cancel();
  }
  if (m_endRxEvent.IsRunning()) {
    m_endRxEvent.Cancel();
  }
  NotifyRxDrop(m_currentEvent->GetPsdu(), reason);
  m_interference.NotifyRxEnd();
  m_currentEvent = nullptr;
}

WifiMode WigigPhy::GetDmgMcs(uint8_t index) {
  switch (index) {
  case 0:
    return GetDmgMcs0();
  case 1:
    return GetDmgMcs1();
  case 2:
    return GetDmgMcs2();
  case 3:
    return GetDmgMcs3();
  case 4:
    return GetDmgMcs4();
  case 5:
    return GetDmgMcs5();
  case 6:
    return GetDmgMcs6();
  case 7:
    return GetDmgMcs7();
  case 8:
    return GetDmgMcs8();
  case 9:
    return GetDmgMcs9();
  case 10:
    return GetDmgMcs10();
  case 11:
    return GetDmgMcs11();
  case 12:
    return GetDmgMcs12();
  case 13:
    return GetDmgMcs13();
  case 14:
    return GetDmgMcs14();
  case 15:
    return GetDmgMcs15();
  case 16:
    return GetDmgMcs16();
  case 17:
    return GetDmgMcs17();
  case 18:
    return GetDmgMcs18();
  case 19:
    return GetDmgMcs19();
  case 20:
    return GetDmgMcs20();
  case 21:
    return GetDmgMcs21();
  case 22:
    return GetDmgMcs22();
  case 23:
    return GetDmgMcs23();
  case 24:
    return GetDmgMcs24();
  case 25:
    return GetDmgMcs25();
  case 26:
    return GetDmgMcs26();
  case 27:
    return GetDmgMcs27();
  case 28:
    return GetDmgMcs28();
  case 29:
    return GetDmgMcs29();
  case 30:
    return GetDmgMcs30();
  case 31:
    return GetDmgMcs31();
  default:
    NS_ABORT_MSG("Inexistent (or not supported) index ("
                 << +index << ") requested for DMG PHY");
    return WifiMode();
  }
}

double WigigPhy::GetCodeRatio(WifiCodeRate codeRate) {
  switch (codeRate) {
  case WIFI_CODE_RATE_5_6:
    return (5.0 / 6.0);
  case WIFI_CODE_RATE_3_4:
    return (3.0 / 4.0);
  case WIFI_CODE_RATE_2_3:
    return (2.0 / 3.0);
  case WIFI_CODE_RATE_1_2:
    return (1.0 / 2.0);
  case WIFI_CODE_RATE_7_8:
    return (7.0 / 8.0);
  case WIFI_CODE_RATE_5_8:
    return (5.0 / 8.0);
  case WIFI_CODE_RATE_13_16:
    return (13.0 / 16.0);
  case WIFI_CODE_RATE_1_4:
    return (1.0 / 4.0);
  case WIFI_CODE_RATE_13_28:
    return (13.0 / 28.0);
  case WIFI_CODE_RATE_13_21:
    return (13.0 / 21.0);
  case WIFI_CODE_RATE_52_63:
    return (52.0 / 63.0);
  case WIFI_CODE_RATE_13_14:
    return (13.0 / 14.0);
  case WIFI_CODE_RATE_UNDEFINED:
  default:
    NS_FATAL_ERROR("trying to get code ratio for undefined coding rate");
    return 0;
  }
}

WifiCodeRate WigigPhy::GetCodeRate(uint8_t mcsValue) {
  switch (mcsValue) {
  case 0:
  case 2:
  case 6:  // Also has DmgMcs9_1 which has WIFI_CODE_RATE_7_8
  case 10: // Also has DmgMcs12_4 which has WIFI_CODE_RATE_3_4
  case 13:
  case 15:
  case 18:
    return WIFI_CODE_RATE_1_2;
  case 1:
    return WIFI_CODE_RATE_1_4;
  case 3:
  case 16:
  case 7:  // Also has DmgMcs12_1 which has WIFI_CODE_RATE_13_6
  case 11: // Also has DmgMcs12_5 which has WIFI_CODE_RATE_13_16
  case 19:
  case 22:
    return WIFI_CODE_RATE_5_8;
  case 4:
  case 12: // Also has DmgMcs12_6 which has WIFI_CODE_RATE_7_8
  case 17:
  case 20:
  case 8: // Also has DmgMcs12_2 which has WIFI_CODE_RATE_7_8
  case 23:
    return WIFI_CODE_RATE_3_4;
  case 5:
  case 9: // Also has DmgMcs12_3 which has WIFI_CODE_RATE_5_8
  case 21:
  case 24:
    return WIFI_CODE_RATE_13_16;
  case 14:
    return WIFI_CODE_RATE_5_8;
  case 25:
  case 28:
    return WIFI_CODE_RATE_13_28;
  case 26:
  case 29:
    return WIFI_CODE_RATE_13_21;
  case 27:
  case 30:
    return WIFI_CODE_RATE_52_63;
  case 31:
    return WIFI_CODE_RATE_13_14;
  default:
    return WIFI_CODE_RATE_UNDEFINED;
  }
}

uint16_t WigigPhy::GetConstellationSize(uint8_t mcsValue) {
  switch (mcsValue) {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
  case 13:
  case 14:
  case 25:
  case 26:
  case 27:
  case 28:
    return 2;
  case 6:
  case 7: // DmgMcs12_1 has 16
  case 8: // DmgMcs12_2 has 16
  case 9: // DmgMcs12_3 has 64
  case 15:
  case 16:
  case 17:
  case 29:
  case 30:
  case 31:
    return 4;
  case 10: // DmgMcs12_4 has 64
  case 11: // DmgMcs12_5 has 64
  case 12: // DmgMcs12_6 has 64
  case 18:
  case 19:
  case 20:
  case 21:
    return 16;
  case 22:
  case 23:
  case 24:
    return 64;
  default:
    return 0;
  }
}

uint16_t WigigPhy::GetUsableSubcarriers(WifiModulationClass modulationClass) {
  return (modulationClass == WIFI_MOD_CLASS_DMG_OFDM) ? 336 : 1;
}

uint64_t WigigPhy::CalculateDataRate(WifiModulationClass modulationClass,
                                     uint16_t numberOfBitsPerSubcarrier,
                                     double codingRate) {
  uint16_t usableSubCarriers = GetUsableSubcarriers(modulationClass);
  double symbolRate = 0;
  switch (modulationClass) {
  case WIFI_MOD_CLASS_DMG_CTRL:
    symbolRate = (1 / 1.8182) * 1e8;
    break;
  case WIFI_MOD_CLASS_DMG_SC:
    symbolRate = (1 / 6.493506) * 1e10;
    break;
  case WIFI_MOD_CLASS_DMG_OFDM:
    symbolRate = (1 / 2.424) * 1e7;
    break;
  case WIFI_MOD_CLASS_DMG_LP_SC:
    symbolRate = (1 / 7.416) * 1e10;
    break;
  default:
    NS_ASSERT_MSG(false, "undefined datarate for the modulation class!");
    break;
  }
  return lrint(ceil(symbolRate * usableSubCarriers * numberOfBitsPerSubcarrier *
                    codingRate));
}

uint64_t WigigPhy::GetDataRate(uint8_t mcsValue,
                               WifiModulationClass modulationClass,
                               uint16_t /*channelWidth*/,
                               uint16_t /*guardInterval*/, uint8_t /*nss*/) {
  return CalculateDataRate(
      modulationClass,
      static_cast<uint16_t>(log2(GetConstellationSize(mcsValue))),
      GetCodeRatio(GetCodeRate(mcsValue)));
}

uint64_t WigigPhy::GetPhyRate(uint8_t mcsValue,
                              WifiModulationClass modulationClass,
                              uint16_t channelWidth, uint16_t guardInterval,
                              uint8_t nss) {
  WifiCodeRate codeRate = GetCodeRate(mcsValue);
  uint64_t dataRate =
      GetDataRate(mcsValue, modulationClass, channelWidth, guardInterval, nss);
  return CalculatePhyRate(codeRate, dataRate);
}

uint64_t WigigPhy::CalculatePhyRate(WifiCodeRate codeRate, uint64_t dataRate) {
  return (dataRate / GetCodeRatio(codeRate));
}

uint64_t WigigPhy::GetPhyRateFromTxVector(const WifiTxVector &txVector,
                                          uint16_t /* staId */) {
  return GetPhyRate(txVector.GetMode().GetMcsValue(),
                    txVector.GetMode().GetModulationClass(),
                    txVector.GetChannelWidth(), txVector.GetGuardInterval(),
                    txVector.GetNss());
}

uint64_t WigigPhy::GetDataRateFromTxVector(const WifiTxVector &txVector,
                                           uint16_t /* staId */) {
  return GetDataRate(txVector.GetMode().GetMcsValue(),
                     txVector.GetMode().GetModulationClass(),
                     txVector.GetChannelWidth(), txVector.GetGuardInterval(),
                     txVector.GetNss());
}

bool WigigPhy::IsAllowed(const WifiTxVector & /*txVector*/) { return true; }

uint64_t WigigPhy::GetNonHtReferenceRate() {
  NS_ASSERT_MSG(false,
                "NonHtReferenceRateCallback shall not be called for DMG rate");
  return 0;
}

/**** DMG Control PHY MCS ****/
WifiMode WigigPhy::GetDmgMcs0() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs0", 0, WIFI_MOD_CLASS_DMG_CTRL, true,
      MakeBoundCallback(&GetCodeRate, 0),
      MakeBoundCallback(&GetConstellationSize, 0),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

/**** DMG SC PHY MCSs ****/
WifiMode WigigPhy::GetDmgMcs1() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs1", 1, WIFI_MOD_CLASS_DMG_SC, true,
      MakeBoundCallback(&GetCodeRate, 1),
      MakeBoundCallback(&GetConstellationSize, 1),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs2() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs2", 2, WIFI_MOD_CLASS_DMG_SC, true,
      MakeBoundCallback(&GetCodeRate, 2),
      MakeBoundCallback(&GetConstellationSize, 2),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs3() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs3", 3, WIFI_MOD_CLASS_DMG_SC, true,
      MakeBoundCallback(&GetCodeRate, 3),
      MakeBoundCallback(&GetConstellationSize, 3),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs4() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs4", 4, WIFI_MOD_CLASS_DMG_SC, true,
      MakeBoundCallback(&GetCodeRate, 4),
      MakeBoundCallback(&GetConstellationSize, 4),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs5() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs5", 5, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 5),
      MakeBoundCallback(&GetConstellationSize, 5),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs6() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs6", 6, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 6),
      MakeBoundCallback(&GetConstellationSize, 6),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs7() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs7", 7, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 7),
      MakeBoundCallback(&GetConstellationSize, 7),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs8() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs8", 8, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 8),
      MakeBoundCallback(&GetConstellationSize, 8),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs9() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs9", 9, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 9),
      MakeBoundCallback(&GetConstellationSize, 9),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

/**** Extended SC MCS ****/
WifiMode WigigPhy::GetDmgMcs9_1() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs9_1", 6, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 6),
      MakeBoundCallback(&GetConstellationSize, 6),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs10() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs10", 10, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 10),
      MakeBoundCallback(&GetConstellationSize, 10),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs11() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs11", 11, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 11),
      MakeBoundCallback(&GetConstellationSize, 11),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

/**** Extended SC MCSs Below ****/
WifiMode WigigPhy::GetDmgMcs12() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12", 12, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 12),
      MakeBoundCallback(&GetConstellationSize, 12),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_1() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_1", 7, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 7),
      MakeBoundCallback(&GetConstellationSize, 7),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_2() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_2", 8, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 8),
      MakeBoundCallback(&GetConstellationSize, 8),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_3() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_3", 9, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 9),
      MakeBoundCallback(&GetConstellationSize, 9),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_4() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_4", 10, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 10),
      MakeBoundCallback(&GetConstellationSize, 10),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_5() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_5", 11, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 11),
      MakeBoundCallback(&GetConstellationSize, 11),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs12_6() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs12_6", 12, WIFI_MOD_CLASS_DMG_SC, false,
      MakeBoundCallback(&GetCodeRate, 12),
      MakeBoundCallback(&GetConstellationSize, 12),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

/**** OFDM MCSs BELOW ****/
WifiMode WigigPhy::GetDmgMcs13() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs13", 13, WIFI_MOD_CLASS_DMG_OFDM, true,
      MakeBoundCallback(&GetCodeRate, 13),
      MakeBoundCallback(&GetConstellationSize, 13),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs14() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs14", 14, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 14),
      MakeBoundCallback(&GetConstellationSize, 14),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs15() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs15", 15, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 15),
      MakeBoundCallback(&GetConstellationSize, 15),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs16() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs16", 16, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 16),
      MakeBoundCallback(&GetConstellationSize, 16),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs17() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs17", 17, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 17),
      MakeBoundCallback(&GetConstellationSize, 17),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs18() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs18", 18, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 18),
      MakeBoundCallback(&GetConstellationSize, 18),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs19() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs19", 19, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 19),
      MakeBoundCallback(&GetConstellationSize, 19),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs20() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs20", 20, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 20),
      MakeBoundCallback(&GetConstellationSize, 20),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs21() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs21", 21, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 21),
      MakeBoundCallback(&GetConstellationSize, 21),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs22() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs22", 22, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 22),
      MakeBoundCallback(&GetConstellationSize, 22),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs23() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs23", 23, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 23),
      MakeBoundCallback(&GetConstellationSize, 23),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs24() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs24", 24, WIFI_MOD_CLASS_DMG_OFDM, false,
      MakeBoundCallback(&GetCodeRate, 24),
      MakeBoundCallback(&GetConstellationSize, 24),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

/**** Low Power SC MCSs ****/
WifiMode WigigPhy::GetDmgMcs25() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs25", 25, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 25),
      MakeBoundCallback(&GetConstellationSize, 25),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs26() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs26", 26, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 26),
      MakeBoundCallback(&GetConstellationSize, 26),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs27() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs27", 27, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 27),
      MakeBoundCallback(&GetConstellationSize, 27),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs28() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs28", 28, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 28),
      MakeBoundCallback(&GetConstellationSize, 28),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs29() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs29", 29, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 29),
      MakeBoundCallback(&GetConstellationSize, 29),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs30() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs30", 30, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 30),
      MakeBoundCallback(&GetConstellationSize, 30),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

WifiMode WigigPhy::GetDmgMcs31() {
  static WifiMode mcs = WifiModeFactory::CreateWifiMcs(
      "DmgMcs31", 31, WIFI_MOD_CLASS_DMG_LP_SC, false,
      MakeBoundCallback(&GetCodeRate, 31),
      MakeBoundCallback(&GetConstellationSize, 31),
      MakeCallback(&GetPhyRateFromTxVector),
      MakeCallback(&GetDataRateFromTxVector),
      MakeCallback(&GetNonHtReferenceRate), MakeCallback(&IsAllowed));
  return mcs;
}

int64_t WigigPhy::AssignStreams(int64_t stream) {
  NS_LOG_FUNCTION(this << stream);
  m_random->SetStream(stream);
  return 1;
}

} // namespace ns3

namespace {

/**
 * Constructor class
 */
class Constructor {
public:
  Constructor() {
    ns3::WigigPhy::GetDmgMcs0();
    ns3::WigigPhy::GetDmgMcs1();
    ns3::WigigPhy::GetDmgMcs2();
    ns3::WigigPhy::GetDmgMcs3();
    ns3::WigigPhy::GetDmgMcs4();
    ns3::WigigPhy::GetDmgMcs5();
    ns3::WigigPhy::GetDmgMcs6();
    ns3::WigigPhy::GetDmgMcs7();
    ns3::WigigPhy::GetDmgMcs8();
    ns3::WigigPhy::GetDmgMcs9();
    ns3::WigigPhy::GetDmgMcs9_1();
    ns3::WigigPhy::GetDmgMcs10();
    ns3::WigigPhy::GetDmgMcs11();
    ns3::WigigPhy::GetDmgMcs12();
    ns3::WigigPhy::GetDmgMcs12_1();
    ns3::WigigPhy::GetDmgMcs12_2();
    ns3::WigigPhy::GetDmgMcs12_3();
    ns3::WigigPhy::GetDmgMcs12_4();
    ns3::WigigPhy::GetDmgMcs12_5();
    ns3::WigigPhy::GetDmgMcs12_6();
    ns3::WigigPhy::GetDmgMcs13();
    ns3::WigigPhy::GetDmgMcs14();
    ns3::WigigPhy::GetDmgMcs15();
    ns3::WigigPhy::GetDmgMcs16();
    ns3::WigigPhy::GetDmgMcs17();
    ns3::WigigPhy::GetDmgMcs18();
    ns3::WigigPhy::GetDmgMcs19();
    ns3::WigigPhy::GetDmgMcs20();
    ns3::WigigPhy::GetDmgMcs21();
    ns3::WigigPhy::GetDmgMcs22();
    ns3::WigigPhy::GetDmgMcs23();
    ns3::WigigPhy::GetDmgMcs24();
  }
} g_constructor; ///< the constructor

} // namespace
