/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Mirko Banchi <mk.banchi@gmail.com>
 *          Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIFI_MAC_HEADER_COPY_H
#define WIFI_MAC_HEADER_COPY_H

#include "ns3/wifi-mac-header.h"
#include "ns3/wigig-data-types.h"

namespace ns3 {

class Time;

/**
 * \ingroup wigig
 *
 * Implements the IEEE 802.11 MAC header
 */
class WigigMacHeader : public WifiMacHeader {
public:
  WigigMacHeader();
  ~WigigMacHeader() override;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  void Serialize(Buffer::Iterator start) const override;
  uint32_t Deserialize(Buffer::Iterator start) override;

  /**
   * Set Type/Subtype values with the correct values depending
   * on the given type.
   *
   * \param type the WifiMacType for the header
   * \param resetToDsFromDs whether the ToDs and FromDs flags
   *        should be reset.
   */
  void SetType(WifiMacType type, bool resetToDsFromDs = true) override;

  /**
   * Set as DMG PPDU.
   */
  void SetAsDmgPpdu();
  /**
   * Check whether the current PPDU is DMG
   * \return True if the frame is DMG PPDU, otherwise false.
   */
  bool IsDmgPpdu() const;
  /**
   * The RDG/More PPDU subfield of the HT Control field is interpreted
   * differently depending on whether it is transmitted by an RD initiator or an
   * RD responder, as defined in Table 8-13. \return
   */
  void SetQosRdGrant(bool value);

  /**
   * Return the type (enum WifiMacType)
   *
   * \return the type (enum WifiMacType)
   */
  WifiMacType GetType() const override;
  /**
   * Return true if the header is a DMG CTS header.
   *
   * \return true if the header is a DMG CTS header, false otherwise
   */
  bool IsDmgCts() const;
  /**
   * Return true if the header is a DMG Beacon header.
   *
   * \return true if the header is a DMG Beacon header, false otherwise
   */
  bool IsDmgBeacon() const;
  /**
   * Return true if the header is a SSW header.
   *
   * \return true if the header is a SSW header, false otherwise
   */
  bool IsSsw() const;
  /**
   * Return true if the header is a SSW-Feedback header.
   *
   * \return true if the header is a SSW-Feedback header, false otherwise
   */
  bool IsSswFbck() const;
  /**
   * Return true if the header is a SSW-ACK header.
   *
   * \return true if the header is a SSW-ACK header, false otherwise
   */
  bool IsSswAck() const;
  /**
   * Return true if the header is a Poll header.
   *
   * \return true if the header is a Poll header, false otherwise
   */
  bool IsPollFrame() const;
  /**
   * Return true if the header is a SPR header.
   *
   * \return true if the header is a SPR header, false otherwise
   */
  bool IsSprFrame() const;
  /**
   * Return true if the header is a Grant header.
   *
   * \return true if the header is a Grant header, false otherwise
   */
  bool IsGrantFrame() const;
  /**
   * Check if it is RD Grant.
   *
   * \return True if RDG is set, otherwise false.
   */
  bool IsQosRdGrant() const;
  /**
   * Return the size of the WigigMacHeader in octets.
   * GetSerializedSize calls this function.
   *
   * \return the size of the WigigMacHeader in octets
   */
  uint32_t GetSize() const override;
  /**
   * Return a string corresponds to the header type.
   *
   * \returns a string corresponds to the header type.
   */
  const char *GetTypeString() const override;

  /**
   * TracedCallback signature for WigigMacHeader
   *
   * \param [in] header The header
   */
  typedef void (*TracedCallback)(const WigigMacHeader &header);

  /**
   * Set BRP Packet Type.
   * \param type The type of BRP packet.
   */
  void SetPacketType(PacketType type);
  /**
   * Get BRP Packet Type.
   * \return The type of BRP packet.
   */
  PacketType GetPacketType() const;
  /**
   * Set the length of the training field.
   * \param length The length of the training field.
   */
  void SetTrainingFieldLength(uint8_t length);
  /**
   * Get the length of the training field.
   * \return The length of te training field.
   */
  uint8_t GetTrainingFieldLength() const;
  /**
   * Request Beam Tracking.
   */
  void RequestBeamTracking();
  /**
   * \return True if Beam Tracking requested, otherwise false.
   */
  bool IsBeamTrackingRequested() const;

private:
  /**
   * Return the raw Frame Control field.
   *
   * \return the raw Frame Control field
   */
  uint16_t GetFrameControl() const override;
  /**
   * Return the raw QoS Control field.
   *
   * \return the raw QoS Control field
   */
  uint16_t GetQosControl() const override;
  /**
   * Set the Frame Control field with the given raw value.
   *
   * \param control the raw Frame Control field value
   */
  void SetFrameControl(uint16_t control) override;
  /**
   * Set the QoS Control field with the given raw value.
   *
   * \param qos the raw QoS Control field value
   */
  void SetQosControl(uint16_t qos) override;

  /* DMG QoS Control Field */
  bool m_dmgPpdu;
  uint8_t m_qosRdg;
  /* New fields for DMG Support. */
  uint32_t m_ctrlFrameExtension;
  bool m_beamTrackingRequired;
  PacketType m_brpPacketType;
  uint8_t m_trainingFieldLength;
};

} // namespace ns3

#endif /* WIFI_MAC_HEADER_COPY_H */
