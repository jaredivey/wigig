/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */

#include "wigig-channel-access-manager.h"

#include "mac-low.h"
#include "wigig-phy.h"
#include "wigig-txop.h"

#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/wifi-phy-listener.h"

namespace ns3 {

/**
 * Listener for PHY events. Forwards to WigigChannelAccessManager
 */
class PhyListener : public ns3::WifiPhyListener {
public:
  /**
   * Create a PhyListener for the given WigigChannelAccessManager.
   *
   * \param cam the WigigChannelAccessManager
   */
  PhyListener(ns3::WigigChannelAccessManager *cam) : m_cam(cam) {}

  ~PhyListener() override {}

  void NotifyRxStart(Time duration) override {
    m_cam->NotifyRxStartNow(duration);
  }

  void NotifyRxEndOk() override { m_cam->NotifyRxEndOkNow(); }

  void NotifyRxEndError() override { m_cam->NotifyRxEndErrorNow(); }

  void NotifyTxStart(Time duration, double txPowerDbm) override {
    m_cam->NotifyTxStartNow(duration);
  }

  void
  NotifyCcaBusyStart(Time duration, WifiChannelListType /*channelType*/,
                     const std::vector<Time> & /*per20MhzDurations*/) override {
    m_cam->NotifyCcaBusyStartNow(duration);
  }

  void NotifySwitchingStart(Time duration) override {
    m_cam->NotifySwitchingStartNow(duration);
  }

  void NotifySleep() override { m_cam->NotifySleepNow(); }

  void NotifyOff() override { m_cam->NotifyOffNow(); }

  void NotifyWakeup() override { m_cam->NotifyWakeupNow(); }

  void NotifyOn() override { m_cam->NotifyOnNow(); }

private:
  ns3::WigigChannelAccessManager
      *m_cam; //!< WigigChannelAccessManager to forward events to
};

NS_LOG_COMPONENT_DEFINE("WigigChannelAccessManager");

/****************************************************************
 *      Implement the channel access manager of all WigigTxop holders
 ****************************************************************/

WigigChannelAccessManager::WigigChannelAccessManager()
    : m_lastAckTimeoutEnd(MicroSeconds(0)),
      m_lastCtsTimeoutEnd(MicroSeconds(0)), m_lastNavStart(MicroSeconds(0)),
      m_lastNavDuration(MicroSeconds(0)), m_lastRxStart(MicroSeconds(0)),
      m_lastRxDuration(MicroSeconds(0)), m_lastRxReceivedOk(true),
      m_lastTxStart(MicroSeconds(0)), m_lastTxDuration(MicroSeconds(0)),
      m_lastBusyStart(MicroSeconds(0)), m_lastBusyDuration(MicroSeconds(0)),
      m_lastSwitchingStart(MicroSeconds(0)),
      m_lastSwitchingDuration(MicroSeconds(0)), m_sleeping(false), m_off(false),
      m_slot(Seconds(0.0)), m_sifs(Seconds(0.0)), m_phyListener(nullptr),
      m_accessAllowed(true) {
  NS_LOG_FUNCTION(this);
}

WigigChannelAccessManager::~WigigChannelAccessManager() {
  NS_LOG_FUNCTION(this);
  delete m_phyListener;
  m_phyListener = nullptr;
}

void WigigChannelAccessManager::DoDispose() {
  NS_LOG_FUNCTION(this);
  for (Ptr<WigigTxop> i : m_txops) {
    i->Dispose();
    i = nullptr;
  }
  m_phy = nullptr;
}

void WigigChannelAccessManager::SetupPhyListener(Ptr<WigigPhy> phy) {
  NS_LOG_FUNCTION(this << phy);
  NS_ASSERT(!m_phyListener);
  m_phyListener = new PhyListener(this);
  phy->RegisterListener(m_phyListener);
  m_phy = phy;
}

void WigigChannelAccessManager::RemovePhyListener(Ptr<WigigPhy> phy) {
  NS_LOG_FUNCTION(this << phy);
  if (m_phyListener) {
    phy->UnregisterListener(m_phyListener);
    delete m_phyListener;
    m_phyListener = nullptr;
    m_phy = nullptr;
  }
}

void WigigChannelAccessManager::SetupLow(Ptr<MacLow> low) {
  NS_LOG_FUNCTION(this << low);
  low->RegisterWigigChannelAccessManager(this);
}

void WigigChannelAccessManager::SetSlot(Time slotTime) {
  NS_LOG_FUNCTION(this << slotTime);
  m_slot = slotTime;
}

void WigigChannelAccessManager::SetSifs(Time sifs) {
  NS_LOG_FUNCTION(this << sifs);
  m_sifs = sifs;
}

void WigigChannelAccessManager::SetEifsNoDifs(Time eifsNoDifs) {
  NS_LOG_FUNCTION(this << eifsNoDifs);
  m_eifsNoDifs = eifsNoDifs;
}

Time WigigChannelAccessManager::GetEifsNoDifs() const {
  NS_LOG_FUNCTION(this);
  return m_eifsNoDifs;
}

void WigigChannelAccessManager::Add(Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << txop);
  m_txops.push_back(txop);
}

Time WigigChannelAccessManager::MostRecent(
    std::initializer_list<Time> list) const {
  NS_ASSERT(list.size() > 0);
  return *std::max_element(list.begin(), list.end());
}

bool WigigChannelAccessManager::IsBusy() const {
  NS_LOG_FUNCTION(this);
  // PHY busy
  Time lastRxEnd = m_lastRxStart + m_lastRxDuration;
  if (lastRxEnd > Simulator::Now()) {
    return true;
  }
  Time lastTxEnd = m_lastTxStart + m_lastTxDuration;
  if (lastTxEnd > Simulator::Now()) {
    return true;
  }
  // NAV busy
  Time lastNavEnd = m_lastNavStart + m_lastNavDuration;
  if (lastNavEnd > Simulator::Now()) {
    return true;
  }
  // CCA busy
  Time lastCcaBusyEnd = m_lastBusyStart + m_lastBusyDuration;
  return lastCcaBusyEnd > Simulator::Now();
}

void WigigChannelAccessManager::AllowChannelAccess() { m_accessAllowed = true; }

void WigigChannelAccessManager::DisableChannelAccess() {
  m_accessAllowed = false;

  Time now = Simulator::Now();

  // Cancel timeout
  if (m_accessTimeout.IsRunning()) {
    m_accessTimeout.Cancel();
  }

  // Reset backoffs
  for (auto txop : m_txops) {
    uint32_t remainingSlots = txop->GetBackoffSlots();
    if (remainingSlots > 0) {
      txop->UpdateBackoffSlotsNow(remainingSlots, now);
      NS_ASSERT(txop->GetBackoffSlots() == 0);
    }
    txop->ResetCw();
    txop->m_accessRequested = false;
  }
}

bool WigigChannelAccessManager::NeedBackoffUponAccess(Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << txop);

  // the WigigTxop might have a stale value of remaining backoff slots
  UpdateBackoff();

  /*
   * From section 10.3.4.2 "Basic access" of IEEE 802.11-2016:
   *
   * A STA may transmit an MPDU when it is operating under the DCF access
   * method, either in the absence of a PC, or in the CP of the PCF access
   * method, when the STA determines that the medium is idle when a frame is
   * queued for transmission, and remains idle for a period of a DIFS, or an
   * EIFS (10.3.2.3.7) from the end of the immediately preceding medium-busy
   * event, whichever is the greater, and the backoff timer is zero. Otherwise
   * the random backoff procedure described in 10.3.4.3 shall be followed.
   *
   * From section 10.22.2.2 "EDCA backoff procedure" of IEEE 802.11-2016:
   *
   * The backoff procedure shall be invoked by an EDCAF when any of the
   * following events occurs: a) An MA-UNITDATA.request primitive is received
   * that causes a frame with that AC to be queued for transmission such that
   * one of the transmit queues associated with that AC has now become non-empty
   * and any other transmit queues associated with that AC are empty; the medium
   * is busy on the primary channel
   */
  if (!txop->HasFramesToTransmit() && txop->GetBackoffSlots() == 0) {
    if (!IsBusy()) {
      // medium idle. If this is a DCF, use immediate access (we can transmit
      // in a DIFS if the medium remains idle). If this is an EDCAF, update
      // the backoff start time kept by the EDCAF to the current time in order
      // to correctly align the backoff start time at the next slot boundary
      // (performed by the next call to
      // WigigChannelAccessManager::RequestAccess())
      Time delay =
          (txop->IsWigigQosTxop() ? Seconds(0)
                                  : m_sifs + txop->GetAifsn() * m_slot);
      txop->UpdateBackoffSlotsNow(0, Simulator::Now() + delay);
    } else {
      // medium busy, backoff is needed
      return true;
    }
  }
  return false;
}

bool WigigChannelAccessManager::IsAccessAllowed() const {
  return m_accessAllowed;
}

bool WigigChannelAccessManager::CanAccess() const {
  NS_LOG_FUNCTION(this);
  // Deny access if in sleep mode
  if (m_sleeping) {
    return false;
  }
  // PHY busy
  Time lastRxEnd = m_lastRxStart + m_lastRxDuration;
  if (lastRxEnd > Simulator::Now()) {
    return false;
  }
  Time lastTxEnd = m_lastTxStart + m_lastTxDuration;
  if (lastTxEnd > Simulator::Now()) {
    return false;
  }
  // CCA busy
  Time lastCcaBusyEnd = m_lastBusyStart + m_lastBusyDuration;
  return lastCcaBusyEnd <= Simulator::Now();
}

void WigigChannelAccessManager::RequestAccess(Ptr<WigigTxop> txop,
                                              bool isCfPeriod) {
  NS_LOG_FUNCTION(this << txop);
  // Deny access if in sleep mode or off
  if (m_sleeping || m_off) {
    return;
  }
  if (isCfPeriod) {
    txop->NotifyAccessRequested();
    Time delay = (MostRecent({GetAccessGrantStart(true), Simulator::Now()}) -
                  Simulator::Now());
    m_accessTimeout = Simulator::Schedule(
        delay, &WigigChannelAccessManager::DoGrantPcfAccess, this, txop);
    return;
  }
  /*
   * EDCAF operations shall be performed at slot boundaries (Sec. 10.22.2.4 of
   * 802.11-2016)
   */
  Time accessGrantStart = GetAccessGrantStart() + (txop->GetAifsn() * m_slot);

  if (txop->IsWigigQosTxop() && txop->GetBackoffStart() > accessGrantStart) {
    // The backoff start time reported by the EDCAF is more recent than the last
    // time the medium was busy plus an AIFS, hence we need to align it to the
    // next slot boundary.
    Time diff = txop->GetBackoffStart() - accessGrantStart;
    uint32_t nIntSlots = (diff / m_slot).GetHigh() + 1;
    txop->UpdateBackoffSlotsNow(0, accessGrantStart + (nIntSlots * m_slot));
  }

  UpdateBackoff();
  NS_ASSERT(!txop->IsAccessRequested());
  txop->NotifyAccessRequested();
  DoGrantDcfAccess();
  DoRestartAccessTimeoutIfNeeded();
}

void WigigChannelAccessManager::DoGrantPcfAccess(Ptr<WigigTxop> txop) {
  txop->NotifyAccessGranted();
}

void WigigChannelAccessManager::DoGrantDcfAccess() {
  NS_LOG_FUNCTION(this);
  uint32_t k = 0;
  for (WigigTxops::iterator i = m_txops.begin(); i != m_txops.end(); k++) {
    Ptr<WigigTxop> txop = *i;
    if (txop->IsAccessRequested() &&
        GetBackoffEndFor(txop) <= Simulator::Now()) {
      /**
       * This is the first WigigTxop we find with an expired backoff and which
       * needs access to the medium. i.e., it has data to send.
       */
      NS_LOG_DEBUG(
          "dcf " << k
                 << " needs access. backoff expired. access granted. slots="
                 << txop->GetBackoffSlots());
      i++; // go to the next item in the list.
      k++;
      std::vector<Ptr<WigigTxop>> internalCollisionWigigTxops;
      for (WigigTxops::iterator j = i; j != m_txops.end(); j++, k++) {
        Ptr<WigigTxop> otherWigigTxop = *j;
        if (otherWigigTxop->IsAccessRequested() &&
            GetBackoffEndFor(otherWigigTxop) <= Simulator::Now()) {
          NS_LOG_DEBUG(
              "dcf "
              << k
              << " needs access. backoff expired. internal collision. slots="
              << otherWigigTxop->GetBackoffSlots());
          /**
           * all other WigigTxops with a lower priority whose backoff
           * has expired and which needed access to the medium
           * must be notified that we did get an internal collision.
           */
          internalCollisionWigigTxops.push_back(otherWigigTxop);
        }
      }

      /**
       * Now, we notify all of these changes in one go. It is necessary to
       * perform first the calculations of which WigigTxops are colliding and
       * then only apply the changes because applying the changes through
       * notification could change the global state of the manager, and, thus,
       * could change the result of the calculations.
       */
      txop->NotifyAccessGranted();
      for (auto collidingWigigTxop : internalCollisionWigigTxops) {
        collidingWigigTxop->NotifyInternalCollision();
      }
      break;
    }
    i++;
  }
}

void WigigChannelAccessManager::AccessTimeout() {
  NS_LOG_FUNCTION(this);
  UpdateBackoff();
  DoGrantDcfAccess();
  DoRestartAccessTimeoutIfNeeded();
}

Time WigigChannelAccessManager::GetAccessGrantStart(bool ignoreNav) const {
  NS_LOG_FUNCTION(this);
  Time lastRxEnd = m_lastRxStart + m_lastRxDuration;
  Time rxAccessStart = lastRxEnd + m_sifs;
  if ((lastRxEnd <= Simulator::Now()) && !m_lastRxReceivedOk) {
    rxAccessStart += m_eifsNoDifs;
  }
  Time busyAccessStart = m_lastBusyStart + m_lastBusyDuration + m_sifs;
  Time txAccessStart = m_lastTxStart + m_lastTxDuration + m_sifs;
  Time navAccessStart = m_lastNavStart + m_lastNavDuration + m_sifs;
  Time ackTimeoutAccessStart = m_lastAckTimeoutEnd + m_sifs;
  Time ctsTimeoutAccessStart = m_lastCtsTimeoutEnd + m_sifs;
  Time switchingAccessStart =
      m_lastSwitchingStart + m_lastSwitchingDuration + m_sifs;
  Time accessGrantedStart;
  if (ignoreNav) {
    accessGrantedStart = MostRecent(
        {rxAccessStart, busyAccessStart, txAccessStart, ackTimeoutAccessStart,
         ctsTimeoutAccessStart, switchingAccessStart});
  } else {
    accessGrantedStart = MostRecent(
        {rxAccessStart, busyAccessStart, txAccessStart, navAccessStart,
         ackTimeoutAccessStart, ctsTimeoutAccessStart, switchingAccessStart});
  }
  NS_LOG_INFO("access grant start=" << accessGrantedStart
                                    << ", rx access start=" << rxAccessStart
                                    << ", busy access start=" << busyAccessStart
                                    << ", tx access start=" << txAccessStart
                                    << ", nav access start=" << navAccessStart);
  return accessGrantedStart;
}

Time WigigChannelAccessManager::GetBackoffStartFor(Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << txop);
  Time mostRecentEvent =
      MostRecent({txop->GetBackoffStart(),
                  GetAccessGrantStart() + (txop->GetAifsn() * m_slot)});
  NS_LOG_DEBUG("Backoff start: " << mostRecentEvent.As(Time::US));

  return mostRecentEvent;
}

Time WigigChannelAccessManager::GetBackoffEndFor(Ptr<WigigTxop> txop) {
  NS_LOG_FUNCTION(this << txop);
  Time backoffEnd =
      GetBackoffStartFor(txop) + (txop->GetBackoffSlots() * m_slot);
  NS_LOG_DEBUG("Backoff end: " << backoffEnd.As(Time::US));

  return backoffEnd;
}

void WigigChannelAccessManager::UpdateBackoff() {
  NS_LOG_FUNCTION(this);
  uint32_t k = 0;
  for (auto txop : m_txops) {
    Time backoffStart = GetBackoffStartFor(txop);
    if (backoffStart <= Simulator::Now()) {
      uint32_t nIntSlots =
          ((Simulator::Now() - backoffStart) / m_slot).GetHigh();
      /*
       * EDCA behaves slightly different to DCA. For EDCA we
       * decrement once at the slot boundary at the end of AIFS as
       * well as once at the end of each clear slot
       * thereafter. For DCA we only decrement at the end of each
       * clear slot after DIFS. We account for the extra backoff
       * by incrementing the slot count here in the case of
       * EDCA. The if statement whose body we are in has confirmed
       * that a minimum of AIFS has elapsed since last busy
       * medium.
       */
      if (txop->IsWigigQosTxop()) {
        nIntSlots++;
      }
      uint32_t n = std::min(nIntSlots, txop->GetBackoffSlots());
      NS_LOG_DEBUG("dcf " << k << " dec backoff slots=" << n);
      Time backoffUpdateBound = backoffStart + (n * m_slot);
      txop->UpdateBackoffSlotsNow(n, backoffUpdateBound);
    }
    ++k;
  }
}

void WigigChannelAccessManager::DoRestartAccessTimeoutIfNeeded() {
  NS_LOG_FUNCTION(this);
  /**
   * Is there a WigigTxop which needs to access the medium, and,
   * if there is one, how many slots for AIFS+backoff does it require ?
   */
  bool accessTimeoutNeeded = false;
  Time expectedBackoffEnd = Simulator::GetMaximumSimulationTime();
  for (auto txop : m_txops) {
    if (txop->IsAccessRequested()) {
      Time tmp = GetBackoffEndFor(txop);
      if (tmp > Simulator::Now()) {
        accessTimeoutNeeded = true;
        expectedBackoffEnd = std::min(expectedBackoffEnd, tmp);
      }
    }
  }
  NS_LOG_DEBUG("Access timeout needed: " << accessTimeoutNeeded);
  if (accessTimeoutNeeded) {
    NS_LOG_DEBUG("expected backoff end=" << expectedBackoffEnd);
    Time expectedBackoffDelay = expectedBackoffEnd - Simulator::Now();
    if (m_accessTimeout.IsRunning() &&
        Simulator::GetDelayLeft(m_accessTimeout) > expectedBackoffDelay) {
      m_accessTimeout.Cancel();
    }
    if (m_accessTimeout.IsExpired()) {
      m_accessTimeout =
          Simulator::Schedule(expectedBackoffDelay,
                              &WigigChannelAccessManager::AccessTimeout, this);
    }
  }
}

bool WigigChannelAccessManager::IsReceiving() const {
  return (m_lastRxStart + m_lastRxDuration > Simulator::Now());
}

void WigigChannelAccessManager::NotifyRxStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  NS_LOG_DEBUG("rx start for=" << duration);
  UpdateBackoff();
  m_lastRxStart = Simulator::Now();
  m_lastRxDuration = duration;
  m_lastRxReceivedOk = true;
}

void WigigChannelAccessManager::NotifyRxEndOkNow() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("rx end ok");
  m_lastRxDuration = Simulator::Now() - m_lastRxStart;
  m_lastRxReceivedOk = true;
}

void WigigChannelAccessManager::NotifyRxEndErrorNow() {
  NS_LOG_FUNCTION(this);
  NS_LOG_DEBUG("rx end error");
  Time now = Simulator::Now();
  Time lastRxEnd = m_lastRxStart + m_lastRxDuration;
  if (lastRxEnd > now) {
    m_lastBusyStart = now;
    m_lastBusyDuration = lastRxEnd - m_lastBusyStart;
  }
  m_lastRxDuration = now - m_lastRxStart;
  m_lastRxReceivedOk = false;
}

void WigigChannelAccessManager::NotifyTxStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  m_lastRxReceivedOk = true;
  Time now = Simulator::Now();
  Time lastRxEnd = m_lastRxStart + m_lastRxDuration;
  if (lastRxEnd > now) {
    // this may be caused only if PHY has started to receive a packet
    // inside MBIFS, so, we check that lastRxStart was maximum a MBIFS ago
    NS_ASSERT_MSG(now - m_lastRxStart <= m_sifs * 3,
                  "lastRxStart should be maximum a MBIFS ago");
    m_lastRxDuration = now - m_lastRxStart;
  }
  NS_LOG_DEBUG("tx start for " << duration);
  UpdateBackoff();
  m_lastTxStart = now;
  m_lastTxDuration = duration;
}

void WigigChannelAccessManager::NotifyCcaBusyStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  NS_LOG_DEBUG("busy start for " << duration);
  UpdateBackoff();
  m_lastBusyStart = Simulator::Now();
  m_lastBusyDuration = duration;
}

void WigigChannelAccessManager::NotifySwitchingStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  Time now = Simulator::Now();
  NS_ASSERT(m_lastTxStart + m_lastTxDuration <= now);
  NS_ASSERT(m_lastSwitchingStart + m_lastSwitchingDuration <= now);

  m_lastRxReceivedOk = true;

  if (m_lastRxStart + m_lastRxDuration > now) {
    // channel switching during packet reception
    m_lastRxDuration = now - m_lastRxStart;
  }
  if (m_lastNavStart + m_lastNavDuration > now) {
    m_lastNavDuration = now - m_lastNavStart;
  }
  if (m_lastBusyStart + m_lastBusyDuration > now) {
    m_lastBusyDuration = now - m_lastBusyStart;
  }
  if (m_lastAckTimeoutEnd > now) {
    m_lastAckTimeoutEnd = now;
  }
  if (m_lastCtsTimeoutEnd > now) {
    m_lastCtsTimeoutEnd = now;
  }

  // Cancel timeout
  if (m_accessTimeout.IsRunning()) {
    m_accessTimeout.Cancel();
  }

  // Reset backoffs
  for (auto txop : m_txops) {
    uint32_t remainingSlots = txop->GetBackoffSlots();
    if (remainingSlots > 0) {
      txop->UpdateBackoffSlotsNow(remainingSlots, now);
      NS_ASSERT(txop->GetBackoffSlots() == 0);
    }
    txop->ResetCw();
    txop->m_accessRequested = false;
    txop->NotifyChannelSwitching();
  }

  NS_LOG_DEBUG("switching start for " << duration);
  m_lastSwitchingStart = Simulator::Now();
  m_lastSwitchingDuration = duration;
}

void WigigChannelAccessManager::NotifySleepNow() {
  NS_LOG_FUNCTION(this);
  m_sleeping = true;
  // Cancel timeout
  if (m_accessTimeout.IsRunning()) {
    m_accessTimeout.Cancel();
  }

  // Reset backoffs
  for (auto txop : m_txops) {
    txop->NotifySleep();
  }
}

void WigigChannelAccessManager::NotifyOffNow() {
  NS_LOG_FUNCTION(this);
  m_off = true;
  // Cancel timeout
  if (m_accessTimeout.IsRunning()) {
    m_accessTimeout.Cancel();
  }

  // Reset backoffs
  for (auto txop : m_txops) {
    txop->NotifyOff();
  }
}

void WigigChannelAccessManager::NotifyWakeupNow() {
  NS_LOG_FUNCTION(this);
  m_sleeping = false;
  for (auto txop : m_txops) {
    uint32_t remainingSlots = txop->GetBackoffSlots();
    if (remainingSlots > 0) {
      txop->UpdateBackoffSlotsNow(remainingSlots, Simulator::Now());
      NS_ASSERT(txop->GetBackoffSlots() == 0);
    }
    txop->ResetCw();
    txop->m_accessRequested = false;
    txop->NotifyWakeUp();
  }
}

void WigigChannelAccessManager::NotifyOnNow() {
  NS_LOG_FUNCTION(this);
  m_off = false;
  for (auto txop : m_txops) {
    uint32_t remainingSlots = txop->GetBackoffSlots();
    if (remainingSlots > 0) {
      txop->UpdateBackoffSlotsNow(remainingSlots, Simulator::Now());
      NS_ASSERT(txop->GetBackoffSlots() == 0);
    }
    txop->ResetCw();
    txop->m_accessRequested = false;
    txop->NotifyOn();
  }
}

void WigigChannelAccessManager::NotifyNavResetNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  NS_LOG_DEBUG("nav reset for=" << duration);
  UpdateBackoff();
  m_lastNavStart = Simulator::Now();
  m_lastNavDuration = duration;
  /**
   * If the NAV reset indicates an end-of-NAV which is earlier
   * than the previous end-of-NAV, the expected end of backoff
   * might be later than previously thought so, we might need
   * to restart a new access timeout.
   */
  DoRestartAccessTimeoutIfNeeded();
}

void WigigChannelAccessManager::NotifyNavStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  NS_ASSERT(m_lastNavStart <= Simulator::Now());
  NS_LOG_DEBUG("nav start for=" << duration);
  UpdateBackoff();
  Time newNavEnd = Simulator::Now() + duration;
  Time lastNavEnd = m_lastNavStart + m_lastNavDuration;
  if (newNavEnd > lastNavEnd) {
    m_lastNavStart = Simulator::Now();
    m_lastNavDuration = duration;
  }
}

void WigigChannelAccessManager::NotifyAckTimeoutStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  NS_ASSERT(m_lastAckTimeoutEnd < Simulator::Now());
  m_lastAckTimeoutEnd = Simulator::Now() + duration;
}

void WigigChannelAccessManager::NotifyAckTimeoutResetNow() {
  NS_LOG_FUNCTION(this);
  m_lastAckTimeoutEnd = Simulator::Now();
  DoRestartAccessTimeoutIfNeeded();
}

void WigigChannelAccessManager::NotifyCtsTimeoutStartNow(Time duration) {
  NS_LOG_FUNCTION(this << duration);
  m_lastCtsTimeoutEnd = Simulator::Now() + duration;
}

void WigigChannelAccessManager::NotifyCtsTimeoutResetNow() {
  NS_LOG_FUNCTION(this);
  m_lastCtsTimeoutEnd = Simulator::Now();
  DoRestartAccessTimeoutIfNeeded();
}

} // namespace ns3
