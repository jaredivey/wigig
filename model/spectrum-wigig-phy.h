/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 *          Ghada Badawy <gbadawy@gmail.com>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 *
 * Ported from yans-wifi-phy.h by several contributors starting
 * with Nicola Baldo and Dean Armstrong
 */

#ifndef SPECTRUM_WIGIG_PHY_H
#define SPECTRUM_WIGIG_PHY_H

#include "codebook-parametric.h"
#include "wigig-phy.h"

#include "ns3/antenna-model.h"
#include "ns3/spectrum-channel.h"

namespace ns3 {

class WigigSpectrumPhyInterface;
class WigigPpdu;

/**
 * \ingroup wigig
 *
 * Signal parameters for DMG WiFi.
 */
class WigigSpectrumSignalParameters : public SpectrumSignalParameters {
public:
  // inherited from SpectrumSignalParameters
  Ptr<SpectrumSignalParameters> Copy() const override;

  /**
   * default constructor
   */
  WigigSpectrumSignalParameters();
  /**
   * copy constructor
   * \param p the object to copy from.
   */
  WigigSpectrumSignalParameters(const WigigSpectrumSignalParameters &p);

  /**
   * The packet being transmitted with this signal
   */
  Ptr<WigigPpdu> ppdu;
  /**
   * The type of the PLCP.
   */
  PlcpFieldType plcpFieldType;
  /**
   * TxVector companioned by this transmission.
   */
  WigigTxVector txVector;
  /**
   * Current active transmit antenna array ID.
   */
  AntennaId antennaId;
  /**
   * Pointer to the active Tx pattern configuration.
   */
  Ptr<PatternConfig> txPatternConfig;
};

/**
 * \brief 802.11 PHY layer model
 * \ingroup wigig
 *
 * This PHY implements a spectrum-aware enhancement of the 802.11
 * SpectrumWigigPhy model.
 *
 * This PHY model depends on a channel loss and delay
 * model as provided by the ns3::SpectrumPropagationLossModel
 * and ns3::PropagationDelayModel classes.
 *
 */
class SpectrumWigigPhy : public WigigPhy {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  SpectrumWigigPhy();
  ~SpectrumWigigPhy() override;

  void StartTx(Ptr<WigigPpdu> ppdu) override;
  Ptr<Channel> GetChannel() const override;

  /**
   * Set the SpectrumChannel this SpectrumWigigPhy is to be connected to.
   *
   * \param channel the SpectrumChannel this SpectrumWigigPhy is to be connected
   * to
   */
  void SetChannel(const Ptr<SpectrumChannel> channel);

  /**
   * Input method for delivering a signal from the spectrum channel
   * and low-level PHY interface to this SpectrumWigigPhy instance.
   *
   * \param rxParams Input signal parameters
   */
  void StartRx(Ptr<SpectrumSignalParameters> rxParams);

  /**
   * Get the center frequency of the channel corresponding the current TxVector
   * rather than that of the supported channel width. Consider that this
   * "primary channel" is on the lower part for the time being.
   *
   * \param txVector the TXVECTOR that has the channel width that is to be used
   * \return the center frequency in MHz corresponding to the channel width to
   * be used
   */
  uint16_t
  GetCenterFrequencyForChannelWidth(const WigigTxVector &txVector) const;

  /**
   * Method to encapsulate the creation of the WigigSpectrumPhyInterface
   * object (used to bind the SpectrumWigigPhy to a SpectrumChannel) and
   * to link it to this SpectrumWigigPhy instance
   *
   * \param device pointer to the NetDevice object including this new object
   */
  void CreateWifiSpectrumPhyInterface(Ptr<NetDevice> device);
  /**
   * \return pointer to WifiSpectrumPhyInterfaceCopy associated with this Phy
   */
  Ptr<WigigSpectrumPhyInterface> GetSpectrumPhy() const;
  /**
   * \return the SpectrumModel that this SpectrumPhy expects to be used
   *         for all SpectrumValues that are passed to StartRx. If 0 is
   *         returned, it means that any model will be accepted.
   */
  Ptr<const SpectrumModel> GetRxSpectrumModel() const;

  /**
   * \return the width of the guard band (MHz)
   *
   * Note: in order to properly model out of band transmissions for OFDM, the
   * guard band has been configured so as to expand the modeled spectrum up to
   * the outermost referenced point in "Transmit spectrum mask" sections' PSDs
   * of each PHY specification of 802.11-2016 standard. It thus ultimately
   * corresponds to the current channel bandwidth (which can be different from
   * devices max channel width).
   */
  uint16_t GetGuardBandwidth() const;

  /**
   * Callback invoked when the PHY model starts to process a signal
   *
   * \param signalType Whether signal is WiFi (true) or foreign (false)
   * \param senderNodeId Node Id of the sender of the signal
   * \param rxPower received signal power (dBm)
   * \param duration Signal duration
   */
  typedef void (*SignalArrivalCallback)(bool signalType, uint32_t senderNodeId,
                                        double rxPower, Time duration);

  // The following four methods call to the base WigigPhy class method
  // but also generate a new SpectrumModel if called during runtime
  void SetChannelNumber(uint8_t id) override;
  void SetFrequency(uint16_t freq) override;
  void SetChannelWidth(uint16_t channelwidth) override;
  void ConfigureStandard(WifiStandard standard) override;

protected:
  void DoDispose() override;
  void DoInitialize() override;

  /**
   * \param txVector the TXVECTOR that has tx parameters such as mode, the
   * transmission mode to use to send this packet, and txPowerLevel, a power
   * level to use to send this packet. The real transmission power is calculated
   * as txPowerMin + txPowerLevel * (txPowerMax - txPowerMin) / nTxLevels \param
   * fieldType The type of the PLCP field. \param txDuration duration of the
   * transmission.
   */
  void TxSubfield(const WigigTxVector &txVector, PlcpFieldType fieldType,
                  Time txDuration);
  /**
   * Start AGC Subfield transmission..
   * \param txVector TxVector companioned by this transmission.
   */
  void StartAgcSubfieldTx(const WigigTxVector &txVector) override;
  /**
   * Start TRN-CE Subfield transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  void StartCeSubfieldTx(const WigigTxVector &txVector) override;
  /**
   * Start TRN-SF transmission.
   * \param txVector TxVector companioned by this transmission.
   */
  void StartTrnSubfieldTx(const WigigTxVector &txVector) override;

private:
  /**
   * \param centerFrequency center frequency (MHz)
   * \param channelWidth channel width (MHz) of the channel for the current
   * transmission \param txPowerW power in W to spread across the bands \param
   * modulationClass the modulation class \return Pointer to SpectrumValue
   *
   * This is a helper function to create the right TX PSD corresponding
   * to the standard in use.
   */
  Ptr<SpectrumValue>
  GetTxPowerSpectralDensity(uint16_t centerFrequency, uint16_t channelWidth,
                            double txPowerW,
                            WifiModulationClass modulationClass) const;

  /**
   * Perform run-time spectrum model change
   */
  void ResetSpectrumModel();
  /**
   * Filter a signal
   * \param filter
   * \param receivedSignalPsd
   * \return
   */
  double FilterSignal(Ptr<SpectrumValue> filter,
                      Ptr<SpectrumValue> receivedSignalPsd);

  Ptr<SpectrumChannel>
      m_channel; //!< SpectrumChannel that this SpectrumWigigPhy is connected to

  Ptr<WigigSpectrumPhyInterface>
      m_wifiSpectrumPhyInterface; //!< Spectrum PHY interface
  mutable Ptr<const SpectrumModel>
      m_rxSpectrumModel; //!< receive spectrum model
  bool
      m_disableWifiReception; //!< forces this PHY to fail to sync on any signal
  TracedCallback<bool, uint32_t, double, Time> m_signalCb; //!< Signal callback
};

} // namespace ns3

#endif /* SPECTRUM_WIGIG_PHY_H */
