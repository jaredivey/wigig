/*
 * Copyright (c) 2019 Orange Labs
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Rediet <getachew.redieteab@orange.com>
 */

#ifndef WIGIG_PPDU_H
#define WIGIG_PPDU_H

#include "wigig-phy-header.h"
#include "wigig-tx-vector.h"

#include "ns3/nstime.h"

#include <list>

namespace ns3 {

class WigigPsdu;

/**
 * \ingroup wigig
 *
 * WigigPpdu stores a preamble, a modulation class, PHY headers and a PSDU.
 * This class should be extended later on to handle MU PPDUs.
 */
class WigigPpdu : public SimpleRefCount<WigigPpdu> {
public:
  /**
   * Create a single user PPDU storing a PSDU.
   *
   * \param psdu the PHY payload (PSDU)
   * \param txVector the TXVECTOR that was used for this PPDU
   * \param ppduDuration the transmission duration of this PPDU
   * \param frequency the frequency used for the transmission of this PPDU
   */
  WigigPpdu(Ptr<const WigigPsdu> psdu, const WigigTxVector &txVector,
            Time ppduDuration, uint16_t frequency);

  virtual ~WigigPpdu();

  /**
   * Get the TXVECTOR used to send the PPDU.
   * \return the TXVECTOR of the PPDU.
   */
  WigigTxVector GetTxVector() const;
  /**
   * Get the payload of the PPDU.
   * \return the PSDU
   */
  Ptr<const WigigPsdu> GetPsdu() const;
  /**
   * Return true if the PPDU's transmission was aborted due to transmitter
   * switch off \return true if the PPDU's transmission was aborted due to
   * transmitter switch off
   */
  bool IsTruncatedTx() const;
  /**
   * Indicate that the PPDU's transmission was aborted due to transmitter switch
   * off.
   */
  void SetTruncatedTx();
  /**
   * Get the total transmission duration of the PPDU.
   * \return the transmission duration of the PPDU
   */
  Time GetTxDuration() const;

  /**
   * \brief Print the PPDU contents.
   * \param os output stream in which the data should be printed.
   */
  void Print(std::ostream &os) const;

private:
  DmgControlHeader
      m_dmgCtrlHeader;           //!< The IEEE 802.11ad DMG PHY Control header.
  DmgScHeader m_dmgScHeader;     //!< The IEEE 802.11ad DMG PHY SC header.
  DmgOfdmHeader m_dmgOfdmHeader; //!< The IEEE 802.11ad DMG PHY OFDM header.

  WifiPreamble m_preamble; //!< the PHY preamble
  WifiModulationClass
      m_modulation; //!< the modulation used for the transmission of this PPDU
  Ptr<const WigigPsdu> m_psdu; //!< the PSDU contained in this PPDU
  bool m_truncatedTx; //!< flag indicating whether the frame's transmission was
                      //!< aborted due to transmitter switch off
  uint16_t m_frequency; //!< the frequency used to transmit that PPDU in MHz
  uint16_t
      m_channelWidth; //!< the channel width used to transmit that PPDU in MHz
  uint8_t m_txPowerLevel; //!< the transmission power level (used only for TX
                          //!< and initializing the returned WigigTxVector)
};

/**
 * \brief Stream insertion operator.
 *
 * \param os the stream
 * \param ppdu the PPDU
 * \returns a reference to the stream
 */
std::ostream &operator<<(std::ostream &os, const WigigPpdu &ppdu);

} // namespace ns3

#endif /* WIGIG_PPDU_H */
