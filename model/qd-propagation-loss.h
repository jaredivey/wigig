/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef QD_PROPAGATION_LOSS_MODEL_H
#define QD_PROPAGATION_LOSS_MODEL_H

#include <ns3/spectrum-propagation-loss-model.h>

namespace ns3 {

class QdPropagationEngine;
class MobilityModel;

struct SpectrumSignalParameters;

class QdPropagationLossModel : public SpectrumPropagationLossModel {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId();

  QdPropagationLossModel();
  ~QdPropagationLossModel() override;
  /**
   * Class constructor
   * \param qdPropagationEngine Pointer to the Q-D Propagation Engine class.
   */
  QdPropagationLossModel(Ptr<QdPropagationEngine> qdPropagationEngine);

private:
  Ptr<SpectrumValue>
  DoCalcRxPowerSpectralDensity(Ptr<const SpectrumSignalParameters> params,
                               Ptr<const MobilityModel> a,
                               Ptr<const MobilityModel> b) const override;

  Ptr<QdPropagationEngine> m_qdPropagationEngine;
};

} // namespace ns3

#endif /* QD_PROPAGATION_LOSS_MODEL_H */
