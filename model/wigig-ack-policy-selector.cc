/*
 * Copyright (c) 2019 Universita' degli Studi di Napoli Federico II
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Stefano Avallone <stavallo@unina.it>
 */

#include "wigig-ack-policy-selector.h"

#include "ns3/log.h"

#include <set>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigAckPolicySelector");

NS_OBJECT_ENSURE_REGISTERED(WigigAckPolicySelector);

TypeId WigigAckPolicySelector::GetTypeId() {
  static TypeId tid = TypeId("ns3::WigigAckPolicySelector")
                          .SetParent<Object>()
                          .SetGroupName("Wigig");
  return tid;
}

WigigAckPolicySelector::~WigigAckPolicySelector() { NS_LOG_FUNCTION(this); }

void WigigAckPolicySelector::DoDispose() {
  NS_LOG_FUNCTION(this);
  m_qosTxop = nullptr;
}

void WigigAckPolicySelector::SetQosTxop(Ptr<WigigQosTxop> qosWigigTxop) {
  NS_LOG_FUNCTION(this << qosWigigTxop);
  m_qosTxop = qosWigigTxop;
}

void WigigAckPolicySelector::SetAckPolicy(
    Ptr<WigigPsdu> psdu, const MacLowTransmissionParameters &params) {
  NS_LOG_FUNCTION(psdu << params);

  std::set<uint8_t> tids = psdu->GetTids();
  NS_ASSERT(tids.size() == 1);
  uint8_t tid = *tids.begin();

  if (params.MustWaitNormalAck() || params.MustWaitBlockAck()) {
    // Normal Ack or Implicit Block Ack Request policy
    psdu->SetAckPolicyForTid(tid, WigigMacHeader::NORMAL_ACK);
  } else {
    // Block Ack policy
    psdu->SetAckPolicyForTid(tid, WigigMacHeader::BLOCK_ACK);
  }
}

} // namespace ns3
