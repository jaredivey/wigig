/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef FIELDS_HEADERS_H
#define FIELDS_HEADERS_H

#include "wigig-data-types.h"

#include "ns3/header.h"
#include "ns3/mac48-address.h"

namespace ns3 {

/***********************************
 *    Sector Sweep Field (8.4a.1)
 ***********************************/

enum BeamformingDirection {
  BeamformingInitiator = 0,
  BeamformingResponder = 1
};

/**
 * \ingroup wigig
 * \brief Sector Sweep (SSW) Field.
 */
class DmgSswField : public ObjectBase {
public:
  DmgSswField();
  ~DmgSswField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the direction field in the SSW field.
   * \param dir 0 = the frame is transmiited by the beamforming initiator.
   *       1 = the frame is transmiited by the beamforming responder.
   */
  void SetDirection(BeamformingDirection dir);
  /**
   * Set the Count Down (CDOWN) field. the number of remaining DMG beacon frame
   * transmissions to the end of TXSS, or the number of the remaining SSW frame
   * transmissions to the end of the TXSS/RXSS. \param cdown This field is set
   * to 0 in the last frame DMG Beacon and SSW frame transmission. Possible
   * values range from 0 to 511.
   */
  void SetCountDown(uint16_t cdown);
  /**
   * Set Sector ID (SID).
   * \param sid indicates the sector number through which the frame containing
   * this SSW field is transmitted.
   */
  void SetSectorId(uint8_t sid);
  /**
   * Set the DMG Antenna ID.
   * \param antenna_id the DMG antenna the transmitter is currently using for
   * this transmission.
   */
  void SetDmgAntennaId(uint8_t antenna_id);
  /**
   * Set the Receive Sector Sweep (RXSS) Length.
   * \param length the length of a receive sector sweep as required by the
   * transmitting STA. It is defined in units of a SSW frame
   */
  void SetRxssLength(uint8_t length);

  /**
   * Get the direction field in the SSW field.
   *
   * \return Whether the frame is transmitted by Beamforming Initiator or
   * Responder.
   */
  BeamformingDirection GetDirection() const;
  /**
   * Get the Count Down (CDOWN) field value.
   * \return the number of remaining DMG beacon frame transmissions to the end
   * of TXSS, or the number of the remaining SSW frame transmissions to the end
   * of the TXSS/RXSS.
   */
  uint16_t GetCountDown() const;
  /**
   * Get Sector ID (SID) value.
   * \return the sector number through which the frame containing this SSW field
   * is transmitted.
   */
  uint8_t GetSectorId() const;
  /**
   * Get the DMG Antenna ID value.
   * \return the DMG antenna the transmitter is currently using for this
   * transmission.
   */
  uint8_t GetDmgAntennaId() const;
  /**
   * Get the RXSS Length.
   * \return the length of a receive sector sweep as required by the
   * transmitting STA. It is defined in units of a SSW frame
   */
  uint8_t GetRxssLength() const;

private:
  BeamformingDirection m_dir;
  uint16_t m_cdown;
  uint8_t m_sid;
  uint8_t m_antenna_id;
  uint8_t m_length;
};

/****************************************
 * Dynamic Allocation Info Field (8.4a.2)
 ****************************************/

/**
 * \ingroup wigig
 * \brief Dynamic Allocation Information Field.
 */
class DynamicAllocationInfoField : public ObjectBase {
public:
  DynamicAllocationInfoField();
  ~DynamicAllocationInfoField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the TID field that identifies the TC or TS for the allocation request
   * or grant. \param tid The value of the Traffic Identifier field.
   */
  void SetTid(uint8_t tid);
  /**
   * Set the allocation type field.
   * \param
   */
  void SetAllocationType(AllocationType value);
  /**
   * Set source AID.
   * \param
   */
  void SetSourceAid(uint8_t aid);
  /**
   * Setd destination AID.
   * \param
   */
  void SetDestinationAid(uint8_t aid);
  /**
   * Set the furation of the current allocation .
   * \param
   */
  void SetAllocationDuration(uint16_t duration);

  uint8_t GetTid() const;
  AllocationType GetAllocationType() const;
  uint8_t GetSourceAid() const;
  uint8_t GetDestinationAid() const;
  uint16_t GetAllocationDuration() const;

private:
  uint8_t m_tid;
  AllocationType m_allocationType;
  uint8_t m_sourceAID;
  uint8_t m_destinationAID;
  uint16_t m_allocationDuration;
};

/*******************************************
 *    Sector Sweep Feedback Field (8.4a.3)
 *******************************************/

/**
 * \ingroup wigig
 * \brief Sector Sweep Feedback Field.
 */
class DmgSswFbckField : public ObjectBase {
public:
  static TypeId GetTypeId();
  DmgSswFbckField();
  ~DmgSswFbckField() override;

  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the total number of sectors the initiator uses in the ISS or the ID of
   * the frame that was received with best quality in the preceding sector
   * sweep. \param value the total number of sectors the initiator uses in the
   * ISS or the ID of the best sector.
   */
  void SetSector(uint16_t value);
  /**
   * Set the number of RX DMG Antennas in ISS or Select DMG Antenna in case
   * \param antennas the number of RX DMG antennas the initiator uses during the
   * following RSS.
   */
  void SetDmgAntenna(uint8_t antennas);
  /**
   * Set the SNR Report if not ISS or the reserved value in the case of ISS.
   * \param value the SNR value is measured in linear scale.
   */
  void SetSnrReport(double value);
  /**
   * Set whether a non-PCP/non-AP STA requires the PCP/AP to initiate the
   * communication. \param value
   */
  void SetPollRequired(bool value);
  /**
   * Set whether the SSW Feedback Field is transmitted as part of ISS.
   * \param value True if the SSW Feedback Field is transmitted as part of ISS,
   * otherwise false.
   */
  void IsPartOfIss(bool value);

  uint16_t GetSector() const;
  uint8_t GetDmgAntenna() const;
  /**
   * Return the SNR Report if not ISS or the reserved value in the case of ISS.
   * \return The SNR value is measured in dB.
   */
  double GetSnrReport() const;
  bool GetPollRequired() const;

private:
  uint16_t m_sectors;
  uint8_t m_antennas;
  int8_t m_snrReport;
  bool m_pollRequired;
  bool m_iss;
};

/***********************************
 *    BRP Request Field (8.4a.4).
 ***********************************/

/**
 * \ingroup wigig
 * \brief Beam Refinement Protocol Request Field.
 */
class BrpRequestField : public ObjectBase {
public:
  BrpRequestField();
  ~BrpRequestField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  void SetLRx(uint8_t value);
  /**
   * \param value The TX-TRN-REQ field is set to 1 to indicate that the STA
   * needs transmit training as part of beam refinement. Otherwise, it is set to
   * 0.
   */
  void SetTxTrnReq(bool value);
  /**
   * If the MID-REQ field is set to 0, the L-RX field indicates the compressed
   * number of TRN-R subfields requested by the transmitting STA as part of beam
   * refinement. To obtain the desired number of TRN-R subfields, the value of
   * the L-RX field is multiplied by 4. Possible values range from 0 to 16,
   * corresponding to 0 to 64 TRN-R fields. Other values are reserved. If the
   * field is set to 0, the transmitting STA does not need receiver training as
   * part of beam refinement. If the MID-REQ field is set to 1, the L-RX field
   * indicates the compressed number of AWV settings that the STA uses during
   * the MID phase. To obtain the number of AWVs that is used, the value of the
   * L-RX field is multiplied by 4.
   */
  void SetMidReq(bool value);
  /**
   * A STA sets the BC-REQ field to 1 in SSW-Feedback or BRP frames to indicate
   * a request for an I/R-BC subphase; otherwise, the STA sets the field to 0 to
   * indicate it is not requesting an I/R-BC subphase. In case an R-BC subphase
   * is requested, the STA can include information on the TX sector IDs to be
   * used by the STA receiving this request. The STA receiving this request sets
   * the BC-grant field in SSW-ACK or BRP frames to 1 to grant this request;
   * otherwise, the STA sets it to 0 to reject the request.
   */
  void SetBcReq(bool value);
  void SetMidGrant(bool value);
  void SetBcGrant(bool value);
  void SetChannelFbckCap(bool value);
  void SetTxSectorId(uint8_t value);
  void SetOtherAid(uint8_t value);
  void SetTxAntennaId(uint8_t value);

  uint8_t GetLRx() const;
  bool GetTxTrnReq() const;
  bool GetMidReq() const;
  bool GetBcReq() const;
  bool GetMidGrant() const;
  bool GetBcGrant() const;
  bool GetChannelFbckCap() const;
  uint8_t GetTxSectorId() const;
  uint8_t GetOtherAid() const;
  uint8_t GetTxAntennaId() const;

private:
  uint8_t m_lRx;
  bool m_txTrnReq;
  bool m_midReq;
  bool m_bcReq;
  bool m_midGrant;
  bool m_bcGrant;
  bool m_channelFbckCap;
  uint8_t m_txSectorId;
  uint8_t m_otherAid;
  uint8_t m_txAntennaId;
};

/***************************************
 * Beamforming Control Field (8.4a.5)
 ***************************************/

/**
 * \ingroup wigig
 * \brief The Beamforming Control Field.
 */
class BfControlField : public ObjectBase {
public:
  BfControlField();
  ~BfControlField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  void SetBeamformTraining(bool value);
  void SetAsInitiatorTXSS(bool value);
  void SetAsResponderTxss(bool value);

  void SetTotalNumberOfSectors(uint8_t sectors);
  void SetNumberOfRxDmgAntennas(uint8_t antennas);

  void SetRxssLength(uint8_t length);
  void SetRxssTxRate(bool rate);

  bool IsBeamformTraining() const;
  bool IsInitiatorTxss() const;
  bool IsResponderTxss() const;

  uint8_t GetTotalNumberOfSectors() const;
  uint8_t GetNumberOfRxDmgAntennas() const;

  uint8_t GetRxssLength() const;
  bool GetRxssTxRate() const;

private:
  bool m_beamformTraining;
  bool m_isInitiatorTxss;
  bool m_isResponderTxss;

  /* BF Control Fields when both IsInitiatorTxss and IsResponderTxss subfields
   * are equal to 1 and the BF Control field is transmitted in Grant or Grant
   * ACK frames */
  uint8_t m_sectors;
  uint8_t m_antennas;

  /* BF Control field format in all other cases */
  uint8_t m_rxssLength;
  bool m_rxssTXRate;
};

/***************************************
 * Beamformed Link Maintenance (8.4a.6)
 ***************************************/

enum BeamLinkMaintenanceUnitIndex { UNIT_32US = 0, UNIT_2000US = 1 };

/**
 * \ingroup wigig
 * \brief The Beamformed Link Maintenance provides the DMG STA with the value of
 * dot11BeamLinkMaintenanceTime.
 */
class BfLinkMaintenanceField : public ObjectBase {
public:
  BfLinkMaintenanceField();
  ~BfLinkMaintenanceField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  /**
   * Set the encoding of the BeamLink Maintenance Unit Index.
   * \param index The encoding of the BeamLink Maintenance Unit Index.
   */
  void SetUnitIndex(BeamLinkMaintenanceUnitIndex index);
  /**
   * Set the number of RX DMG Antennas in ISS or Select DMG Antenna in case
   * \param value The number of RX DMG antennas the initiator uses during the
   * following RSS.
   */
  void SetMaintenanceValue(uint8_t value);
  /**
   * Set the BeamLink isMaster field is set to 1 to indicate that the DMG STA is
   * the master of the data transfer and set to 0 if the DMG STA is a slave of
   * the data transfer. \param value Indicate whether the DMG STA is the master
   * or not.
   */
  void SetAsMaster(bool value);

  BeamLinkMaintenanceUnitIndex GetUnitIndex() const;
  uint8_t GetMaintenanceValue() const;
  bool IsMaster() const;

private:
  BeamLinkMaintenanceUnitIndex m_unitIndex;
  uint8_t m_value;
  bool m_isMaster;
};

/******************************************
 * Beacon Clustering Control Field (8-34c&d)
 *******************************************/

enum ClusterMemberRole {
  NOT_PARTICIPATING = 0,
  SYNC_PCP_AP = 1,
  PARTICIPATING = 2
};

/**
 * \ingroup wigig
 * Implement the header for Clustering Control Field.
 */
class ExtDmgClusteringControlField : public ObjectBase {
public:
  ExtDmgClusteringControlField();
  ~ExtDmgClusteringControlField() override;

  static TypeId GetTypeId();
  TypeId GetInstanceTypeId() const override;
  void Print(std::ostream &os) const;
  uint32_t GetSerializedSize() const;
  Buffer::Iterator Serialize(Buffer::Iterator start) const;
  Buffer::Iterator Deserialize(Buffer::Iterator start);

  void SetDiscoveryMode(bool value);
  bool GetDiscoveryMode() const;

  /** Clustering Control field format if the Discovery Mode is False **/

  /**
   * If ECPAC Policy Enforced field is set to 0, the Beacon SP Duration subfield
   * indicates the duration, in units of 8 μs, of the Beacon SPs in the cluster.
   * If ECPAC Policy Enforced field is set to 1, the Beacon SP Duration subfield
   * indicates the maximum duration, in units of 8 μs, of the beacon header
   * interval (BHI) of the BSS, and the minimum duration of Beacon SPs in the
   * cluster (see 9.34.2.2). \param duration
   */
  void SetBeaconSpDuration(uint8_t duration);
  /**
   * The cluster to which the transmitter of the Clustering Control field
   * belongs is identified by the Cluster ID subfield. The MAC address of the
   * synchronization PCP (S-PCP)/S-AP is the Cluster ID of the cluster. \param
   * clusterID
   */
  void SetClusterId(Mac48Address clusterID);
  /**
   * The Cluster Member Role subfield identifies the role that the transmitting
   * STA assumes within the cluster. A value of 0 means that the STA is
   * currently not participating in clustering. A value of 1 means that the STA
   * acts as the S-PCP/S-AP of the cluster. A value of 2 means that the STA
   * participates in the cluster, but not as the S-PCP/S-AP. The value 3 is
   * reserved. \param role
   */
  void SetClusterMemberRole(ClusterMemberRole role);
  /**
   * The ClusterMaxMem subfield defines the maximum number of PCPs and/or APs,
   * including the S-PCP/S-AP, that can participate in the cluster. The value of
   * the ClusterMaxMem subfield is computed in relation to the beacon interval
   * value (9.34.2). The value 0 is reserved. Values 8 and above are reserved if
   * the ECPAC Policy Enforced field is set to 0. The value 1 is assigned to the
   * S-PCP/S-AP. \param max
   */
  void SetClusterMaxMem(uint8_t max);

  uint8_t GetBeaconSpDuration() const;
  Mac48Address GetClusterId() const;
  ClusterMemberRole GetClusterMemberRole() const;
  uint8_t GetClusterMaxMem() const;

  /** Clustering Control field format if the Discovery Mode is True **/

  /**
   * The A-BFT Responder Address subfield contains the MAC address of the STA
   * that is allowed to transmit during the A-BFT, if present, that follows the
   * BTI. \param address The MAC address of the A-BFT Responder Address.
   */
  void SetAbftResponderAddress(Mac48Address address);

  Mac48Address GetAbftResponderAddress() const;

private:
  bool m_discoveryMode;
  /* With Discovery Mode Disabled */
  uint8_t m_beaconSpDuration;
  Mac48Address m_clusterId;
  uint8_t m_clusterMemberRole;
  uint8_t m_clusterMaxMem;

  /* With Discovery Mode Enabled */
  Mac48Address m_responderAddress;
};

} // namespace ns3

#endif /* FIELDS_HEADERS_H */
