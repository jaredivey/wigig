/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_HELPER_H
#define WIGIG_HELPER_H

#include "wigig-mac-helper.h"

#include "ns3/qos-utils.h"
#include "ns3/trace-helper.h"
#include "ns3/wigig-channel.h"

#include <functional>

namespace ns3 {

class SpectrumChannel;
class RadiotapHeader;

/**
 * An enumeration of the ASCII trace types.
 */
enum SupportedAsciiTraceTypes {
  ASCII_TRACE_LEGACY = 0,
  ASCII_TRACE_PHY_ACTIVITY = 1,
};

/**
 * \brief manage and create wifi channel objects for the IEEE 802.11ad/ay
 * models.
 *
 * The intent of this class is to make it easy to create a channel object
 * which implements the Dmg channel model. The Dmg channel model is described
 * in "Yet Another Network Simulator", http://cutebugs.net/files/wns2-Dmg.pdf
 */
class WigigChannelHelper {
public:
  /**
   * Create a channel helper without any parameter set. The user must set
   * them all to be able to call Create later.
   */
  WigigChannelHelper();

  /**
   * Create a channel helper in a default working state. By default, we create
   * a channel model with a propagation delay equal to a constant, the speed of
   * light, and a propagation loss based on a log distance model with a
   * reference loss of 46.6777 dB at reference distance of 1m. \returns
   * WigigChannelHelper
   */
  static WigigChannelHelper Default();

  /**
   * \param name the name of the model to add
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Add a propagation loss model to the set of currently-configured loss
   * models. This method is additive to allow you to construct complex
   * propagation loss models such as a log distance + jakes model, etc.
   *
   * The order in which PropagationLossModels are added may be significant. Some
   * propagation models are dependent of the "txPower" (eg. Nakagami model), and
   * are therefore not commutative. The final receive power (excluding receiver
   * gains) are calculated in the order the models are added.
   */
  void AddPropagationLoss(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());
  /**
   * \param name the name of the model to set
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Configure a propagation delay for this channel.
   */
  void SetPropagationDelay(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());

  /**
   * \returns a new channel
   *
   * Create a channel based on the configuration parameters set previously.
   */
  Ptr<WigigChannel> Create() const;

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by the channel.  Typically this corresponds to random variables
   * used in the propagation loss models.  Return the number of streams
   * (possibly zero) that have been assigned.
   *
   * \param c NetDeviceContainer of the set of net devices for which the
   *          WifiNetDevice should be modified to use fixed streams
   * \param stream first stream index to use
   *
   * \return the number of stream indices assigned by this helper
   */
  int64_t AssignStreams(Ptr<WigigChannel> c, int64_t stream);

private:
  std::vector<ObjectFactory>
      m_propagationLoss;            ///< vector of propagation loss models
  ObjectFactory m_propagationDelay; ///< propagation delay model
};

/**
 * \brief Make it easy to create and manage PHY objects for the Dmg model.
 *
 * The Dmg PHY model is described in "Yet Another Network Simulator",
 * http://cutebugs.net/files/wns2-Dmg.pdf
 *
 * The Pcap and ascii traces generated by the EnableAscii and EnablePcap methods
 * defined in this class correspond to PHY-level traces and come to us via
 * WifiPhyHelper
 *
 */
class WigigPhyHelper : public PcapHelperForDevice,
                       public AsciiTraceHelperForDevice {
public:
  WigigPhyHelper(const std::string &errorModel);
  ~WigigPhyHelper() override;

  /**
   * \param channel the channel to associate to this helper
   *
   * Every PHY created by a call to Install is associated to this channel.
   */
  void SetChannel(Ptr<WigigChannel> channel);
  /**
   * \param channelName The name of the channel to associate to this helper
   *
   * Every PHY created by a call to Install is associated to this channel.
   */
  void SetChannel(std::string channelName);

  /**
   * \param name the name of the attribute to set
   * \param v the value of the attribute
   *
   * Set an attribute of the underlying PHY object.
   */
  void Set(std::string name, const AttributeValue &v);
  /**
   * \param name the name of the error rate model to set.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Set the error rate model and its attributes to use when Install is called.
   */
  void SetErrorRateModel(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());
  /**
   * \param name the name of the frame capture model to set.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Set the frame capture model and its attributes to use when Install is
   * called.
   */
  void SetFrameCaptureModel(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());
  /**
   * \param name the name of the preamble detection model to set.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Set the preamble detection model and its attributes to use when Install is
   * called.
   */
  void SetPreambleDetectionModel(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());

  /**
   * Disable the preamble detection model.
   */
  void DisablePreambleDetectionModel();

  /**
   * An enumeration of the pcap data link types (DLTs) which this helper
   * supports.  See http://wiki.wireshark.org/Development/LibpcapFileFormat
   * for more information on these formats.
   */
  enum SupportedPcapDataLinkTypes {
    DLT_IEEE802_11 = PcapHelper::DLT_IEEE802_11, /**< IEEE 802.11 Wireless LAN
                                                    headers on packets */
    DLT_PRISM_HEADER = PcapHelper::DLT_PRISM_HEADER, /**< Include Prism monitor
                                                        mode information */
    DLT_IEEE802_11_RADIO =
        PcapHelper::DLT_IEEE802_11_RADIO /**< Include Radiotap link layer
                                            information */
  };

  /**
   * Set the data link type of PCAP traces to be used. This function has to be
   * called before EnablePcap(), so that the header of the pcap file can be
   * written correctly.
   *
   * \see SupportedPcapDataLinkTypes
   *
   * \param dlt The data link type of the pcap file (and packets) to be used
   */
  void SetPcapDataLinkType(SupportedPcapDataLinkTypes dlt);

  /**
   * Set the type of the ascii trace to be printed. This function has to be
   * called before EnableAscii(), so that the type of the ascii trace file
   * can be written correctly.
   *
   * \param traceType The type of the ascrii trace to be printed.
   */
  void SetAsciiTraceType(SupportedAsciiTraceTypes traceType);

  /**
   * Set the maximum length of packet data stored in the PCAP file.
   *
   * \param length The length of the snapshot in bytes.
   */
  void SetSnapshotLength(uint32_t length);

  /**
   * Get the data link type of PCAP traces to be used.
   *
   * \see SupportedPcapDataLinkTypes
   *
   * \returns The data link type of the pcap file (and packets) to be used
   */
  PcapHelper::DataLinkType GetPcapDataLinkType() const;
  /**
   * Get the maximum length of packet data stored in the PCAP file.
   *
   * \return length The length of the snapshot in bytes.
   */
  uint32_t GetSnapshotLength() const;

protected:
  friend class WigigHelper;

  /**
   * \param node the node on which we wish to create a wifi PHY
   * \param device the device within which this PHY will be created
   * \returns a newly-created PHY object.
   *
   * This method implements the pure virtual method defined in \ref
   * ns3::WifiPhyHelper.
   */
  virtual Ptr<WigigPhy> Create(Ptr<Node> node, Ptr<NetDevice> device) const;

  /**
   * \param file the pcap file wrapper
   * \param packet the packet
   * \param channelFreqMhz the channel frequency
   * \param txVector the TXVECTOR
   * \param aMpdu the A-MPDU information
   *
   * Handle TX pcap.
   */
  static void PcapSniffTxEvent(Ptr<PcapFileWrapper> file,
                               Ptr<const Packet> packet,
                               uint16_t channelFreqMhz,
                               const WigigTxVector &txVector, MpduInfo aMpdu);
  /**
   * \param file the pcap file wrapper
   * \param packet the packet
   * \param channelFreqMhz the channel frequency
   * \param txVector the TXVECTOR
   * \param aMpdu the A-MPDU information
   * \param signalNoise the RX signal and noise information
   *
   * Handle RX pcap.
   */
  static void PcapSniffRxEvent(Ptr<PcapFileWrapper> file,
                               Ptr<const Packet> packet,
                               uint16_t channelFreqMhz,
                               const WigigTxVector &txVector, MpduInfo aMpdu,
                               SignalNoiseDbm signalNoise);

  Ptr<WigigChannel> m_channel;               ///< Dmg wifi channel
  ObjectFactory m_phy;                       ///< PHY object
  ObjectFactory m_errorRateModel;            ///< error rate model
  PcapHelper::DataLinkType m_pcapDlt;        ///< PCAP data link type
  SupportedAsciiTraceTypes m_asciiTraceType; ///< ASCII Trace type.
  uint32_t m_snaplen;                        ///< Snapshot length in bytes.
  ObjectFactory m_frameCaptureModel;         ///< frame capture model
  ObjectFactory m_preambleDetectionModel;    ///< preamble detection model

private:
  /**
   * Get the Radiotap header.
   *
   * \param packet the packet
   * \param channelFreqMhz the channel frequency
   * \param txVector the TXVECTOR
   * \param aMpdu the A-MPDU information
   *
   * \returns the Radiotap header
   */
  static RadiotapHeader GetRadiotapHeader(Ptr<Packet> packet,
                                          uint16_t channelFreqMhz,
                                          const WigigTxVector &txVector,
                                          MpduInfo aMpdu);

  /**
   * \brief Enable pcap output the indicated net device.
   *
   * NetDevice-specific implementation mechanism for hooking the trace and
   * writing to the trace file.
   *
   * \param prefix Filename prefix to use for pcap files.
   * \param nd Net device for which you want to enable tracing.
   * \param promiscuous If true capture all possible packets available at the
   * device. \param explicitFilename Treat the prefix as an explicit filename if
   * true
   */
  void EnablePcapInternal(std::string prefix, Ptr<NetDevice> nd,
                          bool promiscuous, bool explicitFilename) override;

  /**
   * \brief Enable ASCII trace output on the indicated net device.
   *
   * NetDevice-specific implementation mechanism for hooking the trace and
   * writing to the trace file.
   *
   * \param stream The output stream object to use when logging ASCII traces.
   * \param prefix Filename prefix to use for ASCII trace files.
   * \param nd Net device for which you want to enable tracing.
   * \param explicitFilename Treat the prefix as an explicit filename if true
   */
  void EnableAsciiInternal(Ptr<OutputStreamWrapper> stream, std::string prefix,
                           Ptr<NetDevice> nd, bool explicitFilename) override;
};

/**
 * \brief Make it easy to create and manage PHY objects for the spectrum model.
 *
 * The Pcap and ascii traces generated by the EnableAscii and EnablePcap methods
 * defined in this class correspond to PHY-level traces and come to us via
 * WifiPhyHelper
 *
 */
class SpectrumWigigPhyHelper : public WigigPhyHelper {
public:
  /**
   * Create a phy helper without any parameter set. The user must set
   * them all to be able to call Install later.
   */
  SpectrumWigigPhyHelper(const std::string &errorModel);

  /**
   * Create a phy helper in a default working state.
   * \returns a default SpectrumWifPhyHelper
   */
  static SpectrumWigigPhyHelper Default();

  /**
   * \param channel the channel to associate to this helper
   *
   * Every PHY created by a call to Install is associated to this channel.
   */
  void SetChannel(Ptr<SpectrumChannel> channel);
  /**
   * \param channelName The name of the channel to associate to this helper
   *
   * Every PHY created by a call to Install is associated to this channel.
   */
  void SetChannel(std::string channelName);

private:
  friend class WigigHelper;
  /**
   * \param node the node on which we wish to create a wifi PHY
   * \param device the device within which this PHY will be created
   * \returns a newly-created PHY object.
   *
   * This method implements the pure virtual method defined in \ref
   * ns3::WifiPhyHelper.
   */
  Ptr<WigigPhy> Create(Ptr<Node> node, Ptr<NetDevice> device) const override;

  Ptr<SpectrumChannel> m_channel; ///< the channel
};

/**
 * \brief helps to create WifiNetDevice objects
 *
 * This class can help to create a large set of similar
 * WifiNetDevice objects and to configure a large set of
 * their attributes during creation.
 */
class WigigHelper {
public:
  WigigHelper();
  virtual ~WigigHelper();

  /**
   * \param type the type of ns3::WifiRemoteStationManager to create.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * All the attributes specified in this method should exist
   * in the requested station manager.
   */
  void SetRemoteStationManager(
      std::string type, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());

  /**
   * \param ac the Access Category to attach the ack policy selector to.
   * \param type the type of ns3::WigigAckPolicySelector to create.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * All the attributes specified in this method should exist
   * in the requested ack policy selector.
   */
  void SetAckPolicySelectorForAc(
      AcIndex ac, std::string type, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());

  /// Callback invoked to determine the MAC queue selected for a given packet
  typedef std::function<std::size_t(Ptr<QueueItem>)> SelectQueueCallback;

  /**
   * \param f the select queue callback
   *
   * Set the select queue callback to set on the NetDevice queue interface
   * aggregated to the WifiNetDevice, in case WifiMac with QoS enabled is used
   */
  void SetSelectQueueCallback(SelectQueueCallback f);

  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param first lower bound on the set of nodes on which a wifi device must be
   * created \param last upper bound on the set of nodes on which a wifi device
   * must be created \returns a device container which contains all the devices
   * created by this method.
   */
  NetDeviceContainer Install(const WigigPhyHelper &phy,
                             const WigigMacHelper &mac,
                             NodeContainer::Iterator first,
                             NodeContainer::Iterator last) const;
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param c the set of nodes on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const WigigPhyHelper &phy,
                             const WigigMacHelper &mac, NodeContainer c) const;
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param node the node on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const WigigPhyHelper &phy,
                             const WigigMacHelper &mac, Ptr<Node> node) const;
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param nodeName the name of node on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const WigigPhyHelper &phy,
                             const WigigMacHelper &mac,
                             std::string nodeName) const;
  /**
   * \param name the name of the codebook to set.
   * \param n0 the name of the attribute to set
   * \param v0 the value of the attribute to set
   * \param n1 the name of the attribute to set
   * \param v1 the value of the attribute to set
   * \param n2 the name of the attribute to set
   * \param v2 the value of the attribute to set
   * \param n3 the name of the attribute to set
   * \param v3 the value of the attribute to set
   * \param n4 the name of the attribute to set
   * \param v4 the value of the attribute to set
   * \param n5 the name of the attribute to set
   * \param v5 the value of the attribute to set
   * \param n6 the name of the attribute to set
   * \param v6 the value of the attribute to set
   * \param n7 the name of the attribute to set
   * \param v7 the value of the attribute to set
   *
   * Set the codebook type and its attributes to use when Install is called.
   */
  void SetCodebook(
      std::string name, std::string n0 = "",
      const AttributeValue &v0 = EmptyAttributeValue(), std::string n1 = "",
      const AttributeValue &v1 = EmptyAttributeValue(), std::string n2 = "",
      const AttributeValue &v2 = EmptyAttributeValue(), std::string n3 = "",
      const AttributeValue &v3 = EmptyAttributeValue(), std::string n4 = "",
      const AttributeValue &v4 = EmptyAttributeValue(), std::string n5 = "",
      const AttributeValue &v5 = EmptyAttributeValue(), std::string n6 = "",
      const AttributeValue &v6 = EmptyAttributeValue(), std::string n7 = "",
      const AttributeValue &v7 = EmptyAttributeValue());
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param first lower bound on the set of nodes on which a wifi device must be
   * created \param last upper bound on the set of nodes on which a wifi device
   * must be created \returns a device container which contains all the devices
   * created by this method.
   */
  NetDeviceContainer Install(const SpectrumWigigPhyHelper &phyHelper,
                             const WigigMacHelper &macHelper,
                             NodeContainer::Iterator first,
                             NodeContainer::Iterator last,
                             bool installCodebook = true) const;

  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param c the set of nodes on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const SpectrumWigigPhyHelper &phy,
                             const WigigMacHelper &mac, NodeContainer c,
                             bool installCodebook = true) const;
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param node the node on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const SpectrumWigigPhyHelper &phy,
                             const WigigMacHelper &mac, Ptr<Node> node,
                             bool installCodebook = true) const;
  /**
   * \param phy the PHY helper to create PHY objects
   * \param mac the MAC helper to create MAC objects
   * \param nodeName the name of node on which a wifi device must be created
   * \returns a device container which contains all the devices created by this
   * method.
   */
  NetDeviceContainer Install(const SpectrumWigigPhyHelper &phy,
                             const WigigMacHelper &mac, std::string nodeName,
                             bool installCodebook = true) const;

  /**
   * Helper to enable all WigigNetDevice log components with one statement
   */
  static void EnableLogComponents();

  /**
   * Assign a fixed random variable stream number to the random variables
   * used by the PHY and MAC aspects of the Wifi models.  Each device in
   * container c has fixed stream numbers assigned to its random variables.
   * The Wifi channel (e.g. propagation loss model) is excluded.
   * Return the number of streams (possibly zero) that
   * have been assigned. The Install() method should have previously been
   * called by the user.
   *
   * \param c NetDeviceContainer of the set of net devices for which the
   *          WifiNetDevice should be modified to use fixed streams
   * \param stream first stream index to use
   * \return the number of stream indices assigned by this helper
   */
  int64_t AssignStreams(NetDeviceContainer c, int64_t stream);

private:
  ObjectFactory m_stationManager;       ///< station manager
  ObjectFactory m_ackPolicySelector[4]; ///< ack policy selector for all ACs
  WifiStandard m_standard;              ///< wifi standard
  SelectQueueCallback m_selectQueueCallback; ///< select queue callback
  ObjectFactory m_codeBook; ///< Codebook factory for all the devices
};

} // namespace ns3

#endif /* WIGIG_HELPER_H */
