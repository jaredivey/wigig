/*
 * Copyright (c) 2021 HANY ASSASA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "beamforming-trace-helper.h"

#include "ns3/log.h"
#include "ns3/wigig-net-device.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("BeamformingTraceHelper");

BeamformingTraceHelper::BeamformingTraceHelper(
    Ptr<QdPropagationEngine> qdPropagationEngine, std::string tracesFolder,
    std::string runNumber, NODE_ID_MAPPING mapping)
    : m_tracesFolder(tracesFolder), m_qdPropagationEngine(qdPropagationEngine),
      m_runNumber(runNumber), m_mapping(mapping) {
  NS_LOG_FUNCTION(this << qdPropagationEngine << tracesFolder << runNumber
                       << mapping);
}

BeamformingTraceHelper::~BeamformingTraceHelper() {}

void BeamformingTraceHelper::ConnectTrace(Ptr<WigigMac> wifiMac) {
  NS_LOG_FUNCTION(this << wifiMac);
  if (m_mapping == NODE_ID_MAPPING_NS3_IDS) {
    map_Mac2ID[wifiMac->GetAddress()] =
        wifiMac->GetDevice()->GetNode()->GetId();
  } else if (m_mapping == NODE_ID_MAPPING_QD_CUSTOM_IDS) {
    map_Mac2ID[wifiMac->GetAddress()] = m_qdPropagationEngine->GetQdId(
        wifiMac->GetDevice()->GetNode()->GetId());
  }
  m_mapMac2Class[wifiMac->GetAddress()] = wifiMac;
  DoConnectTrace(wifiMac);
}

void BeamformingTraceHelper::ConnectTrace(
    const NetDeviceContainer &deviceConteiner) {
  NS_LOG_FUNCTION(this);
  for (NetDeviceContainer::Iterator i = deviceConteiner.Begin();
       i != deviceConteiner.End(); ++i) {
    ConnectTrace(
        StaticCast<WigigMac>(StaticCast<WigigNetDevice>(*i)->GetMac()));
  }
}

void BeamformingTraceHelper::SetTracesFolder(std::string traceFolder) {
  NS_LOG_FUNCTION(this << traceFolder);
  m_tracesFolder = traceFolder;
}

std::string BeamformingTraceHelper::GetTracesFolder() const {
  return m_tracesFolder;
}

Ptr<OutputStreamWrapper> BeamformingTraceHelper::GetStreamWrapper() const {
  return m_stream;
}

void BeamformingTraceHelper::SetRunNumber(string runNumber) {
  m_runNumber = runNumber;
}

std::string BeamformingTraceHelper::GetRunNumber() const { return m_runNumber; }

/******************************************************************/

SlsBeamformingTraceHelper::SlsBeamformingTraceHelper(
    Ptr<QdPropagationEngine> qdPropagationEngine, std::string tracesFolder,
    std::string runNumber, NODE_ID_MAPPING mapping)
    : BeamformingTraceHelper(qdPropagationEngine, tracesFolder, runNumber,
                             mapping) {
  NS_LOG_FUNCTION(this << qdPropagationEngine << tracesFolder << runNumber
                       << mapping);
  DoGenerateTraceFiles();
}

SlsBeamformingTraceHelper::~SlsBeamformingTraceHelper() {}

void SlsBeamformingTraceHelper::DoGenerateTraceFiles() {
  NS_LOG_FUNCTION(this);
  m_stream =
      m_ascii.CreateFileStream(m_tracesFolder + "sls_" + m_runNumber + ".csv");
  // Add MetaData?
  *m_stream->GetStream() << "TIME,TRACE_ID,SRC_ID,DST_ID,BFT_ID,ANTENNA_ID,"
                            "SECTOR_ID,ROLE,BSS_ID,SINR_DB"
                         << std::endl;
}

void SlsBeamformingTraceHelper::DoConnectTrace(Ptr<WigigMac> wifiMac) {
  wifiMac->TraceConnectWithoutContext(
      "SLSCompleted",
      MakeBoundCallback(&SlsBeamformingTraceHelper::SLSCompleted, this,
                        wifiMac));
}

void SlsBeamformingTraceHelper::SLSCompleted(
    Ptr<SlsBeamformingTraceHelper> recorder, Ptr<WigigMac> wifiMac,
    SlsCompletionAttrbitutes attributes) {
  uint32_t srcID = recorder->map_Mac2ID[wifiMac->GetAddress()];
  uint32_t dstID = recorder->map_Mac2ID[attributes.peerStation];
  uint32_t AP_ID = recorder->map_Mac2ID[wifiMac->GetBssid()];
  double linkSnr = RatioToDb(attributes.maxSnr);
  *recorder->m_stream->GetStream()
      << Simulator::Now().GetNanoSeconds() << ","
      << recorder->m_qdPropagationEngine->GetCurrentTraceIndex() << "," << srcID
      << "," << dstID << "," << attributes.bftID << ","
      << uint16_t(attributes.antennaID - 1) << ","
      << uint16_t(attributes.sectorID - 1) << "," << wifiMac->GetTypeOfStation()
      << "," << AP_ID << "," << linkSnr << std::endl;
}

GroupBeamformingTraceHelper::GroupBeamformingTraceHelper(
    Ptr<QdPropagationEngine> qdPropagationEngine, std::string tracesFolder,
    std::string runNumber, NODE_ID_MAPPING mapping)
    : BeamformingTraceHelper(qdPropagationEngine, tracesFolder, runNumber,
                             mapping) {
  NS_LOG_FUNCTION(this << qdPropagationEngine << tracesFolder << runNumber
                       << mapping);
  DoGenerateTraceFiles();
}

GroupBeamformingTraceHelper::~GroupBeamformingTraceHelper() {}

void GroupBeamformingTraceHelper::DoGenerateTraceFiles() {
  NS_LOG_FUNCTION(this);
  m_stream = m_ascii.CreateFileStream(m_tracesFolder + "group_" + m_runNumber +
                                      ".csv");
  // Add MetaData?
  *m_stream->GetStream() << "TIME,TRACE_ID,SRC_ID,DST_ID,BFT_ID,ANTENNA_ID,"
                            "SECTOR_ID,AWV_ID,ROLE,BSS_ID,SINR_DB"
                         << std::endl;
}

void GroupBeamformingTraceHelper::DoConnectTrace(Ptr<WigigMac> wifiMac) {
  wifiMac->TraceConnectWithoutContext(
      "GroupBeamformingCompleted",
      MakeBoundCallback(&GroupBeamformingTraceHelper::GroupCompleted, this,
                        wifiMac));
}

void GroupBeamformingTraceHelper::GroupCompleted(
    Ptr<GroupBeamformingTraceHelper> recorder, Ptr<WigigMac> wifiMac,
    GroupBfCompletionAttrbitutes attributes) {
  uint32_t srcID = recorder->map_Mac2ID[wifiMac->GetAddress()];
  uint32_t dstID = recorder->map_Mac2ID[attributes.peerStation];
  uint32_t AP_ID = recorder->map_Mac2ID[wifiMac->GetBssid()];
  double linkSnr = RatioToDb(attributes.maxSnr);
  *recorder->m_stream->GetStream()
      << Simulator::Now().GetNanoSeconds() << ","
      << recorder->m_qdPropagationEngine->GetCurrentTraceIndex() << "," << srcID
      << "," << dstID << "," << attributes.bftID << ","
      << uint16_t(attributes.antennaID - 1) << ","
      << uint16_t(attributes.sectorID - 1) << "," << uint16_t(attributes.awvID)
      << "," << attributes.beamformingDirection << "," << AP_ID << ","
      << linkSnr << std::endl;
}

/******************************************************************/

} // namespace ns3
