/*
 * Copyright (c) 2021 HANY ASSASA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#ifndef BEAMFORMING_TRACE_HELPER_H
#define BEAMFORMING_TRACE_HELPER_H

#include "ns3/net-device-container.h"
#include "ns3/object-factory.h"
#include "ns3/qd-propagation-engine.h"
#include "ns3/trace-helper.h"
#include "ns3/wigig-mac.h"

#include <map>
#include <string>

using namespace std;

namespace ns3 {

typedef std::map<Mac48Address, uint32_t>
    MAP_MAC2ID; //!< Typedef for mapping between MAC address and ns-3 node ID.
typedef MAP_MAC2ID::iterator
    MAP_MAC2ID_I; //!< Typedef for iterator over MAC to ns-3 node IDs.
typedef std::map<Mac48Address, Ptr<WigigMac>>
    MAP_MAC2CLASS; //!< Typedef for mapping between MAC address and ns-3 node
                   //!< ID.
typedef std::map<Mac48Address, Ptr<OutputStreamWrapper>>
    MAP_MAC2STREAM; //!< Typedef for mapping between MAC address and ns-3 node
                    //!< ID.
typedef std::pair<uint32_t, uint32_t>
    SRC_DST_ID_PAIR; //!< Typedef for a source and destination ID pair.
typedef std::map<SRC_DST_ID_PAIR, Ptr<OutputStreamWrapper>>
    MAP_PAIR2STREAM; //!< Typedef for mapping between a source and destination
                     //!< ID pair and a stream.
typedef MAP_PAIR2STREAM::iterator
    MAP_PAIR2STREAM_I; //!< Typedef for a iterator over a source and
                       //!< destionatio ID pair and a stream.

enum NODE_ID_MAPPING {
  NODE_ID_MAPPING_NS3_IDS = 0,
  NODE_ID_MAPPING_QD_CUSTOM_IDS,
};

class BeamformingTraceHelper : public SimpleRefCount<BeamformingTraceHelper> {
public:
  /**
   * Beamforming trace helper constructor.
   * \param qdPropagationEngine Pointer to the QdPropagationEngine class.
   * \param tracesFolder Path to the folder where we store traces files.
   * \param mapping Type of node ID mapping either directly use ns-3 IDs or map
   * ns-3 nodes IDs to a specific ones based on QdPropagationEngine class.
   */
  BeamformingTraceHelper(Ptr<QdPropagationEngine> qdPropagationEngine,
                         std::string tracesFolder, string runNumber,
                         NODE_ID_MAPPING mapping = NODE_ID_MAPPING_NS3_IDS);
  virtual ~BeamformingTraceHelper();

  /**
   * Connect beamforming trace for the given WifiMac.
   * \param wifiMac Pointer to the WigigMac class.
   */
  void ConnectTrace(Ptr<WigigMac> wifiMac);
  /**
   * Connect beamforming trace for all the devices in the NetDeviceContainer
   * \param deviceConteiner List of device containers containing WifiNetDevices.
   */
  void ConnectTrace(const NetDeviceContainer &deviceConteiner);
  /**
   * Set the output folder where you want to dump the trace files.
   * \param traceFolder Path to the trace folder.
   */
  void SetTracesFolder(std::string traceFolder);
  /**
   * Get the current trace folder.
   * \return Path of the current trace folder
   */
  std::string GetTracesFolder() const;
  /**
   * Get pointer to the stream wrapper.
   * \return
   */
  Ptr<OutputStreamWrapper> GetStreamWrapper() const;
  /**
   * Set the simulation run number.
   * \return
   */
  void SetRunNumber(std::string runNumber);
  /**
   * Get the current simulation run number.
   * \return Simulation run number
   */
  std::string GetRunNumber() const;

protected:
  /**
   * In this function, we generate all the traces files related to the helper.
   */
  virtual void DoGenerateTraceFiles() = 0;
  virtual void DoConnectTrace(Ptr<WigigMac> wifiMac) = 0;

protected:
  AsciiTraceHelper m_ascii;   //!< Ascii tracer helper instance.
  std::string m_tracesFolder; //!< Path to the folder where we store the traces.
  Ptr<OutputStreamWrapper> m_stream; //!< Pointer to the stream wrapper.
  MAP_MAC2ID
      map_Mac2ID; //!< Map between WifiNetDevice MAC address and ns-3 node ID.
  Ptr<QdPropagationEngine>
      m_qdPropagationEngine; //!< Pointer to the Q-D propagation engine.
  std::string m_runNumber;   //!< Simulation Run Number
  NODE_ID_MAPPING
      m_mapping; //!< The type of mapping between ns-3 IDs and Q-D software IDs.
  MAP_MAC2CLASS
      m_mapMac2Class; //!< Data structure for mapping between MAC Address and
                      //!< the corresponding WigigMac class.
};

class SlsBeamformingTraceHelper : public BeamformingTraceHelper {
public:
  /**
   * SLS beamforming trace helper constructor.
   * \param qdPropagationEngine Pointer to the QdPropagationEngine class.
   * \param tracesFolder Path to the folder where we store traces files.
   * \param mapping Type of node ID mapping either directly use ns-3 IDs or map
   * ns-3 nodes IDs to a specific ones based on QdPropagationEngine class.
   */
  SlsBeamformingTraceHelper(Ptr<QdPropagationEngine> qdPropagationEngine,
                            std::string tracesFolder, std::string runNumber,
                            NODE_ID_MAPPING mapping = NODE_ID_MAPPING_NS3_IDS);
  ~SlsBeamformingTraceHelper() override;

protected:
  void DoGenerateTraceFiles() override;
  void DoConnectTrace(Ptr<WigigMac> wifiMac) override;

private:
  /**
   * This function is called when a SLS beamforming training is completed.
   * \param helper Pointer to the SLS beamforming trace helper.
   * \param wifiMac Pointer to the DMG Wifi MAC class.
   * \param attributes SLS completion attributes structure.
   */
  static void SLSCompleted(Ptr<SlsBeamformingTraceHelper> helper,
                           Ptr<WigigMac> wifiMac,
                           SlsCompletionAttrbitutes attributes);
};

class GroupBeamformingTraceHelper : public BeamformingTraceHelper {
public:
  /**
   * Group beamforming trace helper constructor.
   * \param qdPropagationEngine Pointer to the QdPropagationEngine class.
   * \param tracesFolder Path to the folder where we store traces files.
   * \param runNumber The run number of the simulation.
   * \param mapping Type of node ID mapping either directly use ns-3 IDs or map
   * ns-3 nodes IDs to a specific ones based on QdPropagationEngine class.
   */
  GroupBeamformingTraceHelper(
      Ptr<QdPropagationEngine> qdPropagationEngine, std::string tracesFolder,
      std::string runNumber, NODE_ID_MAPPING mapping = NODE_ID_MAPPING_NS3_IDS);
  ~GroupBeamformingTraceHelper() override;

protected:
  void DoGenerateTraceFiles() override;
  void DoConnectTrace(Ptr<WigigMac> wifiMac) override;

private:
  /**
   * This function is called when a Group beamforming training is completed.
   * \param helper Pointer to the Group beamforming trace helper.
   * \param wifiMac Pointer to the DMG Wifi MAC class.
   * \param attributes Group completion attributes structure.
   */
  static void GroupCompleted(Ptr<GroupBeamformingTraceHelper> helper,
                             Ptr<WigigMac> wifiMac,
                             GroupBfCompletionAttrbitutes attributes);
};

} // namespace ns3

#endif // BEAMFORMING_TRACE_HELPER_H
