/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef WIGIG_MAC_HELPER_H
#define WIGIG_MAC_HELPER_H

#include "ns3/object-factory.h"

namespace ns3 {

class WigigMac;
class NetDevice;

/**
 * \brief create DMG-enabled MAC layers for a ns3::WifiNetDevice.
 *
 * This class can create MACs of type ns3::ApWigigMac, ns3::StaWigigMac,
 * and, ns3::AdhocWigigMac, with QosSupported, HTSupported and DMGSupported
 * attributes set to True.
 */
class WigigMacHelper {
public:
  /**
   * Create a WigigMacHelper that is used to make life easier when working
   * with Wifi devices using a DMG MAC layer.
   */
  WigigMacHelper();

  /**
   * Destroy a WigigMacHelper
   */
  virtual ~WigigMacHelper();

  /**
   * \tparam Args \deduced Template type parameter pack for the sequence of
   * name-value pairs. \param type the type of ns3::WifiMac to create. \param
   * args A sequence of name-value pairs of the attributes to set.
   *
   * All the attributes specified in this method should exist
   * in the requested MAC.
   */
  template <typename... Args> void SetType(std::string type, Args &&...args);

  /**
   * \param device the device within which the MAC object will reside
   * \returns a new MAC object.
   *
   * This allows the ns3::DmgWifiHelper class to create MAC objects from
   * ns3::WifiHelper::Install.
   */
  virtual Ptr<WigigMac> Create(Ptr<NetDevice> device) const;

protected:
  ObjectFactory m_mac; ///< MAC object factory
};

} // namespace ns3

/***************************************************************
 *  Implementation of the template declared above.
 ***************************************************************/

namespace ns3 {

template <typename... Args>
void WigigMacHelper::SetType(std::string type, Args &&...args) {
  m_mac.SetTypeId(type);
  m_mac.Set(args...);
}

} // namespace ns3

#endif /* WIGIG_MAC_HELPER_H */
