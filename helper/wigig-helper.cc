/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-helper.h"

#include "ns3/ampdu-subframe-header.h"
#include "ns3/config.h"
#include "ns3/error-rate-model.h"
#include "ns3/log.h"
#include "ns3/names.h"
#include "ns3/net-device-queue-interface.h"
#include "ns3/pointer.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/radiotap-header.h"
#include "ns3/spectrum-wigig-phy.h"
#include "ns3/string.h"
#include "ns3/wifi-helper.h"
#include "ns3/wifi-phy-common.h"
#include "ns3/wigig-ack-policy-selector.h"
#include "ns3/wigig-mac-queue.h"
#include "ns3/wigig-mac.h"
#include "ns3/wigig-net-device.h"
#include "ns3/wigig-phy.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("WigigHelper");

/**
 * ASCII trace PHY transmit sink with context
 * \param stream the output stream
 * \param context the context name
 * \param p the packet
 * \param mode the wifi mode
 * \param preamble the wifi preamble
 * \param txLevel the transmit power level
 */
static void AsciiPhyTransmitSinkWithContext(Ptr<OutputStreamWrapper> stream,
                                            std::string context,
                                            Ptr<const Packet> p, WifiMode mode,
                                            WifiPreamble preamble,
                                            uint8_t txLevel) {
  NS_LOG_FUNCTION(stream << context << p << mode << preamble << txLevel);
  *stream->GetStream() << "t " << Simulator::Now().GetSeconds() << " "
                       << context << " " << mode << " " << *p << std::endl;
}

/**
 * ASCII trace PHY transmit sink without context
 * \param stream the output stream
 * \param p the packet
 * \param mode the wifi mode
 * \param preamble the wifi preamble
 * \param txLevel the transmit power level
 */
static void AsciiPhyTransmitSinkWithoutContext(Ptr<OutputStreamWrapper> stream,
                                               Ptr<const Packet> p,
                                               WifiMode mode,
                                               WifiPreamble preamble,
                                               uint8_t txLevel) {
  NS_LOG_FUNCTION(stream << p << mode << preamble << txLevel);
  *stream->GetStream() << "t " << Simulator::Now().GetSeconds() << " " << mode
                       << " " << *p << std::endl;
}

/**
 * ASCII trace PHY receive sink with context
 * \param stream the output stream
 * \param context the context name
 * \param p the packet
 * \param snr the SNR
 * \param mode the wifi mode
 * \param preamble the wifi preamble
 */
static void AsciiPhyReceiveSinkWithContext(Ptr<OutputStreamWrapper> stream,
                                           std::string context,
                                           Ptr<const Packet> p, double snr,
                                           WifiMode mode,
                                           WifiPreamble preamble) {
  NS_LOG_FUNCTION(stream << context << p << snr << mode << preamble);
  *stream->GetStream() << "r " << Simulator::Now().GetSeconds() << " " << mode
                       << "" << context << " " << *p << std::endl;
}

/**
 * ASCII trace PHY receive sink without context
 * \param stream the output stream
 * \param p the packet
 * \param snr the SNR
 * \param mode the wifi mode
 * \param preamble the wifi preamble
 */
static void AsciiPhyReceiveSinkWithoutContext(Ptr<OutputStreamWrapper> stream,
                                              Ptr<const Packet> p, double snr,
                                              WifiMode mode,
                                              WifiPreamble preamble) {
  NS_LOG_FUNCTION(stream << p << snr << mode << preamble);
  *stream->GetStream() << "r " << Simulator::Now().GetSeconds() << " " << mode
                       << " " << *p << std::endl;
}

/**
 * ASCII PHY Activity trace without context
 * \param stream the output stream
 * \param srcID the ID of the transmitting node.
 * \param dstID the ID of the receiving node.
 * \param duration the duration of the activity in nanoseconds.
 * \param power the power of the transmitted or received part of the PLCP.
 * \param fieldType the type of the PLCP field being transmitted or received.
 * \param activityType the type of the PHY activity.
 */
static void AsciiPhyActivityTraceWithoutContext(Ptr<OutputStreamWrapper> stream,
                                                uint32_t srcID, uint32_t dstID,
                                                Time duration, double power,
                                                uint16_t fieldType,
                                                uint16_t activityType) {
  *stream->GetStream() << Simulator::Now().GetNanoSeconds() << "," << srcID
                       << "," << dstID << "," << duration.GetNanoSeconds()
                       << "," << power << "," << fieldType << ","
                       << activityType << std::endl;
}

WigigChannelHelper::WigigChannelHelper() {}

WigigChannelHelper WigigChannelHelper::Default() {
  WigigChannelHelper helper;
  helper.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  helper.AddPropagationLoss("ns3::LogDistancePropagationLossModel");
  return helper;
}

void WigigChannelHelper::AddPropagationLoss(
    std::string type, std::string n0, const AttributeValue &v0, std::string n1,
    const AttributeValue &v1, std::string n2, const AttributeValue &v2,
    std::string n3, const AttributeValue &v3, std::string n4,
    const AttributeValue &v4, std::string n5, const AttributeValue &v5,
    std::string n6, const AttributeValue &v6, std::string n7,
    const AttributeValue &v7) {
  ObjectFactory factory;
  factory.SetTypeId(type);
  factory.Set(n0, v0);
  factory.Set(n1, v1);
  factory.Set(n2, v2);
  factory.Set(n3, v3);
  factory.Set(n4, v4);
  factory.Set(n5, v5);
  factory.Set(n6, v6);
  factory.Set(n7, v7);
  m_propagationLoss.push_back(factory);
}

void WigigChannelHelper::SetPropagationDelay(
    std::string type, std::string n0, const AttributeValue &v0, std::string n1,
    const AttributeValue &v1, std::string n2, const AttributeValue &v2,
    std::string n3, const AttributeValue &v3, std::string n4,
    const AttributeValue &v4, std::string n5, const AttributeValue &v5,
    std::string n6, const AttributeValue &v6, std::string n7,
    const AttributeValue &v7) {
  ObjectFactory factory;
  factory.SetTypeId(type);
  factory.Set(n0, v0);
  factory.Set(n1, v1);
  factory.Set(n2, v2);
  factory.Set(n3, v3);
  factory.Set(n4, v4);
  factory.Set(n5, v5);
  factory.Set(n6, v6);
  factory.Set(n7, v7);
  m_propagationDelay = factory;
}

Ptr<WigigChannel> WigigChannelHelper::Create() const {
  Ptr<WigigChannel> channel = CreateObject<WigigChannel>();
  Ptr<PropagationLossModel> prev = nullptr;
  for (std::vector<ObjectFactory>::const_iterator i = m_propagationLoss.begin();
       i != m_propagationLoss.end(); ++i) {
    Ptr<PropagationLossModel> cur = (*i).Create<PropagationLossModel>();
    if (prev) {
      prev->SetNext(cur);
    }
    if (m_propagationLoss.begin() == i) {
      channel->SetPropagationLossModel(cur);
    }
    prev = cur;
  }
  Ptr<PropagationDelayModel> delay =
      m_propagationDelay.Create<PropagationDelayModel>();
  channel->SetPropagationDelayModel(delay);
  return channel;
}

int64_t WigigChannelHelper::AssignStreams(Ptr<WigigChannel> c, int64_t stream) {
  return c->AssignStreams(stream);
}

WigigPhyHelper::WigigPhyHelper(const std::string &errorModel)
    : m_channel(nullptr), m_pcapDlt(PcapHelper::DLT_IEEE802_11),
      m_asciiTraceType(ASCII_TRACE_LEGACY),
      m_snaplen(std::numeric_limits<uint32_t>::max()) {
  m_phy.SetTypeId("ns3::WigigPhy");
  /* Set the correct error model */
  SetErrorRateModel("ns3::DmgErrorModel", "FileName", StringValue(errorModel));
  Set("RxNoiseFigure", DoubleValue(10));
  // The value correspond to DMG MCS-0.
  // The start of a valid DMG control PHY transmission at a receive level
  // greater than the minimum sensitivity for control PHY (–78 dBm) shall cause
  // CCA to indicate busy with a probability > 90% within 3 μs.
  Set("RxSensitivity", DoubleValue(-101)); // CCA-SD for 802.11 signals.
  // The start of a valid DMG SC PHY transmission at a receive level greater
  // than the minimum sensitivity for MCS 1 (–68 dBm) shall cause CCA to
  // indicate busy with a probability > 90% within 1 μs. The receiver shall hold
  // the carrier sense signal busy for any signal 20 dB above the minimum
  // sensitivity for MCS 1.
  Set("CcaEdThreshold", DoubleValue(-48)); // CCA-ED for non-802.11 signals.
}

WigigPhyHelper::~WigigPhyHelper() {}

void WigigPhyHelper::SetChannel(Ptr<WigigChannel> channel) {
  m_channel = channel;
}

void WigigPhyHelper::SetChannel(std::string channelName) {
  Ptr<WigigChannel> channel = Names::Find<WigigChannel>(channelName);
  m_channel = channel;
}

void WigigPhyHelper::Set(std::string name, const AttributeValue &v) {
  m_phy.Set(name, v);
}

void WigigPhyHelper::SetErrorRateModel(std::string name, std::string n0,
                                       const AttributeValue &v0, std::string n1,
                                       const AttributeValue &v1, std::string n2,
                                       const AttributeValue &v2, std::string n3,
                                       const AttributeValue &v3, std::string n4,
                                       const AttributeValue &v4, std::string n5,
                                       const AttributeValue &v5, std::string n6,
                                       const AttributeValue &v6, std::string n7,
                                       const AttributeValue &v7) {
  m_errorRateModel = ObjectFactory();
  m_errorRateModel.SetTypeId(name);
  m_errorRateModel.Set(n0, v0);
  m_errorRateModel.Set(n1, v1);
  m_errorRateModel.Set(n2, v2);
  m_errorRateModel.Set(n3, v3);
  m_errorRateModel.Set(n4, v4);
  m_errorRateModel.Set(n5, v5);
  m_errorRateModel.Set(n6, v6);
  m_errorRateModel.Set(n7, v7);
}

void WigigPhyHelper::SetFrameCaptureModel(
    std::string name, std::string n0, const AttributeValue &v0, std::string n1,
    const AttributeValue &v1, std::string n2, const AttributeValue &v2,
    std::string n3, const AttributeValue &v3, std::string n4,
    const AttributeValue &v4, std::string n5, const AttributeValue &v5,
    std::string n6, const AttributeValue &v6, std::string n7,
    const AttributeValue &v7) {
  m_frameCaptureModel = ObjectFactory();
  m_frameCaptureModel.SetTypeId(name);
  m_frameCaptureModel.Set(n0, v0);
  m_frameCaptureModel.Set(n1, v1);
  m_frameCaptureModel.Set(n2, v2);
  m_frameCaptureModel.Set(n3, v3);
  m_frameCaptureModel.Set(n4, v4);
  m_frameCaptureModel.Set(n5, v5);
  m_frameCaptureModel.Set(n6, v6);
  m_frameCaptureModel.Set(n7, v7);
}

void WigigPhyHelper::SetPreambleDetectionModel(
    std::string name, std::string n0, const AttributeValue &v0, std::string n1,
    const AttributeValue &v1, std::string n2, const AttributeValue &v2,
    std::string n3, const AttributeValue &v3, std::string n4,
    const AttributeValue &v4, std::string n5, const AttributeValue &v5,
    std::string n6, const AttributeValue &v6, std::string n7,
    const AttributeValue &v7) {
  m_preambleDetectionModel = ObjectFactory();
  m_preambleDetectionModel.SetTypeId(name);
  m_preambleDetectionModel.Set(n0, v0);
  m_preambleDetectionModel.Set(n1, v1);
  m_preambleDetectionModel.Set(n2, v2);
  m_preambleDetectionModel.Set(n3, v3);
  m_preambleDetectionModel.Set(n4, v4);
  m_preambleDetectionModel.Set(n5, v5);
  m_preambleDetectionModel.Set(n6, v6);
  m_preambleDetectionModel.Set(n7, v7);
}

void WigigPhyHelper::DisablePreambleDetectionModel() {
  m_preambleDetectionModel.SetTypeId(TypeId());
}

void WigigPhyHelper::PcapSniffTxEvent(Ptr<PcapFileWrapper> file,
                                      Ptr<const Packet> packet,
                                      uint16_t channelFreqMhz,
                                      const WigigTxVector &txVector,
                                      MpduInfo aMpdu) {
  uint32_t dlt = file->GetDataLinkType();
  switch (dlt) {
  case PcapHelper::DLT_IEEE802_11:
    file->Write(Simulator::Now(), packet);
    return;
  case PcapHelper::DLT_PRISM_HEADER: {
    NS_FATAL_ERROR("PcapSniffTxEvent(): DLT_PRISM_HEADER not implemented");
    return;
  }
  case PcapHelper::DLT_IEEE802_11_RADIO: {
    Ptr<Packet> p = packet->Copy();
    RadiotapHeader header =
        GetRadiotapHeader(p, channelFreqMhz, txVector, aMpdu);
    p->AddHeader(header);
    file->Write(Simulator::Now(), p);
    return;
  }
  default:
    NS_ABORT_MSG("PcapSniffTxEvent(): Unexpected data link type " << dlt);
  }
}

void WigigPhyHelper::PcapSniffRxEvent(Ptr<PcapFileWrapper> file,
                                      Ptr<const Packet> packet,
                                      uint16_t channelFreqMhz,
                                      const WigigTxVector &txVector,
                                      MpduInfo aMpdu,
                                      SignalNoiseDbm signalNoise) {
  uint32_t dlt = file->GetDataLinkType();
  switch (dlt) {
  case PcapHelper::DLT_IEEE802_11:
    file->Write(Simulator::Now(), packet);
    return;
  case PcapHelper::DLT_PRISM_HEADER: {
    NS_FATAL_ERROR("PcapSniffRxEvent(): DLT_PRISM_HEADER not implemented");
    return;
  }
  case PcapHelper::DLT_IEEE802_11_RADIO: {
    Ptr<Packet> p = packet->Copy();
    RadiotapHeader header =
        GetRadiotapHeader(p, channelFreqMhz, txVector, aMpdu);
    header.SetAntennaSignalPower(signalNoise.signal);
    header.SetAntennaNoisePower(signalNoise.noise);
    p->AddHeader(header);
    file->Write(Simulator::Now(), p);
    return;
  }
  default:
    NS_ABORT_MSG("PcapSniffRxEvent(): Unexpected data link type " << dlt);
  }
}

RadiotapHeader WigigPhyHelper::GetRadiotapHeader(Ptr<Packet> packet,
                                                 uint16_t channelFreqMhz,
                                                 const WigigTxVector &txVector,
                                                 MpduInfo aMpdu) {
  RadiotapHeader header;
  WifiPreamble preamble = txVector.GetPreambleType();

  uint8_t frameFlags = RadiotapHeader::FRAME_FLAG_NONE;
  header.SetTsft(Simulator::Now().GetMicroSeconds());

  // Our capture includes the FCS, so we set the flag to say so.
  frameFlags |= RadiotapHeader::FRAME_FLAG_FCS_INCLUDED;

  if (preamble == WIFI_PREAMBLE_SHORT) {
    frameFlags |= RadiotapHeader::FRAME_FLAG_SHORT_PREAMBLE;
  }

  if (txVector.GetGuardInterval() == 400) {
    frameFlags |= RadiotapHeader::FRAME_FLAG_SHORT_GUARD;
  }

  header.SetFrameFlags(frameFlags);

  uint64_t rate = 0;
  if (txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_CTRL &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_SC &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_OFDM &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_LP_SC) {
    rate = txVector.GetMode().GetDataRate(txVector.GetChannelWidth(),
                                          txVector.GetGuardInterval(), 1) *
           txVector.GetNss() / 500000;
    header.SetRate(static_cast<uint8_t>(rate));
  }

  uint16_t channelFlags = 0;
  if (txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_CTRL &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_SC &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_OFDM &&
      txVector.GetMode().GetModulationClass() != WIFI_MOD_CLASS_DMG_LP_SC) {
    switch (rate) {
    case 2:  // 1Mbps
    case 4:  // 2Mbps
    case 10: // 5Mbps
    case 22: // 11Mbps
      channelFlags |= RadiotapHeader::CHANNEL_FLAG_CCK;
      break;
    default:
      channelFlags |= RadiotapHeader::CHANNEL_FLAG_OFDM;
      break;
    }

    if (channelFreqMhz < 2500) {
      channelFlags |= RadiotapHeader::CHANNEL_FLAG_SPECTRUM_2GHZ;
    } else {
      channelFlags |= RadiotapHeader::CHANNEL_FLAG_SPECTRUM_5GHZ;
    }
  }
  header.SetChannelFrequencyAndFlags(channelFreqMhz, channelFlags);

  if ((txVector.GetMode().GetModulationClass() == WIFI_MOD_CLASS_DMG_CTRL) ||
      (txVector.GetMode().GetModulationClass() == WIFI_MOD_CLASS_DMG_SC) ||
      (txVector.GetMode().GetModulationClass() == WIFI_MOD_CLASS_DMG_LP_SC) ||
      (txVector.GetMode().GetModulationClass() == WIFI_MOD_CLASS_DMG_OFDM)) {
    uint8_t mcsKnown = RadiotapHeader::MCS_KNOWN_NONE;
    uint8_t mcsFlags = RadiotapHeader::MCS_FLAGS_NONE;
    mcsKnown |= RadiotapHeader::MCS_KNOWN_INDEX;
    header.SetMcsFields(mcsKnown, mcsFlags, txVector.GetMode().GetMcsValue());
  }

  if (txVector.IsAggregation()) {
    uint16_t ampduStatusFlags = RadiotapHeader::A_MPDU_STATUS_NONE;
    ampduStatusFlags |= RadiotapHeader::A_MPDU_STATUS_LAST_KNOWN;
    /* For PCAP file, MPDU Delimiter and Padding should be removed by the MAC
     * Driver */
    AmpduSubframeHeader hdr;
    uint32_t extractedLength;
    packet->RemoveHeader(hdr);
    extractedLength = hdr.GetLength();
    packet = packet->CreateFragment(0, static_cast<uint32_t>(extractedLength));
    if (aMpdu.type == LAST_MPDU_IN_AGGREGATE ||
        (hdr.GetEof() && hdr.GetLength() > 0)) {
      ampduStatusFlags |= RadiotapHeader::A_MPDU_STATUS_LAST;
    }
    header.SetAmpduStatus(aMpdu.mpduRefNumber, ampduStatusFlags, 1 /*CRC*/);
  }

  return header;
}

void WigigPhyHelper::SetPcapDataLinkType(SupportedPcapDataLinkTypes dlt) {
  switch (dlt) {
  case DLT_IEEE802_11:
    m_pcapDlt = PcapHelper::DLT_IEEE802_11;
    return;
  case DLT_PRISM_HEADER:
    m_pcapDlt = PcapHelper::DLT_PRISM_HEADER;
    return;
  case DLT_IEEE802_11_RADIO:
    m_pcapDlt = PcapHelper::DLT_IEEE802_11_RADIO;
    return;
  default:
    NS_ABORT_MSG("WigigPhyHelper::SetPcapFormat(): Unexpected format");
  }
}

void WigigPhyHelper::SetAsciiTraceType(SupportedAsciiTraceTypes traceType) {
  m_asciiTraceType = traceType;
}

void WigigPhyHelper::SetSnapshotLength(uint32_t length) { m_snaplen = length; }

PcapHelper::DataLinkType WigigPhyHelper::GetPcapDataLinkType() const {
  return m_pcapDlt;
}

uint32_t WigigPhyHelper::GetSnapshotLength() const { return m_snaplen; }

void WigigPhyHelper::EnablePcapInternal(std::string prefix, Ptr<NetDevice> nd,
                                        bool promiscuous,
                                        bool explicitFilename) {
  NS_LOG_FUNCTION(this << prefix << nd << promiscuous << explicitFilename);

  // All of the Pcap enable functions vector through here including the ones
  // that are wandering through all of devices on perhaps all of the nodes in
  // the system. We can only deal with devices of type WifiNetDevice.
  Ptr<WigigNetDevice> device = nd->GetObject<WigigNetDevice>();
  if (!device) {
    NS_LOG_INFO("WifiHelper::EnablePcapInternal(): Device "
                << &device << " not of type ns3::WifiNetDevice");
    return;
  }

  Ptr<WigigPhy> phy = device->GetPhy();
  NS_ABORT_MSG_IF(!phy, "WigigPhyHelper::EnablePcapInternal(): Phy layer in "
                        "WifiNetDevice must be set");

  PcapHelper pcapHelper;

  std::string filename;
  if (explicitFilename) {
    filename = prefix;
  } else {
    filename = pcapHelper.GetFilenameFromDevice(prefix, device);
  }

  Ptr<PcapFileWrapper> file =
      pcapHelper.CreateFile(filename, std::ios::out, m_pcapDlt, m_snaplen);

  phy->TraceConnectWithoutContext(
      "MonitorSnifferTx",
      MakeBoundCallback(&WigigPhyHelper::PcapSniffTxEvent, file));
  phy->TraceConnectWithoutContext(
      "MonitorSnifferRx",
      MakeBoundCallback(&WigigPhyHelper::PcapSniffRxEvent, file));
}

void WigigPhyHelper::EnableAsciiInternal(Ptr<OutputStreamWrapper> stream,
                                         std::string prefix, Ptr<NetDevice> nd,
                                         bool explicitFilename) {
  // All of the ASCII enable functions vector through here including the ones
  // that are wandering through all of devices on perhaps all of the nodes in
  // the system. We can only deal with devices of type WifiNetDevice.
  Ptr<WigigNetDevice> device = nd->GetObject<WigigNetDevice>();
  if (!device) {
    NS_LOG_INFO("WifiHelper::EnableAsciiInternal(): Device "
                << device << " not of type ns3::WigigNetDevice");
    return;
  }

  if (m_asciiTraceType == ASCII_TRACE_LEGACY) {
    // Our trace sinks are going to use packet printing, so we have to make sure
    // that is turned on.
    Packet::EnablePrinting();

    uint32_t nodeid = nd->GetNode()->GetId();
    uint32_t deviceid = nd->GetIfIndex();
    std::ostringstream oss;

    // If we are not provided an OutputStreamWrapper, we are expected to create
    // one using the usual trace filename conventions and write our traces
    // without a context since there will be one file per context and therefore
    // the context would be redundant.
    if (!stream) {
      // Set up an output stream object to deal with private ofstream copy
      // constructor and lifetime issues. Let the helper decide the actual
      // name of the file given the prefix.
      AsciiTraceHelper asciiTraceHelper;

      std::string filename;
      if (explicitFilename) {
        filename = prefix;
      } else {
        filename = asciiTraceHelper.GetFilenameFromDevice(prefix, device);
      }

      Ptr<OutputStreamWrapper> theStream =
          asciiTraceHelper.CreateFileStream(filename);
      // We could go poking through the phy and the state looking for the
      // correct trace source, but we can let Config deal with that with
      // some search cost.  Since this is presumably happening at topology
      // creation time, it doesn't seem much of a price to pay.
      oss.str("");
      oss << "/NodeList/" << nodeid << "/DeviceList/" << deviceid
          << "/$ns3::WifiNetDevice/Phy/State/RxOk";
      Config::ConnectWithoutContext(
          oss.str(),
          MakeBoundCallback(&AsciiPhyReceiveSinkWithoutContext, theStream));

      oss.str("");
      oss << "/NodeList/" << nodeid << "/DeviceList/" << deviceid
          << "/$ns3::WifiNetDevice/Phy/State/Tx";
      Config::ConnectWithoutContext(
          oss.str(),
          MakeBoundCallback(&AsciiPhyTransmitSinkWithoutContext, theStream));

      return;
    }

    // If we are provided an OutputStreamWrapper, we are expected to use it, and
    // to provide a context. We are free to come up with our own context if we
    // want, and use the AsciiTraceHelper Hook*WithContext functions, but for
    // compatibility and simplicity, we just use Config::Connect and let it deal
    // with coming up with a context.
    oss.str("");
    oss << "/NodeList/" << nodeid << "/DeviceList/" << deviceid
        << "/$ns3::WigigNetDevice/Phy/State/RxOk";
    Config::Connect(oss.str(),
                    MakeBoundCallback(&AsciiPhyReceiveSinkWithContext, stream));

    oss.str("");
    oss << "/NodeList/" << nodeid << "/DeviceList/" << deviceid
        << "/$ns3::WigigNetDevice/Phy/State/Tx";
    Config::Connect(
        oss.str(), MakeBoundCallback(&AsciiPhyTransmitSinkWithContext, stream));
  } else if (m_asciiTraceType == ASCII_TRACE_PHY_ACTIVITY) {
    Ptr<Channel> channel = device->GetChannel();
    NS_ABORT_MSG_IF(!channel, "WigigPhyHelper::EnableAsciiInternal(): Channel "
                              "in WifiNetDevice must be set");

    // Set up an output stream object to deal with private ofstream copy
    // constructor and lifetime issues. Let the helper decide the actual
    // name of the file given the prefix.
    AsciiTraceHelper asciiTraceHelper;

    std::string filename;
    if (explicitFilename) {
      filename = prefix;
    } else {
      filename = asciiTraceHelper.GetFilenameFromDevice(prefix, device);
    }

    Ptr<OutputStreamWrapper> theStream =
        asciiTraceHelper.CreateFileStream(filename);
    channel->TraceConnectWithoutContext(
        "PhyActivityTracker",
        MakeBoundCallback(&AsciiPhyActivityTraceWithoutContext, theStream));
  } else {
    NS_ABORT_MSG("Unsupported ASCII Trace Type");
  }
}

Ptr<WigigPhy> WigigPhyHelper::Create(Ptr<Node> node,
                                     Ptr<NetDevice> device) const {
  Ptr<WigigPhy> phy = m_phy.Create<WigigPhy>();
  Ptr<ErrorRateModel> error = m_errorRateModel.Create<ErrorRateModel>();
  phy->SetErrorRateModel(error);
  phy->SetChannel(m_channel);
  phy->SetDevice(device);
  return phy;
}

WigigHelper::WigigHelper()
    : m_standard(WIFI_STANDARD_80211ad),
      m_selectQueueCallback(&SelectQueueByDSField) {
  SetRemoteStationManager("ns3::ConstantRateWigigManager", "ControlMode",
                          StringValue("DmgMcs4"), "DataMode",
                          StringValue("DmgMcs12"));
  SetAckPolicySelectorForAc(AC_BE, "ns3::ConstantWigigAckPolicySelector");
  SetAckPolicySelectorForAc(AC_BK, "ns3::ConstantWigigAckPolicySelector");
  SetAckPolicySelectorForAc(AC_VI, "ns3::ConstantWigigAckPolicySelector");
  SetAckPolicySelectorForAc(AC_VO, "ns3::ConstantWigigAckPolicySelector");
}

WigigHelper::~WigigHelper() {}

void WigigHelper::SetRemoteStationManager(
    std::string type, std::string n0, const AttributeValue &v0, std::string n1,
    const AttributeValue &v1, std::string n2, const AttributeValue &v2,
    std::string n3, const AttributeValue &v3, std::string n4,
    const AttributeValue &v4, std::string n5, const AttributeValue &v5,
    std::string n6, const AttributeValue &v6, std::string n7,
    const AttributeValue &v7) {
  m_stationManager = ObjectFactory();
  m_stationManager.SetTypeId(type);
  m_stationManager.Set(n0, v0);
  m_stationManager.Set(n1, v1);
  m_stationManager.Set(n2, v2);
  m_stationManager.Set(n3, v3);
  m_stationManager.Set(n4, v4);
  m_stationManager.Set(n5, v5);
  m_stationManager.Set(n6, v6);
  m_stationManager.Set(n7, v7);
}

void WigigHelper::SetAckPolicySelectorForAc(
    AcIndex ac, std::string type, std::string n0, const AttributeValue &v0,
    std::string n1, const AttributeValue &v1, std::string n2,
    const AttributeValue &v2, std::string n3, const AttributeValue &v3,
    std::string n4, const AttributeValue &v4, std::string n5,
    const AttributeValue &v5, std::string n6, const AttributeValue &v6,
    std::string n7, const AttributeValue &v7) {
  m_ackPolicySelector[ac] = ObjectFactory();
  m_ackPolicySelector[ac].SetTypeId(type);
  m_ackPolicySelector[ac].Set(n0, v0);
  m_ackPolicySelector[ac].Set(n1, v1);
  m_ackPolicySelector[ac].Set(n2, v2);
  m_ackPolicySelector[ac].Set(n3, v3);
  m_ackPolicySelector[ac].Set(n4, v4);
  m_ackPolicySelector[ac].Set(n5, v5);
  m_ackPolicySelector[ac].Set(n6, v6);
  m_ackPolicySelector[ac].Set(n7, v7);
}

void WigigHelper::SetSelectQueueCallback(SelectQueueCallback f) {
  m_selectQueueCallback = f;
}

NetDeviceContainer WigigHelper::Install(const WigigPhyHelper &phyHelper,
                                        const WigigMacHelper &macHelper,
                                        NodeContainer::Iterator first,
                                        NodeContainer::Iterator last) const {
  NetDeviceContainer devices;
  for (NodeContainer::Iterator i = first; i != last; ++i) {
    Ptr<Node> node = *i;
    Ptr<WigigNetDevice> device = CreateObject<WigigNetDevice>();
    Ptr<WigigRemoteStationManager> manager =
        m_stationManager.Create<WigigRemoteStationManager>();
    Ptr<WigigMac> mac = StaticCast<WigigMac>(macHelper.Create(device));
    Ptr<WigigPhy> phy = StaticCast<WigigPhy>(phyHelper.Create(node, device));
    Ptr<Codebook> codebook = m_codeBook.Create<Codebook>();
    codebook->SetDevice(device);
    mac->SetAddress(Mac48Address::Allocate());
    mac->ConfigureStandard(m_standard);
    mac->SetCodebook(codebook);
    phy->SetCodebook(codebook);
    phy->ConfigureStandard(m_standard);
    device->SetMac(mac);
    device->SetPhy(phy);
    device->SetRemoteStationManager(manager);
    node->AddDevice(device);
    devices.Add(device);
    NS_LOG_DEBUG("node=" << node
                         << ", mob=" << node->GetObject<MobilityModel>());

    Ptr<NetDeviceQueueInterface> ndqi;
    PointerValue ptr;
    Ptr<WigigMacQueue> wmq;
    Ptr<WigigAckPolicySelector> ackSelector;

    ndqi = CreateObjectWithAttributes<NetDeviceQueueInterface>(
        "NTxQueues", UintegerValue(4));

    mac->GetAttributeFailSafe("BE_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_BE].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(0)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("BK_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_BK].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(1)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("VI_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_VI].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(2)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("VO_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_VO].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(3)->ConnectQueueTraces(wmq);
    ndqi->SetSelectQueueCallback(m_selectQueueCallback);

    device->AggregateObject(ndqi);
  }
  return devices;
}

NetDeviceContainer WigigHelper::Install(const WigigPhyHelper &phyHelper,
                                        const WigigMacHelper &macHelper,
                                        NodeContainer c) const {
  return Install(phyHelper, macHelper, c.Begin(), c.End());
}

NetDeviceContainer WigigHelper::Install(const WigigPhyHelper &phy,
                                        const WigigMacHelper &mac,
                                        Ptr<Node> node) const {
  return Install(phy, mac, NodeContainer(node));
}

NetDeviceContainer WigigHelper::Install(const WigigPhyHelper &phy,
                                        const WigigMacHelper &mac,
                                        std::string nodeName) const {
  Ptr<Node> node = Names::Find<Node>(nodeName);
  return Install(phy, mac, NodeContainer(node));
}

void WigigHelper::SetCodebook(std::string name, std::string n0,
                              const AttributeValue &v0, std::string n1,
                              const AttributeValue &v1, std::string n2,
                              const AttributeValue &v2, std::string n3,
                              const AttributeValue &v3, std::string n4,
                              const AttributeValue &v4, std::string n5,
                              const AttributeValue &v5, std::string n6,
                              const AttributeValue &v6, std::string n7,
                              const AttributeValue &v7) {
  m_codeBook = ObjectFactory();
  m_codeBook.SetTypeId(name);
  m_codeBook.Set(n0, v0);
  m_codeBook.Set(n1, v1);
  m_codeBook.Set(n2, v2);
  m_codeBook.Set(n3, v3);
  m_codeBook.Set(n4, v4);
  m_codeBook.Set(n5, v5);
  m_codeBook.Set(n6, v6);
  m_codeBook.Set(n7, v7);
}

NetDeviceContainer WigigHelper::Install(const SpectrumWigigPhyHelper &phyHelper,
                                        const WigigMacHelper &macHelper,
                                        NodeContainer::Iterator first,
                                        NodeContainer::Iterator last,
                                        bool installCodebook) const {
  NetDeviceContainer devices;
  for (NodeContainer::Iterator i = first; i != last; ++i) {
    Ptr<Node> node = *i;
    Ptr<WigigNetDevice> device = CreateObject<WigigNetDevice>();
    Ptr<WigigRemoteStationManager> manager =
        m_stationManager.Create<WigigRemoteStationManager>();
    Ptr<WigigMac> mac = StaticCast<WigigMac>(macHelper.Create(device));
    Ptr<SpectrumWigigPhy> phy =
        StaticCast<SpectrumWigigPhy>(phyHelper.Create(node, device));
    mac->SetAddress(Mac48Address::Allocate());
    mac->ConfigureStandard(m_standard);
    if (installCodebook) {
      Ptr<Codebook> codebook = m_codeBook.Create<Codebook>();
      mac->SetCodebook(codebook);
      phy->SetCodebook(codebook);
    }
    phy->ConfigureStandard(m_standard);
    device->SetMac(mac);
    device->SetPhy(phy);
    device->SetRemoteStationManager(manager);
    node->AddDevice(device);
    devices.Add(device);

    Ptr<NetDeviceQueueInterface> ndqi;
    PointerValue ptr;
    Ptr<WigigMacQueue> wmq;
    Ptr<WigigAckPolicySelector> ackSelector;

    ndqi = CreateObjectWithAttributes<NetDeviceQueueInterface>(
        "NTxQueues", UintegerValue(4));

    mac->GetAttributeFailSafe("BE_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_BE].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(0)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("BK_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_BK].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(1)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("VI_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_VI].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(2)->ConnectQueueTraces(wmq);

    mac->GetAttributeFailSafe("VO_Txop", ptr);
    ackSelector = m_ackPolicySelector[AC_VO].Create<WigigAckPolicySelector>();
    ackSelector->SetQosTxop(ptr.Get<WigigQosTxop>());
    ptr.Get<WigigQosTxop>()->SetAckPolicySelector(ackSelector);
    wmq = ptr.Get<WigigQosTxop>()->GetWigigMacQueue();
    ndqi->GetTxQueue(3)->ConnectQueueTraces(wmq);
    ndqi->SetSelectQueueCallback(m_selectQueueCallback);

    device->AggregateObject(ndqi);
  }
  return devices;
}

NetDeviceContainer WigigHelper::Install(const SpectrumWigigPhyHelper &phyHelper,
                                        const WigigMacHelper &macHelper,
                                        NodeContainer c,
                                        bool installCodebook) const {
  return Install(phyHelper, macHelper, c.Begin(), c.End(), installCodebook);
}

NetDeviceContainer WigigHelper::Install(const SpectrumWigigPhyHelper &phy,
                                        const WigigMacHelper &mac,
                                        Ptr<Node> node,
                                        bool installCodebook) const {
  return Install(phy, mac, NodeContainer(node), installCodebook);
}

NetDeviceContainer WigigHelper::Install(const SpectrumWigigPhyHelper &phy,
                                        const WigigMacHelper &mac,
                                        std::string nodeName,
                                        bool installCodebook) const {
  Ptr<Node> node = Names::Find<Node>(nodeName);
  return Install(phy, mac, NodeContainer(node), installCodebook);
}

void WigigHelper::EnableLogComponents() {
  WifiHelper::EnableLogComponents();
  LogComponentEnable("WigigHelper", LOG_LEVEL_ALL);
  LogComponentEnable("WigigBlockAckAgreement", LOG_LEVEL_ALL);
  LogComponentEnable("BlockAckCache", LOG_LEVEL_ALL);
  LogComponentEnable("WigigBlockAckManager", LOG_LEVEL_ALL);
  LogComponentEnable("WigigChannelAccessManager", LOG_LEVEL_ALL);
  LogComponentEnable("Codebook", LOG_LEVEL_ALL);
  LogComponentEnable("CodebookAnalytical", LOG_LEVEL_ALL);
  LogComponentEnable("CodebookNumerical", LOG_LEVEL_ALL);
  LogComponentEnable("CodebookParametric", LOG_LEVEL_ALL);
  LogComponentEnable("ConstantWigigAckPolicySelector", LOG_LEVEL_ALL);
  LogComponentEnable("AdhocWigigMac", LOG_LEVEL_ALL);
  LogComponentEnable("ApWigigMac", LOG_LEVEL_ALL);
  LogComponentEnable("DmgAtiTxop", LOG_LEVEL_ALL);
  LogComponentEnable("DmgBeaconTxop", LOG_LEVEL_ALL);
  LogComponentEnable("DmgErrorModel", LOG_LEVEL_ALL);
  LogComponentEnable("DmgSlsTxop", LOG_LEVEL_ALL);
  LogComponentEnable("StaWigigMac", LOG_LEVEL_ALL);
  LogComponentEnable("WigigChannel", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMac", LOG_LEVEL_ALL);
  LogComponentEnable("WigigPhy", LOG_LEVEL_ALL);
  LogComponentEnable("WigigSpectrumPhyInterface", LOG_LEVEL_ALL);
  LogComponentEnable("IdealWigigManager", LOG_LEVEL_ALL);
  LogComponentEnable("WigigInterferenceHelper", LOG_LEVEL_ALL);
  LogComponentEnable("MacLow", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMacRxMiddle", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMpduAggregator", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMsduAggregator", LOG_LEVEL_ALL);
  LogComponentEnable("WigigOriginatorBlockAckAgreement", LOG_LEVEL_ALL);
  LogComponentEnable("QdPropagationDelayModel", LOG_LEVEL_ALL);
  LogComponentEnable("QdPropagationEngine", LOG_LEVEL_ALL);
  LogComponentEnable("QdPropagationLossModel", LOG_LEVEL_ALL);
  LogComponentEnable("WigigQosTxop", LOG_LEVEL_ALL);
  LogComponentEnable("RfChain", LOG_LEVEL_ALL);
  LogComponentEnable("SimpleWigigFrameCaptureModel", LOG_LEVEL_ALL);
  LogComponentEnable("SpectrumWigigPhy", LOG_LEVEL_ALL);
  LogComponentEnable("WigigTxop", LOG_LEVEL_ALL);
  LogComponentEnable("WigigAckPolicySelector", LOG_LEVEL_ALL);
  LogComponentEnable("WigigNetDevice", LOG_LEVEL_ALL);
  LogComponentEnable("WigigPhyStateHelper", LOG_LEVEL_ALL);
  LogComponentEnable("WigigPpdu", LOG_LEVEL_ALL);
  LogComponentEnable("WigigPsdu", LOG_LEVEL_ALL);
  LogComponentEnable("WigigRemoteStationManager", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMacQueueItem", LOG_LEVEL_ALL);
  LogComponentEnable("WigigMacQueue", LOG_LEVEL_ALL);
}

int64_t WigigHelper::AssignStreams(NetDeviceContainer c, int64_t stream) {
  int64_t currentStream = stream;
  Ptr<NetDevice> netDevice;
  for (NetDeviceContainer::Iterator i = c.Begin(); i != c.End(); ++i) {
    netDevice = (*i);
    Ptr<WigigNetDevice> wifi = DynamicCast<WigigNetDevice>(netDevice);
    if (wifi) {
      // Handle any random numbers in the PHY objects.
      currentStream += wifi->GetPhy()->AssignStreams(currentStream);

      // Handle any random numbers in the MAC objects.
      Ptr<WigigMac> mac = wifi->GetMac();
      PointerValue ptr;

      mac->GetAttribute("Txop", ptr);
      Ptr<WigigTxop> txop = ptr.Get<WigigTxop>();
      currentStream += txop->AssignStreams(currentStream);

      mac->GetAttribute("VO_Txop", ptr);
      Ptr<WigigQosTxop> vo_txop = ptr.Get<WigigQosTxop>();
      currentStream += vo_txop->AssignStreams(currentStream);

      mac->GetAttribute("VI_Txop", ptr);
      Ptr<WigigQosTxop> vi_txop = ptr.Get<WigigQosTxop>();
      currentStream += vi_txop->AssignStreams(currentStream);

      mac->GetAttribute("BE_Txop", ptr);
      Ptr<WigigQosTxop> be_txop = ptr.Get<WigigQosTxop>();
      currentStream += be_txop->AssignStreams(currentStream);

      mac->GetAttribute("BK_Txop", ptr);
      Ptr<WigigQosTxop> bk_txop = ptr.Get<WigigQosTxop>();
      currentStream += bk_txop->AssignStreams(currentStream);
    }
  }
  return (currentStream - stream);
}

SpectrumWigigPhyHelper::SpectrumWigigPhyHelper(const std::string &errorModel)
    : WigigPhyHelper(errorModel) {
  m_phy.SetTypeId("ns3::SpectrumWigigPhy");
}

void SpectrumWigigPhyHelper::SetChannel(Ptr<SpectrumChannel> channel) {
  m_channel = channel;
}

void SpectrumWigigPhyHelper::SetChannel(std::string channelName) {
  Ptr<SpectrumChannel> channel = Names::Find<SpectrumChannel>(channelName);
  m_channel = channel;
}

Ptr<WigigPhy> SpectrumWigigPhyHelper::Create(Ptr<Node> node,
                                             Ptr<NetDevice> device) const {
  Ptr<SpectrumWigigPhy> phy = m_phy.Create<SpectrumWigigPhy>();
  phy->CreateWifiSpectrumPhyInterface(device);
  Ptr<ErrorRateModel> error = m_errorRateModel.Create<ErrorRateModel>();
  phy->SetErrorRateModel(error);
  phy->SetChannel(m_channel);
  phy->SetDevice(device);
  phy->SetMobility(node->GetObject<MobilityModel>());
  return phy;
}

} // namespace ns3
