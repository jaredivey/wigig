/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#include "wigig-mac-helper.h"

#include "ns3/boolean.h"
#include "ns3/wigig-mac.h"

namespace ns3 {

WigigMacHelper::WigigMacHelper() {
  SetType("ns3::StaWigigMac", "DmgSupported", BooleanValue(true));
}

WigigMacHelper::~WigigMacHelper() {}

Ptr<WigigMac> WigigMacHelper::Create(Ptr<NetDevice> device) const {
  Ptr<WigigMac> mac = m_mac.Create<WigigMac>();
  mac->SetDevice(device);
  return mac;
}

} // namespace ns3
