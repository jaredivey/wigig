/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <iomanip>

/**
 * Simulation Objective:
 * Evaluate beamformed link maintenance procedure for allocated Service Periods.
 *
 * Network Topology:
 * The scenario consists of 2 DMG STAs (West + East) and one PCP/AP as
 * following:
 *
 *                        DMG PCP/AP (0,1)
 *
 *
 * West DMG STA (-1,0)                      East DMG STA (1,0)
 *
 * Simulation Description:
 * Once all the stations have associated successfully with the DMG PCP/AP.
 * The PCP/AP allocates a service period to perform TXSS between the two
 * stations. Once West DMG STA has completed SLS TXSS with East DMG STA,
 * the DMG PCP/AP allocates one static service periods for communication
 * as following:
 *
 * SP: DMG West STA -----> DMG East STA (SP Length = 3.2ms)
 *
 * Running the Simulation:
 * ./waf --run wigig-test-beamformedlink-maintenance-example
 *
 * Output:
 * From the PCAP files, we can see that data transmission takes place during the
 * SPs. In addition, we can notice the announcement of two static service period
 * allocations inside each DMG Beacon.
 */

NS_LOG_COMPONENT_DEFINE("TestBeamFormedLinkMaintenance");

using namespace ns3;
using namespace std;

/* Network Nodes */
NetDeviceContainer staDevices;
Ptr<WigigNetDevice> apWigigNetDevice, westWigigNetDevice, eastWigigNetDevice;
Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> westWigigMac, eastWigigMac;
Ptr<WigigPhy> westWigigPhy, eastWigigPhy;
Ptr<WigigChannel> mmWaveChannel;

/*** Access Point Variables ***/
uint8_t associatedStations =
    0; /* Total number of associated stations with the DMG PCP/AP. */
uint8_t stationsTrained = 0;         /* Number of BF trained stations. */
bool scheduledStaticPeriods = false; /* Flag to indicate whether we scheduled
                                        Static Service Periods or not. */

/*** Service Periods ***/
uint16_t spDuration =
    10000; /* The duration of the allocated service period in MicroSeconds. */

void CalculateThroughput(Ptr<PacketSink> sink, uint64_t lastTotalRx,
                         double averageThroughput) {
  Time now = Simulator::Now();
  double cur = static_cast<double>(sink->GetTotalRx() - lastTotalRx) * 8.0 /
               1e5; /* Convert Application RX Packets to MBits. */
  std::cout << std::left << std::setw(12) << now.GetSeconds() << std::left
            << std::setw(12) << cur << std::endl;
  lastTotalRx = sink->GetTotalRx();
  averageThroughput += cur;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput, sink,
                      lastTotalRx, averageThroughput);
}

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA " << staWigigMac->GetAddress()
            << " associated with DMG PCP/AP " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  associatedStations++;
  /* Map AID to MAC Addresses in each node instead of requesting information */
  Ptr<StaWigigMac> dmgStaMac;
  for (NetDeviceContainer::Iterator i = staDevices.Begin();
       i != staDevices.End(); ++i) {
    dmgStaMac =
        StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*i)->GetMac());
    if (dmgStaMac != staWigigMac) {
      dmgStaMac->MapAidToMacAddress(staWigigMac->GetAssociationId(),
                                    staWigigMac->GetAddress());
    }
  }

  /* Check if all stations have associated with the DMG PCP/AP */
  if (associatedStations == 2) {
    /* For simplicity we assume that each station is aware of the capabilities
     * of the peer station */
    /* Otherwise, we have to request the capabilities of the peer station. */
    westWigigMac->StorePeerDmgCapabilities(eastWigigMac);
    eastWigigMac->StorePeerDmgCapabilities(westWigigMac);

    std::cout << "All stations got associated with " << address << std::endl;
    /* Schedule Beamforming Training SP */
    std::cout << "Allocate Beamforming Training SP from SRC AID="
              << westWigigMac->GetAssociationId()
              << " --> DST AID=" << eastWigigMac->GetAssociationId()
              << std::endl;
    apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(), 0,
        true);
  }
}

void SLSCompleted(Ptr<WigigMac> staWigigMac,
                  SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    std::cout << "DMG STA " << staWigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    if (!scheduledStaticPeriods) {
      std::cout << "Schedule Static Periods" << std::endl;
      scheduledStaticPeriods = true;
      /* Schedule two static SPs */
      uint32_t allocationStart = 0;
      /* First Service Period */
      allocationStart = apWigigMac->AllocateSingleContiguousBlock(
          1, SERVICE_PERIOD_ALLOCATION, true, westWigigMac->GetAssociationId(),
          eastWigigMac->GetAssociationId(), allocationStart, spDuration);

      /* Second Service Period */
      allocationStart = allocationStart + MilliSeconds(2).GetMicroSeconds();
      allocationStart = apWigigMac->AllocateSingleContiguousBlock(
          1, SERVICE_PERIOD_ALLOCATION, true, westWigigMac->GetAssociationId(),
          eastWigigMac->GetAssociationId(), allocationStart, spDuration);
    }
  }
}

void StartBeamformingServicePeriod(Time btDuration) {
  westWigigMac->StartBeamformingTraining(eastWigigMac->GetAssociationId(),
                                         eastWigigMac->GetAddress(), true, true,
                                         true, btDuration);
}

void BeamLinkMaintenanceTimerStateChanged(Ptr<StaWigigMac> wigigMac,
                                          Ptr<OutputStreamWrapper> stream,
                                          BeamLinkMaintenanceTimerState state,
                                          uint8_t aid, Mac48Address address,
                                          Time timeLeft) {
  *stream->GetStream() << Simulator::Now().GetNanoSeconds() << "," << state
                       << "," << uint16_t(aid) << ","
                       << timeLeft.GetMicroSeconds() << std::endl;
  if (state == BEAM_LINK_MAINTENANCE_TIMER_EXPIRES) {
    std::cout << "BeamLink Maintenance Timer Expired for "
              << wigigMac->GetAddress() << " with " << address << std::endl;
  }
}

/************* Functions to intrduce blockage in a communication link
 * *********************/

bool induceBlockage =
    false; /* Flag to indicate if we want to block a link or not */
double m_blockageValue = -100; /* Link loss due to blockage in dBm */

/**
 * Insert Blockage
 * \return The actual value of the blockage we introduce in the simulator.
 */
double DoInsertBlockage() { return m_blockageValue; }

/**
 * Insert Blockage on a certain path from Src -> Destination.
 * \param channel The channel to insert the blockage in.
 * \param srcWigigPhy The source WifiPhy.
 * \param dstWigigPhy The destination WifiPhy.
 */
void BlockLink(Ptr<WigigChannel> channel, Ptr<WigigPhy> srcWigigPhy,
               Ptr<WigigPhy> dstWigigPhy) {
  std::cout << "Blockage Inserted at " << Simulator::Now() << std::endl;
  induceBlockage = true;
  channel->AddBlockage(&DoInsertBlockage, srcWigigPhy, dstWigigPhy);
}

void InduceBlockage() { induceBlockage = true; }

void ServicePeriodStarted(Mac48Address source, Mac48Address destination) {
  if (induceBlockage) {
    std::cout
        << "Service Period for which we induce link blockage has started at "
        << Simulator::Now() << std::endl;
    Simulator::Schedule(MilliSeconds(1), &BlockLink, mmWaveChannel,
                        westWigigPhy, eastWigigPhy);
  }
}

void ServicePeriodEnded(Mac48Address source, Mac48Address destination) {
  if (induceBlockage) {
    std::cout
        << "Service Period for which we induced link blockage has ended at "
        << Simulator::Now() << std::endl;
    induceBlockage = false;
    mmWaveChannel->RemoveBlockage();
  }
}

int main(int argc, char *argv[]) {
  uint32_t payloadSize = 1472; /* Transport Layer Payload size in bytes. */
  string dataRate = "100Mbps"; /* Application Layer Data Rate. */
  string msduAggSize =
      "max"; /* The maximum aggregation size for A-MSDU in Bytes. */
  string mpduAggSize =
      "0"; /* The maximum aggregation size for A-MPDU in Bytes. */
  string queueSize = "4000p"; /* Wigig MAC Queue Size. */
  uint32_t maintenanceUnit =
      0; /* Maintenance Time Unit for the beamformed link. */
  uint32_t maintenanceValue =
      10; /* Maintenance Time Value for the beamformed link. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 1;   /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("payloadSize", "Payload size in bytes", payloadSize);
  cmd.AddValue("dataRate", "Payload size in bytes", dataRate);
  cmd.AddValue("msduAggregation",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("queueSize", "The size of the Wigig Mac Queue", queueSize);
  cmd.AddValue("spDuration", "The duration of service period in MicroSeconds",
               spDuration);
  cmd.AddValue("maintenanceTimeUnit",
               "The unit of beamform meaintenance time: 0 = 32 US, 1 = 2000 US",
               maintenanceUnit);
  cmd.AddValue("maintenanceTimeValue",
               "The value of beamform meaintenance time", maintenanceValue);
  cmd.AddValue("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();
  /* Wigig MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /**** WigigHelper is a meta-helper: it helps creates helpers ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("TestBeamFormedLinkMaintenance", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make four nodes and set them up with the phy and the mac */
  NodeContainer wigigNodes;
  wigigNodes.Create(3);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> westNode = wigigNodes.Get(1);
  Ptr<Node> eastNode = wigigNodes.Get(2);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("Maintenance");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize), "BeamLinkMaintenanceUnit",
                   EnumValue(maintenanceUnit), "BeamLinkMaintenanceValue",
                   UintegerValue(maintenanceValue));

  staDevices =
      wigig.Install(wigigPhy, wigigMac, NodeContainer(westNode, eastNode));

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* DMG PCP/AP */
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0)); /* West DMG STA */
  positionAlloc->Add(Vector(+1.0, 0.0, 0.0)); /* East DMG STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer ApInterface;
  ApInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /*** Install Applications ***/

  /* Install a simple UDP server on the east Node */
  PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 9999));
  ApplicationContainer sinks =
      sinkHelper.Install(NodeContainer(westNode, eastNode));

  /** East Node Variables **/
  uint64_t eastNodeLastTotalRx = 0;
  double eastNodeAverageThroughput = 0;

  /* Install a simple UDP Transmitter on the West Node (Transmit to the East
   * Node) */
  ApplicationContainer container;
  OnOffHelper onoff("ns3::UdpSocketFactory",
                    InetSocketAddress(staInterfaces.GetAddress(1), 9999));
  // onoff.SetAttribute ("MaxPackets", UintegerValue (0));
  onoff.SetAttribute("PacketSize", UintegerValue(payloadSize));
  onoff.SetAttribute("OnTime",
                     StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
  onoff.SetAttribute("OffTime",
                     StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  onoff.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
  container = onoff.Install(westNode);
  container.Start(Seconds(0.0));
  container.Stop(Seconds(simulationTime));

  /* Schedule Throughput Calulcations */
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput,
                      StaticCast<PacketSink>(sinks.Get(1)), eastNodeLastTotalRx,
                      eastNodeAverageThroughput);

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("Traces/AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Traces/WestNode", staDevices.Get(0), false);
    wigigPhy.EnablePcap("Traces/EastNode", staDevices.Get(1), false);
  }

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  westWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  eastWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(1));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  westWigigMac = StaticCast<StaWigigMac>(westWigigNetDevice->GetMac());
  eastWigigMac = StaticCast<StaWigigMac>(eastWigigNetDevice->GetMac());

  mmWaveChannel = StaticCast<WigigChannel>(westWigigNetDevice->GetChannel());
  westWigigPhy = StaticCast<WigigPhy>(westWigigNetDevice->GetPhy());
  eastWigigPhy = StaticCast<WigigPhy>(eastWigigNetDevice->GetPhy());

  /** Connect Traces **/
  westWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, eastWigigMac));
  westWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, eastWigigMac));
  westWigigMac->TraceConnectWithoutContext("ServicePeriodStarted",
                                           MakeCallback(&ServicePeriodStarted));
  westWigigMac->TraceConnectWithoutContext("ServicePeriodEnded",
                                           MakeCallback(&ServicePeriodEnded));

  /* Create Stream for Timer States Traces */
  AsciiTraceHelper ascii; /* ASCII Helper. */
  Ptr<OutputStreamWrapper> outputTimerTraces =
      ascii.CreateFileStream("BeamLinkMaintenanceTimerTraces.csv");
  *outputTimerTraces->GetStream() << "TIME,STATE,AID,TIMELEFT," << std::endl;
  westWigigMac->TraceConnectWithoutContext(
      "BeamLinkMaintenanceTimerStateChanged",
      MakeBoundCallback(&BeamLinkMaintenanceTimerStateChanged, westWigigMac,
                        outputTimerTraces));

  /* Schedule event to introduce link blockage */
  Simulator::Schedule(Seconds(0.5), &InduceBlockage);

  Simulator::Stop(Seconds(simulationTime));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}
