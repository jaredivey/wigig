/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

/**
 * Simulation Objective:
 * Evaluate various options to control different access periods within the
 * Beacon Interval.
 *
 * Network Topology:
 * Network topology is simple and consists of One Access Point + One Station.
 * Each station has one antenna array with eight virtual sectors to cover 360 in
 * the 2D domain.
 *
 *              DMG PCP/AP (0,0)                       DMG STA (-1,0)
 *
 * Running Simulation:
 * To evaluate the script, run the following command:
 * ./waf --run "wigig-evaluate-beacon-interval-example --beaconInterval=102400
 * --nextBeacon=1
 * --beaconRandomization=true
 * --nextAbft=0 --atiPresent=false --simulationTime=10"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 */

NS_LOG_COMPONENT_DEFINE("EvaluateBeaconInterval");

using namespace ns3;
using namespace std;

Ptr<Node> apWigigNode;
Ptr<Node> staWigigNode;

Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> staWigigMac;

int main(int argc, char *argv[]) {
  uint32_t beaconInterval = 102400; /* The interval between two Target Beacon
                                       Transmission Times (TBTTs). */
  bool beaconRandomization =
      false; /* Whether to change the sequence of DMG Beacons at each BI. */
  uint32_t nextBeacon = 0; /* The number of BIs following the current BI during
                              which the DMG Beacon is not be present. */
  uint32_t nextAbft = 0;   /* The number of beacon intervals during which the
                              A-BFT is not be present. */
  uint32_t slotsPerABFT = 8; /* The number of Sector Sweep Slots Per A-BFT. */
  uint32_t sswPerSlot = 8; /* The number of SSW Frames per Sector Sweep Slot. */
  bool atiPresent =
      false; /* Whether the BI period contains ATI access period. */
  uint16_t atiDuration = 300; /* The duration of the ATI access period. */
  bool verbose = false;       /* Print Logging Information. */
  double simulationTime = 4;  /* Simulation time in seconds. */
  bool pcapTracing = true;    /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;

  cmd.AddValue(
      "beaconInterval",
      "The interval between two Target Beacon Transmission Times (TBTTs)",
      beaconInterval);
  cmd.AddValue("beaconRandomization",
               "Whether to change the sequence of DMG Beacons at each BI",
               beaconRandomization);
  cmd.AddValue("nextBeacon",
               "The number of beacon intervals following the current beacon "
               "interval during"
               "which the DMG Beacon is not be present",
               nextBeacon);
  cmd.AddValue(
      "nextAbft",
      "The number of beacon intervals during which the A-BFT is not be present",
      nextAbft);
  cmd.AddValue("slotsPerABFT", "The number of Sector Sweep Slots Per A-BFT",
               slotsPerABFT);
  cmd.AddValue("sswPerSlot", "The number of SSW Frames per Sector Sweep Slot",
               sswPerSlot);
  cmd.AddValue("atiPresent",
               "Flag to indicate if the BI period contains ATI access period",
               atiPresent);
  cmd.AddValue("atiDuration", "The duration of the ATI access period",
               atiDuration);
  cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /**** WigigHelper is a meta-helper: it helps creates helpers ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("EvaluateBeaconInterval", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue("DmgMcs12"));

  /* Make two nodes and set them up with the PHY and the MAC */
  NodeContainer wigigNodes;
  wigigNodes.Create(2);
  apWigigNode = wigigNodes.Get(0);
  staWigigNode = wigigNodes.Get(1);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  Ssid ssid = Ssid("BTI_Test");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BeaconInterval",
      TimeValue(MicroSeconds(beaconInterval)), "EnableBeaconRandomization",
      BooleanValue(beaconRandomization), "NextBeacon",
      UintegerValue(nextBeacon), "NextAbft", UintegerValue(nextAbft),
      "SSSlotsPerAbft", UintegerValue(slotsPerABFT), "SSFramesPerSlot",
      UintegerValue(sswPerSlot), "ATIPresent", BooleanValue(atiPresent),
      "ATIDuration", TimeValue(MicroSeconds(atiDuration)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  /* Create Wigig Network Devices (WifiNetDevice) */
  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apWigigNode);

  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false));

  NetDeviceContainer staDevice;
  staDevice = wigig.Install(wigigPhy, wigigMac, staWigigNode);

  /* Setting mobility model, Initial Position 1 meter apart */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, 0.0, 0.0));
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0));

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterface;
  staInterface = address.Assign(staDevice);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Station", staDevice, false);
  }

  Simulator::Stop(Seconds(simulationTime));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}
