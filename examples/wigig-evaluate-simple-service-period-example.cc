/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

/**
 * Simulation Objective:
 * This script is used to evaluate the throughput achieved using a simple
 * allocation of a static service period for a communication from DMG PCP/AP to
 * a DMG STA.
 *
 * Network Topology:
 * The scenario consists of a single DMG STA and one DMG PCP/AP as following:
 *
 *
 *                  DMG PCP/AP (0,0)          DMG STA (+1,0)
 *
 *
 * Simulation Description:
 * Once the statio have associated successfully with the DMG PCP/AP. The DMG
 * PCP/AP allocates a single static service period for communication as
 * following:
 *
 * SP: DMG PCP/AP -----> DMG STA (SP Length = 3.2 ms)
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run wigig-evaluate-simple-service-period-example
 *
 * To run the script with different duration for the service period e.g.
 * SP=10ms:
 * ./waf --run "wigig-evaluate-simple-service-period-example --spDuration=10000"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see that data
 * transmission takes place during its SP. In addition, we can notice in the
 * announcement of the two Static Allocation Periods inside each DMG Beacon.
 */

NS_LOG_COMPONENT_DEFINE("EvaluateSimpleServicePeriod");

using namespace ns3;
using namespace std;

/**  Application Variables **/
Ptr<PacketSink> packetSink;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> staWigigNetDevice;
Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> staWigigMac;
NetDeviceContainer staDevices;

/*** Service Periods ***/
uint16_t spDuration =
    3200; /* The duration of the allocated service period in MicroSeconds */

void CalculateThroughput(Ptr<PacketSink> sink, uint64_t lastTotalRx,
                         double averageThroughput) {
  Time now = Simulator::Now(); /* Return the simulator's virtual time. */
  double cur = static_cast<double>(sink->GetTotalRx() - lastTotalRx) * 8.0 /
               1e5; /* Convert Application RX Packets to MBits. */
  std::cout << now.GetSeconds() << '\t' << cur << std::endl;
  lastTotalRx = sink->GetTotalRx();
  averageThroughput += cur;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput, sink,
                      lastTotalRx, averageThroughput);
}

void StationAssociated(Mac48Address address, uint16_t aid) {
  std::cout << "DMG STA: " << staWigigMac->GetAddress()
            << " associated with DMG PCP/AP: " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  std::cout << "Schedule Static Service Period (DMG PCP/AP ----> DMG STA)"
            << std::endl;
  /* Schedule Static Periods */
  apWigigMac->AllocateSingleContiguousBlock(1, SERVICE_PERIOD_ALLOCATION, true,
                                            AID_AP, aid, 0, spDuration);
}

/**
 * Callback method to log the number of packets in the Wigig MAC Queue.
 */
void QueueOccupancyChange(Ptr<OutputStreamWrapper> file, uint32_t oldValue,
                          uint32_t newValue) {
  std::ostream *output = file->GetStream();
  *output << Simulator::Now().GetNanoSeconds() << "," << newValue << endl;
}

int main(int argc, char *argv[]) {
  uint32_t packetSize = 1448;  /* Transport Layer Payload size in bytes. */
  string dataRate = "300Mbps"; /* Application Layer Data Rate. */
  uint64_t maxPackets = 0;     /* The maximum number of packets to transmit. */
  string msduAggSize =
      "max"; /* The maximum aggregation size for A-MSDU in Bytes. */
  string mpduAggSize =
      "0"; /* The maximum aggregation size for A-MPDU in Bytes. */
  string queueSize = "4000p";  /* Wigig MAC Queue Size. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 10;  /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";
  string codebookFolder = "contrib/wigig/model/reference/Codebook";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("packetSize", "Payload size in bytes", packetSize);
  cmd.AddValue("dataRate", "Data rate for OnOff Application", dataRate);
  cmd.AddValue("maxPackets", "The maximum number of packets to transmit",
               maxPackets);
  cmd.AddValue("msduAggSize",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("queueSize", "The size of the Wigig Mac Queue", queueSize);
  cmd.AddValue("spDuration", "The duration of service period in MicroSeconds",
               spDuration);
  cmd.AddValue("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.AddValue("codebookFolder", "Channel for cookbooks", codebookFolder);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();
  /* Wigig MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /**** WigigHelper is a meta-helper ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("EvaluateSimpleServicePeriod", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make four nodes and set them up with the phy and the mac */
  NodeContainer wigigNodes;
  wigigNodes.Create(2);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> staNode = wigigNodes.Get(1);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("ServicePeriod");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize));

  staDevices = wigig.Install(wigigPhy, wigigMac, staNode);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, 0.0, 0.0)); /* PCP/AP */
  positionAlloc->Add(Vector(1.0, 0.0, 0.0)); /* DMG STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /* Install Simple UDP Server on the STA */
  PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 9999));
  ApplicationContainer sinks = sinkHelper.Install(staNode);
  packetSink = StaticCast<PacketSink>(sinks.Get(0));

  /** STA Node Variables **/
  uint64_t staNodeLastTotalRx = 0;
  double staNodeAverageThroughput = 0;

  /* Install Simple UDP Transmitter on the DMG PCP/AP */
  Ptr<OnOffApplication> onoff;
  ApplicationContainer srcApp;
  OnOffHelper src("ns3::UdpSocketFactory",
                  InetSocketAddress(staInterfaces.GetAddress(0), 9999));
  // src.SetAttribute ("MaxPackets", UintegerValue (maxPackets));
  src.SetAttribute("PacketSize", UintegerValue(packetSize));
  src.SetAttribute("OnTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
  src.SetAttribute("OffTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
  srcApp = src.Install(apNode);
  onoff = StaticCast<OnOffApplication>(srcApp.Get(0));

  /* Schedule Applications */
  srcApp.Start(Seconds(1.0));
  srcApp.Stop(Seconds(simulationTime));
  sinks.Start(Seconds(1.0));

  /* Schedule Throughput Calulcations */
  Simulator::Schedule(Seconds(1.1), &CalculateThroughput, packetSink,
                      staNodeLastTotalRx, staNodeAverageThroughput);

  /* Connect Wigig MAC Queue Occupancy */
  AsciiTraceHelper asciiTraceHelper;
  Ptr<OutputStreamWrapper> queueOccupanyStream;
  /* Trace DMG PCP/AP MAC Queue Changes */
  queueOccupanyStream =
      asciiTraceHelper.CreateFileStream("AccessPointMacQueueOccupany.txt");
  // Config::ConnectWithoutContext
  // ("/NodeList/0/DeviceList/*/$ns3::WigigNetDevice/Mac/$ns3::WigigMac/BE_QosTxop/Queue/OccupancyChanged",
  //                                MakeBoundCallback (&QueueOccupancyChange,
  //                                queueOccupanyStream));

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("Traces/AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Traces/STA", staDevices.Get(0), false);
  }

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  /** Connect Traces **/
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  staWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  staWigigMac = StaticCast<StaWigigMac>(staWigigNetDevice->GetMac());
  apWigigMac->TraceConnectWithoutContext("StationAssociated",
                                         MakeCallback(&StationAssociated));

  Simulator::Stop(Seconds(simulationTime + 0.101));
  Simulator::Run();
  Simulator::Destroy();

  /* Print per flow statistics */
  PrintFlowMonitorStatistics(flowmon, monitor, simulationTime - 1);

  /* Print Application Layer Results Summary */
  std::cout << "\nApplication Layer Statistics:" << std::endl;
  ;
  // std::cout << "  Tx Packets: " << onoff->GetTotalTxPackets () << std::endl;
  // std::cout << "  Tx Bytes:   " << onoff->GetTotalTxBytes () << std::endl;
  // std::cout << "  Rx Packets: " << packetSink->GetTotalReceivedPackets () <<
  // std::endl;
  std::cout << "  Rx Bytes:   " << packetSink->GetTotalRx() << std::endl;
  std::cout << "  Throughput: "
            << packetSink->GetTotalRx() * 8.0 / ((simulationTime - 1) * 1e6)
            << " Mbps" << std::endl;

  return 0;
}
