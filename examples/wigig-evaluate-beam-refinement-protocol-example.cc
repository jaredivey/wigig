/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

/**
 * Simulation Objective:
 * This script is used to evaluate the beam refinement protocol (BRP) in IEEE
 * 802.11ad.
 *
 * Network Topology:
 * The scenario consists of 2 DMG STAs (West + East) and one PCP/AP as
 * following:
 *
 *                         DMG AP (0,1)
 *
 *
 * West DMG STA (-1,0)                      East DMG STA (1,0)
 *
 * Simulation Description:
 * Once all the stations have associated successfully with the PCP/AP, the
 * PCP/AP allocates one SP to perform Beamforming Training (TXSS) between West
 * DMG STA and East DMG STA. Once the SLS TXSS is compelted, West DMG STA
 * initiate BRP to refine its beam pattern.
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run wigig-evaluate-beam-refinement-protocol-example
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see the
 * allocation of beamforming service periods.
 * 2. BRP ASCII traces.
 */

NS_LOG_COMPONENT_DEFINE("BRP_Protocol");

using namespace ns3;
using namespace std;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> westWigigNetDevice;
Ptr<WigigNetDevice> eastWigigNetDevice;

NetDeviceContainer staDevices;

Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> westWigigMac;
Ptr<StaWigigMac> eastWigigMac;

/*** Access Point Variables ***/
uint8_t associatedStations =
    0; /* Total number of associated stations with the AP */
uint8_t stationsTrained = 0;         /* Number of BF trained stations */
bool scheduledStaticPeriods = false; /* Flag to indicate whether we scheduled
                                        Static Service Periods or not */

/*** Beamforming Service Periods ***/
uint8_t beamformedLinks = 0; /* Number of beamformed links */

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA " << staWigigMac->GetAddress()
            << " associated with DMG AP " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  associatedStations++;
  /* Map AID to MAC Addresses in each node instead of requesting information */
  Ptr<StaWigigMac> dmgStaMac;
  for (NetDeviceContainer::Iterator i = staDevices.Begin();
       i != staDevices.End(); ++i) {
    dmgStaMac =
        StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*i)->GetMac());
    if (dmgStaMac != staWigigMac) {
      dmgStaMac->MapAidToMacAddress(staWigigMac->GetAssociationId(),
                                    staWigigMac->GetAddress());
      staWigigMac->StorePeerDmgCapabilities(dmgStaMac);
    }
  }

  /* Check if all stations have associated with the PCP/AP */
  if (associatedStations == 2) {
    std::cout << "All stations got associated with " << address << std::endl;
    /*** Schedule Beamforming Training SPs ***/
    uint32_t startTime = 0;
    startTime = apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(),
        startTime, true);
  }
}

void SLSCompleted(Ptr<WigigMac> wigigMac, SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_BHI) {
    if (wigigMac == apWigigMac) {
      std::cout << "DMG AP " << apWigigMac->GetAddress()
                << " completed SLS phase with DMG STA "
                << attributes.peerStation << std::endl;
    } else {
      std::cout << "DMG STA " << wigigMac->GetAddress()
                << " completed SLS phase with DMG AP " << attributes.peerStation
                << std::endl;
    }
    std::cout << "Best Tx Antenna Configuration: AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
  } else if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    beamformedLinks++;
    std::cout << "DMG STA " << wigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    if (beamformedLinks == 2) {
      // apWigigMac->PrintSnrTable ();
      // westWigigMac->PrintSnrTable ();
      // eastWigigMac->PrintSnrTable ();
      std::cout << "West DMG STA " << wigigMac->GetAddress()
                << " initiating BRP Transaction with DMG STA "
                << attributes.peerStation << std::endl;
      Simulator::Schedule(MicroSeconds(3), &StaWigigMac::InitiateBrpTransaction,
                          westWigigMac, eastWigigMac->GetAddress(), 0, true);
    }
  }
}

void BRPCompleted(Ptr<WigigMac> wigigMac, Mac48Address address,
                  BeamRefinementType type, AntennaId antennaID,
                  SectorId sectorID, AwvId awvID) {
  std::cout << "DMG STA " << wigigMac->GetAddress();
  if (type == RefineTransmitSector) {
    std::cout << " completed BRP-TX ";
  } else {
    std::cout << " completed BRP-RX ";
  }
  std::cout << "with DMG STA " << address << std::endl;
  std::cout << "Best beamforming configuration: Custom AWV ID="
            << uint16_t(awvID) << ", AntennaId=" << uint16_t(antennaID)
            << ", AntennaId=" << uint16_t(sectorID) << std::endl;
  // eastWigigMac->PrintBeamRefinementMeasurements ();
}

int main(int argc, char *argv[]) {
  uint16_t sectors = 8;       /* The number of sectors in the antenna array. */
  uint16_t awvs = 8;          /* The number of custom Awvs per sector. */
  bool verbose = false;       /* Print Logging Information. */
  double simulationTime = 10; /* Simulation time in seconds. */
  bool pcapTracing = false;   /* PCAP Tracing is enabled or not. */
  bool asciiTracing = false;  /* ASCII Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("sectors", "The number of sectors in the antenna array",
               sectors);
  cmd.AddValue("awvs", "The number of custom Awvs per sector", awvs);
  cmd.AddValue("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("ascii", "Enable ASCII Tracing", asciiTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();

  /**** WigigHelper is a meta-helper ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("BRP_Protocol", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue("DmgMcs12"));

  /* Make four nodes and set them up with the phy and the mac */
  NodeContainer wigigNodes;
  wigigNodes.Create(3);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> westNode = wigigNodes.Get(1);
  Ptr<Node> eastNode = wigigNodes.Get(2);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("BRP");
  wigigMac.SetType("ns3::ApWigigMac", "Ssid", SsidValue(ssid),
                   "BE_MaxAmpduSize", UintegerValue(0), "SSSlotsPerAbft",
                   UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
                   "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(sectors), "Awvs",
                    UintegerValue(awvs));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize", UintegerValue(0));

  staDevices =
      wigig.Install(wigigPhy, wigigMac, NodeContainer(westNode, eastNode));

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* PCP/AP */
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0)); /* West STA */
  positionAlloc->Add(Vector(+1.0, 0.0, 0.0)); /* East STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer ApInterface;
  ApInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /* Enable PCAP Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("Traces/AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Traces/WestNode", staDevices.Get(0), false);
    wigigPhy.EnablePcap("Traces/EastNode", staDevices.Get(1), false);
  }

  /* Enable ASCII Traces */
  if (asciiTracing) {
    wigigPhy.SetAsciiTraceType(ASCII_TRACE_PHY_ACTIVITY);
    wigigPhy.EnableAscii("Traces/AccessPoint", apDevice);
    //      wigigPhy.EnableAscii ("Traces/WestNode", staDevices.Get (0));
    //      wigigPhy.EnableAscii ("Traces/EastNode", staDevices.Get (1));
  }

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  westWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  eastWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(1));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  westWigigMac = StaticCast<StaWigigMac>(westWigigNetDevice->GetMac());
  eastWigigMac = StaticCast<StaWigigMac>(eastWigigNetDevice->GetMac());

  /** Connect Traces **/
  westWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, eastWigigMac));
  apWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, apWigigMac));
  westWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, westWigigMac));
  westWigigMac->TraceConnectWithoutContext(
      "BRPCompleted", MakeBoundCallback(&BRPCompleted, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, eastWigigMac));

  Simulator::Stop(Seconds(simulationTime));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}
