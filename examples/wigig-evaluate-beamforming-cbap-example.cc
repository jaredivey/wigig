/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <iomanip>

/**
 * Simulation Objective:
 * Evaluate contention-based TXSS SLS beamforming training in the DTI channel
 * access period.
 *
 * Network Topology:
 * The scenario consists of 2 DMG STAs (West + East) and one PCP/AP as
 * following:
 *
 *                         DMG AP  (0,+1)
 *
 *
 *                         DMG STA (0,-1)
 *
 * Simulation Description:
 * Once all the stations have associated successfully with the PCP/AP, the
 * PCP/AP allocates three SPs to perform Beamforming Training (TXSS) as
 * following:
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run wigig-evaluate-beamforming-cbap-example
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see the
 * allocation of beamforming service periods.
 * 2. SNR Dump for each sector.
 */

NS_LOG_COMPONENT_DEFINE("BeamformingCBAP");

using namespace ns3;
using namespace std;

/**  Application Variables **/
uint64_t totalRx = 0;
double throughput = 0;
Ptr<PacketSink> packetSink;
Ptr<OnOffApplication> onoff;
Ptr<BulkSendApplication> bulk;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> staWigigNetDevice;

NetDeviceContainer staDevices;

Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> staWigigMac;

/* Flow monitor */
Ptr<FlowMonitor> monitor;

/*** Access Point Variables ***/
uint8_t stationsTrained = 0; /* Number of BF trained stations */

/*** Beamforming Service Periods ***/
uint8_t beamformedLinks = 0; /* Number of beamformed links */

void CalculateThroughput() {
  double thr = CalculateSingleStreamThroughput(packetSink, totalRx, throughput);
  std::cout << std::left << std::setw(12) << Simulator::Now().GetSeconds()
            << std::left << std::setw(12) << thr << std::endl;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput);
}

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA " << staWigigMac->GetAddress()
            << " associated with DMG AP " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  staWigigMac->PerformTxssTxop(apWigigMac->GetAddress());
}

void SLSCompleted(Ptr<WigigMac> wigigMac, SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_BHI) {
    if (wigigMac == apWigigMac) {
      std::cout << "DMG AP " << apWigigMac->GetAddress()
                << " completed SLS phase with DMG STA "
                << attributes.peerStation << std::endl;
    } else {
      std::cout << "DMG STA " << wigigMac->GetAddress()
                << " completed SLS phase with DMG AP " << attributes.peerStation
                << std::endl;
    }
    std::cout << "Best Tx Antenna Configuration: AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
  } else if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    beamformedLinks++;
    std::cout << "DMG STA " << wigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    if (beamformedLinks == 2) {
      // apWigigMac->PrintSnrTable ();
      // staWigigMac->PrintSnrTable ();
    }
  }
}

void ActiveTxSectorIdChanged(Ptr<WigigMac> wigigMac, SectorId oldSectorId,
                             SectorId newSectorId) {
  //  std::cout << "DMG STA: " << wigigMac->GetAddress () << " , SectorId=" <<
  //  uint16_t (newSectorId) << std::endl;
}

int main(int argc, char *argv[]) {
  bool activateApp =
      true; /* Flag to indicate whether we activate onoff or bulk App */
  string applicationType = "bulk";             /* Type of the Tx application */
  string socketType = "ns3::TcpSocketFactory"; /* Socket Type (TCP/UDP) */
  uint32_t packetSize = 1448;    /* Application payload size in bytes. */
  string dataRate = "300Mbps";   /* Application data rate. */
  string tcpVariant = "NewReno"; /* TCP Variant Type. */
  uint32_t bufferSize = 131072;  /* TCP Send/Receive Buffer Size. */
  uint32_t maxPackets = 0;       /* Maximum Number of Packets */
  string msduAggSize =
      "0"; /* The maximum aggregation size for A-MSDU in Bytes. */
  string mpduAggSize =
      "3000"; /* The maximum aggregation size for A-MSPU in Bytes. */
  string queueSize = "4000p";  /* Wigig MAC Queue Size. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 10;  /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("activateApp", "Whether to activate data transmission or not",
               activateApp);
  cmd.AddValue("applicationType", "Type of the Tx Application: onoff or bulk",
               applicationType);
  cmd.AddValue("packetSize", "Application packet size in bytes", packetSize);
  cmd.AddValue("dataRate", "Application data rate", dataRate);
  cmd.AddValue("maxPackets", "Maximum number of packets to send", maxPackets);
  cmd.AddValue("tcpVariant", TCP_VARIANTS_NAMES, tcpVariant);
  cmd.AddValue(
      "socketType",
      "Type of the Socket (ns3::TcpSocketFactory, ns3::UdpSocketFactory)",
      socketType);
  cmd.AddValue("bufferSize", "TCP Buffer Size (Send/Receive) in Bytes",
               bufferSize);
  cmd.AddValue("msduAggSize",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("mpduAggSize",
               "The maximum aggregation size for A-MPDU in Bytes", mpduAggSize);
  cmd.AddValue("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();
  /* Wigig MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /*** Configure TCP Options ***/
  ConfigureTcpOptions(tcpVariant, packetSize, bufferSize);

  /**** WigigHelper is a meta-helper ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("BeamformingCBAP", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make four nodes and set them up with the phy and the mac */
  NodeContainer wigigNodes;
  wigigNodes.Create(2);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> staNode = wigigNodes.Get(1);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("BeamformingCBAP");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize));

  staDevices = wigig.Install(wigigPhy, wigigMac, staNode);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* DMG PCP/AP */
  positionAlloc->Add(Vector(0.0, -1.0, 0.0)); /* DMG STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  if (activateApp) {
    /* Install Simple UDP Server on the DMG AP */
    PacketSinkHelper sinkHelper(socketType,
                                InetSocketAddress(Ipv4Address::GetAny(), 9999));
    ApplicationContainer sinkApp = sinkHelper.Install(apNode);
    packetSink = StaticCast<PacketSink>(sinkApp.Get(0));
    sinkApp.Start(Seconds(0.0));

    /* Install TCP/UDP Transmitter on the DMG STA */
    Address dest(InetSocketAddress(apInterface.GetAddress(0), 9999));
    ApplicationContainer srcApp;
    if (applicationType == "onoff") {
      OnOffHelper src(socketType, dest);
      src.SetAttribute("MaxPackets", UintegerValue(maxPackets));
      src.SetAttribute("PacketSize", UintegerValue(packetSize));
      src.SetAttribute(
          "OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
      src.SetAttribute("OffTime",
                       StringValue("ns3::ConstantRandomVariable[Constant=0]"));
      src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
      srcApp = src.Install(staNode);
      onoff = StaticCast<OnOffApplication>(srcApp.Get(0));
    } else if (applicationType == "bulk") {
      BulkSendHelper src(socketType, dest);
      srcApp = src.Install(staNode);
      bulk = StaticCast<BulkSendApplication>(srcApp.Get(0));
    }
    srcApp.Start(Seconds(2.0));
    srcApp.Stop(Seconds(simulationTime));
  }

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("Traces/AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Traces/StaNode", staDevices.Get(0), false);
  }

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  staWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  staWigigMac = StaticCast<StaWigigMac>(staWigigNetDevice->GetMac());

  /** Connect Traces **/
  staWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, staWigigMac));
  apWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, apWigigMac));
  staWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, staWigigMac));

  apWigigMac->GetCodebook()->TraceConnectWithoutContext(
      "ActiveTxSectorId",
      MakeBoundCallback(&ActiveTxSectorIdChanged, apWigigMac));
  staWigigMac->GetCodebook()->TraceConnectWithoutContext(
      "ActiveTxSectorId",
      MakeBoundCallback(&ActiveTxSectorIdChanged, staWigigMac));

  FlowMonitorHelper flowmon;
  if (activateApp) {
    /* Install FlowMonitor on all nodes */
    monitor = flowmon.InstallAll();

    /* Print Output*/
    std::cout << std::left << std::setw(12) << "Time [s]" << std::left
              << std::setw(12) << "Throughput [Mbps]" << std::endl;

    /* Schedule Throughput Calulcations */
    Simulator::Schedule(Seconds(2.1), &CalculateThroughput);
  }

  /* Schedule many TXSS CBAPs during the data transmission interval. */
  Simulator::Schedule(Seconds(2.1), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(2.3), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(2.5), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(2.7), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(2.9), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(3.1), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(3.6), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(4.2), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(4.7), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(4.8), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(5.0), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(5.0), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(5.2), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(5.5), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(5.7), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(6.0), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(6.32), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(6.567), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(7.123), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(8.0), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(8.1), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(8.5), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());
  Simulator::Schedule(Seconds(8.5), &WigigMac::PerformTxssTxop, staWigigMac,
                      apWigigMac->GetAddress());

  Simulator::Stop(Seconds(simulationTime + 0.101));
  Simulator::Run();
  Simulator::Destroy();

  if (activateApp) {
    PrintFlowMonitorStatistics(flowmon, monitor, simulationTime - 1);

    /* Print Application Layer Results Summary */
    std::cout << "\nApplication Layer Statistics:" << std::endl;
    if (applicationType == "onoff") {
      // std::cout << "  Tx Packets: " << onoff->GetTotalTxPackets () <<
      // std::endl; std::cout << "  Tx Bytes:   " << onoff->GetTotalTxBytes ()
      // << std::endl;
    } else {
      // std::cout << "  Tx Packets: " << bulk->GetTotalTxPackets () <<
      // std::endl; std::cout << "  Tx Bytes:   " << bulk->GetTotalTxBytes () <<
      // std::endl;
    }
  }

  // std::cout << "  Rx Packets: " << packetSink->GetTotalReceivedPackets () <<
  // std::endl;
  std::cout << "  Rx Bytes:   " << packetSink->GetTotalRx() << std::endl;
  std::cout << "  Throughput: "
            << packetSink->GetTotalRx() * 8.0 / ((simulationTime - 1) * 1e6)
            << " Mbps" << std::endl;

  return 0;
}
