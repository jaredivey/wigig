/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <iomanip>

/**
 * Simulation Objective:
 * Evaluate allocation of static service periods using traffic stream in the
 * IEEE 802.11ad standard.
 *
 * Network Topology:
 * The scenario consists of 3 DMG STAs (West + South + East) and one DMG PCP/AP
 * as following:
 *
 *                         DMG AP (0,1)
 *
 *
 * West DMG STA (-1,0)                      East DMG STA (1,0)
 *
 *
 *                      South DMG STA (0,-1)
 *
 * Simulation Description:
 * Once all the stations have associated successfully with the PCP/AP. The
 * PCP/AP allocates three SPs to perform SLS (TXSS) between all the stations.
 * Once West DMG STA has completed TXSS phase with East and South DMG STAs. The
 * West DMG STA sends two ADDTS Request for SP allocations request as following:
 *
 * Traffic Format = ISOCHRONOUS Traffic Type (Periodic Traffic)
 * Allocation Period = BI/4 i.e. 4 SPs per BI.
 * Single SP Allocation Duration = 3.2ms
 *
 * SP1: West DMG STA -----> East DMG STA
 * SP2: West DMG STA -----> South DMG STA
 *
 * The PCP/AP takes care of positioning the SPs within the BI.
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run wigig-evaluate-trafficstream-allocation-example
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see that data
 * transmission takes place during its SP. In addition, we can notice the
 * announcement of two static allocation periods inside each DMG Beacon.
 * 2. Summary for the total number of received packets and the total throughput
 * during each service period.
 */

NS_LOG_COMPONENT_DEFINE("EvaluateTrafficStreamAllocation");

using namespace ns3;
using namespace std;

/** West-East Allocation Variables **/
uint64_t westEastLastTotalRx = 0;
double westEastAverageThroughput = 0;
/** West-South Node Allocation Variables **/
uint64_t westSouthTotalRx = 0;
double westSouthAverageThroughput = 0;

Ptr<PacketSink> sink1, sink2;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> southWigigNetDevice;
Ptr<WigigNetDevice> westWigigNetDevice;
Ptr<WigigNetDevice> eastWigigNetDevice;
NetDeviceContainer staDevices;
Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> southWigigMac;
Ptr<StaWigigMac> westWigigMac;
Ptr<StaWigigMac> eastWigigMac;

/*** Access Point Variables ***/
uint8_t associatedStations =
    0; /* Total number of associated stations with the AP. */
uint8_t stationsTrained = 0;         /* Number of BF trained stations. */
bool scheduledStaticPeriods = false; /* Flag to indicate whether we scheduled
                                        Static Service Periods or not. */

/*** Service Period ***/
uint16_t spDuration = 3200; /* The duration of the allocated service period
                               block in MicroSeconds. */

void CalculateThroughput() {
  double thr1;
  double thr2;
  string duration =
      to_string_with_precision<double>(Simulator::Now().GetSeconds() - 0.1, 1) +
      " - " +
      to_string_with_precision<double>(Simulator::Now().GetSeconds(), 1);
  thr1 = CalculateSingleStreamThroughput(sink1, westEastLastTotalRx,
                                         westEastAverageThroughput);
  thr2 = CalculateSingleStreamThroughput(sink2, westSouthTotalRx,
                                         westSouthAverageThroughput);
  std::cout << std::left << std::setw(12) << duration << std::left
            << std::setw(12) << thr1 << std::left << std::setw(12) << thr2
            << std::endl;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput);
}

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA " << staWigigMac->GetAddress()
            << " associated with DMG PCP/AP " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  associatedStations++;
  /* Check if all stations have associated with the PCP/AP */
  if (associatedStations == 3) {
    /* Map AID to MAC Addresses in each node instead of requesting information
     */
    Ptr<StaWigigMac> srcMac;
    Ptr<StaWigigMac> dstMac;
    for (NetDeviceContainer::Iterator i = staDevices.Begin();
         i != staDevices.End(); ++i) {
      srcMac =
          StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*i)->GetMac());
      for (NetDeviceContainer::Iterator j = staDevices.Begin();
           j != staDevices.End(); ++j) {
        dstMac =
            StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*j)->GetMac());
        if (srcMac != dstMac) {
          srcMac->MapAidToMacAddress(dstMac->GetAssociationId(),
                                     dstMac->GetAddress());
        }
      }
    }

    std::cout << "All stations got associated with " << address << std::endl;

    /* For simplicity we assume that each station is aware of the capabilities
     * of the peer station */
    /* Otherwise, we have to request the capabilities of the peer station. */
    westWigigMac->StorePeerDmgCapabilities(eastWigigMac);
    westWigigMac->StorePeerDmgCapabilities(southWigigMac);
    eastWigigMac->StorePeerDmgCapabilities(westWigigMac);
    eastWigigMac->StorePeerDmgCapabilities(southWigigMac);
    southWigigMac->StorePeerDmgCapabilities(westWigigMac);
    southWigigMac->StorePeerDmgCapabilities(eastWigigMac);

    /* Schedule Beamforming Training SP */
    uint32_t allocationStart = 0;
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(),
        allocationStart, true);
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), southWigigMac->GetAssociationId(),
        allocationStart, true);
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        southWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(),
        allocationStart, true);
  }
}

DmgTspecElement CreateTimeAllocationRequest(AllocationFormat format,
                                            uint8_t destAid,
                                            bool multipleAllocation,
                                            uint16_t period,
                                            uint16_t spDuration) {
  DmgTspecElement element;

  DmgAllocationInfo info;
  info.SetAllocationId(10);
  info.SetAllocationType(SERVICE_PERIOD_ALLOCATION);
  info.SetAllocationFormat(format);
  info.SetAsPseudoStatic(true);
  info.SetAsTruncatable(false);
  info.SetAsExtendable(false);
  info.SetLpScUsed(false);
  info.SetUp(0);
  info.SetDestinationAid(destAid);
  element.SetDmgAllocationInfo(info);

  BfControlField bfField;
  bfField.SetBeamformTraining(false); // This SP for data communication
  element.SetBfControl(bfField);

  /* For more deetails on the meaning of this field refer to IEEE
   * 802.11-2012ad 10.4.13*/
  element.SetAllocationPeriod(period, multipleAllocation);
  element.SetMinimumAllocation(spDuration * period);
  element.SetMaximumAllocation(spDuration * period * 2);
  element.SetMinimumDuration(spDuration);

  return element;
}

void SLSCompleted(Ptr<WigigMac> staWigigMac,
                  SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    std::cout << "DMG STA " << staWigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    if ((westWigigMac->GetAddress() == staWigigMac->GetAddress()) &&
        ((southWigigMac->GetAddress() == attributes.peerStation) ||
         (eastWigigMac->GetAddress() == attributes.peerStation))) {
      stationsTrained++;
    }
    if ((stationsTrained == 2) & !scheduledStaticPeriods) {
      std::cout << "West DMG STA " << staWigigMac->GetAddress()
                << " completed SLS phase with South and East DMG STAs "
                << std::endl;
      std::cout << "Schedule Static Periods" << std::endl;
      scheduledStaticPeriods = true;
      /* Create Airtime Allocation Requests */
      DmgTspecElement element;
      element = CreateTimeAllocationRequest(
          ISOCHRONOUS, eastWigigMac->GetAssociationId(), false, 4, spDuration);
      westWigigMac->CreateAllocation(element);
      element = CreateTimeAllocationRequest(
          ISOCHRONOUS, southWigigMac->GetAssociationId(), false, 4, spDuration);
      westWigigMac->CreateAllocation(element);
    }
  }
}

void ADDTSReceived(Ptr<ApWigigMac> apWigigMac, Mac48Address address,
                   DmgTspecElement element) {
  DmgAllocationInfo info = element.GetDmgAllocationInfo();
  StatusCode code;
  uint8_t srcAid = apWigigMac->GetStationAid(address);
  /* Decompose Allocation */
  if (info.GetAllocationFormat() == ISOCHRONOUS) {
    if (element.GetAllocationPeriod() >= 1) {
      if (element.IsAllocationPeriodMultipleBi()) {
        /******* Allocation Period = BI * n *******/
      } else {
        /******* Allocation Period = BI / n *******/

        /* Check current allocations for empty slots */
        AllocationFieldList allocationList = apWigigMac->GetAllocationList();
        Time allocationPeriod =
            apWigigMac->GetBeaconInterval() / element.GetAllocationPeriod();
        /**
         * For the time being, we assume all the stations request the same block
         * size so the AP can allocate these blocks one behind the other.
         */
        apWigigMac->AddAllocationPeriod(
            info.GetAllocationId(), SERVICE_PERIOD_ALLOCATION,
            info.IsPseudoStatic(), srcAid, info.GetDestinationAid(),
            uint32_t(spDuration * allocationList.size()), // Start Time
            element.GetMinimumDuration(), // Block Duration (SP Duration that
                                          // makes up the allocation)
            allocationPeriod.GetMicroSeconds(), element.GetAllocationPeriod());

        /* Set status code */
        code.SetSuccess();
      }
    }
  } else if (info.GetAllocationFormat() == ASYNCHRONOUS) {
    /******* Allocation Period = BI * n *******/
  }

  /* The PCP/AP shall transmit the ADDTS Response frame to the STAs identified
   * as source and destination AID of the DMG TSPEC contained in the ADDTS
   * Request frame if the ADDTS Request it is sent by a non-PCP/ non-AP STA. */
  TsDelayElement delayElem;
  Mac48Address destAddress =
      apWigigMac->GetStationAddress(info.GetDestinationAid());
  apWigigMac->SendDmgAddTsResponse(address, code, delayElem, element);
  if (code.IsSuccess()) {
    apWigigMac->SendDmgAddTsResponse(destAddress, code, delayElem, element);
  }
}

void DeleteAllocation(Ptr<StaWigigMac> wigigMac, uint8_t id, uint8_t destAid) {
  DmgAllocationInfo info;
  info.SetAllocationId(id);
  info.SetDestinationAid(destAid);
  wigigMac->DeleteAllocation(0, info);
}

int main(int argc, char *argv[]) {
  uint32_t payloadSize = 1472; /* Transport Layer Payload size in bytes. */
  string dataRate = "300Mbps"; /* Application Layer Data Rate. */
  string msduAggSize =
      "max"; /* The maximum aggregation size for A-MSDU in Bytes. */
  string mpduAggSize =
      "0"; /* The maximum aggregation size for A-MPDU in Bytes. */
  string queueSize = "4000p";  /* Wigig MAC Queue Size. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 10;  /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled or not. */
  uint32_t snapshotLength =
      std::numeric_limits<uint32_t>::max(); /* The maximum PCAP Snapshot Length
                                             */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("payloadSize", "Payload size in bytes", payloadSize);
  cmd.AddValue("dataRate", "Payload size in bytes", dataRate);
  cmd.AddValue("msduAggSize",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("queueSize", "The size of the Wigig Mac Queue", queueSize);
  cmd.AddValue("duration", "The duration of service period in MicroSeconds",
               spDuration);
  cmd.AddValue("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("snapshotLength", "The maximum PCAP Snapshot Length",
               snapshotLength);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();
  /* Wigig MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /**** WigigHelper is a meta-helper: it helps creates helpers ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("EvaluateTrafficStreamAllocation", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** SETUP ALL NODES ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make four nodes and set them up with the phy and the mac */
  NodeContainer wigigNodes;
  wigigNodes.Create(4);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> westNode = wigigNodes.Get(1);
  Ptr<Node> southNode = wigigNodes.Get(2);
  Ptr<Node> eastNode = wigigNodes.Get(3);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("TrafficStream");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize));

  staDevices = wigig.Install(wigigPhy, wigigMac,
                             NodeContainer(westNode, southNode, eastNode));

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* DMG PCP/AP */
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0)); /* DMG STA West */
  positionAlloc->Add(Vector(0.0, -1.0, 0.0)); /* DMG STA South */
  positionAlloc->Add(Vector(+1.0, 0.0, 0.0)); /* DMG STA East */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /*** Install Applications ***/

  /* Install Simple UDP Server on both south and east Node */
  PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 9999));
  ApplicationContainer sinks =
      sinkHelper.Install(NodeContainer(eastNode, southNode));
  sink1 = StaticCast<PacketSink>(sinks.Get(0));
  sink2 = StaticCast<PacketSink>(sinks.Get(1));

  /* Install Simple UDP Transmitter on the West Node (Transmit to the East Node)
   */
  ApplicationContainer srcApp;
  OnOffHelper src("ns3::UdpSocketFactory",
                  InetSocketAddress(staInterfaces.GetAddress(2), 9999));
  // src.SetAttribute ("MaxPackets", UintegerValue (0));
  src.SetAttribute("PacketSize", UintegerValue(payloadSize));
  src.SetAttribute("OnTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
  src.SetAttribute("OffTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
  srcApp = src.Install(westNode);
  srcApp.Start(Seconds(3.0));
  srcApp.Stop(Seconds(simulationTime));

  /* Install Simple UDP Transmitter on the West Node (Transmit to the South
   * Node) */
  src.SetAttribute("Remote", AddressValue(InetSocketAddress(
                                 staInterfaces.GetAddress(1), 9999)));
  srcApp = src.Install(westNode);
  srcApp.Start(Seconds(3.0));
  srcApp.Stop(Seconds(simulationTime));

  /* Schedule Throughput Calulcations */
  Simulator::Schedule(Seconds(3.1), &CalculateThroughput);

  /* Connect Traces */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  westWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  southWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(1));
  eastWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(2));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  westWigigMac = StaticCast<StaWigigMac>(westWigigNetDevice->GetMac());
  southWigigMac = StaticCast<StaWigigMac>(southWigigNetDevice->GetMac());
  eastWigigMac = StaticCast<StaWigigMac>(eastWigigNetDevice->GetMac());

  /* Association Traces */
  westWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, westWigigMac));
  southWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, southWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, eastWigigMac));

  /* Beamforming Training Traces */
  westWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, westWigigMac));
  southWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, southWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, eastWigigMac));

  apWigigMac->TraceConnectWithoutContext(
      "ADDTSReceived", MakeBoundCallback(&ADDTSReceived, apWigigMac));

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.SetSnapshotLength(snapshotLength);
    wigigPhy.EnablePcap("AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("WestNode", staDevices.Get(0), false);
    wigigPhy.EnablePcap("SouthNode", staDevices.Get(1), false);
    wigigPhy.EnablePcap("EastNode", staDevices.Get(2), false);
  }

  /* Print Output*/
  std::cout << std::left << std::setw(12) << "Time [s]" << std::left
            << std::setw(12) << "SP1" << std::left << std::setw(12) << "SP2"
            << std::endl;

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  Simulator::Stop(Seconds(simulationTime + 0.101));
  Simulator::Run();
  Simulator::Destroy();

  /* Print per flow statistics */
  PrintFlowMonitorStatistics(flowmon, monitor, simulationTime - 1);

  /* Print Results Summary */
  std::cout << "Total number of packets received during each service period:"
            << std::endl;
  // std::cout << "SP1 = " << sink1->GetTotalReceivedPackets () << std::endl;
  // std::cout << "SP2 = " << sink2->GetTotalReceivedPackets () << std::endl;

  std::cout << "Total throughput [Mbps] during each service period allocation:"
            << std::endl;
  std::cout << "SP1 = "
            << westEastAverageThroughput / ((simulationTime - 3) * 10)
            << std::endl;
  std::cout << "SP2 = "
            << westSouthAverageThroughput / ((simulationTime - 3) * 10)
            << std::endl;

  return 0;
}
