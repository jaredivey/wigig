/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

/**
 * Simulation Objective:
 * Evaluate the allocation of Beamforming Service Periods in IEEE 802.11ad.
 *
 * Network Topology:
 * The scenario consists of 2 DMG STAs (West + East) and one DMG PCP/AP as
 * following:
 *
 *                       DMG PCP/AP (0,+1)
 *
 *
 * West DMG STA (-1,0)                      East DMG STA (+1,0)
 *
 * Simulation Description:
 * The script simulates the steps required to do beamforming in DTI access
 * period between an initiator and a responder as defined in 802.11ad. During
 * the asoociation phase, each station includes its DMG Capabilities IE in its
 * Association Request frame. Once all the stations have associated successfully
 * with the DMG PCP/AP, the DMG West STA sends an Information Request frame to
 * the DMG PCP/AP to request the capabilities of the DMG East STA. Once this
 * information is available, the DMG West STA sends a request to the MG PCP/AP
 * to allocate a single SP to perform Beamforming Training (TXSS) as following:
 *
 * SP: West DMG STA (TXSS) -------> East DMG STA (TXSS)
 *
 * All devices in the network have different hardware capabiltiies. The DMG
 * PCP/AP has a single phased antenna array with 16 virtual sectors. While the
 * west DMG STA has 2 arrays each of which has 8 virtual sectors. The east DMG
 * STA has a single array with only 6 virtual sectors.
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run "wigig-evaluate-beamforming-itxss-rtxss-example --pcap=1"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see the
 * allocation of beamforming service periods.
 * 2. SNR Dump for each sector.
 */

NS_LOG_COMPONENT_DEFINE("BeamformingTraining");

using namespace ns3;
using namespace std;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> westWigigNetDevice;
Ptr<WigigNetDevice> eastWigigNetDevice;

Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> westWigigMac;
Ptr<StaWigigMac> eastWigigMac;

/*** Access Point Variables ***/
uint8_t associatedStations =
    0; /* Total number of associated stations with the DMG PCP/AP. */
uint8_t stationsTrained = 0;         /* Number of BF trained stations. */
bool scheduledStaticPeriods = false; /* Flag to indicate whether we scheduled
                                        Static Service Periods or not. */

/*** Beamforming Service Periods ***/
uint8_t beamformedLinks = 0; /* Number of beamformed links. */
uint32_t beamformingStartTime =
    0; /* The start time of the next neafmorming SP. */
uint8_t receivedInformation = 0; /* Number of information response received. */

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA: " << staWigigMac->GetAddress()
            << " associated with DMG PCP/AP: " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  associatedStations++;
  /* Check if all stations have associated with the PCP/AP */
  if (associatedStations == 2) {
    std::cout << "All stations got associated with PCP/AP: " << address
              << std::endl;
    /* Create list of Information Element ID to request */
    WifiInformationElementIdList list;
    list.emplace_back(std::make_pair(IE_DMG_CAPABILITIES, 0));
    /* West DMG STA Request information about East STA */
    westWigigMac->RequestInformation(eastWigigMac->GetAddress(), list);
    /* East DMG STA Request information about West STA */
    eastWigigMac->RequestInformation(westWigigMac->GetAddress(), list);
  }
}

DmgTspecElement CreateBeamformingAllocationRequest(AllocationFormat format,
                                                   uint8_t destAid,
                                                   bool isInitiatorTxss,
                                                   bool isResponderTxss,
                                                   uint16_t spDuration) {
  DmgTspecElement element;

  DmgAllocationInfo info;
  info.SetAllocationId(10);
  info.SetAllocationType(SERVICE_PERIOD_ALLOCATION);
  info.SetAllocationFormat(format);
  info.SetAsPseudoStatic(false);
  info.SetAsTruncatable(false);
  info.SetAsExtendable(false);
  info.SetLpScUsed(false);
  info.SetUp(0);
  info.SetDestinationAid(destAid);
  element.SetDmgAllocationInfo(info);

  BfControlField bfField;
  bfField.SetBeamformTraining(true);
  bfField.SetAsInitiatorTXSS(isInitiatorTxss);
  bfField.SetAsResponderTxss(isResponderTxss);
  element.SetBfControl(bfField);

  /* For more deetails on the meaning of this field refer to IEEE
   * 802.11-2012ad 10.4.13*/
  element.SetAllocationPeriod(0, false);
  element.SetMinimumDuration(spDuration);

  return element;
}

void InformationResponseReceived(Ptr<StaWigigMac> staWigigMac,
                                 Mac48Address address) {
  std::cout << "DMG STA=" << staWigigMac->GetAddress()
            << " received Information Response regarding DMG STA=" << address
            << std::endl;
  receivedInformation++;
  if (receivedInformation == 2) {
    /** Create Airtime Allocation Request for Beamforming Training **/
    DmgTspecElement element;
    Time duration;
    /* SP Allocation */
    duration =
        westWigigMac->ComputeBeamformingAllocationSize(address, true, true);
    element = CreateBeamformingAllocationRequest(
        ISOCHRONOUS, eastWigigMac->GetAssociationId(), true, true,
        duration.GetMicroSeconds());
    westWigigMac->CreateAllocation(element);
  }
}

void SLSCompleted(Ptr<WigigMac> wigigMac, SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    beamformedLinks++;
    std::cout << "DMG STA " << wigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    if (beamformedLinks == 2) {
      // apWigigMac->PrintSnrTable ();
      // westWigigMac->PrintSnrTable ();
      // eastWigigMac->PrintSnrTable ();
    }
  }
}

void ADDTSReceived(Ptr<ApWigigMac> apWigigMac, Mac48Address address,
                   DmgTspecElement element) {
  DmgAllocationInfo info = element.GetDmgAllocationInfo();
  uint8_t srcAid = apWigigMac->GetStationAid(address);
  /* Decompose Allocation */
  BfControlField bfControl = element.GetBfControl();
  std::cout
      << "DMG PCP/AP received ADDTS Request for allocating BF Service Period"
      << std::endl;
  beamformingStartTime = apWigigMac->AllocateBeamformingServicePeriod(
      srcAid, info.GetDestinationAid(), beamformingStartTime,
      element.GetMinimumDuration(), bfControl.IsInitiatorTxss(),
      bfControl.IsResponderTxss());

  /* Set status code */
  StatusCode code;
  code.SetSuccess();

  /* The DMG PCP/AP shall transmit the ADDTS Response frame to the STAs
   * identified as source and destination AID of the DMG TSPEC contained in the
   * ADDTS Request frame if the ADDTS Request is sent by a non-PCP/ non-AP STA.
   */
  TsDelayElement delayElem;
  Mac48Address destAddress =
      apWigigMac->GetStationAddress(info.GetDestinationAid());
  apWigigMac->SendDmgAddTsResponse(address, code, delayElem, element);
  apWigigMac->SendDmgAddTsResponse(destAddress, code, delayElem, element);
}

int main(int argc, char *argv[]) {
  bool verbose = false;      /* Print Logging Information. */
  double simulationTime = 1; /* Simulation time in seconds. */
  bool pcapTracing = false;  /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();

  /**** WigigHelper is a meta-helper ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("BeamformingTraining", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue("DmgMcs12"));

  /* Make four nodes and set them up with the PHY and the MAC */
  NodeContainer wigigNodes;
  NetDeviceContainer staDevices;
  wigigNodes.Create(3);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> westNode = wigigNodes.Get(1);
  Ptr<Node> eastNode = wigigNodes.Get(2);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("ServicePeriod");
  wigigMac.SetType("ns3::ApWigigMac", "Ssid", SsidValue(ssid),
                   "BE_MaxAmpduSize", UintegerValue(0), "SSSlotsPerAbft",
                   UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
                   "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG PCP/AP */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(16));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize", UintegerValue(0));

  /* Set Analytical Codebook for the the West Node */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(2),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer staDevice;
  staDevice = wigig.Install(wigigPhy, wigigMac, westNode);
  staDevices.Add(staDevice);

  /* Set Analytical Codebook for the the East Node */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(6));

  staDevice = wigig.Install(wigigPhy, wigigMac, eastNode);
  staDevices.Add(staDevice);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* DMG PCP/AP */
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0)); /* West DMG STA */
  positionAlloc->Add(Vector(+1.0, 0.0, 0.0)); /* East DMG STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer ApInterface;
  ApInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.EnablePcap("AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("WestNode", staDevices.Get(0), false);
    wigigPhy.EnablePcap("EastNode", staDevices.Get(1), false);
  }

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  westWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  eastWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(1));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  westWigigMac = StaticCast<StaWigigMac>(westWigigNetDevice->GetMac());
  eastWigigMac = StaticCast<StaWigigMac>(eastWigigNetDevice->GetMac());

  /** Connect Traces **/
  westWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, eastWigigMac));
  westWigigMac->TraceConnectWithoutContext(
      "InformationResponseReceived",
      MakeBoundCallback(&InformationResponseReceived, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "InformationResponseReceived",
      MakeBoundCallback(&InformationResponseReceived, eastWigigMac));
  westWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, westWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, eastWigigMac));
  apWigigMac->TraceConnectWithoutContext(
      "ADDTSReceived", MakeBoundCallback(&ADDTSReceived, apWigigMac));

  Simulator::Stop(Seconds(simulationTime));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}
