/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <iomanip>

/**
 * Simulation Objective:
 * Compare the performance of the channel access schemes in IEEE 802.11ad.
 * Basically, the simulation compares the achievable throughput between CSMA/CA
 * and SP allocations. The two devices support DMG/EDMG SC and OFDM PHY layers.
 *
 * Network Topology:
 * The scenario consists of single DMG STA and single PCP/AP.
 *
 *          DMG PCP/AP (0,0)                       DMG STA (+1,0)
 *
 * Simulation Description:
 * In the case of CSMA/CA access period, the whole DTI access period is
 * allocated as CBAP. Whereas in the of SP allocation, once the DMG STA has
 * associated successfully with the PCP/AP. The PCP/AP allocates the whole DTI
 * as SP allocation.
 *
 * Running Simulation:
 * To evaluate CSMA/CA channel access scheme:
 * ./waf --run "wigig-compare-access-schemes-example --scheme=1
 * --simulationTime=10 --pcap=true"
 *
 * To evaluate Service Period (SP) channel access scheme:
 * ./waf --run "wigig-compare-access-schemes-example --scheme=0
 * --simulationTime=10 --pcap=true"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 * 2. The achieved throughput during a window of 100 ms.
 */

NS_LOG_COMPONENT_DEFINE("CompareAccessSchemes");

using namespace ns3;
using namespace std;

/**  Application Variables **/
uint64_t totalRx = 0;
double throughput = 0;
Ptr<PacketSink> packetSink;

/* Network Nodes */
Ptr<Node> apWigigNode;
Ptr<Node> staWigigNode;
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> staWigigNetDevice;
Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> staWigigMac;

/* Access Period Parameters */
uint32_t allocationType =
    CBAP_ALLOCATION; /* The type of channel access scheme during DTI (CBAP is
                        the default). */

void CalculateThroughput() {
  double thr = CalculateSingleStreamThroughput(packetSink, totalRx, throughput);
  std::cout << std::left << std::setw(12) << Simulator::Now().GetSeconds()
            << std::left << std::setw(12) << thr << std::endl;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput);
}

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA " << staWigigMac->GetAddress()
            << " associated with DMG AP " << address << std::endl;
  std::cout << "Association ID (AID) = " << aid << std::endl;
  if (allocationType == SERVICE_PERIOD_ALLOCATION) {
    std::cout << "Allocate DTI as Service Period" << std::endl;
    apWigigMac->AllocateDtiAsServicePeriod(1, staWigigMac->GetAssociationId(),
                                           AID_AP);
  }
}

int main(int argc, char *argv[]) {
  uint32_t payloadSize = 1472; /* Application payload size in bytes. */
  string dataRate = "300Mbps"; /* Application data rate. */
  string msduAggSize =
      "max"; /* The maximum aggregation size for A-MSDU in Bytes. */
  string mpduAggSize =
      "max"; /* The maximum aggregation size for A-MPDU in Bytes. */
  bool enableRts = false; /* Flag to indicate if RTS/CTS handskahre is enabled
                             or disabled. */
  uint32_t rtsThreshold = 0;   /* RTS/CTS handshake threshold. */
  string queueSize = "4000p";  /* Wigig MAC Queue Size. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 2;   /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled. */
  uint32_t snapshotLength =
      std::numeric_limits<uint32_t>::max(); /* The maximum PCAP Snapshot Length.
                                             */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("payloadSize", "Application payload size in bytes", payloadSize);
  cmd.AddValue("dataRate", "Application data rate", dataRate);
  cmd.AddValue("msduAggSize",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("mpduAggSize",
               "The maximum aggregation size for A-MPDU in Bytes", mpduAggSize);
  cmd.AddValue("scheme",
               "The access scheme used for channel access (0: SP allocation, "
               "1: CBAP allocation)",
               allocationType);
  cmd.AddValue("enableRts", "Enable or disable RTS/CTS handshake", enableRts);
  cmd.AddValue("rtsThreshold", "The RTS/CTS threshold value", rtsThreshold);
  cmd.AddValue("queueSize", "The maximum size of the Wigig MAC Queue",
               queueSize);
  cmd.AddValue("phyMode", "The WiGig PHY Mode", phyMode);
  cmd.AddValue("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("snapshotLength", "The maximum PCAP snapshot length",
               snapshotLength);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation(enableRts, rtsThreshold);
  /* Wifi MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /**** WigigHelper is a meta-helper: it helps creates helpers ****/
  WigigHelper wigig;
  // wigig.EnableLogComponents ();

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("CompareAccessSchemes", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Add support for the OFDM PHY */
  wigigPhy.Set("SupportOfdmPhy", BooleanValue(true));

  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make two nodes and set them up with the PHY and the MAC */
  NodeContainer wigigNodes;
  wigigNodes.Create(2);
  apWigigNode = wigigNodes.Get(0);
  staWigigNode = wigigNodes.Get(1);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  Ssid ssid = Ssid("Compare");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  /* Create Wigig Network Devices (WifiNetDevice) */
  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apWigigNode);

  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize));

  NetDeviceContainer staDevice;
  staDevice = wigig.Install(wigigPhy, wigigMac, staWigigNode);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, 0.0, 0.0)); /* PCP/AP */
  positionAlloc->Add(Vector(1.0, 0.0, 0.0)); /* DMG STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterface;
  staInterface = address.Assign(staDevice);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /* Install Simple UDP Server on the DMG AP */
  PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 9999));
  ApplicationContainer sinkApp = sinkHelper.Install(apWigigNode);
  packetSink = StaticCast<PacketSink>(sinkApp.Get(0));
  sinkApp.Start(Seconds(0.0));

  /* Install UDP Transmitter on the DMG STA */
  ApplicationContainer srcApp;
  OnOffHelper src("ns3::UdpSocketFactory",
                  InetSocketAddress(apInterface.GetAddress(0), 9999));
  // src.SetAttribute ("MaxPackets", UintegerValue (0));
  src.SetAttribute("PacketSize", UintegerValue(payloadSize));
  src.SetAttribute("OnTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
  src.SetAttribute("OffTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
  srcApp = src.Install(staWigigNode);
  srcApp.Start(Seconds(1.0));
  srcApp.Stop(Seconds(simulationTime));

  /* Print Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);

    wigigPhy.EnablePcap("AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("Station", staDevice, false);
  }

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  staWigigNetDevice = StaticCast<WigigNetDevice>(staDevice.Get(0));
  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  staWigigMac = StaticCast<StaWigigMac>(staWigigNetDevice->GetMac());

  /* Connect Traces */
  staWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, staWigigMac));

  /* Print Output*/
  std::cout << std::left << std::setw(12) << "Time [s]" << std::left
            << std::setw(12) << "Throughput [Mbps]" << std::endl;

  /* Schedule Throughput Calulcations */
  Simulator::Schedule(Seconds(1.1), &CalculateThroughput);

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor;
  monitor = flowmon.InstallAll();

  Simulator::Stop(Seconds(simulationTime + 0.101));
  Simulator::Run();
  Simulator::Destroy();

  /* Print Flow-Monitor Statistics */
  PrintFlowMonitorStatistics(flowmon, monitor, simulationTime - 1);

  /* Print Results Summary */
  // std::cout << "Total #Received Packets = " <<
  // packetSink->GetTotalReceivedPackets () << std::endl;
  std::cout << "Total Throughput [Mbps] = "
            << throughput / ((simulationTime - 1) * 10) << std::endl;

  return 0;
}
