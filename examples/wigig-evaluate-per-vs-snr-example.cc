/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <complex>
#include <iomanip>
#include <string>

/**
 * Simulation Objective:
 * This script is used to evaluate the performance of the IEEE 802.11ad protocol
 * using a custom SNR to BER lookup tables The tables are generated in MATLAB
 * R2018b using the WLAN Toolbox. For the time being, we assume AWGN channel.
 * For the future, we want to try L2SM approach.
 *
 * Network Topology:
 * The scenario consists of a single DMG STA and a single DMG PCP/AP.
 *
 *          DMG PCP/AP (0,0)                       DMG STA (+1,0)
 *
 * Simulation Description:
 * The DMG STA generates un uplink UDP traffic towards the DMG PCP/AP. The user
 * changes the distance between the DMG STA and the DMG PCP/AP to
 * decrease/increase the received SNR.
 *
 * Running Simulation:
 * ./waf --run wigig-evaluate-per-vs-snr-example
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 * 2. IP Layer Statistics using Flow Monitor Module.
 * 3. Custom traces to report PHY and MAC layer statistics.
 *
 */

NS_LOG_COMPONENT_DEFINE("EvaluateDmgErrorModel");

using namespace ns3;
using namespace std;

/**  Application Variables **/
Ptr<PacketSink> packetSink;
Ptr<OnOffApplication> onoff;

/* Network Nodes */
Ptr<Node> apWigigNode, staWigigNode;
Ptr<WigigNetDevice> staWigigNetDevice, apWigigNetDevice;
Ptr<WigigPhy> staWigigPhy, apWigigPhy;

/* Statistics */
uint64_t macTxDataFailed = 0;
double snr = 0.0;
uint64_t macRxOk = 0;
uint64_t transmittedPackets = 0;
uint64_t droppedPackets = 0;
uint64_t receivedPackets = 0;

void MacRxOk(WifiMacType, Mac48Address, double snrValue) {
  macRxOk++;
  snr += snrValue;
}

void MacTxDataFailed(Mac48Address) { macTxDataFailed++; }

void PhyTxEnd(Ptr<const Packet>) { transmittedPackets++; }

void PhyRxDrop(Ptr<const Packet>, WifiPhyRxfailureReason) { droppedPackets++; }

void PhyRxEnd(Ptr<const Packet>) { receivedPackets++; }

void SetAntennaConfigurations(NetDeviceContainer &apDevice,
                              NetDeviceContainer &staDevice) {
  Ptr<WigigNetDevice> apWigigNetDevice =
      DynamicCast<WigigNetDevice>(apDevice.Get(0));
  Ptr<WigigNetDevice> staWigigNetDevice =
      DynamicCast<WigigNetDevice>(staDevice.Get(0));
  Ptr<AdhocWigigMac> apWigigMac =
      DynamicCast<AdhocWigigMac>(apWigigNetDevice->GetMac());
  Ptr<AdhocWigigMac> staWigigMac =
      DynamicCast<AdhocWigigMac>(staWigigNetDevice->GetMac());
  apWigigMac->AddAntennaConfig(1, 1, staWigigMac->GetAddress());
  staWigigMac->AddAntennaConfig(5, 1, apWigigMac->GetAddress());
  apWigigMac->SteerAntennaToward(staWigigMac->GetAddress());
  staWigigMac->SteerAntennaToward(apWigigMac->GetAddress());
}

int main(int argc, char *argv[]) {
  uint32_t payloadSize = 1472; /* Application payload size in bytes. */
  string dataRate = "150Mbps"; /* Application data rate. */
  double simulationTime = 1;   /* Simulation time in seconds. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";
  double distance = 10.0;

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("payloadSize", "Application payload size in bytes", payloadSize);
  cmd.AddValue("dataRate", "The data rate of the OnOff application", dataRate);
  cmd.AddValue("simulationTime", "Simulation time in Seconds", simulationTime);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.AddValue("distance", "Distance between nodes", distance);
  cmd.Parse(argc, argv);

  AsciiTraceHelper ascii; /* ASCII Helper. */
  Ptr<OutputStreamWrapper> outputFile =
      ascii.CreateFileStream("PER_vs_SNR_11ad.csv");
  //*outputFile->GetStream () <<
  //"MCS,DIST,APP_TX_PKTS,MAC_RX_OK,MAC_TX_FAILED,PHY_TX_PKTS,PHX_RX_PKTS,PHY_RX_DROPPED,SNR"
  //<<
  // std::endl;
  *outputFile->GetStream() << "MCS,DIST,MAC_RX_OK,MAC_TX_FAILED,PHY_TX_PKTS,"
                              "PHX_RX_PKTS,PHY_RX_DROPPED,SNR"
                           << std::endl;

  for (uint8_t mcs = 1; mcs <= 12; mcs++) {
    double distance = 10.0;
    /* Reset Counters */
    macTxDataFailed = 0;
    snr = 0.0;
    macRxOk = 0;
    transmittedPackets = 0;
    droppedPackets = 0;
    receivedPackets = 0;

    /* Configure RTS/CTS and Fragmentation */
    ConfigureRtsCtsAndFragmenatation();

    /**** WigigHelper is a meta-helper: it helps creates helpers ****/
    WigigHelper wigig;

    /**** Set up Channel ****/
    WigigChannelHelper wigigChannel;
    /* Simple propagation delay model */
    wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    /* Friis model with standard-specific wavelength */
    wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                    "Frequency", DoubleValue(60.48e9));

    /**** Setup physical layer ****/
    WigigPhyHelper wigigPhy(wigigErrorModel);
    /* Nodes will be added to the channel we set up earlier */
    wigigPhy.SetChannel(wigigChannel.Create());
    /* All nodes transmit at 0 dBm == 1 mW, no adaptation */
    wigigPhy.Set("TxPowerStart", DoubleValue(0.0));
    wigigPhy.Set("TxPowerEnd", DoubleValue(0.0));
    wigigPhy.Set("TxPowerLevels", UintegerValue(1));
    /* Set operating channel */
    wigigPhy.Set("ChannelNumber", UintegerValue(2));
    /* Set default algorithm for all nodes to be constant rate */
    wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                  StringValue("DmgMcs" + std::to_string(mcs)));

    /* Make two nodes and set them up with the PHY and the MAC */
    NodeContainer wigigNodes;
    wigigNodes.Create(2);
    apWigigNode = wigigNodes.Get(0);
    staWigigNode = wigigNodes.Get(1);

    /* Add a DMG upper mac */
    WigigMacHelper wigigMac;

    /* Set Analytical Codebook for the DMG Devices */
    wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                      EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                      "Sectors", UintegerValue(8));

    /* Create Wigig Network Devices (WifiNetDevice) */
    wigigMac.SetType(
        "ns3::AdhocWigigMac", "BE_MaxAmpduSize",
        UintegerValue(
            0), // Enable A-MPDU with the maximum size allowed by the standard.
        "BE_MaxAmsduSize", UintegerValue(0));

    NetDeviceContainer apDevice;
    apDevice = wigig.Install(wigigPhy, wigigMac, apWigigNode);

    NetDeviceContainer staDevice;
    staDevice = wigig.Install(wigigPhy, wigigMac, staWigigNode);

    /* Set the best antenna configurations */
    Simulator::ScheduleNow(&SetAntennaConfigurations, apDevice, staDevice);

    /* Setting mobility model */
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc =
        CreateObject<ListPositionAllocator>();
    positionAlloc->Add(Vector(0.0, 0.0, 0.0));      /* DMG PCP/AP */
    positionAlloc->Add(Vector(distance, 0.0, 0.0)); /* DMG STA */

    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wigigNodes);

    /* Internet stack*/
    InternetStackHelper stack;
    stack.Install(wigigNodes);

    Ipv4AddressHelper address;
    address.SetBase("10.0.0.0", "255.255.255.0");
    Ipv4InterfaceContainer apInterface;
    apInterface = address.Assign(apDevice);
    Ipv4InterfaceContainer staInterface;
    staInterface = address.Assign(staDevice);

    /* Populate routing table */
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    /* We do not want any ARP packets */
    PopulateArpCache();

    /* Install Simple UDP Server on the DMG AP */
    PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                                InetSocketAddress(Ipv4Address::GetAny(), 9999));
    ApplicationContainer sinkApp = sinkHelper.Install(apWigigNode);
    packetSink = StaticCast<PacketSink>(sinkApp.Get(0));
    sinkApp.Start(Seconds(0.0));

    /* Install UDP Transmitter on the DMG STA */
    ApplicationContainer srcApp;
    OnOffHelper src("ns3::UdpSocketFactory",
                    InetSocketAddress(apInterface.GetAddress(0), 9999));
    // src.SetAttribute ("MaxPackets", UintegerValue (0));
    src.SetAttribute("PacketSize", UintegerValue(payloadSize));
    src.SetAttribute("OnTime",
                     StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
    src.SetAttribute("OffTime",
                     StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
    srcApp = src.Install(staWigigNode);
    srcApp.Start(Seconds(0.0));
    srcApp.Stop(Seconds(simulationTime));
    onoff = StaticCast<OnOffApplication>(srcApp.Get(0));

    /* Stations */
    apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
    apWigigPhy = StaticCast<WigigPhy>(apWigigNetDevice->GetPhy());
    staWigigNetDevice = StaticCast<WigigNetDevice>(staDevice.Get(0));
    staWigigPhy = StaticCast<WigigPhy>(staWigigNetDevice->GetPhy());
    auto staRemoteStationManager = staWigigNetDevice->GetRemoteStationManager();

    /* Connect MAC and PHY Traces */
    apWigigPhy->TraceConnectWithoutContext("PhyRxEnd", MakeCallback(&PhyRxEnd));
    apWigigPhy->TraceConnectWithoutContext("PhyRxDrop",
                                           MakeCallback(&PhyRxDrop));
    staWigigPhy->TraceConnectWithoutContext("PhyTxEnd",
                                            MakeCallback(&PhyTxEnd));
    staRemoteStationManager->TraceConnectWithoutContext(
        "MacTxDataFailed", MakeCallback(&MacTxDataFailed));
    staRemoteStationManager->TraceConnectWithoutContext("MacRxOK",
                                                        MakeCallback(&MacRxOk));

    /* Change the maximum number of retransmission attempts for a DATA packet */
    staRemoteStationManager->SetAttribute("MaxSlrc", UintegerValue(0));

    Simulator::Stop(Seconds(simulationTime + 0.101));
    Simulator::Run();
    Simulator::Destroy();

    *outputFile->GetStream()
        << uint16_t(mcs) << ","
        << distance /*<< "," << onoff->GetTotalTxPackets ()*/
        << "," << macRxOk << "," << macTxDataFailed << "," << transmittedPackets
        << "," << receivedPackets << "," << droppedPackets << ","
        << RatioToDb(snr / double(macRxOk)) << std::endl;
  }

  return 0;
}
