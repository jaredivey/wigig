/*
 * Copyright (c) 2015-2020 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "wigig-examples-common-functions.h"

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wigig-module.h"

#include <iomanip>

/**
 * Simulation Objective:
 * Evaluate the allocation of multiple static service periods in the DTI access
 * period.
 *
 * Network Topology:
 * The scenario consists of 3 DMG STAs (West + South + East) and one DMG PCP/AP
 * as following:
 *
 *                        DMG PCP/AP (0,1)
 *
 *
 * West DMG STA (-1,0)                      East DMG STA (1,0)
 *
 *
 *                      South DMG STA (0,-1)
 *
 * Simulation Description:
 * Once all the stations have associated successfully with the PCP/AP. The
 * PCP/AP allocates three SPs to perform TXSS between all the stations. Once
 * West DMG STA has completed TXSS phase with East and South DMG STAs. The
 * PCP/AP allocates three static service periods for data communication as
 * following:
 *
 * SP1: West DMG STA -----> East DMG STA (SP Length = 3.2ms)
 * SP2: West DMG STA -----> South DMG STA (SP Length = 3.2ms)
 * SP3: DMG PCP/AP -----> West DMG STA (SP Length = 5ms)
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run wigig-evaluate-multi-service-period-example
 *
 * To run the script with different duration for the allocations e.g. SP1=10ms
 * and SP2=5ms:
 * ./waf --run "wigig-evaluate-multi-service-period-example --sp1Duration=10000
 * --sp2Duration=5000"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see that data
 * transmission takes place during its SP. In addition, we can notice in the
 * announcement of the two Static Allocation Periods inside each DMG Beacon.
 *
 */

NS_LOG_COMPONENT_DEFINE("MultiSP");

using namespace ns3;
using namespace std;

/** West -> East Allocation Variables **/
uint64_t westEastLastTotalRx = 0;
double westEastAverageThroughput = 0;
/** West -> South Node Allocation Variables **/
uint64_t westSouthTotalRx = 0;
double westSouthAverageThroughput = 0;
/** AP -> West Node Allocation Variables **/
uint64_t apWestTotalRx = 0;
double apWestAverageThroughput = 0;

Ptr<PacketSink> sink1, sink2, sink3;

/* Network Nodes */
Ptr<WigigNetDevice> apWigigNetDevice;
Ptr<WigigNetDevice> southWigigNetDevice;
Ptr<WigigNetDevice> westWigigNetDevice;
Ptr<WigigNetDevice> eastWigigNetDevice;
NetDeviceContainer staDevices;
Ptr<ApWigigMac> apWigigMac;
Ptr<StaWigigMac> southWigigMac;
Ptr<StaWigigMac> westWigigMac;
Ptr<StaWigigMac> eastWigigMac;

/*** Access Point Variables ***/
uint8_t associatedStations =
    0; /* Total number of associated stations with the AP. */
uint8_t stationsTrained = 0;         /* Number of BF trained stations. */
bool scheduledStaticPeriods = false; /* Flag to indicate whether we scheduled
                                        Static Service Periods or not. */

/*** Service Period Parameters ***/
uint16_t sp1Duration = 3200; /* The duration of the allocated service period (1)
                                in MicroSeconds. */
uint16_t sp2Duration = 3200; /* The duration of the allocated service period (2)
                                in MicroSeconds. */
uint16_t sp3Duration = 5000; /* The duration of the allocated service period (3)
                                in MicroSeconds. */

void CalculateThroughput() {
  double thr1;
  double thr2;
  double thr3;
  Time now = Simulator::Now();
  thr1 = CalculateSingleStreamThroughput(sink1, westEastLastTotalRx,
                                         westEastAverageThroughput);
  thr2 = CalculateSingleStreamThroughput(sink2, westSouthTotalRx,
                                         westSouthAverageThroughput);
  thr3 = CalculateSingleStreamThroughput(sink3, apWestTotalRx,
                                         apWestAverageThroughput);
  std::cout << std::left << std::setw(12) << now.GetSeconds() << std::left
            << std::setw(12) << thr1 << std::left << std::setw(12) << thr2
            << std::left << std::setw(12) << thr3 << std::endl;
  Simulator::Schedule(MilliSeconds(100), &CalculateThroughput);
}

void StationAssociated(Ptr<StaWigigMac> staWigigMac, Mac48Address address,
                       uint16_t aid) {
  std::cout << "DMG STA: " << staWigigMac->GetAddress()
            << " associated with DMG PCP/AP: " << address << std::endl;
  std::cout << "Association ID (AID) = " << staWigigMac->GetAssociationId()
            << std::endl;
  associatedStations++;
  /* Check if all stations have associated with the PCP/AP */
  if (associatedStations == 3) {
    /* Map AID to MAC Addresses in each node instead of requesting information
     */
    Ptr<StaWigigMac> srcMac;
    Ptr<StaWigigMac> dstMac;
    for (NetDeviceContainer::Iterator i = staDevices.Begin();
         i != staDevices.End(); ++i) {
      srcMac =
          StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*i)->GetMac());
      for (NetDeviceContainer::Iterator j = staDevices.Begin();
           j != staDevices.End(); ++j) {
        dstMac =
            StaticCast<StaWigigMac>(StaticCast<WigigNetDevice>(*j)->GetMac());
        if (srcMac != dstMac) {
          srcMac->MapAidToMacAddress(dstMac->GetAssociationId(),
                                     dstMac->GetAddress());
        }
      }
    }

    std::cout << "All stations got associated with DMG PCP/AP: " << address
              << std::endl;

    /* For simplicity we assume that each station is aware of the capabilities
     * of the peer station */
    /* Otherwise, we have to request the capabilities of the peer station. */
    westWigigMac->StorePeerDmgCapabilities(eastWigigMac);
    westWigigMac->StorePeerDmgCapabilities(southWigigMac);
    eastWigigMac->StorePeerDmgCapabilities(westWigigMac);
    eastWigigMac->StorePeerDmgCapabilities(southWigigMac);
    southWigigMac->StorePeerDmgCapabilities(westWigigMac);
    southWigigMac->StorePeerDmgCapabilities(eastWigigMac);

    /* Schedule Beamforming Training SPs */
    uint32_t allocationStart = 0;
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(),
        allocationStart, true);
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        westWigigMac->GetAssociationId(), southWigigMac->GetAssociationId(),
        allocationStart, true);
    allocationStart = apWigigMac->AllocateBeamformingServicePeriod(
        southWigigMac->GetAssociationId(), eastWigigMac->GetAssociationId(),
        allocationStart, true);
  }
}

void SLSCompleted(Ptr<WigigMac> staWigigMac,
                  SlsCompletionAttrbitutes attributes) {
  if (attributes.accessPeriod == CHANNEL_ACCESS_DTI) {
    std::cout << "DMG STA " << staWigigMac->GetAddress()
              << " completed SLS phase with DMG STA " << attributes.peerStation
              << std::endl;
    std::cout << "The best antenna configuration is AntennaId="
              << uint16_t(attributes.antennaID)
              << ", SectorId=" << uint16_t(attributes.sectorID) << std::endl;
    stationsTrained++;
    if ((stationsTrained == 6) & !scheduledStaticPeriods) {
      std::cout << "Schedule Static Periods" << std::endl;
      scheduledStaticPeriods = true;
      /* Schedule Static Periods */
      uint32_t startAllocation = 0;
      Time guardTime = MicroSeconds(10);
      /* SP1 */
      startAllocation = apWigigMac->AllocateSingleContiguousBlock(
          1, SERVICE_PERIOD_ALLOCATION, true, westWigigMac->GetAssociationId(),
          eastWigigMac->GetAssociationId(), startAllocation, sp1Duration);
      /* SP2 */
      startAllocation = apWigigMac->AllocateSingleContiguousBlock(
          2, SERVICE_PERIOD_ALLOCATION, true, westWigigMac->GetAssociationId(),
          southWigigMac->GetAssociationId(),
          startAllocation + guardTime.GetMicroSeconds(), sp2Duration);
      /* SP3 */
      apWigigMac->AllocateSingleContiguousBlock(
          3, SERVICE_PERIOD_ALLOCATION, true, AID_AP,
          westWigigMac->GetAssociationId(),
          startAllocation + guardTime.GetMicroSeconds(), sp3Duration);
    }
  }
}

int main(int argc, char *argv[]) {
  uint32_t packetSize = 1472; /* Transport Layer Payload size in bytes. */
  string dataRate1 =
      "50Mbps"; /* Application Layer Data Rate for WestNode->EastNode. */
  string dataRate2 =
      "40Mbps"; /* Application Layer Data Rate for WestNode->SouthNode. */
  string dataRate3 =
      "10Mbps"; /* Application Layer Data Rate for ApNode->WestNode. */
  string msduAggSize = MAX_DMG_AMSDU_LENGTH; /* The maximum aggregation size for
                                                A-MSDU in Bytes. */
  string mpduAggSize =
      "0"; /* The maximum aggregation size for A-MPDU in Bytes. */
  string queueSize = "1000p";  /* Wigig MAC Queue Size. */
  string phyMode = "DmgMcs12"; /* Type of the Physical Layer. */
  bool verbose = false;        /* Print Logging Information. */
  double simulationTime = 10;  /* Simulation time in seconds. */
  bool pcapTracing = false;    /* PCAP Tracing is enabled or not. */
  string wigigErrorModel =
      "contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt";

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue("packetSize", "Payload size in bytes", packetSize);
  cmd.AddValue("dataRate1",
               "Data rate for OnOff Application WestNode->EastNode", dataRate1);
  cmd.AddValue("dataRate2",
               "Data rate for OnOff Application WestNode->SouthNode",
               dataRate2);
  cmd.AddValue("dataRate3", "Data rate for OnOff Application ApNode->WestNode",
               dataRate3);
  cmd.AddValue("msduAggSize",
               "The maximum aggregation size for A-MSDU in Bytes", msduAggSize);
  cmd.AddValue("queueSize", "The size of the Wigig Mac Queue", queueSize);
  cmd.AddValue("sp1Duration",
               "The duration of service period (1) in MicroSeconds",
               sp1Duration);
  cmd.AddValue("sp2Duration",
               "The duration of service period (2) in MicroSeconds",
               sp2Duration);
  cmd.AddValue("sp3Duration",
               "The duration of service period (3) in MicroSeconds",
               sp3Duration);
  cmd.AddValue("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue("errorModel", "Path to the Wigig error model", wigigErrorModel);
  cmd.Parse(argc, argv);

  /* Validate A-MSDU and A-MPDU values */
  ValidateFrameAggregationAttributes(msduAggSize, mpduAggSize);
  /* Configure RTS/CTS and Fragmentation */
  ConfigureRtsCtsAndFragmenatation();
  /* Wigig MAC Queue Parameters */
  ChangeQueueSize(queueSize);

  /**** WigigHelper is a meta-helper: it helps creates helpers ****/
  WigigHelper wigig;

  /* Turn on logging */
  if (verbose) {
    ns3::WigigHelper::EnableLogComponents();
    LogComponentEnable("EvaluateServicePeriod", LOG_LEVEL_ALL);
  }

  /**** Set up Channel ****/
  WigigChannelHelper wigigChannel;
  /* Simple propagation delay model */
  wigigChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wigigChannel.AddPropagationLoss("ns3::FriisPropagationLossModel", "Frequency",
                                  DoubleValue(60.48e9));

  /**** Setup physical layer ****/
  WigigPhyHelper wigigPhy(wigigErrorModel);
  /* Nodes will be added to the channel we set up earlier */
  wigigPhy.SetChannel(wigigChannel.Create());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wigigPhy.Set("TxPowerStart", DoubleValue(10.0));
  wigigPhy.Set("TxPowerEnd", DoubleValue(10.0));
  wigigPhy.Set("TxPowerLevels", UintegerValue(1));
  /* Set operating channel */
  wigigPhy.Set("ChannelNumber", UintegerValue(2));
  /* Set default algorithm for all nodes to be constant rate */
  wigig.SetRemoteStationManager("ns3::ConstantRateWigigManager", "DataMode",
                                StringValue(phyMode));

  /* Make four nodes and set them up with the PHY and the MAC */
  NodeContainer wigigNodes;
  wigigNodes.Create(4);
  Ptr<Node> apNode = wigigNodes.Get(0);
  Ptr<Node> westNode = wigigNodes.Get(1);
  Ptr<Node> southNode = wigigNodes.Get(2);
  Ptr<Node> eastNode = wigigNodes.Get(3);

  /* Add a DMG upper mac */
  WigigMacHelper wigigMac;

  /* Install DMG PCP/AP Node */
  Ssid ssid = Ssid("MultiSP");
  wigigMac.SetType(
      "ns3::ApWigigMac", "Ssid", SsidValue(ssid), "BE_MaxAmpduSize",
      StringValue(mpduAggSize), "BE_MaxAmsduSize", StringValue(msduAggSize),
      "SSSlotsPerAbft", UintegerValue(8), "SSFramesPerSlot", UintegerValue(8),
      "BeaconInterval", TimeValue(MicroSeconds(102400)));

  /* Set Simple Analytical Codebook for the DMG Devices */
  wigig.SetCodebook("ns3::CodebookAnalytical", "CodebookType",
                    EnumValue(SIMPLE_CODEBOOK), "Antennas", UintegerValue(1),
                    "Sectors", UintegerValue(8));

  NetDeviceContainer apDevice;
  apDevice = wigig.Install(wigigPhy, wigigMac, apNode);

  /* Install DMG STA Nodes */
  wigigMac.SetType("ns3::StaWigigMac", "Ssid", SsidValue(ssid), "ActiveProbing",
                   BooleanValue(false), "BE_MaxAmpduSize",
                   StringValue(mpduAggSize), "BE_MaxAmsduSize",
                   StringValue(msduAggSize));

  staDevices = wigig.Install(wigigPhy, wigigMac,
                             NodeContainer(westNode, southNode, eastNode));

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc =
      CreateObject<ListPositionAllocator>();
  positionAlloc->Add(Vector(0.0, +1.0, 0.0)); /* PCP/AP */
  positionAlloc->Add(Vector(-1.0, 0.0, 0.0)); /* West STA */
  positionAlloc->Add(Vector(0.0, -1.0, 0.0)); /* South STA */
  positionAlloc->Add(Vector(+1.0, 0.0, 0.0)); /* East STA */

  mobility.SetPositionAllocator(positionAlloc);
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wigigNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wigigNodes);

  Ipv4AddressHelper address;
  address.SetBase("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer ApInterface;
  ApInterface = address.Assign(apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign(staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  /* We do not want any ARP packets */
  PopulateArpCache();

  /*** Install Applications ***/

  /* Install Simple UDP Server on both south and east Node */
  PacketSinkHelper sinkHelper("ns3::UdpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 9999));
  ApplicationContainer sinks =
      sinkHelper.Install(NodeContainer(eastNode, southNode, westNode));
  sink1 = StaticCast<PacketSink>(sinks.Get(0));
  sink2 = StaticCast<PacketSink>(sinks.Get(1));
  sink3 = StaticCast<PacketSink>(sinks.Get(2));

  /* Install Simple UDP Transmitter on the West Node (Transmit to the East Node)
   */
  ApplicationContainer srcApp;
  OnOffHelper src("ns3::UdpSocketFactory",
                  InetSocketAddress(staInterfaces.GetAddress(2), 9999));
  // src.SetAttribute ("MaxPackets", UintegerValue (0));
  src.SetAttribute("PacketSize", UintegerValue(packetSize));
  src.SetAttribute("OnTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=1e6]"));
  src.SetAttribute("OffTime",
                   StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate1)));
  // src.SetAttribute ("EnableE2EStats", BooleanValue (true));
  srcApp.Add(src.Install(westNode));

  /* Install Simple UDP Transmitter on the West Node (Transmit to the South
   * Node) */
  src.SetAttribute("Remote", AddressValue(InetSocketAddress(
                                 staInterfaces.GetAddress(1), 9999)));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate2)));
  srcApp.Add(src.Install(westNode));

  /* Install Simple UDP Transmitter on the AP Node (Transmit to the West Node)
   */
  src.SetAttribute("Remote", AddressValue(InetSocketAddress(
                                 staInterfaces.GetAddress(0), 9999)));
  src.SetAttribute("DataRate", DataRateValue(DataRate(dataRate3)));
  srcApp.Add(src.Install(apNode));

  /* Start and stop applications */
  srcApp.Start(Seconds(3.0));
  srcApp.Stop(Seconds(simulationTime));
  sinks.Start(Seconds(3.0));

  /* Schedule Throughput Calulcations */
  Simulator::Schedule(Seconds(3.1), &CalculateThroughput);

  /* Enable Traces */
  if (pcapTracing) {
    wigigPhy.SetPcapDataLinkType(WigigPhyHelper::DLT_IEEE802_11_RADIO);
    wigigPhy.SetSnapshotLength(160);
    wigigPhy.EnablePcap("AccessPoint", apDevice, false);
    wigigPhy.EnablePcap("WestNode", staDevices.Get(0), false);
    wigigPhy.EnablePcap("SouthNode", staDevices.Get(1), false);
    wigigPhy.EnablePcap("EastNode", staDevices.Get(2), false);
  }

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  /* Stations */
  apWigigNetDevice = StaticCast<WigigNetDevice>(apDevice.Get(0));
  westWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(0));
  southWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(1));
  eastWigigNetDevice = StaticCast<WigigNetDevice>(staDevices.Get(2));

  apWigigMac = StaticCast<ApWigigMac>(apWigigNetDevice->GetMac());
  westWigigMac = StaticCast<StaWigigMac>(westWigigNetDevice->GetMac());
  southWigigMac = StaticCast<StaWigigMac>(southWigigNetDevice->GetMac());
  eastWigigMac = StaticCast<StaWigigMac>(eastWigigNetDevice->GetMac());

  /** Connect Traces **/
  westWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, westWigigMac));
  southWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, southWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "Assoc", MakeBoundCallback(&StationAssociated, eastWigigMac));

  westWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, westWigigMac));
  southWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, southWigigMac));
  eastWigigMac->TraceConnectWithoutContext(
      "SLSCompleted", MakeBoundCallback(&SLSCompleted, eastWigigMac));

  /* Print Output */
  std::cout << std::left << std::setw(12) << "Time  [s]" << std::left
            << std::setw(12) << "SP1 [Mbps]" << std::left << std::setw(12)
            << "SP2 [Mbps]" << std::left << std::setw(12) << "SP3 [Mbps]"
            << std::endl;

  Simulator::Stop(Seconds(simulationTime + 0.101));
  Simulator::Run();
  Simulator::Destroy();

  /* Print per flow statistics */
  PrintFlowMonitorStatistics(flowmon, monitor, simulationTime - 1);

  /* Print Application Layer Results Summary */
  Ptr<OnOffApplication> onoff;
  Ptr<PacketSink> sink;
  std::cout << "\nApplication Layer Statistics:" << std::endl;
  ;
  for (uint32_t i = 0; i < srcApp.GetN(); i++) {
    onoff = StaticCast<OnOffApplication>(srcApp.Get(i));
    sink = StaticCast<PacketSink>(sinks.Get(i));
    std::cout << "Stats (" << i + 1 << ")" << std::endl;
    // std::cout << "  Tx Packets: " << onoff->GetTotalTxPackets () <<
    // std::endl; std::cout << "  Tx Bytes:   " << onoff->GetTotalTxBytes () <<
    // std::endl; std::cout << "  Rx Packets: " << sink->GetTotalReceivedPackets
    // () << std::endl;
    std::cout << "  Rx Bytes:   " << sink->GetTotalRx() << std::endl;
    std::cout << "  Throughput: "
              << sink->GetTotalRx() * 8.0 / ((simulationTime - 3) * 1e6)
              << " Mbps" << std::endl;
  }

  return 0;
}
